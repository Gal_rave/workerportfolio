﻿using System;
using System.Globalization;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;

using WebServiceProvider.SmsManager;
using WebServiceProvider.DataMappers;
using DataAccess.Extensions;
using DataAccess.Models;
using System.Threading.Tasks;
using DataAccess.BLLs;
using Logger;

namespace WebServiceProvider
{
    public static class SmsManagerProvider
    {
        #region private class parameters
        private static PortalSmsManagerWSService manager;
        private static bool isInitialized = false;
        private static int headerCustomer;
        private static int headerRecipient;
        private static int headerSender;
        private static string headerUserId;
        private static string headerUserPass;
        private static string Sender;
        private static string Signature;
        private static bool onlyIsraelPhones;
        #endregion private class parameters

        private static void initializ()
        {
            if (isInitialized)
                return;
            manager = new PortalSmsManagerWSService();
            manager.Timeout = 123456789;
            manager.Url = ConfigurationManager.AppSettings["SmsManagerProvider"];

            headerCustomer = ConfigurationManager.AppSettings["SMS_headerCustomer"].ToInt();
            headerRecipient = ConfigurationManager.AppSettings["SMS_headerRecipient"].ToInt();
            headerSender = ConfigurationManager.AppSettings["SMS_headerSender"].ToInt();
            headerUserId = ConfigurationManager.AppSettings["SMS_headerUserId"];
            headerUserPass = ConfigurationManager.AppSettings["SMS_headerUserPass"];
            Sender = ConfigurationManager.AppSettings["SMS_Sender"];
            Signature = ConfigurationManager.AppSettings["SMS_Signature"];
            onlyIsraelPhones = ConfigurationManager.AppSettings["SMS_onlyIsraelPhones"].ToBool();
            
            isInitialized = true;
        }
        
        public static ReturnObject SendSMS(SmsNotificationModel model, bool inErorr = false)
        {
            initializ();
            MailBll bll = new MailBll();

            ReturnObject obj = doSend(model.message, model.phoneNumber.ToStringArray());
            
            model.response = obj.ToJSON();
            bll.SaveSMS(model);

            if (obj.Msg.RcNumber > 0 && obj.Msg.RcType == 9 && !inErorr)
            {
                return SendSMS(model, true);
            }

            return obj;
        }

        #region private
        private static ReturnObject doSend(string text, string[] phones)
        {
            initializ();
            try
            {
                return manager.addSmsRequest(getHeader(), text, onlyIsraelPhones, phones, "", false, "now", "", "", Sender, Signature, "", "", "");
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        private static ReturnObject SendSMS(SystemHeader systemHeader, string smsText, bool onlyIsraelPhones, string[] phones, string requestKey, bool withPostConfirmation, string scheduledDate, string smsTTSInMinutes, string smsTTLInMinutes, string smsSender, string smsSignature, string smsRateType, string smsSpecificRate, string smsSystemRate)
        {
            initializ();

            ReturnObject obj = manager.addSmsRequest(getHeader(), smsText, onlyIsraelPhones, phones, requestKey, withPostConfirmation, scheduledDate, smsTTSInMinutes, smsTTLInMinutes, smsSender, smsSignature, smsRateType, smsSpecificRate, smsSystemRate);
            return obj;
        }
        private static SystemHeader getHeader()
        {
            return new SystemHeader
            {
                Customer = headerCustomer,
                Recipient = headerRecipient,
                Sender = headerSender,
                UserId = headerUserId,
                UserPass = headerUserPass,
            };
        }
        #endregion
    }
}
