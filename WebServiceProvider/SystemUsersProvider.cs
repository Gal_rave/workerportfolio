﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Linq;
using System.Data;
using System.Net.Http;
using System.Data.OleDb;
using System.Globalization;
using System.Configuration;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using DataAccess.Extensions;
using DataAccess.Models;
using DataAccess.BLLs;
using AsyncHelpers;
using Logger;
using Newtonsoft.Json;
using System.Web;

namespace WebServiceProvider
{
    public static class SystemUsersProvider
    {
        private static bool isInitialized;
        private static string url;
        private static bool isDebugeMode;
        private static void initializ()
        {
            if (isInitialized)
                return;
            url = ConfigurationManager.AppSettings["SystemUsersProvider"];
            isDebugeMode = ConfigurationManager.AppSettings["isDebugeMode"].ToBool();
            isInitialized = true;
        }

        public static DataTable GetDataTableFromCsv(string path, bool isFirstRowHeader)
        {
            initializ();

            string header = isFirstRowHeader ? "Yes" : "No";

            string pathOnly = Path.GetDirectoryName(path);
            string fileName = Path.GetFileName(path);

            string sql = @"SELECT * FROM [" + fileName + "]";

            using (OleDbConnection connection = new OleDbConnection(
                      @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathOnly +
                      ";Extended Properties=\"Text;HDR=" + header + "\""))
            using (OleDbCommand command = new OleDbCommand(sql, connection))
            using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            {
                DataTable dataTable = new DataTable();
                dataTable.Locale = CultureInfo.CurrentCulture;
                adapter.Fill(dataTable);
                return dataTable;
            }
        }
        public static async void SaveTemporerySystemUsers(string csv, string uploads, int fileCounter)
        {
            initializ();
            try
            {
                SetUpBll bll = new SetUpBll();
                using (StreamWriter outputFile = new StreamWriter(uploads))
                {
                    outputFile.Write(csv);
                }

                DataTable dt = GetDataTableFromCsv(uploads, true);
                AsyncFunctionCaller.RunAsync(() => { bll.SaveDatatable(dt, fileCounter); });

                await Task.Delay(25);
                File.Delete(uploads);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static void RunImportSystemUsers()
        {
            initializ();
            Log.ApplicationLog("Start RunImportSystemUsers");
            SetUpBll bll = new SetUpBll();

            Log.ApplicationLog("SpliteSystemUsersEmails");
            bll.SpliteSystemUsersEmails();
            Log.ApplicationLog("SpliteSystemUsersEmails END");

            Log.ApplicationLog("SPImportSystemUsers");
            AsyncFunctionCaller.RunAsyncAwait(HttpContext.Current, () => { bll.SPImportSystemUsers(); });
            Log.ApplicationLog("SPImportSystemUsers END");

            Log.ApplicationLog("FixSystemUsersIdNumbers");
            bll.FixSystemUsersIdNumbers();
            Log.ApplicationLog("FixSystemUsersIdNumbers END");

            Log.ApplicationLog("UpdatFromSystemAndRefresh");
            AsyncFunctionCaller.RunAsyncAwait(HttpContext.Current, () => { bll.UpdatFromSystemAndRefresh(); });
            Log.ApplicationLog("UpdatFromSystemAndRefresh END");

            Log.ApplicationLog("End RunImportSystemUsers");
        }
        public static void StartImportProcessFromWS()
        {
            initializ();
            RegisterBll bll = new RegisterBll();
            List<int> municipalities = bll.GetAllmunicipalities(true);

            bool start = true;
            int counter = 0;
            foreach (IEnumerable<int> mun in municipalities.Chunk(25))
            {
                Log.LogLoad(string.Join(",", mun.Select(n => n.ToString()).ToArray()), "StartImportProcessFromWS");
                counter += getResponseString(string.Format("{0}GetWorkers?municipalities={1}{2}", url, string.Join(",", mun.Select(n => n.ToString()).ToArray()), start ? "&start=true" : ""));
                start = false;
            }
            getResponseString(string.Format("{0}GetWorkers?municipalities={1}", url, "&end=true"));
            Log.LogLoad(string.Format("StartImportProcessFromWS => Total Users Inport ==>> {0}", counter));
        }
        public static List<SYSTEM_CITIES> GetCitiesFromWS()
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}", url, "GetCityStreet?getType=true"));
                    var str = Encoding.UTF8.GetString(contact);
                    List<SYSTEM_CITIES> cities = str.ToClass<List<SYSTEM_CITIES>>();

                    return cities;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static List<SYSTEM_STREETS> GetStreetsFromWS()
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}", url, "GetCityStreet?getType=false"));
                    var str = Encoding.UTF8.GetString(contact);
                    List<SYSTEM_STREETS> streets = str.ToClass<List<SYSTEM_STREETS>>();

                    return streets;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static void StartImportmunicipalityProcessFromWS(int municipality)
        {
            initializ();
            getResponseString(string.Format("{0}GetWorkers?municipalities={1}{2}", url, municipality, "&start=true"));
            getResponseString(string.Format("{0}GetWorkers?municipalities={1}", url, "&end=true"));
        }
        public static void StartImportCustomMunicipalitiesFromWS(string municipalities)
        {
            initializ();
            getResponseString(string.Format("{0}GetWorkers?municipalities={1}{2}", url, municipalities, "&start=true"));
            getResponseString(string.Format("{0}GetWorkers?municipalities={1}", url, "&end=true"));
        }
        public static List<MishtemaeshDinamiModel> getMishtamshimDinamim(int rashutId, int mncplityId, List<int> types)
        {
            initializ();

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}{2}{3}{4}", url, "getMishtamshimDinamiim?municipality=", mncplityId, "&rashut=", rashutId));
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 180000;

                //TEMP
                types = new List<int> { 4, 5, 4, 6, 7 };

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    try
                    {
                        Log.FormLog("getMishtamshimDinamim streamWriter 1");
                        string json = types.ToJSON();
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                        Log.FormLog("getMishtamshimDinamim streamWriter 2");
                    }
                    catch (Exception Iex)
                    {
                        throw Iex.LogError();
                    }
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Log.FormLog("getMishtamshimDinamim streamWriter 3 END");
                    Log.FormLog(result);
                    return result.ToClass<List<MishtemaeshDinamiModel>>();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static List<PackedProcessModel> GetPackedProcessesData(int municipality)
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}{2}", url, "getUserProcesses?rashut=", municipality));
                    var str = Encoding.UTF8.GetString(contact);
                    List<PackedProcessModel> packedProcessModel = str.ToClass<List<PackedProcessModel>>();

                    return packedProcessModel;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static List<PackedProcessModel> GetPackedProcessesData(int municipality, string calcUserId)
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}{2}", url, "getUserProcesses?rashut=", municipality));
                    var str = Encoding.UTF8.GetString(contact);
                    List<PackedProcessModel> packedProcessModel = str.ToClass<List<PackedProcessModel>>();

                    return packedProcessModel;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static List<int> getWorkersIds(int rashut, int mncplity, List<YehidaIrgunitModel> units)
        {
            initializ();

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}{2}{3}{4}", url, "getWorkersIdsForUnits?rashut=", rashut, "&mncplity=", mncplity));
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 180000;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    try
                    {
                        Log.FormLog("getWorkersIds streamWriter 1");
                        string json = units.ToJSON();
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                        Log.FormLog("getWorkersIds streamWriter 2");
                    }
                    catch (Exception Iex)
                    {
                        throw Iex.LogError();
                    }
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Log.FormLog("getWorkersIds streamWriter 3 END");
                    Log.FormLog(result);
                    return result.Replace("[", "").Replace("]", "").ToIntLIst();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static List<SACHAR_USERS_GELEM> GetSacharSystemUsers(List<int> municipalities)
        {
            initializ();

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}", url, "getSacharSystemUsers"));
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 180000;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    try
                    {
                        Log.FormLog("getSacharSystemUsers streamWriter 1");
                        string json = municipalities.ToJSON();
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                        Log.FormLog("getSacharSystemUsers streamWriter 2");
                    }
                    catch (Exception Iex)
                    {
                        throw Iex.LogError();
                    }
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Log.FormLog("getSacharSystemUsers streamWriter 3 END");
                    Log.FormLog(result);
                    return result.ToClass<List<SACHAR_USERS_GELEM>>();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static List<UnitManagerModel> getOrganizationUnitsManagers(int municipality, string calcUserId)
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}{2}", url, "getOrganizationUnitsManagers?municipality=", municipality));
                    var str = Encoding.UTF8.GetString(contact);
                    List<UnitManagerModel> unitManagers = str.ToClass<List<UnitManagerModel>>();
                    return unitManagers;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static List<UnitManagerModel> getOrganizationUnitsManagers(int municipality)
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}{2}", url, "getOrganizationUnitsManagers?municipality=", municipality));
                    var str = Encoding.UTF8.GetString(contact);
                    List<UnitManagerModel> unitManagers = str.ToClass<List<UnitManagerModel>>();
                    return unitManagers;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static List<YehidaIrgunitModel> getYehidaIrgunit(int municipality, string calcUserId)
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}{2}{3}{4}", url, "getUserYehidaIrgunit?municipality=", municipality, "&userId=", calcUserId));
                    var str = Encoding.UTF8.GetString(contact);
                    List<YehidaIrgunitModel> yehidot = str.ToClass<List<YehidaIrgunitModel>>();
                    return yehidot;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static List<UpdateProcessModel> GetProcessesStatus(List<int> municipalities)
        {
            initializ();

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}", url, "getProcessesStatus"));
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 180000;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    try
                    {
                        Log.FormLog("GetProcessesStatus streamWriter 1");
                        string json = municipalities.ToJSON();
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                        Log.FormLog("GetProcessesStatus streamWriter 2");
                    }
                    catch (Exception Iex)
                    {
                        throw Iex.LogError();
                    }
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Log.FormLog("GetProcessesStatus streamWriter 3 END");
                    Log.FormLog(result);
                    return result.ToClass<List<UpdateProcessModel>>();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static ProcessRCModel PostVacationForm(List<VacationFormModel> models)
        {
            initializ();
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}", url, "postTikOvedVacationForm"));
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 180000;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    try
                    {
                        Log.FormLog("PostVacationForm streamWriter 1");
                        string json = models.ToJSON();
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                        Log.FormLog("PostVacationForm streamWriter 2");
                    }
                    catch (Exception Iex)
                    {
                        throw Iex.LogError();
                    }
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Log.FormLog("PostVacationForm streamWriter 3 END");
                    Log.FormLog(result);
                    ProcessRCModel res = result.ToClass<ProcessRCModel>();
                    return res;
                }
            }
            catch (WebException wex)
            {
                var pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();
                pageContent.LogError();
                throw wex.LogError();
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static ProcessRCModel PostCourseForm(List<CourseFormModel> models)
        {
            initializ();
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}", url, "postTikOvedCourseForm"));
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 180000;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    try
                    {
                        Log.FormLog("PostCourseForm streamWriter 1");
                        string json = models.ToJSON();
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                        Log.FormLog("PostCourseForm streamWriter 2");
                    }
                    catch (Exception Iex)
                    {
                        throw Iex.LogError();
                    }
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Log.FormLog("PostCourseForm streamWriter 3 END");
                    Log.FormLog(result);
                    ProcessRCModel res = result.ToClass<ProcessRCModel>();
                    return res;
                }
            }
            catch (WebException wex)
            {
                var pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();
                pageContent.LogError();
                throw wex.LogError();
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static ProcessRCModel approveAndContinueProcess(ProcessModel model, string calculatedIdNumber)
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}{2}{3}{4}{5}{6}", url, "approveAndContinueProcess?municipality=", model.RASHUT, "&process=", model.TM_TAHALICH_ID, "&step=", model.CURR_MISPAR_SHALAV));
                    var str = Encoding.UTF8.GetString(contact);
                    ProcessRCModel res = str.ToClass<ProcessRCModel>();
                    return res;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static ProcessRCModel completeAProcess(ProcessModel model, string calculatedIdNumber)
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}{2}{3}{4}{5}{6}", url, "completeAProcess?municipality=", model.RASHUT, "&process=", model.TM_TAHALICH_ID, "&step=", model.CURR_MISPAR_SHALAV));
                    var str = Encoding.UTF8.GetString(contact);
                    ProcessRCModel res = str.ToClass<ProcessRCModel>();
                    return res;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static ProcessRCModel setProcessStatusToCanceled(ProcessModel model, string calculatedIdNumber)
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}{2}{3}{4}{5}{6}", url, "setProcessStatusToCanceled?municipality=", model.RASHUT, "&process=", model.TM_TAHALICH_ID, "&step=", model.CURR_MISPAR_SHALAV));
                    var str = Encoding.UTF8.GetString(contact);
                    ProcessRCModel res = str.ToClass<ProcessRCModel>();
                    return res;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static academicWrraper GetAcademicInstFromWS()
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData("https://data.gov.il/api/action/datastore_search?resource_id=b2970446-98e0-483a-86c1-7ec00170b386");
                    var str = Encoding.UTF8.GetString(contact);
                    academicWrraper academicInsts = str.ToClass<academicWrraper>();

                    return academicInsts;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static int getRashutNumber(int municipality)
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}{2}", url, "getRashutByHanalatHeshbonot?municipality=", municipality));
                    var str = Encoding.UTF8.GetString(contact);
                    int rashut = str.ToClass<int>();
                    return rashut;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static string fireDinamicGroupsProcedures(List<int> municipalities)
        {
            municipalities = new List<int> { 14000, 39500, 62999 };

            using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
            {
                initializ();
                try
                {
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}", url, "fireStoredProceduresForDinamicGroups"));
                    httpWebRequest.ContentType = "application/json; charset=utf-8";
                    httpWebRequest.Method = "POST";
                    httpWebRequest.Timeout = 180000;

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        try
                        {
                            Log.FormLog("fireStoredproceduresForDinamicGroups streamWriter 1");
                            string json = municipalities.ToJSON();
                            streamWriter.Write(json);
                            streamWriter.Flush();
                            streamWriter.Close();
                            Log.FormLog("fireStoredproceduresForDinamicGroups streamWriter 2");
                        }
                        catch (Exception Iex)
                        {
                            throw Iex.LogError();
                        }
                    }
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        Log.FormLog("fireStoredproceduresForDinamicGroups streamWriter 3 END");
                        Log.FormLog(result);

                        //result
                        var ress = JsonConvert.DeserializeObject(result);

                        return ress.ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public static EmployeeDada GetPersonalInformation(string calculatedIdNumber, int rashutId)
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}?RASHUT={2}&ZEHUT={3}", url, "GetWorkerData", rashutId, calculatedIdNumber));
                    var str = Encoding.UTF8.GetString(contact);
                    return str.ToClass<EmployeeDada>();
                }
            }
            catch (Exception ex)
            {
                ex.LogError(new { _calculatedIdNumber = calculatedIdNumber, _rashutId = rashutId, _url = string.Format("{0}{1}?RASHUT={2}&ZEHUT={3}", url, "GetWorkerData", rashutId, calculatedIdNumber) });
                return null;
            }
        }
        public static string GetPayCheckAttachments(DateTime requestDate)
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}?requestDate={2}{3}{4}", url, "GetPayCheckAttachment", requestDate.Year, requestDate.Month > 9? "":"0", requestDate.Month));
                    var str = Encoding.UTF8.GetString(contact);
                    Log.TestLog(str, "GetPayCheckAttachments");
                    return str;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }

        #region private
        private static List<string[]> parseCsv(string csv)
        {
            List<string[]> csvTable = new List<string[]>();
            string[] values;
            List<string> rows = Regex.Split(csv, "\\n").ToList();
            foreach (string row in rows)
            {
                values = Regex.Split(row, ",");
                csvTable.Add(values);
            }
            return csvTable;
        }
        private static List<SystemUserModel> distinctTheList(List<SystemUserModel> SystemUsers)
        {
            List<SystemUserModel> newlist = new List<SystemUserModel>();
            newlist = SystemUsers.GroupBy(g => new { g.idNumber, g.municipalityId }).Select(p => p.First()).ToList();

            return newlist;
        }
        private static int getResponseString(string _url, bool inError = false)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = new TimeSpan(1, 15, 15);
                    var result = client.GetAsync(_url).Result;
                    result.EnsureSuccessStatusCode();

                    string stringResult = result.Content.ReadAsStringAsync().Result;
                    GetWorkersResponse workersResponse = new GetWorkersResponse();
                    try
                    {
                        workersResponse = stringResult.ToClass<GetWorkersResponse>();
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                    Log.LogLoad(new { stringResult = stringResult }, "getResponseString");

                    return workersResponse.rowIndex;
                }
            }
            catch (Exception ex)
            {
                ex.LogError(_url);
                if (!inError)
                {
                    return getResponseString(_url, true);
                }
                else
                {
                    return 0;
                }
            }
        }
        #endregion
    }
}
