﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServiceProvider.DataMappers
{
    public class SystemSchema
    {
        public string schema_name { get; set; }
        public string customer_number { get; set; }
        public string customer_name_heb { get; set; }
        public string mifal { get; set; }
        public string shem_mifal { get; set; }
        public string dist { get; set; }
        public string rashut_mancol { get; set; }
        public string rashut_sachar { get; set; }
    }
}
