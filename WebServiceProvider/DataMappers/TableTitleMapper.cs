﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServiceProvider.DataMappers
{
    public static class TableTitleMapper
    {
        public static List<Title> ListTitles(this ICollection<ResponseSalaryFigures> list)
        {
            List<Title> tl = new List<Title>();
            if (list == null || list.Count == 0)
                return tl;

            foreach (ICollection<ResponseSalaryFigures> item in list.GroupBy(l => l.columnTitle))
            {
                ResponseSalaryFigures rsf = item.First();
                if(rsf.columnNumber >= 0 && !string.IsNullOrWhiteSpace(rsf.columnTitle))
                    tl.Add(new Title(rsf.columnNumber, rsf.columnTitle));
            }

            return tl.Distinct().ToList();
        }
        public static List<Title> ListTitles(this ICollection<ResponseWorkAbsence> list, bool addMonth = false)
        {
            List<Title> tl = new List<Title>();
            if (list == null || list.Count == 0)
                return tl;

            foreach (ICollection<ResponseWorkAbsence> item in list.GroupBy(l => l.columnTitle))
            {
                ResponseWorkAbsence rsf = item.First();
                if (rsf.columnNumber >= 0 && !string.IsNullOrWhiteSpace(rsf.columnTitle))
                    tl.Add(new Title(rsf.columnNumber, rsf.columnTitle));
            }
            if (addMonth) tl.Add(new Title(0, "חודש"));

            return tl.Distinct().ToList().OrderBy(t=> t.number).ToList();
        }
    }
}
