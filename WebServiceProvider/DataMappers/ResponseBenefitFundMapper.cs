﻿using DataAccess.Extensions;
using DataAccess.Models;
using System;
using System.Globalization;
using WebServiceProvider.PortalWS;

namespace WebServiceProvider.DataMappers
{
    public static class ResponseBenefitFundMapper
    {
        public static ResponseBenefitFund Map(this Kai006n1ResponseNetuneFunds model)
        {
            if (model == null)
                return null;

            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;//

            return new ResponseBenefitFund
            {
                fundBrutoGemel = model.Res_kg_bruto_gemel.ToString("N", nfi),
                fundName = model.Res_Fund_Name.StringTrim(),
                fundNumber = model.Res_Fund_Number,
                provisionsAmount = model.Res_Hafrasha_Amount.ToString("N", nfi),
                provisionsPercentage = model.Res_Hafrasha_Percentage,
                month = model.Res_month,
                deductionAmount = model.Res_Nikui_Amount.ToString("N", nfi),
                deductionPercentage = model.Res_Nikui_Percentage,
                disabilityInsuranceAmount = model.Res_Ovdan_Amount.ToString("N", nfi),
                disabilityInsurancePercentage = model.Res_Ovdan_Percentage,
                severanceAmount = model.Res_Pizui_Amount.ToString("N", nfi),
                severancePercentage = model.Res_Pizui_Percentage,
                monthTotal = Math.Round(model.Res_Nikui_Amount + model.Res_Ovdan_Amount + model.Res_Hafrasha_Amount + model.Res_Pizui_Amount, 2).ToString("N", nfi),
                monthName = model.Res_month > 0 && model.Res_month < 13? CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(model.Res_month) : string.Empty,
                order = model.Res_month.ToInt()
            };
        }
    }
}
  
 
 
  
  
 
  
  
  
  
  
  
