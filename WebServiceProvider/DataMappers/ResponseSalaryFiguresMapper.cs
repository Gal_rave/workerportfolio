﻿using DataAccess.Extensions;
using DataAccess.Models;
using System;
using System.Globalization;
using WebServiceProvider.PortalWS;

namespace WebServiceProvider.DataMappers
{
    public static class ResponseSalaryFiguresMapper
    {
        public static ResponseSalaryFigures Map(this Kai003n0ResponseNetuneSachar mode, CultureInfo currentculture)
        {
            if (mode == null)
                return null;

            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;

            return new ResponseSalaryFigures
            {
                columnNumber = mode.NNumber,
                columnTitle = mode.NTeur.StringTrim(),
                month = mode.Month,
                value = mode.Value.ToString("N", nfi),
                monthName = mode.Month > 0 && mode.Month < 12 ? currentculture.DateTimeFormat.GetMonthName(mode.Month) : string.Empty,
            };
        }
        public static ResponseSalaryFigures Map(this Kai004n0ResponseSacharSmalim mode, CultureInfo currentculture)
        {
            if (mode == null)
                return null;

            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;

            return new ResponseSalaryFigures
            {
                columnNumber = mode.NNumber,
                columnTitle = mode.NTeur.StringTrim(),
                month = mode.Month,
                value = mode.Value.ToString("N", nfi),
                monthName = mode.Month > 0 && mode.Month < 12? currentculture.DateTimeFormat.GetMonthName(mode.Month) : string.Empty,
            };
        }
    }
}
