﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataAccess.Extensions;

namespace WebServiceProvider.DataMappers
{
    public static class IntToDateMapper
    {
        public static DateTime ToDate(this int date)
        {
            ///1010000 => is lowest int to convert into date
            string str = date.ToString();
            if (date < 1010000 || str.Length < 7)
                return new DateTime();

            DateTime now;
            int length = str.Length;
            string year = str.Substring(length - 4, 4);
            string month = str.Substring(length - 6, 2);
            string day = str.Substring(0, length - 6);
            now = new DateTime(year.ToInt(), month.ToInt(), day.ToInt());

            return now;
        }
        public static DateTime ToDate(this string date)
        {
            if(string.IsNullOrWhiteSpace(date) || date.Length < 7 || date.ToInt() < 1010000)
                return new DateTime();

            DateTime now;
            int length = date.Length;
            string year = date.Substring(length - 4, 4);
            string month = date.Substring(length - 6, 2);
            string day = date.Substring(0, length - 6);
            now = new DateTime(year.ToInt(), month.ToInt(), day.ToInt());

            return now;
        }
    }
}
