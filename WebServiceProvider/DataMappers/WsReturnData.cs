﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServiceProvider.DataMappers
{
    public class WsReturnData
    {
        public int rowIndex { get; set; }
        public bool isDev { get; set; }
        public bool success { get; set; }
        public string OvedId { get; set; }
    }
}