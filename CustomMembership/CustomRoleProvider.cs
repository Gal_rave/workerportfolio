﻿using System;
using System.Linq;
using System.Configuration;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Web.Configuration;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Collections.Generic;

using DataAccess.Models;
using DataAccess.Extensions;
using DataAccess.BLLs;
using DataAccess.Mappers;
using CustomMembership.Classes;
using Logger;
using System.Web;

namespace CustomMembership
{
    public static class CustomRoleProvider
    {
        #region Class Variables
        private static bool isInitialized = false;
        private static UserGroupsBll groupsBll;
        #endregion

        #region Properties 
        public static bool IsInitialized
        {
            get
            {
                return isInitialized;
            }
        }
        #endregion
        public static void Initialize()
        {
            if (IsInitialized)
                return;

            groupsBll = new UserGroupsBll();

            isInitialized = true;
        }

        #region groups function
        private static List<int> GetGroupIdsFromNames(string[] GroupsNames)
        {
            return groupsBll.GetGroupIdsFromNames(GroupsNames).Select(g => g.ID.ToInt()).ToList();
        }
        public static string[] GetAllGroups()
        {
            Initialize();

            return groupsBll.GetAllGroups().Select(g => g.name).ToArray();
        }
        public static List<GroupModel> GetGroups()
        {
            Initialize();

            return groupsBll.GetAllGroups();
        }
        public static bool GroupExists(string GroupName)
        {
            Initialize();

            return groupsBll.GetGroup(GroupName) != null;
        }
        #endregion

        #region create / delete group
        public static bool CreateGroup(string name, int immunity = 10)
        {
            Initialize();

            return groupsBll.CreateGroup(name, immunity);
        }
        public static bool DeleteGroup(string name)
        {
            Initialize();

            return groupsBll.DeleteGroup(name);
        }
        #endregion

        #region user groups
        public static List<UserModel> FindUsersInGroup(string groupName, string idNumber = null, string email = null)
        {
            Initialize();

            IQueryable<USER> users = groupsBll.GetUsersInGroup(groupName);
            if (!string.IsNullOrWhiteSpace(idNumber))
                users = users.Where(u=> u.IDNUMBER == idNumber || u.IDNUMBER.StartsWith(idNumber) || u.IDNUMBER.EndsWith(idNumber) || u.IDNUMBER.Contains(idNumber));
            if (!string.IsNullOrWhiteSpace(email))
                users = users.Where(u => u.EMAIL == email || u.EMAIL.StartsWith(email) || u.EMAIL.EndsWith(email) || u.EMAIL.Contains(email));

            return users.Select(u => u.Map(false)).ToList();
        }
        public static List<GroupModel> GetGroupsForUser(string idNumber)
        {
            Initialize();

            return groupsBll.GetGroupsForUser(idNumber);
        }
        public static List<GroupModel> GetGroupsForUser(int userId)
        {
            Initialize();

            return groupsBll.GetGroupsForUser(userId);
        }
        public static bool IsUserInGroup(string idNumber, string groupName)
        {
            Initialize();

            return groupsBll.GetUsersInGroup(groupName).FirstOrDefault(u=> u.IDNUMBER == idNumber) != null;
        }
        public static bool IsUserInGroup(int userId, string groupName)
        {
            Initialize();

            return groupsBll.GetUsersInGroup(groupName).FirstOrDefault(u => u.ID == userId) != null;
        }
        public static bool HasPermission(string idNumber, string groupName)
        {
            Initialize();
            try
            {
                List<GroupModel> usesrsGroups = groupsBll.GetGroupsForUser(idNumber);
                GROUP target = groupsBll.GetGroup(groupName);
                if (usesrsGroups == null || usesrsGroups.Count == 0 || target == null)
                    return false;
                foreach (GroupModel ug in usesrsGroups)
                {
                    if (ug.name == groupName || ug.immunity > target.IMMUNITY)
                        return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return ex.LogError(false);
            }
        }
        public static bool HasSpecificPermission(string idNumber, string groupName)
        {
            if (string.IsNullOrWhiteSpace(idNumber) || string.IsNullOrWhiteSpace(groupName))
                return false;

            Initialize();
            try
            {
                List<GroupModel> usesrsGroups = groupsBll.GetGroupsForUser(idNumber);
                if (usesrsGroups == null || usesrsGroups.Count == 0)
                    return false;
                foreach (GroupModel ug in usesrsGroups)
                {
                    if (ug.name.ToLower() == groupName.ToLower() || ug.immunity > int.Parse(HttpContext.Current.Application.Get("MinAdminImmunity").ToString()))
                        return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return ex.LogError(false);
            }
        }
        public static int GetUserMaxImmunity(string idNumber)
        {
            Initialize();

            try
            {
                return groupsBll.GetMaxGroupImmunityForUser(idNumber);
            }
            catch (Exception ex)
            {
                return ex.LogError(0); ;
            }
        }
        public static bool HasPermission(string groupName)
        {
            Initialize();

            try
            {
                List<GroupModel> usesrsGroups = groupsBll.GetGroupsForUser(WebSecurity.CurrentUserName);
                GROUP target = groupsBll.GetGroup(groupName);
                if (usesrsGroups == null || usesrsGroups.Count == 0 || target == null)
                    return false;
                foreach (GroupModel ug in usesrsGroups)
                {
                    if (ug.name == groupName || ug.immunity > target.IMMUNITY)
                        return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return ex.LogError(false); ;
            }
        }
        #endregion
        #region add/remove user Group
        public static void AddGroupToUser(string idNumber, string groupName)
        {
            Initialize();

            groupsBll.AddUserToGroup(idNumber, groupName);
        }
        public static void RemoveUserFromGroup(string idNumber, string groupName)
        {
            Initialize();

            groupsBll.RemoveUserFromGroup(idNumber, groupName);
        }
        #endregion
    }
}
