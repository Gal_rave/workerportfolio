﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomMembership.Classes
{
    public class CustomPrincipalSerializeModel
    {
        public int Id { get; set; }
        public string idNUmber { get; set; }
        public string email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsLockedOut { get; set; }
        public int LockedOutTime { get; set; }
        public bool IsConfirmed { get; set; }
        public int FaildLoginCount { get; set; }
        public int currentMunicipalityId { get; set; }
        public int calculatedIdNumber { get; set; }
        public int currentFactoryId { get; set; }
    }
}
