﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace CustomMembership.Classes
{
    interface ICustomPrincipal : IPrincipal
    {
        int Id { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }        
        string idNUmber { get; set; }
        string email { get; set; }
        bool IsLockedOut { get; set; }
        int LockedOutTime { get; set; }
        bool IsConfirmed { get; set; }
        int FaildLoginCount { get; set; }
        int currentMunicipalityId { get; set; }
        int calculatedIdNumber { get; set; }
        int currentFactoryId { get; set; }
    }
}
