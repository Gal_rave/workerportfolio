﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace CustomMembership.Classes
{
    public class CustomPrincipal : ICustomPrincipal
    {
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role) { return false; }

        public CustomPrincipal(string idNmber)
        {
            this.Identity = new GenericIdentity(idNmber);
        }
        public CustomPrincipal(string idNmber, CustomPrincipalSerializeModel serializeModel)
        {
            this.Identity = new GenericIdentity(idNmber);
            this.Id = serializeModel.Id;
            this.idNUmber = serializeModel.idNUmber;
            this.email = serializeModel.email;
            this.FirstName = serializeModel.FirstName;
            this.LastName = serializeModel.LastName;
            this.IsLockedOut = serializeModel.IsLockedOut;
            this.LockedOutTime = serializeModel.LockedOutTime;
            this.IsConfirmed = serializeModel.IsConfirmed;
            this.FaildLoginCount = serializeModel.FaildLoginCount;
            this.currentMunicipalityId = serializeModel.currentMunicipalityId;
            this.calculatedIdNumber = serializeModel.calculatedIdNumber;
            this.currentFactoryId = serializeModel.currentFactoryId;
        }

        public int Id { get; set; }
        public string idNUmber { get; set; }
        public string email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsLockedOut { get; set; }
        public int LockedOutTime { get; set; }
        public bool IsConfirmed { get; set; }
        public int FaildLoginCount { get; set; }
        public int currentMunicipalityId { get; set; }
        public int calculatedIdNumber { get; set; }
        public int currentFactoryId { get; set; }
    }
}
