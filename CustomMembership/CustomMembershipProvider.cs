﻿using System;
using System.Linq;
using System.Configuration;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Web.Configuration;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Collections.Generic;

using DataAccess.Models;
using DataAccess.Extensions;
using DataAccess.BLLs;
using DataAccess.Mappers;
using CustomMembership.Classes;
using Logger;
using AsyncHelpers;

namespace CustomMembership
{
    /// <summary>
    /// MembershipException - 12;
    /// </summary>
    public static class CustomMembershipProvider
    {
        #region Class Variables
        private static bool isInitialized = false;
        private static string applicationName;
        private static string defaultSiteEmail;
        private static bool enablePasswordReset;
        private static bool enablePasswordRetrieval;
        private static bool requiresQuestionAndAnswer;
        private static bool requiresUniqueEmail;
        private static int maxInvalidPasswordAttempts;
        private static int passwordAttemptWindow;
        private static MembershipPasswordFormat passwordFormat;
        private static int minRequiredNonAlphanumericCharacters;
        private static int minRequiredNumericCharacters;
        private static int minRequiredPasswordLength;
        private static MembershipProviderBll membershipBll;
        private static RegisterBll registerBll;
        private static UserBll userBll;
        private static UserGroupsBll groupsBll;
        private static string providerName;
        private static int verificationTokenExpirationTime;
        private static int userLockInterval;
        private static int passwordExpiresMaxDays;
        private static int smsCodeExpirationMinutes;
        #endregion Class Variables

        #region Properties        
        public static string ApplicationName
        {
            get
            {
                return applicationName;
            }

            set
            {
                applicationName = value;
            }
        }
        public static string ProviderName
        {
            get
            {
                return providerName;
            }
        }
        public static bool IsInitialized
        {
            get
            {
                return isInitialized;
            }
        }
        public static bool EnablePasswordReset
        {
            get
            {
                return enablePasswordReset;
            }
        }
        public static bool RequiresQuestionAndAnswer
        {
            get
            {
                return requiresQuestionAndAnswer;
            }
        }
        public static bool RequiresUniqueEmail
        {
            get
            {
                return requiresUniqueEmail;
            }
        }
        public static bool EnablePasswordRetrieval
        {
            get
            {
                return enablePasswordRetrieval;
            }
        }
        public static int MaxInvalidPasswordAttempts
        {
            get
            {
                return maxInvalidPasswordAttempts;
            }
        }
        public static int VerificationTokenExpirationTime
        {
            get
            {
                return verificationTokenExpirationTime;
            }
        }
        public static int SmsCodeExpirationMinutes
        {
            get
            {
                return smsCodeExpirationMinutes;
            }
        }
        public static int MinRequiredNonAlphanumericCharacters
        {
            get
            {
                return minRequiredNonAlphanumericCharacters;
            }
        }
        public static int MinRequiredNumericCharacters
        {
            get
            {
                return minRequiredNumericCharacters;
            }
        }
        public static int MinRequiredPasswordLength
        {
            get
            {
                return minRequiredPasswordLength;
            }
        }
        public static int PasswordAttemptWindow
        {
            get
            {
                return passwordAttemptWindow;
            }
        }
        public static int UserLockInterval
        {
            get
            {
                return userLockInterval;
            }
        }
        public static int PasswordExpiresMaxDays
        {
            get
            {
                return passwordExpiresMaxDays;
            }
        }
        public static MembershipPasswordFormat PasswordFormat
        {
            get
            {
                return passwordFormat;
            }
        }
        public static string DefaultSiteEmail
        {
            get
            {
                return defaultSiteEmail;
            }
        }
        #endregion Properties

        #region MembershipProvider
        public static void Initialize(string name, NameValueCollection config = null)
        {
            if (config == null)
            {
                string configPath = "~/web.config";
                Configuration NexConfig = WebConfigurationManager.OpenWebConfiguration(configPath);
                MembershipSection section = (MembershipSection)NexConfig.GetSection("system.web/membership");
                ProviderSettingsCollection settings = section.Providers;
                NameValueCollection membershipParams = settings[section.DefaultProvider].Parameters;
                config = membershipParams;
            }

            if (name == null || name.Length == 0)
            {
                name = "WebSecurity";
            }

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Custom Membership Provider");
            }

            applicationName = GetConfigValue(config["applicationName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            maxInvalidPasswordAttempts = (GetConfigValue(config["maxInvalidPasswordAttempts"], "5")).ToInt();
            passwordAttemptWindow = (GetConfigValue(config["passwordAttemptWindow"], "10")).ToInt();
            minRequiredNonAlphanumericCharacters = (GetConfigValue(config["minRequiredNonAlphanumericCharacters"], "1")).ToInt();
            minRequiredPasswordLength = (GetConfigValue(config["minRequiredPasswordLength"], "6")).ToInt();
            enablePasswordReset = (GetConfigValue(config["enablePasswordReset"], "true")).ToBool();
            enablePasswordRetrieval = (GetConfigValue(config["enablePasswordRetrieval"], "true")).ToBool();
            requiresQuestionAndAnswer = (GetConfigValue(config["requiresQuestionAndAnswer"], "false")).ToBool();
            requiresUniqueEmail = (GetConfigValue(config["requiresUniqueEmail"], "true")).ToBool();
            minRequiredNumericCharacters = (GetConfigValue(config["minRequiredNumericCharacters"], "0")).ToInt();
            providerName = GetConfigValue(config["name"], "CustomMembershipProvider");
            verificationTokenExpirationTime = (GetConfigValue(config["verificationTokenExpirationTime"], "24")).ToInt();
            smsCodeExpirationMinutes = (GetConfigValue(config["smsCodeExpirationMinutes"], "30")).ToInt();
            userLockInterval = (GetConfigValue(config["userLockInterval"], "30")).ToInt();
            passwordExpiresMaxDays = (GetConfigValue(config["passwordExpiresMaxDays"], "180")).ToInt();
            defaultSiteEmail = GetConfigValue(ConfigurationManager.AppSettings["smtpDefaultEmail"], "tikoved@iula.org.il");

            string temp_format = config["passwordFormat"];
            if (temp_format == null)
            {
                temp_format = "Hashed";
            }

            switch (temp_format)
            {
                case "Hashed":
                    passwordFormat = MembershipPasswordFormat.Hashed;
                    break;
                case "Encrypted":
                    passwordFormat = MembershipPasswordFormat.Encrypted;
                    break;
                case "Clear":
                    passwordFormat = MembershipPasswordFormat.Clear;
                    break;
                default:
                    throw new ProviderException("Password format not supported.");
            }

            membershipBll = new MembershipProviderBll();
            userBll = new UserBll();
            groupsBll = new UserGroupsBll();
            registerBll = new RegisterBll();

            isInitialized = true;
        }

        #region CreateUser
        public static WebSecurityModel CreateUser(string idNumber, string password, string email, string firstname, string lastname, string phone, string cell, DateTime? birthDate, DateTime? immigrationDate, bool requireConfirmationToken)
        {
            WebSecurityModel model = new WebSecurityModel();
            ///check for uniq user
            try
            {
                UserModel user = membershipBll.GetUser(idNumber, email);

                if (membershipBll.IsDuplicateIdNumber(idNumber))
                    throw new MembershipException("DuplicateIdNumber", 2);
                if (membershipBll.GetUserByEmail(email) != null)
                    throw new MembershipException("DuplicateEmail", 3);

                if (!validatingPassword(password))
                {
                    throw new MembershipException("InvalidPassword", 4);
                }
                if (!validateEmail(email))
                {
                    throw new MembershipException("InvalidEmail", 5);
                }
                if (!IdNumberExtender.validateIDNumber(idNumber))
                {
                    throw new MembershipException("InvalidIDNumber", 6);
                }

                if (string.IsNullOrWhiteSpace(user.idNumber) && !string.IsNullOrWhiteSpace(user.calculatedIdNumber))
                    user.idNumber = IdNumberExtender.GenerateFullIdNumber(user.calculatedIdNumber);

                if (string.IsNullOrWhiteSpace(user.calculatedIdNumber) && !string.IsNullOrWhiteSpace(user.idNumber))
                    user.calculatedIdNumber = IdNumberExtender.GenerateCalculatedIdNumber(user.idNumber);

                DateTime now = DateTime.Now;
                ///create new user
                user = new UserModel
                {
                    birthDate = birthDate,
                    cell = cell,
                    createdDate = now,
                    email = email,
                    firstName = firstname,
                    idNumber = idNumber,
                    immigrationDate = immigrationDate,
                    lastName = lastname,
                    phone = phone
                };
                PasswordHashModel passHash = HashProvider.Hash(password);
                user.membership = new MembershipModel
                {
                    confirmationToken = FixString(HashProvider.Salt(64).ByteArrayToString()),
                    createdDate = now,
                    isConfirmed = !requireConfirmationToken,
                    lastPasswordFailureDate = now,
                    password = passHash.password,
                    passwordChangeDdate = now,
                    PasswordFailureSCounter = 0,
                    passwordSalt = passHash.passwordSalt,
                    passwordVerificationToken = FixString(HashProvider.Salt(64).ByteArrayToString()),
                    verificationTokenExpiration = now.AddHours(verificationTokenExpirationTime),
                    passwordFormat = PasswordFormat.ToString()
                };
                ///add group if user dont need requireConfirmationToken
                if (!requireConfirmationToken)
                    groupsBll.AddUserToGroup(user.ID, 1);

                model.updateByUser(membershipBll.CreateUser(user));
            }
            catch (MembershipException ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            return model;
        }
        public static WebSecurityModel CreateUser(UserModel user, bool requireConfirmationToken = false)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                if (!validatingPassword(user.password))
                {
                    throw new MembershipException("InvalidPassword", 4);
                }
                if (!validateEmail(user.email))
                {
                    throw new MembershipException("InvalidEmail", 5);
                }

                if (string.IsNullOrWhiteSpace(user.idNumber) && !string.IsNullOrWhiteSpace(user.calculatedIdNumber))
                    user.idNumber = IdNumberExtender.GenerateFullIdNumber(user.calculatedIdNumber);

                if (string.IsNullOrWhiteSpace(user.calculatedIdNumber) && !string.IsNullOrWhiteSpace(user.idNumber))
                    user.calculatedIdNumber = IdNumberExtender.GenerateCalculatedIdNumber(user.idNumber);

                if (!IdNumberExtender.validateIDNumber(user.idNumber))
                {
                    throw new MembershipException("InvalidIDNumber", 6);
                }

                if (membershipBll.IsDuplicateIdNumber(user.idNumber))
                    throw new MembershipException("DuplicateIdNumber", 2);
                if (membershipBll.GetUserByEmail(user.email) != null)
                    throw new MembershipException("DuplicateEmail", 3);

                DateTime now = DateTime.Now;
                PasswordHashModel passHash = HashProvider.Hash(user.password);
                user.membership = new MembershipModel
                {
                    confirmationToken = FixString(HashProvider.Salt(64).ByteArrayToString()),
                    createdDate = now,
                    isConfirmed = !requireConfirmationToken,
                    lastPasswordFailureDate = now,
                    password = passHash.password,
                    passwordChangeDdate = now,
                    PasswordFailureSCounter = 0,
                    passwordSalt = passHash.passwordSalt,
                    passwordVerificationToken = FixString(HashProvider.Salt(64).ByteArrayToString()),
                    verificationTokenExpiration = now.AddHours(verificationTokenExpirationTime),
                    passwordFormat = PasswordFormat.ToString()
                };
                ///add group if user dont need requireConfirmationToken
                if (!requireConfirmationToken)
                    groupsBll.AddUserToGroup(user.ID, 1);

                model.updateByUser(membershipBll.CreateUser(user));
            }
            catch (MembershipException ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            return model;
        }
        public static WebSecurityModel CreateUser(CreateUserModel model, bool requireConfirmationToken = false)
        {
            WebSecurityModel securitymodel = new WebSecurityModel();

            UserModel usermodel = model.Map();
            securitymodel = CreateUser(usermodel, requireConfirmationToken);
            if (securitymodel.success && securitymodel.exceptionId == 0)
            {
                try
                {
                    StreetBll bll = new StreetBll();
                    ///add connection to municipality
                    if (model.municipality != null && model.municipality.ID > 0)
                    {
                        bll.AddMunicipalityToUser(model.municipality, securitymodel.user);
                    }
                    else
                    {
                        bll.AddMunicipalityToUser(model.municipalityName, securitymodel.user);
                    }
                }
                catch (MembershipException ex)
                {
                    securitymodel = new WebSecurityModel(ex.LogError());
                }
                catch (Exception ex)
                {
                    securitymodel = new WebSecurityModel(ex.LogError());
                }
            }

            return securitymodel;
        }
        public static WebSecurityModel CreateUserFromSystem(UserModel user, bool requireConfirmationToken = false)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                if (!validatingPassword(user.password))
                {
                    throw new MembershipException("InvalidPassword", 4);
                }
                if (!string.IsNullOrWhiteSpace(user.email) && !validateEmail(user.email))
                {
                    throw new MembershipException("InvalidEmail", 5);
                }

                if (string.IsNullOrWhiteSpace(user.idNumber) && !string.IsNullOrWhiteSpace(user.calculatedIdNumber))
                    user.idNumber = IdNumberExtender.GenerateFullIdNumber(user.calculatedIdNumber);

                if (string.IsNullOrWhiteSpace(user.calculatedIdNumber) && !string.IsNullOrWhiteSpace(user.idNumber))
                    user.calculatedIdNumber = IdNumberExtender.GenerateCalculatedIdNumber(user.idNumber);

                if (!IdNumberExtender.validateIDNumber(user.idNumber))
                {
                    throw new MembershipException("InvalidIDNumber", 6);
                }

                if (!membershipBll.IsDuplicateIdNumber(user.idNumber))
                    throw new MembershipException("DuplicateIdNumber", 2);
                if (!string.IsNullOrWhiteSpace(user.email) && membershipBll.GetUserByEmail(user.email) != null)
                    throw new MembershipException("DuplicateEmail", 3);

                DateTime now = DateTime.Now;
                PasswordHashModel passHash = HashProvider.Hash(user.password);
                user.membership = new MembershipModel
                {
                    confirmationToken = FixString(HashProvider.Salt(64).ByteArrayToString()),
                    createdDate = now,
                    isConfirmed = !requireConfirmationToken,
                    lastPasswordFailureDate = now,
                    password = passHash.password,
                    passwordChangeDdate = now,
                    PasswordFailureSCounter = 0,
                    passwordSalt = passHash.passwordSalt,
                    passwordVerificationToken = FixString(HashProvider.Salt(64).ByteArrayToString()),
                    verificationTokenExpiration = now.AddHours(verificationTokenExpirationTime),
                    passwordFormat = PasswordFormat.ToString()
                };
                ///create user
                user = membershipBll.CreateUser(user);
                ///add municipalities to user
                registerBll.InsertMunicipalitiesToUser(user.idNumber);
                model.updateByUser(user);
                groupsBll.AddUserToGroup(user.ID, 1);
            }
            catch (MembershipException ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            return model;
        }
        /// <summary>
        /// for bulk user register only
        /// NO VALIDATION
        /// </summary>
        /// <param name="user"></param>
        /// <param name="requireConfirmationToken"></param>
        /// <returns></returns>
        public static WebSecurityModel InternalCreateUserFromSystem(UserModel user)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                if (string.IsNullOrWhiteSpace(user.idNumber) && !string.IsNullOrWhiteSpace(user.calculatedIdNumber))
                    user.idNumber = IdNumberExtender.GenerateFullIdNumber(user.calculatedIdNumber);

                if (string.IsNullOrWhiteSpace(user.calculatedIdNumber) && !string.IsNullOrWhiteSpace(user.idNumber))
                    user.calculatedIdNumber = IdNumberExtender.GenerateCalculatedIdNumber(user.idNumber);

                if (!membershipBll.IsDuplicateIdNumber(user.idNumber))
                    throw new MembershipException("DuplicateIdNumber", 2);

                DateTime now = DateTime.Now;
                PasswordHashModel passHash = HashProvider.Hash(user.password);
                user.membership = new MembershipModel
                {
                    confirmationToken = FixString(HashProvider.Salt(64).ByteArrayToString()),
                    createdDate = now,
                    isConfirmed = true,
                    lastPasswordFailureDate = now,
                    password = passHash.password,
                    passwordChangeDdate = now,
                    PasswordFailureSCounter = 0,
                    passwordSalt = passHash.passwordSalt,
                    passwordVerificationToken = FixString(HashProvider.Salt(64).ByteArrayToString()),
                    verificationTokenExpiration = now.AddHours(verificationTokenExpirationTime),
                    passwordFormat = PasswordFormat.ToString()
                };
                ///create user
                user = membershipBll.CreateUser(user);
                ///add municipalities to user
                registerBll.InsertMunicipalitiesToUser(user.idNumber);
                groupsBll.AddUserToGroup(user.ID, 1);
            }
            catch (MembershipException ex)
            {
                ex.LogError(user);
                model = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            return model;
        }
        #endregion

        #region Change Password
        public static WebSecurityModel ChangePassword(string idNumber, string oldPassword, string newPassword)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                model = ValidateUser(idNumber, oldPassword);
                if (!model.success)
                    throw new MembershipException("InvalidPassworException", 1);

                PasswordHashModel passHash = HashProvider.Hash(newPassword);

                model.success = membershipBll.UpdatePassword(model.user.membership, passHash, PasswordFormat.ToString());
            }
            catch (MembershipException ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            return model;
        }
        public static WebSecurityModel ResetPassword(UserSearchModel model)
        {
            WebSecurityModel wsm = new WebSecurityModel();
            try
            {
                ///if developer reset
                if (string.IsNullOrWhiteSpace(model.searchIdNumber) || string.IsNullOrWhiteSpace(model.userToken))
                {
                    ///check developer 
                    if (CustomRoleProvider.HasPermission(model.idNumber, "DEVELOPER"))
                    {
                        PasswordHashModel passHash = HashProvider.Hash(model.password);
                        wsm.success = membershipBll.ResetPassword(model, passHash, PasswordFormat.ToString());
                        return wsm;
                    }
                    throw new AccessViolationException("AccessViolationException");
                }

                wsm = ResetPasswordCode(model.searchIdNumber, model.userToken, model.password);
                return wsm;
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static WebSecurityModel ResetPassword(string idNumber, int tokenExpirationInMinutesFromNow = 0)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                model.user = membershipBll.GetMembershipAndUserByIdNumber(idNumber);
                MembershipModel membershipmodel = model.user.membership;
                if (membershipmodel.verificationTokenExpiration.HasValue && membershipmodel.verificationTokenExpiration.Value > DateTime.Now)
                {
                    model.passwordVerificationToken = membershipmodel.passwordVerificationToken;
                }
                else
                {
                    membershipmodel.passwordVerificationToken = FixString(HashProvider.Salt(64).ByteArrayToString());
                    membershipmodel.verificationTokenExpiration = DateTime.Now.AddHours(verificationTokenExpirationTime + tokenExpirationInMinutesFromNow);
                    membershipBll.UpdatePassword(membershipmodel);
                    model.passwordVerificationToken = membershipmodel.passwordVerificationToken;
                }
            }
            catch (MembershipException ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            return model;
        }
        public static WebSecurityModel GeneratePasswordResetToken(string idNumber, int tokenExpirationInMinutesFromNow = 0)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                DateTime NOW = DateTime.Now.AddHours(verificationTokenExpirationTime + tokenExpirationInMinutesFromNow);

                MembershipModel membershipmodel = membershipBll.GetMembershipByIdNumber(idNumber);
                if (membershipmodel.verificationTokenExpiration.HasValue && membershipmodel.verificationTokenExpiration.Value > NOW)
                {
                    model.passwordVerificationToken = membershipmodel.passwordVerificationToken;
                }
                else
                {
                    membershipmodel.passwordVerificationToken = FixString(HashProvider.Salt(64).ByteArrayToString());
                    membershipmodel.verificationTokenExpiration = DateTime.Now.AddHours(verificationTokenExpirationTime + tokenExpirationInMinutesFromNow);
                    membershipBll.UpdatePassword(membershipmodel);
                    model.passwordVerificationToken = membershipmodel.passwordVerificationToken;
                }
            }
            catch (MembershipException ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            return model;
        }
        public static WebSecurityModel ResetPasswordWithToken(string passwordVerificationToken, string newPassword)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                UserModel user = GetUserFromResetToken(passwordVerificationToken);
                if (user == null)
                    throw new MembershipException("NullReferenceException", 7);
                if (!user.membership.verificationTokenExpiration.HasValue || user.membership.verificationTokenExpiration.Value < DateTime.Now)
                    throw new MembershipException("verificationTokenExpirationException", 8);


                PasswordHashModel passHash = HashProvider.Hash(newPassword);
                model.success = membershipBll.UpdatePassword(user.membership, passHash, PasswordFormat.ToString());
                model.updateByUser(user);
            }
            catch (MembershipException ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            return model;
        }
        public static UserModel ValidateUserWithToken(string passwordVerificationToken)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                UserModel user = GetUserFromResetToken(passwordVerificationToken);
                if (user == null)
                    throw new MembershipException("NullReferenceException", 7);
                if (!user.membership.verificationTokenExpiration.HasValue || user.membership.verificationTokenExpiration.Value < DateTime.Now)
                    throw new MembershipException("verificationTokenExpirationException", 8);

                user.membership.verificationTokenExpiration = DateTime.Now.AddDays(-3);
                membershipBll.UpdateVerificationTokenExpiration(user);
                return user;
            }
            catch (MembershipException ex)
            {
                throw ex.LogError();
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static UserModel SynerionValidateToken(string passwordVerificationToken)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                UserModel user = GetUserFromResetToken(passwordVerificationToken);
                if (user == null)
                    throw new MembershipException("NullReferenceException", 7);
                return user;
            }
            catch (MembershipException ex)
            {
                throw ex.LogError();
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static WebSecurityModel ResetPasswordCode(string idNumber, string code, string newPassword)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                UserModel user = membershipBll.GetUserFromPasswordVerificationCode(idNumber, code);
                if (user == null)
                    throw new MembershipException("NullReferenceException", 7);
                if (!user.membership.smsTokenExpiration.HasValue || user.membership.smsTokenExpiration.Value < DateTime.Now)
                    throw new MembershipException("verificationTokenExpirationException", 8);


                PasswordHashModel passHash = HashProvider.Hash(newPassword);
                model.success = membershipBll.UpdatePassword(user.membership, passHash, PasswordFormat.ToString());
                model.updateByUser(user);
            }
            catch (MembershipException ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            return model;
        }
        public static WebSecurityModel GenerateSMSCode(string idNumber)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                model.user = membershipBll.GetMembershipAndUserByIdNumber(idNumber);
                MembershipModel membershipmodel = model.user.membership;

                membershipmodel.smsTokenExpiration = DateTime.Now.AddMinutes(SmsCodeExpirationMinutes);
                membershipmodel.smsVerificationToken = generateStringCode();

                membershipBll.UpdatePassword(membershipmodel);
                model.user.membership = membershipmodel;
            }
            catch (MembershipException ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            return model;
        }
        #endregion

        #region DeleteUser
        public static bool DeleteUser(string username)
        {
            return DeleteUser(username, false);
        }
        public static bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            return membershipBll.DeleteUser(username, deleteAllRelatedData);
        }
        #endregion

        #region get users section
        public static UserModel GetUserByConfirmationToken(string confirmationToken)
        {
            return membershipBll.GetUserByConfirmationToken(confirmationToken);
        }
        public static List<UserModel> FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            return membershipBll.FindUsersByEmail(emailToMatch, pageIndex, pageSize, out totalRecords);
        }
        public static List<UserModel> FindUsersByIdNumber(string idNumberToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            return membershipBll.FindUsersByIdNumber(idNumberToMatch, pageIndex, pageSize, out totalRecords);
        }
        public static List<UserModel> GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            return membershipBll.GetAllUsers(pageIndex, pageSize, out totalRecords);
        }
        public static string GetUserNameByEmail(string email)
        {
            return getUserName(membershipBll.GetUserByEmail(email));
        }
        public static bool UserExists(string idNumber)
        {
            return membershipBll.GetUserByIdNumber(idNumber) != null;
        }
        public static UserModel GetUser(string idNumber)
        {
            return membershipBll.GetUserByIdNumber(idNumber);
        }
        public static int GetUserDefaultMunicipality(string idNumber)
        {
            return membershipBll.GetUserDefaultMunicipality(idNumber);
        }
        public static int GetUserDefaultRashutId(int municipalityId)
        {
            return membershipBll.GetUserDefaultRashutId(municipalityId);
        }
        public static UserModel GetUserFromResetToken(string passwordResetToken)
        {
            return membershipBll.GetUserFromPasswordVerificationToken(passwordResetToken);
        }
        public static MembershipModel GetMembership(string idNumber)
        {
            return membershipBll.GetMembershipByIdNumber(idNumber);
        }
        public static bool ValidateUserToken(WSPostExtensionModel model)
        {
            try
            {
                UserModel user = membershipBll.GetUserFromPasswordVerificationCode(model.idNumber, model.Token);
                return user != null;
            }
            catch(Exception ex)
            {
                return ex.LogError(false);
            }
        }
        public static int GetUserFactory(int userId, int municipalityId)
        {
            return membershipBll.GetUserFactoryId(userId, municipalityId);
        }
        #endregion

        #region UpdateUser
        public static void UpdateUser(UserModel user)
        {
            membershipBll.UpdateUser(user);
        }
        public static void UpdateUser(USER user)
        {
            membershipBll.UpdateUser(user);
        }
        public static UserModel UpdateUserDataFromWS(ResponsePersonalInformation res, UserModel user)
        {
            try
            {
                user = userBll.UpdateUserDataFromWS(res, user);
            }
            catch (Exception ex)
            {
                ex.LogError(user);
            }

            return user;
        }
        public static UserModel UpdateUserDataFromWS(EmployeeDada employee, UserModel user)
        {
            try
            {
                user = userBll.UpdateUserDataFromWS(employee, user.ID);
            }
            catch (Exception ex)
            {
                ex.LogError(user);
            }

            return user;
        }
        public static void RegisterLoginLog(int userId, int currentMunicipalityId, string action = "login")
        {
            ///TODO => GET USER IP ADDRESS FROM SECURITY
            string ip = "";
            userBll.UpdateLoginLog(userId, currentMunicipalityId, action, ip);
        }
        public static void RegisterLoginLog(UserCredentialsModel credentials, string action = "login")
        {
            ///TODO => GET USER IP ADDRESS FROM SECURITY
            string ip = "";
            userBll.UpdateLoginLog(credentials.idNumber, credentials.currentMunicipalityId, action, ip);
        }
        #endregion

        #region Validate User
        public static WebSecurityModel ValidateUser(string idNumber, string password)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                UserModel user = membershipBll.GetUserByIdNumber(idNumber);
                if (user == null)
                    throw new MembershipException("NullReferenceException", 7);
                if (!user.isConfirmed || !user.membership.isConfirmed)
                    throw new MembershipException("UserNotConfirmedException", 9);
                model.updateByUser(user);
                model.success = HashProvider.Verify(password, user.membership.password, user.membership.passwordSalt);
                if (!model.success)
                {
                    model.exceptionId = 10;
                    model.exception = "UserVerifyException";
                    membershipBll.UpdatePasswordFailure(model.user.membership);
                    WebSecurity.GenerateFailLoginCookie(idNumber, model.user.membership.PasswordFailureSCounter.ToInt());
                }
                else if(user.municipalities.FirstOrDefault(m=> m.active) == null)
                {
                    model.success = false;
                    model.exceptionId = 11;
                    model.exception = "UserMunicipalityVerifyException";
                }
                else
                {
                    model.user.membership.PasswordFailureSCounter = -1;
                    AsyncFunctionCaller.RunAsync(() =>
                    {
                        membershipBll.UpdatePasswordFailure(model.user.membership);
                    }, true);
                    WebSecurity.GenerateFailLoginCookie(idNumber, 0);
                }
            }
            catch (MembershipException ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            return model;
        }
        public static void ValidateUser(this WebSecurityModel model, string idNumber, string password)
        {
            UserModel user = membershipBll.GetUserByIdNumber(idNumber);
            if (user == null)
            {
                model.exceptionId = 7;
                model.exception = "NullReferenceException";
                return;
            }
            if (!user.isConfirmed || !user.membership.isConfirmed)
                throw new MembershipException("UserNotConfirmedException", 9);

            model.updateByUser(user);
            model.success = HashProvider.Verify(password, user.membership.password, user.membership.passwordSalt);
            if (!model.success)
            {
                model.exceptionId = 10;
                model.exception = "UserVerifyException";
                membershipBll.UpdatePasswordFailure(model.user.membership);
                WebSecurity.GenerateFailLoginCookie(idNumber, model.user.membership.PasswordFailureSCounter.ToInt());
            }
            else
            {
                model.user.membership.PasswordFailureSCounter = 0;
                membershipBll.UpdatePasswordFailure(model.user.membership);
                WebSecurity.GenerateFailLoginCookie(idNumber, 0);
            }

            return;
        }
        #endregion

        #region Confirm Account
        public static WebSecurityModel ConfirmAccount(string confirmationToken)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                UserModel user = membershipBll.TokenUserConfirmation(confirmationToken);
                ///add group to user
                new UserGroupsBll().AddUserToGroup(user.ID.ToInt(), 1);

                model.updateByUser(user);
            }
            catch (MembershipException ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            return model;
        }
        public static WebSecurityModel ConfirmAccount(string idNumber, string token)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                UserModel user = membershipBll.GetUserFromPasswordVerificationCode(idNumber, token);
                if (user == null)
                    throw new MembershipException("NullReferenceException", 7);
                if (!user.membership.smsTokenExpiration.HasValue || user.membership.smsTokenExpiration.Value < DateTime.Now)
                    throw new MembershipException("verificationTokenExpirationException", 8);

                registerBll.InsertMunicipalitiesToUser(user.idNumber);
                membershipBll.ConfirmUserFromSmsToken(token);

                user = membershipBll.GetUserById(user.ID.ToDecimal());

                model.updateByUser(user);
            }
            catch (MembershipException ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            return model;
        }
        #endregion

        #endregion

        #region Utility Methods
        private static string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
            {
                return defaultValue;
            }

            return configValue;
        }
        private static string getUserName(USER model)
        {
            if (model == null)
                return string.Empty;
            return string.Format("{0} {1}", model.FIRSTNAME, model.LASTNAME);
        }
        private static string getUserName(UserModel model)
        {
            if (model == null)
                return string.Empty;
            return string.Format("{0} {1}", model.firstName, model.lastName);
        }
        private static UserNameClass parseUsername(string username)
        {
            string[] name = string.IsNullOrWhiteSpace(username) || username.IndexOf(" ") < 1 ? new string[2] : username.Split(' ');
            return new UserNameClass
            {
                firstName = string.Join(" ", name.Take(name.Length - 1)),
                lastName = name[name.Length - 1],
            };
        }
        private static void UpdatePasswordFailure(MembershipModel model)
        {
            membershipBll.UpdatePasswordFailure(model);
        }
        public static void updateByUser(this WebSecurityModel mode, UserModel user)
        {
            if (mode == null)
                mode = new WebSecurityModel();
            if (user == null)
                user = mode.user;

            mode.user = user;
            mode.userId = user.ID;
            mode.userIdNumber = user.idNumber;
            mode.isConfirmed = user.isConfirmed;
        }
        private static string FixString(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9]", "");
        }
        #endregion

        #region validation Methods
        private static bool validatingPassword(string Password)
        {
            if (string.IsNullOrWhiteSpace(Password) || Password.Length < minRequiredPasswordLength)
            {
                return false;
            }
            if (Regex.Matches(Password, @"\W|_").Count < minRequiredNonAlphanumericCharacters || Regex.Matches(Password, @"\D|_").Count < minRequiredNumericCharacters)
            {
                return false;
            }
            return true;
        }
        private static bool validateEmail(string email)
        {
            try
            {
                MailAddress m = new MailAddress(email);
                return true;
            }
            catch (FormatException ex)
            {
                return ex.LogError(false);
            }
            catch (Exception ex)
            {
                return ex.LogError(false);
            }
        }
        private static string generateStringCode()
        {
            Random rnd = new Random();
            string code = string.Empty;
            int codeLength = rnd.Next(6, 11);
            while (code.Length <= codeLength)
                code += rnd.Next(0, 111).ToString();

            return code.Substring(0, 6);
        }
        #endregion
    }
}
