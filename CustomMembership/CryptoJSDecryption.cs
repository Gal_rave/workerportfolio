﻿using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using DataAccess.Models;
using System.Text.RegularExpressions;
using System.Web;
using Logger;

namespace CustomMembership
{
    public static class CryptoJSDecryption
    {
        public static CryptoModel GenerateCrypto()
        {
            Regex rgx = new Regex("[^a-zA-Z0-9]");
            
            CryptoModel model = new CryptoModel();
            model.iv = rgx.Replace(Guid.NewGuid().ToString().ToLower(), "").Substring(0, 16);
            model.key = rgx.Replace(Guid.NewGuid().ToString().ToLower(), "").Substring(0, 16);

            return model;
        }
        public static void DecryptCryptoModel(this CryptoModel model)
        {
            model.popolateKeys();
            byte[] KEY = Encoding.UTF8.GetBytes(model.key);
            byte[] IV = Encoding.UTF8.GetBytes(model.iv);

            //DECRYPT FROM CRIPTOJS
            model.idNumber = decrypItem(model.idNumber, KEY, IV);
            model.password = decrypItem(model.password, KEY, IV);
        }
        public static void DecryptId(this CryptoModel model)
        {
            model.popolateKeys();
            byte[] KEY = Encoding.UTF8.GetBytes(model.key);
            byte[] IV = Encoding.UTF8.GetBytes(model.iv);

            //DECRYPT FROM CRIPTOJS
            model.idNumber = decrypItem(model.idNumber, KEY, IV);
        }
        public static void DecryptWSPostModel(this WSPostModel model)
        {
            string[] keys = getCryptoKeys();
            byte[] KEY = Encoding.UTF8.GetBytes(keys[0]);
            byte[] IV = Encoding.UTF8.GetBytes(keys[1]);
            
            model.calculatedIdNumber = decrypItem(model.calculatedIdNumber, KEY, IV);
            model.idNumber = decrypItem(model.idNumber, KEY, IV);
            model.requiredRole = decrypItem(model.requiredRole, KEY, IV);
        }
        public static void DecryptWSPostModel(this WSPostExtensionModel model)
        {
            string[] keys = getCryptoKeys();
            byte[] KEY = Encoding.UTF8.GetBytes(keys[0]);
            byte[] IV = Encoding.UTF8.GetBytes(keys[1]);
            
            model.calculatedIdNumber = decrypItem(model.calculatedIdNumber, KEY, IV);
            model.idNumber = decrypItem(model.idNumber, KEY, IV);
            model.Cell = decrypItem(model.Cell, KEY, IV);
            model.Email = decrypItem(model.Email, KEY, IV);
            model.Password = decrypItem(model.Password, KEY, IV);
            model.Token = decrypItem(model.Token, KEY, IV);
        }
        public static void DecryptWSPostModel(this UserSearchModel model)
        {
            string[] keys = getCryptoKeys();
            byte[] KEY = Encoding.UTF8.GetBytes(keys[0]);
            byte[] IV = Encoding.UTF8.GetBytes(keys[1]);
            
            model.idNumber = decrypItem(model.idNumber, KEY, IV);
            model.password = decrypItem(model.password, KEY, IV);
            model.requiredRole = decrypItem(model.requiredRole, KEY, IV);
        }
        public static string DecrypString(this string str)
        {
            string[] keys = getCryptoKeys();
            byte[] KEY = Encoding.UTF8.GetBytes(keys[0]);
            byte[] IV = Encoding.UTF8.GetBytes(keys[1]);
            
            return string.IsNullOrWhiteSpace(str)? string.Empty : decryptStringFromBytes(Convert.FromBase64String(str), KEY, IV);
        }
        public static string EncriptString(string str)
        {
            string[] keys = getCryptoKeys();
            return encriptStringFromBytes(str, Encoding.UTF8.GetBytes(keys[0]), Encoding.UTF8.GetBytes(keys[1]));
        }

        #region private
        private static string encriptStringFromBytes(string cipherText, byte[] key, byte[] iv)
        {
            using (var rijAlg = new RijndaelManaged())
            {
                //Settings
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.
                var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                var msEncrypt = new MemoryStream();
                using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                using (var swEncrypt = new StreamWriter(csEncrypt))
                {
                    swEncrypt.Write(cipherText);
                }

                return Convert.ToBase64String(msEncrypt.ToArray());
            }
        }
        private static string decrypItem(this string str, byte[] KEY, byte[] IV)
        {
            return string.IsNullOrWhiteSpace(str) ? string.Empty : decryptStringFromBytes(Convert.FromBase64String(str), KEY, IV);
        }
        private static string[] getCryptoKeys()
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(HttpContext.Current.Application.Get("SecurePasswordEncryptionCookieName").ToString());
            return cookie.Value.Split(',');
        }
        private static void popolateKeys(this CryptoModel model)
        {
            string[] keys = getCryptoKeys();
            model.key = keys[0];
            model.iv = keys[1];
        }
        private static string decryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an RijndaelManaged object
            // with the specified key and IV.
            try
            {
                using (var rijAlg = new RijndaelManaged())
                {
                    //Settings
                    rijAlg.Mode = CipherMode.CBC;
                    rijAlg.Padding = PaddingMode.PKCS7;
                    rijAlg.FeedbackSize = 128;

                    rijAlg.Key = key;
                    rijAlg.IV = iv;

                    // Create a decrytor to perform the stream transform.
                    var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                    // Create the streams used for decryption.
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream
                                // and place them in a string.
                                plaintext = srDecrypt.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (CryptographicUnexpectedOperationException CUOEx)
            {
                throw CUOEx.LogError();
            }
            catch (CryptographicException CEx)
            {
                throw CEx.LogError();
            }            
            catch (Exception ex)
            {
                throw ex.LogError();
            }

            return plaintext;
        }
        #endregion
    }
}