﻿using System;
using System.Security.Cryptography;

using DataAccess.Models;
using DataAccess.Extensions;

namespace CustomMembership
{
    public static class HashProvider
    {
        /// <summary>
        /// Size of salt
        /// </summary>
        private const int SaltSize = 32;
        /// <summary>
        /// size of user confirmation token
        /// </summary>
        private const int TokenSize = 64;
        /// <summary>
        /// Size of hash
        /// </summary>
        private const int HashSize = 40;
        /// <summary>
        /// number of hash iterations
        /// </summary>
        private const int iterations = 500;
        /// <summary>
        /// create new salt for paswword
        /// additionally create the ConfirmationToken to send to user on register action
        /// </summary>
        /// <param name="saltSize">optional</param>
        /// <returns></returns>
        public static byte[] Salt(int saltSize = SaltSize)
        {
            var salt = new byte[saltSize];
            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(salt);
            }

            return salt;
        }
        /// <summary>
        /// create the `PasswordSalt` and `PasswordHash` for a registering user
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static PasswordHashModel Hash(string password)
        {
            PasswordHashModel pass = new PasswordHashModel
            {
                PasswordSalt = Salt(),
                ConfirmationToken = Salt(TokenSize),
                VerificationToken = Salt(TokenSize)
            };
            //create hash
            using (var pbkdf2 = new Rfc2898DeriveBytes(password, pass.PasswordSalt, iterations))
            {
                pass.PasswordHash = pbkdf2.GetBytes(HashSize);
            }
            
            //combine salt and hash
            pass.Password = new byte[SaltSize + HashSize];
            Array.Copy(pass.PasswordSalt, 0, pass.Password, 0, SaltSize);
            Array.Copy(pass.PasswordHash, 0, pass.Password, SaltSize, HashSize);
                        
            return pass;
        }
        public static bool Verify(string password, string hashedPassword, string passwordSalt)
        {
            string verifyPassord = hash(password, passwordSalt);
            return (verifyPassord == hashedPassword);
        }

        private static string hash(string password, string salt)
        {
            byte[] pass;
            byte[] hash;
            //create hash
            using (var pbkdf2 = new Rfc2898DeriveBytes(password, salt.ToByteArray(), iterations))
            {
                hash = pbkdf2.GetBytes(HashSize);
            }
            //combine salt and hash
            pass = new byte[SaltSize + HashSize];
            Array.Copy(salt.ToByteArray(), 0, pass, 0, SaltSize);
            Array.Copy(hash, 0, pass, SaltSize, HashSize);
            return Convert.ToBase64String(pass);
        }
    }
}
