﻿using System;
using System.IO;
using System.Web;
using System.Web.Http;

using Logger;
using DataAccess.BLLs;
using CustomMembership;
using DataAccess.Models;
using DataAccess.Extensions;
using WorkerPortfolio.App_Start;

namespace WorkerPortfolio.Controllers
{
    [Authorize]
    [RoutePrefix("filemanager")]
    [InitializeSimpleMembership]
    public class FileManagerController : ApiController
    {
        #region private class variables
        private FilesBll filesBll;
        #endregion

        public FileManagerController()
        {
            this.filesBll = new FilesBll();
        }

        [HttpPost]
        [Route("GetFileStructure")]
        public IHttpActionResult GetFileStructure(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAdminAuthorizetion(model))
                    return Ok(false);

                return Ok(this.filesBll.DirectoryStructure(model.currentMunicipalityId));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("UpdateFolder")]
        public IHttpActionResult UpdateFolder(PostDirectoryModel model)
        {
            try
            {
                if (!WebSecurity.CheckAdminAuthorizetion(model.wsModel))
                    return Ok(false);

                return Ok(this.filesBll.UpdateDirectory(model.directory));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPut]
        [Route("AddNewFolder")]
        public IHttpActionResult AddNewFolder(PostDirectoryModel model)
        {
            try
            {
                if (!WebSecurity.CheckAdminAuthorizetion(model.wsModel))
                    return Ok(false);

                return Ok(this.filesBll.CreateDirectory(model.directory));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPatch]
        [Route("DeleteFolder")]
        public IHttpActionResult DeleteFolder(PostDirectoryModel model)
        {
            try
            {
                if (!WebSecurity.CheckAdminAuthorizetion(model.wsModel))
                    return Ok(false);

                return Ok(this.filesBll.DeleteDirectory(model.directory));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("PostFile")]
        public IHttpActionResult Post()
        {
            try
            {
                HttpPostedFile file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;
                string userIDNumber = HttpContext.Current.Request.Form["userIDNumber"].DecrypString();
                string directoryId = HttpContext.Current.Request.Form["directoryId"];

                if (!WebSecurity.IsCurrentUser(userIDNumber))
                {
                    throw new Exception("UserdetailsAccessException");
                }

                if (string.IsNullOrWhiteSpace(directoryId) || string.IsNullOrWhiteSpace(userIDNumber))
                    throw new Exception("missing required field data");

                BinaryReader binaryReader = new BinaryReader(file.InputStream);
                byte[] imageBytes = binaryReader.ReadBytes(file.InputStream.Length.ToInt());

                ImageModel model = new ImageModel
                {
                    directoryId = directoryId.ToInt(),
                    municipalityId = WebSecurity.GetSetCurrentMunicipality(),
                    name = Path.GetFileNameWithoutExtension(file.FileName),
                    type = Path.GetExtension(file.FileName).Replace(".", ""),
                    fileBlob = imageBytes
                };
                return Ok(this.filesBll.AddImage(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpDelete]
        [Route("DeleteImage/{imageId}/{idNumber}")]
        public IHttpActionResult DeleteImage(int imageId, string idNumber)
        {
            try
            {
                if (!WebSecurity.IsCurrentUser(idNumber.DecrypString()))
                {
                    throw new Exception("UserdetailsAccessException");
                }

                return Ok(this.filesBll.DeleteImage(imageId));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("UpdateImage")]
        public IHttpActionResult UpdateImage(PostDirectoryModel model)
        {
            try
            {
                if (!WebSecurity.CheckAdminAuthorizetion(model.wsModel))
                    return Ok(false);

                return Ok(this.filesBll.UpdateImage(model.image));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("GetEmailTemplates")]
        public IHttpActionResult GetEmailTemplates(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAdminAuthorizetion(model))
                    return Ok(false);

                return Ok(this.filesBll.GetEmailTemplates(model.currentMunicipalityId));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("GetHtmlTemplates")]
        public IHttpActionResult GetHtmlTemplates(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAdminAuthorizetion(model))
                    return Ok(false);

                return Ok(this.filesBll.GetHtmlTemplates(model.currentMunicipalityId));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("GetPageById")]
        public IHttpActionResult GetMunicipalityPage(PostDirectoryModel model)
        {
            try
            {
                if (!WebSecurity.CheckAdminAuthorizetion(model.wsModel))
                    return Ok(false);

                return Ok(this.filesBll.GetTemplateById(model.template));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("UpdateTemplate")]
        public IHttpActionResult UpdateTemplate(PostDirectoryModel model)
        {
            try
            {
                if (!WebSecurity.CheckAdminAuthorizetion(model.wsModel))
                    return Ok(false);

                return Ok(this.filesBll.UpdateHtmlTemplate(model.template, WebSecurity.CurrentUserId));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPatch]
        [Route("DeleteTemplate")]
        public IHttpActionResult DeleteTemplate(PostDirectoryModel model)
        {
            try
            {
                if (!WebSecurity.CheckAdminAuthorizetion(model.wsModel))
                    return Ok(false);

                return Ok(this.filesBll.DeleteHtmlTemplate(model.template));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
    }
}
