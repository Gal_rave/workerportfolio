﻿using System;
using System.Web;
using System.Linq;
using System.Web.Http;
using System.Web.Routing;
using System.Configuration;
using System.Collections.Generic;

using Mail;
using Logger;
using DataAccess.BLLs;
using CustomMembership;
using DataAccess.Models;
using DataAccess.Extensions;
using WorkerPortfolio.App_Start;
using AsyncHelpers;
using DataAccess.Enums;
using System.Threading.Tasks;
using System.Threading;
using DataAccess.Mappers;

namespace WorkerPortfolio.Controllers
{
    [Authorize]
    [RoutePrefix("Mailer")]
    [InitializeSimpleMembership]
    public class MailerController : ApiController
    {
        #region private vars
        private bool isDebugeMode;
        private string debugeModeEmails;
        private string defaultSiteEmail;
        private MailerBll mailerBll;
        private MailBll mailBll;
        private MailSender ms;
        #endregion

        public MailerController()
        {
            this.isDebugeMode = ConfigurationManager.AppSettings["isDebugeMode"].ToBool();
            this.debugeModeEmails = !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["debugeModeEmails"]) ? ConfigurationManager.AppSettings["debugeModeEmails"] : "galr@ladpc.co.il";
            this.defaultSiteEmail = !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["smtpDefaultEmail"]) ? ConfigurationManager.AppSettings["smtpDefaultEmail"] : "tikoved@iula.org.il";
            this.mailerBll = new MailerBll();
            this.mailBll = new MailBll();
            this.ms = new MailSender();
        }
        [HttpGet]
        [Route("Send_NessZionaFestival")]
        public IHttpActionResult Send_NessZionaFestival(int municipalityId)
        {
            try
            {
                List<UserModel> users = mailerBll.GetUsersByMunicipalityId(municipalityId);
                UserCredentialsModel ucm;
                users.ForEach(u =>
                {
                    ucm = new UserCredentialsModel
                    {
                        firstName = u.firstName,
                        lastName = u.lastName,
                        fullName = string.Format("{0} {1}", u.firstName, u.lastName),
                        idNumber = u.idNumber,
                        email = u.email
                    };
                    if (sendEmail(ucm, "Ness-Ziona-Festival", "פסטיבל נס המזרח", "הזמנה לעובדי עיריית נס-ציונה"))
                    {
                        if (!isDebugeMode)
                            mailBll.SaveNotification(new PostNotificationMail
                            {
                                department = "עיריית נס-ציונה",
                                subject = "הזמנה לעובדי עיריית נס-ציונה",
                                userName = string.Format("{0} {1}", u.firstName, u.lastName),
                                userIdNumber = u.idNumber,
                                recipientEmail = u.email
                            }
                            );
                    }
                });

                return Ok(users.Count);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPut]
        [Route("SendFormReminder")]
        public IHttpActionResult Send101FormReminder(UserSearchModel model)
        {
            try
            {
                ///set extended timeout
                HttpContext.Current.Server.ScriptTimeout = 900;

                DateTime start = DateTime.Now;
                int step = 0;

                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");

                Log.MailLog(string.Format("FormReminder,  Step => {0},  Time => {1}", step++, 0));

                int municipalityId = model.searchMunicipalitiesId.ToInt();
                MunicipalityModel municipality = mailerBll.GetMunicipalityAndContacts(municipalityId, false);
                List<USER> users = mailerBll.GetFormReminderUsers(municipalityId, model.searchDate.Year);
                
                int OKcounter = 0;
                UserCredentialsModel ucm;

                Log.MailLog(string.Format("FormReminder,  Step => {0},  Time => {1},  Municipality => {2},  counter => {3}", step++, DateTime.Now.Subtract(start).TotalSeconds, municipalityId, users.Count));
                List<Task<string>> tasks = new List<Task<string>>();
                var Context = HttpContext.Current;
                string subject = string.Format("הודעה בדבר חובת מילוי טופס 101 לשנת המס {0}", model.searchDate.Year);

                users.ForEach(u =>
                {
                    try
                    {
                        ucm = new UserCredentialsModel
                        {
                            firstName = u.FIRSTNAME,
                            lastName = u.LASTNAME,
                            fullName = string.Format("{0} {1}", u.FIRSTNAME, u.LASTNAME),
                            idNumber = u.IDNUMBER,
                            email = string.Join(";", u.EMAILBYMUNICIPALITies.Where(e => e.MUNICIPALITYID == municipalityId).Select(e => e.EMAIL).ToList().ToList()),
                            confirmationToken = u.MEMBERSHIP.CONFIRMATIONTOKEN,
                            currentMunicipalityId = municipalityId,
                            municipalities = new List<MunicipalityModel>() { municipality },
                            currentTerminationDate = model.searchDate,
                            currentRashutId = (int)RedirectTypeEnum.Form_employer_employee
                        };
                        string template = createTemplate(ucm, "Form101Reminder");

                        if (!string.IsNullOrWhiteSpace(template))
                        {
                            Log.MailLog(ucm.idNumber, "Start Task");
                            tasks.Add(Task<string>.Factory.StartNew(() =>
                            {
                                HttpContext.Current = Context;
                                UserCredentialsModel ucm_C = new UserCredentialsModel
                                {
                                    firstName = u.FIRSTNAME,
                                    lastName = u.LASTNAME,
                                    fullName = string.Format("{0} {1}", u.FIRSTNAME, u.LASTNAME),
                                    idNumber = u.IDNUMBER,
                                    email = string.Join(";", u.EMAILBYMUNICIPALITies.Where(e => e.MUNICIPALITYID == municipalityId).Select(e => e.EMAIL).ToList().ToList()),
                                    confirmationToken = u.MEMBERSHIP.CONFIRMATIONTOKEN,
                                    currentMunicipalityId = municipalityId,
                                    municipalities = new List<MunicipalityModel>() { municipality }
                                };
                                try
                                {
                                    if (send(ucm_C, template, subject, municipality.name))
                                    {
                                        return saveMailNotification(ucm_C.email, defaultSiteEmail, ucm_C.idNumber, ucm_C.fullName, subject, template, MailTypeEnum.General);
                                    }
                                    throw new Exception("FormReminder_Exception");
                                }
                                catch (Exception ex)
                                {
                                    throw ex.LogError();
                                }
                            }));
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                });

                tasks.ForEach(t =>
                {
                    Log.MailLog(t.Result, "End Task");
                    OKcounter++;
                });

                Log.MailLog(string.Format("FormReminder,  Step => {0},  Time => {1},  municipality => {2},  counter => {3}", step++, DateTime.Now.Subtract(start).TotalSeconds, municipalityId, users.Count));
                return Ok(new { Total = users.Count, Sent = OKcounter });
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPut]
        [Route("SendProposal")]
        public IHttpActionResult SendEPC_Proposal(UserSearchModel model)
        {
            try
            {
                ///set extended timeout
                HttpContext.Current.Server.ScriptTimeout = 900;

                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");

                int municipalityId = model.searchMunicipalitiesId.ToInt();
                MunicipalityModel municipality = mailerBll.GetMunicipalityAndContacts(municipalityId);
                List<USER> users = mailerBll.UsersByMunicipalityEmail(municipalityId);
                int OKcounter = 0;
                UserCredentialsModel ucm;

                DateTime start = DateTime.Now;
                Log.MailLog(string.Format("Start EPCProposal,  Municipality => {0},  counter => {1}", municipalityId, users.Count));
                List<Task<string>> tasks = new List<Task<string>>();
                var Context = HttpContext.Current;
                
                users.ForEach(u =>
                {
                    try
                    {
                        ucm = new UserCredentialsModel
                        {
                            firstName = u.FIRSTNAME,
                            lastName = u.LASTNAME,
                            fullName = string.Format("{0} {1}", u.FIRSTNAME, u.LASTNAME),
                            idNumber = u.IDNUMBER,
                            email = string.Join(";", u.EMAILBYMUNICIPALITies.Where(e => e.MUNICIPALITYID == municipalityId).Select(e => e.EMAIL).ToList().ToList()),
                            confirmationToken = u.MEMBERSHIP.CONFIRMATIONTOKEN,
                            currentMunicipalityId = municipalityId,
                            municipalities = new List<MunicipalityModel>() { municipality },
                            currentRashutId = (int)RedirectTypeEnum.UserMenu_UPCEStatus
                        };
                        string template = createTemplate(ucm, "UPCEProposal");
                        string subject = "תלוש שכר מאובטח במייל";

                        if (!string.IsNullOrWhiteSpace(template))
                        {
                            Log.MailLog(ucm.idNumber, "Start Task");
                            tasks.Add(Task<string>.Factory.StartNew(() =>
                                {
                                    HttpContext.Current = Context;
                                    UserCredentialsModel ucm_C = new UserCredentialsModel
                                    {
                                        firstName = u.FIRSTNAME,
                                        lastName = u.LASTNAME,
                                        fullName = string.Format("{0} {1}", u.FIRSTNAME, u.LASTNAME),
                                        idNumber = u.IDNUMBER,
                                        email = string.Join(";", u.EMAILBYMUNICIPALITies.Where(e => e.MUNICIPALITYID == municipalityId).Select(e => e.EMAIL).ToList().ToList()),
                                        confirmationToken = u.MEMBERSHIP.CONFIRMATIONTOKEN,
                                        currentMunicipalityId = municipalityId,
                                        municipalities = new List<MunicipalityModel>() { municipality }
                                    };
                                    try
                                    {
                                        if (send(ucm_C, template, subject, municipality.name))
                                        {
                                            OKcounter++;
                                            return saveMailNotification(ucm_C.email, defaultSiteEmail, ucm_C.idNumber, ucm_C.fullName, subject, template, MailTypeEnum.General);
                                        }
                                        throw new Exception("SendUEPC_Exception");
                                    }
                                    catch (Exception ex)
                                    {
                                        throw ex.LogError();
                                    }
                                }));
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                });
                
                tasks.ForEach(t =>
                {
                    Log.MailLog(t.Result, "End Task");
                });
                Log.MailLog(string.Format("End EPCProposal,  municipality => {2},  Time => {0}, counter => {1}", DateTime.Now.Subtract(start).TotalSeconds, users.Count, municipalityId));

                return Ok(new { Total = users.Count, Sent = OKcounter });
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }

        #region private functions
        private bool send(UserCredentialsModel ucm, string template, string subject = "", string displayName = "")
        {
            try
            {
                this.ms.Send(!isDebugeMode ? ucm.email : this.debugeModeEmails, template, false, subject, null, displayName, defaultSiteEmail);
                Log.MailLog(ucm.idNumber, "**** send ****");
                return true;
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return false;
            }
        }
        private string createTemplate(UserCredentialsModel ucm, string templateName)
        {
            string template = string.Empty;
            RouteData routedata = new RouteData();
            routedata.Values.Add("token", ucm.confirmationToken.Substring(0, 10));
            MCE mce = new MCE();
            try
            {
                return mce.RenderView(HttpContext.Current, routedata, "MailTemplate", templateName, ucm);
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return string.Empty;
            }
        }
        private bool sendEmail(HttpContext Context, UserCredentialsModel ucm, string templateName, string subject = "", string displayName = "")
        {
            try
            {
                string template = string.Empty;
                RouteData routedata = new RouteData();
                routedata.Values.Add("token", ucm.confirmationToken.Substring(0, 10));
                MCE mce = new MCE();

                /* // send the mail */
                if (!isDebugeMode)
                {
                    template = mce.RenderView(Context, routedata, "MailTemplate", templateName, ucm);
                    this.ms.Send(ucm.email, template, false, subject, null, displayName, defaultSiteEmail);
                }
                else
                {
                    template = mce.RenderView(Context, routedata, "MailTemplate", templateName, ucm);
                    this.ms.Send(this.debugeModeEmails, template, false, subject, null, displayName, defaultSiteEmail);
                }

                /*// save the mail notification */
                //saveMailNotification(ucm.email, defaultSiteEmail, ucm.idNumber, ucm.fullName, subject, template, MailTypeEnum.General);

                return true;
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return false;
            }
        }
        private bool sendEmail(UserCredentialsModel ucm, string templateName, string subject = "", string displayName = "")
        {
            try
            {
                string template = string.Empty;
                RouteData routedata = new RouteData();

                /* // send the mail */
                if (!isDebugeMode)
                {
                    template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", templateName, ucm);
                    this.ms.Send(ucm.email, template, false, subject, null, displayName, defaultSiteEmail);
                }
                else
                {
                    template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", templateName, ucm);
                    this.ms.Send(this.debugeModeEmails, template, false, subject, null, displayName, defaultSiteEmail);
                }

                /*// save the mail notification */
                saveMailNotification(ucm.email, defaultSiteEmail, ucm.idNumber, ucm.fullName, subject, template, MailTypeEnum.General);

                return true;
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return false;
            }
        }
        private bool validateUser(UserSearchModel model)
        {
            if (!WebSecurity.CheckAdminAuthorizetion(model))
                throw new AccessViolationException("AccessViolationException");

            model.municipalityId = WebSecurity.GetSetCurrentMunicipality();
            model.maxImmunity = WebSecurity.GetMaxImmunity(model.idNumber);
            return true;
        }
        private static string saveMailNotification(string recipientEmail, string senderEmail, string userIdNumber, string userName, string subject, string text, MailTypeEnum type, bool includingFiles = false)
        {
            new MailBll().SaveNotification(
                new MAILNOTIFICATION
                {
                    SENDEREMAIL = senderEmail,
                    RECIPIENTEMAIL = recipientEmail,
                    USERIDNUMBER = userIdNumber,
                    USERNAME = userName,
                    SUBJECT = subject,
                    FREETEXT = text,
                    INCLUDINGFILES = includingFiles,
                    MAILTYPE = (int)type,
                    CREATEDDATE = DateTime.Now
                }
            );
            return userIdNumber;
        }
        #endregion
    }
}
