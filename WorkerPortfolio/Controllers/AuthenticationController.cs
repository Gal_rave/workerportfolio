﻿using CustomMembership;
using DataAccess.BLLs;
using DataAccess.Enums;
using DataAccess.Extensions;
using DataAccess.Models;
using Logger;
using System;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace WorkerPortfolio.Controllers
{
    public class AuthenticationController : Controller
    {
        public ActionResult ConfirmAccount(string confirmationToken)
        {
            try
            {
                WebSecurityModel model = WebSecurity.ConfirmAccount(confirmationToken);
                if (model.success)
                {
                    return Redirect("/#/authentication/authenticateduserlogin/" + confirmationToken);
                }
                return Redirect("/error/" + confirmationToken);
            }
            catch
            {
                return Redirect("/error/" + confirmationToken);
            }
        }
        public ActionResult RegisterAccount(string confirmationToken)
        {
            UserModel model = WebSecurity.getUserFromConfirmationToken(confirmationToken);
            if (model != null)
            {
                return Redirect("/#/authentication/authenticateduserlogin/" + confirmationToken);
            }
            return Redirect("/error/" + confirmationToken);
        }
        public ActionResult AuthenticateKadAccount(string Token, int rashut, int mncpility)
        {
            try
            {
                WebSecurityModel model = new WebSecurityModel();
                UserModel user = CustomMembershipProvider.ValidateUserWithToken(Token);
                if (user != null)
                {
                    model.updateByUser(user);
                    WebSecurity.LoginFromToken(model);
                    if (model.success)
                    {
                        return Redirect("/#/authentication/logintoprocess/" + user.membership.confirmationToken + "/" + rashut + "/" + mncpility);
                    }
                }
                return Redirect("/error/" + user.membership.confirmationToken);
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return Redirect("/error/");
            }
        }
        public ActionResult ConfirmUPCE(int municipalityId, string confirmationToken)
        {
            Log.MailLog(new { confirmationToken = confirmationToken, municipalityId = municipalityId }, "ConfirmUPCE");
            try
            {
                WebSecurity.Logout();

                UserBll userBll = new UserBll();
                MembershipProviderBll MPbll = new MembershipProviderBll();

                UserModel user = MPbll.GetUserByConfirmationToken(confirmationToken);
                WebSecurityModel model = new WebSecurityModel();
                if (user != null)
                {
                    model.updateByUser(user);
                    WebSecurity.LoginFromToken(model);
                    if (model.success)
                    {
                        PasswordHashModel hash = HashProvider.Hash("password");
                        MPbll.UpdateUserTokens(user, hash);
                        return Redirect("/#/authentication/confirm-upce/" + hash.confirmationToken + "/" + municipalityId);

                        //return Redirect("/#/authentication/login-redirect/UserMenu.UPCEStatus/" + confirmationToken + "/" + municipalityId);
                    }
                }
                return Redirect("/error/" + confirmationToken);
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return Redirect("/error/");
            }
        }
        public ActionResult AuthenticationReDirect(int redirectType, int municipalityId, string confirmationToken)
        {
            string redirectLocation = translateRedirectType(redirectType);

            Log.MailLog(new { confirmationToken = confirmationToken, municipalityId = municipalityId, _redirectLocation = redirectLocation, redirectType = redirectType }, "FormDirect");
            try
            {
                WebSecurity.Logout();

                UserBll userBll = new UserBll();
                MembershipProviderBll MPbll = new MembershipProviderBll();

                UserModel user = MPbll.GetUserByConfirmationToken(confirmationToken);
                WebSecurityModel model = new WebSecurityModel();
                if (user != null)
                {
                    model.updateByUser(user);
                    WebSecurity.LoginFromToken(model);
                    if (model.success)
                    {
                        
                        PasswordHashModel hash = HashProvider.Hash("password");
                        MPbll.UpdateUserTokens(user, hash);
                        return Redirect(string.Format("/#/authentication/login-redirect/{0}/{1}/{2}", redirectLocation, hash.confirmationToken, municipalityId));
                        
                        //return Redirect(string.Format("/#/authentication/login-redirect/{0}/{1}/{2}", redirectLocation, confirmationToken, municipalityId));
                    }
                }
                return Redirect("/error/" + confirmationToken);
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return Redirect("/error/");
            }
        }

        #region private
        private string translateRedirectType(int redirectType)
        {
            Regex regex = new Regex(Regex.Escape("_"));
            string redirectLocation = regex.Replace(regex.Replace(((RedirectTypeEnum)redirectType).ToString(), ".", 1), "-");
            return redirectLocation;
        }
        #endregion private
    }
}