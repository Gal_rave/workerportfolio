﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

using CustomMembership;
using DataAccess.BLLs;
using DataAccess.Models;
using Logger;
using Mail;
using WorkerPortfolio.App_Start;
using System.Configuration;

namespace WorkerPortfolio.Controllers
{
    [Authorize]
    [RoutePrefix("api/ContactUs")]
    [InitializeSimpleMembership]
    public class ContactUsController : ApiController
    {
        private UserBll userBll;
        private MailBll mailBll;

        public ContactUsController()
        {
            this.userBll = new UserBll();
            this.mailBll = new MailBll();
        }

        [HttpPost]
        [Route("ToMailItems")]
        public IHttpActionResult GetToMailItems(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model.idNumber, model.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");
                
                List<humanResourcesContactModel> list = mailBll.GetContacts(model.currentMunicipalityId);

                return Ok(list);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("SendNotificationMail")]
        public IHttpActionResult SendNotificationMail(PostNotificationMail model)
        {
            try
            {
                if (!WebSecurity.IsCurrentUser(model.userIdNumber))
                    throw new FieldAccessException("UserdetailsAccessException");

                return Ok(MailActions.SendNotificationMail(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("ContactUs")]
        public IHttpActionResult SendContactUsMail(PostNotificationMail model)
        {
            try
            {
                return Ok(MailActions.SendContactUsMail(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [AllowAnonymous]
        [HttpPut]
        [Route("GetSiteMail")]
        public IHttpActionResult GetSiteMail()
        {
            try
            {
                return Ok(ConfigurationManager.AppSettings["smtpDefaultEmail"]);
            }
            catch(Exception e)
            {
                return this.ServerError(e);
            }
        }
    }
}
