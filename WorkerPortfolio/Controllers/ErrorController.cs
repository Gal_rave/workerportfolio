﻿using System.Configuration;
using System.Web.Mvc;

namespace WorkerPortfolio.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            Logger.Log.LogError("ErrorController Index");
            ViewBag.email = ConfigurationManager.AppSettings["smtpDefaultEmail"];
            return View();

            //return RedirectToAction("Index2", "Home");
        }
        public ActionResult Error()
        {
            Logger.Log.LogError("ErrorController Error");
            ViewBag.email = ConfigurationManager.AppSettings["smtpDefaultEmail"];
            return View();

            //return RedirectToAction("Index2", "Home");
        }
    }
}
