﻿using System;
using System.Web.Http;

using WorkerPortfolio.App_Start;
using DataAccess.Models;
using DataAccess.BLLs;
using CustomMembership;
using Logger;
using System.Web;

namespace WorkerPortfolio.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    [InitializeSimpleMembership]
    public class AccountController : ApiController
    {
        private UserBll userbll;
        public AccountController()
        {
            userbll = new UserBll();
        }

        [HttpPost]
        [Route("GetFullUserdetails")]
        public IHttpActionResult GetFullUserdetails(PostTokenModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model.idNumber, model.Token))
                    throw new FieldAccessException("UserdetailsAccessException");
                return Ok(userbll.GetUserByConfirmationToken(model.Token));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPut]
        [Route("UpdateUserDetails")]
        public IHttpActionResult UpdateUserDetails(UserModel model)
        {
            try
            {
                return Ok(userbll.UpdateUserDetails(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPatch]
        [Route("ResetPassword")]
        public object ResetPassword(PostTokenModel model)
        {
            try
            {
                return Ok(WebSecurity.ChangePasswordFromConfirmationToken(model.Token, model.oldPassword, model.newPassword));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("Userdetails")]
        public IHttpActionResult GetUserdetails(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                return Ok(userbll.GetFullUserdetails(model.idNumber));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("PartnerDetails")]
        public IHttpActionResult GetPartnerDetails(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                return Ok(userbll.GetPartnerDetails(model.idNumber));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("EducationStatus")]
        public IHttpActionResult GetUserEducationStatus(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                return Ok(userbll.GetUserEducation(model.idNumber));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("UserChildren")]
        public IHttpActionResult GetUserChildren(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                return Ok(userbll.GetUserChildren(model.idNumber));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("ChangeMunicipality")]
        public IHttpActionResult ChangeCurrentMunicipality(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                UserCredentialsModel Credentials = new UserCredentialsModel();
                Credentials.currentMunicipalityId = WebSecurity.GetSetCurrentMunicipality(model.currentMunicipalityId);
                if (model.updateUserData)
                {
                    Credentials = WebSecurity.updateAndSetUser(model).userCredentials;
                }
                return Ok(Credentials);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("UpdateUserData")]
        public IHttpActionResult UpdateUserData(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                return Ok(WebSecurity.updateAndSetUser(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("GetUPCData")]
        public IHttpActionResult GetUPCData(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                return Ok(userbll.GetUserUPCEData(model.idNumber));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("UserUpdateTypes")]
        public IHttpActionResult UserUpdateTypes(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                return Ok(userbll.FindUserUpdateTypes(model.idNumber));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("SynerionValidate/{Token}")]
        public IHttpActionResult SynerionValidate(string Token)
        {
            try
            {
                Log.TestLog(HttpContext.Current.Request.Url, "SynerionValidate");

                UserModel user = CustomMembershipProvider.SynerionValidateToken(Token);
                if (user == null)
                    throw new MembershipException("NullUserReferenceException", 102);
                SynerionRedirectData obj = new SynerionRedirectData(user.idNumber);

                Log.TestLog(obj, "SynerionValidate => obj");
                return Ok(obj);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }

        #region private
        private class SynerionRedirectData
        {
            public SynerionRedirectData(string _ssn)
            {
                this.ssn = Pad(_ssn);
            }
            public string ssn { get; set; }

            private string Pad(string val)
            {
                while (val.Length < 16)
                    val += " ";

                return val;
            }
        }
        #endregion
    }
}