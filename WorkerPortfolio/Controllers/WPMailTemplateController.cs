﻿using System.Web.Mvc;
using DataAccess.Models;
using Logger;
using System;

namespace WorkerPortfolio.Controllers
{
    public class WPMailTemplateController : Controller
    {
        public ActionResult Index(UserCredentialsModel model)
        {
            Log.TestLog("WPMailTemplateController => Index");
            return View(model);
        }
        public ActionResult PasswordReset(UserCredentialsModel model)
        {
            Log.TestLog("WPMailTemplateController => PasswordReset");
            return View(model);
        }
        public ActionResult NewUserRegister(UserCredentialsModel model)
        {
            Log.TestLog("WPMailTemplateController => NewUserRegister");
            return View(model);
        }
        public ActionResult Form101SeccessEmail(UserCredentialsModel model)
        {
            Log.TestLog("WPMailTemplateController => Form101SeccessEmail");
            return View(model);
        }
        //public ActionResult ShowTemplate()
        //{
        //    UserCredentialsModel ucm = new UserCredentialsModel
        //    {
        //        fullName = string.Format("{0} {1}", "שושנה", "פרץ"),
        //        idNumber = "032103210",
        //        email = "galr@ladpc.co.il",
        //        confirmationToken = "98s7df8s7df98w7ef987sd9f87sd",
        //        currentMunicipalityId = 62999,
        //        currentTerminationDate = new DateTime(2011, 7, 7),
        //        currentRashutId = 1
        //    };
        //    return View("../MailTemplate/Form101Reminder_Copy", ucm);
        //}
    }
}