﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Web;
using System.Threading;
using System.Configuration;
using System.Web.Routing;

using Logger;
using WebServiceProvider;
using DataAccess.Models;
using AsyncHelpers;
using CustomMembership;
using DataAccess.Extensions;
using DataAccess.BLLs;
using WorkerPortfolio.App_Start;
using Mail;
using DataAccess.Mappers;

namespace WorkerPortfolio.Controllers
{
    [RoutePrefix("DataImporter")]
    public class DataImportController : ApiController
    {
        #region private vars
        private UserBll userbll;
        private RegisterBll registerBll;
        private SetUpBll setUpBll;
        private FormBll formBll;
        private ProcessBll processBll;
        private int PeyCheckStartMonth;
        private bool isDebugeMode;
        private int chunkSize;
        private List<decimal> TerminationCauseExitList;
        private MailSender ms;

        private static int fileCounter;
        private static object startDataImportFromWS_Lock = new object();
        private static object RunUsersUpdateSequence_Lock = new object();
        private static object InternalBulkRegister_Lock = new object();
        private static object InternalBulkRegisterByMunicipality_Lock = new object();
        private static object reDoSendEvent_Lock = new object();
        private static object customImport_Lock = new object();
        private static object schoolImport_Lock = new object();
        private static object processesImport_Lock = new object();
        private static object updateProcesses_Lock = new object();
        private static object sacharUsersImport_Lock = new object();
        private static object orgUnitMangrsImport_Lock = new object();
        private static object rashutNumberImport_Lock = new object();
        private static object SynchronizeUserPayCheckByMail_Lock = new object();
        private static object CreateMobileImages_Lock = new object();
        private static object ProcessesStoredProcedures_Lock = new object();
        private static object BirthdayGreetings_Lock = new object();
        #endregion

        public DataImportController()
        {
            this.userbll = new UserBll();
            this.registerBll = new RegisterBll();
            this.setUpBll = new SetUpBll();
            this.formBll = new FormBll();
            this.processBll = new ProcessBll();
            this.ms = new MailSender();

            PeyCheckStartMonth = ConfigurationManager.AppSettings["PeyCheckStartMonth"].ToInt();
            this.isDebugeMode = ConfigurationManager.AppSettings["isDebugeMode"].ToBool();
            this.chunkSize = ConfigurationManager.AppSettings["chunker"].ToInt();
            this.TerminationCauseExitList = ConfigurationManager.AppSettings["TerminationCauseExit"].ToDecimalLIst();
        }
        
        [HttpGet]
        [Route("ImportCustomMunicipalities/{municipalities}")]
        public IHttpActionResult ImportCustomMunicipalities(string municipalities)
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "ImportCustomMunicipalities");
            fileCounter = 0;
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(startDataImportFromWS_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (startDataImportFromWS_Lock)
                        {
                            SystemUsersProvider.StartImportCustomMunicipalitiesFromWS(municipalities);
                        }
                    }
                    finally
                    {
                        Monitor.Exit(startDataImportFromWS_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("StartImportSingle/{municipality}")]
        public IHttpActionResult ImportSingle(int municipality)
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "StartImportSingle");
            fileCounter = 0;
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(startDataImportFromWS_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (startDataImportFromWS_Lock)
                        {
                            SystemUsersProvider.StartImportmunicipalityProcessFromWS(municipality);
                        }
                    }
                    finally
                    {
                        Monitor.Exit(startDataImportFromWS_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("ImportCitiesStreets/{importType}")]
        public IHttpActionResult ImportCitiesStreets(bool importType)
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "ImportCitiesStreets");
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(customImport_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (customImport_Lock)
                        {
                            if (importType)
                            {
                                CityBll city = new CityBll();
                                city.ImportCitiesFromWS(SystemUsersProvider.GetCitiesFromWS());
                            }
                            else
                            {
                                StreetBll street = new StreetBll();
                                street.ImportStreetsFromWS(SystemUsersProvider.GetStreetsFromWS());
                            }
                        }
                    }
                    finally
                    {
                        Monitor.Exit(customImport_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("ImportProcessesData")]
        public IHttpActionResult ImportProcessesData()
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "ImportProcessesData");
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(processesImport_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (processesImport_Lock)
                        {
                            RegisterBll bll = new RegisterBll();
                            List<int> municipalities = bll.GetAllmunicipalities(true, true);

                            List<TahalichModel> tahalichim;
                            List<StepModel> shlavim;
                            List<ReshimatTiyugModel> reshimotTiyug;
                            List<MismachBereshimaModel> mismachimBeReshima;
                            List<MismachModel> mismachim;
                            List<KvuzatMishtamshimModel> kvuzotMishtamshim;
                            List<MishtamsheiKvuzaModel> mishtamsheiKvuza;


                            foreach (int mun in municipalities)
                            {
                                Log.ApplicationLog(string.Join(",", mun, "StartImportProcessesData"));
                                List<PackedProcessModel> packedProcessModel = SystemUsersProvider.GetPackedProcessesData(mun);
                                Log.ApplicationLog(string.Join(",", mun, "EndImportProcessesData"));

                                if (packedProcessModel.Count() > 0 && packedProcessModel != null)
                                {
                                    tahalichim = new List<TahalichModel>();
                                    shlavim = new List<StepModel>();

                                    reshimotTiyug = new List<ReshimatTiyugModel>();
                                    mismachimBeReshima = new List<MismachBereshimaModel>();
                                    mismachim = new List<MismachModel>();
                                    kvuzotMishtamshim = new List<KvuzatMishtamshimModel>();
                                    mishtamsheiKvuza = new List<MishtamsheiKvuzaModel>();

                                    reArrangeProcessesData(packedProcessModel, tahalichim, shlavim, reshimotTiyug, mismachimBeReshima, mismachim, kvuzotMishtamshim, mishtamsheiKvuza);//only processes and steps 

                                    Log.ApplicationLog(string.Join(",", mun, "EndImportProcessesData"));

                                    int rashut = tahalichim.Count > 0 ? tahalichim.First().rashut : 0;
                                    processBll.SaveTahalichimData(tahalichim, shlavim, reshimotTiyug, mismachimBeReshima, mismachim, kvuzotMishtamshim, mishtamsheiKvuza, rashut);
                                }
                            }
                        }
                    }
                    catch (Exception Ie)
                    {
                        return this.ServerError(Ie);
                    }
                    finally
                    {
                        Monitor.Exit(processesImport_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("ImportSacharUsers")]
        public IHttpActionResult ImportSacharUsers()
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "ImportSacharUsers");
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(sacharUsersImport_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (sacharUsersImport_Lock)
                        {
                            RegisterBll bll = new RegisterBll();
                            List<int> municipalities = bll.GetAllmunicipalities(true, true);

                            List<SACHAR_USERS_GELEM> sacharUsers = SystemUsersProvider.GetSacharSystemUsers(municipalities);

                            Log.ApplicationLog(string.Join(",", municipalities, "ImportSacharUsers"));

                            if (sacharUsers.Count() > 0 && sacharUsers != null)
                            {
                                Log.ApplicationLog(string.Join(",", "stored procedure insert sachar users", "ImportSacharUsers"));
                                processBll.synchronizeUsersAndSave(sacharUsers);
                            }
                        }
                    }
                    catch (Exception Ie)
                    {
                        return this.ServerError(Ie);
                    }
                    finally
                    {
                        Monitor.Exit(sacharUsersImport_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("ImportAcademicInst")]
        public IHttpActionResult ImportAcademicInst()
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "ImportAcademicInst");
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(schoolImport_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (schoolImport_Lock)
                        {
                            //academicWrraper allAcademicInsts = SystemUsersProvider.GetAcademicInstFromWS();
                            //processBll.saveAcademicInsts(allAcademicInsts);
                        }
                    }
                    catch (Exception Ie)
                    {
                        return this.ServerError(Ie);
                    }
                    finally
                    {
                        Monitor.Exit(schoolImport_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("ImportOrganizationUnitManagers")]
        public IHttpActionResult ImportOrganizationUnitManagers()
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "ImportOrganizationUnitManagers");
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(orgUnitMangrsImport_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (orgUnitMangrsImport_Lock)
                        {
                            RegisterBll bll = new RegisterBll();
                            List<int> municipalities = bll.GetAllmunicipalities(true, true);

                            foreach (int mun in municipalities)
                            {
                                Log.ApplicationLog(string.Join(",", mun, "StartImportOrganizationUnitManagers"));
                                List<UnitManagerModel> unitManagers = SystemUsersProvider.getOrganizationUnitsManagers(mun);
                                Log.ApplicationLog(string.Join(",", mun, "EndImportOrganizationUnitManagers"));
                                int rashut = unitManagers.Count > 0 ? unitManagers.First().rashutNumber : 0;
                                processBll.SaveUnitMngrs(unitManagers, rashut);
                            }
                            processBll.SyncUnitMngrsAndUsers();
                        }
                    }
                    catch (Exception Ie)
                    {
                        return this.ServerError(Ie);
                    }
                    finally
                    {
                        Monitor.Exit(orgUnitMangrsImport_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("UpdateProcessesStatus")]
        public IHttpActionResult UpdateProcessesStatus()
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "UpdateProcessesStatus");
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(updateProcesses_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (updateProcesses_Lock)
                        {
                            RegisterBll bll = new RegisterBll();
                            ProcessBll processBll = new ProcessBll();
                            List<int> municipalities = bll.GetAllmunicipalities(true, true);//get the active municipalities which have processes module

                            List<UpdateProcessModel> processesResults = SystemUsersProvider.GetProcessesStatus(municipalities);

                            if (processesResults.Count() > 0 && processesResults != null)
                            {
                                List<int> sentMailProcesses = new List<int>();

                                foreach (UpdateProcessModel process in processesResults)
                                {
                                    List<PROCESS> prcs = processBll.getProcessesByTmIdAndRashut(process.tmProcessId, process.customerNumber);//
                                    if (prcs != null)
                                    {
                                        foreach (PROCESS pr in prcs)
                                        {
                                            if (pr.STATUS == 3)//if the process already completed -> do nothing
                                            {
                                                continue;
                                            }
                                            else//update new status section
                                            {
                                                if (process.processStatus == 2)//if new status is completed then send suitable emails (2 is completed in java)
                                                {
                                                    if (!mailSentAlready(sentMailProcesses, process.tmProcessId))
                                                    {
                                                        ProcessModel p = processBll.getProcessRecord(pr.ID);
                                                        sendProcessCompletedEmails(p, process);
                                                        sentMailProcesses.Add(process.tmProcessId);
                                                    }
                                                }
                                                processBll.updateProcessRecordStatus(pr, process);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception Ie)
                    {
                        return this.ServerError(Ie);
                    }
                    finally
                    {
                        Monitor.Exit(updateProcesses_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("StartDataImportFromWS")]
        public IHttpActionResult StartDataImportFromWS()
        {
            Log.LogLoad(HttpContext.Current.Request.Url, "startDataImportFromWS");
            fileCounter = 0;
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(startDataImportFromWS_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (startDataImportFromWS_Lock)
                        {
                            ///set extended timeout
                            HttpContext.Current.Server.ScriptTimeout = 3600;

                            SystemUsersProvider.StartImportProcessFromWS();
                            ///delete old directories
                            FolderExtensions.CleanDirectory(Path.Combine(HttpContext.Current.Server.MapPath("~/Content"), "uploads"), 90);
                        }
                    }
                    finally
                    {
                        Monitor.Exit(startDataImportFromWS_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("RunUsersUpdateSequence")]
        public IHttpActionResult RunUsersUpdateSequence()
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "RunUsersUpdateSequence");
            fileCounter = 0;
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(RunUsersUpdateSequence_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (RunUsersUpdateSequence_Lock)
                        {
                            ///set extended timeout
                            HttpContext.Current.Server.ScriptTimeout = 3600;

                            SystemUsersProvider.RunImportSystemUsers();
                            DistinctContactData();
                        }
                    }
                    finally
                    {
                        Monitor.Exit(RunUsersUpdateSequence_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("InternalBulkRegister")]
        public bool InternalBulkRegister()
        {
            bool s = false;
            Log.LogLoad(HttpContext.Current.Request.Url, "InternalBulkRegister");
            fileCounter = 0;
            if (HttpContext.Current.Request.IsLocal)
            {

                if (!Monitor.TryEnter(InternalBulkRegister_Lock, new TimeSpan(0)))
                {
                    throw new Exception("ServiceBusyException_Locked!");
                }
                try
                {
                    lock (InternalBulkRegister_Lock)
                    {
                        ///set extended timeout
                        HttpContext.Current.Server.ScriptTimeout = 3600;

                        registerBll.RemoveEmptyUsers();
                        List<int> municipalitis = registerBll.GetAllmunicipalities(true);
                        Log.LogLoad(string.Join(",", municipalitis), "InternalBulkRegister -> municipalitis");
                        DistinctContactData();
                        foreach (int municipality in municipalitis)
                        {
                            registerByMunicipality(municipality);
                        }
                        registerBll.ConnectEmailsByMunicipality();
                        DistinctContactData();
                        WebSecurity.SendWellcomeEmails();
                        Log.LogLoad("after SendWellcomeEmails");
                        registerBll.RemoveEmptyUsers();
                        s = true;
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(InternalBulkRegister_Lock);
                }
            }
            return s;
        }
        [HttpGet]
        [Route("BulkRegisterByMunicipality")]
        public bool InternalBulkRegisterByMunicipality(string municipality)
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "BulkRegisterByMunicipality");
            fileCounter = 0;
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {
                    if (!Monitor.TryEnter(InternalBulkRegisterByMunicipality_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (InternalBulkRegisterByMunicipality_Lock)
                        {
                            registerBll.RemoveEmptyUsers();

                            if (municipality.IndexOf(",") < 0)
                            {
                                registerByMunicipality(municipality.ToInt());
                            }
                            else
                            {
                                foreach (int num in municipality.ToIntLIst())
                                {
                                    registerByMunicipality(num.ToInt());
                                }
                            }

                            registerBll.RemoveEmptyUsers();
                            WebSecurity.SendWellcomeEmails();
                        }
                    }
                    finally
                    {
                        Monitor.Exit(InternalBulkRegisterByMunicipality_Lock);
                    }
                }
            }
            catch (Exception e)
            {
                this.ServerError(e);
            }
            return true;
        }
        [HttpPost]
        [Route("GetFile")]
        public async Task<bool> GetFilesFromApi()
        {
            Log.ApplicationLog(string.Format("GetFile ({0})", ++fileCounter));
            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

                var provider = new MultipartMemoryStreamProvider();
                string uploads = Path.Combine(HttpContext.Current.Server.MapPath("~/Content"), "uploads");
                FolderExtensions.CreateFolder(uploads);
                await Request.Content.ReadAsMultipartAsync(provider);

                foreach (HttpContent file in provider.Contents)
                {
                    string filename = file.Headers.ContentDisposition.FileName.Trim('\"');
                    byte[] buffer = await file.ReadAsByteArrayAsync();
                    string result = System.Text.Encoding.UTF8.GetString(buffer);

                    SystemUsersProvider.SaveTemporerySystemUsers(result, Path.Combine(uploads, filename), fileCounter);
                }

                await Task.Delay(25);
                return true;
            }
            catch (Exception e)
            {
                throw e.LogError();
            }
        }
        [HttpGet]
        [Route("ReDoSendEvent")]
        public IHttpActionResult ReDoSendEvent()
        {
            int index = 0;
            Log.LogLoad("ReDoSendEvent Start");
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {
                    if (!Monitor.TryEnter(reDoSendEvent_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("reDoSendEvent_Lock!");
                    }
                    try
                    {
                        lock (reDoSendEvent_Lock)
                        {

                            try
                            {
                                DateTime start = DateTime.Now;
                                string year = (start.Year).ToString();
                                Log.LogLoad(string.Format("ReDoSendEvent get Year => {0}", year));

                                List<Form101DataExtensionModel> ErroneousForm = formBll.GetErroneousForm101(year);
                                Create101FormAction CreateForm = new Create101FormAction();

                                ErroneousForm.ForEach(f =>
                                {
                                    try
                                    {
                                        Log.LogLoad(string.Format("ReDoSendEvent Start Loop Step {0}, isLast=> {1}", index, ((index + 1) == ErroneousForm.Count)));

                                        CreateForm.ReCreateFull101Form(f, ((index + 1) == ErroneousForm.Count));

                                        Log.LogLoad(string.Format("ReDoSendEvent End Loop Step {0}", index++));
                                    }
                                    catch (Exception IIe)
                                    {
                                        this.ServerError(IIe);
                                    }
                                });
                                AsyncFunctionCaller.RunAsyncAll();

                                Log.LogLoad(new { _start = start, now = DateTime.Now, count = ErroneousForm.Count });

                                return Ok(index);
                            }
                            catch (Exception Ie)
                            {
                                return this.ServerError(Ie);
                            }
                        }
                    }
                    finally
                    {
                        Monitor.Exit(reDoSendEvent_Lock);
                    }
                }
                Log.LogLoad("ReDoSendEvent End");
                return Ok(index);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }

        }
        [HttpGet]
        [Route("SynchronizeUPCBM")]
        public bool SynchronizeUserPayCheckByMail()
        {
            bool s = false;
            Log.PcuLog(HttpContext.Current.Request.Url, "SynchronizeUserPayCheckByMail");
            if (HttpContext.Current.Request.IsLocal)
            {

                if (!Monitor.TryEnter(SynchronizeUserPayCheckByMail_Lock, new TimeSpan(0)))
                {
                    throw new Exception("ServiceBusyException_Locked!");
                }
                try
                {
                    lock (SynchronizeUserPayCheckByMail_Lock)
                    {
                        ///set extended timeout
                        HttpContext.Current.Server.ScriptTimeout = 3600;

                        int index = 0;
                        DateTime NOW = DateTime.Now;
                        Log.PcuLog(string.Format("start import, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds));

                        List<Tlush_MailModel> wsUsers = FormsProvider.GetUsersForEmailPayCheckFromWS(0);

                        Log.PcuLog(new { counter = wsUsers.Count }, "GetUsersForEmailPayCheckFromWS");

                        wsUsers.ForEach(gu =>
                        {
                            Log.PcuLog(gu, "UpdateUserStatus");
                            this.userbll.UpdateUserPayCheckToEmailStatus(gu);
                        });
                        Log.PcuLog(string.Format("end import, wsUsers => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, wsUsers.Count));

                        List<PayCheckUsersByMailModel> siteUsers = setUpBll.SynchronizeUsersPayCheckByMail();
                        siteUsers.ForEach(u =>
                        {
                            try
                            {
                                Log.PcuLog(u, "SynchronizeUPCBMFromSite");

                                AsyncFunctionCaller.RunAsync(HttpContext.Current, () => { FormsProvider.SynchronizeUPCBMFromSite(u); });
                                index++;
                                if (index % 25 == 0)
                                {
                                    Log.PcuLog(string.Format("index => {0}, time => {1}", index, DateTime.Now.Subtract(NOW).TotalSeconds));
                                    AsyncFunctionCaller.RunAsyncAll();
                                }
                            }
                            catch (Exception Ie)
                            {
                                this.ServerError(Ie);
                            }
                        });
                        AsyncFunctionCaller.RunAsyncAll();
                        Log.PcuLog(string.Format("index => {0}, siteUsers => {2}, time => {1}", index, DateTime.Now.Subtract(NOW).TotalSeconds, siteUsers.Count));
                        s = true;
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(SynchronizeUserPayCheckByMail_Lock);
                }
            }
            return s;
        }
        [HttpGet]
        [Route("PreCollectPayChecks")]
        public IHttpActionResult PreCollectPayChecks()
        {
            Log.PcuLog(HttpContext.Current.Request.Url, "PreCollectPayChecks");
            DateTime NOW = DateTime.Now;
            try
            {
                if (!HttpContext.Current.Request.IsLocal)
                    throw new EntryPointNotFoundException();

                if (!Monitor.TryEnter(SynchronizeUserPayCheckByMail_Lock, new TimeSpan(0)))
                    throw new Exception("ServiceBusyException_Locked!");

                try
                {
                    lock (SynchronizeUserPayCheckByMail_Lock)
                    {
                        ///set extended timeout
                        HttpContext.Current.Server.ScriptTimeout = 3600;

                        Log.PcuLog("START ALL!!!", "PreCollectPayChecks");

                        #region AsyncFunctionCaller Set Up
                        AsyncFunctionCaller.UnInit();
                        AsyncFunctionCaller.InIt(16, false);
                        TaskFactory factory = AsyncFunctionCaller.createFactory();
                        #endregion

                        #region parameters                        
                        int steps = 0;
                        MunicipalityUpcbmModel municipalityUpcbmModel;
                        DateTime currentPayMonth;
                        List<USER> ToGetList;
                        List<Task<decimal>> taskList = new List<Task<decimal>>();
                        List<decimal> UPCMlist = new List<decimal>();
                        int errorCounter = 0;
                        decimal res;
                        int TotalToGetList = 0;
                        #endregion

                        #region factory flag
                        Log.PcuLog(string.Format("B-4 DoGlobalPCUE step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, steps++), "PreCollectPayChecks");
                        setUpBll.DoGlobalPCUE(62999);
                        Log.PcuLog(string.Format("After DoGlobalPCUE step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, steps++), "PreCollectPayChecks");
                        #endregion

                        #region get municipalities
                        Log.PcuLog(string.Format("B-4 GetMunicipalitiesForUPCM step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, steps++), "PreCollectPayChecks");
                        List<MunicipalityModel> municipalities = setUpBll.GetMunicipalitiesForUPCM();
                        
                        Log.PcuLog(string.Format("After GetMunicipalitiesForUPCM step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, steps++), "PreCollectPayChecks");
                        #endregion

                        foreach (MunicipalityModel municipality in municipalities)
                        {
                            try
                            {
                                Log.PcuLog(string.Format("start CheckRashutLastPayCheckMonth step {1} time => {0},  municipalityID => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, steps++, municipality.ID), "PreCollectPayChecks");
                                municipalityUpcbmModel = FormsProvider.CheckRashutLastPayCheckMonth(municipality.ID.ToInt());
                                Log.PcuLog(municipalityUpcbmModel, "municipalityUpcbmModel");
                                
                                #region municipality upcbm model success
                                if (municipalityUpcbmModel.success && municipalityUpcbmModel.PayCounter > 0)
                                {
                                    currentPayMonth = DateExtension.maxDateToDate(municipalityUpcbmModel);
                                    
                                    Log.PcuLog(string.Format("B-4 ToGetList step {1} time => {0},  municipalityID => {2},  currentPayMonth => {3}", DateTime.Now.Subtract(NOW).TotalSeconds, steps++, municipality.ID, currentPayMonth.ToString("dd/MM/yyyy")), "PreCollectPayChecks");
                                    ToGetList = setUpBll.GetUpcm_ByMunicipality(municipality.ID, currentPayMonth);
                                    Log.PcuLog(string.Format("After ToGetList step {1} time => {0},  municipalityID => {2},  ToGetList => {3}", DateTime.Now.Subtract(NOW).TotalSeconds, steps++, municipality.ID, ToGetList.Count), "PreCollectPayChecks");

                                    TotalToGetList += ToGetList.Count;

                                    Log.PcuLog(string.Format("B-4 collectPayChecks step {1} time => {0},  municipalityID => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, steps++, municipality.ID), "PreCollectPayChecks");
                                    foreach (Task<decimal> task in collectPayChecks(factory, municipality.ID, currentPayMonth, ToGetList))
                                    {
                                        taskList.Add(task);
                                    }
                                    Log.PcuLog(string.Format("After collectPayChecks step {1} time => {0},  municipalityID => {2},  taskList => {3}", DateTime.Now.Subtract(NOW).TotalSeconds, steps++, municipality.ID, taskList.Count), "PreCollectPayChecks");
                                }
                                #endregion
                            }
                            catch (Exception Iex)
                            {
                                this.ServerError(Iex);
                            }
                        }

                        #region run PCU collection Tasks
                        Log.PcuLog(string.Format("Start WaitAll step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, steps++), "PreCollectPayChecks");
                        while (taskList.Count(t => t.Status == TaskStatus.WaitingToRun) > 0)
                        {
                            Task.WaitAll(taskList.Where(t => !t.IsCompleted && !t.IsCanceled && !t.IsFaulted).Take(10).ToArray());
                            int waiting = taskList.Count(t => t.Status == TaskStatus.WaitingToRun);
                            Log.PcuLog(new { Waiting = waiting, Running = taskList.Count(t => t.Status == TaskStatus.Running), RanToCompletion = taskList.Count(t => t.Status == TaskStatus.RanToCompletion), time = calculateSleepTime(DateTime.Now.Subtract(NOW).TotalMilliseconds) }, "taskList");
                            Thread.Sleep(calculateSleep(waiting));
                        }
                        Log.PcuLog(string.Format("End WaitAll step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, steps++), "PreCollectPayChecks");
                        #endregion

                        #region get PCU Tasks results
                        Log.PcuLog(string.Format("Start Get Task Results step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, steps++), "PreCollectPayChecks");
                        foreach (Task<decimal> task in taskList)
                        {
                            if (task.IsCompleted && !task.IsCanceled && !task.IsFaulted)
                            {
                                res = task.Result;
                                if (res > 0) UPCMlist.Add(res);
                                else
                                    errorCounter++;
                            }
                        }
                        Log.PcuLog(string.Format("End Get Task Results step {1} time => {0},  TotalToGetList => {4},  UPCMlist => {2},  errorCounter => {3}", calculateSleepTime(DateTime.Now.Subtract(NOW).TotalMilliseconds), steps++, UPCMlist.Count, errorCounter, TotalToGetList), "PreCollectPayChecks");
                        #endregion
                    }
                }
                catch (Exception Ie)
                {
                    this.ServerError(Ie);
                    return Ok(false);
                }
                finally
                {
                    #region End AsyncFunctionCaller Set Up
                    AsyncFunctionCaller.UnInit();
                    #endregion

                    Monitor.Exit(SynchronizeUserPayCheckByMail_Lock);
                }
                Log.PcuLog(string.Format("PreCollectPayChecks - END ALL!!! (TRUE)  time => {0}", calculateSleepTime(DateTime.Now.Subtract(NOW).TotalMilliseconds)));
                return Ok(true);

            }
            catch (Exception e)
            {
                this.ServerError(e);
                return Ok(false);
            }
        }
        [HttpGet]
        [Route("SendToRegisterdUsers")]
        public IHttpActionResult SendToRegisterdUsers()
        {
            Log.PcuLog(HttpContext.Current.Request.Url, "SendToRegisterd");
            try
            {
                if (!HttpContext.Current.Request.IsLocal)
                    throw new EntryPointNotFoundException();

                if (!Monitor.TryEnter(SynchronizeUserPayCheckByMail_Lock, new TimeSpan(0)))
                    throw new Exception("ServiceBusyException_Locked!");

                try
                {
                    lock (SynchronizeUserPayCheckByMail_Lock)
                    {
                        ///set extended timeout
                        HttpContext.Current.Server.ScriptTimeout = 3600;

                        #region AsyncFunctionCaller Set Up
                        AsyncFunctionCaller.UnInit();
                        AsyncFunctionCaller.InIt(16, false);
                        TaskFactory factory = AsyncFunctionCaller.createFactory();
                        HttpContext Context = HttpContext.Current;
                        #endregion

                        #region parameters
                        DateTime NOW = DateTime.Now;
                        int steps = 0;
                        List<UpcMailsExtension> UpcMailsList = new List<UpcMailsExtension>();
                        #endregion

                        #region get upc to send
                        Log.PcuLog(string.Format("step {1} time => {0}, Start GetAllUpcMails", DateTime.Now.Subtract(NOW).TotalSeconds, steps++), "SendToRegisterd");
                        UpcMailsList = setUpBll.GetAllUpcMails();
                        Log.PcuLog(string.Format("step {1} time => {0}, End GetAllUpcMails,  UpcMailsList => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, steps++, UpcMailsList.Count), "SendToRegisterd");
                        #endregion

                        #region send mails
                        Log.PcuLog(string.Format("step {1} time => {0}, Start sendUPCemail", DateTime.Now.Subtract(NOW).TotalSeconds, steps++), "SendToRegisterd");
                        sendUPCemail(UpcMailsList, factory, Context);
                        Log.PcuLog(string.Format("step {1} time => {0}, End sendUPCemail", DateTime.Now.Subtract(NOW).TotalSeconds, steps++), "SendToRegisterd");
                        #endregion
                    }
                }
                catch (Exception Ie)
                {
                    this.ServerError(Ie);
                    return Ok(false);
                }
                finally
                {
                    #region End AsyncFunctionCaller Set Up
                    AsyncFunctionCaller.UnInit();
                    #endregion

                    Monitor.Exit(SynchronizeUserPayCheckByMail_Lock);
                }
                return Ok(true);
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return Ok(false);
            }
        }
        [HttpGet]
        [Route("CreateMobileImages")]
        public bool CreateMobileImages()
        {
            bool s = false;
            CreateMobileImage createImage = new CreateMobileImage();
            Log.ApplicationLog(HttpContext.Current.Request.Url, "CreateMobileImages");
            if (HttpContext.Current.Request.IsLocal)
            {

                if (!Monitor.TryEnter(CreateMobileImages_Lock, new TimeSpan(0)))
                {
                    throw new Exception("ServiceBusyException_Locked!");
                }
                try
                {
                    lock (CreateMobileImages_Lock)
                    {
                        ///set extended timeout
                        HttpContext.Current.Server.ScriptTimeout = 3600;

                        DateTime NOW = DateTime.Now;
                        int index = 0;
                        Log.ApplicationLog(string.Format("step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");

                        List<UserImageModel> payChecks = userbll.GetUserImagesForCreateMobileImages(300, "TLUSH");
                        payChecks.AddRange(userbll.GetUserImagesForCreateMobileImages(50, "T106"));

                        Log.ApplicationLog(string.Format("step {1} time => {0}, Images count => {2}, ids => {3}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, payChecks.Count, string.Join(",", payChecks.Select(i => i.ID).ToList())), "CreateMobileImages");

                        foreach (UserImageModel image in payChecks)
                        {
                            Log.ApplicationLog(string.Format("step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");
                            AsyncFunctionCaller.RunAsync(HttpContext.Current, () =>
                            {
                                try
                                {
                                    userbll.CreateUserImage(createImage.CreateImage(image));
                                }
                                catch (Exception Ie)
                                {
                                    this.ServerError(Ie);
                                    userbll.RemoveImageById(image.ID);
                                }
                            });
                            if (index % 50 == 0)
                            {
                                Thread.Sleep(750);
                                Log.ApplicationLog(string.Format("Sleep 750, step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");
                            }
                        }

                        Log.ApplicationLog(string.Format("RunAsyncAll step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");
                        AsyncFunctionCaller.RunAsyncAll();
                        Log.ApplicationLog(string.Format("End RunAsyncAll step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");

                        Log.ApplicationLog(string.Format("EmptyDirectoryEraser step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");
                        createImage.EmptyDirectoryEraser();
                        Log.ApplicationLog(string.Format("End EmptyDirectoryEraser step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");

                        Log.ApplicationLog(string.Format("CleanRuningProcesses step {0} time => {1}", index++, DateTime.Now.Subtract(NOW).TotalSeconds), "CreateMobileImages");
                        createImage.CleanRuningProcesses();
                        Log.ApplicationLog(string.Format("End CleanRuningProcesses step {0} time => {1}", index++, DateTime.Now.Subtract(NOW).TotalSeconds), "CreateMobileImages");
                        createImage.CleanUploadsFolder();

                        Log.ApplicationLog(string.Format("last step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");
                        s = true;
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(CreateMobileImages_Lock);
                }
            }
            return s;
        }
        [HttpGet]
        [Route("CleanRuningProcesses")]
        public bool CleanRuningProcesses()
        {
            bool s = false;
            CreateMobileImage createImage = new CreateMobileImage();
            Log.ApplicationLog(HttpContext.Current.Request.Url, "CleanRuningProcesses");
            if (HttpContext.Current.Request.IsLocal)
            {

                if (!Monitor.TryEnter(CreateMobileImages_Lock, new TimeSpan(0)))
                {
                    throw new Exception("ServiceBusyException_Locked!");
                }
                try
                {
                    lock (CreateMobileImages_Lock)
                    {
                        DateTime NOW = DateTime.Now;
                        int index = 0;
                        Log.ApplicationLog(string.Format("step {0} time => {1}", index++, DateTime.Now.Subtract(NOW).TotalSeconds), "CleanRuningProcesses");
                        createImage.CleanRuningProcesses();
                        Log.ApplicationLog(string.Format("step {0} time => {1}", index++, DateTime.Now.Subtract(NOW).TotalSeconds), "CleanRuningProcesses");
                        createImage.CleanUploadsFolder();
                        Log.ApplicationLog(string.Format("step {0} time => {1}", index++, DateTime.Now.Subtract(NOW).TotalSeconds), "CleanRuningProcesses");

                        s = true;
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(CreateMobileImages_Lock);
                }
            }
            return s;
        }
        [HttpGet]
        [Route("UpdateMunicipalityStatus")]
        public bool UpdateMunicipalityStatus()
        {
            bool s = false;
            Log.ApplicationLog(HttpContext.Current.Request.Url, "UpdateMunicipalityStatus");
            if (HttpContext.Current.Request.IsLocal)
            {

                if (!Monitor.TryEnter(rashutNumberImport_Lock, new TimeSpan(0)))
                {
                    throw new Exception("rashutNumberImport_Lock!");
                }
                try
                {
                    lock (rashutNumberImport_Lock)
                    {
                        List<MunicipalitySchemaExtension> mun = FormsProvider.GetMunicipalitySchema();
                        s = setUpBll.UpdateMunicipalities(mun);
                        registerBll.ConnectEmailsByMunicipality();
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(rashutNumberImport_Lock);
                }
            }
            Log.ApplicationLog("END UpdateMunicipalityStatus");
            return s;
        }
        [HttpGet]
        [Route("FireStoredProceduresForProcessesDinamicGroups")]
        public bool FireStoredProceduresForProcessesDinamicGroups()
        {
            bool ans = false;
            Log.ApplicationLog(HttpContext.Current.Request.Url, "FireStoredProceduresForProcessesDinamicGroups");
            if (HttpContext.Current.Request.IsLocal)
            {
                if (!Monitor.TryEnter(ProcessesStoredProcedures_Lock, new TimeSpan(0)))
                {
                    throw new Exception("ProcessesStoredProcedures_Lock!");
                }
                try
                {
                    lock (ProcessesStoredProcedures_Lock)
                    {
                        RegisterBll bll = new RegisterBll();
                        List<int> municipalities = bll.GetAllmunicipalities(true);
                        string res = SystemUsersProvider.fireDinamicGroupsProcedures(municipalities);
                        if (res != null)
                            ans = true;
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(ProcessesStoredProcedures_Lock);
                }
            }
            return ans;
        }
        [HttpGet]
        [Route("BirthdayGreetings/{day?}")]
        public bool BirthdayGreetings(string day = null)
        {
            bool ans = false;
            Log.ApplicationLog(HttpContext.Current.Request.Url, "BirthdayGreetings");

            if (HttpContext.Current.Request.IsLocal)
            {
                if (!Monitor.TryEnter(BirthdayGreetings_Lock, new TimeSpan(0)))
                {
                    throw new Exception("BirthdayGreetings_Lock!");
                }
                try
                {
                    lock (BirthdayGreetings_Lock)
                    {
                        FolderExtensions.CleanDirectory(Path.Combine(HttpContext.Current.Server.MapPath("~/Content"), "B_D"));

                        List<UserModel> users = string.IsNullOrWhiteSpace(day) ? this.registerBll.GetUsersBirthdays() : this.registerBll.GetUsersBirthdays(day.ToInt());
                        Log.ApplicationLog(new { count = users.Count, day = string.IsNullOrWhiteSpace(day) ? "NULL" : day, ids = string.Join(",", users.Select(u => u.ID).ToList()) }, "BirthdayGreetings");

                        CreateMobileImage sm = new CreateMobileImage();
                        users.ForEach(u =>
                            {
                                try
                                {
                                    if (!sm.SendBirthdayGreetingEmail(u))
                                        throw new Exception("SendBirthdayGreetingEmail_Exception");
                                }
                                catch (Exception Ie)
                                {
                                    this.ServerError(Ie);
                                    ans = false;
                                }
                            }
                        );

                        Thread.Sleep(175);
                        FolderExtensions.CleanDirectory(Path.Combine(HttpContext.Current.Server.MapPath("~/Content"), "B_D"));

                        ans = true;
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(BirthdayGreetings_Lock);
                }
            }
            return ans;
        }
        [HttpGet]
        [Route("DistinctData")]
        public bool DistinctData()
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "DistinctData");

            if (HttpContext.Current.Request.IsLocal)
            {
                if (!Monitor.TryEnter(InternalBulkRegister_Lock, new TimeSpan(0)))
                {
                    throw new Exception("DistinctData_Lock!");
                }
                try
                {
                    lock (InternalBulkRegister_Lock)
                    {
                        DistinctContactData();
                        return true;
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(InternalBulkRegister_Lock);
                }
            }
            return false;
        }

        #region private
        private Task<decimal> collectPayCheck(TaskFactory factory, decimal municipalityID, DateTime currentPayMonth, USER user)
        {
            HttpContext context = HttpContext.Current;
            CancellationTokenSource cts = new CancellationTokenSource(AsyncFunctionCaller.timeout);
            TaskCompletionSource<decimal> taskCompletionSource = new TaskCompletionSource<decimal>();
            Task<decimal> task = null;

            task = factory.StartNew<decimal>(() =>
            {
                HttpContext.Current = context;
                try
                {
                    using (cts.Token.Register(() => taskCompletionSource.TrySetCanceled(cts.Token)))
                    {
                        try
                        {
                            int? factoryId = null;
                            try
                            {
                                factoryId = user.USERSTOFACTORIES.First(f => f.MUNICIPALITYID == municipalityID).FACTORYID.ToInt();
                            }
                            catch { }

                            UserImageModel img = FormsProvider.GetPaycheck(user.IDNUMBER, user.CALCULATEDIDNUMBER.ToInt(), municipalityID.ToInt(), currentPayMonth, factoryId);
                            UPC_MAILS upc = new UPC_MAILS
                            {
                                CALCULATEDIDNUMBER = user.CALCULATEDIDNUMBER,
                                CONFIRMATIONTOKEN = user.MEMBERSHIP.CONFIRMATIONTOKEN,
                                CURRENTPAYMONTH = currentPayMonth,
                                EMAIL = string.Join(";", user.EMAILBYMUNICIPALITies.Where(em => em.MUNICIPALITYID == municipalityID).Select(em => em.EMAIL).ToList()),
                                FULLNAME = string.Format("{0} {1}", user.FIRSTNAME, user.LASTNAME),
                                IDNUMBER = user.IDNUMBER,
                                MUNICIPALITYID = municipalityID,
                                USERID = user.ID,
                                USERIMAGEID = img.ID,
                                CREATEDDATE = DateTime.Now,
                                ACTIVE = true
                            };
                            setUpBll.SaveUpcMail(upc);
                            return upc.ID;
                        }
                        catch { }
                        return -1;
                    }
                }
                catch (ThreadAbortException tae)
                {
                    if (cts.IsCancellationRequested && cts.Token.IsCancellationRequested)
                    {
                        throw new TaskCanceledException(task);
                    }
                    tae.LogError();
                    return -1;
                }
                catch (TaskCanceledException tce)
                {
                    if (cts.IsCancellationRequested && cts.Token.IsCancellationRequested)
                    {
                        throw new TaskCanceledException(task);
                    }
                    tce.LogError();
                    return -1;
                }
                catch (OperationCanceledException oce)
                {
                    if (cts.IsCancellationRequested && cts.Token.IsCancellationRequested)
                    {
                        throw new TaskCanceledException(task);
                    }
                    oce.LogError();
                    return -1;
                }
                catch (Exception ex)
                {
                    if (cts.IsCancellationRequested && cts.Token.IsCancellationRequested)
                    {
                        throw new TaskCanceledException(task);
                    }
                    cts.Token.ThrowIfCancellationRequested();
                    cts.Cancel(true);

                    ex.LogError();
                    return -1;
                }
            }, cts.Token);
            return task;
        }
        private IEnumerable<Task<decimal>> collectPayChecks(TaskFactory factory, decimal municipalityID, DateTime currentPayMonth, List<USER> users)
        {
            foreach (USER user in users.Distinct<USER>())
            {
                yield return collectPayCheck(factory, municipalityID, currentPayMonth, user);
            }
        }
        private byte[] create101forms(int userId, int municipalityId, int formYear)
        {
            try
            {
                int i = 0;
                Log.PcuLog(string.Format("create101forms {0}", i++));
                Create101FormAction CreateForm = new Create101FormAction();
                TaxFromModel taxFromModel = CreateForm.GetBaseFormData(userId, municipalityId, formYear);
                Log.PcuLog(string.Format("create101forms {0}", i++));
                byte[] form101 = CreateForm.CreateForm101(taxFromModel);
                Log.PcuLog(string.Format("create101forms {0}", i++));

                return form101;
            }
            catch (Exception e)
            {
                this.ServerError(e);
                throw e;
            }
        }
        private bool mailSentAlready(List<int> sentMailProcesses, int tmProcessId)
        {
            if (sentMailProcesses.Count < 1)
            {
                return false;
            }
            foreach (int tmId in sentMailProcesses)
            {
                if (tmId == tmProcessId)
                {
                    return true;
                }
            }
            return false;
        }
        private void sendProcessCompletedEmails(ProcessModel processModel, UpdateProcessModel process)
        {
            string template = string.Empty;
            string ans = process.processStatus == 2 ? "בקשתך אושרה סופית!" : "הבקשה אושרה זמנית, והועברה להמשך טיפול הגורמים הרלונטיים";//2 completed in java

            MailDataModel data = new MailDataModel
            {
                userid = processModel.USERID,
                target_userid = processModel.TARGET_USERID,
                user_name = processModel.OVED_NESU_BAKASHA != null && processModel.OVED_NESU_BAKASHA > 0 ? processModel.OVED_NESU_BAKASHA_NAME : processModel.USERMODEL.firstName + " " + processModel.USERMODEL.lastName,
                target_user_name = processModel.OVED_NESU_BAKASHA != null && processModel.OVED_NESU_BAKASHA > 0 ? processModel.OVED_NESU_BAKASHA_NAME : processModel.TARGET_USER != null ? processModel.TARGET_USER.firstName + " " + processModel.TARGET_USER.lastName : getTargetUsername(processModel.TARGET_USERID),
                by_user_name = processModel.TARGET_USER != null ? processModel.TARGET_USER.firstName + " " + processModel.TARGET_USER.lastName : getTargetUsername(processModel.TARGET_USERID),
                tahalich_name = processModel.PROCESS_DESCRIPTION,
                from_date = processModel.FROM_DATE,
                to_date = processModel.TO_DATE,
                description = processModel.PROCESS_TYPE != 3 ? processModel.CONTENT1 : processModel.CONTENT4,//סיבת בקשת חופשה או תוכנית קורס
                answer = ans,
                reason = string.IsNullOrEmpty(processModel.COMMENTS) ? "ללא סיבה" : processModel.COMMENTS
            };

            Log.FormLog(data, "MailDataModel");
            RouteData routedata = HttpContext.Current.Request.RequestContext.RouteData;
            template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "ProcessAnswer", data);

            string theEmail = processModel.OVED_NESU_BAKASHA != null && processModel.OVED_NESU_BAKASHA > 0 ? getOvedNesuEmail(processModel.OVED_NESU_BAKASHA) : processModel.USERMODEL.email;

            // send the mail
            ms.Send(theEmail/*"itayi@ladpc.co.il"*/, template, false, "tikoved.co.il תשובה סופית לבקשה", null, "", "");
        }
        private void DistinctContactData()
        {
            try
            {
                DateTime NOW = DateTime.Now;
                Log.ApplicationLog(string.Format("start distinct user cells function ,time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds));
                setUpBll.DistinctUserCells(NOW);
                Log.ApplicationLog(string.Format("end distinct user cells function ,time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds));
                NOW = DateTime.Now;
                Log.ApplicationLog(string.Format("start distinct user emails function ,time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds));
                setUpBll.DistinctUserEmails(NOW);
                Log.ApplicationLog(string.Format("end distinct user emails function ,time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds));
            }
            catch (Exception e)
            {
                this.ServerError(e);
                throw e;
            }
        }
        private void collectPayChecks(List<UserImageModel> payChecks, List<PayCheckUsersByMailModel> siteUsers, DateTime currentPayMonth, int municipality, bool factoryUsers)
        {
            List<PayCheckUsersByMailModel> lUsers = factoryUsers ? setUpBll.GetUsersByFactoryPC(currentPayMonth, municipality) : setUpBll.GetUsersToSendMail(currentPayMonth, municipality);
            Log.PcuLog(string.Format("PayCheckUsersByMailModel=>, count => {1},  \n users to send => {0}", string.Join(",", lUsers.Select(iu => iu.userId).ToList()), lUsers.Count));
            if (lUsers.Count == 0)
                return;
            siteUsers.AddRange(lUsers);

            AsyncFunctionCaller.RunAsync(HttpContext.Current, () =>
            {
                ///first step => get all the pay-checks into the db....
                foreach (PayCheckUsersByMailModel pcu in lUsers)
                {
                    try
                    {
                        payChecks.Add(FormsProvider.GetPaycheck(pcu.user.idNumber, pcu.user.calculatedIdNumber.ToInt(), municipality, currentPayMonth));
                    }
                    catch (Exception e)
                    {
                        this.ServerError(e);
                    }
                }
            }, true);

            AsyncFunctionCaller.RunAsyncAll();
        }
        private void sendUPCemail(List<UpcMailsExtension> UpcMailsList, TaskFactory factory, HttpContext Context)
        {
            CreateMobileImage createImage = new CreateMobileImage();
            List<Task<decimal>> tasks = new List<Task<decimal>>();
            MailSender ms = new MailSender();
            string View = string.Empty;
            List<decimal> results = new List<decimal>();
            decimal res;
            int index = 0;
            int Sleep = 0;
            int WaitingToRun = 0;

            UpcMailsList.ForEach(p =>
            {
                try
                {
                    View = createImage.RenderPayCheckEmailView(p);
                    if(!string.IsNullOrWhiteSpace(View))
                    {
                        tasks.Add(createImage.sendUpcMail(factory, Context, p, View));
                        if (++index > this.chunkSize)
                        {
                            WaitingToRun = tasks.Count(T => T.Status == TaskStatus.WaitingToRun);
                            Sleep = calculateSleep(WaitingToRun);
                            Log.PcuLog(string.Format("ForEach index => {0}, Sleep => {1}, Sleep Time => {2}, RanToCompletion => {3}, WaitingToRun => {4}", index, Sleep, calculateSleepTime(Sleep), tasks.Count(T => T.Status == TaskStatus.RanToCompletion), WaitingToRun), "sendUPCemail");
                            Thread.Sleep(Sleep);
                            index = 0;
                        }
                    }
                    else
                    {
                        Log.LogError(string.Format("RenderPayCheckEmailView Error Userid => {0}, UPC_MAILS.ID =>{1}", p.USERID, p.ID), "sendUPCemail");
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                }
            });
            
            tasks.ForEach(t =>
            {
                try
                {
                    res = t.Result;
                    if (res > 0)
                    {
                        results.Add(res);
                    }
                }
                catch (Exception ex)
                {
                    ex.LogError(t);
                }
            });
            setUpBll.UpdatePcueSendArchive(UpcMailsList.Where(upc => results.Contains(upc.ID)).ToList());
            Log.PcuLog(string.Format("OK Results => {0}, Total To Send => {1}", results.Count, UpcMailsList.Count), "sendUPCemail");
        }
        private void sendUPCemail(List<UserImageModel> payChecks, List<PayCheckUsersByMailModel> siteUsers, DateTime currentPayMonth, bool factoryUsers)
        {
            CreateMobileImage createImage = new CreateMobileImage();
            payChecks.ForEach(p =>
            {
                try
                {
                    PayCheckUsersByMailModel pcu = siteUsers.First(u => u.userId == p.userId);
                    //if (currentPayMonth.Month == 12 || currentPayMonth.Month == 1)
                    //{
                    //    int Year = (currentPayMonth.Month == 1 ? currentPayMonth.Year : (currentPayMonth.Year + 1));
                    //    Log.PcuLog(new { userId = p.userId, municipalityId = pcu.municipalityId, Year = Year });

                    //    if (createImage.SendPayCheckEmail(p, pcu, create101forms(p.userId, pcu.municipalityId, Year), Year))
                    //    {
                    //        pcu.lastSendDate = new DateTime(currentPayMonth.Year, currentPayMonth.Month, 1);
                    //        pcu.lastSendMonth = currentPayMonth.Month;
                    //        pcu.lastSendYear = currentPayMonth.Year;
                    //        if (!this.isDebugeMode)
                    //        {
                    //            if (factoryUsers)
                    //                setUpBll.UpdateFactoryPCU(pcu);
                    //            else
                    //                setUpBll.UpdatePCU(pcu);
                    //        }
                    //    }
                    //}
                    //else 
                    if (createImage.SendPayCheckEmail(p, pcu))
                    {
                        pcu.lastSendDate = new DateTime(currentPayMonth.Year, currentPayMonth.Month, 1);
                        pcu.lastSendMonth = currentPayMonth.Month;
                        pcu.lastSendYear = currentPayMonth.Year;
                        if (!this.isDebugeMode)
                        {
                            if (factoryUsers)
                                setUpBll.UpdateFactoryPCU(pcu);
                            else
                                setUpBll.UpdatePCU(pcu);
                        }
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                }
            });
        }
        private void registerByMunicipality(int municipalityId)
        {
            //List<SystemUserModel> users = registerBll.GetBulkUsersToInternalRegister(municipalityId);
            List<SystemUserModel> users = registerBll.GetBulkUsersToRegister(municipalityId, this.TerminationCauseExitList);

            if (users.Count <= 0)
            {
                Log.LogLoad(new { municipalityId = municipalityId, usersCount = users.Count }, "counter id 0");
                return;
            }

            Log.LogLoad(new { municipalityId = municipalityId, usersCount = users.Count }, "start register all users");
            List<WebSecurityModel> securityModels = new List<WebSecurityModel>();

            foreach (IEnumerable<SystemUserModel> sysusers in users.Chunk(this.chunkSize))
            {
                foreach (SystemUserModel su in sysusers)
                {
                    try
                    {
                        AsyncFunctionCaller.RunAsync(() =>
                        {
                            securityModels.Add(WebSecurity.InternalRegisterFromSystem(
                                new RegisterUserModel()
                                {
                                    idNumber = su.fullIdNumber,
                                    calculatedIdNumber = su.idNumber,
                                    cell = su.cellNumber,
                                    password = su.initialPassword,
                                    initialPassword = su.initialPassword,
                                    municipality = su.municipality,
                                    email = su.email
                                }
                            ));
                        }, true);
                    }
                    catch (Exception e)
                    {
                        this.ServerError(e);
                    }
                }
                AsyncFunctionCaller.RunAsyncAll();
                Log.LogLoad(new { municipalityId = municipalityId, sysUsersCount = sysusers.Count() }, "End register Chunk");
            }
            Log.LogLoad(new { municipalityId = municipalityId, success = securityModels.Where(s => s.success).Count(), error = securityModels.Where(s => !s.success).Count() }, "End register all users");
        }
        private void reArrangeProcessesData(List<PackedProcessModel> data, List<TahalichModel> tahalichim, List<StepModel> shlavim, List<ReshimatTiyugModel> reshimotTiyug, List<MismachBereshimaModel> mismachimBeReshima, List<MismachModel> mismachim, List<KvuzatMishtamshimModel> kvuzotMishtamshim, List<MishtamsheiKvuzaModel> mishtamsheiKvuza)
        {

            for (var i = 0; i < data.Count(); i++)
            {
                if (!isExist(tahalichim, data[i].process))
                {
                    tahalichim.Add(data[i].process);
                }
                if (!isExist(shlavim, data[i].step))
                {
                    shlavim.Add(data[i].step);
                }
                if (!isExist(kvuzotMishtamshim, data[i].kvuzatMishtamshim))
                {
                    kvuzotMishtamshim.Add(data[i].kvuzatMishtamshim);
                }
                if (!isExist(mishtamsheiKvuza, data[i].mishtamsheyKvuza))
                {
                    mishtamsheiKvuza.Add(data[i].mishtamsheyKvuza);
                }
                if (data[i].reshimatTiyug != null)
                    if (!isExist(reshimotTiyug, data[i].reshimatTiyug))
                    {
                        reshimotTiyug.Add(data[i].reshimatTiyug);
                    }
                if (data[i].mismachBeReshima != null)
                    if (!isExist(mismachimBeReshima, data[i].mismachBeReshima))
                    {
                        mismachimBeReshima.Add(data[i].mismachBeReshima);
                    }
                if (data[i].mismach != null)
                    if (!isExist(mismachim, data[i].mismach))
                    {
                        mismachim.Add(data[i].mismach);
                    }
            }
        }
        private bool isExist(List<TahalichModel> listTahalichim, TahalichModel tahalich)
        {
            for (var i = 0; i < listTahalichim.Count(); i++)
            {
                if (listTahalichim[i].recId == tahalich.recId)
                {
                    return true;
                }
            }
            return false;
        }
        private bool isExist(List<StepModel> listShlavim, StepModel shalav)
        {
            for (var i = 0; i < listShlavim.Count(); i++)
            {
                if (listShlavim[i].recId == shalav.recId)
                {
                    return true;
                }
            }
            return false;
        }
        private bool isExist(List<ReshimatTiyugModel> listReshimot, ReshimatTiyugModel reshima)
        {
            for (var i = 0; i < listReshimot.Count(); i++)
            {
                if (listReshimot[i].recId == reshima.recId)
                {
                    return true;
                }
            }
            return false;
        }
        private bool isExist(List<MismachBereshimaModel> listMismachimBereshima, MismachBereshimaModel mismachBereshima)
        {
            for (var i = 0; i < listMismachimBereshima.Count(); i++)
            {
                if (listMismachimBereshima[i].recId == mismachBereshima.recId)
                {
                    return true;
                }
            }
            return false;
        }
        private bool isExist(List<MismachModel> listMismachim, MismachModel mismach)
        {
            for (var i = 0; i < listMismachim.Count(); i++)
            {
                if (listMismachim[i].recId == mismach.recId)
                {
                    return true;
                }
            }
            return false;
        }
        private bool isExist(List<KvuzatMishtamshimModel> listKvuzatMishtamshim, KvuzatMishtamshimModel kvuzatMishtamshim)
        {
            for (var i = 0; i < listKvuzatMishtamshim.Count(); i++)
            {
                if (listKvuzatMishtamshim[i].recId == kvuzatMishtamshim.recId)
                {
                    return true;
                }
            }
            return false;
        }
        private bool isExist(List<MishtamsheiKvuzaModel> listMishtamsheyKvuza, MishtamsheiKvuzaModel mishtameshkvuza)
        {
            for (var i = 0; i < listMishtamsheyKvuza.Count(); i++)
            {
                if (listMishtamsheyKvuza[i].recId == mishtameshkvuza.recId)
                {
                    return true;
                }
            }
            return false;
        }
        private string getOvedNesuEmail(decimal? ovedId)
        {
            if (ovedId != null)
            {
                return processBll.getEmployeeEmail(ovedId);
            }
            return string.Empty;
        }
        private string getTargetUsername(decimal userId)
        {
            return processBll.getTargetUsername(userId);
        }
        private int calculateSleep(int waitingCounter)
        {
            if (waitingCounter < 100)
                return 0;
            double Sleep = Math.Ceiling((double)(waitingCounter / 7 * 777));

            if (Sleep > 120000) Sleep = 120000;
            if (Sleep < 10000)
                return 0;
            return (int)Sleep;
        }
        private string calculateSleepTime(double sleep)
        {
            double Minutes = ConvertMillisecondsToMinutes(sleep);
            double Seconds = 0;

            if (Minutes > 1)
            {
                Minutes = Math.Floor(Minutes);
                sleep -= ConvertMinutesToMilliseconds(Minutes);
            }
            Seconds = Math.Floor(ConvertMillisecondsToSeconds(sleep));
            sleep -= ConvertSecondsToMilliseconds(Seconds);

            return string.Format("00:{0}:{1}.{2}", pad(Minutes > 1 ? Minutes : 0), pad(Seconds), pad(sleep));
        }
        private string pad(double val)
        {
            return val > 9 ? val.ToString() : string.Format("0{0}", val);
        }
        #endregion

        #region transform Milliseconds
        public double ConvertMillisecondsToMinutes(double milliseconds)
        {
            return TimeSpan.FromMilliseconds(milliseconds).TotalMinutes;
        }
        public static double ConvertMillisecondsToSeconds(double milliseconds)
        {
            return TimeSpan.FromMilliseconds(milliseconds).TotalSeconds;
        }
        public static double ConvertSecondsToMilliseconds(double seconds)
        {
            return TimeSpan.FromSeconds(seconds).TotalMilliseconds;
        }
        public static double ConvertMinutesToMilliseconds(double minutes)
        {
            return TimeSpan.FromMinutes(minutes).TotalMilliseconds;
        }
        #endregion
    }
}