﻿using System;
using System.Web.Http;

using WorkerPortfolio.App_Start;
using DataAccess.Models;
using DataAccess.BLLs;
using DataAccess.Mappers;
using DataAccess.Extensions;
using CustomMembership;
using Logger;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using WebServiceProvider;

namespace WorkerPortfolio.Controllers
{
    [RoutePrefix("api/AnonymousAccount")]
    [InitializeSimpleMembership]
    public class AnonymousAccountController : ApiController
    {
        #region private vars
        private UserBll userbll;
        private RegisterBll registerBll;
        #endregion
        public AnonymousAccountController()
        {
            this.userbll = new UserBll();
            this.registerBll = new RegisterBll();
        }

        [HttpPut]
        public IHttpActionResult CreateUser(CreateUserModel model)
        {
            try
            {
                WebSecurityModel securityModel = WebSecurity.CreateUserAndAccount(model, true);
                return Ok(securityModel);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("{idNumber}/{password}")]
        public IHttpActionResult login(string idNumber, string password)
        {
            try
            {
                WebSecurityModel model = WebSecurity.Login(new CryptoModel { idNumber = idNumber, password = password });

                return Ok(model);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        /// <summary>
        /// new action to log in from kad mail
        /// </summary>
        /// <param name="idNumber"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ConfirmKadAccount/{idNumber}")]
        public IHttpActionResult ConfirmKadAccount(string idNumber)
        {
            try
            {
                idNumber = IdNumberExtender.CheckAndGenerateFullIdNumber(IdNumberExtender.GenerateCalculatedIdNumber(idNumber));
                string passwordVerificationToken = WebSecurity.GeneratePasswordResetToken(idNumber);

                if (string.IsNullOrWhiteSpace(passwordVerificationToken))
                    throw new MembershipException("NullUserReferenceException", 102);

                return Ok(passwordVerificationToken);
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return Redirect("/");
            }
        }
        [HttpPost]
        [Route("login")]
        public IHttpActionResult login(CryptoModel cryptoModel)
        {
            try
            {
                WebSecurityModel model = WebSecurity.Login(cryptoModel);
                return Ok(model);
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(CryptographicUnexpectedOperationException) || e.GetType() == typeof(CryptographicException))
                {
                    return Content((HttpStatusCode)511, e);
                }
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("LogOut")]
        public IHttpActionResult LogOut()
        {
            try
            {
                WebSecurity.Logout();
                return Ok(true);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("{idNumber}/{mail}/{sms}")]
        public IHttpActionResult UserResetPassword(string idNumber, bool mail, bool sms)
        {
            try
            {
                WebSecurityModel model = WebSecurity.UserResetPassword(idNumber, mail, sms);
                return Ok(model);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("ChangePasswordFromToken")]
        public IHttpActionResult ChangePasswordFromToken(PostTokenModel model)
        {
            try
            {
                WebSecurityModel securityModel = new WebSecurityModel();
                ///passwordResetType = 1 -> mail
                if (model.passwordResetType == 1)
                {
                    securityModel = WebSecurity.ResetPasswordFromToken(model.Token, model.newPassword, true);
                }
                ///passwordResetType = 2 -> sms
                if (model.passwordResetType == 2)
                {
                    securityModel = WebSecurity.ResetPasswordFromSmsCode(model);
                }

                return Ok(securityModel);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("GetLocalPasswordSettings")]
        public IHttpActionResult GetLocalPasswordSettings()
        {
            try
            {
                return Ok(WebSecurity.PasswordSettings);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("{authenticationToken}")]
        public IHttpActionResult Post(string authenticationToken)
        {
            try
            {
                UserCredentialsModel Credentials = new UserCredentialsModel();
                UserModel usermodel = userbll.GetUserByConfirmationToken(authenticationToken, true);
                if (usermodel.ID == WebSecurity.CurrentUserId)
                {
                    Credentials = usermodel.MapCredentials();
                }

                return Ok(Credentials);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("Kad/{authenticationToken}/{rashut}/{mncplity}")]
        public IHttpActionResult Kad(string authenticationToken, int rashut, int mncplity)
        {
            try
            {
                UserCredentialsModel Credentials = new UserCredentialsModel();
                UserModel usermodel = userbll.GetUserByConfirmationToken(authenticationToken, true);

                if (usermodel != null)
                {
                    Credentials = usermodel.MapCredentialsWithoutRashut(rashut, mncplity);
                }

                return Ok(Credentials);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("{authenticationToken}")]
        public IHttpActionResult Get(string authenticationToken)
        {
            try
            {
                UserModel usermodel = WebSecurity.getUserFromConfirmationToken(authenticationToken);
                return Ok(usermodel);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("{authenticationToken}/{municipalityId}")]
        public IHttpActionResult Get(string authenticationToken, int municipalityId)
        {
            try
            {
                WebSecurityModel SecurityModel = WebSecurity.LoginFromToken(authenticationToken, municipalityId);
                return Ok(SecurityModel);
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(CryptographicUnexpectedOperationException) || e.GetType() == typeof(CryptographicException))
                {
                    return Content((HttpStatusCode)511, e);
                }
                return this.ServerError(e);
            }
        }
        [HttpPut]
        [Route("RegisterFromInitialPassword")]
        public IHttpActionResult RegisterFromInitialPassword(RegisterUserModel model)
        {
            try
            {
                WebSecurityModel securityModel = WebSecurity.RegisterFromInitialPassword(model);
                return Ok(securityModel);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPut]
        [Route("RegisterFromSms")]
        public IHttpActionResult RegisterFromSms(RegisterUserModel model)
        {
            try
            {
                WebSecurityModel securityModel = WebSecurity.RegisterFromSms(model);
                return Ok(securityModel);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPut]
        [Route("RegisterFromEmail")]
        public IHttpActionResult RegisterFromEmail(RegisterUserModel model)
        {
            try
            {
                WebSecurityModel securityModel = WebSecurity.RegisterFromEmail(model);
                return Ok(securityModel);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("VerifySmsToken")]
        public IHttpActionResult VerifySmsToken(RegisterUserModel model)
        {
            try
            {
                WebSecurityModel securityModel = WebSecurity.VerifyAccountFromSmsToken(model);
                return Ok(securityModel);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPut]
        [Route("CheckRegister")]
        public IHttpActionResult CheckRegister(RegisterUserModel model)
        {
            object obj = new { success = false, exceptionId = -1 };
            try
            {
                List<SystemUserModel> users = registerBll.CheckUserBeforeRegister(model);
                if (!string.IsNullOrWhiteSpace(model.initialPassword) && users.Select(u => u.initialPassword).ToList().Contains(model.initialPassword))
                {///valid initialPassword
                    obj = new { success = true, exceptionId = 0 };
                }
                else if (!string.IsNullOrWhiteSpace(model.cell) && users.Select(u => u.cellNumber).ToList().Contains(model.cell))
                {///valid cellNumber
                    obj = new { success = true, exceptionId = 1 };
                }
                else if (!string.IsNullOrWhiteSpace(model.email) && users.Select(u => u.email).ToList().Contains(model.email))
                {///valid email
                    obj = new { success = true, exceptionId = 2 };
                }
                return Ok(obj);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("CheckInitialActivation")]
        public IHttpActionResult CheckInitialActivation(CryptoModel cryptoModel)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                cryptoModel.DecryptId();

                CreateUserModel user = registerBll.CheckInitialUser(cryptoModel);
                if (user != null && user.idNumber == cryptoModel.idNumber)
                    model = WebSecurity.UserResetPassword(cryptoModel.idNumber, cryptoModel.password.ToInt() == 1, cryptoModel.password.ToInt() == 2);
            }
            catch (MembershipException e)
            {
                model = new WebSecurityModel(e);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
            return Ok(model);
        }
    }
}
