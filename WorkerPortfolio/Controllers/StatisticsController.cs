﻿using CustomMembership;
using DataAccess.BLLs;
using DataAccess.Extensions;
using DataAccess.Models;
using Logger;
using Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebServiceProvider;
using WorkerPortfolio.App_Start;

namespace WorkerPortfolio.Controllers
{
    [Authorize]
    [RoutePrefix("AdminStatistics")]
    [InitializeSimpleMembership]
    public class StatisticsController : ApiController
    {
        private StatisticsBll stBll;
        public StatisticsController()
        {
            this.stBll = new StatisticsBll();
        }

        [HttpPost]
        [Route("MunicipalityFormStatistics")]
        public IHttpActionResult GetMunicipalityFormStatistics(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");

                return Ok(this.stBll.GetFormStatisticsForMunicipality(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("Populations")]
        public IHttpActionResult GetPopulations(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAdminAuthorizetion(model))
                    throw new AccessViolationException("AccessViolationException");

                return Ok(FormsProvider.GetPopulationsList(model.currentRashutId));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("CallPopulation")]
        public IHttpActionResult CallPopulation(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAdminAuthorizetion(model))
                    throw new AccessViolationException("AccessViolationException");
                
                return Ok(stBll.GetPopulationUsers(FormsProvider.CallPopulationWorkers(model.currentRashutId, model.page, model.selectedDate.ToString("yyyyMMdd")), WebSecurity.GetSetCurrentMunicipality()));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("GroupEmailSearch")]
        public IHttpActionResult GroupEmailSearchUsers(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");
                return Ok(stBll.GroupEmailSearch(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("PopulationGroups")]
        public IHttpActionResult CallPopulationGroups(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAdminAuthorizetion(model))
                    throw new AccessViolationException("AccessViolationException");

                return Ok(stBll.GetPopulationGroups());
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPut]
        [Route("SendPopulationEmails")]
        public IHttpActionResult SendPopulationEmails(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");
                TemplateMailer tm = new TemplateMailer();
                tm.Send(model.municipalityId, model.page, model.searchMunicipalitiesId.ToDecimalLIst());
                return Ok(model);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }

        #region private
        private bool validateUser(UserSearchModel model)
        {
            if (!WebSecurity.CheckAdminAuthorizetion(model))
                throw new AccessViolationException("AccessViolationException");

            model.municipalityId = WebSecurity.GetSetCurrentMunicipality();
            model.maxImmunity = WebSecurity.GetMaxImmunity(model.idNumber);
            return true;
        }
        #endregion
    }
}
