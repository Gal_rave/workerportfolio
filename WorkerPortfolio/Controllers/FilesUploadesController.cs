﻿using System;
using System.IO;
using System.Web;
using System.Linq;
using System.Web.Http;
using System.Diagnostics;
using System.Web.Routing;
using System.Configuration;
using System.IO.Compression;
using System.Collections.Generic;

using iTextSharp.text;
using iTextSharp.text.pdf;

using Mail;
using Logger;
using AsyncHelpers;
using SftpConnector;
using DataAccess.BLLs;
using CustomMembership;
using DataAccess.Models;
using WebServiceProvider;
using DataAccess.Mappers;
using DataAccess.Extensions;
using WorkerPortfolio.App_Start;
using DataAccess.Enums;

namespace WorkerPortfolio.Controllers
{
    [RoutePrefix("api/FilesUploades")]
    [InitializeSimpleMembership]
    public class FilesUploadesController : ApiController
    {
        #region private class parameters
        private readonly bool isDebugeMode;
        private readonly string baseContent = "~/Content";
        private readonly string baseFolder = "uploads";
        private readonly string notificationFolder = "notification";
        private readonly string uploads = string.Empty;
        private string pdfFiels = "PDFs";
        private string tempPdfFiels = "Temps";
        private string ghostScriptPath = string.Empty;
        private string ghostScriptViewjpegPath = string.Empty;
        private UserBll userbll;
        private FormBll formbll;
        private static string DefaultSiteEmail
        {
            get
            {
                return CustomMembershipProvider.DefaultSiteEmail;
            }
        }
        private static readonly string smsMsg = @"שלום {0} {1},
טופס 101 ששלחת התקבל בהצלחה,
קוד הגירסה הינו:
{2}";
        #endregion

        public FilesUploadesController()
        {
            uploads = Path.Combine(HttpContext.Current.Server.MapPath(baseContent), baseFolder);
            FolderExtensions.CreateFolder(uploads);

            pdfFiels = Path.Combine(HttpContext.Current.Server.MapPath(baseContent), pdfFiels);

            tempPdfFiels = Path.Combine(pdfFiels, tempPdfFiels);
            FolderExtensions.CreateFolder(tempPdfFiels);

            ghostScriptPath = ConfigurationManager.AppSettings["GhostScriptDllPath"];
            ghostScriptViewjpegPath = ConfigurationManager.AppSettings["GhostScriptViewjpegPath"];
            isDebugeMode = ConfigurationManager.AppSettings["isDebugeMode"].ToBool();

            userbll = new UserBll();
            formbll = new FormBll();
        }

        [HttpPost]
        [Route("NotificationUploader")]
        public IHttpActionResult NotificationUploader()
        {
            try
            {
                string idNumber = HttpContext.Current.Request.Form["idNumber"];
                string filename = HttpContext.Current.Request.Form["filename"];

                string path = Path.Combine(uploads, notificationFolder, idNumber);
                FolderExtensions.CreateFolder(path);
                string returnPath = Path.Combine(baseContent, baseFolder, notificationFolder, idNumber, filename);

                if (string.IsNullOrWhiteSpace(filename) || string.IsNullOrWhiteSpace(filename)) { }

                var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;
                if (file == null) { }

                if (file != null && file.ContentLength > 0)
                {
                    file.SaveAs(Path.Combine(path, filename));
                }

                return Ok(new { success = true, idNumber = idNumber, filename = filename, path = returnPath });
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("PostFile")]
        public IHttpActionResult Post()
        {
            try
            {
                HttpPostedFile file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;
                string userIDNumber = HttpContext.Current.Request.Form["userIDNumber"];

                if (!WebSecurity.IsCurrentUser(userIDNumber))
                {
                    throw new Exception("UserdetailsAccessException");
                }

                string year = HttpContext.Current.Request.Form["year"];
                string newFilename = HttpContext.Current.Request.Form["filename"];
                if (string.IsNullOrWhiteSpace(newFilename) || string.IsNullOrWhiteSpace(userIDNumber))
                    throw new Exception("missing required field data");

                BinaryReader binaryReader = new BinaryReader(file.InputStream);
                byte[] imageBytes = binaryReader.ReadBytes(file.InputStream.Length.ToInt());
                string returnPath = newFilename + Path.GetExtension(file.FileName);
                DateTime date = new DateTime(year.ToInt(), 1, 1);

                ///commpress PDF file
                if (Path.GetExtension(file.FileName).ToLower().Contains("pdf"))
                {
                    newFilename += ".pdf";
                }

                ///convert all image files into pdf
                string[] imageTypes = { ".jpg", ".png", ".gif", ".tif", ".jpeg" };
                if (imageTypes.Contains(Path.GetExtension(file.FileName).ToLower()))
                {
                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(imageBytes);
                    string nfName = string.Format("{0}.{1}", Path.GetFileNameWithoutExtension(file.FileName.Replace(" ", "").Trim()), "pdf");
                    using (FileStream fs = new FileStream(Path.Combine(tempPdfFiels, nfName), FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        using (Document doc = new Document(PageSize.A4))
                        {
                            using (PdfWriter writer = PdfWriter.GetInstance(doc, fs))
                            {
                                doc.Open();
                                img.ScaleToFit(doc.PageSize);
                                img.SetAbsolutePosition(0, 0);

                                doc.Add(img);
                                doc.NewPage();
                                doc.Close();
                            }
                        }
                    }
                    imageBytes = FolderExtensions.ReadFile(Path.Combine(tempPdfFiels, nfName));
                    FolderExtensions.DeleteFile(Path.Combine(tempPdfFiels, nfName));
                    newFilename += ".pdf";
                }

                if (!Path.GetExtension(newFilename).ToLower().Contains("pdf"))
                {
                    newFilename += Path.GetExtension(file.FileName);
                }

                UserImageModel image = new UserImageModel
                {
                    fileBlob = imageBytes,
                    fileDate = date,
                    fileName = newFilename,
                    municipalityId = WebSecurity.GetSetCurrentMunicipality(),
                    type = "FORM101_UPLOAD",
                    userId = WebSecurity.CurrentUserId
                };

                image = userbll.CreateUpdateUserImage(image);

                AsyncFunctionCaller.RunAsync(HttpContext.Current, () =>
                {
                    compresSavedFile(imageBytes, newFilename, image);
                }, false);

                return Ok(new { success = true, FileName = file.FileName, path = returnPath, imageId = image.ID });
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [Route("SaveBase64")]
        [HttpPost]
        public IHttpActionResult SaveBase64(SignatureModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model.idNumber, model.Token))
                    throw new FieldAccessException("UserdetailsAccessException");

                byte[] imageBytes = Convert.FromBase64String(model.base64.Replace("data:image/png;base64,", ""));
                UserImageModel image = saveIntoDB(model.idNumber, imageBytes, "FORM101_SIGNATURE", model.municipalityId, model.year.ToInt());
                model.saved = true;
                model.image = image;
                return Ok(model);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("{file}")]
        public IHttpActionResult DeleteFile(string file)
        {
            try
            {
                var server = HttpContext.Current.Server;
                if (string.IsNullOrWhiteSpace(file))
                    throw new NullReferenceException("EmptyFileException");
                string[] files = file.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (!files.Any())
                    throw new NullReferenceException("EmptyFileException");
                foreach (string f in files)
                {
                    FolderExtensions.DeleteFile(server.MapPath(f));
                }

                return Ok(file);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPut]
        [Route("createForm101")]
        public IHttpActionResult createForm101(TaxFromExtensionModel form101)
        {
            DateTime NOW = DateTime.Now;
            Log.FormLog("start createForm101 0");
            BaseFont baseFont = BaseFont.CreateFont(@"C:\Windows\Fonts\arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            PdfReader pdfReader = new PdfReader(Path.Combine(pdfFiels, string.Format("Base101Form{0}.pdf", ((FormTypeEnum)form101.FormType).ToString())));
            string newFileName = string.Format("form101_{2}_{0}_{1}.pdf", form101.year, form101.employee.idNumber, form101.FormType);
            ///if file exists delete first
            FolderExtensions.DeleteFile(Path.Combine(tempPdfFiels, newFileName));

            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(Path.Combine(tempPdfFiels, newFileName), FileMode.Create));
            
            Document document = new Document(PageSize.A4);
            int index = 0;
            
            try
            {
                document.Open();

                #region clear fields data
                AcroFields fields = pdfStamper.AcroFields;
                foreach (var field in fields.Fields)
                {
                    fields.SetFieldProperty(field.Key, "textsize", 10f, null);
                    switch (fields.GetFieldType(field.Key))
                    {
                        case AcroFields.FIELD_TYPE_CHECKBOX:
                            fields.SetField(field.Key, "Off");
                            break;
                        case AcroFields.FIELD_TYPE_TEXT:
                            fields.SetField(field.Key, "");
                            break;
                    }
                    fields.SetDirection(field.Key, 1);
                    fields.SetColor(field.Key, 0);
                }
                fields.AddSubstitutionFont(baseFont);
                #endregion

                Log.FormLog(string.Format("clear fields data {0}", DateTime.Now.Subtract(NOW).TotalMilliseconds));

                #region general form data
                fields.SetDirection("year", 2);
                fields.SetField("year", form101.year.ToString());
                fields.SetFieldProperty("signatureDate", "textsize", 10f, null);
                fields.SetDirection("signatureDate", 2);
                fields.SetField("signatureDate", form101.signatureDate.ToString("dd/MM/yyyy"));

                UserImageModel signatureImagModel = new UserImageModel();
                if (form101.signatureImageID > 0)
                {
                    signatureImagModel = userbll.GetImageByID(form101.signatureImageID);
                }
                else
                {
                    signatureImagModel = userbll.GetUserImage(form101.employee.idNumber, "FORM101_SIGNATURE", new DateTime(form101.year, 01, 01), form101.municipalityId);
                }
                System.Drawing.Image img = Base64ToImage(signatureImagModel.fileBlob);

                AcroFields.FieldPosition fieldPosition = pdfStamper.AcroFields.GetFieldPositions("signatureDataUrl")[0];
                PushbuttonField imageField = new PushbuttonField(pdfStamper.Writer, fieldPosition.position, "signatureDataUrl_Replaced");

                imageField.Layout = PushbuttonField.LAYOUT_ICON_ONLY;
                imageField.ScaleIcon = PushbuttonField.SCALE_ICON_ALWAYS;
                imageField.ProportionalIcon = false;
                imageField.Options = BaseField.READ_ONLY;
                imageField.Image = iTextSharp.text.Image.GetInstance(img, BaseColor.WHITE);
                pdfStamper.AcroFields.RemoveField("signatureDataUrl");
                pdfStamper.AddAnnotation(imageField.Field, fieldPosition.page);
                #endregion

                Log.FormLog(string.Format("general form data {0}", DateTime.Now.Subtract(NOW).TotalMilliseconds));

                #region warning and version
                fields.SetDirection("warning", 2);
                fields.SetColor("warning", 2);
                fields.SetFieldProperty("warning", "textsize", 9f, null);
                fields.SetField("warning", " .נסח חתום בחתימה אלקטרונית מאושרת מהווה מקור, כל עוד הוא נשמר באופן אלקטרוני כאמור בחוק חתימה אלקטרונית, תשס''א - 2001");

                fields.SetDirection("version", 2);
                fields.SetFieldProperty("version", "textsize", 4f, null);
                TimeSpan span = DateTime.Now - new DateTime(2017, 1, 1);
                string version = string.Format("V:{0}.{1}", DateTime.Now.ToString("yyyyMMdd"), span.TotalSeconds);
                fields.SetField("version", version);
                form101.version = version;
                #endregion

                Log.FormLog(string.Format("warning and version {0}", DateTime.Now.Subtract(NOW).TotalMilliseconds));

                #region employer
                fields.SetField("employer.name", form101.employer.name);
                fields.SetField("employer.address", form101.employer.address);
                fields.SetDirection("employer.phoneNumber", 2);
                fields.SetField("employer.phoneNumber", string.Format("{0}-{1}", form101.employer.phoneNumber.GetPhonePrefix(), form101.employer.phoneNumber.GetPhoneNumber()));
                fields.SetDirection("employer.deductionFileID", 3);
                fields.SetField("employer.deductionFileID", form101.employer.deductionFileID.DeductionFileID());
                #endregion

                Log.FormLog(string.Format("employer {0}", DateTime.Now.Subtract(NOW).TotalMilliseconds));

                #region employee
                fields.SetColor("employee.idNumber", form101.employee.CheckidNumber);
                fields.SetColor("user.pageIdNumber", form101.employee.CheckidNumber);
                fields.SetColor("employee.firstName", form101.employee.CheckfirstName);
                fields.SetColor("employee.lastName", form101.employee.ChecklastName);
                fields.SetColor("employee.immigrationDate", form101.employee.CheckimmigrationDate);
                fields.SetColor("employee.birthDate", form101.employee.CheckbirthDate);

                fields.SetDirection("employee.cell", 3);
                fields.SetColor("employee.cell", form101.employee.Checkcell);
                fields.SetColor("employee.cellPrefix", form101.employee.Checkcell);

                fields.SetDirection("employee.phone", 3);
                fields.SetColor("employee.phone", form101.employee.Checkphone);
                fields.SetColor("employee.phonePrefix", form101.employee.Checkphone);

                fields.SetColor("employee.street", form101.employee.Checkstreet);
                fields.SetColor("employee.houseNumber", form101.employee.CheckhouseNumber);
                fields.SetColor("employee.city", form101.employee.Checkcity);
                fields.SetColor("employee.zipCode", form101.employee.Checkzip);
                fields.SetColor("employee.hmoName", form101.employee.CheckhmoName);
                fields.SetColor("employee.email", form101.employee.Checkemail);
                fields.SetColor("employee.kibbutzResident_false", form101.employee.CheckkibbutzResident);
                fields.SetColor("employee.kibbutzResident_true", form101.employee.CheckkibbutzResident);
                fields.SetColor("employee.gender.male", form101.employee.Checkgender);
                fields.SetColor("employee.gender.female", form101.employee.Checkgender);
                fields.SetColor("employee.maritalStatus.Single", form101.employee.CheckmaritalStatus);
                fields.SetColor("employee.maritalStatus.Married", form101.employee.CheckmaritalStatus);
                fields.SetColor("employee.maritalStatus.divorcee", form101.employee.CheckmaritalStatus);
                fields.SetColor("employee.maritalStatus.widower", form101.employee.CheckmaritalStatus);
                fields.SetColor("employee.maritalStatus.separated", form101.employee.CheckmaritalStatus);
                fields.SetColor("employee.ilResident", form101.employee.CheckilResident);
                fields.SetColor("employee.ilResident_true", form101.employee.CheckilResident);
                fields.SetColor("employee.ilResident_false", form101.employee.CheckilResident);
                fields.SetColor("employee.hmoMember_false", form101.employee.CheckhmoMember);
                fields.SetColor("employee.hmoMember_true", form101.employee.CheckhmoMember);

                fields.SetDirection("employee.idNumber", 2);
                fields.SetField("employee.idNumber", form101.employee.idNumber);
                fields.SetField("user.pageIdNumber", form101.employee.idNumber);
                fields.SetField("employee.lastName", form101.employee.lastName);
                fields.SetField("employee.firstName", form101.employee.firstName);
                if (form101.employee.immigrationDate.HasValue)
                {
                    fields.SetFieldProperty("employee.immigrationDate", "textsize", 10f, null);
                    fields.SetDirection("employee.immigrationDate", 2);
                    fields.SetField("employee.immigrationDate", dateToString(form101.employee.immigrationDate.Value, form101.FormType));
                }

                fields.SetFieldProperty("employee.birthDate", "textsize", 10f, null);
                fields.SetDirection("employee.birthDate", 2);
                fields.SetField("employee.birthDate", dateToString(form101.employee.birthDate.Value, form101.FormType));

                if (!string.IsNullOrWhiteSpace(form101.employee.cell) && form101.employee.cell.Length > 7)
                {
                    fields.SetFieldProperty("employee.cell", "textsize", 10f, null);
                    fields.SetField("employee.cell", form101.employee.cell.GetPhoneNumber(true));
                    fields.SetField("employee.cellPrefix", form101.employee.cell.GetPhonePrefix(true));
                }
                if (!string.IsNullOrWhiteSpace(form101.employee.phone) && form101.employee.phone.Length > 7)
                {
                    fields.SetFieldProperty("employee.phone", "textsize", 10f, null);
                    fields.SetField("employee.phone", form101.employee.phone.GetPhoneNumber());
                    fields.SetField("employee.phonePrefix", form101.employee.phone.GetPhonePrefix());
                }

                fields.SetField("employee.street", form101.employee.street);
                fields.SetDirection("employee.houseNumber", 2);
                fields.SetField("employee.houseNumber", form101.employee.houseNumber.ToString());
                fields.SetField("employee.city", form101.employee.city);
                fields.SetDirection("employee.zipCode", 3);
                fields.SetField("employee.zipCode", form101.employee.zip);
                fields.SetDirection("employee.hmoName", 3);
                fields.SetField("employee.hmoName", form101.employee.hmoName);
                fields.SetDirection("employee.email", 3);
                fields.SetField("employee.email", form101.employee.email);

                fields.SetCheckBox("employee.kibbutzResident_false", form101.employee.kibbutzResident, false);
                fields.SetCheckBox("employee.kibbutzResident_true", form101.employee.kibbutzResident, true);

                fields.SetCheckBox("employee.gender.male", form101.employee.gender, true);
                fields.SetCheckBox("employee.gender.female", form101.employee.gender, false);

                fields.SetCheckBox("employee.maritalStatus.Single", form101.employee.maritalStatus == 1, true);
                fields.SetCheckBox("employee.maritalStatus.Married", form101.employee.maritalStatus == 2, true);
                fields.SetCheckBox("employee.maritalStatus.divorcee", form101.employee.maritalStatus == 3, true);
                fields.SetCheckBox("employee.maritalStatus.widower", form101.employee.maritalStatus == 4, true);
                fields.SetCheckBox("employee.maritalStatus.separated", form101.employee.maritalStatus == 5, true);

                fields.SetCheckBox("employee.ilResident", form101.employee.ilResident, true);
                fields.SetCheckBox("employee.ilResident_true", form101.employee.ilResident, true);
                fields.SetCheckBox("employee.ilResident_false", form101.employee.ilResident, false);

                fields.SetCheckBox("employee.hmoMember_false", form101.employee.hmoMember, false);
                fields.SetCheckBox("employee.hmoMember_true", form101.employee.hmoMember, true);
                #endregion

                Log.FormLog(string.Format("employee {0}", DateTime.Now.Subtract(NOW).TotalMilliseconds));

                if ((FormTypeEnum)form101.FormType != FormTypeEnum._Retired)
                {
                    #region thisIncome
                    fields.SetColor("thisIncome.startDate", form101.income.CheckstartDate);
                    fields.SetColor("thisIncome.monthSalary", form101.income.incomeType.CheckmonthSalary);
                    fields.SetColor("thisIncome.anotherSalary", form101.income.incomeType.CheckanotherSalary);
                    fields.SetColor("thisIncome.partialSalary", form101.income.incomeType.CheckpartialSalary);
                    fields.SetColor("thisIncome.daySalary", form101.income.incomeType.CheckdaySalary);
                    fields.SetColor("thisIncome.allowance", form101.income.incomeType.Checkallowance);
                    fields.SetColor("thisIncome.stipend", form101.income.incomeType.Checkstipend);

                    fields.SetFieldProperty("thisIncome.startDate", "textsize", 10f, null);
                    fields.SetField("thisIncome.startDate", dateToString(form101.income.startDate, form101.FormType));
                    fields.SetCheckBox("thisIncome.monthSalary", form101.income.incomeType.monthSalary, true);
                    fields.SetCheckBox("thisIncome.anotherSalary", form101.income.incomeType.anotherSalary, true);
                    fields.SetCheckBox("thisIncome.partialSalary", form101.income.incomeType.partialSalary, true);
                    fields.SetCheckBox("thisIncome.daySalary", form101.income.incomeType.daySalary, true);
                    fields.SetCheckBox("thisIncome.allowance", form101.income.incomeType.allowance, true);
                    fields.SetCheckBox("thisIncome.stipend", form101.income.incomeType.stipend, true);
                    #endregion

                    Log.FormLog(string.Format("thisIncome {0}", DateTime.Now.Subtract(NOW).TotalMilliseconds));
                }

                #region children
                foreach (ChildExtensionModel child in form101.children.Where(c => c.birthDate.checkChildAge(19)))
                {
                    fields.SetColor("children[" + index + "].name", child.CheckfirstName);
                    fields.SetColor("children[" + index + "].idNumber", child.CheckidNumber);
                    fields.SetColor("children[" + index + "].birthDate", child.CheckbirthDate);
                    fields.SetColor("children[" + index + "].possession", child.Checkpossession);
                    fields.SetColor("children[" + index + "].childBenefit", child.CheckchildBenefit);

                    fields.SetField("children[" + index + "].name", child.firstName);
                    fields.SetField("children[" + index + "].idNumber", child.idNumber);
                    fields.SetField("children[" + index + "].birthDate", dateToString(child.birthDate, form101.FormType));

                    fields.SetCheckBox("children[" + index + "].possession", child.possession, true);
                    fields.SetCheckBox("children[" + index + "].childBenefit", child.childBenefit, true);

                    index++;
                    if (index > 12 || ((FormTypeEnum)form101.FormType == FormTypeEnum._Retired && index > 3))
                        break;
                }
                #endregion

                Log.FormLog(string.Format("children {0}", DateTime.Now.Subtract(NOW).TotalMilliseconds));

                #region otherIncomes
                fields.SetColor("otherIncomes.hasIncomes.true", form101.otherIncomes.CheckhasIncomes);
                fields.SetColor("otherIncomes.hasIncomes.false", form101.otherIncomes.CheckhasIncomes);
                fields.SetColor("otherIncomes.anotherSourceDetails", form101.otherIncomes.CheckanotherSourceDetails);
                fields.SetColor("otherIncomes.monthSalary", form101.otherIncomes.incomeType.CheckmonthSalary);
                fields.SetColor("otherIncomes.anotherSalary", form101.otherIncomes.incomeType.CheckanotherSalary);
                fields.SetColor("otherIncomes.partialSalary", form101.otherIncomes.incomeType.CheckpartialSalary);
                fields.SetColor("otherIncomes.daySalary", form101.otherIncomes.incomeType.CheckdaySalary);
                fields.SetColor("otherIncomes.allowance", form101.otherIncomes.incomeType.Checkallowance);
                fields.SetColor("otherIncomes.stipend", form101.otherIncomes.incomeType.Checkstipend);
                fields.SetColor("otherIncomes.anotherSource", form101.otherIncomes.incomeType.CheckanotherSource);
                fields.SetColor("otherIncomes.workDisability", form101.otherIncomes.CheckworkDisability);
                fields.SetColor("otherIncomes.trainingFund", form101.otherIncomes.ChecktrainingFund);
                fields.SetColor("otherIncomes.incomeTaxCredits1", form101.otherIncomes.CheckincomeTaxCredits);
                fields.SetColor("otherIncomes.incomeTaxCredits2", form101.otherIncomes.CheckincomeTaxCredits);

                if (form101.otherIncomes.hasIncomes)
                {
                    fields.SetCheckBox("otherIncomes.hasIncomes.true", form101.otherIncomes.hasIncomes, true);
                    fields.SetField("otherIncomes.anotherSourceDetails", form101.otherIncomes.anotherSourceDetails);

                    fields.SetCheckBox("otherIncomes.monthSalary", form101.otherIncomes.incomeType.monthSalary, true);
                    fields.SetCheckBox("otherIncomes.anotherSalary", form101.otherIncomes.incomeType.anotherSalary, true);
                    fields.SetCheckBox("otherIncomes.partialSalary", form101.otherIncomes.incomeType.partialSalary, true);
                    fields.SetCheckBox("otherIncomes.daySalary", form101.otherIncomes.incomeType.daySalary, true);
                    fields.SetCheckBox("otherIncomes.allowance", form101.otherIncomes.incomeType.allowance, true);
                    fields.SetCheckBox("otherIncomes.stipend", form101.otherIncomes.incomeType.stipend, true);
                    fields.SetCheckBox("otherIncomes.anotherSource", form101.otherIncomes.incomeType.anotherSource, true);

                    fields.SetCheckBox("otherIncomes.workDisability", form101.otherIncomes.workDisability, true);
                    fields.SetCheckBox("otherIncomes.trainingFund", form101.otherIncomes.trainingFund, true);

                    fields.SetCheckBox("otherIncomes.incomeTaxCredits1", form101.otherIncomes.incomeTaxCredits == 1, true);
                    fields.SetCheckBox("otherIncomes.incomeTaxCredits2", form101.otherIncomes.incomeTaxCredits == 2, true);
                }
                else
                    fields.SetCheckBox("otherIncomes.hasIncomes.false", form101.otherIncomes.hasIncomes, false);
                #endregion

                Log.FormLog(string.Format("otherIncomes {0}", DateTime.Now.Subtract(NOW).TotalMilliseconds));

                #region spouse
                if (form101.employee.maritalStatus == 2)
                {
                    fields.SetColor("spouse.idNumber", form101.spouse.CheckidNumber);
                    fields.SetColor("spouse.lastName", form101.spouse.ChecklastName);
                    fields.SetColor("spouse.firstName", form101.spouse.CheckfirstName);

                    ///the dates (birthDate / immigrationDate) are in revers Bcose the inputs in the base form are!
                    fields.SetColor("spouse.birthDate", form101.spouse.CheckimmigrationDate);
                    fields.SetColor("spouse.immigrationDate", form101.spouse.CheckbirthDate);

                    fields.SetColor("spouse.income.true", form101.spouse.CheckhasIncome);
                    fields.SetColor("spouse.income.false", form101.spouse.CheckhasIncome);
                    fields.SetColor("spouse.incomeType1", form101.spouse.CheckincomeType);
                    fields.SetColor("spouse.incomeType2", form101.spouse.CheckincomeType);

                    fields.SetField("spouse.idNumber", form101.spouse.idNumber);
                    fields.SetField("spouse.lastName", form101.spouse.lastName);
                    fields.SetField("spouse.firstName", form101.spouse.firstName);

                    if (form101.spouse.immigrationDate.HasValue)
                    {
                        fields.SetFieldProperty("spouse.birthDate", "textsize", 10f, null);
                        fields.SetField("spouse.birthDate", dateToString(form101.spouse.immigrationDate.Value, form101.FormType));
                    }
                    fields.SetFieldProperty("spouse.immigrationDate", "textsize", 10f, null);
                    fields.SetField("spouse.immigrationDate", dateToString(form101.spouse.birthDate.Value, form101.FormType));


                    fields.SetCheckBox("spouse.income.true", form101.spouse.hasIncome.Value, true);
                    fields.SetCheckBox("spouse.income.false", form101.spouse.hasIncome.Value, false);

                    fields.SetCheckBox("spouse.incomeType1", form101.spouse.incomeType.ToInt() == 1, true);
                    fields.SetCheckBox("spouse.incomeType2", form101.spouse.incomeType.ToInt() == 2, true);
                }
                #endregion

                Log.FormLog(string.Format("spouse {0}", DateTime.Now.Subtract(NOW).TotalMilliseconds));

                #region taxExemption
                fields.SetColor("taxExemption.infantchildren", form101.taxExemption.Checkinfantchildren);
                if ((FormTypeEnum)form101.FormType == FormTypeEnum._2018)
                {
                    fields.SetColor("taxExemption.infantchildren3", form101.taxExemption.Checkinfantchildren);
                    fields.SetColor("taxExemption.infantchildren3count", form101.taxExemption.Checkinfantchildren);
                    fields.SetColor("taxExemption.infantchildren12", form101.taxExemption.Checkinfantchildren);
                    fields.SetColor("taxExemption.infantchildren12count", form101.taxExemption.Checkinfantchildren);
                }
                if ((FormTypeEnum)form101.FormType == FormTypeEnum._2019)
                {
                    fields.SetColor("taxExemption.infantchildren_ThisYear", form101.taxExemption.Checkinfantchildren);
                    fields.SetColor("taxExemption.infantchildren_ThisYear_Counter", form101.taxExemption.Checkinfantchildren);
                    fields.SetColor("taxExemption.infantchildren_1to5", form101.taxExemption.Checkinfantchildren);
                    fields.SetColor("taxExemption.infantchildren_1to5_Counter", form101.taxExemption.Checkinfantchildren);
                }

                fields.SetColor("taxExemption.childrenInCare", form101.taxExemption.CheckchildrenInCare);
                fields.SetColor("taxExemption.childrenInCare15", form101.taxExemption.CheckchildrenInCare);
                fields.SetColor("taxExemption.childrenInCare15count", form101.taxExemption.CheckchildrenInCare);
                if ((FormTypeEnum)form101.FormType == FormTypeEnum._2018)
                {
                    fields.SetColor("taxExemption.childrenInCare19", form101.taxExemption.CheckchildrenInCare);
                    fields.SetColor("taxExemption.childrenInCare19count", form101.taxExemption.CheckchildrenInCare);
                    fields.SetColor("taxExemption.childrenInCare18", form101.taxExemption.CheckchildrenInCare);
                    fields.SetColor("taxExemption.childrenInCare18count", form101.taxExemption.CheckchildrenInCare);
                }
                if ((FormTypeEnum)form101.FormType == FormTypeEnum._2019)
                {
                    fields.SetColor("taxExemption.childrenInCare_ThisYear", form101.taxExemption.CheckchildrenInCare);
                    fields.SetColor("taxExemption.childrenInCare_ThisYear_Counter", form101.taxExemption.CheckchildrenInCare);
                    fields.SetColor("taxExemption.childrenInCare_6to17", form101.taxExemption.CheckchildrenInCare);
                    fields.SetColor("taxExemption.childrenInCare_6to17_Counter", form101.taxExemption.CheckchildrenInCare);
                    fields.SetColor("taxExemption.childrenInCare_18", form101.taxExemption.CheckchildrenInCare);
                    fields.SetColor("taxExemption.childrenInCare_18_Counter", form101.taxExemption.CheckchildrenInCare);
                }

                fields.SetColor("taxExemption.alimonyParticipates", form101.taxExemption.CheckalimonyParticipates);

                if ((FormTypeEnum)form101.FormType == FormTypeEnum._2019) fields.SetColor("taxExemption.incompetentChild", form101.taxExemption.CheckincompetentChild);

                fields.SetColor("taxExemption.blindDisabled", form101.taxExemption.CheckblindDisabled);
                fields.SetColor("taxExemption.exAlimony", form101.taxExemption.CheckexAlimony);
                fields.SetColor("taxExemption.exServiceman", form101.taxExemption.CheckexServiceman);
                fields.SetColor("taxExemption.servicemanStartDate", form101.taxExemption.CheckexServiceman);
                fields.SetColor("taxExemption.servicemanEndDate", form101.taxExemption.CheckexServiceman);
                fields.SetColor("taxExemption.graduation", form101.taxExemption.Checkgraduation);
                fields.SetColor("taxExemption.isImmigrant", form101.taxExemption.CheckisImmigrant);
                fields.SetColor("taxExemption.immigration.immigrantStatus1", form101.taxExemption.CheckimmigrantStatus);
                fields.SetColor("taxExemption.immigration.immigrantStatus2", form101.taxExemption.CheckimmigrantStatus);
                fields.SetColor("taxExemption.immigrantStatusDate", form101.taxExemption.CheckimmigrantStatusDate);
                fields.SetColor("taxExemption.noIncomeDate", form101.taxExemption.ChecknoIncomeDate);
                fields.SetColor("taxExemption.isResident", form101.taxExemption.CheckisResident);
                fields.SetColor("taxExemption.settlementCredits.settlement", form101.taxExemption.Checksettlement);
                fields.SetColor("taxExemption.settlementCredits.startResidentDate", form101.taxExemption.CheckstartResidentDate);
                fields.SetColor("taxExemption.partnersUnder18", form101.taxExemption.CheckpartnersUnder18);
                fields.SetColor("taxExemption.separatedParent", form101.taxExemption.CheckseparatedParent);
                fields.SetColor("taxExemption.singleParent", form101.taxExemption.ChecksingleParent);
                fields.SetColor("taxExemption.spouseNoIncome", form101.taxExemption.CheckspouseNoIncome);

                fields.SetCheckBox("taxExemption.infantchildren", form101.taxExemption.infantchildren, true);
                if (form101.taxExemption.infantchildren)
                {
                    if ((FormTypeEnum)form101.FormType != FormTypeEnum._2018)
                    {
                        int ThisYear = form101.children.ThisYear(form101.year);
                        if (ThisYear > 0)
                        {
                            fields.SetCheckBox("taxExemption.infantchildren_ThisYear", true, true);
                            fields.SetField("taxExemption.infantchildren_ThisYear_Counter", ThisYear.ToString());
                        }
                        int oneToFive = form101.children.OneToFive(form101.year);
                        if (oneToFive > 0)
                        {
                            fields.SetCheckBox("taxExemption.infantchildren_1to5", true, true);
                            fields.SetField("taxExemption.infantchildren_1to5_Counter", oneToFive.ToString());
                        }
                    }
                    if ((FormTypeEnum)form101.FormType == FormTypeEnum._2018)
                    {
                        int threeOrBorn = form101.children.ThreeOrBorn(form101.year);
                        int oneOrTow = form101.children.OneOrTow(form101.year);
                        if (threeOrBorn > 0)
                        {
                            fields.SetCheckBox("taxExemption.infantchildren3", true, true);
                            fields.SetField("taxExemption.infantchildren3count", threeOrBorn.ToString());
                        }
                        if (oneOrTow > 0)
                        {
                            fields.SetCheckBox("taxExemption.infantchildren12", true, true);
                            fields.SetField("taxExemption.infantchildren12count", oneOrTow.ToString());
                        }
                    }
                }

                fields.SetCheckBox("taxExemption.childrenInCare", form101.taxExemption.childrenInCare, true);
                if (form101.taxExemption.childrenInCare)
                {
                    int oneToFive = form101.children.OneToFive(form101.year);
                    if (oneToFive > 0)
                    {
                        fields.SetCheckBox("taxExemption.childrenInCare15", true, true);
                        fields.SetField("taxExemption.childrenInCare15count", oneToFive.ToString());
                    }

                    if ((FormTypeEnum)form101.FormType != FormTypeEnum._2018)
                    {
                        int ThisYear = form101.children.ThisYear(form101.year);
                        if (ThisYear > 0)
                        {
                            fields.SetCheckBox("taxExemption.childrenInCare_ThisYear", true, true);
                            fields.SetField("taxExemption.childrenInCare_ThisYear_Counter", ThisYear.ToString());
                        }
                        int sixTo17 = form101.children.sixToSeventeen(form101.year);
                        if (sixTo17 > 0)
                        {
                            fields.SetCheckBox("taxExemption.childrenInCare_6to17", true, true);
                            fields.SetField("taxExemption.childrenInCare_6to17_Counter", sixTo17.ToString());
                        }
                        int Eighteen = form101.children.Eighteen(form101.year);
                        if (Eighteen > 0)
                        {
                            fields.SetCheckBox("taxExemption.childrenInCare_18", true, true);
                            fields.SetField("taxExemption.childrenInCare_18_Counter", Eighteen.ToString());
                        }
                    }

                    if ((FormTypeEnum)form101.FormType == FormTypeEnum._2018)
                    {
                        int anderNineteen = form101.children.AnderNineteen(form101.year);
                        int bornOr18 = form101.children.BornOrEighteen(form101.year);

                        if (anderNineteen > 0)
                        {
                            fields.SetCheckBox("taxExemption.childrenInCare19", true, true);
                            fields.SetField("taxExemption.childrenInCare19count", anderNineteen.ToString());
                        }
                        if (bornOr18 > 0)
                        {
                            fields.SetCheckBox("taxExemption.childrenInCare18", true, true);
                            fields.SetField("taxExemption.childrenInCare18count", bornOr18.ToString());
                        }
                    }
                }

                fields.SetCheckBox("taxExemption.alimonyParticipates", form101.taxExemption.alimonyParticipates, true);

                if ((FormTypeEnum)form101.FormType != FormTypeEnum._2018) fields.SetCheckBox("taxExemption.incompetentChild", form101.taxExemption.incompetentChild, true);

                fields.SetCheckBox("taxExemption.blindDisabled", form101.taxExemption.blindDisabled, true);
                fields.SetCheckBox("taxExemption.exAlimony", form101.taxExemption.exAlimony, true);

                fields.SetCheckBox("taxExemption.exServiceman", form101.taxExemption.exServiceman, true);
                if (form101.taxExemption.exServiceman)
                {
                    fields.SetFieldProperty("taxExemption.servicemanStartDate", "textsize", 10f, null);
                    fields.SetFieldProperty("taxExemption.servicemanEndDate", "textsize", 10f, null);
                    fields.SetDirection("taxExemption.servicemanStartDate", 1);
                    fields.SetDirection("taxExemption.servicemanEndDate", 1);
                    if (form101.taxExemption.serviceStartDate.HasValue) fields.SetField("taxExemption.servicemanStartDate", dateToString(form101.taxExemption.serviceStartDate.Value, 0));
                    if (form101.taxExemption.serviceEndDate.HasValue) fields.SetField("taxExemption.servicemanEndDate", dateToString(form101.taxExemption.serviceEndDate.Value, 0));
                }

                fields.SetCheckBox("taxExemption.graduation", form101.taxExemption.graduation, true);

                fields.SetCheckBox("taxExemption.isImmigrant", form101.taxExemption.isImmigrant, true);
                if (form101.taxExemption.isImmigrant)
                {
                    fields.SetCheckBox("taxExemption.immigration.immigrantStatus1", form101.taxExemption.immigrationStatus == 1, true);
                    fields.SetCheckBox("taxExemption.immigration.immigrantStatus2", form101.taxExemption.immigrationStatus == 2, true);

                    if (form101.taxExemption.immigrationStatusDate.HasValue)
                    {
                        fields.SetFieldProperty("taxExemption.immigrantStatusDate", "textsize", 10f, null);
                        fields.SetField("taxExemption.immigrantStatusDate", dateToString(form101.taxExemption.immigrationStatusDate.Value, 0));
                    }
                    if (form101.taxExemption.immigrationNoIncomeDate.HasValue)
                    {
                        fields.SetFieldProperty("taxExemption.noIncomeDate", "textsize", 10f, null);
                        fields.SetField("taxExemption.noIncomeDate", dateToString(form101.taxExemption.immigrationNoIncomeDate.Value, 0));
                    }
                }

                fields.SetCheckBox("taxExemption.isResident", form101.taxExemption.isResident, true);
                if (form101.taxExemption.isResident)
                {
                    fields.SetField("taxExemption.settlementCredits.settlement", form101.taxExemption.settlement);
                    if (form101.taxExemption.startSettlementDate.HasValue)
                    {
                        fields.SetFieldProperty("taxExemption.settlementCredits.startResidentDate", "textsize", 10f, null);
                        fields.SetField("taxExemption.settlementCredits.startResidentDate", dateToString(form101.taxExemption.startSettlementDate.Value, 0));
                    }
                }

                fields.SetCheckBox("taxExemption.partnersUnder18", form101.taxExemption.partnersUnder18, true);
                fields.SetCheckBox("taxExemption.separatedParent", form101.taxExemption.separatedParent, true);
                fields.SetCheckBox("taxExemption.singleParent", form101.taxExemption.singleParent, true);
                fields.SetCheckBox("taxExemption.spouseNoIncome", form101.taxExemption.spouseNoIncome, true);
                #endregion

                Log.FormLog(string.Format("taxExemption {0}", DateTime.Now.Subtract(NOW).TotalMilliseconds));

                #region employers
                ///only if selected `additional incomes`
                if (form101.taxCoordination.requestReason == 2)
                {
                    index = 0;
                    //employers[0].incomeType
                    foreach (EmployerExtensionModel employer in form101.taxCoordination.employers.Where(e => e.name != null && e.salary > 0))
                    {
                        fields.SetColor("employers[" + index + "].name", employer.Checkname);
                        fields.SetColor("employers[" + index + "].address", employer.Checkaddress);
                        fields.SetColor("employers[" + index + "].deductionFileID", employer.CheckdeductionFileID);
                        fields.SetColor("employers[" + index + "].salary", employer.Checksalary);
                        fields.SetColor("employers[" + index + "].taxDeduction", employer.ChecktaxDeduction);
                        fields.SetColor("employers[" + index + "].incomeType", employer.CheckincomeType);

                        fields.SetField("employers[" + index + "].name", employer.name);
                        fields.SetField("employers[" + index + "].address", employer.address);
                        fields.SetDirection("employers[" + index + "].deductionFileID", 3);
                        fields.SetField("employers[" + index + "].deductionFileID", employer.deductionFileID);
                        fields.SetDirection("employers[" + index + "].salary", 2);
                        fields.SetField("employers[" + index + "].salary", employer.salary.ToString());
                        fields.SetDirection("employers[" + index + "].taxDeduction", 2);
                        fields.SetField("employers[" + index + "].taxDeduction", employer.taxDeduction.ToString());

                        if (employer.incomeType.monthSalary)
                            fields.SetField("employers[" + index + "].incomeType", "עבודה");
                        if (employer.incomeType.allowance)
                            fields.SetField("employers[" + index + "].incomeType", "קיצבה");
                        if (employer.incomeType.stipend)
                            fields.SetField("employers[" + index + "].incomeType", "מילגה");
                        if (employer.incomeType.anotherSource)
                            fields.SetField("employers[" + index + "].incomeType", "אחר");

                        index++;
                        if (index > 2)
                            break;
                    }
                }
                #endregion

                Log.FormLog(string.Format("employers {0}", DateTime.Now.Subtract(NOW).TotalMilliseconds));

                #region taxCoordination
                if (form101.taxCoordination.requestCoordination)
                {
                    fields.SetColor("taxCoordination.reason1", form101.taxCoordination.Checkreason);
                    fields.SetColor("taxCoordination.reason2", form101.taxCoordination.Checkreason);
                    fields.SetColor("taxCoordination.reason3", form101.taxCoordination.Checkreason);

                    fields.SetCheckBox("taxCoordination.reason1", form101.taxCoordination.requestReason == 1, true);
                    fields.SetCheckBox("taxCoordination.reason2", form101.taxCoordination.requestReason == 2, true);
                    fields.SetCheckBox("taxCoordination.reason3", form101.taxCoordination.requestReason == 3, true);
                }
                #endregion

                Log.FormLog(string.Format("taxCoordination {0}", DateTime.Now.Subtract(NOW).TotalMilliseconds));

                #region close
                // flatten form fields and close document
                pdfStamper.FreeTextFlattening = true;
                pdfStamper.FormFlattening = true;

                document.Close();
                pdfStamper.Close();
                pdfReader.Close();
                #endregion

                Log.FormLog(string.Format("close {0}", DateTime.Now.Subtract(NOW).TotalMilliseconds));

                #region save the 101 form into DB and delete the form
                byte[] pdfBytes = FolderExtensions.ReadFile(Path.Combine(tempPdfFiels, newFileName));
                UserImageModel formImageModel = saveIntoDB(form101.employee.idNumber, pdfBytes, "FORM101", form101.municipalityId, form101.year, "", form101.showMobile);
                form101.pdf = formImageModel;
                FolderExtensions.DeleteFile(Path.Combine(tempPdfFiels, newFileName));
                #endregion

                Log.FormLog(string.Format("End createForm101 {0}", DateTime.Now.Subtract(NOW).TotalMilliseconds));

                form101.sign = true;

                return Ok(form101);
            }
            catch (Exception e)
            {
                document.Close();
                pdfStamper.Close();
                pdfReader.Close();
                ///delete file if exists
                FolderExtensions.DeleteFile(Path.Combine(tempPdfFiels, newFileName));
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("SendFormToWS")]
        public IHttpActionResult SendFormToWS(TaxFromExtensionModel form101)
        {
            try
            {
                Log.FormLog("start SendFormToWS");

                WebSecurityModel returnModel = new WebSecurityModel();

                List<decimal> uploadImages = form101.connectedImages.Select(i => i.ID.ToDecimal()).ToList();
                List<UserImageModel> images = formbll.GetFormImages(uploadImages);
                UserImageModel theForm = formbll.GetFormImages(new List<decimal> { form101.pdf.ID }).First();

                Log.FormLog("GetFormImages");

                doSendToWS(form101, images, theForm);

                returnModel.success = true;
                returnModel.exceptionId = 0;
                return Ok(returnModel);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPut]
        [Route("LocalSendFormToWS")]
        [AllowAnonymous]
        public IHttpActionResult LocalSendFormToWS(TaxFromExtensionModel form101, bool inError = false, bool isLast = false)
        {
            if (!HttpContext.Current.Request.IsLocal)
                throw new Exception("SecurityException");

            try
            {
                Log.LogLoad(new { msg = "start LocalSendFormToWS", _inError = inError, _isLast = isLast });

                WebSecurityModel returnModel = new WebSecurityModel();

                List<decimal> uploadImages = form101.connectedImages.Select(i => i.ID.ToDecimal()).ToList();
                List<UserImageModel> images = formbll.GetFormImages(uploadImages);
                UserImageModel theForm = formbll.GetFormImages(new List<decimal> { form101.pdf.ID }).First();

                Log.LogLoad("GetFormImages");

                UserModel user = userbll.GetUserByID(theForm.userId, true);
                string template = string.Empty;
                string[] UserEmails = new string[] { (string.IsNullOrWhiteSpace(user.email) ? "" : user.email), (string.IsNullOrWhiteSpace(form101.employee.email) ? "" : form101.employee.email) }.Where(x => !string.IsNullOrEmpty(x)).ToArray();

                SmsNotificationModel smsNotification = new SmsNotificationModel();

                Log.LogLoad(string.Format("UserEmails => {0}, Length => {2} ,   userId => {1}", string.Join(",", UserEmails), theForm.userId, UserEmails.Length));
                #region set up email/sms notification template

                if (!isDebugeMode)
                {
                    if (UserEmails.Length > 0)
                    {///setup mail template
                        UserCredentialsModel data = new UserCredentialsModel
                        {
                            confirmationToken = form101.version,
                            firstName = user.firstName,
                            lastName = user.lastName,
                            idNumber = form101.year.ToString(),
                            fullName = string.Format("{0} {1}", user.firstName, user.lastName),
                        };
                        RouteData routedata = HttpContext.Current.Request.RequestContext.RouteData;
                        routedata.Values.Add("token", user.membership.confirmationToken);
                        template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "Form101SeccessEmail", data);
                    }
                    else if (!string.IsNullOrWhiteSpace(user.cell))
                    {///setup sms template
                        string msg = string.Format(smsMsg, user.firstName, user.lastName, form101.version);
                        smsNotification = user.MapToSms(new string[] { (string.IsNullOrWhiteSpace(form101.employee.cell) ? form101.employee.phone : form101.employee.cell) }, msg);
                    }
                    #endregion
                    Log.LogLoad("email/sms");
                }

                #region commpresion
                string newFileName = string.Format("form101_{2}_{0}_{1}.pdf", form101.year, form101.employee.idNumber, form101.FormType);
                File.WriteAllBytes(Path.Combine(tempPdfFiels, newFileName), theForm.fileBlob);
                pdfFullCompress(newFileName, form101.year, form101.employee.idNumber);
                byte[] pdfBytes = FolderExtensions.ReadFile(Path.Combine(tempPdfFiels, newFileName));

                theForm.fileBlob = pdfBytes;
                FolderExtensions.DeleteFile(Path.Combine(tempPdfFiels, newFileName));
                #endregion
                Log.LogLoad("commpresion");

                #region save form data as JSON
                Form101DataModel model = formbll.GetFormDataByVersion(form101.version);
                if (model == null)
                {
                    model = new Form101DataModel();
                    model.createdDate = new DateTime(form101.year, 1, 1);
                    model.jsonData = form101.ToJSON();
                    model.userId = theForm.userId;
                    model.formImageId = form101.pdf.ID;
                    model.signatureImagId = form101.signatureImageID;
                    model.version = form101.version;
                    model.municipalityId = form101.municipalityId;
                }
                #endregion
                Log.LogLoad("JSON");

                #region add attachments
                string path = string.Format("{0}.pdf", form101.employee.idNumber);
                var ms = new MemoryStream();
                var archive = new ZipArchive(ms, ZipArchiveMode.Create, true);
                List<UserImageModel> caseAttachmentModels = new List<UserImageModel>();
                images = checkUplodedFiles(form101, images);
                if (images.Count > 0)
                {
                    byte[] uploded = concatAndAddContent(images.Where(i => i.fileName.Contains(".pdf")).Select(i => i.fileBlob).ToList());

                    uploded = concatAndAddContent(new List<byte[]> { theForm.fileBlob, uploded });
                    caseAttachmentModels.Add(new UserImageModel() { fileName = path, fileBlob = uploded });
                }
                else
                {
                    byte[] uploded = concatAndAddContent(new List<byte[]> { theForm.fileBlob });
                    caseAttachmentModels.Add(new UserImageModel() { fileName = path, fileBlob = uploded });
                }
                #endregion
                Log.LogLoad("add Attachment");

                int exceptionId = 0;
                int _rashut = form101.employer.rashut;
                int _mifal = form101.employer.mifal;
                if (_rashut < 1 && _mifal > 1)
                {
                    _rashut = _mifal;
                    form101.employer.rashut = _rashut;
                }
                if (_rashut < 1 && _mifal < 1)
                {
                    MunicipalityContactModel municipalitycontactmodel = formbll.GetMunicipalityContactByFactoryId(
                                            form101.municipalityId,
                                            CustomMembershipProvider.GetUserFactory(form101.employee.ID, form101.municipalityId)
                                            , false);

                    _mifal = municipalitycontactmodel.mifal.ToInt();
                    form101.employer.mifal = _mifal;
                    _rashut = municipalitycontactmodel.rashut.ToInt();
                    form101.employer.rashut = _rashut;

                    if (_rashut < 1 && _mifal > 1)
                    {
                        _rashut = _mifal;
                        form101.employer.rashut = _mifal;
                    }
                }
                if (_rashut < 1)
                    throw new NullReferenceException("NullRashutReferenceException");

                #region send zip to archive
                try
                {
                    if (!isDebugeMode)
                    {
                        SftpTransfer.UplodTransferZip(caseAttachmentModels, string.Format("{0}-T101-{1}-T_{2}.zip", _rashut.Pad(3), form101.year, form101.employee.idNumber));
                    }
                }
                catch (Exception Ie)
                {
                    this.ServerError(Ie);
                    exceptionId -= 1;
                }
                #endregion
                Log.LogLoad("send zip");

                #region post the data into WS
                try
                {
                    Tt_Full101FromModel fullForm = form101.MapFullTofes101();
                    Log.FormLog(fullForm.ToJSON());

                    if (!isDebugeMode)
                    {
                        FormsProvider.PostTofes101ToWs(form101, true, isLast);
                    }
                }
                catch (Exception Ie)
                {
                    this.ServerError(Ie);
                    exceptionId -= 2;
                }
                #endregion
                Log.LogLoad("post data");

                model.deliveryStatus = exceptionId;

                #region save data/form state and send to user if no exception

                Log.LogLoad(new { exceptionId = exceptionId, id = theForm.userId });
                if (exceptionId == 0 && !isDebugeMode)
                {
                    if ((UserEmails.Length > 0))
                    {///send email
                        Log.LogLoad("send email");
                        MailSender mailsender = new MailSender();
                        mailsender.SendMail(String.Join(",", UserEmails), template, false, string.Format("טופס 101 לשנת {0} התקבל במערכת", form101.year), new Dictionary<string, byte[]> { { string.Format("{0}_{1}.pdf", DateTime.Now.ToString("dd_MM_yyyy"), user.idNumber), theForm.fileBlob } }, "", DefaultSiteEmail);
                    }
                    else if (!string.IsNullOrWhiteSpace(user.cell))
                    {///send sms
                        Log.LogLoad("send sms");
                        SmsManagerProvider.SendSMS(smsNotification);
                    }
                }
                #endregion
                Log.LogLoad("End LocalSendFormToWS");

                returnModel.success = (exceptionId == 0);
                returnModel.exceptionId = exceptionId;
                formbll.SaveFormData(model);
                return Ok(returnModel);
            }
            catch (Exception e)
            {
                this.ServerError(e);
                if (!inError)///retry to send 1 more time
                    return LocalSendFormToWS(form101, true);

                return this.ServerError(e);
            }
        }

        #region private helper functions
        private List<UserImageModel> checkUplodedFiles(TaxFromExtensionModel form101, List<UserImageModel> images)
        {
            List<UserImageModel> removeImages;
            if (!form101.taxExemption.blindDisabled)//blindDisabledFiles_1
            {
                removeImages = returnRemoveImages("blinddisabledfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.alimonyParticipates)//alimonyParticipatesFiles
            {
                removeImages = returnRemoveImages("alimonyparticipatesfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.exServiceman)//servicemanFiles
            {
                removeImages = returnRemoveImages("servicemanfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxCoordination.requestCoordination)//coordinationFiles / reasonFiles
            {
                removeImages = returnRemoveImages("coordinationfiles", images);
                removeImages.ForEach(i => images.Remove(i));
                removeImages = returnRemoveImages("reasonfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.spouseNoIncome)//spouseNoIncomeFiles
            {
                removeImages = returnRemoveImages("spousenoincomefiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.incompetentChild)//incompetentChildFiles
            {
                removeImages = returnRemoveImages("incompetentchildfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.isImmigrant)//immigrantCertificateFiles / returningResidentFiles
            {
                removeImages = returnRemoveImages("immigrantcertificatefiles", images);
                removeImages.ForEach(i => images.Remove(i));
                removeImages = returnRemoveImages("returningresidentfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.exAlimony)//exAlimonyFiles
            {
                removeImages = returnRemoveImages("exalimonyfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.graduation)//graduationFiles
            {
                removeImages = returnRemoveImages("graduationfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.isResident)//isaFiles
            {
                removeImages = returnRemoveImages("isafiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            return images;
        }
        private List<UserImageModel> returnRemoveImages(string imageType, List<UserImageModel> images)
        {
            return images.Where(i => i.fileName.ToLower().StartsWith(imageType)).ToList();
        }
        private void compresSavedFile(byte[] imageBytes, string filename, UserImageModel image)
        {
            string newFileName = string.Format("{0}_{1}", image.ID, filename);
            File.WriteAllBytes(Path.Combine(tempPdfFiels, newFileName), imageBytes);
            pdfCompress(newFileName);
            byte[] pdfBytes = FolderExtensions.ReadFile(Path.Combine(tempPdfFiels, newFileName));
            FolderExtensions.DeleteFile(Path.Combine(tempPdfFiels, newFileName));
            if (pdfBytes.Length < image.fileBlob.Length)
            {
                image.fileBlob = pdfBytes;
                userbll.CreateUpdateUserImage(image);
            }
        }
        private void compresSavedFile(UserImageModel image)
        {
            string newFileName = string.Format("{0}_{1}", image.ID, image.fileName);
            File.WriteAllBytes(Path.Combine(tempPdfFiels, newFileName), image.fileBlob);
            pdfCompress(newFileName);
            byte[] pdfBytes = FolderExtensions.ReadFile(Path.Combine(tempPdfFiels, newFileName));
            FolderExtensions.DeleteFile(Path.Combine(tempPdfFiels, newFileName));
            if (pdfBytes.Length < image.fileBlob.Length)
            {
                image.fileBlob = pdfBytes;
                userbll.CreateUpdateUserImage(image);
            }
        }
        private void doSendToWS(TaxFromExtensionModel form101, List<UserImageModel> images, UserImageModel theForm)
        {
            UserModel user = userbll.GetUserByID(theForm.userId, true);
            string template = string.Empty;
            string[] UserEmails = new string[] { (string.IsNullOrWhiteSpace(user.email) ? "" : user.email), (string.IsNullOrWhiteSpace(form101.employee.email) ? "" : form101.employee.email) }.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            SmsNotificationModel smsNotification = new SmsNotificationModel();

            Log.FormLog(string.Format("UserEmails => {0}, Length => {2} ,   userId => {1}", string.Join(",", UserEmails), theForm.userId, UserEmails.Length));
            #region set up email/sms notification template
            if (UserEmails.Length > 0 && !isDebugeMode)
            {///setup mail template
                UserCredentialsModel data = new UserCredentialsModel
                {
                    confirmationToken = form101.version,
                    firstName = user.firstName,
                    lastName = user.lastName,
                    idNumber = form101.year.ToString(),
                    fullName = string.Format("{0} {1}", user.firstName, user.lastName),
                };
                RouteData routedata = HttpContext.Current.Request.RequestContext.RouteData;
                routedata.Values.Add("token", user.membership.confirmationToken);
                template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "Form101SeccessEmail", data);
            }
            else if (!string.IsNullOrWhiteSpace(user.cell) && !isDebugeMode)
            {///setup sms template
                string msg = string.Format(smsMsg, user.firstName, user.lastName, form101.version);
                smsNotification = user.MapToSms(new string[] { (string.IsNullOrWhiteSpace(form101.employee.cell) ? form101.employee.phone : form101.employee.cell) }, msg);
            }
            #endregion
            Log.FormLog("email/sms");

            int _rashut = form101.employer.rashut;
            int _mifal = form101.employer.mifal;
            if (_rashut < 1 && _mifal > 1)
            {
                _rashut = _mifal;
                form101.employer.rashut = _rashut;
            }
            if (_rashut < 1 && _mifal < 1)
            {
                MunicipalityContactModel municipalitycontactmodel = formbll.GetMunicipalityContactByFactoryId(
                                            form101.municipalityId,
                                            CustomMembershipProvider.GetUserFactory(form101.employee.ID, form101.municipalityId)
                                            , false);

                _mifal = municipalitycontactmodel.mifal.ToInt();
                form101.employer.mifal = _mifal;
                _rashut = municipalitycontactmodel.rashut.ToInt();
                form101.employer.rashut = _rashut;

                if (_rashut < 1 && _mifal > 1)
                {
                    _rashut = _mifal;
                    form101.employer.rashut = _mifal;
                }
            }
            if (_rashut < 1)
                throw new NullReferenceException("NullRashutReferenceException");

            AsyncFunctionCaller.RunAsync(HttpContext.Current, () =>
             {
                 #region commpresion
                 string newFileName = string.Format("form101_{0}_{1}.pdf", form101.year, form101.employee.idNumber);
                 File.WriteAllBytes(Path.Combine(tempPdfFiels, newFileName), theForm.fileBlob);
                 pdfFullCompress(newFileName, form101.year, form101.employee.idNumber);
                 byte[] pdfBytes = FolderExtensions.ReadFile(Path.Combine(tempPdfFiels, newFileName));

                 theForm.fileBlob = pdfBytes;
                 FolderExtensions.DeleteFile(Path.Combine(tempPdfFiels, newFileName));
                 #endregion
                 Log.FormLog("commpresion");

                 #region save form data as JSON
                 Form101DataModel model = new Form101DataModel();
                 model.createdDate = new DateTime(form101.year, 1, 1);
                 model.jsonData = form101.ToJSON();
                 model.userId = theForm.userId;
                 model.formImageId = form101.pdf.ID;
                 model.signatureImagId = form101.signatureImageID;
                 model.version = form101.version;
                 model.municipalityId = form101.municipalityId;
                 #endregion
                 Log.FormLog("JSON");

                 #region add attachments
                 string path = string.Format("{0}.pdf", form101.employee.idNumber);
                 var ms = new MemoryStream();
                 var archive = new ZipArchive(ms, ZipArchiveMode.Create, true);
                 List<UserImageModel> caseAttachmentModels = new List<UserImageModel>();
                 byte[] uploded = new byte[1];

                 images = checkUplodedFiles(form101, images);
                 if (images.Count > 0)
                 {
                     uploded = concatAndAddContent(images.Where(i => i.fileName.Contains(".pdf")).Select(i => i.fileBlob).ToList());

                     uploded = concatAndAddContent(new List<byte[]> { theForm.fileBlob, uploded });
                     caseAttachmentModels.Add(new UserImageModel() { fileName = path, fileBlob = uploded });
                 }
                 else
                 {
                     uploded = concatAndAddContent(new List<byte[]> { theForm.fileBlob });
                     caseAttachmentModels.Add(new UserImageModel() { fileName = path, fileBlob = uploded });
                 }
                 #endregion
                 Log.FormLog("add Attachment");

                 int exceptionId = 0;
                 #region send zip to archive
                 try
                 {
                     if (!isDebugeMode)
                         SftpTransfer.UplodTransferZip(caseAttachmentModels, string.Format("{0}-T101-{1}-T_{2}.zip", _rashut.Pad(3), form101.year, form101.employee.idNumber));
                 }
                 catch (Exception e)
                 {
                     this.ServerError(e);
                     exceptionId -= 1;
                 }
                 #endregion
                 Log.FormLog("send zip");

                 #region post the data into WS
                 try
                 {
                     if (!isDebugeMode)
                         FormsProvider.PostTofes101ToWs(form101);
                 }
                 catch (Exception e)
                 {
                     this.ServerError(e);
                     exceptionId -= 2;
                 }
                 #endregion
                 Log.FormLog("post data");

                 model.deliveryStatus = exceptionId;

                 #region save data/form state and send to user if no exception
                 formbll.SaveFormData(model);

                 Log.FormLog(new { exceptionId = exceptionId, id = theForm.userId });
                 if (exceptionId == 0 && !isDebugeMode)
                 {
                     if ((UserEmails.Length > 0))
                     {///send email
                         Log.FormLog("send email");
                         MailSender mailsender = new MailSender();
                         mailsender.SendMail(String.Join(",", UserEmails), template, false, string.Format("טופס 101 לשנת {0} התקבל במערכת", form101.year), new Dictionary<string, byte[]> { { string.Format("{0}_{1}.pdf", DateTime.Now.ToString("dd_MM_yyyy"), user.idNumber), /*theForm.fileBlob*/
            uploded } }, "", DefaultSiteEmail);
                     }
                     else if (!string.IsNullOrWhiteSpace(user.cell))
                     {///send sms
                         Log.FormLog("send sms");
                         SmsManagerProvider.SendSMS(smsNotification);
                     }
                 }
                 #endregion
                 Log.FormLog("End SendFormToWS");
             }, false);

        }
        private byte[] concatAndAddContent(List<byte[]> pdfByteContent)
        {

            using (var ms = new MemoryStream())
            {
                using (var doc = new Document())
                {
                    using (var copy = new PdfSmartCopy(doc, ms))
                    {
                        doc.Open();

                        //Loop through each byte array
                        foreach (var p in pdfByteContent)
                        {

                            //Create a PdfReader bound to that byte array
                            using (var reader = new PdfReader(p))
                            {
                                int pageNum = reader.NumberOfPages;
                                for (int i = 1; i <= pageNum; i++)
                                {
                                    reader.SetPageContent(i, reader.GetPageContent(i), 9, true);
                                }
                                reader.RemoveAnnotations();
                                reader.RemoveUnusedObjects();
                                reader.RemoveUsageRights();

                                //Add the entire document instead of page-by-page
                                copy.AddDocument(reader);
                            }
                        }

                        doc.Close();
                    }
                }

                //Return just before disposing
                return ms.ToArray();
            }
        }
        /// <summary>
        /// a two-step process, 
        /// 1. using iTextSharp to remove all inputs and annotations
        /// 2. using ghost script to commpress the PDF
        /// </summary>
        /// <param name="newFileName"></param>
        /// <param name="year"></param>
        /// <param name="idNumber"></param>
        private void pdfFullCompress(string newFileName, int year, string idNumber)
        {
            ///using itex
            PdfReader reader = new PdfReader(Path.Combine(tempPdfFiels, newFileName));
            string outputPath = string.Format("form101_{0}_{1}_1.pdf", year, idNumber);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(Path.Combine(tempPdfFiels, outputPath), FileMode.Create), PdfWriter.VERSION_1_7);
            int pageNum = reader.NumberOfPages;
            for (int i = 1; i <= pageNum; i++)
            {
                reader.SetPageContent(i, reader.GetPageContent(i), 9, true);
            }

            reader.RemoveUnusedObjects();
            reader.RemoveUsageRights();
            
            stamper.FormFlattening = true;
            stamper.Writer.PdfVersion = '7';
            stamper.Writer.SetAtLeastPdfVersion('7');
            
            stamper.SetFullCompression();
            stamper.Close();
            reader.Close();
            FolderExtensions.DeleteFile(Path.Combine(tempPdfFiels, newFileName));

            String ars = string.Format(@" -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dCompatibilityLevel=1.3 -dEncodeColorImages=true -sOutputFile={1} {0}", Path.Combine(tempPdfFiels, outputPath), Path.Combine(tempPdfFiels, newFileName));
            Process proc = new Process();
            proc.StartInfo.FileName = ghostScriptPath;
            proc.StartInfo.Arguments = ars;
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            proc.Start();
            proc.WaitForExit(60000);
            proc.Close();
            FolderExtensions.DeleteFile(Path.Combine(tempPdfFiels, outputPath));
        }
        private void pdfCompress(string inputPath)
        {
            try
            {
                string outputPath = "TEMP_" + inputPath;
                String ars = string.Format(@" -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dCompatibilityLevel=1.3 -dEncodeColorImages=true -sOutputFile={0} {1}", Path.Combine(tempPdfFiels, outputPath), Path.Combine(tempPdfFiels, inputPath));
                Process proc = new Process();
                proc.StartInfo.FileName = ghostScriptPath;
                proc.StartInfo.Arguments = ars;
                proc.StartInfo.CreateNoWindow = true;
                proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proc.Start();
                proc.WaitForExit(60000);
                proc.Close();
                FolderExtensions.ReNameFile(Path.Combine(tempPdfFiels, inputPath), Path.Combine(tempPdfFiels, outputPath));
            }
            catch (Exception e)
            {
                this.ServerError(e);
                throw;
            }
        }
        private void jpegCompress(string inputPath)
        {
            string _inputPath = Path.Combine(tempPdfFiels, inputPath);
            _inputPath = _inputPath.Replace(@"\", @"/");
            string _outputPath = string.Format("TEMP_{0}.{1}", Path.GetFileNameWithoutExtension(inputPath), "pdf");

            String ars = string.Format(@"-sDEVICE=pdfwrite -o {0} ""{1}"" -c ({2}) viewJPEG", Path.Combine(tempPdfFiels, _outputPath), ghostScriptViewjpegPath, _inputPath);
            Process proc = new Process();
            proc.StartInfo.FileName = ghostScriptPath;
            proc.StartInfo.Arguments = ars;
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.Start();
            proc.WaitForExit(60000);
            proc.Close();
            FolderExtensions.ReNameFile(inputPath, _outputPath);
            pdfCompress(string.Format("{0}.{1}", Path.GetFileNameWithoutExtension(inputPath), "pdf"));
        }
        private UserImageModel saveIntoDB(string idNumber, byte[] img, string type, int municipalityId, int year, string fileName = "", bool? addMobileDisplay = null)
        {
            UserImageModel image = new UserImageModel
            {
                fileDate = new DateTime(year, 01, 01),
                type = type,
                fileBlob = img,
                fileString = img.ByteArrayToString(),
                municipalityId = municipalityId,
                fileName = fileName
            };
            if (addMobileDisplay.HasValue && addMobileDisplay.Value)
            {
                CreateMobileImage createImage = new CreateMobileImage();
                createImage.CreateImage(image);
            }
            image = userbll.SaveForm101Image(image, idNumber);
            return image;
        }
        private void ExecuteCommand(string Command)
        {
            try
            {
                ProcessStartInfo ProcessInfo;
                Process Process;

                ProcessInfo = new ProcessStartInfo("cmd.exe", "/K " + Command);
                ProcessInfo.CreateNoWindow = true;
                ProcessInfo.UseShellExecute = false;

                Process = Process.Start(ProcessInfo);
            }
            catch { }
        }
        private System.Drawing.Image Base64ToImage(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }
        private System.Drawing.Image Base64ToImage(byte[] imageBytes)
        {
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }
        private string dateToString(DateTime date, int FormType)
        {
            switch (FormType)
            {
                case 1:
                    return date.ToString("dd/MM/yyyy");
                case 2:
                case 3:
                    return date.ToString("ddMMyyyy");
                default:
                    return date.ToString("dd/MM/yyyy");
            }
        }
        #endregion
    }
}