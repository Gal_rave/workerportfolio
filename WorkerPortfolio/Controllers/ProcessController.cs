﻿using System;
using System.Linq;
using System.Web.Http;

using DataAccess.Models;
using DataAccess.BLLs;
using WorkerPortfolio.App_Start;
using CustomMembership;
using WebServiceProvider;
using DataAccess.Extensions;
using Logger;
using System.Collections.Generic;
using DataAccess.Mappers;
using System.Web;
using Mail;
using System.Web.Routing;

namespace WorkerPortfolio.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    [RoutePrefix("api/Process")]

    public class ProcessController : ApiController
    {
        private UserBll userBll;
        private FormBll formBll;
        private ProcessBll processBll;
        private MailSender ms;

        public ProcessController()
        {
            userBll = new UserBll();
            formBll = new FormBll();
            processBll = new ProcessBll();
            ms = new MailSender();
        }
        
        [HttpPost]
        [Route("GetAllProcessesData")]
        public IHttpActionResult GetAllProcessesData(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model.idNumber, model.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");

                List<TahalichModel> tahalichim = processBll.getRashutProcesses(model.currentRashutId);
                List<StepModel> shlavim = processBll.getRashutSteps(model.currentRashutId);

                Log.FormLog(tahalichim, "GetAllProcessesData");

                return Ok(tahalichim);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("GetProcessesData")]
        public IHttpActionResult GetProcessesData(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model.idNumber, model.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");
                
                List<TahalichModel> tahalichim = processBll.getRashutProcesses(model.currentRashutId);

                Log.FormLog(tahalichim, "GetProcessesData");

                return Ok(tahalichim);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("GetMishtamsheiKvuza/{confirmationToken}/{idNumber}/{calculatedIdNumber}/{currentMunicipalityId}/{currentRashutId}")]
        public IHttpActionResult GetMishtamsheiKvuza(string confirmationToken, string idNumber, string calculatedIdNumber, int currentMunicipalityId, int currentRashutId, List<MishtamsheiKvuzaModel> mishtamshim)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(idNumber, confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");

                List<MistameshSacharModel> users = processBll.getMishtamsheiKvuza(currentRashutId, currentMunicipalityId, mishtamshim);

                Log.FormLog(users, "GetMishtamsheiKvuza");

                return Ok(users);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("GetMishtamsheiKvuzaDinamit/{confirmationToken}/{idNumber}/{calculatedIdNumber}/{currentMunicipalityId}/{currentRashutId}")]
        public IHttpActionResult GetMishtamsheiKvuzaDinamit(string confirmationToken, string idNumber, string calculatedIdNumber, int currentMunicipalityId, int currentRashutId, List<MishtamsheiKvuzaModel> kvuzotDinamiyot)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(idNumber, confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");

                List<int> types = new List<int>();
                foreach(MishtamsheiKvuzaModel kvuzaAsRow in kvuzotDinamiyot)
                {
                    int type = kvuzaAsRow.mishtameshId;//mishtameshId in kvuzot dinamiyot is the type of the dinamic group. e.g gizbarim => type = 4
                    types.Add(type);
                }
                List<MishtemaeshDinamiModel> users = SystemUsersProvider.getMishtamshimDinamim(currentRashutId, currentMunicipalityId, types);

                Log.FormLog(users, "GetMishtamsheiKvuzaDinamit");

                return Ok(users);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("GetStepsData")]
        public IHttpActionResult GetStepsData(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model.idNumber, model.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");
                
                List<StepModel> steps = processBll.getRashutSteps(model.currentRashutId);

                Log.FormLog(steps, "GetStepsData");

                return Ok(steps);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("getUserManagers")]
        public IHttpActionResult getUserManagers(GetMngrsModel model)
        {
            if (model == null) return null;

            WSPostModel wsModel = model.MapToWSPostModel();
            List<YehidaIrgunitModel> yehidot = model.MapToYehidaIrgunit();

            try
            {
                if (!WebSecurity.CheckAuthorizetion(wsModel.idNumber, wsModel.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");
                
                List<UnitManagerModel> unitManagers = processBll.getUserUnitMngrs(yehidot, wsModel.currentRashutId, wsModel.calculatedIdNumber);

                Log.FormLog(unitManagers, "getUserManagers");

                return Ok(unitManagers);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("getWorkerVacationData")]
        public IHttpActionResult GetWorkerVacationData(GetWorkerDataModel model)
        {
            if (model == null) return null;

            WSPostModel wsModel = model.MapToWSPostModel();
            RepresentedWorkerModel worker = model.MapToRepresentedWorker();

            try
            {
                var obj = WSProvider.GetEmployeeAttendanceFiguresData(worker.idNumber, wsModel.currentMunicipalityId, wsModel.selectedDate.Year);

                Log.FormLog(obj, "GetWorkerVacationData");

                return Ok(obj);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("getWorkerManagers")]
        public IHttpActionResult GetWorkerManagers(GetWorkerMngrsModel model)
        {
            if (model == null) return null;

            WSPostModel wsModel = model.MapToWSPostModel();
            RepresentedWorkerModel worker = model.MapToRepresentedWorker();
            List<YehidaIrgunitModel> units = model.MapToUnits();

            try
            {
                if (!WebSecurity.CheckAuthorizetion(wsModel.idNumber, wsModel.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");
                
                List<UnitManagerModel> unitManagers = processBll.getUserUnitMngrs(units, wsModel.currentRashutId, wsModel.calculatedIdNumber);

                Log.FormLog(unitManagers, "GetWorkerManagers");

                return Ok(unitManagers);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("getRepresentedWorkers")]
        public IHttpActionResult getRepresentedWorkers(GetMngrsModel model)
        {
            if (model == null) return null;

            WSPostModel wsModel = model.MapToWSPostModel();
            List<YehidaIrgunitModel> yehidot = model.MapToYehidaIrgunit();

            try
            {
                if (!WebSecurity.CheckAuthorizetion(wsModel.idNumber, wsModel.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");
                
                List<YehidaIrgunitModel> secretaryUnits = processBll.findSecretaryUnits(wsModel, wsModel.currentRashutId);
                List<RepresentedWorkerModel> totalRepresentedWorkers = new List<RepresentedWorkerModel>();
                List<int> workerIds = SystemUsersProvider.getWorkersIds(wsModel.currentRashutId, wsModel.currentMunicipalityId, secretaryUnits);
                totalRepresentedWorkers = processBll.getWorkers(workerIds);

                Log.FormLog(totalRepresentedWorkers, "getRepresentedWorkers");

                return Ok(totalRepresentedWorkers);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("getOrganizationUnitsManagers")]
        public IHttpActionResult GetOrganizationUnitsManagers(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model.idNumber, model.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");
                
                List<UnitManagerModel> unitManagers = processBll.getUnitMngrs(model.currentMunicipalityId);

                Log.FormLog(unitManagers, "GetOrganizationUnitsManagers");

                return Ok(unitManagers);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("getYehidaIrgunit")]
        public IHttpActionResult GetYehidaIrgunit(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model.idNumber, model.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");

                List<YehidaIrgunitModel> yehidot = SystemUsersProvider.getYehidaIrgunit(model.currentMunicipalityId, model.calculatedIdNumber);
                Log.FormLog(yehidot, "getYehidaIrgunit");

                return Ok(yehidot);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("getWorkerYehidaIrgunit")]
        public IHttpActionResult GetWorkerYehidaIrgunit(GetWorkerDataModel model)
        {
            if (model == null) return null;

            WSPostModel wsModel = model.MapToWSPostModel();
            RepresentedWorkerModel worker = model.MapToRepresentedWorker();

            try
            {
                if (!WebSecurity.CheckAuthorizetion(wsModel.idNumber, wsModel.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");

                List<YehidaIrgunitModel> yehidot = SystemUsersProvider.getYehidaIrgunit(wsModel.currentMunicipalityId, worker.idNumber.ToString());

                Log.FormLog(yehidot, "GetWorkerYehidaIrgunit");

                return Ok(yehidot);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("getMyProcessesData")]
        public IHttpActionResult GetMyProcessesData(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model.idNumber, model.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");

                List<ProcessModel> myProcesses = processBll.getMyProcesses(model);

                Log.FormLog(myProcesses, "GetMyProcessesData");

                return Ok(myProcesses);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("getUserRecord")]
        public IHttpActionResult getUserRecord(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model.idNumber, model.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");

                UserModel user = processBll.getUserRecord(model);

                Log.FormLog(user, "getUserRecord");

                return Ok(user);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("GetMyRequestProcesses")]
        public IHttpActionResult GetMyRequestProcesses(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model.idNumber, model.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");

                List<ProcessModel> myReqProcesses = processBll.getMyRequestProcesses(model);

                Log.FormLog(myReqProcesses, "GetMyRequestProcesses");

                return Ok(myReqProcesses);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("getUsers")]
        public IHttpActionResult GetUsers(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model.idNumber, model.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");

                List<MistameshSacharModel> users = processBll.getUsers(model);

                Log.FormLog(users, "GetUsers");

                return Ok(users);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("getAcademicInsts")]
        public IHttpActionResult GetAcademicInsts()
        {
            try
            {
                List<AcademicInstModel> academics = processBll.getAcademicInsts();
                Log.FormLog(academics, "GetAcademicInsts");

                return Ok(academics);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("postVacationForm")]
        public IHttpActionResult PostVacationForm(List<VacationFormModel> models)
        {
            try
            {
                if (models.Count > 0)
                {
                    VacationFormModel model = models.First();//for authorization
                    if (!WebSecurity.CheckAuthorizetion(model.ovedModel.idNumber, model.ovedModel.confirmationToken))
                        throw new FieldAccessException("UserdetailsAccessException");

                    ProcessRCModel res = SystemUsersProvider.PostVacationForm(models);

                    if (res.processRc > 0)
                    {
                        decimal tmId = res.processRc.ToDecimal();//זהות מופע התהליך שהוזנק במערכת השכר
                        foreach (VacationFormModel rec in models)
                        {
                            ProcessModel process = rec.MapToProcess();
                            process.TM_TAHALICH_ID = tmId;
                            processBll.CreateProcess(process);
                        }
                    }

                    Log.FormLog(models, "PostVacationForm models");
                    Log.FormLog(res, "PostVacationForm res");

                    return Ok(res);
                }
                return null;
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("postCourseForm")]
        public IHttpActionResult PostCourseForm(List<CourseFormModel> models)
        {
            try
            {
                if (models.Count > 0)
                {
                    CourseFormModel model = models.First();//for authorization
                    if (!WebSecurity.CheckAuthorizetion(model.ovedModel.idNumber, model.ovedModel.confirmationToken))
                        throw new FieldAccessException("UserdetailsAccessException");

                    ProcessRCModel res = SystemUsersProvider.PostCourseForm(models);

                    if (res.processRc > 0)
                    {
                        decimal tmId = res.processRc.ToDecimal();//זהות מופע התהליך שהוזנק במערכת השכר
                        foreach (CourseFormModel rec in models)
                        {
                            ProcessModel process = rec.MapToProcess();
                            process.TM_TAHALICH_ID = tmId;
                            processBll.CreateProcess(process);
                        }
                    }
                    Log.FormLog(models, "PostVacationForm modles");
                    Log.FormLog(res, "PostVacationForm res");

                    return Ok(res);
                }
                return null;
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("saveProcess/{confirmationToken}/{idNumber}/{calculatedIdNumber}")]
        public IHttpActionResult SaveProcess(ProcessModel model, string confirmationToken, string idNumber, string calculatedIdNumber)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(idNumber, confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");

                ProcessRCModel res = new ProcessRCModel();

                // 1. check for target user change
                if(model.TARGET_USER.idNumber.ToDecimal() != model.TARGET_USERID)
                {
                    if(model.TARGET_USER.ID != model.TARGET_USERID)
                    {
                        Log.FormLog("saveProcess target user changed");

                        processBll.deleteProcessRecordsFromUsers(model, calculatedIdNumber);

                        Log.FormLog("saveProcess target user changed - other processes were deleted");
                        Log.FormLog("saveProcess target user changed - sending emails...");

                        sendNewOwnerForStepEmail(model);
                        sendNewOwnerStateToUserEmail(model);
                    }
                }
                //2. check if a comment added and update if so--OLD
                // 3. check if the process was rejected, and if so - go to java and set process to canceled status
                if(model.STATUS == 2)//rejected
                {

                    Log.FormLog("saveProcess - process were canceled");

                    sendProcessAnswerEmail(model);

                    Log.FormLog("saveProcess - process were canceled - email was sent");
                    //set status 2 to all relevant records (נדחה)
                    processBll.setStatusToProcess(model, calculatedIdNumber);
                    res = SystemUsersProvider.setProcessStatusToCanceled(model, calculatedIdNumber);

                    Log.FormLog("saveProcess - process were canceled - status were updated in java and tik oved");
                }
                // 4. check if the process were approved  - and if so - keep processes logic.
                else if(model.STATUS == 1)//temp approved
                {

                    Log.FormLog("saveProcess - process status is 1");

                    sendProcessAnswerEmail(model);

                    Log.FormLog("saveProcess - process status is 1 - email sent");

                    //set status 1 to all relevant records
                    processBll.setStatusToProcess(model, calculatedIdNumber);

                    Log.FormLog("saveProcess - process status is 1 - status updated in tik oved");

                    // 4. check the current step number with number of steps, and if it is the last step, comlete the process (in java too using tm_tahalich_id and curr_mispar_shalav)
                    // else, set process model to be the next step
                    if (model.CURR_MISPAR_SHALAV == model.SHLAVIM_COUNTER)
                    {
                        //process complete section here
                        res = SystemUsersProvider.completeAProcess(model, calculatedIdNumber);
                        if(res.processRc == 1)
                        {
                            model.STATUS = 3;//אושר סופית והושלם
                            processBll.setStatusToProcess(model, calculatedIdNumber);
                        }
                    }
                    else if(model.CURR_MISPAR_SHALAV < model.SHLAVIM_COUNTER)
                    {
                        //continue process section here
                        model.CURR_MISPAR_SHALAV += 1;

                        Log.FormLog("saveProcess - process status is 1 - mispar shalav incremented");

                        res = SystemUsersProvider.approveAndContinueProcess(model, calculatedIdNumber);//sending the process to java with incremented step

                        Log.FormLog("saveProcess - process status is 1 - approve and continue in java also");

                        //TODO move all the process records to be related to new target users according to the step kvuza:
                        List<StepModel> shlavim = processBll.getTahalichSteps(model.TT_TAHALICH_ID);
                        List<MishtamsheiKvuzaModel> mishtamshim = new List<MishtamsheiKvuzaModel>();
                        foreach(StepModel shalav in shlavim)
                        {
                            if (shalav.misparShalav == model.CURR_MISPAR_SHALAV) 
                            {
                                if (shalav.kvuzatMishtamshimModel.kvuzaType == 1)//kvuza regila 
                                {
                                    Log.FormLog("saveProcess - process status is 1 - next shalav is kvuza regila");
                                    foreach (MishtamsheiKvuzaModel mishtamesh in shalav.kvuzatMishtamshimModel.mishtamsheiKvuzaModel)
                                    {
                                        mishtamshim.Add(mishtamesh);
                                    }
                                }
                                else if(shalav.kvuzatMishtamshimModel.kvuzaType == 2)//kvuza dinamit
                                {
                                    Log.FormLog("saveProcess - process status is 1 - next shalav is kvuza dinamit");
                                    if (shalav.kvuzatMishtamshimModel.mishtamsheiKvuzaModel[0].mishtameshId == 2)//kvuza dinamit is menaalim yeshirim?
                                    {
                                        Log.FormLog("saveProcess - process status is 1 - next shalav is kvuza dinamit - menaalim yeshirim");
                                        //TODO get org_unit_mngrs and set the process for them in DB
                                        //1. get yehida irgunit
                                        GetMngrsModel getMngrsModel = new GetMngrsModel();
                                        WSPostModel wsModel = new WSPostModel();
                                        wsModel.calculatedIdNumber = calculatedIdNumber;
                                        wsModel.confirmationToken = confirmationToken;
                                        wsModel.currentMunicipalityId = model.RASHUT;
                                        wsModel.idNumber = idNumber;
                                        List<YehidaIrgunitModel> yehidot = GetYehidaIrgunitOfUser(wsModel);
                                        getMngrsModel.yehida = yehidot != null ? yehidot : new List<YehidaIrgunitModel>();
                                        getMngrsModel.model = wsModel;

                                        //2.get unit mngrs
                                        List<UnitManagerModel> mngrs = getManagersOftheUser(getMngrsModel);
                                        
                                        //3. add them to mishtamshim list
                                        if(mngrs != null)
                                        {
                                            foreach(UnitManagerModel mngr in mngrs)
                                            {
                                                MishtamsheiKvuzaModel mishtamesh = new MishtamsheiKvuzaModel();
                                                mishtamesh.mishtameshId = mngr.idNumber;
                                                mishtamesh.rashut = mngr.rashutNumber;
                                                mishtamshim.Add(mishtamesh);
                                            }
                                        }
                                        Log.FormLog("saveProcess - process status is 1 - next shalav is kvuza dinamit - menaalim yeshirim - updating tik oved records");
                                    }
                                    else
                                    {
                                        //TODO read the tm_taha_mishtamshim_dinamiim table of rashut, get the mishtamesh id as type to read by
                                        //for each mishtamesh dinami create the process record
                                        List<int> types = new List<int>();
                                        foreach (MishtamsheiKvuzaModel mishtamesh in shalav.kvuzatMishtamshimModel.mishtamsheiKvuzaModel)
                                        {
                                            types.Add(mishtamesh.mishtameshId);
                                        }

                                        Log.FormLog(types, "saveProcess - process status is 1 - next shalav is kvuza dinamit - types");

                                        List <MishtemaeshDinamiModel> dinamicUsers = SystemUsersProvider.getMishtamshimDinamim(shalav.rashut, model.RASHUT, types);
                                        if(dinamicUsers != null)
                                        {
                                            foreach(MishtemaeshDinamiModel dinamicUser in dinamicUsers)
                                            {
                                                MishtamsheiKvuzaModel mishtamesh = new MishtamsheiKvuzaModel();
                                                mishtamesh.mishtameshId = dinamicUser.zehut;
                                                mishtamesh.rashut = dinamicUser.rashut;
                                                mishtamshim.Add(mishtamesh);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        processBll.updateKvuzatMishtamshim(model, mishtamshim);

                        if(res.processRc == 1)//notifications in kad system are not been checked!!!
                        {
                            processBll.incrementToNextStep(model);
                        }
                    }
                    
                }
                else if (model.STATUS == 3 && model.CURR_MISPAR_SHALAV == model.SHLAVIM_COUNTER)//final approve
                {
                    Log.FormLog("saveProcess - process status is 3 - completing process...");
                    sendProcessAnswerEmail(model);
                    processBll.setStatusToProcess(model, calculatedIdNumber);
                    res = SystemUsersProvider.completeAProcess(model, calculatedIdNumber);
                    Log.FormLog(res, "saveProcess - process status is 3 - completing process res of java");
                }
                if (!string.IsNullOrEmpty(model.COMMENTS))
                {
                    Log.FormLog("saveProcess - updating process comments in tik oved");
                    processBll.updateComments(model, calculatedIdNumber);
                }

                var myProcesses = processBll.getMyProcesses(calculatedIdNumber);

                Log.FormLog(myProcesses, "saveProcess - after all: myProcesses");

                return Ok(myProcesses);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        #region private
        private List<UnitManagerModel> getManagersOftheUser(GetMngrsModel model)
        {
            if (model == null) return null;

            WSPostModel wsModel = model.MapToWSPostModel();
            List<YehidaIrgunitModel> yehidot = model.MapToYehidaIrgunit();

            try
            {
                if (!WebSecurity.CheckAuthorizetion(wsModel.idNumber, wsModel.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");

                List<UnitManagerModel> unitManagers = processBll.getUserUnitMngrs(yehidot, wsModel.currentRashutId, wsModel.calculatedIdNumber);

                return unitManagers;
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return null;
            }
        }
        private List<YehidaIrgunitModel> GetYehidaIrgunitOfUser(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model.idNumber, model.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");

                List<YehidaIrgunitModel> yehidot = SystemUsersProvider.getYehidaIrgunit(model.currentMunicipalityId, model.calculatedIdNumber);
                return yehidot;
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return null;
            }
        }
        private string getTargetUsername(decimal userId)
        {
            return processBll.getTargetUsername(userId);
        }
        private void sendProcessAnswerEmail(ProcessModel model)
        {
            //start section of send email of the user request result
            string template = string.Empty;
            string ans = model.STATUS == 3 && model.CURR_MISPAR_SHALAV == model.SHLAVIM_COUNTER ? "הבקשה אושרה" :
                model.STATUS == 1 && model.CURR_MISPAR_SHALAV < model.SHLAVIM_COUNTER ? "הבקשה אושרה זמנית, והועברה להמשך טיפול הגורמים הרלונטיים" : "הבקשה נדחתה";

            MailDataModel data = new MailDataModel
            {
                userid = model.USERID,
                target_userid = model.TARGET_USERID,
                user_name = model.OVED_NESU_BAKASHA != null && model.OVED_NESU_BAKASHA > 0 ? model.OVED_NESU_BAKASHA_NAME : model.USERMODEL.firstName + " " + model.USERMODEL.lastName,
                target_user_name = model.OVED_NESU_BAKASHA != null && model.OVED_NESU_BAKASHA > 0 ? model.OVED_NESU_BAKASHA_NAME : model.TARGET_USER != null ? model.TARGET_USER.firstName + " " + model.TARGET_USER.lastName : getTargetUsername(model.TARGET_USERID),
                by_user_name = model.TARGET_USER != null ? model.TARGET_USER.firstName + " " + model.TARGET_USER.lastName : getTargetUsername(model.TARGET_USERID),
                tahalich_name = model.PROCESS_DESCRIPTION,
                from_date = model.FROM_DATE,
                to_date = model.TO_DATE,
                description = model.PROCESS_TYPE != 3 ? model.CONTENT1 : model.CONTENT4,//סיבת בקשת חופשה או תוכנית קורס
                answer = ans,
                reason = string.IsNullOrEmpty(model.COMMENTS) ? "ללא סיבה" : model.COMMENTS
            };

            Log.FormLog(data, "MailDataModel");
            RouteData routedata = HttpContext.Current.Request.RequestContext.RouteData;
            template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "ProcessAnswer", data);

            string theEmail = model.OVED_NESU_BAKASHA != null && model.OVED_NESU_BAKASHA > 0 ? getOvedNesuEmail(model.OVED_NESU_BAKASHA) : model.USERMODEL.email;

            // send the mail
            ms.Send(theEmail/*"itayi@ladpc.co.il"*/, template, false, "tikoved.co.il תשובה לבקשה", null, "", "");
        }
        private void sendNewOwnerForStepEmail(ProcessModel model)
        {
            //get the new owner into USERMODEL 
            UserModel newOwner = processBll.getNewOwnerUserModel(model.TARGET_USERID);
            
            string template = string.Empty;
            string ans = model.STATUS == 3 && model.CURR_MISPAR_SHALAV == model.SHLAVIM_COUNTER ? "הבקשה אושרה" :
                model.STATUS == 1 && model.CURR_MISPAR_SHALAV < model.SHLAVIM_COUNTER ? "הבקשה אושרה זמנית, והעברה להמשך טיפול הגורמים הרלונטיים" :
                model.STATUS == 0 ? "ממתין להחלטתך" : "הבקשה נדחתה";

            MailDataModel data = new MailDataModel
            {
                userid = model.USERID,
                target_userid = getRelevantId(model),
                user_name = newOwner.firstName + " " + newOwner.lastName,
                target_user_name = model.OVED_NESU_BAKASHA != null && model.OVED_NESU_BAKASHA > 0 ? model.OVED_NESU_BAKASHA_NAME : model.USERMODEL.firstName + " " + model.USERMODEL.lastName,
                by_user_name = model.USERMODEL.firstName + " " + model.USERMODEL.lastName,
                tahalich_name = model.PROCESS_DESCRIPTION,
                from_date = model.FROM_DATE,
                to_date = model.TO_DATE,
                description = model.PROCESS_TYPE != 3 ? model.CONTENT1 : model.CONTENT4,//סיבת בקשת חופשה או תוכנית קורס
                answer = ans,
                reason = "טרם"
            };

            Log.FormLog(data, "MailDataModel");
            RouteData routedata = HttpContext.Current.Request.RequestContext.RouteData;
            template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "NewOwnerForStepEmail", data);

            // send the mail
            ms.Send(newOwner.email/*"itayi@ladpc.co.il"*/, template, false, "tikoved.co.il העברת בעלות לבקשה", null, "", "");
        }
        private void sendNewOwnerStateToUserEmail(ProcessModel model)
        {
            //get the new owner into USERMODEL 
            UserModel newOwner = processBll.getNewOwnerUserModel(model.TARGET_USERID);

            string template = string.Empty;
            MailDataModel data = new MailDataModel
            {
                userid = model.USERID,
                target_userid = newOwner.idNumber.ToDecimal(),
                user_name = model.OVED_NESU_BAKASHA != null && model.OVED_NESU_BAKASHA > 0 ? model.OVED_NESU_BAKASHA_NAME : model.USERMODEL.firstName + " " + model.USERMODEL.lastName,
                target_user_name = newOwner.firstName + " " + newOwner.lastName,
                by_user_name = model.USERMODEL.firstName + " " + model.USERMODEL.lastName,
                tahalich_name = model.PROCESS_DESCRIPTION,
                from_date = model.FROM_DATE,
                to_date = model.TO_DATE,
                description = model.PROCESS_TYPE != 3 ? model.CONTENT1 : model.CONTENT4,//סיבת בקשת חופשה או תוכנית קורס
                answer = "ממתין להחלטה",
                reason = "טרם"
            };

            Log.FormLog(data, "MailDataModel");
            RouteData routedata = HttpContext.Current.Request.RequestContext.RouteData;
            template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "NewOwnerStateEmail", data);

            // send the mail
            ms.Send(model.USERMODEL.email/*"itayi@ladpc.co.il"*/, template, false, "tikoved.co.il בקשה בטיפול", null, "", "");
        }
        private decimal getRelevantId(ProcessModel model)
        {
            if (model.OVED_NESU_BAKASHA != null)
            {
                if (model.OVED_NESU_BAKASHA > 0)
                {
                    return (decimal)model.OVED_NESU_BAKASHA;
                }
                else return model.USERMODEL.idNumber.ToDecimal();//processBll.getZehut(model.TARGET_USERID);
            }
            else return model.USERMODEL.idNumber.ToDecimal();//processBll.getZehut(model.TARGET_USERID);
        }
        private string getOvedNesuEmail(decimal? ovedId)
        {
            if(ovedId != null)
            {
                return processBll.getEmployeeEmail(ovedId);
            }
            return string.Empty;
        }
        #endregion
    }
}

    
   