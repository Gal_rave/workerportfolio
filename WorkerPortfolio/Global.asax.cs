﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Threading;
using System.Web.Routing;
using System.Web.Security;
using System.Globalization;
using System.Web.Http.Dispatcher;
using System.Web.Http.Controllers;

using Logger;
using DataAccess;
using DataAccess.Extensions;
using CustomMembership;
using CustomMembership.Classes;
using DataAccess.Models;
using WorkerPortfolio.Controllers;
using WorkerPortfolio.App_Start;
using System.Configuration;

namespace WorkerPortfolio
{
    public class MvcApplication : HttpApplication
    {
        private static readonly int sbvn = ConfigurationManager.AppSettings["SolutionBuildVersionNumber"].ToInt();

        protected void Application_Start()
        {
            HttpContext.Current.Application.Add("SolutionBuildVersionNumber", sbvn);
            HttpContext.Current.Application.Add("TicketExpirationTime", ConfigurationManager.AppSettings["TicketExpirationTime"].ToInt());
            HttpContext.Current.Application.Add("MinAdminImmunity", 70);

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            ///adds global error handels for api
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerSelector), new HttpNotFoundAwareDefaultHttpControllerSelector(GlobalConfiguration.Configuration));
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpActionSelector), new HttpNotFoundAwareControllerActionSelector());

            RepositoryUtil.SetDbContextType();
            HttpContext.Current.Application.Add("SecureUserCookieName", string.Format(".MVCCSUCN{0}", sbvn.ToString()));
            HttpContext.Current.Application.Add("SecureMunicipalityCookieName", string.Format(".MVCCSMCN{0}", sbvn.ToString()));
            HttpContext.Current.Application.Add("SecurePasswordEncryptionCookieName", string.Format(".MVCSPECN{0}", sbvn.ToString()));
            HttpContext.Current.Application.Add("SecureTicketExpirationCookieName", string.Format(".MVCTET{0}", sbvn.ToString()));
            HttpContext.Current.Application.Add("culturalSettings", "he-IL");

            ///disable mvc header
            MvcHandler.DisableMvcResponseHeader = true;
        }
        /// <summary>
        /// if user is logedin then `Current.User` is set;
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[HttpContext.Current.Application.Get("SecureUserCookieName").ToString()];
            if (authCookie != null && !string.IsNullOrWhiteSpace(authCookie.Value))
            {
                try
                {
                    var expirationTime = DateTime.Now.AddMinutes(HttpContext.Current.Application.Get("TicketExpirationTime").ToInt());

                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    if (string.IsNullOrWhiteSpace(authTicket.UserData) || authTicket.Expired)
                    {
                        throw new HttpException(401, "Unauthorized");
                    }
                    HttpContext.Current.User = new CustomPrincipal(authTicket.Name, authTicket.UserData.ToClass<CustomPrincipalSerializeModel>());

                    authTicket = new FormsAuthenticationTicket(5, authTicket.Name, DateTime.Now, expirationTime, false, authTicket.UserData);
                    authCookie.Value = FormsAuthentication.Encrypt(authTicket);
                    Response.Cookies.Add(authCookie);

                    authCookie = null;
                    authCookie = Request.Cookies[HttpContext.Current.Application.Get("SecureTicketExpirationCookieName").ToString()];
                    if (authCookie == null)
                    {
                        authCookie = new HttpCookie(HttpContext.Current.Application.Get("SecureTicketExpirationCookieName").ToString());
                        authCookie.Secure = !ConfigurationManager.AppSettings["isDebugeMode"].ToBool();
                    }
                    authCookie.Value = CryptoJSDecryption.EncriptString(expirationTime.ToString("MM-dd-yyyy HH:mm:ss"));
                    authCookie.Expires = expirationTime.AddMinutes(1);
                    Response.Cookies.Add(authCookie);
                }
                catch (Exception ex)
                {
                    ///if en error occurreds delete all cookies an re try!
                    ex.LogError();
                    foreach (string key in Request.Cookies.AllKeys)
                    {
                        HttpCookie c = Request.Cookies.Get(key);
                        c.Expires = DateTime.Now.AddMonths(-1);
                        Response.AppendCookie(c);
                    }
                }
            }
        }
        /// <summary>
        /// add encription key to request
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            HttpContext.Current.Response.Headers.Remove("X-FRAME-OPTIONS");
            HttpContext.Current.Response.Headers.Add("X-FRAME-OPTIONS", "SAMEORIGIN");

            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(HttpContext.Current.Application.Get("culturalSettings").ToString());
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(HttpContext.Current.Application.Get("culturalSettings").ToString());
            try
            {
                HttpCookie cookie = Request.Cookies[HttpContext.Current.Application.Get("SecurePasswordEncryptionCookieName").ToString()];
                if (cookie == null || string.IsNullOrWhiteSpace(cookie.Value))
                {
                    CryptoModel model = CryptoJSDecryption.GenerateCrypto();
                    cookie = new HttpCookie(HttpContext.Current.Application.Get("SecurePasswordEncryptionCookieName").ToString(), string.Format("{0},{1}", model.key, model.iv));
                    cookie.Expires = DateTime.Now.AddDays(7);
                    Response.Cookies.Add(cookie);
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
        }
        /// <summary>
        /// add encription key to ssesion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Session_Start(Object sender, EventArgs e)
        {
            try
            {
                HttpCookie cookie = Request.Cookies[HttpContext.Current.Application.Get("SecurePasswordEncryptionCookieName").ToString()];
                if (cookie == null || string.IsNullOrWhiteSpace(cookie.Value))
                {
                    CryptoModel model = CryptoJSDecryption.GenerateCrypto();
                    cookie = new HttpCookie(HttpContext.Current.Application.Get("SecurePasswordEncryptionCookieName").ToString(), string.Format("{0},{1}", model.key, model.iv));
                    cookie.Expires = DateTime.Now.AddDays(7);
                    Response.Cookies.Add(cookie);
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
        }
        /// <summary>
        /// application error handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_Error(Object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();

            HttpContext.Current.Response.Headers.Remove("X-FRAME-OPTIONS");
            HttpContext.Current.Response.Headers.Add("X-FRAME-OPTIONS", "SAMEORIGIN");

            HttpContextWrapper contextWrapper = new HttpContextWrapper(this.Context);
            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "Error");
            routeData.Values.Add("action", "Index");

            IController controller = new ErrorController();
            RequestContext requestContext = new RequestContext(contextWrapper, routeData);
            controller.Execute(requestContext);
            Response.End();
        }

        //protected void applcation_presendrequestheaders(Object sender, EventArgs e)
        //{
        //    HttpContext.Current.Response.Headers.Remove("Server");
        //}
        //protected void Application_End(Object sender, EventArgs e)
        //{
        //    if (sender == null) { }
        //    if (e == null) { }
        //}
        //protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        //{
        //    if (sender == null) { }
        //    if (e == null) { }
        //}
        //protected void Application_Error(Object sender, EventArgs e)
        //{
        //    if (sender == null) { }
        //    if (e == null) { }
        //}
        //protected void Application_Init(Object sender, EventArgs e)
        //{
        //    SystemUsersProvider.startTimer();
        //}
        //protected void Application_EndRequest(Object sender, EventArgs e)
        //{
        //    if (sender == null) { }
        //    if (e == null) { }
        //}
        //protected void Application_Disposed(Object sender, EventArgs e)
        //{
        //    if (sender == null) { }
        //    if (e == null) { }
        //}
        //protected void Application_PreRequestHandlerExecute(Object sender, EventArgs e)
        //{
        //    if (sender == null) { }
        //    if (e == null) { }
        //}
        //protected void Application_PostRequestHandlerExecute(Object sender, EventArgs e)
        //{
        //    if (sender == null) { }
        //    if (e == null) { }
        //}
        //protected void Applcation_PreSendRequestHeaders(Object sender, EventArgs e)
        //{
        //    if (sender == null) { }
        //    if (e == null) { }
        //}
        //protected void Application_PreSendContent(Object sender, EventArgs e)
        //{
        //    var url = HttpContext.Current.Request.RawUrl;

        //    if (sender == null) { }
        //    if (e == null) { }
        //}
        //protected void Application_AcquireRequestState(Object sender, EventArgs e)
        //{
        //    if (sender == null) { }
        //    if (e == null) { }
        //}
        //protected void Application_ReleaseRequestState(Object sender, EventArgs e)
        //{
        //    if (sender == null) { }
        //    if (e == null) { }
        //}
        //protected void Application_ResolveRequestCache(Object sender, EventArgs e)
        //{
        //    if (sender == null) { }
        //    if (e == null) { }
        //}
        //protected void Application_UpdateRequestCache(Object sender, EventArgs e)
        //{
        //    if (sender == null) { }
        //    if (e == null) { }
        //}
        //protected void Application_AuthorizeRequest(Object sender, EventArgs e)
        //{
        //    if (sender == null) { }
        //    if (e == null) { }
        //}        
    }
}
