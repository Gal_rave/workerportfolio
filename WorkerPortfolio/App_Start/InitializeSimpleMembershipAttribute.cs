﻿using System;
using System.Threading;
using System.Web.Mvc;
using System.Collections.Specialized;
using System.Configuration;
using System.Web.Configuration;

using CustomMembership;
using Logger;

namespace WorkerPortfolio.App_Start
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class InitializeSimpleMembershipAttribute : ActionFilterAttribute
    {
        private static CustomMembershipInitializer _initializer;
        private static object _initializerLock = new object();
        private static bool _isInitialized;

        public InitializeSimpleMembershipAttribute()
        {
            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Ensure ASP.NET Simple Membership is initialized only once per app start
            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
        }

        private class CustomMembershipInitializer
        {
            public CustomMembershipInitializer()
            {
                string configPath = "~/web.config";
                Configuration NexConfig = WebConfigurationManager.OpenWebConfiguration(configPath);
                MembershipSection section = (MembershipSection)NexConfig.GetSection("system.web/membership");
                
                ProviderSettingsCollection settings = section.Providers;
                NameValueCollection membershipParams = settings[section.DefaultProvider].Parameters;
                NameValueCollection config = membershipParams;
                CustomMembershipProvider.Initialize("PortfolioEntities", config);
            }
        }
    }
}