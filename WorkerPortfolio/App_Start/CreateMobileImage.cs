﻿using DataAccess.Extensions;
using DataAccess.Models;
using DataAccess.BLLs;
using AsyncHelpers;
using Logger;
using Mail;

using System;
using System.IO;
using System.Web;
using System.Linq;
using System.Drawing;
using iTextSharp.text;
using System.Threading;
using System.Diagnostics;
using System.Web.Routing;
using iTextSharp.text.pdf;
using System.Globalization;
using System.Configuration;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WorkerPortfolio.App_Start
{
    public class CreateMobileImage
    {
        #region class vars
        private Regex regex = new Regex("[^a-zA-Z0-9]");

        private readonly string baseContent = "~/Content";
        private readonly string baseFolder = "uploads";
        private readonly string birthdayGreetingFolder = "B_D";
        private readonly string birthdayGreetingImage = @"Images\bd.jpg";
        private string ghostScriptPath = string.Empty;
        private string uploads = string.Empty;
        private bool isDebugeMode;
        private string defaultSiteEmail;
        private string debugeModeEmails;

        private MembershipProviderBll membershipBll;
        private FilesBll filesBll;
        private MailSender ms;
        #endregion

        public CreateMobileImage()
        {
            membershipBll = new MembershipProviderBll();
            filesBll = new FilesBll();
            this.ms = new MailSender();

            ghostScriptPath = ConfigurationManager.AppSettings["GhostScriptDllPath"];
            isDebugeMode = ConfigurationManager.AppSettings["isDebugeMode"].ToBool();
            debugeModeEmails = ConfigurationManager.AppSettings["debugeModeEmails"];
            defaultSiteEmail = ConfigurationManager.AppSettings["smtpDefaultEmail"];
            defaultSiteEmail = !string.IsNullOrWhiteSpace(defaultSiteEmail) ? defaultSiteEmail : "tikoved@iula.org.il";
            createUploadsFolder();

            birthdayGreetingFolder = Path.Combine(HttpContext.Current.Server.MapPath(baseContent), birthdayGreetingFolder);
            birthdayGreetingImage = Path.Combine(HttpContext.Current.Server.MapPath(baseContent), birthdayGreetingImage);
            FolderExtensions.CreateFolder(birthdayGreetingFolder);
        }

        public Task<decimal> sendUpcMail(TaskFactory factory, HttpContext context, UpcMailsExtension upc, string View)
        {
            CancellationTokenSource cts = new CancellationTokenSource(AsyncFunctionCaller.timeout);
            TaskCompletionSource<decimal> taskCompletionSource = new TaskCompletionSource<decimal>();
            Task<decimal> task = null;

            task = factory.StartNew<decimal>(() =>
            {
                HttpContext.Current = context;
                try
                {
                    using (cts.Token.Register(() => taskCompletionSource.TrySetCanceled(cts.Token)))
                    {
                        ms.SendMail(this.isDebugeMode ? this.debugeModeEmails : upc.EMAIL, View, false, string.Format("תלוש שכר לחודש {0} {1}", upc.CURRENTPAYMONTH.ToString("MMMM", CultureInfo.CurrentUICulture), upc.CURRENTPAYMONTH.Year), new Dictionary<string, byte[]> { { string.Format("{0}_{1}.pdf", "תלוש_שכר_", upc.CURRENTPAYMONTH.ToString("yyyy_MM")), AddPasswordToPdf(upc.FILEBLOB, upc.IDNUMBER) } }, "", defaultSiteEmail);
                        return upc.ID;
                    }
                }
                catch (ThreadAbortException tae)
                {
                    if (cts.IsCancellationRequested && cts.Token.IsCancellationRequested)
                    {
                        throw new TaskCanceledException(task);
                    }
                    cts.Token.ThrowIfCancellationRequested();
                    cts.Cancel(true);

                    tae.LogError();
                    return -1;
                }
                catch (TaskCanceledException tce)
                {
                    if (cts.IsCancellationRequested && cts.Token.IsCancellationRequested)
                    {
                        throw new TaskCanceledException(task);
                    }
                    cts.Token.ThrowIfCancellationRequested();
                    cts.Cancel(true);

                    tce.LogError();
                    return -1;
                }
                catch (OperationCanceledException oce)
                {
                    if (cts.IsCancellationRequested && cts.Token.IsCancellationRequested)
                    {
                        throw new TaskCanceledException(task);
                    }
                    cts.Token.ThrowIfCancellationRequested();
                    cts.Cancel(true);

                    oce.LogError();
                    return -1;
                }
                catch (Exception ex)
                {
                    if (cts.IsCancellationRequested && cts.Token.IsCancellationRequested)
                    {
                        throw new TaskCanceledException(task);
                    }
                    cts.Token.ThrowIfCancellationRequested();
                    cts.Cancel(true);

                    ex.LogError();
                    return -1;
                }
            }, cts.Token);
            return task;
        }
        public string RenderPayCheckEmailView(UpcMailsExtension upc)
        {
            try
            {
                UserCredentialsModel ucm = new UserCredentialsModel
                {
                    fullName = upc.FULLNAME,
                    idNumber = upc.IDNUMBER,
                    loginDate = upc.CURRENTPAYMONTH,
                    calculatedIdNumber = upc.CALCULATEDIDNUMBER,
                    confirmationToken = upc.CONFIRMATIONTOKEN,
                    currentMunicipalityId = upc.MUNICIPALITYID.ToInt(),
                    email = upc.EMAIL
                };
                RouteData routedata = new RouteData();

                return MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "UserPayCheckMailTemplate", ucm);
            }
            catch (Exception ex)
            {
                ex.LogError();
                return string.Empty;
            }
        }
        public bool SendPayCheckEmail(UpcMailsExtension upc)
        {
            try
            {
                string template = string.Empty;
                UserCredentialsModel ucm = new UserCredentialsModel
                {
                    fullName = upc.FULLNAME,
                    idNumber = upc.IDNUMBER,
                    loginDate = upc.CURRENTPAYMONTH,
                    calculatedIdNumber = upc.CALCULATEDIDNUMBER,
                    confirmationToken = upc.CONFIRMATIONTOKEN,
                    currentMunicipalityId = upc.MUNICIPALITYID.ToInt(),
                    email = upc.EMAIL
                };
                RouteData routedata = new RouteData();

                // send the mail
                template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "UserPayCheckMailTemplate", ucm);

                ms.SendMail(this.isDebugeMode ? this.debugeModeEmails : upc.EMAIL, template, false, string.Format("תלוש שכר לחודש {0} {1}", upc.CURRENTPAYMONTH.ToString("MMMM", CultureInfo.CurrentUICulture), upc.CURRENTPAYMONTH.Year), new Dictionary<string, byte[]> { { string.Format("{0}_{1}.pdf", "תלוש_שכר_", upc.CURRENTPAYMONTH.ToString("yyyy_MM")), AddPasswordToPdf(upc.FILEBLOB, upc.IDNUMBER) } }, "", defaultSiteEmail);

                Log.PcuLog(new { email = upc.EMAIL, idNumber = upc.IDNUMBER }, "SendPayCheckEmail");
                return true;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return false;
            }
        }
        public bool SendPayCheckEmail(UserImageModel image, PayCheckUsersByMailModel pcu)
        {
            try
            {
                string template = string.Empty;
                UserCredentialsModel ucm = new UserCredentialsModel
                {
                    firstName = pcu.user.firstName,
                    lastName = pcu.user.lastName,
                    fullName = string.Format("{0} {1}", pcu.user.firstName, pcu.user.lastName),
                    idNumber = pcu.user.idNumber,
                    loginDate = image.fileDate.Value,
                    calculatedIdNumber = pcu.user.calculatedIdNumber,
                    municipalities = pcu.user.municipalities,
                    confirmationToken = pcu.user.membership.confirmationToken,
                    currentMunicipalityId = pcu.municipalityId,
                    email = string.Join(",", pcu.user.emailByMunicipality.Where(e => e.municipalityId == pcu.municipalityId).Select(e => e.email).ToList())
                };
                RouteData routedata = new RouteData();

                // send the mail
                if (!this.isDebugeMode)
                {
                    template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "UserPayCheckMailTemplate", ucm);

                    ms.SendMail(ucm.email, template, false, string.Format("תלוש שכר לחודש {0} {1}", image.fileDate.Value.ToString("MMMM", CultureInfo.CurrentUICulture), image.fileDate.Value.Year), new Dictionary<string, byte[]> { { string.Format("{0}_{1}.pdf", "תלוש_שכר_", image.fileDate.Value.ToString("yyyy_MM")), AddPasswordToPdf(image.fileBlob, pcu.user.idNumber) } }, "", defaultSiteEmail);

                    Log.PcuLog(new { email = String.Join(",", ucm.email), idNumber = ucm.idNumber }, "SendPayCheckEmail");
                }
                return true;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return false;
            }
        }
        public bool SendPayCheckEmail(UserImageModel image, PayCheckUsersByMailModel pcu, byte[] form101, int formYear)
        {
            try
            {
                string template = string.Empty;
                UserCredentialsModel ucm = new UserCredentialsModel
                {
                    firstName = pcu.user.firstName,
                    lastName = pcu.user.lastName,
                    fullName = string.Format("{0} {1}", pcu.user.firstName, pcu.user.lastName),
                    idNumber = pcu.user.idNumber,
                    loginDate = image.fileDate.Value,
                    calculatedIdNumber = pcu.user.calculatedIdNumber,
                    municipalities = pcu.user.municipalities,
                    confirmationToken = pcu.user.membership.confirmationToken,
                    currentMunicipalityId = pcu.municipalityId,
                    email = string.Join(",", pcu.user.emailByMunicipality.Where(e => e.municipalityId == pcu.municipalityId).Select(e => e.email).ToList()),
                    currentTerminationDate = new DateTime(formYear, 1, 1),
                    isAllow101 = pcu.user.municipalities.FirstOrDefault(m => m.ID == pcu.municipalityId && m.allow101) != null
                };
                RouteData routedata = new RouteData();
                // send the mail
                if (!this.isDebugeMode)
                {
                    template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "UserPayCheckMailTemplateAnd101Form", ucm);

                    ms.SendMail(ucm.email, template, false, string.Format("תלוש שכר לחודש {0} {1}", image.fileDate.Value.ToString("MMMM", CultureInfo.CurrentUICulture), image.fileDate.Value.Year), new Dictionary<string, byte[]> { { string.Format("{0}_{1}.pdf", "תלוש_שכר_", image.fileDate.Value.ToString("yyyy_MM")), AddPasswordToPdf(image.fileBlob, pcu.user.idNumber) }, { string.Format("טופס_101_לשנת_{0}.pdf", formYear), form101 } }, "", defaultSiteEmail);

                    Log.PcuLog(new { email = String.Join(",", ucm.email), idNumber = ucm.idNumber }, "SendPayCheckEmail");
                }
                return true;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return false;
            }
        }
        public UserImageModel CreateImage(UserImageModel image)
        {
            DateTime NOW = DateTime.Now;
            int index = 0;
            Log.ApplicationLog(string.Format("CreateImage step {2} , image.ID => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, image.ID, index++));

            string tempUserFolder = Path.Combine(uploads, image.ID.ToString());
            string outputPath = Path.Combine(tempUserFolder, "TEMP_");
            string inputPath = Path.Combine(tempUserFolder, string.Format("{0}.pdf", image.ID));

            #region delete/create the folder
            deleteCreateFolder(tempUserFolder);
            #endregion
            Log.ApplicationLog(string.Format("CreateImage step {2} , image.ID => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, image.ID, index++));
            #region save to file
            if (image.fileimageblob == null || image.fileimageblob.Length < 1000)
            {
                try
                {
                    Log.ApplicationLog(string.Format("CreateImage step {2} , image.ID => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, image.ID, index++));
                    File.WriteAllBytes(inputPath, image.fileBlob);
                    Log.ApplicationLog(string.Format("CreateImage step {2} , image.ID => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, image.ID, index++));
                    #region perse pdf into images then combine images
                    perseImagesAndCombine(tempUserFolder, outputPath, inputPath, image.ID.ToString());
                    #endregion
                    Log.ApplicationLog(string.Format("CreateImage step {2} , image.ID => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, image.ID, index++));
                    #region save the new image into DB
                    image.fileimageblob = FolderExtensions.ReadFile(Path.Combine(tempUserFolder, string.Format("{0}.jpeg", image.ID)));
                    Log.ApplicationLog(string.Format("CreateImage step {2} , image.ID => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, image.ID, index++));
                    FolderExtensions.DeletFolder(tempUserFolder);
                    #endregion
                }
                catch (Exception ex)
                {
                    FolderExtensions.DeletFolder(tempUserFolder);
                    throw ex.LogError();
                }
            }
            #endregion
            Log.ApplicationLog(string.Format("CreateImage step {2} , image.ID => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, image.ID, index++));
            return image;
        }
        public UserImageModel CreateImage(UserImageModel image, string idNumber)
        {
            DateTime NOW = DateTime.Now;
            int index = 0;
            Log.ApplicationLog(string.Format("CreateImage step {2} , idNumber => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, idNumber, index++));

            string tempUserFolder = Path.Combine(uploads, idNumber);
            string outputPath = Path.Combine(tempUserFolder, "TEMP_");
            string inputPath = Path.Combine(tempUserFolder, string.Format("{0}.pdf", idNumber));

            #region delete/create the folder
            deleteCreateFolder(tempUserFolder);
            #endregion
            Log.ApplicationLog(string.Format("CreateImage step {2} , idNumber => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, idNumber, index++));
            #region save to file
            if (image.fileimageblob == null || image.fileimageblob.Length < 1000)
            {
                Log.ApplicationLog(string.Format("CreateImage step {2} , idNumber => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, idNumber, index++));
                File.WriteAllBytes(inputPath, image.fileBlob);
                Log.ApplicationLog(string.Format("CreateImage step {2} , idNumber => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, idNumber, index++));
                #region perse pdf into images then combine images
                perseImagesAndCombine(tempUserFolder, outputPath, inputPath, idNumber);
                #endregion
                Log.ApplicationLog(string.Format("CreateImage step {2} , idNumber => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, idNumber, index++));
                #region save the new image into DB
                image.fileimageblob = FolderExtensions.ReadFile(Path.Combine(tempUserFolder, string.Format("{0}.jpeg", idNumber)));
                Log.ApplicationLog(string.Format("CreateImage step {2} , idNumber => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, idNumber, index++));
                FolderExtensions.DeletFolder(tempUserFolder);
                #endregion
            }
            #endregion
            Log.ApplicationLog(string.Format("CreateImage step {2} , idNumber => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, idNumber, index++));
            return image;
        }
        public void EmptyDirectoryEraser(string startLocation = null)
        {
            startLocation = !string.IsNullOrWhiteSpace(startLocation) ? startLocation : Path.Combine(HttpContext.Current.Server.MapPath(baseContent), baseFolder);
            foreach (var directory in Directory.GetDirectories(startLocation))
            {
                EmptyDirectoryEraser(directory);
                if (Directory.GetFiles(directory).Length == 0 && Directory.GetDirectories(directory).Length == 0)
                {
                    Directory.Delete(directory, false);
                }
            }
        }
        public void CleanRuningProcesses()
        {
            try
            {
                IEnumerable<Process> processes = Process.GetProcesses().
                                 Where(pr => pr.ProcessName == "gswin32" || pr.ProcessName == "gswin64");

                foreach (var process in processes)
                {
                    Log.ApplicationLog(new
                    {
                        ProcessName = process.ProcessName,
                        Id = process.Id,
                        HasExited = process.HasExited,
                        Responding = process.Responding,
                        SessionId = process.SessionId,
                        StartTime = process.StartTime,
                        TotalProcessorTime = process.TotalProcessorTime
                    }, "CreateMobileImage");
                    process.Kill();
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
        }
        public void CleanUploadsFolder()
        {
            uploads = Path.Combine(HttpContext.Current.Server.MapPath(baseContent), baseFolder);
            DateTime creationTime = DateTime.Now.AddMinutes(-15);

            DirectoryInfo baseDirectory = new DirectoryInfo(uploads);
            List<DirectoryInfo> directories = baseDirectory.EnumerateDirectories("*", SearchOption.AllDirectories)
                                    .Where(d => d.CreationTime < creationTime)
                                    .OrderBy(d => d.CreationTime).ToList();

            directories.ForEach(d =>
            {
                Log.ApplicationLog(d);
                FolderExtensions.DeletFolder(d.FullName);
            });
        }
        public bool SendBirthdayGreetingEmail(UserModel user)
        {
            try
            {
                string template = string.Empty;
                RouteData routedata = new RouteData();
                ImageModel img = createBirthdayGreeting(user);

                UserCredentialsModel ucm = new UserCredentialsModel
                {
                    firstName = user.firstName,
                    lastName = user.lastName,
                    fullName = string.Format("{0} {1}", user.firstName, user.lastName),
                    idNumber = user.idNumber,
                    municipalities = user.municipalities,
                    email = user.email,
                    confirmationToken = img.name
                };

                template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "BirthdayGreetingTemplate", ucm);

                // send the mail
                if (this.isDebugeMode)
                {
                    user.email = this.debugeModeEmails;
                }

                ms.SendMail(user.email, "", "", template, false, "ברכות ואיחולים ליום הולדתך!", new Dictionary<string, byte[]> { { string.Format("מזל_טוב.{0}", img.type), img.fileBlob } }, "ברכות ואיחולים ליום הולדתך!", defaultSiteEmail);

                Log.ApplicationLog(new { email = user.email, idNumber = user.idNumber }, "SendBirthdayGreetingEmail");
                return true;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return false;
            }
        }
        public bool SendPCUE_ReportMail(ManagerModel manager, DateTime pcDate, DateTime reportDate, MunicipalityExtensionModel municipalityModel, string filePath)
        {
            try
            {
                string template = string.Empty;
                RouteData routedata = new RouteData();

                UserCredentialsModel ucm = new UserCredentialsModel
                {
                    firstName = manager.FirstName,
                    lastName = manager.LastName,
                    fullName = string.Format("{0} {1}", manager.FirstName, manager.LastName),
                    email = manager.Email,
                    loginDate = pcDate
                };

                template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "PcueReportMail", ucm);
                string subject = string.Format("דו`ח שליחת `תלוש במייל`, עבור משכורת - {2}{0}/{1}", pcDate.Month, pcDate.Year, pcDate.Month > 9 ? "" : "0");
                string displayName = string.Format("{0} - דו`ח `תלוש במייל` {1}", municipalityModel.name, reportDate.ToString("dd/MM/yyyy"));
                // send the mail
                ms.Send(this.isDebugeMode ? this.debugeModeEmails : manager.Email, "", this.isDebugeMode ? "" : this.debugeModeEmails, template, false, subject, filePath, displayName, defaultSiteEmail, false);

                Log.PcuLog(new { isDebugeMode = this.isDebugeMode, email = this.isDebugeMode ? this.debugeModeEmails : manager.Email, idNumber = manager.idnumber }, "SendPCUE_ReportMail");
                return true;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return false;
            }
        }

        #region private
        private ImageModel createBirthdayGreeting(UserModel user)
        {
            try
            {
                string name = string.Format(",{0} {1}", user.firstName.Trim(), user.lastName.Trim()).Trim();
                string pdfName = string.Format(@"{0}\{1}.pdf", birthdayGreetingFolder, user.idNumber);
                string imageName = string.Format(@"{0}\{1}.jpeg", birthdayGreetingFolder, user.idNumber);

                File.Copy(birthdayGreetingImage, string.Format(@"{0}\{1}.jpg", birthdayGreetingFolder, user.ID));

                iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(string.Format(@"{0}\{1}.jpg", birthdayGreetingFolder, user.ID));

                FileStream fs = new FileStream(pdfName, FileMode.Create, FileAccess.Write, FileShare.None);
                Document doc = new Document(PageSize.POSTCARD);
                PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                doc.Open();

                ///ad the birthday greeting image
                img.ScaleAbsolute(doc.PageSize);
                img.SetAbsolutePosition(0, 0);
                doc.Add(img);

                ///add name
                PdfContentByte cb = writer.DirectContent;
                BaseFont bf = BaseFont.CreateFont(@"C:\Windows\Fonts\ARIALUNI.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 14);
                PdfPTable table = new PdfPTable(1);
                table.DefaultCell.NoWrap = false;
                table.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                table.TotalWidth = 200f;
                PdfPCell pdfpcell = new PdfPCell(new Phrase(name, font));
                pdfpcell.BorderWidth = 0;
                //Ensure that wrapping is on, otherwise Right to Left text will not display 
                pdfpcell.NoWrap = false;
                table.AddCell(pdfpcell);
                table.WriteSelectedRows(0, -1, 21, 197, cb);

                doc.NewPage();
                doc.Close();
                writer.Dispose();
                fs.Dispose();

                pdfToImage(imageName, pdfName);

                ImageModel image = new ImageModel { fileBlob = FolderExtensions.ReadFile(imageName), municipalityId = membershipBll.GetUserDefaultMunicipality(user.idNumber), name = regex.Replace(Guid.NewGuid().ToString(), "_"), type = "jpeg", userId = user.ID };
                filesBll.AddImage(image);

                return image;
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        private void pdfToImage(string output, string input)
        {
            try
            {
                Process proc = new Process();
                proc.EnableRaisingEvents = true;
                DateTime Start = DateTime.Now;
                bool inProcess = true;
                string ars = string.Format(@"-dBATCH -dNOPAUSE -dSAFER -sDEVICE=jpeg -dJPEGQ=90 -r400x400  -dDOINTERPOLATE -sOutputFile={0} {1}", output, input);

                proc.StartInfo.FileName = ghostScriptPath;
                proc.StartInfo.Arguments = ars;
                proc.StartInfo.CreateNoWindow = true;
                proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proc.Exited += new EventHandler(myProcess_Exited);
                proc.Start();

                while (inProcess)
                {
                    Thread.Sleep(250);
                    proc.Refresh();
                    if (proc.HasExited)
                    {
                        inProcess = false;
                    }

                    double totalSeconds = DateTime.Now.Subtract(Start).TotalSeconds;
                    if (totalSeconds > 20 && inProcess)
                    {
                        inProcess = false;
                        proc.Kill();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        private void createUploadsFolder()
        {
            uploads = Path.Combine(HttpContext.Current.Server.MapPath(baseContent), baseFolder);
            FolderExtensions.CreateFolder(uploads);
        }
        private void deleteCreateFolder(string path)
        {
            FolderExtensions.DeletFolder(path);
            Thread.Sleep(4);
            FolderExtensions.CreateFolder(path);
            Thread.Sleep(4);
        }
        private void perseImagesAndCombine(string path, string output, string input, string idNumber)
        {
            Process proc = new Process();
            proc.EnableRaisingEvents = true;
            DateTime Start = DateTime.Now;
            bool inProcess = true;
            try
            {
                string ars = string.Format(@"-dBATCH -dNOPAUSE -dSAFER -sPAPERSIZE=a0 -sDEVICE=jpeg -dJPEGQ=90 -r400x400  -dDOINTERPOLATE -sOutputFile={0}-%03d.jpeg {1}", output, input);

                proc.StartInfo.FileName = ghostScriptPath;
                proc.StartInfo.Arguments = ars;
                proc.StartInfo.CreateNoWindow = true;
                proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proc.Exited += new EventHandler(myProcess_Exited);
                proc.Start();

                while (inProcess)
                {
                    Thread.Sleep(250);
                    proc.Refresh();
                    if (proc.HasExited)
                    {
                        inProcess = false;
                    }

                    double totalSeconds = DateTime.Now.Subtract(Start).TotalSeconds;
                    if (totalSeconds > 60 && inProcess)
                    {
                        inProcess = false;
                        proc.Kill();
                    }
                }
                Thread.Sleep(4);
                CombineImages(path, idNumber);
            }
            catch (Exception ex)
            {
                proc.Kill();
                throw ex.LogError();
            }
        }
        private void myProcess_Exited(object sender, EventArgs e)
        {
        }
        private void CombineImages(string tempUserFolder, string idNumber)
        {
            List<String> imagesList = getAllImages(tempUserFolder);
            int width = 795;
            int height = 1125 * imagesList.Count;
            int imageIndex = 0;
            System.Drawing.Image image;

            string Last_jpg = Path.Combine(tempUserFolder, string.Format("{0}.jpeg", idNumber));
            Bitmap Final_img = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(Final_img);
            g.Clear(Color.Black);

            imagesList.ForEach(i =>
            {
                image = System.Drawing.Image.FromFile(i);
                g.DrawImage(image, new Point(0, (1125 * imageIndex)));
                imageIndex++;
                image.Dispose();
            });

            g.Dispose();
            Final_img.Save(Last_jpg, System.Drawing.Imaging.ImageFormat.Jpeg);
            Final_img.Dispose();
        }
        private List<String> getAllImages(string JpgFolder)
        {
            DirectoryInfo Folder;
            FileInfo[] Images;

            Folder = new DirectoryInfo(JpgFolder);
            Images = Folder.GetFiles("*.jpeg", SearchOption.AllDirectories);
            List<String> imagesList = new List<String>();

            for (int i = 0; i < Images.Length; i++)
            {
                imagesList.Add(String.Format(@"{0}/{1}", JpgFolder, Images[i].Name));
            }

            return imagesList;
        }
        private byte[] AddPasswordToPdf(byte[] input, string idNUmber)
        {
            try
            {
                FolderExtensions.CreateFolder(Path.Combine(uploads, idNUmber));
                string path = Path.Combine(uploads, idNUmber, string.Format("{0}_encrypted.pdf", idNUmber));
                string pass = idNUmber;
                Stream stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None);
                PdfReader reader = new PdfReader(input);
                PdfEncryptor.Encrypt(reader, stream, true, pass, pass, PdfWriter.ALLOW_SCREENREADERS);
                byte[] encrypted = FolderExtensions.ReadFile(path);
                FolderExtensions.DeletFolder(Path.Combine(uploads, idNUmber));
                return encrypted;
            }
            catch (Exception ex)
            {
                FolderExtensions.DeletFolder(Path.Combine(uploads, idNUmber));
                throw ex.LogError();
            }
        }
        #endregion
    }
}