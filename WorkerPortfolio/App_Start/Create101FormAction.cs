﻿using System;
using System.IO;
using System.Web;
using System.Linq;
using iTextSharp.text;
using System.Web.Routing;
using System.Diagnostics;
using iTextSharp.text.pdf;
using System.Configuration;
using System.IO.Compression;
using System.Collections.Generic;

using Mail;
using Logger;
using SftpConnector;
using DataAccess.BLLs;
using DataAccess.Enums;
using CustomMembership;
using DataAccess.Models;
using WebServiceProvider;
using DataAccess.Mappers;
using DataAccess.Extensions;

namespace WorkerPortfolio.App_Start
{
    public class Create101FormAction
    {
        private UserBll userbll;
        private FormBll formBll;

        private readonly string baseContent = "~/Content";
        private readonly string baseFolder = "uploads";
        private readonly string uploads = string.Empty;
        private string pdfFiels = "PDFs";
        private string tempPdfFiels = "Temps";
        private string ghostScriptPath = string.Empty;
        private string ghostScriptViewjpegPath = string.Empty;
        private readonly bool isDebugeMode;

        private static string DefaultSiteEmail
        {
            get
            {
                return CustomMembershipProvider.DefaultSiteEmail;
            }
        }
        private static readonly string smsMsg = @"שלום {0} {1},
טופס 101 ששלחת התקבל בהצלחה,
קוד הגירסה הינו:
{2}";

        public Create101FormAction()
        {
            uploads = Path.Combine(HttpContext.Current.Server.MapPath(baseContent), baseFolder);
            FolderExtensions.CreateFolder(uploads);

            pdfFiels = Path.Combine(HttpContext.Current.Server.MapPath(baseContent), pdfFiels);

            tempPdfFiels = Path.Combine(pdfFiels, tempPdfFiels);
            FolderExtensions.CreateFolder(tempPdfFiels);

            ghostScriptPath = ConfigurationManager.AppSettings["GhostScriptDllPath"];
            ghostScriptViewjpegPath = ConfigurationManager.AppSettings["GhostScriptViewjpegPath"];

            this.isDebugeMode = ConfigurationManager.AppSettings["isDebugeMode"].ToBool();

            this.userbll = new UserBll();
            this.formBll = new FormBll();
        }
        public TaxFromModel GetBaseFormData(int userId, int municipalityId, int formYear)
        {
            Log.PcuLog("start GetBaseFormData");
            UserModel user = userbll.GetUserByID(userId);

            MunicipalityContactModel municipalitycontactmodel;

            int FactoryId = MegaWSProvider.GetMifal(user.calculatedIdNumber.ToInt(), municipalityId, new DateTime(formYear, 1, 1));

            municipalitycontactmodel = formBll.GetMunicipalityContactByFactoryId(municipalityId, FactoryId, true);

            bool isRetirement = FormsProvider.GetRetirementStatus(new WSPostModel() { calculatedIdNumber = user.calculatedIdNumber, currentMunicipalityId = FactoryId, currentRashutId = user.municipalities.FirstOrDefault(m => m.ID == municipalityId).rashut.ToInt() });

            EmployerModel employer = new EmployerModel()
            {
                address = string.Format("{0} {1} {2}", municipalitycontactmodel.city, municipalitycontactmodel.street, municipalitycontactmodel.houseNumber),
                deductionFileID = municipalitycontactmodel.deductionFileID,
                displayEmail = !string.IsNullOrWhiteSpace(municipalitycontactmodel.email),
                email = municipalitycontactmodel.email,
                emailEmployee = true,
                emailEmployer = !string.IsNullOrWhiteSpace(municipalitycontactmodel.email),
                name = municipalitycontactmodel.municipality.name,
                phoneNumber = municipalitycontactmodel.phone,
                mifal = municipalitycontactmodel.mifal.ToInt(),
                rashut = municipalitycontactmodel.rashut.HasValue ? municipalitycontactmodel.rashut.Value : municipalitycontactmodel.mifal.Value
            };

            TaxFromModel taxfrommodel = formBll.GetBaseForm101Data(userId, formYear);

            taxfrommodel.employer = employer;
            taxfrommodel.employee.startWorkTime = formBll.GetUserStartWorkDate(taxfrommodel.employee.ID, municipalityId);

            ///the form type to use
            taxfrommodel.FormType = getFormType(formYear, isRetirement);

            Log.PcuLog("end GetBaseFormData");
            return taxfrommodel;
        }
        public byte[] CreateForm101(TaxFromModel form101)
        {
            Log.PcuLog("start createForm101");
            BaseFont baseFont = BaseFont.CreateFont(@"C:\Windows\Fonts\arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            PdfReader pdfReader = new PdfReader(Path.Combine(pdfFiels, string.Format("Base101Form{0}.pdf", ((FormTypeEnum)form101.FormType).ToString())));
            string newFileName = string.Format("form101_{2}_{0}_{1}.pdf", form101.year, form101.employee.idNumber, form101.FormType);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(Path.Combine(tempPdfFiels, newFileName), FileMode.Create));

            Document document = new Document(PageSize.A4);
            int index = 0;
            try
            {
                document.Open();

                #region clear fields data
                AcroFields fields = pdfStamper.AcroFields;
                foreach (var field in fields.Fields)
                {
                    fields.SetFieldProperty(field.Key, "textsize", 10f, null);
                    switch (fields.GetFieldType(field.Key))
                    {
                        case AcroFields.FIELD_TYPE_CHECKBOX:
                            fields.SetField(field.Key, "Off");
                            break;
                        case AcroFields.FIELD_TYPE_TEXT:
                            fields.SetField(field.Key, "");
                            break;
                    }
                    fields.SetDirection(field.Key, 1);
                    fields.SetColor(field.Key, 0);
                }
                fields.AddSubstitutionFont(baseFont);
                #endregion

                #region general form data
                fields.SetDirection("year", 2);
                fields.SetField("year", form101.year.ToString());
                pdfStamper.AcroFields.RemoveField("signatureDataUrl");
                pdfStamper.AcroFields.RemoveField("signatureDate");
                pdfStamper.AcroFields.RemoveField("warning");
                pdfStamper.AcroFields.RemoveField("version");
                #endregion

                #region employer
                fields.SetField("employer.name", form101.employer.name);
                fields.SetField("employer.address", form101.employer.address);
                fields.SetDirection("employer.phoneNumber", 2);
                fields.SetField("employer.phoneNumber", string.Format("{0}-{1}", form101.employer.phoneNumber.GetPhonePrefix(), form101.employer.phoneNumber.GetPhoneNumber()));
                fields.SetDirection("employer.deductionFileID", 3);
                fields.SetField("employer.deductionFileID", form101.employer.deductionFileID);
                #endregion

                #region employee
                fields.SetDirection("employee.cell", 3);

                fields.SetDirection("employee.phone", 3);

                fields.SetDirection("employee.idNumber", 2);
                fields.SetField("employee.idNumber", form101.employee.idNumber);
                fields.SetField("user.pageIdNumber", form101.employee.idNumber);
                fields.SetField("employee.lastName", form101.employee.lastName);
                fields.SetField("employee.firstName", form101.employee.firstName);
                if (form101.employee.immigrationDate.HasValue)
                {
                    fields.SetFieldProperty("employee.immigrationDate", "textsize", 10f, null);
                    fields.SetDirection("employee.immigrationDate", 2);
                    fields.SetField("employee.immigrationDate", dateToString(form101.employee.immigrationDate.Value, form101.FormType));
                }
                if (form101.employee.birthDate.HasValue)
                {
                    fields.SetFieldProperty("employee.birthDate", "textsize", 10f, null);
                    fields.SetDirection("employee.birthDate", 2);
                    fields.SetField("employee.birthDate", dateToString(form101.employee.birthDate.Value, form101.FormType));
                }
                else
                {
                    pdfStamper.AcroFields.RemoveField("employee.birthDate");
                }

                if (!string.IsNullOrWhiteSpace(form101.employee.cell) && form101.employee.cell.Length > 7)
                {
                    fields.SetFieldProperty("employee.cell", "textsize", 10f, null);
                    fields.SetField("employee.cell", form101.employee.cell.GetPhoneNumber(true));
                    fields.SetField("employee.cellPrefix", form101.employee.cell.GetPhonePrefix(true));
                }
                if (!string.IsNullOrWhiteSpace(form101.employee.phone) && form101.employee.phone.Length > 7)
                {
                    fields.SetFieldProperty("employee.phone", "textsize", 10f, null);
                    fields.SetField("employee.phone", form101.employee.phone.GetPhoneNumber());
                    fields.SetField("employee.phonePrefix", form101.employee.phone.GetPhonePrefix());
                }

                fields.SetField("employee.street", form101.employee.street);
                fields.SetDirection("employee.houseNumber", 2);
                fields.SetField("employee.houseNumber", form101.employee.houseNumber.ToString());
                fields.SetField("employee.city", form101.employee.city.NullableString());
                fields.SetDirection("employee.zipCode", 3);
                fields.SetField("employee.zipCode", form101.employee.zip.NullableString());
                fields.SetDirection("employee.hmoName", 3);
                fields.SetField("employee.hmoName", form101.employee.hmoName.NullableString());
                fields.SetDirection("employee.email", 3);
                fields.SetField("employee.email", form101.employee.email.NullableString());

                fields.SetCheckBox("employee.kibbutzResident_false", form101.employee.kibbutzResident, false);
                fields.SetCheckBox("employee.kibbutzResident_true", form101.employee.kibbutzResident, true);

                fields.SetCheckBox("employee.gender.male", form101.employee.gender, true);
                fields.SetCheckBox("employee.gender.female", form101.employee.gender, false);

                fields.SetCheckBox("employee.maritalStatus.Single", form101.employee.maritalStatus == 1, true);
                fields.SetCheckBox("employee.maritalStatus.Married", form101.employee.maritalStatus == 2, true);
                fields.SetCheckBox("employee.maritalStatus.divorcee", form101.employee.maritalStatus == 3, true);
                fields.SetCheckBox("employee.maritalStatus.widower", form101.employee.maritalStatus == 4, true);
                fields.SetCheckBox("employee.maritalStatus.separated", form101.employee.maritalStatus == 5, true);

                fields.SetCheckBox("employee.ilResident", form101.employee.ilResident, true);
                fields.SetCheckBox("employee.ilResident_true", form101.employee.ilResident, true);
                fields.SetCheckBox("employee.ilResident_false", form101.employee.ilResident, false);

                fields.SetCheckBox("employee.hmoMember_false", form101.employee.hmoMember, false);
                fields.SetCheckBox("employee.hmoMember_true", form101.employee.hmoMember, true);
                #endregion

                #region children
                foreach (ChildModel child in form101.children.Where(c => c.birthDate.checkChildAge(19)))
                {
                    fields.SetField("children[" + index + "].name", child.firstName);
                    fields.SetField("children[" + index + "].idNumber", child.idNumber);
                    fields.SetField("children[" + index + "].birthDate", dateToString(child.birthDate, form101.FormType));

                    fields.SetCheckBox("children[" + index + "].possession", child.possession, true);
                    fields.SetCheckBox("children[" + index + "].childBenefit", child.childBenefit, true);

                    index++;
                    if (index > 12 || ((FormTypeEnum)form101.FormType == FormTypeEnum._Retired && index > 3))
                        break;
                }
                #endregion

                #region spouse
                if (form101.employee.maritalStatus == 2)
                {
                    fields.SetField("spouse.idNumber", form101.spouse.idNumber);
                    fields.SetField("spouse.lastName", form101.spouse.lastName);
                    fields.SetField("spouse.firstName", form101.spouse.firstName);

                    if (form101.spouse.immigrationDate.HasValue)
                    {
                        fields.SetFieldProperty("spouse.birthDate", "textsize", 10f, null);
                        fields.SetField("spouse.birthDate", dateToString(form101.spouse.immigrationDate.Value, form101.FormType));
                    }
                    fields.SetFieldProperty("spouse.immigrationDate", "textsize", 10f, null);
                    fields.SetField("spouse.immigrationDate", dateToString(form101.spouse.birthDate.Value, form101.FormType));

                    if (form101.spouse.hasIncome.HasValue)
                    {
                        fields.SetCheckBox("spouse.income.true", form101.spouse.hasIncome.Value, true);
                        fields.SetCheckBox("spouse.income.false", form101.spouse.hasIncome.Value, false);
                    }
                    else
                    {
                        pdfStamper.AcroFields.RemoveField("spouse.income.true");
                        pdfStamper.AcroFields.RemoveField("spouse.income.false");
                    }

                    fields.SetCheckBox("spouse.incomeType1", form101.spouse.incomeType.ToInt() == 1, true);
                    fields.SetCheckBox("spouse.incomeType2", form101.spouse.incomeType.ToInt() == 2, true);
                }
                #endregion

                #region taxExemption
                fields.SetCheckBox("taxExemption.infantchildren", form101.taxExemption.infantchildren, true);
                if (form101.taxExemption.infantchildren)
                {
                    if ((FormTypeEnum)form101.FormType != FormTypeEnum._2018)
                    {
                        int ThisYear = form101.children.ThisYear(form101.year);
                        if (ThisYear > 0)
                        {
                            fields.SetCheckBox("taxExemption.infantchildren_ThisYear", true, true);
                            fields.SetField("taxExemption.infantchildren_ThisYear_Counter", ThisYear.ToString());
                        }
                        int oneToFive = form101.children.OneToFive(form101.year);
                        if (oneToFive > 0)
                        {
                            fields.SetCheckBox("taxExemption.infantchildren_1to5", true, true);
                            fields.SetField("taxExemption.infantchildren_1to5_Counter", oneToFive.ToString());
                        }
                    }
                    if ((FormTypeEnum)form101.FormType == FormTypeEnum._2018)
                    {
                        int threeOrBorn = form101.children.ThreeOrBorn(form101.year);
                        int oneOrTow = form101.children.OneOrTow(form101.year);
                        if (threeOrBorn > 0)
                        {
                            fields.SetCheckBox("taxExemption.infantchildren3", true, true);
                            fields.SetField("taxExemption.infantchildren3count", threeOrBorn.ToString());
                        }
                        if (oneOrTow > 0)
                        {
                            fields.SetCheckBox("taxExemption.infantchildren12", true, true);
                            fields.SetField("taxExemption.infantchildren12count", oneOrTow.ToString());
                        }
                    }
                }

                fields.SetCheckBox("taxExemption.childrenInCare", form101.taxExemption.childrenInCare, true);
                if (form101.taxExemption.childrenInCare)
                {
                    int oneToFive = form101.children.OneToFive(form101.year);
                    if (oneToFive > 0)
                    {
                        fields.SetCheckBox("taxExemption.childrenInCare15", true, true);
                        fields.SetField("taxExemption.childrenInCare15count", oneToFive.ToString());
                    }

                    if ((FormTypeEnum)form101.FormType != FormTypeEnum._2018)
                    {
                        int ThisYear = form101.children.ThisYear(form101.year);
                        if (ThisYear > 0)
                        {
                            fields.SetCheckBox("taxExemption.childrenInCare_ThisYear", true, true);
                            fields.SetField("taxExemption.childrenInCare_ThisYear_Counter", ThisYear.ToString());
                        }
                        int sixTo17 = form101.children.sixToSeventeen(form101.year);
                        if (sixTo17 > 0)
                        {
                            fields.SetCheckBox("taxExemption.childrenInCare_6to17", true, true);
                            fields.SetField("taxExemption.childrenInCare_6to17_Counter", sixTo17.ToString());
                        }
                        int Eighteen = form101.children.Eighteen(form101.year);
                        if (Eighteen > 0)
                        {
                            fields.SetCheckBox("taxExemption.childrenInCare_18", true, true);
                            fields.SetField("taxExemption.childrenInCare_18_Counter", Eighteen.ToString());
                        }
                    }

                    if ((FormTypeEnum)form101.FormType == FormTypeEnum._2018)
                    {
                        int anderNineteen = form101.children.AnderNineteen(form101.year);
                        int bornOr18 = form101.children.BornOrEighteen(form101.year);

                        if (anderNineteen > 0)
                        {
                            fields.SetCheckBox("taxExemption.childrenInCare19", true, true);
                            fields.SetField("taxExemption.childrenInCare19count", anderNineteen.ToString());
                        }
                        if (bornOr18 > 0)
                        {
                            fields.SetCheckBox("taxExemption.childrenInCare18", true, true);
                            fields.SetField("taxExemption.childrenInCare18count", bornOr18.ToString());
                        }
                    }
                }

                fields.SetCheckBox("taxExemption.alimonyParticipates", form101.taxExemption.alimonyParticipates, true);

                if ((FormTypeEnum)form101.FormType != FormTypeEnum._2018) fields.SetCheckBox("taxExemption.incompetentChild", form101.taxExemption.incompetentChild, true);

                fields.SetCheckBox("taxExemption.blindDisabled", form101.taxExemption.blindDisabled, true);
                fields.SetCheckBox("taxExemption.exAlimony", form101.taxExemption.exAlimony, true);

                fields.SetCheckBox("taxExemption.exServiceman", form101.taxExemption.exServiceman, true);
                if (form101.taxExemption.exServiceman)
                {
                    fields.SetFieldProperty("taxExemption.servicemanStartDate", "textsize", 10f, null);
                    fields.SetFieldProperty("taxExemption.servicemanEndDate", "textsize", 10f, null);
                    fields.SetDirection("taxExemption.servicemanStartDate", 1);
                    fields.SetDirection("taxExemption.servicemanEndDate", 1);
                    if (form101.taxExemption.serviceStartDate.HasValue) fields.SetField("taxExemption.servicemanStartDate", dateToString(form101.taxExemption.serviceStartDate.Value, 0));
                    if (form101.taxExemption.serviceEndDate.HasValue) fields.SetField("taxExemption.servicemanEndDate", dateToString(form101.taxExemption.serviceEndDate.Value, 0));
                }

                fields.SetCheckBox("taxExemption.graduation", form101.taxExemption.graduation, true);

                fields.SetCheckBox("taxExemption.isImmigrant", form101.taxExemption.isImmigrant, true);
                if (form101.taxExemption.isImmigrant)
                {
                    fields.SetCheckBox("taxExemption.immigration.immigrantStatus1", form101.taxExemption.immigrationStatus == 1, true);
                    fields.SetCheckBox("taxExemption.immigration.immigrantStatus2", form101.taxExemption.immigrationStatus == 2, true);

                    if (form101.taxExemption.immigrationStatusDate.HasValue)
                    {
                        fields.SetFieldProperty("taxExemption.immigrantStatusDate", "textsize", 10f, null);
                        fields.SetField("taxExemption.immigrantStatusDate", dateToString(form101.taxExemption.immigrationStatusDate.Value, 0));
                    }
                    if (form101.taxExemption.immigrationNoIncomeDate.HasValue)
                    {
                        fields.SetFieldProperty("taxExemption.noIncomeDate", "textsize", 10f, null);
                        fields.SetField("taxExemption.noIncomeDate", dateToString(form101.taxExemption.immigrationNoIncomeDate.Value, 0));
                    }
                }

                fields.SetCheckBox("taxExemption.isResident", form101.taxExemption.isResident, true);
                if (form101.taxExemption.isResident)
                {
                    fields.SetField("taxExemption.settlementCredits.settlement", form101.taxExemption.settlement);
                    if (form101.taxExemption.startSettlementDate.HasValue)
                    {
                        fields.SetFieldProperty("taxExemption.settlementCredits.startResidentDate", "textsize", 10f, null);
                        fields.SetField("taxExemption.settlementCredits.startResidentDate", dateToString(form101.taxExemption.startSettlementDate.Value, 0));
                    }
                }
                #endregion

                #region close
                // flatten form fields and close document
                pdfStamper.FreeTextFlattening = true;
                pdfStamper.FormFlattening = true;

                document.Close();
                pdfStamper.Close();
                pdfReader.Close();
                #endregion

                #region delete the form  and return data only
                compress(newFileName, string.Format("{0}_{1}.pdf", form101.year, form101.employee.idNumber));

                byte[] pdfBytes = FolderExtensions.ReadFile(Path.Combine(tempPdfFiels, newFileName));
                FolderExtensions.DeleteFile(Path.Combine(tempPdfFiels, newFileName));
                #endregion
                Log.PcuLog("end createForm101");
                return pdfBytes;
            }
            catch (Exception e)
            {
                document.Close();
                pdfStamper.Close();
                pdfReader.Close();
                FolderExtensions.DeleteFile(Path.Combine(tempPdfFiels, newFileName));
                throw e.LogError();
            }
        }
        public void ReCreateFull101Form(Form101DataExtensionModel model, bool isLast = false)
        {
            try
            {
                TaxFromExtensionModel form101 = model.jsonData.ToClass<TaxFromExtensionModel>();
                form101.employee.ID = model.userId;

                List<decimal> uploadImages = form101.connectedImages.Select(i => i.ID.ToDecimal()).ToList();
                List<UserImageModel> images = formBll.GetFormImages(uploadImages);

                string template = string.Empty;
                List<string> UserEmails = new List<string>();
                string _email = string.Empty;
                SmsNotificationModel smsNotification = new SmsNotificationModel();

                _email = model.user.email.SplitDistinct(form101.employee.email);
                _email = _email.SplitDistinct(string.Join(",", model.user.emailByMunicipality.Where(e => e.municipalityId == model.municipalityId).Select(e => e.email).ToList()));
                UserEmails.AddRange(_email.Split(','));

                #region set up email/sms notification template
                if (UserEmails.Count > 0 && !this.isDebugeMode)
                {///setup mail template
                    UserCredentialsModel data = new UserCredentialsModel
                    {
                        confirmationToken = form101.version,
                        firstName = model.user.firstName,
                        lastName = model.user.lastName,
                        idNumber = form101.year.ToString(),
                        fullName = string.Format("{0} {1}", model.user.firstName, model.user.lastName),
                    };
                    RouteData routedata = HttpContext.Current.Request.RequestContext.RouteData;
                    try
                    {
                        routedata.Values.Remove("token");
                        routedata.Values.Add("token", model.user.membership.confirmationToken);
                    }
                    catch { }
                    template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "Form101SeccessEmail", data);
                }
                else if (!string.IsNullOrWhiteSpace(model.user.cell) && !this.isDebugeMode)
                {///setup sms template
                    string msg = string.Format(smsMsg, model.user.firstName, model.user.lastName, form101.version);
                    smsNotification = model.user.MapToSms(new string[] { (string.IsNullOrWhiteSpace(form101.employee.cell) ? form101.employee.phone : form101.employee.cell) }, msg);
                }
                #endregion
                Log.LogLoad("email/sms");

                #region set up rashut / mifal
                int _rashut = form101.employer.rashut;
                int _mifal = form101.employer.mifal;
                if (_rashut < 1 && _mifal > 1)
                {
                    _rashut = _mifal;
                    form101.employer.rashut = _rashut;
                }
                if (_rashut < 1 && _mifal < 1)
                {
                    MunicipalityContactModel municipalitycontactmodel = formBll.GetMunicipalityContactByFactoryId(
                                                form101.municipalityId,
                                                CustomMembershipProvider.GetUserFactory(form101.employee.ID, form101.municipalityId)
                                                , false);

                    _mifal = municipalitycontactmodel.mifal.ToInt();
                    form101.employer.mifal = _mifal;
                    _rashut = municipalitycontactmodel.rashut.ToInt();
                    form101.employer.rashut = _rashut;

                    if (_rashut < 1 && _mifal > 1)
                    {
                        _rashut = _mifal;
                        form101.employer.rashut = _mifal;
                    }
                }
                if (_rashut < 1)
                    throw new NullReferenceException("NullRashutReferenceException");
                #endregion
                Log.LogLoad("rashut/mifal");

                #region commpresion
                string newFileName = string.Format("form101_{0}_{1}.pdf", form101.year, form101.employee.idNumber);
                File.WriteAllBytes(Path.Combine(tempPdfFiels, newFileName), model.theForm.fileBlob);
                compress(newFileName, string.Format("{0}_{1}.pdf", form101.year, form101.employee.idNumber));
                byte[] pdfBytes = FolderExtensions.ReadFile(Path.Combine(tempPdfFiels, newFileName));

                model.theForm.fileBlob = pdfBytes;
                FolderExtensions.DeleteFile(Path.Combine(tempPdfFiels, newFileName));
                #endregion
                Log.LogLoad("commpresion");

                #region add attachments
                string path = string.Format("{0}.pdf", form101.employee.idNumber);
                var ms = new MemoryStream();
                var archive = new ZipArchive(ms, ZipArchiveMode.Create, true);
                List<UserImageModel> caseAttachmentModels = new List<UserImageModel>();
                byte[] uploded = new byte[1];

                images = checkUplodedFiles(form101, images);
                if (images.Count > 0)
                {
                    uploded = concatAndAddContent(images.Where(i => i.fileName.Contains(".pdf")).Select(i => i.fileBlob).ToList());

                    uploded = concatAndAddContent(new List<byte[]> { model.theForm.fileBlob, uploded });
                    caseAttachmentModels.Add(new UserImageModel() { fileName = path, fileBlob = uploded });
                }
                else
                {
                    uploded = concatAndAddContent(new List<byte[]> { model.theForm.fileBlob });
                    caseAttachmentModels.Add(new UserImageModel() { fileName = path, fileBlob = uploded });
                }
                #endregion
                Log.LogLoad("add Attachment");

                int exceptionId = 0;
                #region send zip to archive
                try
                {
                    if (!this.isDebugeMode)
                        SftpTransfer.UplodTransferZip(caseAttachmentModels, string.Format("{0}-T101-{1}-T_{2}.zip", _rashut.Pad(3), form101.year, form101.employee.idNumber));
                }
                catch (Exception e)
                {
                    e.LogError();
                    exceptionId -= 1;
                }
                #endregion
                Log.LogLoad("send zip");

                #region post the data into WS
                try
                {
                    if (!this.isDebugeMode)
                        FormsProvider.PostTofes101ToWs(form101, true, isLast);
                }
                catch (Exception e)
                {
                    e.LogError();
                    exceptionId -= 2;
                }
                #endregion
                Log.FormLog("post data");

                #region save form state
                model.deliveryStatus = exceptionId;
                if (!this.isDebugeMode)
                    formBll.UpdateForm101(model);
                #endregion
                Log.FormLog("save form state");

                #region send mail / sms
                if (exceptionId == 0 && !this.isDebugeMode)
                {
                    if ((UserEmails.Count > 0))
                    {///send email
                        Log.FormLog("send email");
                        MailSender mailsender = new MailSender();
                        mailsender.SendMail(String.Join(",", UserEmails), template, false, string.Format("טופס 101 לשנת {0} התקבל במערכת", form101.year), new Dictionary<string, byte[]> { { string.Format("{0}_{1}.pdf", DateTime.Now.ToString("dd_MM_yyyy"), model.user.idNumber), uploded } }, "", DefaultSiteEmail);
                    }
                    else if (!string.IsNullOrWhiteSpace(model.user.cell))
                    {///send sms
                        Log.FormLog("send sms");
                        SmsManagerProvider.SendSMS(smsNotification);
                    }
                }
                #endregion
                Log.FormLog("End SendFormToWS");
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }

        #region private
        private void compress(string newFileName, string oldFileName)
        {
            ///using itex
            PdfReader reader = new PdfReader(Path.Combine(tempPdfFiels, newFileName));
            PdfStamper stamper = new PdfStamper(reader, new FileStream(Path.Combine(tempPdfFiels, oldFileName), FileMode.Create), PdfWriter.VERSION_1_5);
            int pageNum = reader.NumberOfPages;
            for (int i = 1; i <= pageNum; i++)
            {
                reader.SetPageContent(i, reader.GetPageContent(i), 9, true);
            }

            reader.RemoveUnusedObjects();
            reader.RemoveUsageRights();
            stamper.SetFullCompression();
            stamper.Close();
            reader.Close();
            FolderExtensions.DeleteFile(Path.Combine(tempPdfFiels, newFileName));

            String ars = string.Format(@" -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dCompatibilityLevel=1.3 -dEncodeColorImages=true -sOutputFile={1} {0}", Path.Combine(tempPdfFiels, oldFileName), Path.Combine(tempPdfFiels, newFileName));
            Process proc = new Process();
            proc.StartInfo.FileName = ghostScriptPath;
            proc.StartInfo.Arguments = ars;
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.Start();
            proc.WaitForExit(60000);
            proc.Close();
            FolderExtensions.DeleteFile(Path.Combine(tempPdfFiels, oldFileName));
        }
        private int getFormType(int year, bool retired)
        {
            if (retired)
                return (int)FormTypeEnum._Retired;
            return year > 2018 ? (int)FormTypeEnum._2019 : (int)FormTypeEnum._2019;
        }
        private string dateToString(DateTime date, int FormType)
        {
            switch (FormType)
            {
                case 1:
                    return date.ToString("dd/MM/yyyy");
                case 2:
                case 3:
                    return date.ToString("ddMMyyyy");
                default:
                    return date.ToString("dd/MM/yyyy");
            }
        }
        private List<UserImageModel> checkUplodedFiles(TaxFromExtensionModel form101, List<UserImageModel> images)
        {
            List<UserImageModel> removeImages;
            if (!form101.taxExemption.blindDisabled)//blindDisabledFiles_1
            {
                removeImages = returnRemoveImages("blinddisabledfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.alimonyParticipates)//alimonyParticipatesFiles
            {
                removeImages = returnRemoveImages("alimonyparticipatesfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.exServiceman)//servicemanFiles
            {
                removeImages = returnRemoveImages("servicemanfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxCoordination.requestCoordination)//coordinationFiles / reasonFiles
            {
                removeImages = returnRemoveImages("coordinationfiles", images);
                removeImages.ForEach(i => images.Remove(i));
                removeImages = returnRemoveImages("reasonfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.spouseNoIncome)//spouseNoIncomeFiles
            {
                removeImages = returnRemoveImages("spousenoincomefiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.incompetentChild)//incompetentChildFiles
            {
                removeImages = returnRemoveImages("incompetentchildfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.isImmigrant)//immigrantCertificateFiles / returningResidentFiles
            {
                removeImages = returnRemoveImages("immigrantcertificatefiles", images);
                removeImages.ForEach(i => images.Remove(i));
                removeImages = returnRemoveImages("returningresidentfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.exAlimony)//exAlimonyFiles
            {
                removeImages = returnRemoveImages("exalimonyfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.graduation)//graduationFiles
            {
                removeImages = returnRemoveImages("graduationfiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            if (!form101.taxExemption.isResident)//isaFiles
            {
                removeImages = returnRemoveImages("isafiles", images);
                removeImages.ForEach(i => images.Remove(i));
            }
            return images;
        }
        private List<UserImageModel> returnRemoveImages(string imageType, List<UserImageModel> images)
        {
            return images.Where(i => i.fileName.ToLower().StartsWith(imageType)).ToList();
        }
        private byte[] concatAndAddContent(List<byte[]> pdfByteContent)
        {
            using (var ms = new MemoryStream())
            {
                using (var doc = new Document())
                {
                    using (var copy = new PdfSmartCopy(doc, ms))
                    {
                        doc.Open();

                        //Loop through each byte array
                        foreach (var p in pdfByteContent)
                        {
                            //Create a PdfReader bound to that byte array
                            using (var reader = new PdfReader(p))
                            {
                                int pageNum = reader.NumberOfPages;
                                for (int i = 1; i <= pageNum; i++)
                                {
                                    reader.SetPageContent(i, reader.GetPageContent(i), 9, true);
                                }
                                reader.RemoveAnnotations();
                                reader.RemoveUnusedObjects();
                                reader.RemoveUsageRights();

                                //Add the entire document instead of page-by-page
                                copy.AddDocument(reader);
                            }
                        }
                        doc.Close();
                    }
                }
                //Return just before disposing
                return ms.ToArray();
            }
        }
        #endregion private
    }
}