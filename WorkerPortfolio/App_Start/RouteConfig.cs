﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WorkerPortfolio
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ConfirmUPCE",
                url: "{controller}/{action}/{municipalityId}/{confirmationToken}",
                defaults: new { controller = "Authentication", action = "ConfirmUPCE", municipalityId = UrlParameter.Optional, confirmationToken = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ActionRedirect",
                url: "{controller}/{action}/{redirectType}/{municipalityId}/{confirmationToken}",
                defaults: new { controller = "Authentication", action = "AuthenticationReDirect", redirectType = UrlParameter.Optional, municipalityId = UrlParameter.Optional, confirmationToken = UrlParameter.Optional }
            );
        }
    }
}
