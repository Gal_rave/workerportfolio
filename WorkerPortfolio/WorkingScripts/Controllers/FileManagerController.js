﻿(function () {
    angular.module('WorkerPortfolioApp').controller('FileManagerController', FileManagerController);

    FileManagerController.$inject = ['$scope', '$rootScope', 'ngDialog', '$timeout', 'toastrService', 'FileManager', 'FileUploader', 'fileStructure', 'options'];

    function FileManagerController($scope, $rootScope, ngDialog, $timeout, toastrService, FileManager, FileUploader, fileStructure, options) {
        /* ***controller vars*** */
        $scope.inputValidation = {
            required: true,
            minlength: 2,
            maxlength: 25
        }
        $scope.WSPostModel = $rootScope.getWSPostModel();
        $scope.options = options;
        $scope.fileStructure = fileStructure.data;
        $scope.fileStructure.selected = true;
        $scope.fileStructure.isSelected = true;
        $scope.directoryModel = {
            name: '',
            parentId: null,
            municipalityId: $scope.WSPostModel.currentMunicipalityId,
            subDirectories: [],
            images: []
        };
        ///selected file to work on
        $scope.isSelected = $scope.fileStructure;
        $scope.tempSelected = null;
        $scope.SelectedFile = null;
        $scope.tempFile = null;
        $scope.CurrentDragOver = null;
        ///hold list of selected folder images
        $scope.filesList = $scope.fileStructure.images;
        /* ***controller vars*** */

        /* ***folder functions*** */
        ///manage the folder selection
        $scope.selectMaster = function (mf) {
            mf.selected = true;
            mf.isSelected = true;
            mf.Editing = false;
            mf.Eding = false;

            $scope.isSelected = mf;

            $scope.traverseFolders($scope.fileStructure, mf.ID);
            $scope.CurrentDragOver = null;
            $scope.iteratDropOver(0);

            $scope.filesList = mf.selected ? mf.images : null;
            $scope.parseImages();
        }
        $scope.subscribe('UpdateSelectedFolder', function (sF) {
            sF.Editing = false;
            sF.Eding = false;
            $scope.isSelected = sF;

            $scope.traverseFolders($scope.fileStructure, sF.ID);
            $scope.CurrentDragOver = null;
            $scope.iteratDropOver(0);

            $scope.filesList = sF.selected ? sF.images : null;
            $scope.parseImages();
        });

        ///delete folder AND subfolders AND files
        $scope.deleteFolder = function (selected) {
            if (selected == null || selected === $scope.fileStructure)
                return void [0];

            ngDialog.openConfirm({
                template: '\
                <p><b>האם למחוק את</b> "' + selected.name + '"?<\p>\
                <p><b>שים לב</b>, מחיקת תיקייה תמחוק את כל תיקיות המשנה והקבצים!</p>\
                <div class="ngdialog-buttons">\
                    <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">בטל</button>\
                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(' + selected.ID + ')">מחק</button>\
                </div>',
                plain: true,

                scope: $scope,
            }).then(function (success) {
                var selectedFolder = $scope.getSingleSelectedFolderById(success);
                FileManager.DeleteFolder({ wsModel: $scope.WSPostModel, directory: selectedFolder })
                    .then(function successCallback(res) {
                        $scope.fileStructure = res.data;
                        $scope.filesList = $scope.fileStructure.images;
                        $scope.SetSelected(selectedFolder.parentId);

                        toastrService.success('התיקיה נמחקה!', '');

                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });

            }, function (error) {
                ///do nothing, user canceld
            });

        }

        ///add new folder
        $scope.addFolder = function (selected) {
            if (selected == null)
                return void [0];

            selected.Eding = true;
            $scope.directoryModel.parentId = selected.ID;
            $scope.directoryModel.name = null;

            $timeout(function () {
                $('[name=directory_' + selected.ID + ']').focus();
            }, 15);
        }
        $scope.addFolderOut = function (folder) {
            var inp = $scope.FileManagerForm['directory_' + folder.ID];
            if (folder.Eding !== true || inp.$invalid || !inp.$valid) {
                $scope.SetSelected(folder.ID);
                return void [0];
            }

            $scope.directoryModel.name = $scope.directoryModel.name === undefined || $scope.directoryModel.name === null || $scope.directoryModel.name.length === 0 ? 'תיקייה חדשה' : $scope.directoryModel.name;

            FileManager.AddFolder({ wsModel: $scope.WSPostModel, directory: $scope.directoryModel })
                .then(function successCallback(res) {
                    $scope.fileStructure = res.data;
                    $scope.filesList = $scope.fileStructure.images;
                    $scope.SetSelected(folder.ID);

                    toastrService.success('הוספה תיקיה חדשה!', '');

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.subscribe('AddNewFolder', function (folder) {
            if (folder.Eding == undefined || folder.Eding !== true)
                return void [0];

            $scope.directoryModel.name = folder.newFolderName;
            $scope.directoryModel.parentId = folder.ID;
            $scope.addFolderOut(folder);
        });

        ///update folder name/parent
        $scope.updateFolder = function (selected) {
            if (selected == null)
                return void [0];

            selected.Editing = true;
            $timeout(function () {
                $('[name=folder_' + selected.ID + ']').focus();
            }, 15);

            $scope.tempSelected = angular.copy(selected);
        }
        $scope.updateFolderOut = function (folder) {
            var inp = $scope.FileManagerForm['folder_' + folder.ID];
            if (folder.Editing !== true || inp.$invalid || !inp.$valid) {
                folder.name = $scope.tempSelected !== undefined && $scope.tempSelected !== null ? $scope.tempSelected.name : folder.name;
                folder.parentId = $scope.tempSelected !== undefined && $scope.tempSelected !== null ? $scope.tempSelected.parentId : folder.parentId;
                $scope.tempSelected = null;
                $scope.SetSelected(folder.ID);
                return void [0];
            }

            FileManager.UpdateFolder({ wsModel: $scope.WSPostModel, directory: folder })
                .then(function successCallback(res) {
                    $scope.fileStructure = res.data;
                    $scope.filesList = $scope.fileStructure.images;
                    $scope.SetSelected(folder.ID);

                    toastrService.success('התיקיה התעדכנה!', '');

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });

        }
        $scope.subscribe('UpdateFolderName', function (folder) {
            if (folder.Editing == undefined || folder.Editing !== true)
                return void [0];

            $scope.updateFolderOut(folder);
        });
        /* ***folder functions*** */

        /* ***image functions*** */
        ///set up img src
        $scope.parseImages = function () {
            if ($scope.filesList == null || $scope.filesList.length < 1)
                return void [0];

            $scope.filesList.filter(function (f) {
                if (f.fileBlobString.indexOf('data:image') < 0)
                    f.fileBlobString = 'data:image/png;base64,' + f.fileBlobString;
            });
        }
        
        ///click on selected img
        $scope.selectFile = function (file) {
            file.selected = !file.selected;
            $scope.SelectedFile = file.selected ? file : null;
            $scope.filesList.filter(function (f) {
                if (f !== file)
                    f.selected = false;
            });
        }
        ///select img and close popup
        $scope.insertFile = function (file) {
            file.selected = true;
            $scope.SelectedFile = file;
            $("#" + $scope.options.field_name).val("/Home/displayImage/" + file.ID);
            ngDialog.close();
        }

        ///etit image
        $scope.aditImage = function (folder) {
            if (folder == null || $scope.SelectedFile == null || !$scope.SelectedFile.selected)
                return void [0];

            $scope.SelectedFile.Editing = true;
            $scope.tempFile = angular.copy($scope.SelectedFile);
            $timeout(function () {
                $('[name=file_' + $scope.SelectedFile.ID + ']').focus();
            }, 15);
        }
        $scope.EditFileOut = function (file) {
            if (file == null || !file.selected || !file.Editing || $scope.SelectedFile === null)
                return void [0];
            var inp = $scope.FileManagerForm['file_' + file.ID];
            if (inp.$invalid || !inp.$valid) {
                file.name = $scope.tempFile.name;
                file.directoryId = $scope.tempFile.directoryId;
                $scope.tempFile = null;

                $scope.selectFile(file);
                return void [0];
            }

            FileManager.UpdateImage({ wsModel: $scope.WSPostModel, image: file })
                .then(function successCallback(res) {
                    res.data.fileBlobString = 'data:image/png;base64,' + res.data.fileBlobString;
                    $scope.isSelected.images[$scope.isSelected.images.indexOf(file)] = res.data;

                    toastrService.success('התמונה התעדכנה!', '');

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        ///delete image
        $scope.deleteImage = function (folder) {
            if (folder == null || $scope.SelectedFile == null)
                return void [0];

            FileManager.DeleteImage({ idNumber: $scope.WSPostModel.idNumber, imageId: $scope.SelectedFile.ID })
                .then(function successCallback(res) {
                    folder.images.splice(folder.images.indexOf($scope.SelectedFile), 1);

                    toastrService.success('התמונה נמחקה!', '');

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });

        }
        ///add new image to selected folder
        $scope.addImage = function (folder) {
            if (folder == null)
                return void [0];

            $('[name=_imagesUploader]').trigger('click');
        }
        ///new image uploader & filters
        $scope.imagesUploader = new FileUploader({
            url: 'filemanager/PostFile',
            queueLimit: 1,
            removeAfterUpload: true,
            //maxFileSize: 3,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.imagesUploader.filters.splice(1, 1);
        $scope.imagesUploader.filters.push($scope.queueLimitFilter);
        $scope.imagesUploader.filters.push($scope.sizeFilter);
        $scope.imagesUploader.filters.push($scope.imagesFilter);
        $scope.imagesUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.imagesUploader.onAfterAddingFile = function (fileItem) {
            fileItem.formData = [{ directoryId: $scope.isSelected.ID }, { userIDNumber: FileManager.encrypt($scope.WSPostModel.idNumber) }];
        }
        $scope.imagesUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.imagesUploader.onSuccessItem = function (item, response, status, headers) {
            var selectedFolder = $scope.getSingleSelectedFolderById($scope.isSelected.ID);
            selectedFolder.images.push(response);
            $scope.SetSelected($scope.isSelected.ID);
        }
        /* ***image functions*** */

        /* ***folder helper functions*** */
        ///re-set the selected folder after update/add/delete
        $scope.getSingleSelectedFolderById = function (folderId) {
            var selectedFolder = $scope.getSelectedFolderById(folderId);
            if (selectedFolder instanceof Array) {
                return selectedFolder[0];
            }
            return selectedFolder;
        }
        $scope.getSelectedFolderById = function (folderId, folder, _folder) {
            if (_folder !== undefined && _folder !== null) {
                return _folder;
            }

            if ($scope.fileStructure.ID === folderId)
                return $scope.getSelectedFolderById(null, null, $scope.fileStructure);

            if (folder !== undefined && folder.ID === folderId)
                return $scope.getSelectedFolderById(null, null, folder);

            return $.map(folder !== undefined ? folder.subDirectories : $scope.fileStructure.subDirectories, function (file) {
                if (file.ID !== folderId) {
                    return $scope.getSelectedFolderById(folderId, file);
                }
                if (file.ID === folderId) {
                    return $scope.getSelectedFolderById(null, null, file);
                }
            });
        }
        $scope.SetSelected = function (folderId) {
            $scope.iteratDropOver(0);

            var selectedFolder = $scope.getSingleSelectedFolderById(folderId);
            selectedFolder.selected = true;
            selectedFolder.isSelected = true;
            $scope.publish('UpdateSelectedFolder', selectedFolder);
        }
        $scope.traverseFolders = function (folder, sFID) {
            if (folder.ID != sFID) {
                folder.selected = false;
                folder.isSelected = false;
                folder.Editing = false;
                folder.Eding = false;
            }
            folder.subDirectories.filter(function (f) {
                if (f.ID == sFID) {
                    $scope.iteratParents(f.parentId);
                }

                if (f.ID != sFID) {
                    f.selected = false;
                    f.isSelected = false;
                    f.Editing = false;
                    f.Eding = false;
                }
                $scope.traverseFolders(f, sFID);
            });
        }
        $scope.iteratParents = function (parentId, folder) {
            if (parentId == undefined || parentId == null) {
                return;
            }
            if (folder == undefined) {///first loop!
                $scope.fileStructure.subDirectories.filter(function (f) {
                    if (f.ID == parentId) {
                        f.selected = true;
                    } else {
                        $scope.iteratParents(parentId, f);
                    }
                });
            }
            else {///inner loops
                folder.subDirectories.filter(function (f) {
                    if (f.ID == parentId) {
                        f.selected = true;
                        $scope.iteratParents(f.parentId);
                    } else {
                        $scope.iteratParents(parentId, f);
                    }
                });
            }
        }
        /* ***folder helper functions*** */

        /* ***drag and drop functions*** */
        ///open folder structure on drag over
        $scope.iteratDropOver = function (parentId, folder) {
            if (parentId == undefined || parentId == null) {
                return;
            }
            if (folder == undefined) {///first loop!
                $scope.fileStructure.subDirectories.filter(function (f) {
                    if (f.ID == parentId) {
                        f.dragOver = true;
                    } else {
                        f.dragOver = false;
                        $scope.iteratDropOver(parentId, f);
                    }
                });
            }
            else {///inner loops
                folder.subDirectories.filter(function (f) {
                    if (f.ID == parentId) {
                        f.dragOver = true;
                        $scope.iteratDropOver(f.parentId);
                    } else {
                        f.dragOver = false;
                        $scope.iteratDropOver(parentId, f);
                    }
                });
            }
        }

        ///drag over functions
        $scope.subscribe('dragOver', function ($dropElement, dropData) {
            var parents = $($dropElement.el).parents('ul:first');
            if ($dropElement.el.prop('nodeName') !== 'H5' && (parents.length !== 1 || parents.hasClass('deep-file')))
                return void [0];

            $scope.iteratDropOver(dropData.ID);
            dropData.dragOver = true;
            $timeout(function () {
                $scope.CurrentDragOver = dropData.ID;
            }, 5);
        });
        $scope.OnDragOver = function (data, $dragElement, $dropElement, event, dropData) {
            $scope.publish('dragOver', $dropElement, dropData);
        }
        ///drag leav functions
        $scope.subscribe('dragLeave', function (file, folder) {
            $scope.CurrentDragOver = null;
        });
        $scope.OnDragLeave = function (data, $dragElement, $dropElement, event, dropData) {
            $scope.publish('dragLeave', data, dropData);
        };
        ///file drop functions
        $scope.OnDropFile = function (data, $dragElement, $dropElement, event, dropData) {
            $scope.publish('FileDroped', data, dropData);
        };
        $scope.subscribe('FileDroped', function (file, folder) {
            switch (file.dataType) {
                case 'file':
                    $scope.FileDroped(file, folder);
                    break;
                case 'folder':
                    $scope.FolderDroped(file, folder);
                    break;
                default:
                    $scope.iteratDropOver(0);
                    break;
            }

        });
        ///when directory(folder) is droped
        $scope.FolderDroped = function (folder, Directory) {
            if ($scope.CurrentDragOver == null || folder.dataType !== 'folder' || Directory.ID !== $scope.CurrentDragOver || $scope.CurrentDragOver === folder.parentId) {
                $scope.iteratDropOver(0);
                return void [0];
            }
            
            var parentDirectory = $scope.getSingleSelectedFolderById(folder.parentId);

            parentDirectory.subDirectories.splice(parentDirectory.subDirectories.indexOf(folder), 1);
            Directory.subDirectories.push(folder);
            folder.parentId = Directory.ID;
            
            FileManager.UpdateFolder({ wsModel: $scope.WSPostModel, directory: folder })
                .then(function successCallback(res) {
                    $scope.fileStructure = res.data;
                    $scope.SetSelected(folder.ID);

                    toastrService.success('התיקיה התעדכנה', '');

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
            

        }
        ///when file(image) is droped
        $scope.FileDroped = function (file, folder) {
            if ($scope.CurrentDragOver == null || file.dataType !== 'file') {
                $scope.iteratDropOver(0);
                return void [0];
            }
            var directory = $scope.getSingleSelectedFolderById(file.directoryId);
            folder.images.push(file);
            directory.images.splice(directory.images.indexOf(file), 1);
            $scope.SetSelected(directory.ID);
            file.directoryId = folder.ID;
            $scope.CurrentDragOver = null;

            FileManager.UpdateImage({ wsModel: $scope.WSPostModel, image: file })
                .then(function successCallback(res) {
                    $scope.filesList.filter(function (f) {
                        f.selected = false;
                    });

                    toastrService.success('התמונה התעדכנה', '');

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        /* ***drag and drop functions*** */

        /* ***start controller*** */
        $scope.parseImages();
        
    }
})();


