﻿(function () {
    angular.module('WorkerPortfolioApp').controller('MunicipalityStatisticsController', MunicipalityStatisticsController);

    MunicipalityStatisticsController.$inject = ['$scope', '$rootScope', '$window', '$state', 'toastrService', 'Statistics', 'ValidAdminUser', 'municipalities', '$statistics'];

    function MunicipalityStatisticsController($scope, $rootScope, $window, $state, toastrService, Statistics, ValidAdminUser, municipalities, $statistics) {
        
        var _requiredRole = 'EMAILPAYCHECKROLE';

        if (ValidAdminUser.data !== true || ValidAdminUser.status !== 200) {
            $state.go('Home');
            toastrService.info('', 'הרשאה לא תקינה!', { onHidden: function () { $state.go('Home'); } });
            return void [0];
        }

        ///data set up functions
        var getYears = function (year) {
            var ya = [];
            for (var i = 2018; i <= year; i++) {
                ya.push(i);
            }
            $scope.selectedYear = year;
            return ya;
        }
        var SetUpPager = function (list) {
            $scope.$Pager.PageNumber = 1;
            $scope.$Pager.Counter = list.length
            $scope.$Pager.MaxPages = Math.ceil(list.length / $scope.$Pager.PageSize);
            $scope.$Pager.pagerList = [];
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                $scope.$Pager.pagerList.push(1 + i);
            }
        }
        var pad = function (val) {
            return val > 9 ? String(val) : ('0' + String(val));
        }
        var parseDateToString = function (date, toDate) {
            if (date === undefined || date === null)
                return null;
            date = new Date(date);
            if (toDate === true) {
                return date;
            }
            return pad(date.getDate()) + '/' + pad(date.getMonth() + 1) + '/' + date.getFullYear();
        }

        $scope.ParseStatisticsData = function (sd) {
            return $.map(sd, function (item) {
                item._TERMINATIONDATE = parseDateToString(item.TERMINATIONDATE);
                item.TERMINATIONDATE = parseDateToString(item.TERMINATIONDATE, true);
                
                item._ACTUALCREATIONDATE = parseDateToString(item.ACTUALCREATIONDATE);
                item.ACTUALCREATIONDATE = parseDateToString(item.ACTUALCREATIONDATE, true);

                item._CREATEDDATE = parseDateToString(item.CREATEDDATE);
                item.CREATEDDATE = parseDateToString(item.CREATEDDATE, true);

                item._DELIVERYSTATUS = item.DELIVERYSTATUS === 0 ? 'מילא טופס' : (item.DELIVERYSTATUS === null ? 'לא מילא טופס' : 'שליחה נכשלה');
                return item;
            });
        }

        $scope.WSPostModel = $rootScope.getWSPostModel(true); $rootScope.getWSPostModel(null, null, null, null, 'EMAILPAYCHECKROLE')
        $scope.WSPostModel.isMobileRequest = $scope.isMobile;

        $scope.isMobile = ($window.innerWidth < 801);
        $scope.selectedMunicipality = undefined;
        $scope.selectedYear = new Date().getFullYear();
        $scope.yearsList = getYears($scope.selectedYear);
        $scope.STHolderList = $scope.ParseStatisticsData($statistics.data);
        $scope.STList = angular.copy($scope.STHolderList);
        ///pager
        $scope.$Pager = {
            PageSize: 25,
            PageNumber: 1,
            Counter: 0,
            MaxPages: 0,
            pagerList: []
        }

        $scope.MunicipalitiesList = $.grep(municipalities.data, function (municipality) {
            return municipality.active;
        });

        $scope.selectedMunicipalityCallback = function (Selected) {
            $scope.selectedMunicipality = Selected !== undefined ? (Selected.description !== undefined ? Selected.description.ID : Selected.originalObject.ID) : undefined;
            if (Selected !== undefined && Selected.description !== undefined) {
                $scope.STList = new Array();
                $scope.STHolderList = new Array();
                $scope.$Pager = {
                    PageSize: 25,
                    PageNumber: 1,
                    Counter: 0,
                    MaxPages: 0,
                    pagerList: []
                }
            }
        }

        $scope.InitialValue = $.grep($scope.MunicipalitiesList, function (item) {
            return item.ID === $scope.WSPostModel.currentMunicipalityId;
        })[0];
        $scope.disableInput = $scope.MunicipalitiesList.length < 2;
        ///UserSearch
        $scope.UserSearch = {
            idNumber: $scope.WSPostModel.idNumber,
            confirmationToken: $scope.WSPostModel.confirmationToken,
            page: 1,
            searchMunicipalitiesId: $scope.InitialValue.ID,
            requiredRole: _requiredRole,
            searchDate: new Date()
        };

        $scope.GetStatistics = function () {
            $scope.municipality_hidden_value = null;
            $scope.StatisticsForm.hidden_value.$setValidity('required', false);
            if ($scope.selectedMunicipality !== undefined) {
                $scope.municipality_hidden_value = 1;
                $scope.StatisticsForm.hidden_value.$setValidity('required', true);
            }

            if ($scope.StatisticsForm.$invalid)
                return void [0];

            $scope.UserSearch.page = $scope.selectedMunicipality;
            $scope.UserSearch.searchDate = new Date($scope.selectedYear, 2, 2, 3, 3, 3);
            $scope.Search = {
                IdNUmber: null,
                Status: null,
                name: null,
                tCause: 0,
                tDate: 0,
                SendDate: 0
            }
            
            Statistics.MunicipalityStatistics($scope.UserSearch)
                .then(function successCallback(res) {
                    $scope.STHolderList = $scope.ParseStatisticsData(res.data);
                    $scope.STList = angular.copy($scope.STHolderList);
                    SetUpPager(res.data);
                }, function errorCallback(res) {
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.fileNameFunction = function () {
            var current = $.grep($scope.MunicipalitiesList, function (item) {
                return item.ID === $scope.selectedMunicipality;
            })[0];
            return current.name.replace(/[^א-ת0-9. -]/igm, '').replace(/[ -]/gi, '_').replace(/__/gim, '_').replace(/__/gim, '_') + '_' + String($scope.selectedYear) + '_' + String('.xlsx');
        }
        $scope.workSheetFunction = function () {
            return String('דוח 101 לשנת ') + String($scope.selectedYear);
        }

        $scope.Export = function () {
            var dataObj = [];
            $.map($scope.STList, function (item) {
                dataObj.push({
                    'תעודת זהות': item.IDNUMBER,
                    'שם מלא': item.USER_NAME,
                    'קוד סיום': item.TERMINATIONCAUSE,
                    'סיבת סיום': item.TERMINATIONREASON,
                    'תאריך סיום': item._TERMINATIONDATE,
                    'מצב 101': item._DELIVERYSTATUS,
                    'תאריך שליחה': item._ACTUALCREATIONDATE,
                    'גירסה': item.VERSION
                });
            });
            return dataObj;
        }
        
        ///Filter101Statistics
        $scope.Search = {
            IdNUmber: null,
            Status: null,
            name: null,
            tCause: 0,
            tDate: 0,
            SendDate: 0
        }
        $scope.Filter101Statistics = function () {
            $scope.STList = angular.copy($scope.STHolderList);
            var regex = null;
            ///filter idnumner
            if ($scope.Search.IdNUmber !== null && $scope.Search.IdNUmber.length > 0) {
                regex = RegExp($scope.Search.IdNUmber);
                $scope.STList = $.grep($scope.STList, function (item) {
                    return regex.test(item.IDNUMBER);
                });
            }
            ///filter user name
            if ($scope.Search.name !== null && $scope.Search.name.length > 0) {
                regex = RegExp($scope.Search.name);
                $scope.STList = $.grep($scope.STList, function (item) {
                    return regex.test(item.USER_NAME);
                });
            }
            ///filter DELIVERYSTATUS
            if (parseInt($scope.Search.Status) > 0) {
                switch (parseInt($scope.Search.Status)) {
                    case 1:
                        $scope.STList = $.grep($scope.STList, function (item) {
                            return item.DELIVERYSTATUS === null;
                        });
                        break;
                    case 2:
                        $scope.STList = $.grep($scope.STList, function (item) {
                            return item.DELIVERYSTATUS !== null && item.DELIVERYSTATUS !== 0;
                        });
                        break;
                    case 3:
                        $scope.STList = $.grep($scope.STList, function (item) {
                            return item.DELIVERYSTATUS === 0;
                        });
                        break;
                }
            }

            ///order by TERMINATIONCAUSE
            if ($scope.Search.tCause > 0) {
                switch ($scope.Search.tCause) {
                    case 1:///desc
                        $scope.STList.sort(function (a, b) {
                            return a.TERMINATIONCAUSE < b.TERMINATIONCAUSE ? 1 : a.TERMINATIONCAUSE > b.TERMINATIONCAUSE ? -1 : 0
                        });
                        break;
                    case 2:///asc
                        $scope.STList.sort(function (a, b) {
                            return a.TERMINATIONCAUSE > b.TERMINATIONCAUSE ? 1 : a.TERMINATIONCAUSE < b.TERMINATIONCAUSE ? -1 : 0
                        });
                        break;
                }
            }
            ///order by TERMINATIONDATE
            if ($scope.Search.tDate > 0) {
                switch ($scope.Search.tDate) {
                    case 1:///desc
                        $scope.STList.sort(function (a, b) {
                            return a.TERMINATIONDATE < b.TERMINATIONDATE ? 1 : a.TERMINATIONDATE > b.TERMINATIONDATE ? -1 : 0
                        });
                        break;
                    case 2:///asc
                        $scope.STList.sort(function (a, b) {
                            return a.TERMINATIONDATE > b.TERMINATIONDATE ? 1 : a.TERMINATIONDATE < b.TERMINATIONDATE ? -1 : 0
                        });
                        break;
                }
            }
            ///order by ACTUALCREATIONDATE
            if ($scope.Search.SendDate > 0) {
                switch ($scope.Search.SendDate) {
                    case 1:///desc
                        $scope.STList.sort(function (a, b) {
                            return a.ACTUALCREATIONDATE < b.ACTUALCREATIONDATE ? 1 : a.ACTUALCREATIONDATE > b.ACTUALCREATIONDATE ? -1 : 0
                        });
                        break;
                    case 2:///asc
                        $scope.STList.sort(function (a, b) {
                            return a.ACTUALCREATIONDATE > b.ACTUALCREATIONDATE ? 1 : a.ACTUALCREATIONDATE < b.ACTUALCREATIONDATE ? -1 : 0
                        });
                        break;
                }
            }

            SetUpPager($scope.STList);
        }
        $scope.selectTerminationCause = function () {
            $scope.reSetOtherVariables('tCause');
            switch ($scope.Search.tCause) {
                case 0:
                    $scope.Search.tCause = 1;
                    break;
                case 1:
                    $scope.Search.tCause = 2;
                    break;
                case 2:
                    $scope.Search.tCause = 0;
                    break;
                default:
                    $scope.Search.tCause = 0;
            }
            $scope.Filter101Statistics();
        }
        $scope.selectTerminationDate = function () {
            $scope.reSetOtherVariables('tDate');
            switch ($scope.Search.tDate) {
                case 0:
                    $scope.Search.tDate = 1;
                    break;
                case 1:
                    $scope.Search.tDate = 2;
                    break;
                case 2:
                    $scope.Search.tDate = 0;
                    break;
                default:
                    $scope.Search.tDate = 0;
            }
            $scope.Filter101Statistics();
        }
        $scope.selectDeliveryStatus = function () {
            $scope.reSetOtherVariables('Status');
            switch ($scope.Search.Status) {
                case 0:
                    $scope.Search.Status = 1;
                    break;
                case 1:
                    $scope.Search.Status = 2;
                    break;
                case 2:
                    $scope.Search.Status = 0;
                    break;
                default:
                    $scope.Search.Status = 0;
            }
            $scope.Filter101Statistics();
        }
        $scope.selectSendDate = function () {
            $scope.reSetOtherVariables('SendDate');
            switch ($scope.Search.SendDate) {
                case 0:
                    $scope.Search.SendDate = 1;
                    break;
                case 1:
                    $scope.Search.SendDate = 2;
                    break;
                case 2:
                    $scope.Search.SendDate = 0;
                    break;
                default:
                    $scope.Search.SendDate = 0;
            }
            $scope.Filter101Statistics();
        }

        $scope.reSetOtherVariables = function (Me) {
            switch (Me) {
                case 'tCause':
                    $scope.Search.tDate = 0;
                    $scope.Search.SendDate = 0;
                    break;
                case 'tDate':
                    $scope.Search.tCause = 0;
                    $scope.Search.SendDate = 0;
                    break;
                case 'SendDate':
                    $scope.Search.tCause = 0;
                    $scope.Search.tDate = 0;
                    break;

                default:
                    $scope.Search.tCause = 0;
                    $scope.Search.tDate = 0;
                    $scope.Search.SendDate = 0;
                    break;
            }
        }

        ///send 101 reminder to users
        $scope.SendReminderByMail = function () {
            $scope.UserSearch.searchDate = new Date();
            $scope.UserSearch.searchMunicipalitiesId = $scope.selectedMunicipality;
                        
            Statistics.SendReminder($scope.UserSearch)
                .then(function successCallback(res) {
                    if (res.data.Total > 0)
                        toastrService.info('מתוך ' + res.data.Total + ' משתמשים אפשריים.', 'סך הכל נשלחו: ' + res.data.Sent, { timeOut: 8765 });
                    else
                        toastrService.info('אנא הזינ/י כתובות מייל למשתמשים ונסה/י שנית.', 'לצערנו אין משתמשים פעילים שניתן לשלוח להם הזמנה למייל!', { timeOut: 7654 });
                }, function errorCallback(res) {
                    console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        ///set up functions
        SetUpPager($statistics.data);
    }
})();