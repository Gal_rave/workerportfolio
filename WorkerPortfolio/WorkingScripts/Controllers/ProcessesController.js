﻿(function () {
    angular.module('WorkerPortfolioApp').controller('ProcessesController', ProcessesController);

    ProcessesController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$location', '$interval', '$window', 'toastrService', 'ProcessesData', 'Processes', 'MyProcesses', 'UsersList', 'PayrollServise', 'VacationData', 'FileSaver', 'Blob', 'AcademicInst', 'YehidaIrgunit', 'MyRequestProcesses'];

    function ProcessesController($scope, $rootScope, $state, $stateParams, $location, $interval, $window, toastrService, ProcessesData, Processes, MyProcesses, UsersList, PayrollServise, VacationData, FileSaver, Blob, AcademicInst, YehidaIrgunit, MyRequestProcesses) {

        $scope.processesTypes = [{ id: 7, name: "בקשת יציאה לחופשה" }, { id: 5, name: "בקשת יציאה לחופשה בחול" }, { id: 3, name: "בקשת יציאה להשתלמות/קורס" }];
        
        $scope.processesStatus = [
            { id: 0, name: "טרם טופלו" },
            { id: 1, name: "בקשות שאושרו" },
            { id: 2, name: "בקשות שנדחו" }
        ];

        //as defined in KA_TAVLA -> table 1627
        $scope.stepTikOvedTemplates = [
            { id: 1, action: "בקשת יציאה בתפקיד", rashut: 500, template: "/Scripts/views/partials/Process-Templates/role-form.html", isDefault: true },
            { id: 2, action: "אישור מנהל יציאה בתפקיד", rashut: 500, template: null, isDefault: true },
            { id: 3, action: "בקשת יציאה ללימודים", rashut: 500, template: "/Scripts/views/partials/Process-Templates/course-form.html", isDefault: true },
            { id: 4, action: "אישור בקשה ללימודים", rashut: 500, template: null, isDefault: true },
            { id: 5, action: "בקשת חופשה בחול", rashut: 500, template: "/Scripts/views/partials/Process-Templates/vacation-form.html", isDefault: true },
            { id: 6, action: "אישור חופשה בחול", rashut: 500, template: null, isDefault: true },
            { id: 7, action: "בקשת חופשה", rashut: 500, template: "/Scripts/views/partials/Process-Templates/vacation-form.html", isDefault: true },
            { id: 8, action: "הודעה על קריאה", rashut: 500, template: null, isDefault: true },
            { id: 9, action: "אישור קריאה", rashut: 500, template: null, isDefault: true },
            { id: 10, action: "טופס טיולים לעזיבה", rashut: 500, template: "/Scripts/views/partials/Process-Templates/termination-form.html", isDefault: true },
            { id: 11, action: "בקשת משרה חדשה", rashut: 500, template: "/Scripts/views/partials/Process-Templates/new-job-form.html", isDefault: true },
            { id: 12, action: "אישור משרה", rashut: 500, template: null, isDefault: true },
            { id: 13, action: "אישור דרגה חדשה", rashut: 500, template: null, isDefault: true },
            { id: 14, action: "בקשת שעות נוספות", rashut: 500, template: "/Scripts/views/partials/Process-Templates/extra-hours-form.html", isDefault: true },
            { id: 15, action: "אישור שעות נוספות", rashut: 500, template: null, isDefault: true },
            { id: 16, action: "בקשת החזרי נסיעה", rashut: 500, template: "/Scripts/views/partials/Process-Templates/travel-refunds-form.html", isDefault: true },
            { id: 17, action: "אישור החזרי נסיעה", rashut: 500, template: null, isDefault: true }
        ];
                
        $scope.resetFormFlags = function () {
            $scope.formFlags = {
                isACoureseRequestForm: false,
                isVacationForm: false,
                isAboardVacationForm: false,
                isARoleForm: false,
                isATerminationForm: false,
                isNewJobForm: false,
                isExtraHoursForm: false,
                isTravelRefundsForm: false
            }
        }
    
        var FORM_ENUM = {
            ROLE_FORM: 1,
            COURSE_FORM: 3,
            ABOARD_VACATION_FORM: 5,
            VACATION_FORM: 7,
            TERMINATION_FORM: 10,
            NEW_JOB_FORM: 11,
            EXTRA_HOURS_FORM: 14,
            TRAVEL_REFUND_FORM: 16
        }

        var KVUZOT_ENUM = {
            KVUZAT_MISHTAMSHIM_REGILA: 1,
            KVUZAT_MISHTAMSHIM_DINAMIM: 2
        }

        $scope.isMobile = function () {
            return $window.innerWidth < 801;
        }

        var getTypeOfFirstShalav = function(selectedProcessIndex) {
            
            for (var i = 0; i < $scope.allProcessesData.processes[selectedProcessIndex].shlavim.length;i++) {
                if ($scope.allProcessesData.processes[selectedProcessIndex].shlavim[i].misparShalav == 1) {
                    return $scope.allProcessesData.processes[selectedProcessIndex].shlavim[i].rechivId;
                }
            }
            
        }

        $scope.onProcessChange = function () {
            $scope.selectedProcessIndex = event.currentTarget.selectedIndex;
            $scope.selectedProcess = {
                id: $scope.allProcessesData.processes[$scope.selectedProcessIndex].recId,
                name: $scope.allProcessesData.processes[$scope.selectedProcessIndex].shemTahalich,
                type: getTypeOfFirstShalav($scope.selectedProcessIndex)//,
            }
            
            $scope.setCurrentSteps();
            $scope.setProcessStepTemplate();
        }

        $scope.setCurrentSteps = function () {
            if ($scope.selectedProcess != null && $scope.selectedProcessIndex != null) {
                $scope.currentSteps = angular.copy($scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim);
            }
        }

        $scope.setProcessStepTemplate = function () {
            
            $scope.currentRunningStep = null;
            $scope.currentRunningStepTemplate = null;

            $scope.nextStepOfCurrent = null;

            if($scope.currentSteps.length > 0) {
                $scope.currentRunningStep = $scope.currentSteps.find(function(step) {
                    return step.status == 0 && step.misparShalav == 1;
                });
                if ($scope.currentRunningStep) {
                    //get the next step of the current running step for setting the mngrs (kvuzat mishtamshim) that the request will be sent to
                    $scope.nextStepOfCurrent = $scope.currentSteps.find(function (step) {
                        return step.misparShalav == $scope.currentRunningStep.misparShalav + 1;
                    });
                }
            }
            if ($scope.currentRunningStep) {

                if ($scope.currentRunningStep.modulId != TIK_OVED_SYSTEM) {
                    $scope.resetFormFlags();
                    return;
                }

                switch ($scope.currentRunningStep.rechivId) {

                    case FORM_ENUM.ROLE_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: false,
                            isVacationForm: false,
                            isAboardVacationForm: false,
                            isARoleForm: true,
                            isATerminationForm: false,
                            isNewJobForm: false,
                            isExtraHoursForm: false,
                            isTravelRefundsForm: false
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.ROLE_FORM);
                        break;

                    case FORM_ENUM.COURSE_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: true,
                            isVacationForm: false,
                            isAboardVacationForm: false,
                            isARoleForm: false,
                            isATerminationForm: false,
                            isNewJobForm: false,
                            isExtraHoursForm: false,
                            isTravelRefundsForm: false
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.COURSE_FORM);
                        break;

                    case FORM_ENUM.ABOARD_VACATION_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: false,
                            isVacationForm: false,
                            isAboardVacationForm: true,
                            isARoleForm: false,
                            isATerminationForm: false,
                            isNewJobForm: false,
                            isExtraHoursForm: false,
                            isTravelRefundsForm: false
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.ABOARD_VACATION_FORM);
                        break;

                    case FORM_ENUM.VACATION_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: false,
                            isVacationForm: true,
                            isAboardVacationForm: false,
                            isARoleForm: false,
                            isATerminationForm: false,
                            isNewJobForm: false,
                            isExtraHoursForm: false,
                            isTravelRefundsForm: false
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.VACATION_FORM);
                        break;

                    case FORM_ENUM.TERMINATION_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: false,
                            isVacationForm: false,
                            isAboardVacationForm: false,
                            isARoleForm: false,
                            isATerminationForm: true,
                            isNewJobForm: false,
                            isExtraHoursForm: false,
                            isTravelRefundsForm: false
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.TERMINATION_FORM);
                        break;

                    case FORM_ENUM.NEW_JOB_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: false,
                            isVacationForm: false,
                            isAboardVacationForm: false,
                            isARoleForm: false,
                            isATerminationForm: false,
                            isNewJobForm: true,
                            isExtraHoursForm: false,
                            isTravelRefundsForm: false
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.NEW_JOB_FORM);
                        break;

                    case FORM_ENUM.EXTRA_HOURS_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: false,
                            isVacationForm: false,
                            isAboardVacationForm: false,
                            isARoleForm: false,
                            isATerminationForm: false,
                            isNewJobForm: false,
                            isExtraHoursForm: true,
                            isTravelRefundsForm: false
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.EXTRA_HOURS_FORM);
                        break;

                    case FORM_ENUM.TRAVEL_REFUND_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: false,
                            isVacationForm: false,
                            isAboardVacationForm: false,
                            isARoleForm: false,
                            isATerminationForm: false,
                            isNewJobForm: false,
                            isExtraHoursForm: false,
                            isTravelRefundsForm: true
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.TRAVEL_REFUND_FORM);
                        break;

                    default:
                        $scope.resetFormFlags();
                        $scope.currentRunningStepTemplate = null;
                        break;
                }
                
                if ($scope.currentRunningStepTemplate != null && $scope.nextStepOfCurrent != null) {//only if there is a template rendered - so we set step mngrs that will get the request

                    if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_DINAMIM) {//read the mngrs of next step and send them the request. should be menaalim yeshirim
                        if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.mishtamsheiKvuzaModel[0].mishtameshId == 2) {//mishtamesh id 2 in kvuzot dinamiyot is menaalim yeshirim. see table 1637 in ka_tavla
                            Processes.getUserManagers($scope.currentYehidaIrgunit, $scope.getWSPostModel).then(function successCallback(res) {
                                if (res.status == 200) {
                                    $scope.organizationUnitsManagers = [];
                                    $scope.organizationUnitsManagers = res.data;
                                    removeDuplicateMngrs();
                                }
                                else {
                                    toastrService.error('', 'לא ניתן היה למצוא משתמשים אשר הבקשה תופנה אליהם');
                                    $scope.organizationUnitsManagers = [];
                                }
                            }, function errorCallback(res) {
                                toastrService.error('', 'תקלת מערכת!');
                                $scope.organizationUnitsManagers = [];
                            });
                        }
                        else {
                            Processes.getMishtamsheiKvuzaDinamit($scope.getWSPostModel, $scope.nextStepOfCurrent.kvuzatMishtamshimModel.mishtamsheiKvuzaModel).then(function successCallback(res) {
                                if (res.status == 200) {
                                    $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                                    $scope.currentMishtamsheiKvuzaOfCurrentStep = res.data;
                                }
                                else {
                                    toastrService.error('', 'לא ניתן היה למצוא משתמשים אשר הבקשה תופנה אליהם');
                                    $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                                }
                            }, function errorCallback(res) {
                                toastrService.error('', 'תקלת מערכת!');
                                $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                            });
                         }
                    }
                    else if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_REGILA) {

                            Processes.getMishtamsheiKvuzaRegila($scope.getWSPostModel, $scope.nextStepOfCurrent.kvuzatMishtamshimModel.mishtamsheiKvuzaModel).then(function successCallback(res) {
                                if (res.status == 200) {
                                    $scope.currentMishtamsheiKvuzaOfCurrentStep = [];//this is the mishtamshei kvuza of next step (currentStep is 1, so mishtamshei kvuza of step 2)
                                    $scope.currentMishtamsheiKvuzaOfCurrentStep = res.data;
                                    reArrangeMishtamsehiKvuzaRegila();//to be the same structure as kvuza dinamit
                                }
                                else {
                                    toastrService.error('', 'לא ניתן היה למצוא משתמשים אשר הבקשה תופנה אליהם');
                                    $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                                }
                            }, function errorCallback(res) {
                                toastrService.error('', 'תקלת מערכת!');
                                $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                            });
                    }


                    //if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_DINAMIM || $scope.currentRunningStep.misparShalav == 1) {//read the mngrs of next step and send them the request. should be menaalim yeshirim
                    //    if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.mishtamsheiKvuzaModel[0].mishtameshId == 2 || $scope.currentRunningStep.misparShalav == 1) {//mishtamesh id 2 in kvuzot dinamiyot is menaalim yeshirim. see table 1637 in ka_tavla
                    //        Processes.getUserManagers($scope.currentYehidaIrgunit, $scope.getWSPostModel).then(function successCallback(res) {
                                
                    //            $scope.organizationUnitsManagers = [];
                    //            $scope.organizationUnitsManagers = res.data;
                    //            //reArrangeOrganizationUnitsManagers();
                    //            removeDuplicateMngrs();
                    //        }, function errorCallback(res) {
                                
                    //        });
                    //    }
                    //}
                    //else {

                    //    //REGULAR GROUP MNGRS
                    //    if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_REGILA) {
                    //        Processes.getMishtamsheiKvuzaRegila($scope.getWSPostModel, $scope.nextStepOfCurrent.kvuzatMishtamshimModel.mishtamsheiKvuzaModel).then(function successCallback(res) {
                    //            if (res.status == 200) {
                    //                $scope.currentMishtamsheiKvuzaOfCurrentStep = [];//this is the mishtamshei kvuza of next step (currentStep is 1, so mishtamshei kvuza of step 2)
                    //                $scope.currentMishtamsheiKvuzaOfCurrentStep = res.data;
                    //                reArrangeMishtamsehiKvuzaRegila();//to be the same structure as kvuza dinamit
                    //            }
                    //            else {
                    //                toastrService.error('', 'לא ניתן היה למצוא משתמשים אשר הבקשה תופנה אליהם');
                    //                $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                    //            }
                    //        }, function errorCallback(res) {
                    //            toastrService.error('', 'תקלת מערכת!');
                    //            $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                    //        });
                    //    }
                    //    //DINAMIC GROUP MNGRS
                    //    else if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_DINAMIM) {
                    //        Processes.getMishtamsheiKvuzaDinamit($scope.getWSPostModel, $scope.nextStepOfCurrent.kvuzatMishtamshimModel.mishtamsheiKvuzaModel).then(function successCallback(res) {
                    //            if (res.status == 200) {
                    //                $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                    //                $scope.currentMishtamsheiKvuzaOfCurrentStep = res.data;
                    //            }
                    //            else {
                    //                toastrService.error('', 'לא ניתן היה למצוא משתמשים אשר הבקשה תופנה אליהם');
                    //                $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                    //            }
                    //        }, function errorCallback(res) {
                    //            toastrService.error('', 'תקלת מערכת!');
                    //            $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                    //        });
                    //    }
                    //}

                }
            }
        }

        var reArrangeMishtamsehiKvuzaRegila = function () {
            //הגשת בקשה חדשה 
            if ($scope.currentMishtamsheiKvuzaOfCurrentStep.length > 0) {
                var tempArr = angular.copy($scope.currentMishtamsheiKvuzaOfCurrentStep);
                $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                _.map(tempArr, function (mishtamesh) {
                    var newVal = {
                        shem_prati: mishtamesh.user.firstName,
                        shem_mishpaha: mishtamesh.user.lastName,
                        rashut: mishtamesh.rashut,
                        zehut: mishtamesh.user.idNumber,
                        kodMedaveach: mishtamesh.kodMedaveach
                    }
                    $scope.currentMishtamsheiKvuzaOfCurrentStep.push(newVal);
                });
            }
        }

        var reArrangeMishtamsehiKvuzaRegilaInMyProcessesTab = function (index) {
            if ($scope.mishtamsheiKvuzaPerProcess[index].length > 0) {
                var tempArr = angular.copy($scope.mishtamsheiKvuzaPerProcess[index]);
                $scope.mishtamsheiKvuzaPerProcess[index] = [];
                _.map(tempArr, function (mishtamesh) {
                    var newVal = {
                        shem_prati: mishtamesh.user.firstName,
                        shem_mishpaha: mishtamesh.user.lastName,
                        rashut: mishtamesh.rashut,
                        zehut: mishtamesh.user.idNumber,
                        kodMedaveach: mishtamesh.kodMedaveach
                    }
                    $scope.mishtamsheiKvuzaPerProcess[index].push(newVal);
                });
            }
        }

        var reArrangeMyReqProcesses = function () {
            $scope.processesOftheUser = [];
            _.map($scope.myRequestProcesses, function (processOfUser) {
                var procIndx = isExistUserProcess(processOfUser, $scope.processesOftheUser);
                if (procIndx < 0) {
                    var len = $scope.processesOftheUser.push({ process: processOfUser, targetUsers: [] });
                    $scope.processesOftheUser[len-1].targetUsers.push(processOfUser.TARGET_USER);
                }
                else {
                    $scope.processesOftheUser[procIndx].targetUsers.push(processOfUser.TARGET_USER);
                }
            });
            $scope.showProcessDetails = new Array($scope.processesOftheUser.length);
            $scope.isUpdatedProcess = new Array($scope.processesOftheUser.length);
            for (var indx = 0; indx < $scope.processesOftheUser.length; indx++) {
                $scope.showProcessDetails[indx] = false;
                $scope.isUpdatedProcess[indx] = true;
            }
        }

        var isExistUserProcess = function (processOfUser, arrayOfAllProcessesOfTheUser) {
            for (var indx = 0; indx < arrayOfAllProcessesOfTheUser.length; indx++) {
                if (arrayOfAllProcessesOfTheUser[indx].process.TM_TAHALICH_ID == processOfUser.TM_TAHALICH_ID) {
                    return indx;
                }
            }
            return -1;
        }

        $scope.getStepTemplate = function (templateID) {
            return $scope.stepTikOvedTemplates.find(function (step) {
                return step.id == templateID;
            })
        }

        //values for below types defined in KA_TAVLA -> table: 1627
        const explicitProcessTypes = {
            vacationType: 7,
            aboardVacationType: 5,
            courseRequestType: 3
        }

        const SACHAR_SYSTEM = 1;//for comparing modulId field in the process steps
        const TIK_OVED_SYSTEM = 2;//for comparing modulId field in the process steps

        var dates = {
            convert: function (d) {
                return (
                    d.constructor === Date ? d :
                    d.constructor === Array ? new Date(d[0], d[1], d[2]) :
                    d.constructor === Number ? new Date(d) :
                    d.constructor === String ? new Date(d) :
                    typeof d === "object" ? new Date(d.year, d.month, d.date) :
                    NaN
                );
            },
            compare: function (a, b) {
                return (
                    isFinite(a = this.convert(a).valueOf()) &&
                    isFinite(b = this.convert(b).valueOf()) ?
                    (a > b) - (a < b) :
                    NaN
                );
            },
            inRange: function (d, start, end) {
                return (
                     isFinite(d = this.convert(d).valueOf()) &&
                     isFinite(start = this.convert(start).valueOf()) &&
                     isFinite(end = this.convert(end).valueOf()) ?
                     start <= d && d <= end :
                     NaN
                 );
            }
        }

        $scope.compareDates = function(date1, date2) {
            return dates.compare(date1, date2);
        }

        $scope.autoGrow = function (event) {
            var element = event.target;
            element.style.height = "5px";
            element.style.height = (element.scrollHeight) + "px";
        }

        $scope.updateProcess = function (prcsIndex, process) {

            var currentUpdatedProcess = angular.copy(process);

            Processes.saveProcess(process, $scope.getWSPostModel).then(function successCallback(res) {
                $scope.$broadcast('angucomplete-alt:clearInput');//important => fix autocomplete bug
                if (res.status == 200) {
                    if (currentUpdatedProcess.STATUS == 1) {
                        if (currentUpdatedProcess.curr_mispar_shalav + 1 == currentUpdatedProcess.shlavim_counter) {
                            toastrService.success('הבקשה אושרה סופית!');
                        }
                        else toastrService.success('הבקשה אושרה והועברה לטיפול הגורמים המתאימים!');
                    }
                    else if(currentUpdatedProcess.STATUS == 2) {
                        toastrService.success('הבקשה נדחתה!');
                    }
                    else if (currentUpdatedProcess.STATUS == 3) {
                        toastrService.success('הבקשה אושרה סופית בהצלחה!');
                    }
                    else if (currentUpdatedProcess.TARGET_USERID != $scope.getWSPostModel.calculatedIdNumber) {
                        toastrService.success('הבקשה הועברה בהצלחה!');
                    }
                    
                    $scope.allMyProcessesData = res.data;
                    if ($scope.allMyProcessesData.length > 0) {
                        $scope.myProcessesIsEmpty = false;
                        $scope.filters.selectedStatusFilter = 0;
                        $scope.filterMyProcesses('status');
                    }
                    else {
                        $scope.myProcessesData = new Array();
                        $scope.myProcessesIsEmpty = true;
                    }
                }
                else {
                    toastrService.error('', 'משהו השתבש והעדכון נכשל. אנא נסה שנית..');
                }
            }, function errorCallback(res) {
                toastrService.error('', 'תקלת מערכת!');
            });
        }
        
        $scope.courseForm = {
            empFirstName: '',
            empLastName: '',
            empId: '',
            empRole: '',
            empDepartment: '',
            empYearsOfExperiance: '',
            coursePlan:'',
            courseInstitution: '',
            courseInstitutionID: '',
            courseNumberOfDays: '',
            courseNumberOfHours:''
        }

        $scope.datesRange = {
            from: null,
            to: null
        }

        $scope.newOptions = {
            yearRange: "0:+100",
            minDate: new Date(),
        }

        $scope.myProcessesIsEmpty = false;
        $scope.myRequestProcessesIsEmpty = false;
        $scope.vacationReason = "";
        $scope.vacationLeft = "אין מידע";
        $scope.vacationAmount = new Array();
        $scope.representedWorkers = new Array();
        $scope.selectedRepresentWorker = {
            idNumber: '',
            fullName: '',
            email: ''
        }
        $scope.usersAndUnitMngrs = new Array();
        $scope.showProcessDetails = new Array();
        $scope.isNewTargetUser = new Array();
        $scope.isUpdatedProcess = new Array();
        $scope.myProcessesData = new Array();
        $scope.myRequestProcesses = new Array();

        //for dates-filter models
        var today = new Date(), month, day, year;
        year = today.getFullYear()
        month = today.getMonth()
        date = today.getDate()
        if ((month-1) <= 0) year = today.getFullYear() - 1;

        var formatDate = function (date) {
            if (date) {
                return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + "/" + ((date.getMonth()+1) < 10 ? "0" + (date.getMonth()+1) : (date.getMonth()+1)) + "/" + date.getFullYear();
            }
            else return formatdate(new Date());
        }

        $scope.filters = {
            isDatesFilter: false,
            isEmployeeFilter: false,
            isTypesFilter: false,
            isStatusFilter: false,
            isStatusOpenFilter: false,
            isAllFilter: false,
            selectedFromDateFilter: formatDate(new Date(year, month-1, date)),
            selectedToDateFilter: formatDate(new Date()),
            selectedTypeFilter: null,
            selectedStatusFilter: null,
            selectedEmployeeFilter: null
        }

        $scope.displayEmployeeFilter = function () {

            $scope.filters.isDatesFilter = false;
            $scope.filters.isEmployeeFilter = true;
            $scope.filters.isTypesFilter = false;
            $scope.filters.isStatusFilter = false;
            $scope.filters.isStatusOpenFilter = false;
            $scope.filters.isAllFilter = false;
        }

        $scope.displayDatesFilter = function () {
            if (!$scope.filters.isDatesFilter) {
                $scope.filterMyProcesses('dates');
            }
            $scope.filters.isDatesFilter = true;
            $scope.filters.isEmployeeFilter = false;
            $scope.filters.isTypesFilter = false;
            $scope.filters.isStatusFilter = false;
            $scope.filters.isStatusOpenFilter = false;
            $scope.filters.isAllFilter = false;
        }

        $scope.displayTypesFilter = function () {
            $scope.filters.isDatesFilter = false;
            $scope.filters.isEmployeeFilter = false;
            $scope.filters.isTypesFilter = true;
            $scope.filters.isStatusFilter = false;
            $scope.filters.isStatusOpenFilter = false;
            $scope.filters.isAllFilter = false;
        }

        $scope.displayStatusFilter = function () {
            $scope.filters.isDatesFilter = false;
            $scope.filters.isEmployeeFilter = false;
            $scope.filters.isTypesFilter = false;
            $scope.filters.isStatusFilter = true;
            $scope.filters.isStatusOpenFilter = false;
            $scope.filters.isAllFilter = false;
        }

        $scope.resetSelectedFilters = function () {
            $scope.filters.selectedFromDateFilter = formatDate(new Date(year, month - 1, date));
            $scope.filters.selectedToDateFilter = formatDate(new Date());
            $scope.filters.selectedTypeFilter = null;
            $scope.filters.selectedStatusFilter = null;
            $scope.filters.selectedEmployeeFilter = null;
        }

        $scope.exportExcelTable = function () {
            var dataForExcel = new Array();
            dataForExcel.push('', 'שם הבקשה', 'מתאריך', 'עד תאריך', 'שם העובד', 'ת.ז', 'סטטוס הבקשה', 'תאריך הגשת הבקשה', 'סיבה', 'יתרת חופשה', 'תפקיד העובד', 'מחלקה', 'שנות ותק', 'מוסד השכלה', 'צפי ימים', 'צפי שעות', 'גורם מטפל');
            dataForExcel.push("\n");

            if ($scope.myProcessesData.length > 0) {
                _.map($scope.myProcessesData, function (myProcess) {
                    dataForExcel.push(myProcess.PROCESS_DESCRIPTION);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? myProcess.FROM_DATE : "אין");
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? myProcess.TO_DATE : "אין");
                    dataForExcel.push(myProcess.USERMODEL.firstName + " " + myProcess.USERMODEL.lastName);
                    dataForExcel.push(myProcess.USERMODEL.idNumber);
                    dataForExcel.push(myProcess.STATUS == 0 ? "טרם התקבל" : myProcess.STATUS == 1 ? "אושר בשלב ביניים" : myProcess.STATUS == 3 ? "אושר" : "נדחה");
                    dataForExcel.push(myProcess.CREATION_DATE);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? myProcess.CONTENT1 : myProcess.CONTENT4);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? myProcess.CONTENT2 : "");
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? "" : myProcess.CONTENT1);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? "" : myProcess.CONTENT2);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? "" : myProcess.CONTENT3);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? "" : myProcess.CONTENT5);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? "" : myProcess.CONTENT6);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? "" : myProcess.CONTENT7);
                    dataForExcel.push();
                    dataForExcel.push("\n");
                });
            }
            else if ($scope.myRequestProcesses.length > 0) {
                _.map($scope.processesOftheUser, function (myReqProcess) {//processesOftheUser is new order array of myRequestProcesses 
                    var gormim = getGormimForProcess(myReqProcess);
                    dataForExcel.push(myReqProcess.process.PROCESS_DESCRIPTION);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? myReqProcess.process.FROM_DATE : "אין");
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? myReqProcess.process.TO_DATE : "אין");
                    dataForExcel.push(myReqProcess.process.USERMODEL.firstName + " " + myReqProcess.process.USERMODEL.lastName);
                    dataForExcel.push(myReqProcess.process.USERMODEL.idNumber);
                    dataForExcel.push(myReqProcess.process.STATUS == 0 ? "טרם התקבל" : myReqProcess.process.STATUS == 1 ? "אושר בשלב ביניים" : myReqProcess.process.STATUS == 3 ? "אושר" : "נדחה");
                    dataForExcel.push(myReqProcess.process.CREATION_DATE);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? myReqProcess.process.CONTENT1 : myReqProcess.process.CONTENT4);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? myReqProcess.process.CONTENT2 : "");
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? "" : myReqProcess.process.CONTENT1);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? "" : myReqProcess.process.CONTENT2);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? "" : myReqProcess.process.CONTENT3);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? "" : myReqProcess.process.CONTENT5);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? "" : myReqProcess.process.CONTENT6);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? "" : myReqProcess.process.CONTENT7);
                    dataForExcel.push(gormim);
                    dataForExcel.push("\n");
                });
            }
            try {//chrome
                var blob = new Blob([dataForExcel], {
                    type: 'application/xml;charset=utf-8'
                });
                FileSaver.saveAs(blob, "Report" + Date.now() + ".xls");
            } catch (e) {//explorer
                if (e.name == "InvalidStateError") {

                    //for hebrew support
                    var BOM = "\ufeff";
                    dataForExcel = BOM + dataForExcel;

                    var bb = new MSBlobBuilder();
                    bb.append(dataForExcel);
                    var blob = bb.getBlob("text/csv;charset=utf-8");
                    window.navigator.msSaveOrOpenBlob(blob, "Report" + Date.now() + ".xls");
                }
            }
        }

        var getGormimForProcess = function (reqProcess) {
            var ans = '';
            for (var i = 0; i < reqProcess.targetUsers.length; i++) {
                ans += reqProcess.targetUsers[i].firstName + " " + reqProcess.targetUsers[i].lastName + "   ";
            }
            return ans;
        }

        var isInRange = function (processCreationDate) {
            if (processCreationDate) {
                var y, m, d, y1, m1, d1, y2, m2, d2;
                var fields = processCreationDate.split('/');
                d = fields[0];
                m = fields[1];
                y = fields[2];

                fields = $scope.filters.selectedFromDateFilter.split('/');
                d1 = fields[0];
                m1 = fields[1];
                y1 = fields[2];

                fields = $scope.filters.selectedToDateFilter.split('/');
                d2 = fields[0];
                m2 = fields[1];
                y2 = fields[2];

                return dates.inRange(new Date(y,m-1,d), new Date(y1,m1-1,d1), new Date(y2,m2-1,d2));
            }
            return true;
        }

        $scope.filterMyProcesses = function (filterBy) {
            
            switch (filterBy) {
                case 'dates':
                    $scope.filters.isStatusFilter = false;
                    $scope.filters.isEmployeeFilter = false;
                    $scope.filters.isTypesFilter = false;
                    $scope.filters.isStatusOpenFilter = false;
                    $scope.filters.isAllFilter = false;
                    $scope.myProcessesData = new Array();
                    $scope.myRequestProcesses = new Array();

                    if ($scope.allMyProcessesData.length > 0) {
                        _.map($scope.allMyProcessesData, function (myProcess) {
                            if (isInRange(myProcess.CREATION_DATE)) {
                                $scope.myProcessesData.push(myProcess);
                            }
                        });
                    }
                    else if ($scope.myRequestProcessesData.length > 0) {
                        _.map($scope.myRequestProcessesData, function (myReqProcess) {
                            if (isInRange(myReqProcess.CREATION_DATE)) {
                                $scope.myRequestProcesses.push(myReqProcess);
                            }
                        });
                    }
                    break;

                case 'all':
                    $scope.filters.isDatesFilter = false;
                    $scope.filters.isEmployeeFilter = false;
                    $scope.filters.isTypesFilter = false;
                    $scope.filters.isStatusFilter = false;
                    $scope.filters.isStatusOpenFilter = false;
                    $scope.filters.isAllFilter = true;
                    $scope.myProcessesData = new Array();
                    $scope.myRequestProcesses = new Array();

                    if ($scope.allMyProcessesData.length > 0) {
                        $scope.myProcessesData = angular.copy($scope.allMyProcessesData);
                    }
                    else if ($scope.myRequestProcessesData.length > 0) {
                        $scope.myRequestProcesses = angular.copy($scope.myRequestProcessesData);
                    }
                    break;

                case 'type':
                    $scope.filters.isDatesFilter = false;
                    $scope.filters.isEmployeeFilter = false;
                    $scope.filters.isStatusFilter = false;
                    $scope.filters.isStatusOpenFilter = false;
                    $scope.filters.isAllFilter = false;
                    $scope.myProcessesData = new Array();
                    $scope.myRequestProcesses = new Array();

                    if ($scope.allMyProcessesData.length > 0) {
                        _.map($scope.allMyProcessesData, function (myProcess) {
                            if (myProcess.PROCESS_TYPE == $scope.filters.selectedTypeFilter) {
                                $scope.myProcessesData.push(myProcess);
                            }
                        });
                    }
                    else if ($scope.myRequestProcessesData.length > 0) {
                        _.map($scope.myRequestProcessesData, function (myReqProcess) {
                            if (myReqProcess.PROCESS_TYPE == $scope.filters.selectedTypeFilter) {
                                $scope.myRequestProcesses.push(myReqProcess);
                            }
                        });
                    }
                    break;

                case 'status':
                    $scope.filters.isDatesFilter = false;
                    $scope.filters.isEmployeeFilter = false;
                    $scope.filters.isStatusFilter = true;
                    $scope.filters.isStatusOpenFilter = false;
                    $scope.filters.isAllFilter = false;
                    $scope.filters.isTypesFilter = false;
                    $scope.myProcessesData = new Array();
                    $scope.myRequestProcesses = new Array();

                    if ($scope.allMyProcessesData.length > 0) {
                        _.map($scope.allMyProcessesData, function (myProcess) {
                            if (myProcess.STATUS == $scope.filters.selectedStatusFilter) {//3 is new status for final approve from sachar system
                                $scope.myProcessesData.push(myProcess);
                            }
                            if ($scope.filters.selectedStatusFilter == 1) {//if filter is approval processes(1) so show also processes with final approve(3) together with temp approve
                                if(myProcess.STATUS == 3) {
                                    $scope.myProcessesData.push(myProcess);
                                }
                            }
                            if ($scope.filters.selectedStatusFilter == 0) {//if filter is approval processes(1) so show also processes with final approve(3) together with temp approve
                                if (myProcess.STATUS == 1) {
                                    $scope.myProcessesData.push(myProcess);
                                }
                            }
                        });
                    }
                    else if ($scope.myRequestProcessesData.length > 0) {
                        _.map($scope.myRequestProcessesData, function (myReqProcess) {
                            if (myReqProcess.STATUS == $scope.filters.selectedStatusFilter) {//3 is new status for final approve from sachar system
                                $scope.myRequestProcesses.push(myReqProcess);
                            }
                            if ($scope.filters.selectedStatusFilter == 1) {//if filter is approval processes(1) so show also processes with final approve(3) together with temp approve
                                if (myReqProcess.STATUS == 3) {
                                    $scope.myRequestProcesses.push(myReqProcess);
                                }
                            }
                            if ($scope.filters.selectedStatusFilter == 0) {//if filter is approval processes(1) so show also processes with final approve(3) together with temp approve
                                if (myReqProcess.STATUS == 1) {
                                    $scope.myRequestProcesses.push(myReqProcess);
                                }
                            }
                        });
                    }
                    break;
                case 'employee':
                    $scope.filters.isStatusOpenFilter = false;
                    $scope.filters.isEmployeeFilter = true;
                    $scope.filters.isDatesFilter = false;
                    $scope.filters.isTypesFilter = false;
                    $scope.filters.isStatusFilter = false;
                    $scope.filters.isAllFilter = false;
                    $scope.myProcessesData = new Array();

                    _.map($scope.allMyProcessesData, function (myProcess) {
                        if (myProcess.OVED_NESU_BAKASHA != "" || myProcess.OVED_NESU_BAKASHA != '') {
                            if (myProcess.OVED_NESU_BAKASHA == $scope.filters.selectedEmployeeFilter) {
                                $scope.myProcessesData.push(myProcess);
                            }   
                        }
                        else if (myProcess.USERMODEL.calculatedIdNumber == $scope.filters.selectedEmployeeFilter) {
                            $scope.myProcessesData.push(myProcess);
                        }
                    });
                    break;
                default: break;
            }
            
            //for all cases:
            if($scope.allMyProcessesData.length > 0) {
                $scope.showProcessDetails = new Array($scope.myProcessesData.length);
                $scope.isUpdatedProcess = new Array($scope.myProcessesData.length);
                $scope.isNewTargetUser = new Array($scope.myProcessesData.length);
            }
            else if ($scope.myRequestProcessesData.length > 0) {
                $scope.showProcessDetails = new Array($scope.myRequestProcesses.length);
                $scope.isUpdatedProcess = new Array($scope.myRequestProcesses.length);
                $scope.isNewTargetUser = new Array($scope.myRequestProcesses.length);
                $scope.processesOftheUser = new Array();
            }

            if ($scope.myProcessesData.length > 0) {
                for (var i = 0; i < $scope.myProcessesData.length; i++) {
                    $scope.showProcessDetails[i] = false;
                    $scope.isNewTargetUser[i] = false;

                    if ($scope.myProcessesData[i].STATUS != 0) {
                        $scope.isUpdatedProcess[i] = true;
                    }
                    else {
                        $scope.isUpdatedProcess[i] = false;
                    }
                }
                $scope.myProcessesIsEmpty = false;
            }
            else $scope.myProcessesIsEmpty = true;

            if ($scope.myRequestProcesses.length > 0) {

                //new 24.06.18
                $scope.processesOftheUser = [];
                reArrangeMyReqProcesses();
                //new ends here

                for (var i = 0; i < $scope.myRequestProcesses.length; i++) {
                    $scope.showProcessDetails[i] = false;
                    $scope.isNewTargetUser[i] = false;
                }
                $scope.myRequestProcessesIsEmpty = false;
            }
            else $scope.myRequestProcessesIsEmpty = true;
        }

        $scope.processesOftheUser = [];

        $scope.selectedProcess = {
            id: '',
            name: '',
            type: ''
        }
        $scope.selectedProcessIndex = null;

        $scope.selectedUnitManager = {
            idNumber: '',
            fullName: '',
            rashutNumber: ''
        }

        $scope.instfocusOut = function (selected) {
            if(selected == undefined) {
                $scope.courseForm.courseInstitution = $('#academicInst_value')[0].value;
                $scope.courseForm.courseInstitutionID = '';
                $scope.$broadcast('angucomplete-alt:changeInput', 'academicInst', $scope.courseForm.courseInstitution);
            }
        }

        $scope.onRepresentWorkerChange = function (selected) {
            if(selected != null && selected != undefined) {
                $scope.selectedRepresentWorker = {
                    idNumber: selected.originalObject.idNumber,
                    fullName: selected.originalObject.fullName,
                    email: selected.originalObject.email
                }
                Processes.getWorkerVacationData($scope.getWSPostModel, $scope.selectedRepresentWorker).then(function successCallback(res) {
                    if (res.data.length > 0) {
                        $scope.vacationData = res.data;
                        if($scope.vacationData.length > 0) {
                            reArrangeVacationData();
                        }
                    }
                    else {
                        scope.vacationLeft = "אין מידע";
                    }
                }, function errorCallback(res) {
                    scope.vacationLeft = "אין מידע";
                });
                Processes.getWorkerYehidaIrgunit($scope.getWSPostModel, $scope.selectedRepresentWorker).then(function successCallback(res) {
                    var workerYehidaIrgunit = null
                    if (res.data.length > 0) {
                        workerYehidaIrgunit = res.data.length > 0 ? res.data : null;
                    }
                    else {
                        workerYehidaIrgunit = null;
                    }
                    if (workerYehidaIrgunit != null) {
                        Processes.getWorkerManagers($scope.getWSPostModel, $scope.selectedRepresentWorker, workerYehidaIrgunit).then(function successCallback(res) {
                            if (res.data.length > 0) {
                                
                                $scope.organizationUnitsManagers =[];
                                $scope.organizationUnitsManagers = res.data;
                                $scope.organizationUnitsManagers.map(function (currentValue, index, organizationUnitsManagers) {

                                    if (currentValue.idNumber != $scope.getWSPostModel.calculatedIdNumber) {

                                        currentValue = {

                                            fName: currentValue.fName,
                                            idNumber: currentValue.idNumber,
                                            lName: currentValue.lName,
                                            ramaIrgunit: currentValue.ramaIrgunit,
                                            rashutNumber: currentValue.rashutNumber,
                                            supervisorUnitNumber: currentValue.supervisorUnitNumber,
                                            tekenKoachAdam: currentValue.tekenKoachAdam,
                                            tekenMenaelYahidaIrgunit: currentValue.tekenMenaelYahidaIrgunit,
                                            treeNumber: currentValue.treeNumber,
                                            unitDescription: currentValue.unitDescription,
                                            unitNumber: currentValue.unitNumber,
                                            zehutNatzigKoachAdam: currentValue.zehutNatzigKoachAdam,

                                                //fullName: currentValue.fName + " " +currentValue.lName,
                                                //idNumber: currentValue.idNumber,
                                                //rashutNumber: currentValue.rashutNumber,
                                                //zehutNatzigKoachAdam: currentValue.zehutNatzigKoachAdam,
                                                //tekenKoachAdam: currentValue.tekenKoachAdam,
                                                //tekenMenaelYahidaIrgunit: currentValue.tekenMenaelYahidaIrgunit
                                        }
                                        $scope.organizationUnitsManagers[index]= currentValue;
                                    }
                                });
                                //reArrangeUsersVsUnitsManagers();
                                removeDuplicateMngrs();
                            }
                            else { 
                                toastrService.error('', 'לא ניתן היה למצוא משתמשים אשר הבקשה תופנה אליהם');
                            }
                        }, function errorCallback(res) {
                            toastrService.error('', 'לא ניתן היה למצוא משתמשים אשר הבקשה תופנה אליהם');
                        });
                    }
                }, function errorCallback(res) {
                    toastrService.error('', 'העובד שנבחר לא שובץ ליחידה ארגונית.');
            });
            }
        }

        $scope.setEmptyRepresentWorker = function (selected) {
            if (selected == '' || selected == "" || selected.originalObject == undefined) {
                $scope.selectedRepresentWorker = {
                    idNumber: '',
                    fullName: '',
                    email: ''
                }
                PayrollServise.GetAttendanceFigures($rootScope.getWSPostModel()).then(function successCallback(res) {
                    if (res.data.length > 0) {
                        $scope.vacationData = res.data;
                        if ($scope.vacationData.length > 0) {
                            reArrangeVacationData();
                        }
                    }
                    else {
                        scope.vacationLeft = "אין מידע";
                    }
                }, function errorCallback(res) {
                    scope.vacationLeft = "אין מידע";
                });
                if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_DINAMIM) {//read the mngrs of next step and send them the request. should be menaalim yeshirim
                    if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.mishtamsheiKvuzaModel[0].mishtameshId == 2) {//mishtamesh id 2 in kvuzot dinamiyot is menaalim yeshirim. see table 1637 in ka_tavla
                        Processes.getUserManagers($scope.currentYehidaIrgunit, $scope.getWSPostModel).then(function successCallback(res) {
                            $scope.organizationUnitsManagers = [];
                            $scope.organizationUnitsManagers = res.data;
                            removeDuplicateMngrs();
                        }, function errorCallback(res) {
                            
                        });
                    }
                }
            }
        }

        $scope.onUnitManagerChange = function (selected) {
            $scope.selectedUnitManager.idNumber = selected.originalObject.idNumber;
            $scope.selectedUnitManager.fullName = selected.originalObject.fullName;
            $scope.selectedUnitManager.rashutNumber = selected.originalObject.rashutNumber;
        }

        $scope.onAcademicInstChange = function (selected) {
            $scope.courseForm.courseInstitution = selected.originalObject.NAME;
            $scope.courseForm.courseInstitutionID = selected.originalObject.ID;
        }

        $scope.setEmptyAcademicInst = function (selected) {
            if(selected == '' || selected == "") {
                $scope.courseForm.courseInstitution = '';
                $scope.courseForm.courseInstitutionID = '';
            }
        }

        $scope.setEmptyMngr = function (selected) {
            if (selected =='' || selected == "") {
                $scope.selectedUnitManager.idNumber = '';
                $scope.selectedUnitManager.fullName = '';
                $scope.selectedUnitManager.rashutNumber = '';
            }
        }

        $scope.setNewEmployeeFilter = function (selected) {
            if (selected !== undefined) {
                $scope.filters.selectedEmployeeFilter = selected.originalObject.user.calculatedIdNumber;
                $scope.filterMyProcesses('employee');
            }
        }

        $scope.setEmptyEmployeeFilter = function (selected) {
            if (selected == '') {
                $scope.filters.selectedEmployeeFilter = null;
            }
        }

        $scope.setNewTargetUserId = function (selected) {
            if (selected !== undefined) {
                $scope.myProcessesData[this.$parent.$index].TARGET_USERID = selected.originalObject.zehut;
                $scope.isNewTargetUser[this.$parent.$index] = true;
            }
        }

        $scope.setEmptyTargetUserId = function (selected) {
                
            if (selected == '') {
                $scope.myProcessesData[this.$parent.$index].TARGET_USERID = $rootScope.getWSPostModel().idNumber;
                $scope.isNewTargetUser[this.$parent.$index] = false;
            }
        }

        $scope.onDateFocus = function () {
            $scope.ProcessForm.from.$setValidity('range', true);
            $scope.ProcessForm.from.$setValidity('custom', true);
            $scope.ProcessForm.from.$setValidity('required', true);

            $scope.ProcessForm.to.$setValidity('range', true);
            $scope.ProcessForm.to.$setValidity('custom', true);
            $scope.ProcessForm.to.$setValidity('required', true);
        }

        $scope.onDateChange = function () {
            if ($scope.datesRange.from != undefined && $scope.datesRange.to != undefined) {

                var year = $scope.datesRange.from.slice(6, 10), month = $scope.datesRange.from.slice(3, 5), day = $scope.datesRange.from.slice(0, 2);
                var fromDate = new Date(year, parseInt(month)-1, day);

                year = $scope.datesRange.to.slice(6, 10), month = $scope.datesRange.to.slice(3, 5), day = $scope.datesRange.to.slice(0, 2);
                var toDate = new Date(year, parseInt(month)-1, day);

                if ($scope.compareDates(fromDate, toDate) > 0) {
                    $scope.ProcessForm.to.$setValidity('range', false);
                }
            }
        }

        $scope.removeUndefinedMngrs = function (array) {
            if (array.length <= 0) return;
            var copyArray = angular.copy(array);
            array = [];
            for (var arrIndx = 0; arrIndx < copyArray.length; arrIndx++) {
                if (copyArray[arrIndx] != undefined)
                    array.push(copyArray[arrIndx]);
            }
            return array;
        }
        
        /*submit the form function*/
        $scope.postAForm = function () {

            if ($scope.ProcessForm.$invalid)
                return void 0;

            if ($scope.currentRunningStep.rechivId == 5 || $scope.currentRunningStep.rechivId == 7) {//is a vacation form

                if (!$scope.ProcessForm.from.$dirty) {
                    $scope.ProcessForm.from.$setValidity('required', false);
                }
                if (!$scope.ProcessForm.to.$dirty) {
                    $scope.ProcessForm.to.$setValidity('required', false);
                }
                var vacationFormDataList = new Array();
                if ($scope.currentRunningStep.misparShalav == 1) {
                    _.map($scope.organizationUnitsManagers, function (mishtamesh) {
                        var vacationFormData = {
                            selectedProcess: angular.copy($scope.selectedProcess),
                            datesRange: angular.copy($scope.datesRange),
                            vacationReason: angular.copy($scope.ProcessForm.vacationReason.$modelValue),
                            vacationLeft: angular.copy($scope.vacationLeft),
                            ovedNesuBakasha: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.idNumber : null,
                            ovedNesuBakashaName: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.fullName : null,
                            ovedModel: angular.copy($scope.getWSPostModel),
                            targetUser: zehutToTargetUserModel(mishtamesh.idNumber, mishtamesh.fName, mishtamesh.lName, mishtamesh.rashutNumber),
                            tt_tahalich_id: $scope.selectedProcess.id,
                            tt_shalav_id: $scope.nextStepOfCurrent != null ? $scope.nextStepOfCurrent.recId : $scope.currentRunningStep.recId, /*new 18.06.18 was $scope.currentRunningStep.recId,*/
                            curr_mispar_shalav: $scope.currentRunningStep.misparShalav + 1 <= $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length ? $scope.currentRunningStep.misparShalav + 1 : $scope.currentRunningStep.misparShalav,
                            shlavim_counter: $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length
                        }
                        vacationFormDataList.push(vacationFormData);
                    });
                }
                else {
                    _.map($scope.currentMishtamsheiKvuzaOfCurrentStep, function (mishtamesh) {
                        var vacationFormData = {
                            selectedProcess: angular.copy($scope.selectedProcess),
                            datesRange: angular.copy($scope.datesRange),
                            vacationReason: angular.copy($scope.ProcessForm.vacationReason.$modelValue),
                            vacationLeft: angular.copy($scope.vacationLeft),
                            ovedNesuBakasha: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.idNumber : null,
                            ovedNesuBakashaName: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.fullName : null,
                            ovedModel: angular.copy($scope.getWSPostModel),
                            targetUser: zehutToTargetUserModel(mishtamesh.zehut, mishtamesh.shem_prati, mishtamesh.shem_mishpaha, mishtamesh.rashut),
                            tt_tahalich_id: $scope.selectedProcess.id,
                            tt_shalav_id: $scope.nextStepOfCurrent != null ? $scope.nextStepOfCurrent.recId : $scope.currentRunningStep.recId, /*new 18.06.18 was $scope.currentRunningStep.recId,*/
                            curr_mispar_shalav: $scope.currentRunningStep.misparShalav + 1 <= $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length ? $scope.currentRunningStep.misparShalav + 1 : $scope.currentRunningStep.misparShalav,
                            shlavim_counter: $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length
                        }
                        vacationFormDataList.push(vacationFormData);
                    });
                }

                Processes.postEmployeeVacationForm(vacationFormDataList).then(function successCallback(res) {
                    if(res.status == 200) {
                        if(res.data.mailRc > 0 && res.data.processRc > 0) {
                            toastrService.success('בקשתך התקבלה במערכת!');
                            $state.go("Home");
                        }
                        else if(res.data.mailRc == 0) {
                            toastrService.warning("שים לב, מייל לא נשלח כי חסרים נתונים", "בקשתך התקבלה במערכת!");
                            $state.go("Home");
                        }
                        if (res.data.processRc == 0) {
                            toastrService.warning("נסה שוב", "לא נוצר תהליך לניהול המשך הבקשה עקב שגיאה.");
                        }
                    }
                    else {
                        toastrService.warning("אנא נסה שוב", "התרחשה שגיאה במהלך יצירת הבקשה");
                    }
                }, function errorCallback(res) {
                    toastrService.error('', 'תקלת מערכת!');
                });
            }
            else if ($scope.currentRunningStep.rechivId == 3) {//is a course form
                
                if (isEmpty($scope.courseForm.empFirstName)) {
                    $scope.courseForm.empFirstName = $scope.$$credentials.firstName;
                }
                if (isEmpty($scope.courseForm.empLastName)) {
                    $scope.courseForm.empLastName = $scope.$$credentials.lastName;
                }
                if (isEmpty($scope.courseForm.empId)) {
                    $scope.courseForm.empId = $scope.$$credentials.idNumber;
                }

                var courseFormDataList = [];
                if ($scope.currentRunningStep.misparShalav == 1) {
                    _.map($scope.organizationUnitsManagers, function (mishtamesh) {
                        var courseFormData = {
                            selectedProcess: angular.copy($scope.selectedProcess),
                            formFields: angular.copy($scope.courseForm),
                            ovedNesuBakasha: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.idNumber : null,
                            ovedNesuBakashaName: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.fullName : null,
                            ovedModel: angular.copy($scope.getWSPostModel),
                            targetUser: zehutToTargetUserModel(mishtamesh.idNumber, mishtamesh.fName, mishtamesh.lName, mishtamesh.rashutNumber),
                            tt_tahalich_id: $scope.selectedProcess.id,
                            tt_shalav_id: $scope.nextStepOfCurrent != null ? $scope.nextStepOfCurrent.recId : $scope.currentRunningStep.recId, /*new 18.06.18 was $scope.currentRunningStep.recId,*/
                            curr_mispar_shalav: $scope.currentRunningStep.misparShalav + 1 <= $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length ? $scope.currentRunningStep.misparShalav + 1 : $scope.currentRunningStep.misparShalav,
                            shlavim_counter: $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length
                        }
                        courseFormDataList.push(courseFormData);
                    });
                }
                else {
                    _.map($scope.currentMishtamsheiKvuzaOfCurrentStep, function (mishtamesh) {
                        var courseFormData = {
                            selectedProcess: angular.copy($scope.selectedProcess),
                            formFields: angular.copy($scope.courseForm),
                            ovedNesuBakasha: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.idNumber : null,
                            ovedNesuBakashaName: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.fullName : null,
                            ovedModel: angular.copy($scope.getWSPostModel),
                            targetUser: zehutToTargetUserModel(mishtamesh.zehut, mishtamesh.shem_prati, mishtamesh.shem_mishpaha, mishtamesh.rashut),
                            tt_tahalich_id: $scope.selectedProcess.id,
                            tt_shalav_id: $scope.nextStepOfCurrent != null ? $scope.nextStepOfCurrent.recId : $scope.currentRunningStep.recId, /*new 18.06.18 was $scope.currentRunningStep.recId,*/
                            curr_mispar_shalav: $scope.currentRunningStep.misparShalav + 1 <= $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length ? $scope.currentRunningStep.misparShalav + 1 : $scope.currentRunningStep.misparShalav,
                            shlavim_counter: $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length
                        }
                        courseFormDataList.push(courseFormData);
                    });
                }

                //var courseFormData = {
                //    selectedProcess: angular.copy($scope.selectedProcess),
                //    formFields: angular.copy($scope.courseForm),
                //    //new 30.5.18
                //    ovedNesuBakasha: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.idNumber : 0,
                //    ovedNesuBakashaName: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.fullName : null,
                //    ovedModel: angular.copy($scope.getWSPostModel),
                //    targetUser: angular.copy($scope.selectedUnitManager)
                //}

                Processes.postEmployeeCourseForm(courseFormDataList).then(function successCallback(res) {
                    if (res.status == 200) {
                        if (res.data.mailRc > 0 && res.data.processRc > 0) {
                            toastrService.success('בקשתך התקבלה במערכת!');
                            $state.go("Home");
                        }
                        else if (res.data.mailRc == 0) {
                            toastrService.warning("שים לב, מייל לא נשלח כי חסרים נתונים", "בקשתך התקבלה במערכת!");
                            $state.go("Home");
                        }
                        if (res.data.processRc == 0) {
                            toastrService.warning("נסה שוב", "לא נוצר תהליך לניהול המשך הבקשה עקב שגיאה.");
                        }
                    }
                    else {
                        toastrService.warning("אנא נסה שוב", "התרחשה שגיאה במהלך יצירת הבקשה");
                    }
                }, function errorCallback(res) {
                    toastrService.error('', 'תקלת מערכת!');
                });
            }
        }
        
        var zehutToTargetUserModel = function (zehut, prati, mishpaha, rashut) {
            return {
                idNumber: zehut,
                fullName: prati + ' ' + mishpaha,
                rashutNumber: rashut
            }
        }

        var reArrangeOrganizationUnitsManagers = function () {
            var temp = angular.copy($scope.organizationUnitsManagers);
            $scope.organizationUnitsManagers = [];
            temp.map(function (currentValue, index, temp) {
                
                if (currentValue.idNumber != $scope.getWSPostModel.calculatedIdNumber) {
                    currentValue = {
                        fullName: currentValue.fName + " " + currentValue.lName,
                        idNumber: currentValue.idNumber,
                        rashutNumber: currentValue.rashutNumber,
                        zehutNatzigKoachAdam: currentValue.zehutNatzigKoachAdam,
                        tekenKoachAdam: currentValue.tekenKoachAdam,
                        tekenMenaelYahidaIrgunit: currentValue.tekenMenaelYahidaIrgunit
                    }
                    $scope.organizationUnitsManagers.push(currentValue);
                }
            });
        }

        var removeDuplicateMngrs = function () {
            if ($scope.organizationUnitsManagers.length > 0) {
                var temp = angular.copy($scope.organizationUnitsManagers);
                $scope.organizationUnitsManagers = new Array();
                temp.map(function (currentValue, index, organizationUnitsManagers) {
                    if (!isExistMngr($scope.organizationUnitsManagers, currentValue)) {
                        currentValue = {

                            fName:currentValue.fName,
                            idNumber:currentValue.idNumber,
                            lName:currentValue.lName,
                            ramaIrgunit:currentValue.ramaIrgunit,
                            rashutNumber: currentValue.rashutNumber,
                            supervisorUnitNumber: currentValue.supervisorUnitNumber,
                            tekenKoachAdam: currentValue.tekenKoachAdam,
                            tekenMenaelYahidaIrgunit: currentValue.tekenMenaelYahidaIrgunit,
                            treeNumber: currentValue.treeNumber,
                            unitDescription: currentValue.unitDescription,
                            unitNumber: currentValue.unitNumber,
                            zehutNatzigKoachAdam: currentValue.zehutNatzigKoachAdam,


                            //fullName: currentValue.fullName,
                            //idNumber: currentValue.idNumber,
                            //rashutNumber: currentValue.rashutNumber,
                            //zehutNatzigKoachAdam: currentValue.zehutNatzigKoachAdam,
                            //tekenKoachAdam: currentValue.tekenKoachAdam,
                            //tekenMenaelYahidaIrgunit: currentValue.tekenMenaelYahidaIrgunit
                        }
                        $scope.organizationUnitsManagers[index] = currentValue;
                    }
                });
                $scope.organizationUnitsManagers = $scope.removeUndefinedMngrs($scope.organizationUnitsManagers);
            }
        }

        var isExistMngr = function (arr, mngrRec) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].idNumber == mngrRec.idNumber) {
                    return true;
                }
            }
            return false;
        }

        var reArrangeRepresentedWorkers = function () {
            if ($scope.representedWorkers.length > 0) {
                var copyArr = new Array();
                _.map($scope.representedWorkers, function (worker) {
                    if (!isExistWorker(copyArr, worker.idNumber)) {
                        copyArr.push(worker);
                    }
                    //below add the represented workers to the users list, to be displayed inside employee filter element
                    if ($scope.usersList.length > 0) {
                        $scope.usersList.push(
                            {
                                id: worker.idNumber,
                                rashut: "",
                                kodMedaveach: -1,
                                user: {
                                    lastName: worker.fullName.substr(0, worker.fullName.indexOf(' ')),
                                    firstName: worker.fullName.substr(worker.fullName.indexOf(' ') + 1)
                                }
                            }
                        );
                    }
                });
                removeDuplicatesFromUsersList();

                $scope.representedWorkers = new Array();
                $scope.representedWorkers = angular.copy(copyArr);
            }
        }

        var isExistWorker = function (arr, workerId) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].idNumber == workerId) {
                    return true;
                }
            }
            return false;
        }

        var reArrangeVacationData = function () {
            if($scope.vacationData.length > 0) {
                $scope.vacationData[0].Data.filter(function (row) {
                    $scope.vacationAmount.push(row.sort(function (a, b) { return (a.columnNumber > b.columnNumber); }));
                });
                $scope.vacationLeft = $scope.vacationAmount[$scope.vacationAmount.length - 1][4].columnNumber == 4 ? $scope.vacationAmount[$scope.vacationAmount.length - 1][4].value : "אין מידע";
            }
        }

        var isExist = function (arr, value) {
            for (var i = 0; i < arr.length; i++ ) {
                if(arr[i].recId == value.recId) {
                    return true;
                }
            }
            return false;
        }

        var isEmpty = function (str) {
            return str == "" || str == '' || str == null || str == undefined;
        }
        
        var getSuitableRequestProcessStep = function (requestProcessItem) {//הבקשות שהופנו אלי
            _.map($scope.allProcessesData.processes, function (process) {
                if (process.recId == requestProcessItem.TT_TAHALICH_ID)
                    _.map(process.shlavim, function (step) {
                        //NEW - get the next step mishtamshim (if current shalav is 2 so get mishtamshim of shalav 3)
                        var misparShalav = requestProcessItem.CURR_MISPAR_SHALAV + 1 < requestProcessItem.SHLAVIM_COUNTER ? requestProcessItem.CURR_MISPAR_SHALAV + 1 : requestProcessItem.SHLAVIM_COUNTER;
                        if (step.misparShalav == misparShalav)
                            $scope.stepToProcess.push(step);
                    });
            });
        }

        var setMenaalimYeshirimAsMishtamsheiKvuza = function (menaalimYeshirim) {
            if (menaalimYeshirim.length <= 0) {
                return [];
            }
            var temp = new Array();
            for (var i = 0; i < menaalimYeshirim.length; i++) {
                temp.push(
                    {
                        shem_prati: menaalimYeshirim[i].fName,
                        shem_mishpaha: menaalimYeshirim[i].lName,
                        idNumber: menaalimYeshirim[i].idNumber
                    }
                );
            }
            return temp;
        }

        var removeDuplicatesInMenaalimYeshirim = function (menaalimYeshirim) {
            var temp = menaalimYeshirim;
            menaalimYeshirim = [];
            _.map(temp, function (menael, index) {
                if (!isExistMenael(menael, menaalimYeshirim)) {
                    menaalimYeshirim.push(
                        {
                            shem_prati: menael.shem_prati,
                            shem_mishpaha: menael.shem_mishpaha,
                            idNumber: menael.idNumber
                        }
                    );
                }
            });
            return menaalimYeshirim;
        }

        var isExistMenael = function (menael, menaalimYeshirim) {
            if(menaalimYeshirim.length <= 0)
                return false;
            for (var i = 0; i < menaalimYeshirim.length; i++) {
                if (menaalimYeshirim[i].idNumber == menael.idNumber) {
                    return true;
                }
            }
            return false;
        }

        var removeDuplicatesFromUsersList = function () {
            var temp = angular.copy($scope.usersList);
            $scope.usersList = [];
            _.map(temp, function (user, index) {
                if (!isExistUser(user, $scope.usersList)) {
                    $scope.usersList.push(user);
                }
            });
        }

        var isExistUser = function (user, users) {
            if (users.length <= 0)
                return false;
            for (var i = 0; i < users.length; i++) {
                if (users[i].user.idNumber == user.user.idNumber) {
                    return true;
                }
            }
            return false;
        }

        var setMishtamsheiKvuzaPerProcess = function (stepOfReqProcess, index) {
            //KVUZA REGILA MNGRS
            if (stepOfReqProcess.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_REGILA) {
                Processes.getMishtamsheiKvuzaRegila($scope.getWSPostModel, stepOfReqProcess.kvuzatMishtamshimModel.mishtamsheiKvuzaModel).then(function successCallback(res) {
                    if (res.status == 200) {
                        $scope.mishtamsheiKvuzaPerProcess[index] = res.data;
                        reArrangeMishtamsehiKvuzaRegilaInMyProcessesTab(index);//to be the same structure as kvuza dinamit
                    }
                    else {
                        $scope.mishtamsheiKvuzaPerProcess[index] = {};
                    }
                }, function errorCallback(res) {
                    $scope.mishtamsheiKvuzaPerProcess[index] = {};
                });
            }
            //DKVUZA DINAMIT MNGRS
            else if (stepOfReqProcess.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_DINAMIM) {
                if(stepOfReqProcess.kvuzatMishtamshimModel.mishtamsheiKvuzaModel[0].mishtameshId == 2) {//if it is menaalim regilim
                    Processes.getUserManagers($scope.currentYehidaIrgunit, $scope.getWSPostModel).then(function successCallback(res) {
                        
                        $scope.mishtamsheiKvuzaPerProcess[index] = setMenaalimYeshirimAsMishtamsheiKvuza(res.data);
                        $scope.mishtamsheiKvuzaPerProcess[index] = removeDuplicatesInMenaalimYeshirim($scope.mishtamsheiKvuzaPerProcess[index]);
                    }, function errorCallback(res) {
                        
                    });
                }
                else {
                    Processes.getMishtamsheiKvuzaDinamit($scope.getWSPostModel, stepOfReqProcess.kvuzatMishtamshimModel.mishtamsheiKvuzaModel).then(function successCallback(res) {
                    if (res.status == 200) {
                        $scope.mishtamsheiKvuzaPerProcess[index] = res.data;
                    }
                    else {
                        $scope.mishtamsheiKvuzaPerProcess[index] = {};
                    }
                }, function errorCallback(res) {
                    $scope.mishtamsheiKvuzaPerProcess[index] = {};
                });
                }
            }
        }

        $scope.amITargetUser = function (targetUserId) {

            Processes.getMyRecIdFromUsers($scope.getWSPostModel).then(function successCallback(res) {
                 
                return res.data.ID == targetUserId;
                
            }, function errorCallback(res) {
                return false;
            });
        }

        $scope.amIPartOfProcess = function(process) {
            return process.TARGET_USER.calculatedIdNumber == $scope.getWSPostModel.calculatedIdNumber;
        }
        
        //object that hold the current user details
        $scope.getWSPostModel = $rootScope.getWSPostModel();

        //originally arrived as a promise that resolved in route_config file
        $scope.processesData = ProcessesData.data.length > 0 ? ProcessesData.data : {};
        $scope.allMyProcessesData = MyProcesses.data.length > 0 ? MyProcesses.data : {};
        $scope.usersList = UsersList.data.length > 0 ? UsersList.data : {};
        $scope.vacationData = VacationData.data.length > 0 ? VacationData.data : {};
        $scope.academicInsts = AcademicInst.data.length > 0 ? AcademicInst.data : {};
        $scope.currentYehidaIrgunit = YehidaIrgunit.data.length > 0 ? YehidaIrgunit.data : {};
        $scope.myRequestProcessesData = MyRequestProcesses.data.length > 0 ? MyRequestProcesses.data : {};

        $scope.allProcessesData = {
            processes: []
        }

        if ($scope.processesData.length > 0) {
            $scope.allProcessesData.processes = $scope.processesData;
        }
        if ($scope.allMyProcessesData.length > 0) {
            $scope.filters.selectedStatusFilter = 0;
            $scope.filterMyProcesses('status');//set the default filter viewed

            //NEW 18.06.18 - create a suitable array that hold the correct step of the process using index (בקשות שהופנו אלי)
            $scope.stepToProcess = [];
            if ($scope.processesData.length > 0) {
                _.map($scope.allMyProcessesData, function (ReqProcess) {
                    getSuitableRequestProcessStep(ReqProcess);
                });

                //get the mishtamshei kvuza for each process 
                if ($scope.stepToProcess.length == $scope.allMyProcessesData.length) {//must happen!
                    $scope.mishtamsheiKvuzaPerProcess = [];
                    _.map($scope.allMyProcessesData, function (process, index) {
                        setMishtamsheiKvuzaPerProcess($scope.stepToProcess[index], index);
                    });
                }
            }
        }
        else {
            $scope.myProcessesIsEmpty = true;
        }
        if ($scope.vacationData.length > 0) {
            reArrangeVacationData();
        }

        //NEW FOR YEHIDOT IRGUNIYOT MNGRS
        if ($scope.currentYehidaIrgunit.length > 0) {
            //bolow request ask if the wspostmodel is a secretary and if so, fill represented workers list. if not, the returned list is empty
            Processes.getRepresentedWorkers($scope.currentYehidaIrgunit, $scope.getWSPostModel).then(function successCallback(res) {
                if(res.data.length > 0) {
                    $scope.representedWorkers = res.data;
                    reArrangeRepresentedWorkers();
                }
            }, function errorCallback(res) {
                
            });
        }
        
        if ($scope.usersList.length > 0) {
            removeDuplicatesFromUsersList();
        }

        //new for הבקשות שלי
        if ($scope.myRequestProcessesData.length > 0) {
            $scope.filterMyProcesses('all');
        }
        else {
            $scope.myRequestProcessesIsEmpty = true;
        }
    }
})();
