﻿(function () {
    angular.module('WorkerPortfolioApp').controller('TofesController', TofesController);

    TofesController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'TofesDataService', 'TofesModel', '$Tofes', 'FileUploader', 'toastrService', 'Search'];

    function TofesController($scope, $rootScope, $state, $timeout, TofesDataService, TofesModel, $Tofes, FileUploader, toastrService, Search) {
        
        ///get the base data from api and insert into model
        $scope.tofesModel = TofesModel;

        TofesDataService.UpdateObject($scope.tofesModel, $Tofes.data);

        TofesDataService.CopyRunTimeModel($scope.tofesModel);

        ///update from saved data if have
        TofesDataService.GetExistingData($scope.tofesModel, '$$tofesModel', true);

        ///private variables
        ///get radio buttons data
        $scope.radionStates = TofesDataService.GetRadiosStates();
        ///get custom models for tofesModel lists
        $scope.customModel = TofesDataService.GetCustomModel();
        $scope.childId = 0;
        $scope.goToStep = '';
        ///private variables

        ///go-to next/prev form stap, published from Header directive
        $scope.subscribe('goto.step', function (goTo, direction) {
            if ($scope.validatUploders() || $scope.partForm.$invalid) {
                $scope.ErrorFocus();
                return void [0];
            }

            ///remove autocomplete options
            if ($scope.tofesModel.employee.city !== null && typeof $scope.tofesModel.employee.city === 'object') {
                $scope.tofesModel.employee.city = $scope.tofesModel.employee.city.originalObject.Name;
            }
            if ($scope.tofesModel.employee.street !== null && typeof $scope.tofesModel.employee.street === 'object') {
                $scope.tofesModel.employee.street = $scope.tofesModel.employee.street.originalObject.Name;
            }

            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            ///go to the step
            $state.go(goTo);
        });

        ///main submit form function
        $scope.nextForm = function () {
            ///form is invalid
            if ($scope.validatUploders() || $scope.partForm.$invalid) {
                $scope.ErrorFocus();
                return void [0];
            }

            ///remove autocomplete options
            if (typeof $scope.tofesModel.employee.city === 'object') {
                $scope.tofesModel.employee.cityID = $scope.tofesModel.employee.city.originalObject.ID;
                $scope.tofesModel.employee.city = $scope.tofesModel.employee.city.originalObject.Name;
            }
            if (typeof $scope.tofesModel.employee.street === 'object') {
                $scope.tofesModel.employee.streetID = $scope.tofesModel.employee.street.originalObject.ID;
                $scope.tofesModel.employee.street = $scope.tofesModel.employee.street.originalObject.Name;
            }

            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);

            $scope.publish('$$states', 'current');
            return $scope.goToStep.isempty() ? (void [0]) : $state.go($scope.goToStep);
        }
        ///validate all the uploaders in the scope
        $scope.validatUploders = function () {
            $scope.partForm.idNumberFiles.$setValidity('required', true);
            if (!($scope.partForm.firstName.$pristine && $scope.partForm.lastName.$pristine && $scope.partForm.idNumber.$pristine && $scope.partForm.birthDate.$pristine &&
                    $scope.partForm.immigrationDate.$pristine && $scope.partForm.gender.$pristine && $scope.partForm.maritalStatus.$pristine && $scope.partForm.city.$pristine && $scope.partForm.street.$pristine)) {

                if ($scope.IdNumberuploader.queue.length < 1 || ($scope.IdNumberuploader.queue.length > 0 && (isNaN($scope.IdNumberuploader.progress) || $scope.IdNumberuploader.progress < 100))) {
                    $scope.partForm.idNumberFiles.$setValidity('required', false);
                    return true;
                }
            }

            for (var i in $scope) {
                if ($scope[i] instanceof FileUploader) {
                    ///SAVE UPLOADED FILES TO BE SENT
                    $rootScope.FilterFiles($scope.tofesModel.TofesControllerFilesList, $scope[i].queue, i);
                    $scope[i].Files = $scope.tofesModel.TofesControllerFilesList.filter(function (item) {
                        return item.origin === i;
                    }).length;

                    if ($scope[i].UploaderValidation())
                        return true;
                }
            }
            return false;
        }
        $scope.ChangeRadioStatus = function (radioName) {
            $scope.partForm[radioName].$pristine = false;
        }
        ///AUTOCOMPLETE
        $scope.streets = [];
        $scope.searchCities = function (typed) {
            if (!typed || typed.length < 1)
                return void [0];
            return Search.citySearch({ query: typed })
        }
        $scope.cityFocusOut = function ($event) {
            var text = $('#city_value').val();

            if (!$scope.tofesModel.employee.city) {
                ///ALLOW FREE TEXT ON STREET!
                if (text !== null && text.length > 1) {
                    $scope.$broadcast('angucomplete-alt:changeInput', 'city', text);
                    $scope.tofesModel.employee.city = text;

                    $scope.$broadcast('angucomplete-alt:clearInput', 'street');
                } else {
                    $scope.$broadcast('angucomplete-alt:clearInput');
                }
            } else {
                $scope.streets = typeof $scope.tofesModel.employee.city.originalObject !== 'undefined' ? $scope.tofesModel.employee.city.originalObject.Streets : new Array();
            }
        }

        $scope.streetFocusOut = function () {
            ///ALLOW FREE TEXT ON STREET!
            var text = $('#street_value').val();

            if ($scope.tofesModel.employee.street == undefined && text !== null && text.length > 1) {
                ///ALLOW FREE TEXT ON STREET!
                $scope.$broadcast('angucomplete-alt:changeInput', 'street', text);
                $scope.tofesModel.employee.street = text;
            }
        }
        $scope.streetsSearch = function (str) {
            if ($scope.streets == undefined || $scope.streets.length <= 0) {
                Search.citySearch({ query: $scope.tofesModel.employee.city }).then(function (res) {
                    $scope.streets = res.data.data[0].Streets;
                    return $scope.streetsSearch(str);
                });
            } else {
                var matches = [];
                $scope.streets.forEach(function (street) {
                    if (street.Name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0 && matches.length < 12) {
                        matches.push(street);
                    }
                });
                return matches;
            }
        };
        ///AUTOCOMPLETE

        $scope.$watch('tofesModel.employee.street', function (newValue, oldValue) {
            if (typeof newValue === 'undefined' || newValue === null || newValue === oldValue)
                return void [0];
            $scope.CheckZip(true);
        });
        $scope.$watch('tofesModel.employee.city', function (newValue, oldValue) {
            if (typeof newValue === 'undefined' || newValue === null || newValue === oldValue)
                return void [0];
            $scope.CheckZip(true);
        });

        //zip-code-validation city="{{tofesModel.employee.city}}" street="{{tofesModel.employee.street}}"
        $scope.CheckZip = function (Do) {
            if (($scope.tofesModel.employee.zip === null || $scope.tofesModel.employee.zip.length < 4 || Do) &&
                (typeof $scope.tofesModel.employee.city !== 'undefined' && $scope.tofesModel.employee.city !== null && typeof $scope.tofesModel.employee.street !== 'undefined' && $scope.tofesModel.employee.street !== null))
                $scope.doZipCheck($scope.tofesModel.employee.city, $scope.tofesModel.employee.street, $scope.tofesModel.employee.houseNumber).then(function (res) {
                    var parser = new DOMParser();
                    var doc = parser.parseFromString(res.data, 'text/html');
                    var response = doc.body.innerText.trim();
                    if (response.indexOf('RES8') > -1) {
                        $scope.tofesModel.employee.zip = response.replace('RES8', '');
                    } else {
                        $scope.doZipCheck($scope.tofesModel.employee.city, $scope.tofesModel.employee.street).then(function (res) {
                            var parser = new DOMParser();
                            var doc = parser.parseFromString(res.data, 'text/html');
                            var response = doc.body.innerText.trim();
                            if (response.indexOf('RES8') > -1) {
                                $scope.tofesModel.employee.zip = response.replace('RES8', '');
                            } else {
                                toastrService.info('תקן על מנת לקבל מיקוד.', 'כתובת לא תקינה');
                            }
                        });
                    }
                });

        }
        $scope.doZipCheck = function (city, street, houseNumber) {
            var obj = { Location: city, Street: street };
            if (houseNumber !== undefined && houseNumber !== null)
                obj.House = houseNumber;
            return Search.ZipSearch(obj).then(function (res) {
                return res;
            });
        }

        ///$scope.IdNumberuploader file uploader & filters
        $scope.IdNumberuploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'idNumberFile',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.IdNumberuploader.filters.splice(1, 1);
        $scope.IdNumberuploader.filters.push($scope.queueLimitFilter);
        $scope.IdNumberuploader.filters.push($scope.uniqueKeyFilter);
        $scope.IdNumberuploader.filters.push($scope.sizeFilter);
        $scope.IdNumberuploader.filters.push($scope.typeFilter);
        $scope.IdNumberuploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.IdNumberuploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.IdNumberuploader.onCompleteItem = $scope._onCompleteItem;
        $scope.IdNumberuploader.onCompleteAll = $scope._onCompleteAll;
        $scope.IdNumberuploader.removeFromQueue = $scope._removeFromQueue;
        $scope.IdNumberuploader.UploaderValidation = $scope._UploaderValidation;
        ///end $scope.IdNumberuploader file uploader & filters
        ///$scope.assessorUploader file uploader & filters
        $scope.assessorUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'assessorFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.assessorUploader.filters.splice(1, 1);
        $scope.assessorUploader.filters.push($scope.queueLimitFilter);
        $scope.assessorUploader.filters.push($scope.uniqueKeyFilter);
        $scope.assessorUploader.filters.push($scope.sizeFilter);
        $scope.assessorUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.assessorUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.assessorUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.assessorUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.assessorUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.assessorUploader.UploaderValidation = $scope._UploaderValidation;
        ///end $scope.assessorUploader file uploader & filters
        ///FILE UPLOADER

        $scope.ErrorFocus = function () {
            if (typeof $scope.partForm.$error.required == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
            if (typeof $scope.partForm.$error.custom == 'object') {
                $scope.partForm.$error.custom[0].$$element.focus();
                return;
            }
        }

        if ($scope.streets == undefined || $scope.streets.length <= 0) {
            Search.citySearch({ query: $scope.tofesModel.employee.city }).then(function (res) {
                $scope.streets = res.data.data[0].Streets;
            });
        }
        var GetMaxDate = function () {
            var _d = new Date();
            _d.setDate(_d.getDate() - 1);
            _d.setFullYear((_d.getFullYear() - 14));
            return _d;
        }
        $scope.bdOptions = {
            maxDate: GetMaxDate()
        }
        $scope.idOptions = {
            maxDate: new Date()
        }

        $scope.CheckZip();
    }
})();