﻿(function () {
    angular.module('WorkerPortfolioApp').controller('TaxAdjustmentController', TaxAdjustmentController);

    TaxAdjustmentController.$inject = ['$scope', '$rootScope', '$state', 'TofesDataService', 'TofesModel', 'FileUploader', 'toastrService', 'Search'];

    function TaxAdjustmentController($scope, $rootScope, $state, TofesDataService, TofesModel, FileUploader, toastrService, Search) {
        ///private variables
        ///get radio buttons data
        $scope.radionStates = TofesDataService.GetRadiosStates();
        ///get custom models for tofesModel lists
        $scope.customModel = TofesDataService.GetCustomModel();
        $scope.tofesModel = TofesModel;

        ///update from saved data if have
        TofesDataService.GetExistingData($scope.tofesModel, '$$tofesModel', true);

        $scope.ChengeRequestRule = function () {
            if (!$scope.tofesModel.taxCoordination.request) {
                $scope.tofesModel.taxCoordination.reason = null;
            }
        }

        ///go-to next/prev form stap, published from Header directive
        $scope.subscribe('goto.step', function (goTo, direction) {
            if ($scope.validatUploders() || $scope.partForm.$invalid && direction) {
                $scope.ErrorFocus();
                return void [0];
            }
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            ///go to the step
            $state.go(goTo);
        });

        ///main submit form function
        $scope.nextForm = function () {
            ///form is invalid
            if ($scope.partForm.$invalid || $scope.validatUploders()) {
                $scope.ErrorFocus();
                return void [0];
            }
            
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);

            $scope.publish('$$states', 'current');
            return $scope.goToStep.isempty() ? (void [0]) : $state.go($scope.goToStep);
        }
        ///validate all the uploaders in the scope
        $scope.validatUploders = function () {
            for (var i in $scope) {
                if ($scope[i] instanceof FileUploader) {
                    ///SAVE UPLOADED FILES TO BE SENT
                    $rootScope.FilterFiles($scope.tofesModel.TaxAdjustmentControllerFilesList, $scope[i].queue, i);
                    $scope[i].Files = $scope.tofesModel.TaxAdjustmentControllerFilesList.filter(function (item) {
                        return item.origin === i;
                    }).length;

                    if ($scope[i].UploaderValidation())
                        return true;
                }
            }
            return false;
        }

        ///employers list
        $scope.copyEmployer = function () {
            var employer = angular.copy($scope.customModel.employerModel);
            employer.$id = $scope.tofesModel.taxCoordination.employers.length + 1;
            return employer;
        }
        ///add/remove child from list
        $scope.addEmployer = function () {
            $scope.tofesModel.taxCoordination.employers.push($scope.copyEmployer());
        }
        $scope.removeEmployer = function ($id) {
            $scope.tofesModel.taxCoordination.employers = _.filter($scope.tofesModel.taxCoordination.employers, function (item) {
                return item.$id !== $id
            });
            $.map($scope.tofesModel.taxCoordination.employers, function (item, index) {
                item.$id = (index + 1);
            });
        }
        if ($scope.tofesModel.taxCoordination.employers && $scope.tofesModel.taxCoordination.employers.length === 0)
            $scope.addEmployer();

        ///FILE UPLOADERS
        $scope.reasonUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'reasonFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.reasonUploader.filters.splice(1, 1);
        $scope.reasonUploader.filters.push($scope.queueLimitFilter);
        $scope.reasonUploader.filters.push($scope.uniqueKeyFilter);
        $scope.reasonUploader.filters.push($scope.sizeFilter);
        $scope.reasonUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.reasonUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.reasonUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.reasonUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.reasonUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.reasonUploader.UploaderValidation = $scope._UploaderValidation;

        $scope.coordinationUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'coordinationFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.coordinationUploader.filters.splice(1, 1);
        $scope.coordinationUploader.filters.push($scope.queueLimitFilter);
        $scope.coordinationUploader.filters.push($scope.uniqueKeyFilter);
        $scope.coordinationUploader.filters.push($scope.sizeFilter);
        $scope.coordinationUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.coordinationUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.coordinationUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.coordinationUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.coordinationUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.coordinationUploader.UploaderValidation = $scope._UploaderValidation;
        ///END FILE UPLOADERS

        $scope.ErrorFocus = function () {
            if (typeof $scope.partForm.$error.required == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
            if (typeof $scope.partForm.$error.custom == 'object') {
                $scope.partForm.$error.custom[0].$$element.focus();
                return;
            }
        }
    }
})();
