﻿(function () {

    angular.module('WorkerPortfolioApp').controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', '$rootScope', '$window', 'ngDialog', 'toastrService'];

    function HomeController($scope, $rootScope, $window, ngDialog, toastrService) {

        ///TODO=> remove after data fix
        /* *************************************************************************************************************************************************************** */
        $scope.municipalitiesList = [14009, 99964, 32600, 2405, 99208, 4428, 26300,
                    99375, 14000, 92720, 50831, 99957, 99170, 2404, 67800, 99166, 99316,
                    28400, 67000, 99156, 51020, 83570, 39000, 52610, 69800, 90541, 36700,
                    51015, 55599, 4340, 42620, 32630, 99165, 18600, 50099, 6242, 37300, 36800];
        $scope.removeByMunicipalities = function () {
            return !($scope.$$credentials.loggedIn && $scope.municipalitiesList.indexOf($scope.$$credentials.currentMunicipalityId) >= 0);
        }
        /* *************************************************************************************************************************************************************** */

        $scope.linkClick = function (evnt, uiSref) {
            ///open the popup only 4 mobile\tablet apps
            if ($window.innerWidth < 801) {
                evnt.preventDefault();
                evnt.stopPropagation();

                $scope.target = evnt.currentTarget;
                $scope.popTitle = $scope.target.firstChild.textContent;
                $scope.popText = $scope.target.lastChild.textContent;
                $scope.uiSref = $rootScope.$$credentials.loggedIn ? $scope.target.parentNode.attributes.href.nodeValue : '#';

                ngDialog.open({
                    template: '../../Scripts/Views/Ng-Templates/mobile.home-page-popup.html',
                    closeByNavigation: true,
                    closeByDocument: true,
                    closeByEscape: true,
                    scope: $scope,
                    backdrop: 'static',
                    showClose: true,
                    appendClassName: 'ngdialog-custom',
                    name: 'linkClickPopUp'
                });
            }
        }

        $scope.subscribe('VerifayLoginState', function (val) {
            $scope.removeByMunicipalities();

            $scope.$$popUpWindow = sessionStorage.getItem("$$popUpWindow");
            if ($scope.$$popUpWindow === '1')
                return void [0];

            if (val.loggedIn && ngDialog.getOpenDialogs().length === 0 && val.email !== null && val.email.length > 4) {
                sessionStorage.setItem("$$popUpWindow", '1');
                ngDialog.open({
                    template: '../../Scripts/Views/Ng-Templates/email.paycheck-template.html',
                    closeByNavigation: true,
                    closeByDocument: true,
                    closeByEscape: true,
                    backdrop: 'static',
                    width: ($window.innerWidth < 801 ? '85%' : '45%'),
                    showClose: true,
                    appendClassName: 'ngdialog-custom-email-paycheck',
                    name: 'EmailPaycheckClickPopUp',
                    controller: ['$scope', '$rootScope', 'PayCheckEmail', function ($scope, $rootScope, PayCheckEmail) {
                        $scope.iAgree = true;
                        $scope.agreeToRoles = false;
                        $scope.WSPostModel = $rootScope.getWSPostModel();

                        $scope.AgreeToMail = function () {
                            if (!$scope.agreeToRoles) {
                                return void [0];
                            }
                            if (!$scope.iAgree) {
                                $scope.closeThisDialog('value');
                                return void [0];
                            }
                            $scope.WSPostModel.isMobileRequest = $scope.iAgree;
                            var height = {
                                outHeight: Math.round(($window.innerHeight / 100) * 60),
                                inHeight: 0
                            };
                            height.inHeight = height.outHeight > 180 ? (height.outHeight - 60) : 120;

                            PayCheckEmail.UserSendPayCheckToEmail($scope.WSPostModel)
                                .then(function successCallback(res) {
                                    if (res.data) {
                                        $rootScope.$$credentials.email = '';
                                    }
                                    $scope.closeThisDialog('value');

                                    toastrService.success('', 'בקשתך להצטרף לקבלת תלוש במייל התקבלה בהצלחה!');

                                }, function errorCallback(res) {
                                    toastrService.error('', 'תקלת מערכת!');
                                    $scope.closeThisDialog('value');
                                });

                        }
                    }]
                });
            }
        });

        $scope.publish('TransmitLoginState', null);
        $scope.removeByMunicipalities();
    }
})();
