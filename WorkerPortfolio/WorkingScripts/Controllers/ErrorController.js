﻿(function () {
    
    angular.module('WorkerPortfolioApp').controller('ErrorController', ErrorController);

    ErrorController.$inject = ['$scope', '$rootScope', '$stateParams', 'SiteMail'];

    function ErrorController($scope, $rootScope, $stateParams, SiteMail) {

        $scope.$errorType = 0;
        if (typeof $stateParams.AuthenticationErrorToken !== 'undefined')
            $scope.$errorType = 1;
    }
})();
