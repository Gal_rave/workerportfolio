﻿(function () {

    angular.module('WorkerPortfolioApp').controller('AttendanceController', AttendanceController);

    AttendanceController.$inject = ['$scope', '$rootScope', '$state', 'PayrollServise', 'toastrService', '$controllerState', 'AttendanceData'];

    function AttendanceController($scope, $rootScope, $state, PayrollServise, toastrService, $controllerState, AttendanceData) {
        ///scope functions
        var getYear = function (val) {
            var _d = new Date(val, 2, 6);
            return _d.getYear() > 100 ? (_d.getYear() % 100) : _d.getYear();
        }
        var setTitels = function (status) {
            $scope.pageTitle = null;
            $scope.pageSubTitle = null;
            var title = 'פירוט נתוני TYPE בהתפלגות שנתית';
            var subtitle = 'לא קיימים נתוני TYPE לשנת YEAR'
            switch ($controllerState) {
                case 'Vacation':
                    $scope.pageTitle = title.replace('TYPE', 'חופשה');
                    $scope.pageSubTitle = status === 406 ? subtitle.replace('TYPE', 'חופשה').replace('YEAR', $scope.currentYear) : null;
                    break;
                case 'SickLeave':
                    $scope.pageTitle = title.replace('TYPE', 'מחלה');
                    $scope.pageSubTitle = status === 406 ? subtitle.replace('TYPE', 'מחלה').replace('YEAR', $scope.currentYear) : null;
                    break;
                case 'AdvancedStudy':
                    $scope.pageTitle = title.replace('TYPE', 'השתלמות');
                    $scope.pageSubTitle = status === 406 ? subtitle.replace('TYPE', 'השתלמות').replace('YEAR', $scope.currentYear) : null;
                    break;

                default:
                    $state.go('Home');
                    break;
            }
        }
        var replaceInItem = function (collection, item, replace, replaceWidth) {
            if (typeof replaceWidth === 'undefined' || replaceWidth == null)
                replaceWidth = '';
            collection.filter(function (data) { data[item] = data[item].replace(replace, replaceWidth).trim(); });
        }
        $scope.reSortDataItems = function (sent) {
            $scope.AttendanceData.startYearMonth = sent.startYearMonth;

            if ($scope.AttendanceData.startYearMonth > 1) {
                var sortMonth = 0, length = sent.Data.length;
                var newDataSet = new Array(length * 10);
                sent.Data.filter(function (row) {
                    sortMonth = row[0].month - $scope.AttendanceData.startYearMonth;
                    if (sortMonth < 0) {
                        sortMonth = length + row[0].month;
                        row.filter(function (data) {
                            if (data.columnNumber === 0) {
                                data.value = '(' + String(getYear($scope.currentYear + 1)) + ') ' + data.value;
                            }
                        });
                    } else {
                        row.filter(function (data) {
                            if (data.columnNumber === 0) {
                                data.value = '(' + String(getYear($scope.currentYear)) + ') ' + data.value;
                            }
                        });
                    }

                    newDataSet[sortMonth] = row;
                });

                sent.Data = newDataSet.filter(function (item) {
                    return item !== null && item !== 'undefined';
                });
            }
        }

        $scope.WSPostModel = $rootScope.getWSPostModel();

        $scope.AttendanceData = {
            Titles: new Array(),
            Data: new Array(),
            startYearMonth: 0
        }
        $scope.pageTitle = null;
        $scope.pageSubTitle = null;
        $scope.currentYear = $scope.WSPostModel.selectedDate.getFullYear();

        $scope.$controllerState = $controllerState;

        $scope.changeYear = function () {
            $scope.WSPostModel.selectedDate = new Date($scope.currentYear, 3, 3);
            PayrollServise.GetAttendanceFigures($scope.WSPostModel)
                .then(function successCallback(res) {
                    $scope.AttendanceData = {
                        Titles: new Array(),
                        Data: new Array(),
                    };

                    switch ($controllerState) {
                        case 'Vacation':
                            setTitels(res.status);
                            if (res.status === 406) {
                                return;
                            }
                            $scope.reSortDataItems(res.data[0]);

                            $scope.AttendanceData.Titles = res.data[0].Titles.sort(function (a, b) { return (a.number > b.number); });
                            replaceInItem($scope.AttendanceData.Titles, 'title', 'חופש');
                            replaceInItem($scope.AttendanceData.Titles, 'title', 'זיכוי', 'זיכוי חודשי');
                            replaceInItem($scope.AttendanceData.Titles, 'title', 'ניצול', 'ניצול חודשי');

                            res.data[0].Data.filter(function (row) {
                                $scope.AttendanceData.Data.push(row.sort(function (a, b) { return (a.columnNumber > b.columnNumber); }));
                            });
                            break;
                        case 'SickLeave':
                            setTitels(res.status);
                            if (res.status === 406) {
                                return;
                            }
                            $scope.reSortDataItems(res.data[1]);

                            $scope.AttendanceData.Titles = res.data[1].Titles.sort(function (a, b) { return (a.number > b.number); });
                            replaceInItem($scope.AttendanceData.Titles, 'title', 'ניצול', 'ניצול חודשי');

                            res.data[1].Data.filter(function (row) {
                                $scope.AttendanceData.Data.push(row.sort(function (a, b) { return (a.columnNumber > b.columnNumber); }));
                            });
                            break;
                        case 'AdvancedStudy':
                            setTitels(res.status);
                            if (res.status === 406) {
                                return;
                            }
                            $scope.reSortDataItems(res.data[2]);

                            $scope.AttendanceData.Titles = res.data[2].Titles.sort(function (a, b) { return (a.number > b.number); });
                            res.data[2].Data.filter(function (row) {
                                $scope.AttendanceData.Data.push(row.sort(function (a, b) { return (a.columnNumber > b.columnNumber); }));
                            });
                            break;

                        default:
                            $state.go('Home');
                            break;
                    }

                }, function errorCallback(res) {
                    //console.log('error', res);
                    toastrService.error('', 'תקלת מערכת!');
                });

        }

        switch ($controllerState) {
            case 'Vacation':
                setTitels();
                if (AttendanceData.status === 406 || typeof AttendanceData.data === 'undefined' || AttendanceData.data.length < 1) {
                    return;
                }
                $scope.reSortDataItems(AttendanceData.data[0]);

                $scope.AttendanceData.Titles = AttendanceData.data[0].Titles.sort(function (a, b) { return (a.number > b.number); });
                replaceInItem($scope.AttendanceData.Titles, 'title', 'חופש');
                replaceInItem($scope.AttendanceData.Titles, 'title', 'זיכוי', 'זיכוי חודשי');
                replaceInItem($scope.AttendanceData.Titles, 'title', 'ניצול', 'ניצול חודשי');

                AttendanceData.data[0].Data.filter(function (row) {
                    $scope.AttendanceData.Data.push(row.sort(function (a, b) { return (a.columnNumber > b.columnNumber); }));
                });

                break;
            case 'SickLeave':
                setTitels();
                if (AttendanceData.status === 406 || typeof AttendanceData.data === 'undefined' || AttendanceData.data.length < 1) {
                    return;
                }
                $scope.reSortDataItems(AttendanceData.data[1]);

                $scope.AttendanceData.Titles = AttendanceData.data[1].Titles.sort(function (a, b) { return (a.number > b.number); });
                replaceInItem($scope.AttendanceData.Titles, 'title', 'ניצול', 'ניצול חודשי');

                AttendanceData.data[1].Data.filter(function (row) {
                    $scope.AttendanceData.Data.push(row.sort(function (a, b) { return (a.columnNumber > b.columnNumber); }));
                });

                break;
            case 'AdvancedStudy':
                setTitels(AttendanceData.status);
                if (AttendanceData.status === 406 || typeof AttendanceData.data === 'undefined' || AttendanceData.data.length < 1) {
                    return;
                }
                $scope.reSortDataItems(AttendanceData.data[2]);

                $scope.AttendanceData.Titles = AttendanceData.data[2].Titles.sort(function (a, b) { return (a.number > b.number); });
                replaceInItem($scope.AttendanceData.Titles, 'title', 'השתלמות');

                AttendanceData.data[2].Data.filter(function (row) {
                    $scope.AttendanceData.Data.push(row.sort(function (a, b) { return (a.columnNumber > b.columnNumber); }));
                });

                break;
            case 'AttendanceSheet':
                ///TODO -> OPEN AttendanceSheet FROM OUTER SITE AND GO TO HOME PAGE!

                break;

            default:
                $state.go('Home');
                break;
        }

        $scope.tableType = $scope.AttendanceData.Data[0].length < 4 ? 'short' : 'full';
    }
})();
