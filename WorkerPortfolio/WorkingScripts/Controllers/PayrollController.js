﻿(function () {    

    angular.module('WorkerPortfolioApp').controller('PayrollController', PayrollController);

    PayrollController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$location', '$interval', '$window', '$sce', 'PayrollServise', 'toastrService', '$controllerState', 'PayrollData', '$timeout'];

    function PayrollController($scope, $rootScope, $state, $stateParams, $location, $interval, $window, $sce, PayrollServise, toastrService, $controllerState, PayrollData, $timeout) {

        $scope.isMobile = ($window.innerWidth < 801);
        ///local functions
        var parseMonth = function (date) {
            function pad(v) {return v < 10? '0'+v : v }
            if (!(date instanceof Date) || date == 'Invalid Date')
                date = new Date();
            return [pad(date.getMonth() + 1), date.getFullYear()].join('/');
        }
        var b64toBlob = function (b64Data, contentType) {
            contentType = contentType || '';
            var sliceSize = 512;
            b64Data = b64Data.replace(/^[^,]+,/, '');
            b64Data = b64Data.replace(/\s/g, '');
            var byteCharacters = window.atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }
        var setStartMonthSelect = function (formatDate) {
            var date = $rootScope.getMonthDate();
            return formatDate? date : parseMonth(date);
        }
 
        ///local parameters
        $scope.PayrollData = PayrollData || {};
        $scope.$controllerState = $controllerState;
        $scope.credentials = typeof $rootScope.$$credentials !== 'undefined' ? $rootScope.$$credentials : $scope.lsGet('$$credentials');
        $scope.pageTitle = null;
        $scope.pageSubTitle = null;
        $scope.WSPostModel = $rootScope.getWSPostModel(true);
        $scope.WSPostModel.isMobileRequest = $scope.isMobile;

        $scope.pdfContent = '';
        $scope.$pdfContent = false;
        $scope.selectedMonth = $scope.WSPostModel.selectedDate;
        $scope.selectedMonth.setDate($scope.selectedMonth.getMonth() + 1);

        $scope.currentYear = $scope.WSPostModel.selectedDate.getFullYear();
        
        switch ($controllerState) {
            case 'Paycheck':
                $scope.pageTitle = 'תלוש שכר';
                $scope.pdfContent = '';
                $scope.$pdfContent = false;
                $scope.formMsg = '';
                $scope.pdfDataStatus = PayrollData.status;
                if (PayrollData.status === 406) {
                    $scope.formMsg = 'אין תלוש שכר לתאריך CURRENTDATE'.replace('CURRENTDATE', parseMonth($scope.WSPostModel.selectedDate));
                } else {
                    try {
                        var file = b64toBlob(PayrollData.data.fileString, 'application/pdf');
                        var fileURL = window.URL.createObjectURL(file);
                        $scope.pdfContent = $sce.trustAsResourceUrl(fileURL);
                    } catch (e) {
                        //console.log(e);
                    }

                    $scope.$pdfContent = true;
                    $scope.formMsg = 'תלוש שכר לתאריך CURRENTDATE'.replace('CURRENTDATE', parseMonth($scope.WSPostModel.selectedDate));
                    $scope.fileimageblob = PayrollData.data.fileimageblob;
                }
                
                $timeout(function () {

                    $('#Month').MonthPicker({
                        MaxMonth: 0, IsRTL: true, UseInputMask: true, ShowOn: 'both', buttonText: 'בחר חודש',
                        OnAfterChooseMonth: function (selectedDate) {
                            // Do something with selected JavaScript date.
                            selectedDate.setDate(15);
                            $scope.WSPostModel.selectedDate = selectedDate;
                            $scope.showPaycheckPDF();
                        }
                    });

                }, 55);

                break;
            case 'PayrollBySymbol':
            case 'GeneralWagesData':
                $scope.pageTitle = $controllerState === 'PayrollBySymbol' ? 'פירוט נתוני שכר נבחרים בהתפלגות שנתית' : 'פירוט נתוני שכר כלליים בהתפלגות שנתית';
                $scope.pageSubTitle = $controllerState === 'PayrollBySymbol' ? "המערכת מקבצת את התשלומים / ניכויים הרלוונטיים (ההלוואות, שעות נוספות וכו'..). " : null;
                ///sort the lists
                $scope.PayrollData = {
                        Titles: [],
                        Data:[],
                    };
                $scope.PayrollData.Titles = [];
                $.map(PayrollData.data.Titles, function (item) {
                    $scope.PayrollData.Titles[item.number] = item;
                });

                PayrollData.data.Data.filter(function (row) {
                    var _row = [];
                    $.map(row, function (item) {
                        _row[item.columnNumber] = item;
                    });
                    $scope.PayrollData.Data.push(_row);
                });
                break;
            case 'TrainingAndProvidentFunds':
                $scope.pageTitle = 'פירוט נתוני שכר נבחרים בהתפלגות שנתית - גמל והשתלמות';
                $scope.pageSubTitle = 'המערכת מציגה את נתוני הניכויים והפרשות לכלל קופות הגמל שהתבצעו בשנה הנתונה';
                
                $scope.PayrollData = PayrollData.data;                
                break;
            case '106Form':
                if ($scope.credentials.currentTerminationDate === null)
                    $scope.currentYear--;
                $scope.pageTitle = 'טופס 106';
                $scope.pdfContent = '';
                $scope.$pdfContent = false;
                $scope.formMsg = '';
                $scope.pdfDataStatus = PayrollData.status;
                $scope.WSPostModel.selectedDate = new Date($scope.currentYear, 11, 11);

                if (PayrollData.status === 406) {
                    $scope.formMsg = 'אין טופס 106 לשנת YEAR.'.replace('YEAR', $scope.currentYear);
                } else {
                    try {
                        var file = b64toBlob(PayrollData.data.fileString, 'application/pdf');
                        var fileURL = URL.createObjectURL(file);
                        $scope.pdfContent = $sce.trustAsResourceUrl(fileURL);
                    } catch (e) {
                        //console.log(e);
                    }

                    $scope.$pdfContent = true;
                    $scope.formMsg = 'טופס 106 לשנת YEAR.'.replace('YEAR', $scope.currentYear);
                    $scope.fileimageblob = PayrollData.data.fileimageblob;
                }
                break;

            default:
                $state.go('Home');
                break;
        }

        $scope.changeYear = function () {
            $scope.WSPostModel.selectedDate = new Date($scope.currentYear, 3, 3);
            switch ($scope.$controllerState) {
                case 'GeneralWagesData':
                    PayrollServise.GeneralPayrollData($scope.WSPostModel)
                        .then(function successCallback(res) {
                            $scope.PayrollData = {
                                Titles: [],
                                Data: [],
                            };
                            $scope.PayrollData.Titles = [];
                            $.map(res.data.Titles, function (item) {
                                $scope.PayrollData.Titles[item.number] = item;
                            });

                            res.data.Data.filter(function (row) {
                                var _row = [];
                                $.map(row, function (item) {
                                    _row[item.columnNumber] = item;
                                });
                                $scope.PayrollData.Data.push(_row);
                            });
                        }, function errorCallback(res) {
                            //console.log('error', res);
                            toastrService.error('', 'תקלת מערכת!');
                        });
                    break;
                case 'PayrollBySymbol':
                    PayrollServise.PayrollBySymbol($scope.WSPostModel)
                        .then(function successCallback(res) {
                            $scope.PayrollData = {
                                Titles: [],
                                Data: [],
                            };
                            $scope.PayrollData.Titles = [];
                            $.map(res.data.Titles, function (item) {
                                $scope.PayrollData.Titles[item.number] = item;
                            });

                            res.data.Data.filter(function (row) {
                                var _row = [];
                                $.map(row, function (item) {
                                    _row[item.columnNumber] = item;
                                });
                                $scope.PayrollData.Data.push(_row);
                            });
                        }, function errorCallback(res) {
                            //console.log('error', res);
                            toastrService.error('', 'תקלת מערכת!');
                        });
                    break;
                case 'TrainingAndProvidentFunds':
                    PayrollServise.GetFundsTable($scope.WSPostModel)
                        .then(function successCallback(res) {
                            $scope.PayrollData = res.data;
                        }, function errorCallback(res) {
                            //console.log('error', res);
                            toastrService.error('', 'תקלת מערכת!');
                        });
                    break;
                case '106Form':
                    $scope.show106PDF();
                    break;
                case 'Paycheck':
                    $scope.showPaycheckPDF();
                    break;
            }            
        }
        
        $scope.show106PDF = function () {
            $scope.fileimageblob = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXY3j//j0ABZ4CzkTR0PgAAAAASUVORK5CYII=';
            $scope.WSPostModel.selectedDate = new Date($scope.currentYear, 11, 11);
            
            PayrollServise.Get106Form($scope.WSPostModel)
                .then(function successCallback(res) {
                    $scope.pdfContent = '';
                    $scope.$pdfContent = false;
                    $scope.formMsg = '';
                    $scope.pdfDataStatus = res.status;
                    if (res.status === 406) {
                        $scope.formMsg = 'אין טופס 106 לשנת YEAR.'.replace('YEAR', $scope.currentYear);
                    } else {
                        $scope.formMsg = 'טופס 106 לשנת YEAR.'.replace('YEAR', $scope.currentYear);
                        
                        try {
                            var file = b64toBlob(res.data.fileString, 'application/pdf');
                            var fileURL = URL.createObjectURL(file);
                            $scope.pdfContent = $sce.trustAsResourceUrl(fileURL);
                        } catch (e) {
                            //console.log(e);
                        }

                        $scope.$pdfContent = true;
                        $scope.fileimageblob = res.data.fileimageblob;
                        $scope.publish('PdfContect.Changed', { pdfControllerState: $controllerState, pdfDate: $scope.WSPostModel.selectedDate });
                    }
                    
                }, function errorCallback(res) {
                    //console.log('error', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.showPaycheckPDF = function () {
            $scope.fileimageblob = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXY3j//j0ABZ4CzkTR0PgAAAAASUVORK5CYII=';

            PayrollServise.GetPaycheck($scope.WSPostModel)
                .then(function successCallback(res) {
                    $scope.pdfContent = '';
                    $scope.$pdfContent = false;
                    $scope.formMsg = '';
                    $scope.pdfDataStatus = res.status;
                    if (res.status === 406) {
                        $scope.formMsg = 'אין תלוש שכר לתאריך CURRENTDATE'.replace('CURRENTDATE', parseMonth($scope.WSPostModel.selectedDate));
                    } else {
                        try {
                            var file = b64toBlob(res.data.fileString, 'application/pdf');
                            var fileURL = URL.createObjectURL(file);
                            $scope.pdfContent = $sce.trustAsResourceUrl(fileURL);
                        } catch (e) {
                            //console.log(e);
                        }
                        $scope.fileimageblob = res.data.fileimageblob;

                        $scope.$pdfContent = true;
                        $scope.formMsg = 'תלוש שכר לתאריך CURRENTDATE'.replace('CURRENTDATE', parseMonth($scope.WSPostModel.selectedDate));                        
                        $scope.publish('PdfContect.Changed', { pdfControllerState: $controllerState, pdfDate: $scope.WSPostModel.selectedDate });
                    }
                }, function errorCallback(res) {
                    //console.log('error', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        
    }
})();