﻿(function () {
    angular.module('WorkerPortfolioApp').controller('IncomeController', IncomeController);

    IncomeController.$inject = ['$scope', '$rootScope', '$state', 'TofesDataService', 'TofesModel', 'toastrService', 'Search'];

    function IncomeController($scope, $rootScope, $state, TofesDataService, TofesModel, toastrService, Search) {
        ///private variables
        ///get radio buttons data
        $scope.radionStates = TofesDataService.GetRadiosStates();
        $scope.tofesModel = TofesModel;
        ///update from saved data if have
        TofesDataService.GetExistingData($scope.tofesModel, '$$tofesModel', true);
        $scope.tofesModel.thisIncome.startDate = TofesDataService.GetStartWorkInYearDate($scope.tofesModel.employee.startWorkTime, $scope.tofesModel.year);
        
        ///go-to next/prev form stap, published from Header directive
        $scope.subscribe('goto.step', function (goTo, direction) {
            ///check income selection validity
            $scope.checkedValidity();
            $scope.otherIncomesValidity();
            ///form is invalid
            if ($scope.partForm.$invalid && direction) {
                $scope.ErrorFocus();
                return void [0];
            }
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            ///go to the step
            $state.go(goTo);
        });

        ///main submit form function
        $scope.nextForm = function () {
            ///check income selection validity
            $scope.checkedValidity();
            $scope.otherIncomesValidity();
            ///form is invalid
            if ($scope.partForm.$invalid) {
                $scope.ErrorFocus();
                return void [0];
            }

            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);

            $scope.publish('$$states', 'current');
            return $scope.goToStep.isempty() ? (void [0]) : $state.go($scope.goToStep);
        }

        $scope.checkedValidity = function () {
            $scope.partForm.incomeMultiple.$setValidity('custom', true);
            $scope.partForm.incomeMultiple.$setValidity('length', true);
            $scope.incomeMultiple = $.map($scope.tofesModel.thisIncome, function (value, item) {
                if (['monthSalary', 'anotherSalary', 'partialSalary', 'daySalary'].indexOf(item) > -1 && value === true) return item;
            });
            
            if($scope.incomeMultiple.length < 1)
                $scope.partForm.incomeMultiple.$setValidity('length', false);
            if ($scope.incomeMultiple.length > 1)
                $scope.partForm.incomeMultiple.$setValidity('custom', false);
        }

        $scope.otherIncomesValidity = function () {
            $scope.partForm.hidden_otherIncomesValidity.$setValidity('custom', true);
            if ($scope.tofesModel.otherIncomes.hasIncomes) {
                var otherIncomes = $.map($scope.tofesModel.otherIncomes, function (value, item) {
                    if (['monthSalary', 'anotherSalary', 'partialSalary', 'daySalary', 'allowance', 'stipend', 'anotherSource'].indexOf(item) > -1 && value === true) return item;
                });
                if(otherIncomes.length <= 0)
                    $scope.partForm.hidden_otherIncomesValidity.$setValidity('custom', false);
            }
        }

        $scope.ErrorFocus = function () {
            if (typeof $scope.partForm.$error.required == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
            if (typeof $scope.partForm.$error.custom == 'object') {
                $scope.partForm.$error.custom[0].$$element.focus();
                return;
            }
        }
    }
})();
