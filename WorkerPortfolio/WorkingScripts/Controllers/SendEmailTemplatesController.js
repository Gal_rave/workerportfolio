﻿(function () {
    angular.module('WorkerPortfolioApp').controller('SendEmailTemplatesController', SendEmailTemplatesController);

    SendEmailTemplatesController.$inject = ['$scope', '$rootScope', '$state', 'toastrService', 'Statistics', 'ngDialog', 'ValidAdminUser', 'Populations', 'Groups'];

    function SendEmailTemplatesController($scope, $rootScope, $state, toastrService, Statistics, ngDialog, ValidAdminUser, Populations, Groups) {


        /**********************************************************************************************************/
        //window.$scope = $scope;
        //window.$state = $state;
        /**********************************************************************************************************/


        if (ValidAdminUser.data !== true || ValidAdminUser.status !== 200) {
            $state.go('Home');
            toastrService.info('', 'הרשאה לא תקינה!', { onHidden: function () { $state.go('Home'); } });
            return void [0];
        }

        $scope.WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SYSTEMSEMAILROLE')

        $scope.groups = new Array();
        $scope.UsersList = new Array();
        $scope.toMailUsers = new Array();
        $scope.EmailTemplateModel = {
            Editing: false,
            ID: $state.params.templateId,
            municipalityId: $scope.WSPostModel.currentMunicipalityId,
            createdDate: new Date(),
            updateDate: new Date(),
            updateUserId: null,
            template: null,
            subject: null,
            name: null,
            fromEmail: null,
            municipality: null,
            user: null,
            type: 1,
            Sendind: false,
            pg: [],

            ShowPG: false
        };
        $scope.UserSearch = {
            idNumber: $scope.WSPostModel.idNumber,
            confirmationToken: $scope.WSPostModel.confirmationToken,
            page: 1,
            hidden_value: false,
            $hidden_value: null,
            searchIdNumber: null,
            cell: null,
            firstName: null,
            lastName: null,
            password: null,
            newPassword: null,
            reEnterPassword: null,
            municipalities: null,
            userToken: null,
            searchMunicipalityName: null,
            searchMunicipalitiesId: null,
            requiredRole: 'SYSTEMSEMAILROLE'
        };

        ///pager
        $scope.$GroupsPager = {
            PageSize: 5,
            PageNumber: 1,
            Counter: 0,
            MaxPages: 0,
            pagerList: []
        };
        $scope.$UsersPager = {
            PageSize: 40,
            PageNumber: 1,
            Counter: 0,
            MaxPages: 0,
            pagerList: [],
            Sizes: [80, 60, 40, 20]
        };
        $scope.$SearchUsersPager = {
            PageSize: 20,
            PageNumber: 1,
            Counter: 0,
            MaxPages: 0,
            pagerList: [],
            Sizes: [60, 40, 20]
        };

        var setUsersPager = function () {
            $scope.$UsersPager.PageNumber = 1;
            $scope.$UsersPager.Counter = $scope.toMailUsers.length;
            $scope.$UsersPager.MaxPages = Math.ceil($scope.toMailUsers.length / $scope.$UsersPager.PageSize);
            var num = 1;

            $scope.$UsersPager.pagerList = [];
            for (var i = 0; i <= $scope.$UsersPager.MaxPages; i++) {
                $scope.$UsersPager.pagerList.push(1 + i);
            }
        }
        var checkExists = function (currentItems, varToCheck, distinctPattern) {
            var exists = $.grep(currentItems, function (item) {
                return item[distinctPattern] === varToCheck;
            });

            return exists === null || exists.length <= 0;
        }
        var addDistinct = function (currentItems, newItems, distinctPattern, populationId) {
            $.map(newItems, function (item) {
                item.Send = true;
                if (checkExists(currentItems, item[distinctPattern], distinctPattern)) {
                    if (typeof item.populationId === 'undefined') {
                        item.populationId = new Array();
                        item.populationId.push(populationId);
                    } else if (item.populationId.indexOf(populationId) < 0) {
                        item.populationId.push(populationId);
                    }
                    currentItems.push(item);
                } else {
                    var result = currentItems.find(function (o) {
                        return o.ID === item.ID;
                    });
                    if (result.populationId.indexOf(populationId) < 0) {
                        result.populationId.push(populationId);
                    }
                }
            });
            setUsersPager();
        }

        $scope.addAll = function () {
            var length = $scope.toMailUsers.length;
            addDistinct($scope.toMailUsers, $scope.UsersList, 'ID', -100);
            if ($scope.UsersList === null || $scope.UsersList.length <= 0) {
                toastrService.info('אין נתונים.', '');
            }
            if (length === $scope.toMailUsers.length && $scope.UsersList.length > 0) {
                toastrService.info('לא התווספו נתונים חדשים.', '');
            }
            $scope.UsersList = new Array();
        }
        $scope.addToSendList = function (user) {
            var length = $scope.toMailUsers.length;
            addDistinct($scope.toMailUsers, [user], 'ID', -100);
            
            if (length === $scope.toMailUsers.length) {
                toastrService.info('לא התווספו נתונים חדשים.', '');
            }
            var find = $scope.UsersList.find(function (o) {
                return o.ID === user.ID;
            });
            $scope.UsersList.splice($scope.UsersList.indexOf(find), 1);
        }
        
        ///check if search form is empty
        $scope.emptyForm = function () {
            if ($scope.GetUsersForm.searchIdNumber.$pristine && $scope.GetUsersForm.cell.$pristine &&
                    $scope.GetUsersForm.firstName.$pristine && $scope.GetUsersForm.searchMunicipalitiesId.$pristine) {
                $scope.UserSearch.$hidden_value = null;
                $scope.GetUsersForm.$hidden_value.$setValidity('required', false);
                return true;
            }

            if ($scope.GetUsersForm.searchIdNumber.$modelValue === null && $scope.GetUsersForm.cell.$modelValue === null &&
                    $scope.GetUsersForm.firstName.$modelValue === null && $scope.GetUsersForm.searchMunicipalitiesId.$modelValue === null &&
                    $scope.GetUsersForm.searchIdNumber.$modelValue.isempty() && $scope.GetUsersForm.cell.$modelValue.isempty() &&
                    $scope.GetUsersForm.firstName.$modelValue.isempty() && $scope.GetUsersForm.searchMunicipalitiesId.$modelValue.isempty()) {
                $scope.UserSearch.$hidden_value = null;
                $scope.GetUsersForm.$hidden_value.$setValidity('required', false);
                return true;
            }

            return false;
        }

        $scope.RemoveGroup = function (group) {
            if (group.Selected !== true)
                return void [0];

            group.Selected = false;
            $scope.toMailUsers = $.grep($scope.toMailUsers, function (item) {
                item.populationId.splice(item.populationId.indexOf(group.population_id), 1);
                return item.populationId.length > 0;
            });
        }
        $scope.SelectGroup = function (group) {
            if (group.Selected === true)
                return void [0];

            group.Selected = true;
            $scope.WSPostModel.page = group.population_id;
            $scope.WSPostModel.selectedDate = new Date();
            Statistics.CallPopulation($scope.WSPostModel)
                .then(function successCallback(res) {
                    var length = $scope.toMailUsers.length;
                    addDistinct($scope.toMailUsers, res.data, 'ID', group.population_id);
                    if (res.data === null || res.data.length <= 0) {
                        toastrService.info('אין נתונים.', '');
                    }
                    if (length === $scope.toMailUsers.length && res.data.length > 0) {
                        toastrService.info('לא התווספו נתונים חדשים.', '');
                    }

                }, function errorCallback(res) {
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.Send = function () {
            if ($scope.toMailUsers === null || $scope.toMailUsers.length < 1)
                return void [0];

            $scope.UserSearch.searchMunicipalitiesId = $.map($scope.toMailUsers.filter(function (u) {
                return u.Send;
            }), function (u) {
                return u.ID
            }).join();
            $scope.UserSearch.page = $scope.EmailTemplateModel.ID;

            Statistics.SendEmails($scope.UserSearch)
                .then(function successCallback(res) {
                    console.log('successCallback', res);

            }, function errorCallback(res) {
                console.log('errorCallback', res);
                //toastrService.error('', 'תקלת מערכת!');
            });
        }

        $scope.SearchUsers = function () {
            $scope.UserSearch.$hidden_value = true;
            $scope.GetUsersForm.$hidden_value.$setValidity('required', true);
                        
            if ($scope.emptyForm() || $scope.GetUsersForm.$invalid)
                return void [0];
            
            Statistics.GroupEmailSearchUsers($scope.UserSearch)
                .then(function successCallback(res) {
                    $scope.$SearchUsersPager.PageNumber = 1;
                    $scope.$SearchUsersPager.Counter = res.data.length;
                    $scope.$SearchUsersPager.MaxPages = Math.ceil(res.data.length / $scope.$SearchUsersPager.PageSize);
                    var num = 1;

                    $scope.$SearchUsersPager.pagerList = [];
                    for (var i = 0; i <= $scope.$SearchUsersPager.MaxPages; i++) {
                        $scope.$SearchUsersPager.pagerList.push(1 + i);
                    }

                    $scope.UsersList = res.data;

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        
        /* *** set up controller *** */
        $scope.setUpController = function (populations, groups) {
            $scope.groups = groups;
            if (populations !== null && populations.length > 1) {
                $scope.EmailTemplateModel.pg = populations;

                $scope.$GroupsPager.PageNumber = 1;

                $scope.$GroupsPager.Counter = populations.length;
                $scope.$GroupsPager.MaxPages = Math.ceil(populations.length / $scope.$GroupsPager.PageSize);
                var num = 1;

                $scope.$GroupsPager.pagerList = [];
                for (var i = 0; i <= $scope.$GroupsPager.MaxPages; i++) {
                    $scope.$GroupsPager.pagerList.push(1 + i);
                }
            } else {
                toastrService.info('אין קבוצות אוכלוסיה לרשות.', '');
            }
        };
        $scope.setUpController(Populations.data, Groups.data);
    }
})();
