﻿(function () {
    angular.module('WorkerPortfolioApp').controller('OtherIncomeAdjustmentController', OtherIncomeAdjustmentController);

    OtherIncomeAdjustmentController.$inject = ['$scope', '$rootScope', '$state', 'FileUploader', 'TofesDataService', 'TofesModel', 'toastrService', 'Search'];

    function OtherIncomeAdjustmentController($scope, $rootScope, $state, FileUploader, TofesDataService, TofesModel, toastrService, Search) {

        ///private variables
        ///get radio buttons data
        $scope.radionStates = TofesDataService.GetRadiosStates();
        $scope.tofesModel = TofesModel;
        ///update from saved data if have
        TofesDataService.GetExistingData($scope.tofesModel, '$$tofesModel', true);
        
        ///go-to next/prev form stap, published from Header directive
        $scope.subscribe('goto.step', function (goTo, direction) {
            ///check income selection validity
            $scope.otherIncomesValidity();
            
            ///form is invalid
            if ($scope.validatUploders() || $scope.partForm.$invalid && direction) {
                $scope.ErrorFocus();
                return void [0];
            }
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);

            ///go to the step
            $state.go(goTo);
        });

        ///main submit form function
        $scope.nextForm = function () {
            ///check income selection validity
            $scope.otherIncomesValidity();

            ///form is invalid
            if ($scope.partForm.$invalid) {
                $scope.ErrorFocus();
                return void [0];
            }
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);

            $scope.publish('$$states', 'current');
            return $scope.goToStep.isempty() ? (void [0]) : $state.go($scope.goToStep);
        }
        $scope.ChengeRequestRule = function () {
            if (!$scope.tofesModel.taxCoordination.request) {
                $scope.tofesModel.taxCoordination.reason = null;
            } else {
                $scope.tofesModel.taxCoordination.reason = 3;
            }
        }

        $scope.coordinationUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'coordinationFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.coordinationUploader.filters.splice(1, 1);
        $scope.coordinationUploader.filters.push($scope.queueLimitFilter);
        $scope.coordinationUploader.filters.push($scope.uniqueKeyFilter);
        $scope.coordinationUploader.filters.push($scope.sizeFilter);
        $scope.coordinationUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.coordinationUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.coordinationUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.coordinationUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.coordinationUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.coordinationUploader.UploaderValidation = $scope._UploaderValidation;
        ///END FILE UPLOADERS
        
        $scope.otherIncomesValidity = function () {
            $scope.partForm.hidden_otherIncomesValidity.$setValidity('custom', true);
            if ($scope.tofesModel.otherIncomes.hasIncomes) {
                var otherIncomes = $.map($scope.tofesModel.otherIncomes, function (value, item) {
                    if (['monthSalary', 'anotherSalary', 'partialSalary', 'daySalary', 'allowance', 'stipend', 'anotherSource'].indexOf(item) > -1 && value === true) return item;
                });
                if (otherIncomes.length <= 0)
                    $scope.partForm.hidden_otherIncomesValidity.$setValidity('custom', false);
            }
        }
        ///validate all the uploaders in the scope
        $scope.validatUploders = function () {
            for (var i in $scope) {
                if ($scope[i] instanceof FileUploader) {
                    ///SAVE UPLOADED FILES TO BE SENT
                    $rootScope.FilterFiles($scope.tofesModel.TaxAdjustmentControllerFilesList, $scope[i].queue, i);
                    $scope[i].Files = $scope.tofesModel.TaxAdjustmentControllerFilesList.filter(function (item) {
                        return item.origin === i;
                    }).length;

                    if ($scope[i].UploaderValidation()) {
                        return true;
                    }
                }
            }
            return false;
        }
        $scope.ErrorFocus = function () {
            if (typeof $scope.partForm.$error.required == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
            if (typeof $scope.partForm.$error.custom == 'object') {
                $scope.partForm.$error.custom[0].$$element.focus();
                return;
            }
        }
        
    }
})();
