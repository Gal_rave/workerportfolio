﻿(function () {
    angular.module('WorkerPortfolioApp').controller('TaxExemptionController', TaxExemptionController);

    TaxExemptionController.$inject = ['$scope', '$rootScope', '$state', 'TofesDataService', 'TofesModel', 'FileUploader', 'toastrService', 'Search', '$timeout'];

    function TaxExemptionController($scope, $rootScope, $state, TofesDataService, TofesModel, FileUploader, toastrService, Search, $timeout) {

        ///private variables
        ///get radio buttons data
        $scope.radionStates = TofesDataService.GetRadiosStates();
        $scope.tofesModel = TofesModel;
        ///update from saved data if have
        TofesDataService.GetExistingData($scope.tofesModel, '$$tofesModel', true);

        $scope.taxSectionTitle = $scope.tofesModel.FormType === 3 ? 'ז' : 'ח';
        $scope.childrenInCareTitle = $scope.tofesModel.FormType === 3 ? 'ד' : 'ג';
        $scope.infantchildrenTitle = $scope.tofesModel.FormType === 3 ? '4' : '7';
        $scope.singleParentTitle = $scope.tofesModel.FormType === 3 ? '4 ו-5' : '7 ו-8';

        $scope.Qindex = 1;
        $scope.Qlist = new Array();

        var yearPart = function (d) {
            return parseInt(d.split('/')[2]);
        }
        ///calc children
        $scope.childrenCountsObject = {
            ///מספר ילדים שנולדו בשנת המס ו/או שימלאו להם 18 שנים בשנת המס
            child18: 0,
            ///מספר ילדים שימלאו להם שנה אחת עד חמש שנים בשנת המס
            ander5: 0,
            ///מספר ילדים אחרים שטרם מלאו להם 19 שנים
            ander19: 0,
            ///מספר ילדים שנולדו בשנת המס ו/או שימלאו להם 3 שנים בשנת המס
            upto3: 0,
            ///מספר ילדים שימלאו להם שנה אחת ו/או שנתיים בשנת המס
            upto2: 0,

            ///מס' ילדים שנולדו בשנת המס
            thisYear: 0,
            ///מס' ילדים שימלאו להם 6 שנים עד 17 שנים בשנת המס
            sixTo17: 0,
            ///סך הכל ילדים תחת גיל 19
            totalAnder19: 0,
        }
        $scope.countChildrens = function () {
            var year = parseInt($scope.tofesModel.year);

            $.map($scope.tofesModel.children, function (child) {
                if (child.birthDate == null || typeof child.birthDate == 'undefined')
                    return;

                var y = parseInt(yearPart(child.birthDate));

                if ((year - y) <= 5 && (year - y) > 0)
                    $scope.childrenCountsObject.ander5++;
                if ((year - y) < 19)
                    $scope.childrenCountsObject.totalAnder19++;

                switch ($scope.tofesModel.FormType) {
                    case 1:
                        if (y === year || (year - y) === 18)
                            $scope.childrenCountsObject.child18++;
                        if ((year - y) > 5 && (year - y) < 19 && (year - y) !== 18)
                            $scope.childrenCountsObject.ander19++;
                        if ((year - y) === 3 || (year - y) === 0)
                            $scope.childrenCountsObject.upto3++;
                        if ((year - y) < 3 && (year - y) > 0)
                            $scope.childrenCountsObject.upto2++;
                        break;
                    case 2:
                    case 3:
                        if (y === year)
                            $scope.childrenCountsObject.thisYear++;
                        if ((year - y) >= 6 && (year - y) <= 17)
                            $scope.childrenCountsObject.sixTo17++;
                        if ((year - y) === 18)
                            $scope.childrenCountsObject.child18++;
                        break;
                    default:
                        if (y === year || (year - y) === 18)
                            $scope.childrenCountsObject.child18++;
                        if ((year - y) > 5 && (year - y) < 19 && (year - y) !== 18)
                            $scope.childrenCountsObject.ander19++;
                        if ((year - y) === 3 || (year - y) === 0)
                            $scope.childrenCountsObject.upto3++;
                        if ((year - y) < 3 && (year - y) > 0)
                            $scope.childrenCountsObject.upto2++;
                        break;
                }
            });
        }
        $scope.countChildrens();

        ///go-to next/prev form stap, published from Header directive
        $scope.subscribe('goto.step', function (goTo, direction) {
            ///SAVE UPLOADED FILES TO BE SENT
            $scope.PreValidatUploders();
            if ($scope.validatUploders() || $scope.partForm.$invalid && direction) {
                $scope.ErrorFocus();
                return void [0];
            }
                        
            ///remove autocomplete options
            if (typeof $scope.tofesModel.taxExemption.settlementCredits.settlement === 'object' && $scope.tofesModel.taxExemption.settlementCredits.settlement != null) {
                $scope.tofesModel.taxExemption.settlementCredits.settlementID = $scope.tofesModel.taxExemption.settlementCredits.settlement.originalObject.ID;
                $scope.tofesModel.taxExemption.settlementCredits.settlement = $scope.tofesModel.taxExemption.settlementCredits.settlement.originalObject.Name;
            }

            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            ///go to the step
            $state.go(goTo);
        });

        ///main submit form function
        $scope.nextForm = function (reSend) {
            ///SAVE UPLOADED FILES TO BE SENT
            $scope.PreValidatUploders();
            ///form is invalid
            if ($scope.validatUploders() || $scope.partForm.$invalid) {
                $scope.ErrorFocus();
                return void [0];
            }

            ///remove autocomplete options
            if ($scope.tofesModel.taxExemption.settlementCredits.settlement !== null && typeof $scope.tofesModel.taxExemption.settlementCredits.settlement === 'object') {
                $scope.tofesModel.taxExemption.settlementCredits.settlement = $scope.tofesModel.taxExemption.settlementCredits.settlement.originalObject.Name;
            }

            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);

            $scope.publish('$$states', 'current');
            return $scope.goToStep.isempty() ? (void [0]) : $state.go($scope.goToStep);
        }
        ///validate all the uploaders in the scope
        $scope.PreValidatUploders = function () {
            for (var i in $scope) {
                if ($scope[i] instanceof FileUploader) {
                    ///SAVE UPLOADED FILES TO BE SENT
                    $rootScope.FilterFiles($scope.tofesModel.TaxExemptionControllerFilesList, $scope[i].queue, i);
                    $scope[i].Files = $scope.tofesModel.TaxExemptionControllerFilesList.filter(function (item) {
                        return item.origin === i;
                    }).length;
                }
            }
        }

        $scope.validatUploders = function () {
            for (var i in $scope) {
                if ($scope[i] instanceof FileUploader) {
                    if ($scope[i].UploaderValidation()) {
                        return true;
                    }
                }
            }
            return false;
        }

        $scope.ErrorFocus = function () {
            if (typeof $scope.partForm.$error.required == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
            else if (typeof $scope.partForm.$error.custom == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
        }

        ///FILE UPLOADERS
        $scope.blindDisabledUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'blindDisabledFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.blindDisabledUploader.filters.splice(1, 1);
        $scope.blindDisabledUploader.filters.push($scope.queueLimitFilter);
        $scope.blindDisabledUploader.filters.push($scope.uniqueKeyFilter);
        $scope.blindDisabledUploader.filters.push($scope.sizeFilter);
        $scope.blindDisabledUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.blindDisabledUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.blindDisabledUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.blindDisabledUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.blindDisabledUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.blindDisabledUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.blindDisabledUploader.Files = 0;

        $scope.isaUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'isaFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.isaUploader.filters.splice(1, 1);
        $scope.isaUploader.filters.push($scope.queueLimitFilter);
        $scope.isaUploader.filters.push($scope.uniqueKeyFilter);
        $scope.isaUploader.filters.push($scope.sizeFilter);
        $scope.isaUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.isaUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.isaUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.isaUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.isaUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.isaUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.isaUploader.Files = 0;

        $scope.certificateUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'immigrantCertificateFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.certificateUploader.filters.splice(1, 1);
        $scope.certificateUploader.filters.push($scope.queueLimitFilter);
        $scope.certificateUploader.filters.push($scope.uniqueKeyFilter);
        $scope.certificateUploader.filters.push($scope.sizeFilter);
        $scope.certificateUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.certificateUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.certificateUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.certificateUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.certificateUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.certificateUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.certificateUploader.Files = 0;

        $scope.returningUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'returningResidentFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.returningUploader.filters.splice(1, 1);
        $scope.returningUploader.filters.push($scope.queueLimitFilter);
        $scope.returningUploader.filters.push($scope.uniqueKeyFilter);
        $scope.returningUploader.filters.push($scope.sizeFilter);
        $scope.returningUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.returningUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.returningUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.returningUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.returningUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.returningUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.returningUploader.Files = 0;

        $scope.spouseNoIncomeUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'spouseNoIncomeFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.spouseNoIncomeUploader.filters.splice(1, 1);
        $scope.spouseNoIncomeUploader.filters.push($scope.queueLimitFilter);
        $scope.spouseNoIncomeUploader.filters.push($scope.uniqueKeyFilter);
        $scope.spouseNoIncomeUploader.filters.push($scope.sizeFilter);
        $scope.spouseNoIncomeUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.spouseNoIncomeUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.spouseNoIncomeUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.spouseNoIncomeUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.spouseNoIncomeUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.spouseNoIncomeUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.spouseNoIncomeUploader.Files = 0;

        $scope.alimonyUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'alimonyParticipatesFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.alimonyUploader.filters.splice(1, 1);
        $scope.alimonyUploader.filters.push($scope.queueLimitFilter);
        $scope.alimonyUploader.filters.push($scope.uniqueKeyFilter);
        $scope.alimonyUploader.filters.push($scope.sizeFilter);
        $scope.alimonyUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.alimonyUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.alimonyUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.alimonyUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.alimonyUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.alimonyUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.alimonyUploader.Files = 0;

        $scope.exAlimonyUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'exAlimonyFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.exAlimonyUploader.filters.splice(1, 1);
        $scope.exAlimonyUploader.filters.push($scope.queueLimitFilter);
        $scope.exAlimonyUploader.filters.push($scope.uniqueKeyFilter);
        $scope.exAlimonyUploader.filters.push($scope.sizeFilter);
        $scope.exAlimonyUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.exAlimonyUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.exAlimonyUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.exAlimonyUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.exAlimonyUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.exAlimonyUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.exAlimonyUploader.Files = 0;

        $scope.incompetentChildUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'incompetentChildFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.incompetentChildUploader.filters.splice(1, 1);
        $scope.incompetentChildUploader.filters.push($scope.queueLimitFilter);
        $scope.incompetentChildUploader.filters.push($scope.uniqueKeyFilter);
        $scope.incompetentChildUploader.filters.push($scope.sizeFilter);
        $scope.incompetentChildUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.incompetentChildUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.incompetentChildUploader.onCompleteItem = $scope._onCompleteItem;
        
        $scope.incompetentChildUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.incompetentChildUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.incompetentChildUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.incompetentChildUploader.Files = 0;

        $scope.servicemanUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'servicemanFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.servicemanUploader.filters.splice(1, 1);
        $scope.servicemanUploader.filters.push($scope.queueLimitFilter);
        $scope.servicemanUploader.filters.push($scope.uniqueKeyFilter);
        $scope.servicemanUploader.filters.push($scope.sizeFilter);
        $scope.servicemanUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.servicemanUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.servicemanUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.servicemanUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.servicemanUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.servicemanUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.servicemanUploader.Files = 0;

        var graduationUploader = $scope.graduationUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'graduationFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.graduationUploader.filters.splice(1, 1);
        $scope.graduationUploader.filters.push($scope.queueLimitFilter);
        $scope.graduationUploader.filters.push($scope.uniqueKeyFilter);
        $scope.graduationUploader.filters.push($scope.sizeFilter);
        $scope.graduationUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.graduationUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.graduationUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.graduationUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.graduationUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.graduationUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.graduationUploader.Files = 0;
        ///END FILE UPLOADERS

        ///AUTOCOMPLETE
        $scope.searchSettlement = function (typed) {
            if (!typed || typed.length < 1)
                return void [0];
            return Search.citySearch({ query: typed });
        }
        ///END AUTOCOMPLETE
        var getD = function(){
            var d = new Date();
            d.setFullYear(d.getFullYear() - 1);
            return d;
        }
        $scope.sRdOptions = {
            maxDate: getD()
        }

        $scope.QuestionsNumbers = function (Q) {
            var _q = $scope.Qlist.filter(function (qObj) {
                return qObj !== null && qObj.ID === Q;
            });

            if (_q == null || _q.length === 0) {
                _q = { ID: parseInt(Q), Name: $scope.Qindex++ };
                $scope.Qlist.push(_q);

                $scope.Qlist = $scope.Qlist.sort(function (a, b) {
                    return a.ID > b.ID ? 1 : a.ID < b.ID ? -1 : 0
                });
                $scope.Qlist.filter(function (item, index) {
                    item.Name = (index + 1);
                });

            } else {
                _q = _q[0];
            }
            return _q.Name;
        }

        $scope.PreValidatUploders();
    }
})();
