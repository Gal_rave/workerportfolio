﻿(function () {

    angular.module('WorkerPortfolioApp').config(function (ngDialogProvider) {
        ngDialogProvider.setOpenOnePerName(true);
    });

})();