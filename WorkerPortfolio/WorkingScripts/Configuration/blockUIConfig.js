﻿(function () {
    
    angular.module('WorkerPortfolioApp').config(function (blockUIConfig) {
        // Change the default overlay message
        blockUIConfig.message = 'מעבד נתונים ...';
        // Change the default delay to 100ms before the blocking is visible
        blockUIConfig.delay = 0;
        // Apply these classes to al block-ui elements
        blockUIConfig.cssClass = 'block-ui block-ui-anim-fade block-ui-overlay-dark';
        ///set custom block template
        blockUIConfig.templateUrl = 'angular-block-ui/angular-block-ui.ng.ie.html';
        // Change the displayed message based on the http verbs being used.    
        blockUIConfig.requestFilter = function (config) {
            if (config.headers["X-Search"] == String(1) || config.XSearch)
            {
                return false;
            }
        };
    });

})();