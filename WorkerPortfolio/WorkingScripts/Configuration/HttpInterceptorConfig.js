﻿(function() {    

    angular.module('WorkerPortfolioApp').config(function ($httpProvider, $provide) {
        $provide.factory('httpInterceptor', function ($q, $rootScope) {
            return {
                'request': function (config) {
                    config.$canceller = $q.defer();
                    config.timeout = config.$canceller.promise;
                    return config;
                },
                'requestError': function (rejection) {
                    //console.log('requestError', rejection);
                    if (rejection.status < 0) return void [0];
                    return $q.reject(rejection);
                },
                'responseError': function (rejection) {
                    //console.log('responseError', rejection);
                    ///remove sensitive data from error response
                    if (rejection.data !== undefined && rejection.data.StackTrace !== undefined) rejection.data.StackTrace = {};
                    if (rejection.config !== undefined && rejection.config.data !== undefined) rejection.config.data = {};

                    ///custom rejection status
                    if (rejection.status == 406)
                        return rejection;
                    ///custom CryptographicException rejection status
                    if (rejection.status >= 511) {
                        $rootScope.publish('511_responseError', rejection.status);
                        return $q.reject(rejection);
                    }

                    if (rejection.status === 401 || (rejection.data !== undefined && rejection.data.ExceptionMessage !== undefined && rejection.data.ExceptionMessage === "UserdetailsAccessException")) {
                        ///logout
                        $rootScope.publish('CatchTransmitLogout');
                    }
                    if (rejection.status < 0) return void [0];

                    return $q.reject(rejection);
                },
                'response': function (response) {
                    //console.log('response', response);
                    // Return the response or promise.
                    return response || $q.when(response);
                }
            };
        });

        $httpProvider.interceptors.push('httpInterceptor');
    });
    
})();