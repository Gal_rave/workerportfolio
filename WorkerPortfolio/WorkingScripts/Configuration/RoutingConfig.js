﻿(function () {
    /* 
    *** 
    general site permissions
    100 - DEVELOPER
    80 - SITEADMIN
    50 - ADMIN

    custom models permissions
    43 - SYSTEMSEMAILROLE
    42 - MUNICIPALITYMENUROLE
    41 - EMAILPAYCHECKROLE
    ***
    */
    angular.module('WorkerPortfolioApp').config(['$locationProvider', '$stateProvider', '$urlRouterProvider', function ($locationProvider, $stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            /*errors*/
            .state('Error', {
                url: "/error",
                templateUrl: "../../Scripts/views/error.html",
                controller: "ErrorController",
                resolve: {
                    SiteMail: ['UserService', function (UserService) {
                        return UserService.GetSiteMail();
                    }]
                },
                data: {
                    type: null,
                    name: 'שגיאת מערכת',
                    RequiresAuthentication: false,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('UserAuthenticationError', {
                url: "/error/{AuthenticationErrorToken:[0-9A-Za-z]{1,125}}",
                templateUrl: "../../Scripts/views/error.html",
                controller: "ErrorController",
                resolve: {
                    SiteMail: ['UserService', function (UserService) {
                        return UserService.GetSiteMail();
                    }]
                },
                data: {
                    type: null,
                    name: 'שגיאת אישור משתמש',
                    RequiresAuthentication: false,
                    disabled: false,
                    immunity: 0
                }
            })
            /*end errors*/

            /*home page*/
            .state('Home', {
                url: "/",
                templateUrl: "../../Scripts/views/Home.html",
                controller: "HomeController",
                data: {
                    type: null,                         /// master type on side menu, to what article enter this item
                    name: 'ברוכים הבאים',             /// side menu title
                    RequiresAuthentication: false,     /// if nead to be loged in
                    disabled: false,                   /// if to show in side menu if have this article main type
                    immunity: 0
                }
            })
            /*end home page*/

            /*site admin*/
            .state('Admin', {
                abstract: true,
                url: '/admin',
                template: '<div ui-view/>',
                location: 0,
                faIcon: 'fa-desktop fa-lg',
                data: {
                    type: 'Admin',
                    name: 'ניהול מערכת',
                    RequiresAuthentication: true,
                    disabled: true,
                    ShowAnyway: false,
                    color: 'admin',//008482
                    immunity: 50
                }
            })
            .state('Admin.SearchUsers', {
                url: "/search",
                templateUrl: "../../Scripts/views/admin.html",
                controller: "AdminController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'ADMIN'));
                    }],
                    $user: [function () {
                        return {};
                    }],
                    $passwordSettings: [function () {
                        return {};
                    }],
                    $groups: ['Admin', function (Admin) {
                        return {};
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'חיפוש משתמשים',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 50
                }
            })
            .state('Admin.EditUser', {
                url: "/show-user/{UserID}",
                templateUrl: "../../Scripts/views/Forms/admin.show-user.html",
                controller: "AdminController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'ADMIN'));
                    }],
                    $user: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'ADMIN'),
                            UserSearch = {
                                idNumber: WSPostModel.idNumber,
                                confirmationToken: WSPostModel.confirmationToken,
                                page: $stateParams.UserID,
                                searchIdNumber: null,
                                cell: null,
                                firstName: null,
                                lastName: null,
                                requiredRole: WSPostModel.requiredRole
                            };

                        return Admin.GetUserById(UserSearch);
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }],
                    $groups: ['Admin', function (Admin) {
                        return Admin.GetGroups($rootScope.getWSPostModel(null, null, null, null, 'ADMIN'));
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'צפיה במשתמש',
                    RequiresAuthentication: true,
                    disabled: true,
                    immunity: 50
                }
            })
            .state('Admin.ResetPassword', {
                url: "/reset-password/{IdNUmber}",
                templateUrl: "../../Scripts/views/Forms/admin.reset-password.html",
                controller: "AdminController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'ADMIN'));
                    }],
                    $user: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        return {};
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }],
                    $groups: ['Admin', function (Admin) {
                        return {};
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'איפוס סיסמה',
                    RequiresAuthentication: true,
                    disabled: true,
                    immunity: 50
                }
            })
            .state('Admin.MunicipalitiesSetUp', {
                url: "/municipalities-setup",
                templateUrl: "../../Scripts/views/Forms/admin.municipalities-setup.html",
                controller: "AdminMunicipalitiesController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'));
                    }],
                    municipalities: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetMunicipalities(UserSearch);
                    }],
                    $municipality: [function () {
                        return null;
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'ניהול ארגונים',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 70
                }
            })
            .state('Admin.EditMunicipality', {
                url: "/edit-municipality/{MunicipalityId}",
                templateUrl: "../../Scripts/views/Forms/admin.show-municipality.html",
                controller: "AdminMunicipalitiesController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'));
                    }],
                    municipalities: [function () {
                        return {};
                    }],
                    $municipality: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'),
                            UserSearch = {
                                idNumber: WSPostModel.idNumber,
                                confirmationToken: WSPostModel.confirmationToken,
                                page: $stateParams.MunicipalityId,
                                searchIdNumber: null,
                                cell: null,
                                firstName: null,
                                lastName: null,
                                requiredRole: WSPostModel.requiredRole
                            };
                        return Admin.GetSingleMunicipaly(UserSearch);
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'ניהול ארגונים',
                    RequiresAuthentication: true,
                    disabled: true,
                    immunity: 70
                }
            })
            /*end site admin*/

            /*site statistics*/
            .state('Statistics', {
                abstract: true,
                url: '/statistics',
                template: '<div ui-view/>',
                data: {
                    type: 'Admin',
                    name: 'סטטיסטיקות שימוש',
                    RequiresAuthentication: true,
                    disabled: true,
                    ShowAnyway: false,
                    color: 'admin',//008482
                    immunity: 41
                }
            })
            .state('Statistics.EmailPaycheck', {
                url: "/email-paycheck",
                templateUrl: "../../Scripts/views/statistics.email-paycheck.html",
                controller: "StatisticsController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'EMAILPAYCHECKROLE'));
                    }],
                    municipalities: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'EMAILPAYCHECKROLE'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetMunicipalities(UserSearch);
                    }],
                    $statistics: [function () {
                        return new Array();
                    }],
                    $controllerState: [function () {
                        return 'EmailPaycheck';
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'תלוש במייל',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 41
                }
            })
            .state('Statistics.101Form', {
                url: "/form-101",
                templateUrl: "../../Scripts/views/statistics.email-paycheck.html",
                controller: "StatisticsController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'));
                    }],
                    municipalities: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetMunicipalities(UserSearch);
                    }],
                    $statistics: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetForm101Statistics(UserSearch);
                    }],
                    $controllerState: [function () {
                        return '101Form';
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'טופס 101',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 70
                }
            })
            .state('Statistics.LogInLog', {
                url: "/log-in-log",
                templateUrl: "../../Scripts/views/statistics.email-paycheck.html",
                controller: "StatisticsController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'));
                    }],
                    municipalities: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetMunicipalities(UserSearch);
                    }],
                    $statistics: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetLogInLogStatistics(UserSearch);
                    }],
                    $controllerState: [function () {
                        return 'LogInLog';
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'כניסות לארגון',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 70
                }
            })
            .state('Statistics.DailyUsage', {
                url: "/daily-usage",
                templateUrl: "../../Scripts/views/statistics.email-paycheck.html",
                controller: "StatisticsController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'));
                    }],
                    municipalities: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetMunicipalities(UserSearch);
                    }],
                    $statistics: [function () {
                        return new Array();
                    }],
                    $controllerState: [function () {
                        return 'DailyUsage';
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'כניסות לתאריך',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 70
                }
            })
            .state('Statistics.MunicipalityFormStatistics', {
                url: "/municipality-statistics",
                templateUrl: "../../Scripts/views/Municipality.Statistics.html",
                controller: "MunicipalityStatisticsController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'FORMSTATISTICSROLE'));
                    }],
                    municipalities: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'FORMSTATISTICSROLE'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetMunicipalities(UserSearch);
                    }],
                    $statistics: ['Statistics', '$stateParams', function (Statistics, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'FORMSTATISTICSROLE'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole,
                            searchDate: new Date()
                        };
                        return Statistics.MunicipalityStatistics(UserSearch);
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'דו`ח 101',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 44
                }
            })            
            /*end site statistics*/

            /*site Systems*/
            .state('Systems', {
                abstract: true,
                url: '/system',
                template: '<div ui-view/>',
                location: 1,
                faIcon: 'fa-desktop fa-lg',
                data: {
                    type: 'Systems',
                    name: 'כלי עבודה',
                    RequiresAuthentication: true,
                    disabled: true,
                    ShowAnyway: false,
                    color: 'admin',//008482
                    immunity: 42
                }
            })
            .state('Systems.Email-Templates', {
                url: "/email-templates",
                templateUrl: "../../Scripts/views/Email-Templates/admin.email-templates.html",
                controller: "SystemsEmailTemplatesController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'SYSTEMSEMAILROLE'));
                    }],
                    Templates: ['FileManager', '$rootScope', function (FileManager, $rootScope) {
                        return FileManager.GetEmailTemplates($rootScope.getWSPostModel(null, null, null, null, 'SYSTEMSEMAILROLE'));
                    }]
                },
                data: {
                    type: 'Systems',
                    name: 'תבניות מייל',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 43
                }
            })
            .state('Systems.Send-Email-Templates', {
                url: "/send-templates/{templateId}",
                templateUrl: "../../Scripts/views/Email-Templates/admin.send-email-templates.html",
                controller: "SendEmailTemplatesController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'SYSTEMSEMAILROLE'));
                    }],
                    Populations: ['Statistics', '$rootScope', function (Statistics, $rootScope) {
                        return Statistics.GetPopulations($rootScope.getWSPostModel(null, null, null, null, 'SYSTEMSEMAILROLE'))
                    }],
                    Groups: ['Statistics', '$rootScope', function (Statistics, $rootScope) {
                        return Statistics.GetPopulationGroups($rootScope.getWSPostModel(null, null, null, null, 'SYSTEMSEMAILROLE'))
                    }]
                },
                data: {
                    type: 'Systems',
                    name: 'משלוח מייל',
                    RequiresAuthentication: true,
                    disabled: true,
                    immunity: 43
                }
            })


            .state('Systems.Municipality-Pages', {
                url: "/municipality-pages",
                templateUrl: "../../Scripts/views/Email-Templates/admin.municipality-pages.html",
                controller: "MunicipalityPagesController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'MUNICIPALITYMENUROLE'));
                    }],
                    Templates: ['FileManager', '$rootScope', function (FileManager, $rootScope) {
                        return FileManager.GetHtmlTemplates($rootScope.getWSPostModel(null, null, null, null, 'MUNICIPALITYMENUROLE'));
                    }]
                },
                data: {
                    type: 'Systems',
                    name: 'עמודי אירגון',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 42
                }
            })
            /*end site Systems*/

            /*site user authentication*/
            .state('Authentication', {
                abstract: true,
                url: '/authentication',
                template: '<div ui-view/>',
                data: {
                    type: null,
                    name: 'אימות פרטים',
                    RequiresAuthentication: false,
                    disabled: false,
                    color: 'reddish',
                    immunity: 0
                }
            })
            .state('Authentication.PasswordReset', {
                url: "/reset-password/{Token:[0-9A-Za-z-_=]{1,225}}",
                templateUrl: "../../Scripts/views/Forms/user.authentication.reset-password.html",
                controller: "AuthenticationController",
                resolve: {
                    $controllerState: [function () {
                        return 'PasswordReset';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: null,
                    name: 'איפוס סיסמה',
                    RequiresAuthentication: false,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Authentication.ResetPasswordFromSms', {
                url: "/sms-reset-password",
                templateUrl: "../../Scripts/views/Forms/user.authentication.reset-password.html",
                controller: "AuthenticationController",
                params: {
                    idNumber: null
                },
                resolve: {
                    $controllerState: [function () {
                        return 'SMSReset';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: null,
                    name: 'איפוס סיסמה',
                    RequiresAuthentication: false,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Authentication.AuthenticatedUserLogin', {
                url: "/authenticateduserlogin/{AuthenticationSuccessToken:[0-9A-Za-z]{1,125}}",
                templateUrl: "../../Scripts/views/user.authentication.html",
                controller: "AuthenticationController",
                resolve: {
                    $controllerState: [function () {
                        return 'AuthenticatedUserLogin';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: null,
                    name: 'אישור משתמש',
                    RequiresAuthentication: false,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Authentication.LoginToProcess', {
                url: "/logintoprocess/{AuthenticationSuccessToken:[0-9A-Za-z]{1,125}}/{rashut}/{mncplity}",
                templateUrl: "../../Scripts/views/user.authentication.html",
                controller: "AuthenticationController",
                resolve: {
                    $controllerState: [function () {
                        return 'LoginToProcess';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: null,
                    name: 'אישור משתמש',
                    RequiresAuthentication: false,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Authentication.UserRegister', {
                url: "/user-register",
                templateUrl: "../../Scripts/Views/Forms/user.authentication.register.html",
                controller: "AuthenticationController",
                resolve: {
                    $controllerState: [function () {
                        return 'UserRegister';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: null,
                    name: 'הרשמה לאתר',
                    RequiresAuthentication: false,
                    disabled: true,
                    immunity: 0
                }
            })
            .state('Authentication.InitialActivation', {
                url: "/initial-activation",
                templateUrl: "../../Scripts/Views/Forms/user.initial.activation.html",
                controller: "AuthenticationController",
                resolve: {
                    $controllerState: [function () {
                        return 'InitialActivation';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: null,
                    name: 'הרשמה לאתר',
                    RequiresAuthentication: false,
                    disabled: true,
                    immunity: 0
                }
            })
            .state('Authentication.Login-Redirect', {
                url: "/login-redirect/{redirectState}/{AuthenticationSuccessToken:[0-9A-Za-z]{1,125}}/{municipalityId}",
                templateUrl: "../../Scripts/views/user.authentication.html",
                controller: "AuthenticationController",
                resolve: {
                    $controllerState: [function () {
                        return 'RedirectLogin';
                    }],
                    $passwordSettings: [function () {
                        return { data: {} };
                    }]
                },
                data: {
                    type: null,
                    name: 'אישור משתמש',
                    RequiresAuthentication: false,
                    disabled: false,
                    immunity: 0
                }
            })
            /*end site user authentication*/

            /*user details update*/
            .state('UserDetails', {
                abstract: true,
                url: '/details',
                template: '<div ui-view/>',
                location: 2,
                faIcon: 'fa-user fa-lg',
                data: {
                    type: 'UserDetails',
                    name: 'פרטי עובד',
                    RequiresAuthentication: true,
                    disabled: false,
                    ShowAnyway: true,
                    color: 'reddish',
                    immunity: 0
                }
            })
            .state('UserDetails.UserDetails', {
                url: "/user",
                templateUrl: "../../Scripts/views/user.details.html",
                controller: "UserDetailsController",
                resolve: {
                    $controllerState: [function () {
                        return 'UpdateUser';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }],
                    $personalDetails: ['Credentials', '$rootScope', function (Credentials, $rootScope) {
                        return Credentials.GetUserdetails($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'UserDetails',
                    name: 'פרטים אישיים',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('UserDetails.Partner', {
                url: "/partner",
                templateUrl: "../../Scripts/views/user.details.html",
                controller: "UserDetailsController",
                resolve: {
                    $controllerState: [function () {
                        return 'UpdatePartner';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }],
                    $personalDetails: ['Credentials', '$rootScope', function (Credentials, $rootScope) {
                        return Credentials.GetUserPartnerDetails($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'UserDetails',
                    name: 'פרטי בן/בת זוג',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('UserDetails.Education', {
                url: "/education",
                templateUrl: "../../Scripts/views/user.details.html",
                controller: "UserDetailsController",
                resolve: {
                    $controllerState: [function () {
                        return 'UpdateEducation';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }],
                    $personalDetails: ['Credentials', '$rootScope', function (Credentials, $rootScope) {
                        return Credentials.GetEducationStatus($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'UserDetails',
                    name: 'השכלה',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('UserDetails.Children', {
                url: "/children",
                templateUrl: "../../Scripts/views/user.details.html",
                controller: "UserDetailsController",
                resolve: {
                    $controllerState: [function () {
                        return 'UpdateChildren';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }],
                    $personalDetails: ['Credentials', '$rootScope', function (Credentials, $rootScope) {
                        return Credentials.GetUserChildren($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'UserDetails',
                    name: 'ילדים',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            /*end user details update*/

            /*payroll data*/
            .state('Payroll', {
                abstract: true,
                url: '/payroll',
                template: '<div ui-view/>',
                location: 3,
                faIcon: 'fa-wpforms fa-lg',
                data: {
                    type: 'Payroll',
                    name: 'שכר',
                    RequiresAuthentication: true,
                    disabled: false,
                    ShowAnyway: true,
                    color: 'orange',
                    immunity: 0
                }
            })
            .state('Payroll.Paycheck', {
                url: "/paycheck",
                templateUrl: "../../Scripts/views/payroll.section.html",
                controller: "PayrollController",
                resolve: {
                    $controllerState: [function () {
                        return 'Paycheck';
                    }],
                    PayrollData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.GetPaycheck($rootScope.getWSPostModel(true, null, null, true));
                    }]
                },
                data: {
                    type: 'Payroll',
                    name: 'תלוש שכר',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Payroll.106Form', {
                url: "/form-106",
                templateUrl: "../../Scripts/views/payroll.section.html",
                controller: "PayrollController",
                resolve: {
                    $controllerState: [function () {
                        return '106Form';
                    }],
                    PayrollData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.Get106Form($rootScope.getWSPostModel(true, true, null, true));
                    }]
                },
                data: {
                    type: 'Payroll',
                    name: 'טופס 106',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Payroll.GeneralWagesData', {
                url: "/wages-data",
                templateUrl: "../../Scripts/views/payroll.section.html",
                controller: "PayrollController",
                resolve: {
                    $controllerState: [function () {
                        return 'GeneralWagesData';
                    }],
                    PayrollData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.GeneralPayrollData($rootScope.getWSPostModel(true));
                    }]
                },
                data: {
                    type: 'Payroll',
                    name: 'נתוני שכר כללי',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Payroll.TrainingAndProvidentFunds', {
                url: "/training-and-provident-funds",
                templateUrl: "../../Scripts/views/payroll.section.html",
                controller: "PayrollController",
                resolve: {
                    $controllerState: [function () {
                        return 'TrainingAndProvidentFunds';
                    }],
                    PayrollData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.GetFundsTable($rootScope.getWSPostModel(true));
                    }]
                },
                data: {
                    type: 'Payroll',
                    name: 'ריכוז הפרשות לגמל והשתלמות',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Payroll.PayrollBySymbol', {
                url: "/payroll-by-symbol",
                templateUrl: "../../Scripts/views/payroll.section.html",
                controller: "PayrollController",
                resolve: {
                    $controllerState: [function () {
                        return 'PayrollBySymbol';
                    }],
                    PayrollData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.PayrollBySymbol($rootScope.getWSPostModel(true));
                    }]
                },
                data: {
                    type: 'Payroll',
                    name: 'פירוט תשלומים וניכויים',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            /*end payroll data*/

            /*attendance data*/
            .state('Attendance', {
                abstract: true,
                url: '/attendance',
                template: '<div ui-view/>',
                location: 4,
                faIcon: 'fa-pencil fa-lg',
                data: {
                    type: 'Attendance',
                    name: 'נוכחות',
                    RequiresAuthentication: true,
                    disabled: false,
                    ShowAnyway: true,
                    color: 'green',
                    immunity: 0
                }
            })
            .state('Attendance.Vacation', {
                url: "/vacation",
                templateUrl: "../../Scripts/views/attendance.section.html",
                controller: "AttendanceController",
                resolve: {
                    $controllerState: [function () {
                        return 'Vacation';
                    }],
                    AttendanceData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.GetAttendanceFigures($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'Attendance',
                    name: 'נתוני חופשה',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Attendance.SickLeave', {
                url: "/sick-leave",
                templateUrl: "../../Scripts/views/attendance.section.html",
                controller: "AttendanceController",
                resolve: {
                    $controllerState: [function () {
                        return 'SickLeave';
                    }],
                    AttendanceData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.GetAttendanceFigures($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'Attendance',
                    name: 'נתוני מחלה',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })

            /* **** remove **** */
            /*.state('Attendance.AdvancedStudy', {
                url: "/advanced-study",
                templateUrl: "../../Scripts/views/attendance.section.html",
                controller: "AttendanceController",
                resolve: {
                    $controllerState: [function () {
                        return 'AdvancedStudy';
                    }],
                    AttendanceData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.GetAttendanceFigures($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'Attendance',
                    name: 'נתוני השתלמות',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })*/

            .state('Attendance.AttendanceSheet', {
                url: "/",
                template: "",
                controller: function () {
                    ///empty rout used only to redirect to new tab
                },
                data: {
                    type: 'Attendance',
                    name: 'גיליון נוכחות',
                    RequiresAuthentication: true,
                    disabled: false,
                    replaceUrl: 'Home/SynerionRedirect',
                    immunity: 0
                }
            })
            /*end attendance data*/

            /*user menu*/
            .state('UserMenu', {
                abstract: true,
                url: '/usermenu',
                template: '<div ui-view/>',
                location: 5,
                faIcon: 'fa-envelope-o fa-lg',
                data: {
                    type: 'UserMenu',
                    name: 'תפריט משתמש',
                    RequiresAuthentication: false,
                    disabled: false,
                    ShowAnyway: true,
                    color: 'azure',
                    immunity: 0
                }
            })
            .state('UserMenu.UPCEStatus', {
                url: "/update-upce",
                templateUrl: "../../Scripts/views/email.paycheck.html",
                controller: "UserEmailPayCheckController",
                resolve: {
                    UPCData: ['Credentials', function (Credentials) {
                        return Credentials.GetUPCData($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: 'תלוש במייל',
                    RequiresAuthentication: true,
                    disabled: false,
                    isAllowEPC: true,
                    immunity: 0
                }
            })
            .state('UserMenu.UpdateContactDetails', {
                url: "/update-contact-details",
                templateUrl: "../../Scripts/views/update.contact-details.html",
                controller: "UpdateContactDetailsController",
                resolve: {
                    UpdateTypes: ['Credentials', function (Credentials) {
                        return Credentials.GetUserUpdateTypes($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: 'עדכון פרטי התקשרות',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })

            /*user menu => processes*/
            .state('UserMenu.Processes', {
                url: "/processes",
                templateUrl: "../../Scripts/views/processes.html",
                controller: "ProcessesController",
                resolve: {
                    ProcessesData: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getRashutProcessesData($rootScope.getWSPostModel());
                    }],
                    VacationData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.GetAttendanceFigures($rootScope.getWSPostModel());
                    }],
                    AcademicInst: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getAcademicInsts();
                    }],
                    YehidaIrgunit: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getYehidaIrgunit($rootScope.getWSPostModel());
                    }],
                    //Empty:
                    MyProcesses: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }],
                    MyRequestProcesses: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }],
                    UsersList: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: 'הגשת בקשה חדשה',
                    RequiresAuthentication: true,
                    disabled: false,
                    isAllowProcess: true,
                    immunity: 0
                }
            })
            .state('UserMenu.MyProcesses', {
                url: "/my-processes",
                templateUrl: "../../Scripts/views/my-processes.html",
                controller: "ProcessesController",
                resolve: {
                    MyProcesses: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getUserProcesses($rootScope.getWSPostModel());
                    }],
                    ProcessesData: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getRashutProcessesData($rootScope.getWSPostModel());
                    }],
                    YehidaIrgunit: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getYehidaIrgunit($rootScope.getWSPostModel());
                    }],
                    UsersList: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getUsers($rootScope.getWSPostModel());
                    }],
                    //Empty:
                    MyRequestProcesses: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }],
                    VacationData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return { data: {} };
                    }],
                    AcademicInst: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: 'בקשות שהופנו אליי',
                    RequiresAuthentication: true,
                    disabled: false,
                    isAllowProcess: true,
                    immunity: 0
                }
            })
            .state('UserMenu.MyRequestProcesses', {
                url: "/myrequestprocesses",
                templateUrl: "../../Scripts/views/my-request-processes.html",
                controller: "ProcessesController",
                resolve: {
                    MyRequestProcesses: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getUserRequestProcesses($rootScope.getWSPostModel());
                    }],
                    //Empty:
                    MyProcesses: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }],
                    ProcessesData: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }],
                    UsersList: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }],
                    VacationData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return { data: {} };
                    }],
                    AcademicInst: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }],
                    YehidaIrgunit: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: 'הבקשות שלי',
                    RequiresAuthentication: true,
                    disabled: false,
                    isAllowProcess: true,
                    immunity: 0
                }
            })
            /*user menu => processes*/

            .state('UserMenu.ResetCurrentPassword', {
                url: "/reset-current-password",
                templateUrl: "../../Scripts/views/usermenu.section.html",
                controller: "UserMenuController",
                resolve: {
                    $controllerState: [function () {
                        return 'ResetCurrentPassword';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: 'עדכון סיסמה',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('UserMenu.HumanResourcesNotify', {
                url: "/notify-human-resources",
                templateUrl: "../../Scripts/views/usermenu.section.html",
                controller: "UserMenuController",
                resolve: {
                    $controllerState: [function () {
                        return 'HumanResourcesNotify';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: "הודעה למח' שכר/משאבי אנוש",
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })

            /*keep as last item in `user menu` menu*/
            .state('UserMenu.ContactUs', {
                url: "/contact-us",
                templateUrl: "../../Scripts/views/usermenu.section.html",
                controller: "UserMenuController",
                resolve: {
                    $controllerState: [function () {
                        return 'ContactUs';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return {};
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: 'צור קשר',
                    RequiresAuthentication: false,
                    disabled: false,
                    CustomDisabled: true,
                    immunity: 0
                }
            })
            /*end user menu*/

            /*custom municipality pages*/
            .state('CustomPages', {
                url: "/pages/{PageID:[0-9]{1,10}}",
                templateUrl: "../../Scripts/views/custom-pages.html",
                controller: "CustomPagesController",
                resolve: {
                    $Page: ['FileManager', '$stateParams', function (FileManager, $stateParams) {
                        return FileManager.GetPage({
                            wsModel: $rootScope.getWSPostModel(), template: {
                                ID: $stateParams.PageID,
                                municipalityId: $rootScope.getWSPostModel().currentMunicipalityId
                            }
                        });
                    }]
                },
                data: {
                    type: 'Custom',
                    name: 'דפים',
                    RequiresAuthentication: true,
                    disabled: false,
                    isAllowMunicipalityMenu: true,
                    immunity: 0
                }
            })
            /*end custom municipality pages*/

            /*101 form*/
            .state('Form', {
                abstract: true,
                url: '/form',
                template: '<div ui-view/>',
                data: {
                    type: 'Form101',
                    name: 'טופס 101',
                    RequiresAuthentication: true,
                    disabled: true,
                    color: 'purple',
                    immunity: 0
                }
            })
            /* first step - get the start up data from the server */
            .state('Form.employer-employee', {
                url: "/employee-details",
                templateUrl: "../../Scripts/views/employer-employee.details.html",
                controller: "TofesController",
                title: 'פרטי עובד',
                resolve: {
                    TofesModel: ['TofesDataService', function (TofesDataService) {
                        return TofesDataService.GetTofesControllerData();
                    }],
                    $Tofes: ['Tofes', function (Tofes) {
                        return Tofes.GetFormData($rootScope.getForm101Year());
                    }]
                },
                data: {
                    type: 'Form101',
                    name: 'פרטי עובד',
                    RequiresAuthentication: true,
                    disabled: false,
                    formType: 0,
                    immunity: 0
                }
            })
            /* second step - family */
            .state('Form.family-details', {
                url: "/family-details",
                templateUrl: "../../Scripts/views/partner-children.details.html",
                controller: "FamilyDetailsController",
                title: 'פרטי משפחה',
                resolve: {
                    TofesModel: ['TofesDataService', function (TofesDataService) {
                        return TofesDataService.GetTofesControllerData();
                    }]
                },
                data: {
                    type: 'Form101',
                    name: 'פרטי משפחה',
                    RequiresAuthentication: true,
                    disabled: false,
                    formType: 0,
                    immunity: 0
                }
            })
            /* third step - income */
            .state('Form.employer-other', {
                url: "/income",
                templateUrl: "../../Scripts/views/employer-other.income.html",
                controller: "IncomeController",
                title: 'הכנסות',
                resolve: {
                    TofesModel: ['TofesDataService', function (TofesDataService) {
                        return TofesDataService.GetTofesControllerData();
                    }]
                },
                data: {
                    type: 'Form101',
                    name: 'הכנסות',
                    RequiresAuthentication: true,
                    disabled: false,
                    formType: 2,
                    immunity: 0
                }
            })
            /* fourth step - tax-exemption */
            .state('Form.tax-exemption', {
                url: "/tax-exemption",
                templateUrl: "../../Scripts/views/tax.exemption.html",
                controller: "TaxExemptionController",
                title: 'זיכוי מס',
                resolve: {
                    TofesModel: ['TofesDataService', function (TofesDataService) {
                        return TofesDataService.GetTofesControllerData();
                    }]
                },
                data: {
                    type: 'Form101',
                    name: 'זיכוי מס',
                    RequiresAuthentication: true,
                    disabled: false,
                    formType: 0,
                    immunity: 0
                }
            })
            /* fifth step - tax-adjustment */
            .state('Form.tax-adjustment', {
                url: "/tax-adjustment",
                templateUrl: "../../Scripts/views/tax.adjustment.html",
                controller: "TaxAdjustmentController",
                title: 'תאום מס',
                resolve: {
                    TofesModel: ['TofesDataService', function (TofesDataService) {
                        return TofesDataService.GetTofesControllerData();
                    }]
                },
                data: {
                    type: 'Form101',
                    name: 'תאום מס',
                    RequiresAuthentication: true,
                    disabled: false,
                    formType: 2,
                    immunity: 0
                }
            })
            /* alternative step for `Retired` form in place of steps 3,5 */
            .state('Form.Other-Income-Adjustment', {
                url: "/other-income-adjustment",
                templateUrl: "../../Scripts/views/Other.Income-Adjustment.html",
                controller: "OtherIncomeAdjustmentController",
                title: 'הכנסות אחרות',
                resolve: {
                    TofesModel: ['TofesDataService', function (TofesDataService) {
                        return TofesDataService.GetTofesControllerData();
                    }]
                },
                data: {
                    type: 'Form101',
                    name: 'תאום מס',
                    RequiresAuthentication: true,
                    disabled: false,
                    formType: 3,
                    immunity: 0
                }
            })
            /* sixth step(last) - attestation */
            .state('Form.attestation', {
                url: "/attestation",
                templateUrl: "../../Scripts/views/attestation.html",
                controller: "AttestationController",
                title: 'הצהרה',
                resolve: {
                    TofesModel: ['TofesDataService', function (TofesDataService) {
                        return TofesDataService.GetTofesControllerData();
                    }]
                },
                data: {
                    type: 'Form101',
                    name: 'הצהרה',
                    RequiresAuthentication: true,
                    disabled: false,
                    formType: 0,
                    immunity: 0
                }
            });
        /*end 101 form*/

        $locationProvider.hashPrefix('');

    }]);

})();