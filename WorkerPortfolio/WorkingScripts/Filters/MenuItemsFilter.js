﻿(function () {
    angular.module('WorkerPortfolioApp').filter('MenuItemsFilter', MenuItemsFilter);

    MenuItemsFilter.$inject = [];

    function MenuItemsFilter() {
        return function (obj, vars) {
            if (typeof obj === 'object' && obj.length > 0) {
                return $.grep(obj, function (mi) {
                    if (vars.type === 'Admin' || vars.type === 'Statistics')
                        return mi.data.type === vars.type && (vars.isLoggedIn || !mi.data.RequiresAuthentication) && vars.immunity >= mi.data.immunity;
                    else
                        return mi.data.type === vars.type && (vars.isLoggedIn || !mi.data.RequiresAuthentication);
                });
            }
            return obj;
        }
    }
})();