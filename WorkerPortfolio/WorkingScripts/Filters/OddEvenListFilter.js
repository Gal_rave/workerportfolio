﻿(function() {
    angular.module('WorkerPortfolioApp').filter('OddEvenListFilter', function () {
        return function (obj, vars) {
            
            if (typeof obj === 'object') {
                ///page size MUST BE AN EVEN NUMBER!!!
                if (vars.pager.PageSize % 2 !== 0)
                    vars.pager.PageSize += 1;

                var start = vars.pager.PageSize * (vars.pager.PageNumber - 1);
                var end = vars.pager.PageSize * vars.pager.PageNumber;
                var ret = obj.slice(start, end);
                if (vars.set) {
                    ret = ret.slice(0, (ret.length / 2));
                } else {
                    ret = ret.slice((ret.length / 2), ret.length);
                }
                return ret;
            }
            return obj;
        }
    });
})();