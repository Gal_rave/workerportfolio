﻿(function () {
    angular.module('WorkerPortfolioApp').filter('dateOrder', function () {
        return function (obj, properties) {
            if (angular.isArray(obj)) {
                $.map(obj, function (item) {
                    if(item[properties] != null && item[properties].length > 8){
                        var d = item[properties].split('/');
                        item.dateProp = new Date(d[2], (parseInt(d[1]) - 1), d[0]).getTime();
                    }else{
                        item.dateProp = new Date();
                    }
                });
                var list = obj.sort(function (a, b) {
                    return a.dateProp == b.dateProp ? 0 : (a.dateProp > b.dateProp) || -1;
                });
                return list;
            }
            return obj;
        }
    });

})();