﻿(function () {
    angular.module('WorkerPortfolioApp').filter('TableDataFilter', TableDataFilter);

    TableDataFilter.$inject = [];

    function TableDataFilter() {
        return function (obj, vars) {
            if (typeof obj === 'object' && obj.length > 0) {
                return obj.sort(function (a, b) { return (a[vars.sortItem] > b[vars.sortItem]); });
            }
            return obj;
        }
    }
})();