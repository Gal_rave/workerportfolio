﻿(function () {
    angular.module('WorkerPortfolioApp').directive('menu', menu);

    menu.$inject = ['$window', '$rootScope', '$timeout', '$state', 'toastrService', '$http', '$q'];

    function menu($window, $rootScope, $timeout, $state, toastrService, $http, $q) {
        var helper = {
            municipalitiesList: [14009, 99964, 32600, 2405, 99208, 4428, 26300,
                    99375, 14000, 92720, 50831, 99957, 99170, 2404, 67800, 99166, 99316,
                    28400, 67000, 99156, 51020, 83570, 39000, 52610, 69800, 90541, 36700,
                    51015, 55599, 4340, 42620, 32630, 99165, 18600, 50099, 6242, 37300],
            menu: {
                newMenuItems: new Array(),
                logInState: null,
                MenuTimer: null
            },
            doReCompile: function (scope) {
                if (scope._Width == undefined || scope._Width == null)
                    scope._Width = 0;

                if (($window.innerWidth < (scope._Width - 20) || $window.innerWidth > (scope._Width + 20))) {
                    scope._Width = $window.innerWidth
                    return true;
                } else {
                    return false;
                }
            }
        };
        var directive = {
            link: link,
            restrict: 'AE',
            templateUrl: '../../Scripts/views/ng-addons/Menu-pannel.html',
            replace: true,
            scope: true
        };
        return directive;

        function link(scope, element, attrs) {
            scope.showMobileMenu = false;
            scope.GotUserSatet = false;
            scope.$state = $state;
            scope.newMenuItems = new Array();


            ///TODO=> remove after data fix
            /* *************************************************************************************************************************************************************** */
            ///custom TEMP function to stop `Attendance` propogation
            var removeByMunicipalities = function (state) {
                return !(state.data.type === 'Attendance' && state.name !== "Attendance.AttendanceSheet" && scope.$$credentials.loggedIn
                    && helper.municipalitiesList.indexOf(scope.$$credentials.currentMunicipalityId) >= 0);
            };
            /* *************************************************************************************************************************************************************** */


            ///checks => check witch menus the user hase
            var checkIsAllow = function (data, credentials) {
                if (!credentials.loggedIn)
                    return false;
                var isAllow = $.map(data, function (value, item) {
                    if (String(item).indexOf('isAllow') === 0 && value)
                        return String(item);
                });
                if (isAllow.length === 0)
                    return true;
                return data[isAllow[0]] === credentials[isAllow[0]];
            }
            var validateUserGroups = function (credentials, data) {
                if (!credentials.loggedIn || credentials.groups === undefined || typeof credentials.groups === 'undefined' || credentials.groups.length < 1)
                    return false;

                var ret = false;
                $.map(credentials.groups, function (g) {
                    if (g.immunity === data.immunity) ret = true;
                });
                return ret;
            }
            var validateAuthentication = function (data, credentials) {
                return !data.RequiresAuthentication || credentials.isAdmin > 70
                        || (credentials.loggedIn && data.immunity < 1 && checkIsAllow(data, credentials));

            }
            var validateMenuParents = function () {
                scope.newMenuItems = $.grep(scope.newMenuItems, function (state) {
                    state.inner = $.grep(state.inner, function (s) {
                        return (validateAuthentication(s.data, scope.$$credentials) || validateUserGroups(scope.$$credentials, s.data)) && removeByMunicipalities(s);
                    });
                    return state.inner.length > 0 || state.data.ShowAnyway === true;
                });
            }

            ///start bileding the menu
            var getMenuParents = function (states) {
                return $.grep(states, function (state) {
                    return (state.abstract && state.location !== undefined && (state.data.ShowAnyway === true || scope.$$credentials.loggedIn));
                });
            }
            var getChildrensForParent = function (parent, states) {
                return $.grep(states, function (state) {
                    return !state.abstract && state.data.type === parent.data.type && !state.data.disabled;
                });
            }
            var getInnreMenuItems = function (states) {
                scope.newMenuItems.filter(function (parent) {
                    parent.open = false;
                    parent.inner = getChildrensForParent(parent, states);
                    ///CustomDisabled => remove items if loged in
                    parent.inner = $.grep(parent.inner, function (inner) {
                        return inner.data.CustomDisabled === undefined || (inner.data.CustomDisabled === true && !scope.$$credentials.loggedIn);
                    });
                });
            }

            ///check the time from bild menu to current, allow 15m` till re-bild
            scope.checkMenuTimer = function (menuTimer) {
                var time = new Date(), time = (time.getHours() * 60) + time.getMinutes();
                if (menuTimer === null || menuTimer < 1 || (time - menuTimer) > 15) {
                    helper.menu.MenuTimer = time;
                    return false;
                }
                return true;
            }
            ///calc witch menu to show baset on window
            scope.subscribe('showMobileMenu', function (state) {
                if (typeof state !== 'undefined' && typeof state === 'boolean')
                    scope.showMobileMenu = state;
                else
                    scope.showMobileMenu = !scope.showMobileMenu;
            });
            ///subscribe to menu directive - get the login state
            scope.TransmitLoginState = function () {
                if (!scope.GotUserSatet) {
                    $timeout(function () { scope.publish('TransmitLoginState', scope.GotUserSatet); }, 15);
                }
            };
            ///on chenge of rout open/close the menu
            scope.$watchCollection('$state.$current.path', function (newValue, oldValue) {
                if (newValue === oldValue || newValue === null || newValue.abstract) {
                    return void 0;
                }
                var newRout = newValue[newValue.length - 1].data;
                if (scope.$$childHead.$open && newRout.type !== 'Custom') scope.$$childHead.$open = false;
                if (!scope.$$childHead.$open && newRout.type === 'Custom') scope.$$childHead.$open = true;

                scope.newMenuItems.filter(function (state) {
                    state.open = state.data.type === newRout.type ? true : false;
                });
            }, true);
            ///subscribe to login/logout event
            scope.subscribe('VerifayLoginState', function ($credentials) {
                scope.GotUserSatet = true;
                scope.buildMenuItems().then(function (result) { }, function (error) { });
            });
            scope.subscribe('ChangeMunicipality', function (data) {
                scope.buildMenuItems(true).then(function (result) { }, function (error) { });
            });
            ///main function for $state validation
            scope.$watchCollection('$state.current', function (newValue, oldValue) {
                if (newValue === oldValue || newValue === null || newValue.abstract) {
                    return void 0;
                }
                ///check solution version
                scope.publish('$$SVDcall', true);
                scope.showMobileMenu = false;
                ///if user is logged-in or open rout do nothing
                if (validateAuthentication(newValue.data, scope.$$credentials) || validateUserGroups(scope.$$credentials, newValue.data))
                    return void [0];

                angular.forEach($http.pendingRequests, function (request) {
                    request.$canceller.resolve();
                });
                //if user not logged-in AND is restricted rout return to safety
                if (!scope.$$credentials.loggedIn) {
                    $state.go('Home');
                    toastrService.info('כניסה מותרת למשתמש רשום בלבד!', 'שטח אסור');
                } else {
                    $state.go('Home');
                    toastrService.info('', 'הרשאה לא תקינה!');
                }

            }, true);
            ///open close the menu
            scope.OpemMenuControl = function (menuItem) {
                if ($window.outerWidth < 601) {
                    $.map(scope.newMenuItems, function (state) {
                        if (state.data.type === menuItem) {
                            state.open = !state.open;
                        } else {
                            state.open = false;
                        }
                    });
                } else {
                    $.map(scope.newMenuItems, function (state) {
                        if (state.data.type === menuItem)
                            state.open = !state.open;
                    });
                }
            };

            ///muin entry poit -> start the menu from hear
            scope.buildMenuItems = function (forceUpdate) {
                return $q(function (resolve, reject) {
                    if (helper.menu.logInState === scope.$$credentials.loggedIn && helper.menu.newMenuItems.length > 0 && scope.checkMenuTimer(helper.menu.MenuTimer) &&
                        scope.newMenuItems.length === helper.menu.newMenuItems.length && forceUpdate !== true) {
                        return reject('reject');
                    }
                    var states = angular.copy($state.get());
                    scope.newMenuItems = getMenuParents(states);
                    getInnreMenuItems(states);
                    validateMenuParents();

                    helper.menu.logInState = scope.$$credentials.loggedIn;
                    helper.menu.newMenuItems = scope.newMenuItems;
                    return resolve('resolve');
                });
            }

            $(window).on("resize.doResize", _.debounce(function () {
                if (!helper.doReCompile(scope))
                    return void [0];
                scope.buildMenuItems(true).then(function (result) { }, function (error) { });

            }, 150));

            ///setup the menu AND user state
            scope.buildMenuItems().then(function (result) { }, function (error) { });

            scope.TransmitLoginState();
        }
    }

})();