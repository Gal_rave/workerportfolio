﻿(function () {    

    angular.module('WorkerPortfolioApp').directive('yearPicker', yearPicker);

    yearPicker.$inject = ['$parse'];

    function yearPicker($parse) {
        var directive = {
            link: link,
            restrict: 'E',
            templateUrl: '../../Scripts/views/ng-addons/year-picker.template.html',
            replace: true,
            require: 'ngModel',
            scope: {
                onSelect: '&',
                SlectedYear: '=slectedYear',
            }
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            scope.attrs = attrs
            scope.ngModel = ngModel;
            
            scope.shownYear = "";
            scope.open = false;
            scope.years = [];
            
            scope.openYearPicker = function () {
                scope.open = !scope.open;
                if (scope.years.length === 0) {
                    scope.years = _.range(scope.SlectedYear - 11, scope.SlectedYear + 1);
                }
            }

            scope.selectYear = function (y) {
                scope.SlectedYear = y;
                scope.ngModel.$viewValue = y;
                scope.ngModel.$commitViewValue();
                
                scope.onSelect()();

                scope.open = false;
            }
            scope.yearBefore = function () {
                var year = scope.years[scope.years.length - 2];
                scope.years = _.range(year - 11, year + 1);
            }
            scope.yearAfter = function () {
                var year = scope.years[scope.years.length - 1] + 1;
                if (year <= new Date().getFullYear()) {
                    scope.years = _.range(year - 11, year + 1);
                }
            }
            scope.clear = function () {
                scope.open = false;
            }
            scope.setYear = function (year) {
                scope.SlectedYear = parseInt(year);
                scope.shownYear = year.toString();
            }
            scope.yearChange = function (value) {
                value = (scope.SlectedYear + (value));
                if (value > new Date().getFullYear()) {
                    return void [0];
                }
                scope.selectYear(value);
            }
        }
    }

})();