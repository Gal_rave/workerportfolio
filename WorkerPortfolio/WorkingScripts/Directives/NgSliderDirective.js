﻿(function() {    

    angular.module('WorkerPortfolioApp').directive('ngslider', NgSlider);

    NgSlider.$inject = ['$window'];
    
    function NgSlider($window) {
        var directive = {
            link: link,
            restrict: 'A',
            scope:
			{
			    rule: "=",
			    classes: "@"
			},
        };
        return directive;

        function link(scope, element, attrs) {
            scope.AddindClass = typeof scope.classes !== 'undefined' && !scope.classes.isempty();
                        
            scope.$watch('rule', function (newModel, oldModel) {
                if (newModel === true) {
                    element.removeClass('slideup').addClass("slidedown").delay(750).queue(function () {
                        scope.AddindClass ? $(this).addClass(scope.classes).addClass("overflowinherit").dequeue() : $(this).addClass("overflowinherit").dequeue();
                    });
                } else {
                    if (scope.AddindClass) {
                        element.removeClass(scope.classes).delay(50).queue(function () {
                            element.removeClass("overflowinherit").delay(50).queue(function () {
                                $(this).removeClass('slidedown').addClass("slideup").dequeue();
                            });
                            $(this).dequeue();
                        });
                    } else {
                        element.removeClass('slidedown').addClass("slideup").delay(750).queue(function () {
                            $(this).removeClass("overflowinherit").dequeue();
                        });
                    }
                    
                }
            });
        }
    }

})();