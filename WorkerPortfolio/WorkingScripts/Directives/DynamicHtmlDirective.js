﻿(function () {

    angular.module('WorkerPortfolioApp').directive('dynamicHtml',['$compile', function ($compile) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                html: '<'
            },
            link: function (scope, element, attrs) {
                scope.html = scope.html.replace(/\<\/ul/igm, '</ol').replace(/\<ul/igm, '<ol');

                var content = $compile(scope.html)(scope);
                element.append(content);
            }
        };
    }]);

})();