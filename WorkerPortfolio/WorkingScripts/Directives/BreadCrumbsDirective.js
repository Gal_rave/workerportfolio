﻿(function() {
    angular.module('WorkerPortfolioApp').directive('breadCrumbs', breadCrumbs);

    breadCrumbs.$inject = ['$state'];
    
    function breadCrumbs($state) {
        var directive = {
            link: link,
            restrict: 'EA',
            templateUrl: '../../Scripts/views/ng-addons/bread-crumbs-pannel.html',
            replace: true,
            scope: true,
        };
        return directive;

        function link(scope, element, attrs) {
            scope.$state = $state;
        }
    }

})();

