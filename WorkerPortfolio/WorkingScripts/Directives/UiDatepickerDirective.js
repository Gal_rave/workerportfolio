﻿(function () {
    angular.module('WorkerPortfolioApp').directive('uiDatepicker', uiDatepicker);

    uiDatepicker.$inject = ['$timeout'];

    function uiDatepicker($timeout) {
        var directive = {
            link: link,
            restrict: 'EA',
            scope: {
                starterDate: '=ngModel',
                isNewOptions: '=newOptions',
            },
            require: "ngModel",
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            ///remove autocomplete attr
            element.attr('autocomplete', 'off');

            var updateModel = function (dateText) {
                var date = dateText.split('/');
                date = new Date([date[1], date[0], date[2]].join('/'));
                var valid = !(date == 'Invalid Date');

                scope.$apply(function () {
                    ngModel.$setValidity('custom', valid);
                    ngModel.$setViewValue(dateText);
                });
            };

            var options = {
                constrainInput: true,
                changeYear: true,
                changeMonth: true,
                isRTL: true,
                autoSize: true,

                currentText: "Now",
                buttonImage: 'calendar.gif',
                dateFormat: "dd/mm/yy",
                nextText: '',
                prevText: '',
                yearRange: "-100:+100",
                dayNames: ["ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת"],
                dayNamesMin: ["א'", "ב'", "ג'", "ד'", "ה'", "ו'", "שבת"],
                monthNames: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"],
                monthNamesShort: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"],

                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };
            
            if (scope.isNewOptions != undefined) {
                $.map(scope.isNewOptions, function (val, key) {
                    options[key] = val;
                });
                
                element.datepicker(options)
                .on("input change", function (e) {
                    updateModel(e.target.value);
                });
            }
            else {
                element.datepicker(options)
                .on("input change", function (e) {
                    updateModel(e.target.value);
                });
            }
        }
    }

})();