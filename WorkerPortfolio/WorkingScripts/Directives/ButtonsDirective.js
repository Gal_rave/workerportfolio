﻿(function () {
    angular.module('WorkerPortfolioApp').directive('buttons', Buttons);

    Buttons.$inject = ['$window', '$state', '$rootScope', '$timeout', 'TofesDataService'];

    function Buttons($window, $state, $rootScope, $timeout, TofesDataService) {
        var directive = {
            link: link,
            restrict: 'AE',
            templateUrl: '../../Scripts/views/ng-addons/buttons.component.html',
            replace: true,
            scope: true
        };
        return directive;

        function link(scope, element, attrs) {
            $timeout(function () {
                scope.states = scope.lsGet('states') || TofesDataService.GetComputedStates($state.get(), $state.current.name, parseInt(scope.$parent.tofesModel.FormType));
                
                scope.isFirstStep = false;
                scope.isLastStep = false;
                scope.showPdf = false;

                scope.states.filter(function (s) {
                    if (s.clas.isempty()) {
                        s.isCurrent = false;
                        if (s.goTo === $state.current.name) {
                            s.isCurrent = true;
                            scope.current = s.position;
                            scope.previous = s.position - 1;
                            scope.next = s.position + 1;
                            if (s.position === 0) scope.isFirstStep = true;
                            if (s.position === (scope.states.length - 1)) scope.isLastStep = true;
                        }
                    } else {
                        if (s.isCurrent) {
                            scope.previous = s.position - 1;
                            scope.next = s.position + 1;
                            if (s.position === 0) scope.isFirstStep = true;
                            if (s.position === (scope.states.length - 1)) scope.isLastStep = true;
                        }
                    }
                });

                scope.subscribe('returnToLastStep', function (hide) {
                    scope.showPdf = false;
                });

                scope.mockSubmit = function () {
                    $timeout(function () {
                        $('form[name=partForm] input').addClass('ng-submited');
                        $('form[name=partForm] button[id=submit]').trigger('click');
                    }, 10);
                }

                scope.onPrevious = function () {
                    ///send the direction off movment, prev is `false`
                    scope.publish('goto.step', scope.states[scope.previous].goTo, false);
                }

                scope.onNext = function () {
                    scope.$parent.goToStep = scope.states[scope.next].goTo;
                    scope.mockSubmit();
                }

                scope.onCompleteForm = function () {
                    scope.$parent.goToStep = '';
                    scope.mockSubmit();
                    scope.showPdf = true;
                };

                scope.hidePdf = function () {
                    scope.publish('hidePdf', true);
                    scope.showPdf = false;
                }
                scope.submitPdf = function () {
                    scope.publish('submitPdf', true);
                }

            }, 30);
        }
    }

})();