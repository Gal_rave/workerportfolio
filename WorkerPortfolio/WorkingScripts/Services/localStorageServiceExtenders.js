﻿(function () {
    

    angular.module('WorkerPortfolioApp').service('localStorageServiceExtenders', ['$window', 'localStorageService', 'FileUploader', function ($window, localStorageService, FileUploader) {

        return { Initialize: Initialize };

        function Initialize(scope) {
            ///Directly adds a value to local storage, Returns: Boolean
            scope.constructor.prototype.lsSet = scope.constructor.prototype.lsSet
             || function (key, val) {
                 return localStorageService.set(key, val);
             }
            ///Directly get a value from local storage, Returns: value from local storage
            scope.constructor.prototype.lsGet = scope.constructor.prototype.lsGet
             || function (key) {
                 return localStorageService.get(key);
             }

            ///Return array of keys for local storage, ignore keys that not owned, Returns: value from local storage
            scope.constructor.prototype.lsKeys = scope.constructor.prototype.lsKeys
             || function () {
                 return localStorageService.keys();
             }

            ///Remove an item(s) from local storage by key, Returns: Boolean
            scope.constructor.prototype.lsRemove = scope.constructor.prototype.lsRemove
             || function (key) {
                 return localStorageService.remove(key);
             }
            ///Remove all data for this app from local storage, Returns: Boolean
            ///Note: Optionally takes a regular expression string and removes matching.
            scope.constructor.prototype.lsClearAll = scope.constructor.prototype.lsClearAll
             || function (key) {
                 typeof key !== 'undefined' ? localStorageService.clearAll(key) : localStorageService.clearAll();
             }
            ///Bind $scope key to localStorageService, Returns: deregistration function for this listener.
            ///Usage: localStorageService.bind(property, value[optional], key[optional]) key: The corresponding key used in local storage 
            scope.constructor.prototype.lsBind = scope.constructor.prototype.lsBind
             || function ($scope, property, value) {
                 localStorageService.set(property, value);
                 ///set the coresponding unbind prop
                 $scope[property + '_unbind'] = localStorageService.bind($scope, property);
             }
            
        }
    }]).run(function ($rootScope, localStorageServiceExtenders) {
        localStorageServiceExtenders.Initialize($rootScope);
    });
})();
