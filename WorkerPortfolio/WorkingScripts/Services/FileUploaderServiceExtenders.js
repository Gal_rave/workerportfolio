﻿(function () {
    
    /// GENERAL FILTERS AND FUNCTION for the `FileUploader` element
    angular.module('WorkerPortfolioApp').service('FileUploaderServiceExtenders', ['toastrService', 'UserService', function (toastrService, UserService) {

        return { Initialize: Initialize };

        function Initialize(scope) {
            
            /* **** FUNCTIONS **** */
            ///file uploader validation
            scope.constructor.prototype._UploaderValidation = scope.constructor.prototype._UploaderValidation
            || function () {
                if (this.getNotUploadedItems().length > 0) {
                    toastrService.info('יש לבצע שמירה/הסרה של הקבצים.', 'שם לב! יש קבצים שלא הועלו לשרת!',
                        { clearThenShow: true, allowHtml: true, timeOut: 9999, closeButton: true });                    
                    return true;
                }
                return false;
            }
            ///function to call after file add fail
            scope.constructor.prototype._onWhenAddingFileFailed = scope.constructor.prototype._onWhenAddingFileFailed
             || function (item, filter, options) {
                 var errorText = filter.errorText, errorTitle = filter.errorTitle;
                 switch (filter.name) {
                     case 'sizeFilter':
                         this.maxFileSize = typeof this.maxFileSize === 'undefined' ? 4 : this.maxFileSize;
                         errorTitle = errorTitle.replace(/\{0}/gi, item.name);
                         errorText = errorText.replace(/\{0}/gi, this.maxFileSize);
                         break;
                     case 'uniqueKey':
                         errorTitle = errorTitle.replace(/\{0}/gi, item.name);
                         break;
                     case 'queueLimit':
                         errorTitle = errorTitle.replace(/\{0}/gi, this.queueLimit);
                         break;
                     case 'FileType':
                         errorTitle = errorTitle.replace(/\{0}/gi, item.name.split('.')[1].trim().toLowerCase());
                         errorText = errorText.replace(/\{0}/gi, this.fileTypes);
                         break;
                     default:
                         break;
                 }
                 toastrService.error(errorText, errorTitle);
             }
            ///function to call after file saved to server
            scope.constructor.prototype._onCompleteItem = scope.constructor.prototype._onCompleteItem
             || function (fileItem, response, status, headers) {
                 ///if getting user access exception do logout
                 if (status == 500 && response.ExceptionMessage === "UserdetailsAccessException") {
                     $rootScope.publish('CatchTransmitLogout');
                 }

                 if (typeof this.successItemsList === 'undefined')
                     this.successItemsList = [];
                 if (typeof this.successList === 'undefined')
                     this.successList = [];
                 if (typeof this.errorList === 'undefined')
                     this.errorList = [];
                 if (typeof response.imageId !== 'undefined')
                     fileItem.imageId = response.imageId;
                 ///add/remove files from `idNumberFiles`
                 if (response.success) {///file save success
                     this.successList.push(fileItem.file.name);
                     this.successItemsList.push(response.path);
                 } else {///file save error
                     fileItem.isUploaded = false;
                     this.errorList.push(fileItem.file.name);
                 }
             }
            ///function to call after all files saved to server
            scope.constructor.prototype._onCompleteAll = scope.constructor.prototype._onCompleteAll
             || function (fileItem, response, status, headers) {
                 var msg = '';
                 if (this.successList && this.successList.length > 0) {
                     msg = this.successList.join(', ') + (this.successList.length > 1 ? '\n\r נטענו בהצלחה\n\r' : '\n\r נטען בהצלחה\n\r');
                     toastrService.success(msg, 'הצלחה!', { timeOut: 3322 });
                 }
                 if (this.errorList && this.errorList.length > 0) {
                     msg = this.errorList.join(', ') + (this.errorList.length > 1 ? '\n\r נכשלו בטעינה\n\r' : '\n נכשל בטעינה\n\r');
                     toastrService.warning(msg, 'שגיאה!', { timeOut: 3322 });
                 }
                 this.successList = [];
                 this.errorList = [];
             }
            scope.constructor.prototype._removeFromQueue = scope.constructor.prototype._removeFromQueue
             || function (value) {
                 var self = this;
                 var index = self.getIndexOfItem(value);
                 var item = self.queue[index];
                 if (item.isUploading) item.cancel();
                 self.queue.splice(index, 1);
                 ///function to remove from server
                 if (typeof self.removeFromServer === 'undefined')
                     self.removeFromServer = scope._removeFromServer;
                 
                 if (typeof self.successItemsList !== 'undefined') {
                     self.successItemsList = $.grep(this.successItemsList, function (file) {
                         var fl = file.split('\\');
                         if (item.file.name === fl[fl.length - 1])
                            self.removeFromServer(file);
                         return item.file.name !== fl[fl.length - 1];
                     });                     
                 }

                 item._destroy();
                 self.progress = self._getTotalProgress();
             }
            scope.constructor.prototype._removeFromServer = scope.constructor.prototype._removeFromServer
             || function (item) {
                 UserService.DeleteFile({ file: item })
                    .then(function successCallback(response) {
                    }, function errorCallback(response) {
                        //console.log('full-error', response);
                        toastrService.error('הקובץ לא נמחק מהשרת', 'תקלת מערכת!');
                    });
             }
            /* **** FORM 101 FUNCTIONS **** */
            ///function to call after add file
            scope.constructor.prototype._onAfterAddingFile = scope.constructor.prototype._onAfterAddingFile
             || function (fileItem) {
                 if (this.Files !== undefined) this.Files++;
                 else this.Files = 1;

                 var WSPostModel = $rootScope.getForm101Year();

                 fileItem.formData = [
                     { filename: this.alias + '_' + (this.queue.length) + '' },
                     { userIDNumber: WSPostModel.idNumber },
                     { year: WSPostModel.selectedDate.getFullYear() },
                     { originalName: fileItem.file.name }
                 ];
             }
            /* **** FORM 101 FUNCTIONS **** */
            /* **** FUNCTIONS **** */

            /* **** FILTERS **** */
            ///limit the size of the FileUploader queue
            scope.constructor.prototype.queueLimitFilter = {
                name: 'queueLimit',
                errorTitle: 'ניתן לטעון עד {0} קבצים!',
                errorText: 'יש להסיר קובץ מהרשימה לפני הוספת קובץ חדש.',
                fn: function (item) {
                    return this.queue.length < this.queueLimit;
                }
            }
            ///check that each file is unique
            scope.constructor.prototype.uniqueKeyFilter = {
                name: 'uniqueKey',
                errorTitle: '{0}',
                errorText: 'כבר מסומן לטעינה לשרת!',
                fn: function (item) {
                    var insert = true;
                    this.queue.filter(function (FileItem) {
                        if (item.lastModified === FileItem._file.lastModified && item.size === FileItem._file.size && item.type === FileItem._file.type && item.name === FileItem._file.name)
                            insert = false;
                    });
                    return insert;
                }
            }
            ///check file size limit
            scope.constructor.prototype.sizeFilter = {
                name: 'sizeFilter',
                errorTitle: 'גודל הקובץ המצורף {0} חורג מן הגבול המותר',
                errorText: 'גודל מותר הינו עד {0}MB',
                fn: function (item) {
                    this.maxFileSize = typeof this.maxFileSize === 'undefined' ? 4 : this.maxFileSize;
                    return (item.size / 1024 / 1024).toFixed(2) <= this.maxFileSize;
                }
            }
            ///file type filter
            scope.constructor.prototype.typeFilter = {
                name: 'FileType',
                errorTitle: 'סוג הקובץ [{0}] לא נתמך!',
                errorText: 'ניתן לטעון לשרת קבצים \n מסוגים {0} בלבד.',
                fn: function (item) {
                    
                    if (typeof this.fileTypes === 'undefined')
                        this.fileTypes = ["jpg", "png", "gif", "tif", "jpeg", "pdf"];
                    if (this.fileTypes === true)
                        return true;
                    var type = '';
                    if (item.type && item.type.indexOf('/') > 0) {
                        type = item.type.split('/')[1].trim().toLowerCase();
                    }
                    else {
                        var count = (item.name.match(/\./g) || []).length;
                        type = item.name.split('.');
                        type = type[count].trim().toLowerCase();
                    }
                    return this.fileTypes.indexOf(type) >= 0;
                }
            }
            ///file type filter
            scope.constructor.prototype.imagesFilter = {
                name: 'FileType',
                errorTitle: 'סוג הקובץ [{0}] לא נתמך!',
                errorText: 'ניתן לטעון לשרת קבצים \n מסוגים {0} בלבד.',
                fn: function (item) {

                    if (typeof this.fileTypes === 'undefined')
                        this.fileTypes = ["jpg", "png", "gif", "jpeg"];
                    if (this.fileTypes === true)
                        return true;
                    var type = '';
                    if (item.type && item.type.indexOf('/') > 0) {
                        type = item.type.split('/')[1].trim().toLowerCase();
                    }
                    else {
                        var count = (item.name.match(/\./g) || []).length;
                        type = item.name.split('.');
                        type = type[count].trim().toLowerCase();
                    }
                    return this.fileTypes.indexOf(type) >= 0;
                }
            }
            /* **** FILTERS **** */
        }
    }]).run(function ($rootScope, FileUploaderServiceExtenders) {
        FileUploaderServiceExtenders.Initialize($rootScope);
    });

})();