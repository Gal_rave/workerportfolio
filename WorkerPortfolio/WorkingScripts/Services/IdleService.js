﻿(function () {
    angular.module('WorkerPortfolioApp').service('IdleService', ['Idle', 'Title', '$timeout', '$cookies', function (Idle, Title, $timeout, $cookies) {

        return { Initialize: Initialize };
        function pad(val) {
            return val < 10 ? ('0' + String(val)) : String(val);
        }
        function timeStamp() {
            var t = new Date();
            return pad(t.getMinutes()) + ':' + pad(t.getSeconds()) + '.' + t.getMilliseconds();
        }

        function Initialize(scope) {
            var DecryptCooky = function (reTry) {
                var tiket = $cookies.get('.MVCTET' + _SolutionBuildVersionNumber);
                var keys = $cookies.get('.MVCSPECN' + _SolutionBuildVersionNumber);
                if (tiket === undefined || tiket === null || keys === undefined || keys === null) {
                    ///try to wait for cooky data once
                    if (reTry !== true) {
                        $timeout(function () {
                            DecryptCooky(true);
                        }, 450);
                        return void [0];
                    }
                    ///logout
                    scope.publish('CatchTransmitLogout', false, true);
                    return void [0];
                }
                keys = keys.split(',');
                var key = CryptoJS.enc.Utf8.parse(keys[0]);
                var iv = CryptoJS.enc.Utf8.parse(keys[1]);
                try {
                    var decrypted = CryptoJS.AES.decrypt(tiket, key, {
                        keySize: 128 / 8,
                        iv: iv,
                        mode: CryptoJS.mode.CBC,
                        padding: CryptoJS.pad.Pkcs7
                    }).toString(CryptoJS.enc.Utf8);
                    
                    if (new Date(decrypted) <= new Date()) {
                        ///logout
                        scope.publish('CatchTransmitLogout', false, true);
                        return void [0];
                    }
                } catch (ex) {
                    console.log('ex', ex);
                }
            };
            scope.gotUserState = false;

            ///start the directive
            scope.getLoginState = function () {
                if (!scope.gotUserState) {
                    $timeout(function () {
                        scope.publish('TransmitLoginState', scope.gotUserState);
                    }, 100);
                }
            }
            scope.subscribe('VerifayLoginState', function ($credentials) {
                scope.gotUserState = true;
                if ($credentials.loggedIn && !Idle.running()) {
                    ///start the `idle` directive
                    Idle.watch();
                    ///check server side login     
                    $timeout(function () {
                        DecryptCooky();
                    }, 450);

                } else if (!$credentials.loggedIn) {
                    ///stop the `idle` directive
                    Idle.unwatch();
                }
            });

            scope.$on('IdleTimeout', function (e) {
                // the user has timed out transmit logout msg                
                scope.publish('CatchTransmitLogout', false, true);
                $timeout(function () { Title.restore(); }, 5000);
            });

            scope.$on('Keepalive', function (e) {
                ///check every [5m] the log in state
                scope.publish('TransmitLoginState', scope.gotUserState);
            });

            scope.getLoginState();
            Title.idleMessage("{{minutes}}:{{seconds}} לניתוק מהמערכת!");
            Title.timedOutMessage('נותקת מהמערכת!');
        }
    }]).run(function ($rootScope, IdleService) {
        IdleService.Initialize($rootScope);
    });

})();