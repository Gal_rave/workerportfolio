﻿(function () {
    

    var PayrollDataServise = angular.module('PayrollDataServise', []);

    PayrollDataServise.factory('PayrollServise', ['$http', '$q',
      function ($http, $q) {
          var setAction = function (action, params, type, baseUrl) {
              baseUrl = typeof baseUrl === 'string' ? baseUrl : '/api/WebService';
              type = typeof type !== 'undefined' ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  for (var i in params)
                      action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              } else {
                  for (var i in params)
                      action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return baseUrl + (typeof action !== 'undefined' && action.length > 0 ? '/' + action : '');
          }

          return {
              GeneralPayrollData: function (data) {
                  return $http.post(setAction('General'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              PayrollBySymbol: function (data) {
                  return $http.post(setAction('BySymbol'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetFundsTable: function (data) {
                  return $http.post(setAction('Funds'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              Get106Form: function (data) {
                  return $http.post(setAction('Form106'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetPaycheck: function (data) {
                  return $http.post(setAction('Paycheck'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetPersonalDetails: function (data) {
                  return $http.post(setAction('PersonalDetails'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetAttendanceFigures: function (data) {
                  return $http.post(setAction('AttendanceFigures'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              }
          }

      }]);

})();