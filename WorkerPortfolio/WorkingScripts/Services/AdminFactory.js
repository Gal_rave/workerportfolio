﻿(function () {
    var AdminFactory = angular.module('AdminFactory', []);

    AdminFactory.factory('Admin', ['$http', '$cookies',
      function ($http, $cookies) {
          var setAction = function (action, params, type) {
              var baseUrl = 'api/admin';
              type = typeof type !== 'undefined' && type !== null ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              } else {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return baseUrl + (typeof action !== 'undefined' && action !== null && action.length > 0 ? '/' + action : '');
          },
          encryptInformation = function (text, actionType) {
              if (actionType !== false)
                  actionType = true;
              var keys = $cookies.get('.MVCSPECN' + _SolutionBuildVersionNumber).split(',');
              text = String(text);
              var key = CryptoJS.enc.Utf8.parse(keys[0]);
              var iv = CryptoJS.enc.Utf8.parse(keys[1]);
              var encrypted = actionType !== false ? CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), key,
                  {
                      keySize: 128 / 8,
                      iv: iv,
                      mode: CryptoJS.mode.CBC,
                      padding: CryptoJS.pad.Pkcs7
                  }) : null;

              var decrypted = actionType === false ? CryptoJS.AES.decrypt(text, key, {
                  keySize: 128 / 8,
                  iv: iv,
                  mode: CryptoJS.mode.CBC,
                  padding: CryptoJS.pad.Pkcs7
              }).toString(CryptoJS.enc.Utf8) : null;

              return actionType ? String(encrypted) : String(decrypted);
          },
          encryptIDnumber = function (data) {
              var _data = angular.copy(data);
              _data.idNumber = encryptInformation(_data.idNumber);
              _data.calculatedIdNumber = encryptInformation(_data.calculatedIdNumber);
              if (_data.requiredRole != undefined && _data.requiredRole != null) _data.requiredRole = encryptInformation(_data.requiredRole);
              _data.isEncrypted = true;
              return _data;
          }

          return {
              SearchByIdNumber: function (data, query, results) {
                  data.idNumber = encryptInformation(data.idNumber);

                  return $http({
                      method: 'POST',
                      url: setAction('Idnumber/[query]/[results]', { results: results, query: query }, 'rest'),
                      data: JSON.stringify(data),
                      headers: {
                          'Accept': "application/json; charset=utf-8",
                          "X-Search": '1'
                      }
                  });
              },
              ValidUser: function (data) {
                  return $http.post(setAction("ValidUser"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              SearchUsers: function (data) {
                  return $http.post(setAction("SearchUsers"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetUserById: function (data) {
                  return $http.post(setAction("GetUserById"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              ResetPassword: function (data) {
                  var _data = encryptIDnumber(data);
                  _data.password = encryptInformation(_data.password);
                  return $http.post(setAction("ResetPassword"), JSON.stringify(_data), "json", "application/json; charset=utf-8");
              },
              SendUserReset: function (data) {
                  return $http.post(setAction("SendUserReset"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetMunicipalities: function (data) {
                  return $http.post(setAction("GetMunicipalities"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetSingleMunicipaly: function (data) {
                  return $http.post(setAction("GetSingleMunicipaly"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              UpdateAuthorizationList: function (data) {
                  return $http.put(setAction("UpdateAuthorizations"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              UpdateMunicipalities: function (data) {
                  return $http.put(setAction("UpdateMunicipalities"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetGroups: function (data) {
                  return $http.post(setAction("GetGroups"), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              UpdateUserGroups: function (data) {
                  return $http.post(setAction("UpdateUserGroups"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetUEPStatistics: function (data) {
                  return $http.post(setAction("UEPStatistics"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetForm101Statistics: function (data) {
                  return $http.post(setAction("Form101Statistics"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetLogInLogStatistics: function (data) {
                  return $http.post(setAction("LogInLogStatistics"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetDailyUsagetatistics: function (data) {
                  return $http.post(setAction("DailyUsagetatistics"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              ResetPayrollDisplay: function (data) {
                  return $http.patch(setAction("ResetUserPayrollDisplay"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              DeleteHRContact: function (data) {
                  data.idNumber = encryptInformation(angular.copy(data.idNumber));
                  return $http.delete(setAction('DeleteHRContact/[idNumber]/[contactId]', data, 'rest'), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              UpdateContact: function (data) {
                  return $http.put(setAction("UpdateHRContact"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              CheckUserSmsMessages: function (data) {
                  return $http.post(setAction("UserMessages"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              CheckUserMailMessages: function (data) {
                  return $http.post(setAction("UserMailMessages"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              }
          }

      }]);

})();