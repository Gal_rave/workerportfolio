﻿(function () {
    

    angular.module('WorkerPortfolioApp').factory('TofesDataService', TofesDataService);

    TofesDataService.$inject = ['$rootScope'];

    function TofesDataService($rootScope) {
        var checkIfHaveValue = function (value) {
            return typeof value != 'undefined' && value != null && value != '';
        }
        , validateOrigin = function (origin, update) {
            if (!checkIfHaveValue(origin.date))
                origin.date = new Date();
            var now = new Date();
            var timeDiff = Math.abs(origin.date.getTime() - now.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            if (diffDays > 14 || (checkIfHaveValue(origin.employee.idNumber) && checkIfHaveValue(update.employee.idNumber) && origin.employee.idNumber != update.employee.idNumber)) {
                $rootScope.lsRemove('$$tofesModel');
                $rootScope.lsRemove('states');
                update = angular.copy(baseModel);
                update.date = new Date();
            }
            return update;
        }
        , checkObjectVal = function (obj, attr) {
            return obj != undefined && typeof obj !== 'undefined' && obj != null &&
                    obj[attr] != undefined && typeof obj[attr] !== 'undefined' && obj[attr] != null && !String(obj[attr]).isempty();
        }
        ///private function to parse the `TofesModel` and update it.
        , checkIfDate = function (attr) {
            return (String(attr).toLowerCase().indexOf('date') > -1);
        }
        , transformDate = function (val, attr) {
            var pad = function (val) { return parseInt(val) < 10 ? ('0' + String(val)) : val; }

            if (String(val).indexOf('T') > 3 && !(val instanceof Date)) {    
                val = new Date(val);
            }
            
            if (val instanceof Date) {
                return [pad(val.getDate()), pad(val.getMonth() + 1), val.getFullYear()].join('/');
            }
            return val;
        }
        /* *** update only matching attributes in both objects *** */
        , updateMatchingAttributes = function (original, data) {
            var matching = objectsCompare(original, data);
            $.map(matching, function (item) {
                original[item] = data[item];
            });
        }
        /* *** compare two objects and return matching attributes *** */
        , objectsCompare = function (original, data) {
            var originAray = _.keys(original), dataArray = _.keys(data), intersect = _.intersection(originAray, dataArray);
            return $.map(original, function (val, attr) {
                if (!(original[attr] instanceof Object) && !(original[attr] instanceof Array) && intersect.indexOf(attr) > -1) return attr;
            });
        }
        
        , doUpdatePrimitives = function (keysToUpdate, origin, update, override) {
            $.map(keysToUpdate, function (item) {
                if ((update[item] || typeof update[item] === 'boolean') && !String(update[item]).isempty() && (override || origin[item] === null || !origin[item] || String(origin[item]).isempty()) && (origin[item] != null || update[item] != null)) {
                    origin[item] = update[item];
                }
            });
        }
        , doUpdateDates = function (keysToUpdate, origin, update, override) {
            $.map(keysToUpdate, function (item) {
                if (checkObjectVal(update, item) && update[item] !== '0001-01-01T00:00:00' && (override || !checkObjectVal(origin, item))) {
                    origin[item] = transformDate(update[item]);
                }
            });
        }
        , doUpdateArray = function (model, origin, update, override, attr) {
            var O_uniq = [], U_uniq = [];
            switch (attr) {
                case 'children':
                    O_uniq = _.groupBy(origin, 'idNumber');
                    U_uniq = _.groupBy(update, 'idNumber');
                    break;
                case 'employers':
                    O_uniq = _.groupBy(origin, '$id');
                    U_uniq = _.groupBy(update, '$id');
                    break;
                case 'TofesControllerFilesList':
                case 'TaxExemptionControllerFilesList':
                case 'TaxAdjustmentControllerFilesList':
                    O_uniq = _.groupBy(origin, 'imageId');
                    U_uniq = _.groupBy(update, 'imageId');
                    break;
            }
            $.map(U_uniq, function (item, index) {
                if (typeof O_uniq[index] === 'undefined') {
                    origin.push(UpdateEntireObject(angular.copy(model), U_uniq[index][0], override));
                }
            });        
        }
        , UpdateEntireObject = function (origin, update, override) {
            var originKeys = _.keys(origin), updateKeys = _.keys(update), intersect = _.intersection(originKeys, updateKeys);
            ///update only primitive types
            var primitiveKeys = $.map(origin, function (val, attr) {
                if (!(origin[attr] instanceof Object) && !(origin[attr] instanceof Array) && intersect.indexOf(attr) > -1 && !checkIfDate(attr)) return attr;
            });
            doUpdatePrimitives(primitiveKeys, origin, update, override);

            var dateKeys = $.map(origin, function (val, attr) {
                if (checkIfDate(attr)) return attr;
            });
            doUpdateDates(dateKeys, origin, update, override);
            
            ///update arrays
            $.map(origin, function (val, attr) {
                if ((origin[attr] instanceof Array) && intersect.indexOf(attr) > -1) {
                    doUpdateArray(origin[attr + 'ArrayType'], origin[attr], update[attr], override, attr);
                }
            });
            ///update inner objects
            $.map(origin, function (val, attr) {
                if ((origin[attr] instanceof Object) && intersect.indexOf(attr) > -1)
                    UpdateEntireObject(origin[attr], update[attr], override);
            });

            return origin;
        }

        ,CheckIncomeType = function (newModel, oldModel) {
            for (var i in newModel)
                CheckRunTimeDifferences(newModel[i], oldModel[i]);
        }
        , CheckPrimitiveKeys = function (primitiveKeys, newModel, oldModel) {
            $.map(primitiveKeys, function (attr) {

                if (attr.indexOf('Check') != 0 && attr.indexOf('$') != 0) {
                    var od = oldModel != null && oldModel[attr] != null && !String(oldModel[attr]).isempty();
                    var nd = newModel != null && newModel[attr] != null && !String(newModel[attr]).isempty();
                    
                    ///no change id data
                    newModel['Check' + attr] = 0;
                    if (!od && nd) {
                        ///new data
                        newModel['Check' + attr] = 1;
                    }
                    if (od && nd && newModel[attr] != oldModel[attr]) {
                        ///data changed
                        newModel['Check' + attr] = 2;
                    }
                    if (attr == 'incomeType') {
                        newModel['PrevIncomeType'] = oldModel[attr];
                    }
                    if (attr === 'idNumber') {
                        newModel['PrevIdNumber'] = oldModel[attr];
                    }
                }
            });
        }
        , CheckDateFields = function (DateKeys, newModel, oldModel) {
            $.map(DateKeys, function (attr) {
                if (attr.indexOf('Check') != 0 && typeof newModel[attr] != 'function') {
                    ///no change id data
                    newModel['Check' + attr] = 0;
                    if (((oldModel == null ? null : oldModel[attr]) == null || String((oldModel == null ? null : oldModel[attr])).isempty()) && (newModel[attr] != null && !String(newModel[attr]).isempty())) {
                        ///new data
                        newModel['Check' + attr] = 1;
                    }
                    if ((oldModel == null ? null : oldModel[attr]) != null && !String((oldModel == null ? null : oldModel[attr])).isempty() && newModel[attr] != null && !String(newModel[attr]).isempty()
                        && transformDate(newModel[attr]) != transformDate((oldModel == null ? null : oldModel[attr]))) {
                        ///data changed
                        newModel['Check' + attr] = 2;
                    }
                }
            });
        }
        , CheckArrays = function (ArrayType, newArray, oldArray, attr) {
            var old_uniq = [], new_uniq = [];
            switch (attr) {
                case 'children':
                    old_uniq = _.groupBy(oldArray, 'ID');
                    new_uniq = _.groupBy(newArray, 'ID');
                    break;
                case 'employers':
                    old_uniq = _.groupBy(oldArray, '$id');
                    new_uniq = _.groupBy(newArray, '$id');
                    break;
                case 'TofesControllerFilesList':
                case 'TaxExemptionControllerFilesList':
                case 'TaxAdjustmentControllerFilesList':
                    old_uniq = _.groupBy(oldArray, 'imageId');
                    new_uniq = _.groupBy(newArray, 'imageId');
                    break;
            }
            $.map(new_uniq, function (item, index) {
                ///no new data
                new_uniq[index][0].newItem = 0;
                ///check if new child
                if (typeof old_uniq[index] === 'undefined') {
                    new_uniq[index][0].newItem = 1;
                } else {
                    CheckRunTimeDifferences(new_uniq[index][0], old_uniq[index][0])
                }
            });

        }
        , CheckRunTimeDifferences = function (newModel, oldModel) {
            var newKeys = _.keys(newModel), oldKeys = _.keys(oldModel), intersect = _.intersection(newKeys, oldKeys);
            ///check only primitive types
            var primitiveKeys = $.map(newModel, function (val, attr) {
                if (!(newModel[attr] instanceof Object) && !(newModel[attr] instanceof Array) && intersect.indexOf(attr) > -1 && !checkIfDate(attr)) return attr;
            });
            CheckPrimitiveKeys(primitiveKeys, newModel, oldModel);
            ///check date fields
            var dateKeys = $.map(newModel, function (val, attr) {
                if (checkIfDate(attr)) return attr;
            });
            
            CheckDateFields(dateKeys, newModel, oldModel);

            ///check arrays
            $.map(newModel, function (val, attr) {
                if ((newModel[attr] instanceof Array) && intersect.indexOf(attr) > -1) {
                    CheckArrays(newModel[attr + 'ArrayType'], newModel[attr], oldModel[attr], attr);
                }
            });
            
            ///check inner objects
            $.map(newModel, function (val, attr) {
                if ((newModel[attr] instanceof Object) && intersect.indexOf(attr) > -1 && attr.indexOf('ArrayType') < 0) {
                    
                    //if (attr == 'employers') {
                    //    CheckIncomeType(newModel[attr], oldModel[attr]);
                    //}

                    CheckRunTimeDifferences(newModel[attr], oldModel[attr]);
                }
            });
            return newModel;
        }
        
        , ComputeStates = ComputeStates || function (list, current, formType) {
            var state = { position: 0, isCurrent: false, clas: '', text: '', goTo: '', isCompleted: false }, copy = {}, index = 0;
            ComputeStates.states = [];
            ComputeStates.current = -1;
            list.filter(function (s) {
                if (s.abstract !== true && s.data.type === 'Form101' && (s.data.formType === 0 || (formType !== 3 && s.data.formType === 2) || (formType === 3 && s.data.formType === 3))) {
                    copy = angular.copy(state);
                    copy.text = s.title;
                    copy.goTo = s.name;
                    copy.position = index++;
                    ComputeStates.states.push(copy);
                }                    
            });
            return ComputeStates.states;
        }

        , Genders = [ 
            {
                id:true,
                name: 'זכר',
                selected: false
            },
            {
                id: false,
                name: 'נקבה',
                selected: false
            }
        ]

        , MaritalStates = [
            {
                id: 1,
                name: 'רווק/ה',
                selected: false
            },
            {
                id: 2,
                name: 'נשוי/אה',
                selected: false
            },
            {
                id: 3,
                name: 'גרוש/ה',
                selected: false
            },
            {
                id: 4,
                name: 'אלמן/ה',
                selected: false
            },
            {
                id: 5,
                name: 'פרוד/ה',
                selected: false
            },
        ]

        , ResidentStates = [
            {
                id: 1,
                name: 'כן',
                selected: true
            },
            {
                id: 2,
                name: 'לא',
                selected: false
            }
        ]

        , KibbutzStates = [
            {
                id: 1,
                name: 'כן',
                selected: true
            },
            {
                id: 2,
                name: 'לא',
                selected: false
            }
        ]

        , IncomeStates = [
            {
                id: 1,
                name: 'אין לבן/בת הזוג כל הכנסה',
                selected: false
            },
            {
                id: 2,
                name: 'יש לבן/בת הזוג הכנסה',
                selected: true
            }
        ]

        , IncomeTypeStates = [
            {
                id: 1,
                name: 'עבודה/קיצבה/עסק',
                selected: false
            },
            {
                id: 2,
                name: 'הכנסה אחרת',
                selected: false
            }
        ]

        , OtherIncomesStates = [
            {
                id: 1,
                name: 'אין לי הכנסות אחרות לרבות מלגות',
                selected: false
            },
            {
                id: 2,
                name: 'יש לי הכנסות נוספות כמפורט להלן',
                selected: true
            }
        ]

        , IncomeTaxCreditsStates = [
            {
                id: 1,
                name: 'אבקש לקבל נקודות זיכוי ומדרגות מס כנגד הכנסתי זו (סעיף ד). איני מקבל/ת אותם בהכנסה אחרת.',
                selected: false
            },
            {
                id: 2,
                name: 'אני מקבל/ת נקודות זיכוי ומדרגות מס בהכנסה אחרת ועל כן איני זכאי/ת להם כנגד הכנסה זו.',
                selected: false
            }
        ]

        , ImmigrantStatusStates = [
            {
                id: 1,
                name: 'עולה חדש/ה',
                selected: false
            },
            {
                id: 2,
                name: 'תושב/ת חוזר/ת',
                selected: false
            }
        ]

        , TaxCoordinationReasonStates = [
            {
                id: 1,
                name: 'לא הייתה לי הכנסה מתחילת שנת המס הנוכחית עד לתחילת עבודתי אצל מעביד זה.',
                selected: false
            },
            {
                id: 2,
                name: 'יש לי הכנסות נוספות ממשכורת כמפורט להלן.',
                selected: false
            },
            {
                id: 3,
                name: 'פקיד השומה אישר תיאום לפי אישור מצורף.',
                selected: false
            }
        ]

        , TaxCoordinationIncomeTypeStates = [
            {
                id: 1,
                name: 'עבודה',
                selected: false
            },
            {
                id: 2,
                name: 'קיצבה',
                selected: false
            },
            {
                id: 3,
                name: 'מילגה',
                selected: false
            },
            {
                id: 4,
                name: 'אחר',
                selected: false
            }
        ]
        
        , Hmos = [
            {
                id: 2,
                name: 'כללית'
            },
            {
                id: 3,
                name: 'לאומית'
            },
            {
                id: 4,
                name: 'מכבי'
            },
            {
                id: 23,
                name: 'מאוחדת'
            }
        ]
        
        , FileModel = {
            filename: null,
            userIDNumber: null
        }

        , ChildModel = {
            ID: null,
            firstName: null,
            idNumber: null,
            birthDate: null,
            homeLiving: false,
            childBenefit: false,
            possession: null,
            gender: null,
        }
        
        , FileListModel = {
            filename: null,
            imageId: null,
            userIDNumber: null,
            year: null,
            origin: null,
            originalName:null
        }

        , EmployerModel = {
            $id: 0,
            name: null,
            address: null,
            deductionFileID: null,
            salary: null,
            taxDeduction: null,
            incomeType: null,
            mifal: null,
            rashut: null
        }

        , TofesModel = {
            year: null,
            FormType: null,
            date: null,
            signatureDate: null,
            signature: null,
            signatureDataUrl: null,
            emailEmployee: null,

            employer: {
                name: null,
                address: null,
                phoneNumber: null,
                deductionFileID: null,
                email: null,
                displayEmail: false,
                mifal: null,
                rashut: null,
            },

            employee: {
                firstName: null,
                lastName: null,
                idNumber: null,
                birthDate: null,
                immigrationDate: null,
                city: null,
                street: null,
                houseNumber: null,
                zip: null,
                phone: null,
                gender: null,
                maritalStatus: null,
                ilResident: null,
                kibbutzResident: null,
                hmoMember: null,
                hmoName: null
            },
            
            children: [/*Array of ChildModel*/],
            childrenArrayType: ChildModel,

            thisIncome: {
                startDate: null,
                monthSalary: null,
                anotherSalary: null,
                partialSalary: null,
                daySalary: null,
                allowance: null,
                stipend: null,
            },

            otherIncomes: {
                hasIncomes: null,
                monthSalary: null,
                anotherSalary: null,
                partialSalary: null,
                daySalary: null,
                allowance: null,
                stipend: null,
                anotherSource: null,
                anotherSourceDetails: '',
                incomeTaxCredits: null,
                Provisions: {
                    trainingFund: null,
                    workDisability: null
                }
            },

            spouse: {
                idNum: null,
                firstName: null,
                lastName: null,
                birthDate: null,
                immigrationDate: null,
                hasIncome: null,
                incomeType: null
            },

            taxExemption: {
                blindDisabled: null,
                isResident: null,
                settlementCredits: {
                    startResidentDate: null,
                    settlement: null,
                },
                isImmigrant: null,
                immigration: {
                    immigrantStatus: null,
                    immigrantStatusDate: null,
                    noIncomeDate: null,
                },
                spouseNoIncome: null,
                separatedParent: null,
                childrenInCare: null,
                infantchildren: null,
                singleParent: null,
                alimonyParticipates: null,
                exAlimony: null,
                partnersUnder18: null,
                exServiceman: null,
                servicemanStartDate: null,
                servicemanEndDate: null,
                graduation: null,
                incompetentChild: null
            },

            taxCoordination: {
                request: null,
                reason: null,
                employers: [/*Array of EmployerModel*/],
                employersArrayType: EmployerModel,
            }
        }
        
        , baseModel = {
            TofesControllerFilesList: [],
            TofesControllerFilesListArrayType: FileListModel,
            TaxExemptionControllerFilesList: [],
            TaxExemptionControllerFilesListArrayType: FileListModel,
            TaxAdjustmentControllerFilesList: [],
            TaxAdjustmentControllerFilesListArrayType: FileListModel,
            children: [],
            childrenArrayType: ChildModel,
            employee: {
                firstName: null,
                lastName: null,
                idNumber: null,
                birthDate: null,
                immigrationDate: null,
                city: null,
                street: null,
                houseNumber: null,
                zip: null,
                phone: null,
                cell: null,
                email: null,
                gender: null,
                maritalStatus: null,
                ilResident: null,
                kibbutzResident: null,
                hmoMember: null,
                hmoName: null,
                startWorkTime: null
            },
            employer: {
                name: null,
                address: null,
                phoneNumber: null,
                deductionFileID: null,
                displayEmail: null,
                email: null,
                mifal: null,
                rashut: null,
            },
            spouse: {
                idNumber: null,
                firstName: null,
                lastName: null,
                birthDate: null,
                immigrationDate: null,
                hasIncome:null,
                incomeType: null
            },
            taxCoordination: {
                request: null,
                reason: null,
                employers: [/*Array of EmployerModel*/],
                employersArrayType: EmployerModel,
            },
            taxExemption: {
                blindDisabled: null,
                isResident: null,
                settlementCredits: {
                    startResidentDate: null,
                    settlement: null,
                },
                isImmigrant: null,
                immigration: {
                    immigrantStatus: null,
                    immigrantStatusDate: null,
                    noIncomeDate: null,
                },
                spouseNoIncome: null,
                separatedParent: null,
                childrenInCare: null,
                infantchildren: null,
                singleParent: null,
                alimonyParticipates: null,
                exAlimony: null,
                partnersUnder18: null,
                exServiceman: null,
                servicemanStartDate: null,
                servicemanEndDate: null,
                graduation: null,
                incompetentChild: null,
            },
            year: null,
            date: null,
            signatureDate: null,
            signatureDataUrl: null,
            signature: null,
            emailEmployee: null,
            sign: null,
            signatureImageID: null,
            thisIncome: {
                startDate: null,
                monthSalary: null,
                anotherSalary: null,
                partialSalary: null,
                daySalary: null,
                allowance: null,
                stipend: null,
            },
            otherIncomes: {
                hasIncomes: null,
                monthSalary: null,
                anotherSalary: null,
                partialSalary: null,
                daySalary: null,
                allowance: null,
                stipend: null,
                anotherSource: null,
                anotherSourceDetails: '',
                incomeTaxCredits: null,
                Provisions: {
                    trainingFund: null,
                    workDisability: null
                }
            },
            formPdfImageID: null,
            formPdf: null,
            FormType: null
        }
          
        , baseModelCopy = {}

        return {
            GetStartWorkInYearDate: function(startWorkDate, year){
                var startOfYear = new Date(year, 0, 1);
                if (!checkIfHaveValue(startWorkDate))
                    return startOfYear;
                var d = new Date(startWorkDate);
                startOfYear = d > startOfYear ? d : startOfYear;
                return transformDate(startOfYear);
            },
            TransformToDate: transformDate,
            SetFirstOfYear: function(model){
                if (!checkIfHaveValue(model)) {
                    return '01/01/' + new Date().getFullYear();
                }
                return model;
            },
            GetExistingData: function (origin, type, override) {
                if (override !== true)
                    override = false;
                var preExisting = $rootScope.lsGet(type);
                ///INSERT PRE EXISTING DATA INTO FORM
                if (preExisting && typeof preExisting === 'object') {
                    preExisting = validateOrigin(origin, preExisting);
                    return UpdateEntireObject(origin, preExisting, override);
                }
                return origin;
            },
            GetTofes: function() {
                return TofesModel;
            },
            GetRadiosStates: function () {
                return {
                    genders: Genders,
                    maritalStates: MaritalStates,
                    residentStates: ResidentStates,
                    hmos: Hmos,
                    incomeStates: IncomeStates,
                    incomeTypeStates: IncomeTypeStates,
                    kibbutzStates: KibbutzStates,
                    otherIncomesStates: OtherIncomesStates,
                    incomeTaxCreditsStates: IncomeTaxCreditsStates,
                    immigrantStatusStates: ImmigrantStatusStates,
                    taxCoordinationReasonStates: TaxCoordinationReasonStates,
                    taxCoordinationIncomeTypeStates: TaxCoordinationIncomeTypeStates
                };
            },
            GetCustomModel: function () {
                return {
                    fileModel: FileModel,
                    childModel: ChildModel,
                    employerModel: EmployerModel
                };
            },
            GetComputedStates: function (list, current, formType) {
                return ComputeStates(list, current, formType);
            },
            UpdateObject: function (origin, update, toOverride) {
                if (toOverride !== true)
                    toOverride = false;
                update = validateOrigin(origin, update);
                return UpdateEntireObject(origin, update, toOverride);
            },
            GetTofesControllerData: function () {
                return angular.copy(baseModel);
            },
            SetTofesControllerData: function (obj) {
                TofesControllerData = obj;
            },
            GetFamilyDetailsControllerData: function () {
                return FamilyDetailsControllerData;
            },
            GetIncomeControllerData: function () {
                return IncomeControllerData;
            },
            GetTaxExemptionControllerData: function () {
                return TaxExemptionControllerData;
            },
            GetTaxAdjustmentControllerData: function () {
                return TaxAdjustmentControllerData;
            },
            GetAttestationControllerData: function () {
                return AttestationControllerData;
            },
            CopyRunTimeModel: function (model) {
                if (model !== null && Object.keys(model).length > 10)
                    $rootScope.lsSet('$$tofesModelCopy', angular.copy(model));
            },
            CheckRunTimeCopy: function (model) {
                var old = $rootScope.lsGet('$$tofesModelCopy');
                return CheckRunTimeDifferences(model, old);                 
            }
        }
    }
})();