﻿(function () {
    angular.module('WorkerPortfolioApp').factory('fileUploadeFactory', FileUploadeFactory);

    FileUploadeFactory.$inject = ['$http'];

    function FileUploadeFactory($http) {
        ///set action and get/rest params
        var setAction = function (action, params, type) {
            var baseUrl = 'api/FilesUploades';
            type = typeof type !== 'undefined' ? type.toLowerCase() : 'get';
            if (!params instanceof Object) params = new Object();
            if (type === 'get') {
                for (var i in params)
                    action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

            } else {
                for (var i in params)
                    action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                action = action.replace(/\/\[([^)]+)\]/ig, '');
            }

            return baseUrl + (typeof action !== 'undefined' && action.length > 0 ? '/' + action : '');
        }
        ///do the ajax request
        var request = function (req) {
            return $.ajax(req);
        }

        return {
            Signature: function (idNumber, year, base64, token, municipalityId) {
                var SignatureModel = {
                    idNumber: idNumber,
                    year: year,
                    base64: base64,
                    fileName: 'userSignature.png',
                    Token: token,
                    municipalityId: municipalityId
                };
                return $http.post(setAction('SaveBase64'), JSON.stringify(SignatureModel), "json", "application/json; charset=utf-8");
            },
            SaveForm: function (data) {
                return $http.put(setAction('createForm101'), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            SendForm: function (data) {
                return $http.post(setAction('SendFormToWS'), JSON.stringify(data), "json", "application/json; charset=utf-8");
            }
        }
    }
})();