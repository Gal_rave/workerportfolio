﻿(function () {
    var LoginService = angular.module('LoginService', []);

    LoginService.factory('Credentials', ['$http', '$cookies',
      function ($http, $cookies) {
          var setAction = function (action, params, type, isAnonymous) {
              var baseUrl = '/api/' + (isAnonymous ? 'AnonymousAccount' : 'Account');
              type = typeof type !== 'undefined' ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  for (var i in params)
                      action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              } else {
                  for (var i in params)
                      action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return baseUrl + (typeof action !== 'undefined' && action.length > 0 ? '/' + action : '');
          },
          encryptInformation = function (text, actionType) {
              if (actionType !== false)
                  actionType = true;
              var keys = $cookies.get('.MVCSPECN' + _SolutionBuildVersionNumber).split(',');
              text = String(text);
              var key = CryptoJS.enc.Utf8.parse(keys[0]);
              var iv = CryptoJS.enc.Utf8.parse(keys[1]);
              var encrypted = actionType !== false ? CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), key,
                  {
                      keySize: 128 / 8,
                      iv: iv,
                      mode: CryptoJS.mode.CBC,
                      padding: CryptoJS.pad.Pkcs7
                  }) : null;

              var decrypted = actionType === false ? CryptoJS.AES.decrypt(text, key, {
                  keySize: 128 / 8,
                  iv: iv,
                  mode: CryptoJS.mode.CBC,
                  padding: CryptoJS.pad.Pkcs7
              }).toString(CryptoJS.enc.Utf8) : null;

              return actionType ? String(encrypted) : String(decrypted);
          },
          encryptLogIn = function (data) {
              data.idNumber = encryptInformation(data.idNumber);
              data.password = encryptInformation(data.password);
              return {
                  key: '',
                  iv: '',
                  idNumber: data.idNumber,
                  password: data.password
              };
          },
          encryptWSModel = function (data) {
              var _data = angular.copy(data);
              _data.idNumber = encryptInformation(data.idNumber);
              _data.calculatedIdNumber = encryptInformation(data.calculatedIdNumber);
              if (_data.requiredRole != undefined && _data.requiredRole != null) _data.requiredRole = encryptInformation(_data.requiredRole);
              _data.isEncrypted = true;
              return _data;
          }

          return {
              EncryptData: function (text) {
                  return encryptInformation(text);
              },
              GetFullUserdetails: function (data) {
                  return $http.post(setAction('GetFullUserdetails'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              UpdateUserDetails: function (data) {
                  return $http.put(setAction('UpdateUserDetails'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetUserdetails: function (data) {
                  return $http.post(setAction('Userdetails'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              GetUserPartnerDetails: function (data) {
                  return $http.post(setAction('PartnerDetails'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              GetEducationStatus: function (data) {
                  return $http.post(setAction('EducationStatus'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              GetUserChildren: function (data) {
                  return $http.post(setAction('UserChildren'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              Login: function (data) {
                  return $http.post(setAction('login', undefined, undefined, true), JSON.stringify(encryptLogIn(angular.copy(data))), "json", "application/json; charset=utf-8");
              },
              LogOut: function (data) {
                  return $http.get(setAction('LogOut', undefined, undefined, true), "json", "application/json; charset=utf-8");
              },
              ResetPassword: function (data) {
                  return $http.post(setAction('[idNumber]/[mail]/[sms]', data, 'rest', true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              ChangePasswordFromToken: function (data) {
                  return $http.post(setAction('ChangePasswordFromToken', undefined, undefined, true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetLocalPasswordSettings: function (data) {
                  return $http.get(setAction('GetLocalPasswordSettings', undefined, undefined, true), "json", "application/json; charset=utf-8");
              },
              GetCurrentUser: function (data) {
                  return $http.post(setAction('[authenticationToken]', data, 'rest', true), "json", "application/json; charset=utf-8");
              },
              LoginUserFromToken: function (data) {
                  return $http.get(setAction('[authenticationToken]/[municipalityId]', data, 'rest', true), "json", "application/json; charset=utf-8");
              },
              GetCurrentUserForProcesses: function (data) {
                  return $http.post(setAction('Kad/' + '[authenticationToken]/[rashut]/[mncplity]', data, 'rest', true), "json", "application/json; charset=utf-8");
              },
              AuthenticateUser: function (data) {
                  return $http.get(setAction('[authenticationToken]', data, 'rest', true), "json", "application/json; charset=utf-8");
              },
              CreateUser: function (data) {
                  return $http.put(setAction(undefined, undefined, undefined, true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              RegisterFromInitialPassword: function (data) {
                  return $http.put(setAction('RegisterFromInitialPassword', undefined, undefined, true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              RegisterFromSms: function (data) {
                  return $http.put(setAction('RegisterFromSms', undefined, undefined, true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              VerifySmsToken: function (data) {
                  return $http.post(setAction('VerifySmsToken', undefined, undefined, true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              RegisterFromEmail: function (data) {
                  return $http.put(setAction('RegisterFromEmail', undefined, undefined, true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              CheckValidUser: function (data) {
                  return $http.put(setAction('CheckRegister', undefined, undefined, true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              ChangeMunicipality: function (data) {
                  return $http.post(setAction("ChangeMunicipality"), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              RefreshUserData: function (data) {
                  return $http({
                      method: 'POST',
                      url: setAction('UpdateUserData'),
                      data: JSON.stringify(encryptWSModel(data)),
                      headers: {
                          'Accept': "application/json; charset=utf-8",
                          "X-Search": '1'
                      }
                  });
              },
              InitialActivationCheck: function (data) {
                  data.idNumber = encryptInformation(data.idNumber);
                  return $http.post(setAction('CheckInitialActivation', undefined, undefined, true), JSON.stringify(angular.copy(data)), "json", "application/json; charset=utf-8");
              },
              GetUPCData: function (data) {
                  return $http.post(setAction('GetUPCData', undefined, undefined, false), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              GetUserUpdateTypes: function (data) {
                  return $http.post(setAction('UserUpdateTypes', undefined, undefined, false), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              GetSynerionRedirect: function (data) {
                  return $http.patch(setAction('GetRedirectInfo'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              }
          }

      }]);

})();