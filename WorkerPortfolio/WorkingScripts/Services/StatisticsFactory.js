﻿(function () {
    var StatisticsFactory = angular.module('StatisticsFactory', []);

    StatisticsFactory.factory('Statistics', ['$http', '$cookies',
      function ($http, $cookies) {
          var setAction = function (action, params, type, _url) {
              var baseUrl = typeof _url !== 'undefined' && _url !== null? _url : 'AdminStatistics';
              type = typeof type !== 'undefined' && type !== null ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              } else {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return baseUrl + (typeof action !== 'undefined' && action !== null && action.length > 0 ? '/' + action : '');
          },
          encryptInformation = function (text, actionType) {
              if (actionType !== false)
                  actionType = true;
              var keys = $cookies.get('.MVCSPECN' + _SolutionBuildVersionNumber).split(',');
              text = String(text);
              var key = CryptoJS.enc.Utf8.parse(keys[0]);
              var iv = CryptoJS.enc.Utf8.parse(keys[1]);
              var encrypted = actionType !== false ? CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), key,
                  {
                      keySize: 128 / 8,
                      iv: iv,
                      mode: CryptoJS.mode.CBC,
                      padding: CryptoJS.pad.Pkcs7
                  }) : null;

              var decrypted = actionType === false ? CryptoJS.AES.decrypt(text, key, {
                  keySize: 128 / 8,
                  iv: iv,
                  mode: CryptoJS.mode.CBC,
                  padding: CryptoJS.pad.Pkcs7
              }).toString(CryptoJS.enc.Utf8) : null;

              return actionType ? String(encrypted) : String(decrypted);
          },
          encryptIDnumber = function (data) {
              var _data = angular.copy(data);
              _data.idNumber = encryptInformation(_data.idNumber);
              _data.calculatedIdNumber = encryptInformation(_data.calculatedIdNumber);
              if (_data.requiredRole != undefined && _data.requiredRole != null) _data.requiredRole = encryptInformation(_data.requiredRole);
              _data.isEncrypted = true;
              return _data;
          }

          return {
              MunicipalityStatistics: function (data) {
                  return $http.post(setAction("MunicipalityFormStatistics"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetPopulations: function (data) {
                  return $http.post(setAction("Populations"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              CallPopulation: function (data) {
                  return $http.post(setAction("CallPopulation"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetPopulationGroups: function (data) {
                  return $http.post(setAction("PopulationGroups"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GroupEmailSearchUsers: function (data) {
                  return $http.post(setAction("GroupEmailSearch"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              SendEmails: function (data) {
                  return $http.put(setAction("SendPopulationEmails"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              SendOffer: function (data) {
                  return $http.put(setAction("SendProposal", null, null, "Mailer"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              SendReminder: function (data) {
                  return $http.put(setAction("SendFormReminder", null, null, "Mailer"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              }
          }

      }]);

})();