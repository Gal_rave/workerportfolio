﻿(function () {
    angular.module('WorkerPortfolioApp').service('AccessabilityService', ['$rootScope', 'Title', function ($rootScope, Title) {

        return { Initialize: Initialize };

        function Initialize(scope) {
            scope.Title = Title;
            window.$rootScope = $rootScope;

            scope.constructor.prototype.accessability = scope.constructor.prototype.accessability
             || {
                 show: false,
                 definitions: (function () {
                     var def = $rootScope.lsGet('accessability');
                     if (!!def)
                         return def;
                     return {
                         Size: 'regular',
                     };
                 })(),
                 open: function () {
                     this.show = !this.show;
                 },
                 handleFontsLarg: function () {
                     var that = this;
                     this.definitions.Size = 'large';
                     $rootScope.lsSet("definitions", that.definitions);
                     this.doAccessabilityAction();
                 },
                 handleFontsMedium: function () {
                     var that = this;
                     this.definitions.Size = 'medium';
                     $rootScope.lsSet("definitions", that.definitions);
                     this.doAccessabilityAction();
                 },
                 handleFontsRegular: function () {
                     var that = this;
                     this.definitions.Size = 'regular';
                     $rootScope.lsSet("definitions", that.definitions);
                     this.doAccessabilityAction();
                 },
                 handleContrast: function () {
                     var that = this;
                     this.definitions.contrast = !this.definitions.contrast;
                     $rootScope.lsSet("definitions", that.definitions);
                     this.doAccessabilityAction();
                 }
             }

        }
    }]).run(function ($rootScope, AccessabilityService) {
        AccessabilityService.Initialize($rootScope);
    });
})();