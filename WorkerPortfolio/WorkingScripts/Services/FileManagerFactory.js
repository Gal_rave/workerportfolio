﻿(function () {

    var FileManagerFactory = angular.module('FileManagerFactory', []);

    FileManagerFactory.factory('FileManager', ['$http', '$cookies',
      function ($http, $cookies) {
          var setAction = function (action, params, type) {
              var baseUrl = 'filemanager';
              type = typeof type !== 'undefined' && type !== null ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              } else {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return baseUrl + (typeof action !== 'undefined' && action !== null && action.length > 0 ? '/' + action : '');
          },
          encryptInformation = function (text, actionType) {
              if (actionType !== false)
                  actionType = true;
              var keys = $cookies.get('.MVCSPECN' + _SolutionBuildVersionNumber).split(',');
              text = String(text);
              var key = CryptoJS.enc.Utf8.parse(keys[0]);
              var iv = CryptoJS.enc.Utf8.parse(keys[1]);
              var encrypted = actionType !== false ? CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), key,
                  {
                      keySize: 128 / 8,
                      iv: iv,
                      mode: CryptoJS.mode.CBC,
                      padding: CryptoJS.pad.Pkcs7
                  }) : null;

              var decrypted = actionType === false ? CryptoJS.AES.decrypt(text, key, {
                  keySize: 128 / 8,
                  iv: iv,
                  mode: CryptoJS.mode.CBC,
                  padding: CryptoJS.pad.Pkcs7
              }).toString(CryptoJS.enc.Utf8) : null;

              return actionType ? String(encrypted) : String(decrypted);
          },
          encryptWSModel = function (data) {
              var _data = angular.copy(data);
              _data.idNumber = encryptInformation(data.idNumber);
              _data.calculatedIdNumber = encryptInformation(data.calculatedIdNumber);
              if (_data.requiredRole != undefined && _data.requiredRole != null) _data.requiredRole = encryptInformation(_data.requiredRole);
              _data.isEncrypted = true;
              return _data;
          }

          return {
              encrypt: function (text) {
                  return encryptInformation(angular.copy(text));
              },
              GetFileStructure: function (data) {
                  return $http.post(setAction('GetFileStructure'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              UpdateFolder: function (data) {
                  data.wsModel = encryptWSModel(data.wsModel);
                  return $http.post(setAction('UpdateFolder'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              AddFolder: function (data) {
                  data.wsModel = encryptWSModel(data.wsModel);
                  return $http.put(setAction('AddNewFolder'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              DeleteFolder: function (data) {
                  data.wsModel = encryptWSModel(data.wsModel);
                  return $http.patch(setAction('DeleteFolder'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              DeleteImage: function (data) {
                  data.idNumber = encryptInformation(angular.copy(data.idNumber));
                  return $http.delete(setAction('DeleteImage/[imageId]/[idNumber]', data, 'rest'), "json", "application/json; charset=utf-8");
              },
              UpdateImage: function (data) {
                  data.wsModel = encryptWSModel(data.wsModel);
                  return $http.post(setAction('UpdateImage'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetEmailTemplates: function (data) {
                  return $http.post(setAction('GetEmailTemplates'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              UpdateAddTemplate: function (data) {
                  data.wsModel = encryptWSModel(data.wsModel);
                  return $http.post(setAction('UpdateTemplate'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              DeleteTemplate: function (data) {
                  data.wsModel = encryptWSModel(data.wsModel);
                  return $http.patch(setAction('DeleteTemplate'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetHtmlTemplates: function (data) {
                  return $http.post(setAction('GetHtmlTemplates'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              GetPage: function (data) {
                  data.wsModel = encryptWSModel(data.wsModel);
                  return $http.post(setAction('GetPageById'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              }
          }

      }]);

})();