﻿(function() {
    angular.module('WorkerPortfolioApp').directive('ngmultiple', NgMultiple);

    NgMultiple.$inject = ['$window', '$rootScope'];
    
    function NgMultiple($window, $rootScope) {
        var directive = {
            link: link,
            restrict: 'A',
            require: "ngModel",
            scope: {},
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            $rootScope.lsBind($rootScope, '$$TofesModel', ngModel);

            element.on('change', function (e) {
                ngModel.$setValidity('custom', true);
            });
        }
    }

})();