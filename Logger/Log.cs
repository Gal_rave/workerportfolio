﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Threading;

namespace Logger
{
    public static class Log
    {
        private static bool IsWriting { get; set; }
        private static bool InProsses { get; set; }
        private static object locker = new object();
        private static object CycleStack_Lock = new object();
        private static List<LoadMessage> LogMessages = new List<LoadMessage>();
        private static readonly string BaseDirectory = "~\\Logs_Data\\";
        private static readonly string ErrorLogPath = HttpContext.Current.Server.MapPath(BaseDirectory + "ErrorLog_");
        private static readonly string LoadLogPath = HttpContext.Current.Server.MapPath(BaseDirectory + "LoadLog_");
        private static readonly string FormLogPath = HttpContext.Current.Server.MapPath(BaseDirectory + "FormLog_");
        private static readonly string ApplicationLogPath = HttpContext.Current.Server.MapPath(BaseDirectory + "ApplicationLog_");
        private static readonly string TestLogPath = HttpContext.Current.Server.MapPath(BaseDirectory + "TestLog_");
        private static readonly string PCULogPath = HttpContext.Current.Server.MapPath(BaseDirectory + "PCU_Log_");
        private static readonly string MailSender_LogPath = HttpContext.Current.Server.MapPath(BaseDirectory + "MailSender_Log_");
        private static Stack<LoadMessage> MessageStack = new Stack<LoadMessage>();

        public static void MailLog(object logMessage)
        {
            if (logMessage == null)
                return;

            MessageStack.Push(new LoadMessage(logMessage, "", MailSender_LogPath));
            CycleStack();
        }
        public static void MailLog(object logMessage, string source)
        {
            if (logMessage == null)
                return;

            MessageStack.Push(new LoadMessage(logMessage, source, MailSender_LogPath));
            CycleStack();
        }

        public static void PcuLog(object logMessage)
        {
            if (logMessage == null)
                return;

            MessageStack.Push(new LoadMessage(logMessage, "", PCULogPath));
            CycleStack();
        }
        public static void PcuLog(object logMessage, string source)
        {
            if (logMessage == null)
                return;

            MessageStack.Push(new LoadMessage(logMessage, source, PCULogPath));
            CycleStack();
        }

        public static void TestLog(object logMessage)
        {
            if (logMessage == null)
                return;

            MessageStack.Push(new LoadMessage(logMessage, "", TestLogPath));
            CycleStack();
        }
        public static void TestLog(object logMessage, string source)
        {
            if (logMessage == null)
                return;

            MessageStack.Push(new LoadMessage(logMessage, source, TestLogPath));
            CycleStack();
        }

        public static void ApplicationLog(object logMessage)
        {
            if (logMessage == null)
                return;

            MessageStack.Push(new LoadMessage(logMessage, "", ApplicationLogPath));
            CycleStack();
        }
        public static void ApplicationLog(object logMessage, string source)
        {
            if (logMessage == null)
                return;

            MessageStack.Push(new LoadMessage(logMessage, source, ApplicationLogPath));
            CycleStack();
        }

        public static void FormLog(object logMessage)
        {
            if (logMessage == null)
                return;

            MessageStack.Push(new LoadMessage(logMessage, "", FormLogPath));
            CycleStack();
        }
        public static void FormLog(object logMessage, string source)
        {
            if (logMessage == null)
                return;

            MessageStack.Push(new LoadMessage(logMessage, source, FormLogPath));
            CycleStack();
        }

        public static void LogLoad(object logMessage)
        {
            if (logMessage == null)
                return;

            MessageStack.Push(new LoadMessage(logMessage, "", LoadLogPath));
            CycleStack();
        }
        public static void LogLoad(object logMessage, string source)
        {
            if (logMessage == null)
                return;

            MessageStack.Push(new LoadMessage(logMessage, source, LoadLogPath));
            CycleStack();
        }

        public static void LogError(object logMessage)
        {
            if (logMessage == null)
                return;

            MessageStack.Push(new LoadMessage(logMessage, "", ErrorLogPath));
            CycleStack();
        }
        public static void LogError(object logMessage, string source)
        {
            if (logMessage == null)
                return;

            MessageStack.Push(new LoadMessage(logMessage, source, ErrorLogPath));
            CycleStack();
        }

        #region private helper functions
        private static void TaskLogError(object logMessage, string source)
        {
            if (logMessage == null)
                return;

            MessageStack.Push(new LoadMessage(logMessage, source, ErrorLogPath));
        }
        private static void CycleStack()
        {
            if (!Monitor.TryEnter(CycleStack_Lock, new TimeSpan(0)))
            {
                return;
            }
            try
            {
                lock (CycleStack_Lock)
                {
                    CreateFolderIfNeeded(BaseDirectory);

                    foreach (LoadMessage message in Pop())
                    {
                        while (InProsses)
                            Thread.Sleep(10);
                        WriteLog(message);
                        LogMessages.Remove(message);
                    }
                }
            }
            finally
            {
                Monitor.Exit(CycleStack_Lock);
            }
        }
        private static IEnumerable<LoadMessage> Pop()
        {
            while (MessageStack.Count > 0)
                yield return MessageStack.Pop();
        }
        private static void WriteLog(LoadMessage logMessage)
        {
            if (logMessage.Message.GetType() == typeof(string))
            {
                LogWrite(logMessage);
            }
            else
            {
                LogProp(logMessage);
            }
        }
        private static void LogProp(LoadMessage logMessage)
        {
            if (!Monitor.TryEnter(locker, new TimeSpan(0)))
            {
                Thread.Sleep(15);
                LogProp(logMessage);
            }
            try
            {
                lock (locker)
                {
                    DateTime date = DateTime.Now;
                    using (StreamWriter txtWriter = File.AppendText(string.IsNullOrWhiteSpace(logMessage.LogPath) ? LoadLogPath + date.ToString("yyyy-MM-dd") + ".txt" : logMessage.LogPath))
                    {
                        txtWriter.WriteLine("[{0} {1}] : {2}", logMessage.MessageTime.ToString("yyyy-MM-dd"),
                            logMessage.MessageTime.ToString("HH.mm.ss.ffffff"), logMessage.ExceptionSource);

                        logMessage.Message.GetType().GetProperties().ToList().ForEach(prop =>
                            txtWriter.WriteLine(prop.Name + ": " + prop.GetValue(logMessage.Message, null))
                            );

                        txtWriter.WriteLine("-------------------------------");
                    }
                }
            }
            catch (Exception e)
            {
                TaskLogError(e, "LogProp");
            }
            finally
            {
                Monitor.Exit(locker);
            }
        }
        private static void LogWrite(LoadMessage logMessage)
        {
            if (!Monitor.TryEnter(locker, new TimeSpan(0)))
            {
                Thread.Sleep(15);
                LogWrite(logMessage);
            }
            try
            {
                lock (locker)
                {
                    DateTime date = DateTime.Now;
                    using (StreamWriter txtWriter = File.AppendText(string.IsNullOrWhiteSpace(logMessage.LogPath) ? LoadLogPath + date.ToString("yyyy-MM-dd") + ".txt" : logMessage.LogPath))
                    {
                        txtWriter.Write("[{0} {1}] : {2}", logMessage.MessageTime.ToString("yyyy-MM-dd"),
                            logMessage.MessageTime.ToString("HH:mm:ss.ffffff"), logMessage.ExceptionSource);
                        txtWriter.WriteLine("  {0}", logMessage.Message);
                        txtWriter.WriteLine("-------------------------------");
                    }
                }
            }
            catch (Exception e)
            {
                TaskLogError(e, "LogWrite");
            }
            finally
            {
                Monitor.Exit(locker);
            }
        }
        private static IEnumerable<T> Shadow<T>(this IEnumerable<T> source) where T : new()
        {
            foreach (T item in source)
                yield return item;
        }
        private static bool CreateFolderIfNeeded(string path)
        {
            if (HttpContext.Current == null || HttpContext.Current.Server == null)
                return false;

            bool result = true;
            string MapPath = HttpContext.Current.Server.MapPath(path);

            if (!Directory.Exists(MapPath))
            {
                try
                {
                    Directory.CreateDirectory(MapPath);
                }
                catch
                {
                    result = false;
                }
            }
            return result;
        }
        #endregion  private helper functions
    }
}
