﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace Logger
{
    public static class ActionExceptionLogger
    {
        public static ExceptionResult ServerError(this ApiController controller, Exception exception)
        {
            Log.LogError(exception, string.Format("{0}.{1} [Exception.Type]-{2}", controller.ControllerContext.ControllerDescriptor.ControllerName, controller.ActionContext.ActionDescriptor.ActionName, exception.GetType().Name));

            //try
            //{
            //    var x = controller.ControllerContext.Request.Headers;
            //    Log.TestLog(x, "Headers");
            //}
            //catch { }           

            return new ExceptionResult(new Exception("500", exception), controller);
        }
        public static void ServerError(this Controller controller, Exception exception)
        {
            Log.LogError(exception, string.Format("{0}.{1} [Exception.Type]-{2}", controller.ControllerContext.RouteData.Values["controller"].ToString(), controller.ControllerContext.RouteData.Values["action"].ToString(), exception.GetType().Name));
        }

        public static Exception LogError(this Exception exception)
        {
            MethodBase mth = new StackTrace().GetFrame(1).GetMethod();
            Log.LogError(exception, string.Format("{0}.{1} [Exception.Type]-{2}", mth.ReflectedType.Name, mth.Name, exception.GetType().Name));

            return exception;
        }
        public static T LogError<T>(this Exception exception, T data)
        {
            MethodBase mth = new StackTrace().GetFrame(1).GetMethod();
            Log.LogError(exception, string.Format("{0}.{1} [Exception.Type]-{2}", mth.ReflectedType.Name, mth.Name, exception.GetType().Name));
            Log.LogError(data, string.Format("{0}.{1} [data.Type]-{2}", mth.ReflectedType.Name, mth.Name, data.GetType().Name));

            return data;
        }

        public static T LogError<T>(this T exception)
        {
            MethodBase mth = new StackTrace().GetFrame(1).GetMethod();
            Log.LogError(exception, string.Format("{0}.{1} [Exception.Type]-{2}", mth.ReflectedType.Name, mth.Name, exception.GetType().Name));

            return exception;
        }
        public static T LogError<T>(this T exception, T data)
        {
            MethodBase mth = new StackTrace().GetFrame(1).GetMethod();
            Log.LogError(exception, string.Format("{0}.{1} [Exception.Type]-{2}", mth.ReflectedType.Name, mth.Name, exception.GetType().Name));
            Log.LogError(data, string.Format("{0}.{1} [data.Type]-{2}", mth.ReflectedType.Name, mth.Name, data.GetType().Name));

            return data;
        }
    }
}
