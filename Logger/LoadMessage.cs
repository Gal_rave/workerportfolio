﻿using System;

namespace Logger
{
    public class LoadMessage
    {
        public LoadMessage() { }
        public LoadMessage(object message)
        {
            this.Message = message;
            this.MessageTime = DateTime.Now;
        }
        public LoadMessage(object message, string exceptionsource)
        {
            this.Message = message;
            this.ExceptionSource = exceptionsource;
            this.MessageTime = DateTime.Now;
        }
        public LoadMessage(string path, object message)
        {
            DateTime date = DateTime.Now;
            this.Message = message;
            this.MessageTime = DateTime.Now;
            this.LogPath = path + date.ToString("yyyy-MM-dd") + ".txt";
        }
        public LoadMessage(object message, string exceptionsource, string path)
        {
            DateTime date = DateTime.Now;
            this.Message = message;
            this.MessageTime = DateTime.Now;
            this.ExceptionSource = exceptionsource;
            this.LogPath = path + date.ToString("yyyy-MM-dd") + ".txt";
        }

        public object Message { get; set; }
        public DateTime MessageTime { get; set; }
        public string LogPath { get; set; }
        public string ExceptionSource { get; set; }
    }
}
