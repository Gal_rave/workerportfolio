﻿using System;
using System.Web;
using System.Web.Helpers;
using System.Web.Security;

namespace HttpCookieHandler
{
    public static class CookieExtender
    {
        internal static HttpContextBase Context
        {
            get { return new HttpContextWrapper(HttpContext.Current); }
        }
        internal static HttpRequestBase Request
        {
            get { return Context.Request; }
        }
        internal static HttpResponseBase Response
        {
            get { return Context.Response; }
        }

        public static string GetValue(string cookieName)
        {
            return GetCookie(cookieName).Value;
        }
        public static void SetValue(string cookieName, string value)
        {
            HttpCookie cookie = GetCookie(cookieName);
            cookie.Value = value;
            Response.Cookies.Add(cookie);
        }
        public static FormsAuthenticationTicket GetAuthenticationTicket(string cookieName)
        {
            string Value = GetValue(cookieName);
            if (string.IsNullOrWhiteSpace(Value))
            {
                return null;
            }
            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(Value);
            return authTicket;
        }
        public static int GetCurrentMunicipalityId(string idNumber)
        {
            FormsAuthenticationTicket authTicket = GetAuthenticationTicket(GetApplication("SecureMunicipalityCookieName").ToString());
            if (authTicket == null || string.IsNullOrWhiteSpace(authTicket.UserData) || idNumber != authTicket.Name)
            {
                return -1;
            }
            return authTicket.UserData.ToInt();
        }

        public static void SetCookie(string name, string value, DateTime expires, bool secure = false, bool hash = false)
        {
            HttpCookie c = GetCookie(name, hash);
            c.Value = value;
            c.Expires = expires;
            c.Secure = secure;
            Response.Cookies.Add(c);
        }
        public static void SetCookie(string name, string value, bool hash = false)
        {
            HttpCookie c = GetCookie(name, hash);
            c.Value = value;
            c.Expires = DateTime.Now.AddDays(1);
            c.Secure = false;
            Response.Cookies.Add(c);
        }

        public static void DeleteCookie(string cookieName)
        {
            DateTime now = DateTime.Now;
            ///get cookie
            HttpCookie RequestAuthCookie = Request.Cookies.Get(cookieName);
            HttpCookie ResponseAuthCookie = Response.Cookies.Get(cookieName);
            if (RequestAuthCookie != null)
            {
                RequestAuthCookie.Value = null;
                RequestAuthCookie.Expires = now.AddDays(-3);
                Request.Cookies.Add(RequestAuthCookie);
            }
            if (ResponseAuthCookie != null)
            {
                ResponseAuthCookie.Value = null;
                ResponseAuthCookie.Expires = now.AddDays(-3);
                Response.AppendCookie(ResponseAuthCookie);
            }
        }
        public static object GetApplication(string key)
        {
            return Context.Application.Get(key);
        }

        public static HttpCookie GetCookie(string cookieName, bool hash = false)
        {
            cookieName = hash ? Crypto.SHA256(cookieName) : cookieName;
            HttpCookie cookie = Request.Cookies.Get(cookieName);
            if (cookie == null)
                return new HttpCookie(cookieName);
            return cookie;
        }

        private static int ToInt(this object model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.ToString()))
                return 0;

            int num;
            if (Int32.TryParse(model.ToString(), out num))
                return num;
            else
                return 0;
        }
    }
}
