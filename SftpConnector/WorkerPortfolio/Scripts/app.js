(function () {

    angular.module('WorkerPortfolioApp', [
        // Angular modules 
        'ngRoute', 'ngSanitize', 'ngAnimate', 'ui.router', 'LocalStorageModule', 'ngDialog', 'ngResource', 'ui.select', 'ngCookies'

        // Custom modules 
        , 'TofesService', 'SearchService', 'LoginService', 'UserDataService', 'PayrollDataServise', 'AdminFactory', 'ProcessesService', 'PayCheckEmailFactory', 'FileManagerFactory', 'StatisticsFactory'

        // 3rd Party Modules
        , 'angularFileUpload', 'toastr', 'blockUI', 'wt.responsive', 'ngIdle', 'ngIdle.title', 'angucomplete-alt', 'ngFileSaver', 'ui.tinymce', 'adaptv.adaptStrap'

    ]).run(['$rootScope', '$templateCache', '$timeout', '$state', function ($rootScope, $templateCache, $timeout, $state) {
        ///bind anderscore
        $rootScope._ = _;
        ///add custom block ui template
        $templateCache.put('angular-block-ui/angular-block-ui.ng.ie.html', '<div class=\"block-ui-overlay\"></div><iframe class="block-ui-overlay-iframe" frameborder="0" tabindex="-1" src="" /><div class=\"block-ui-message-container\" aria-live=\"assertive\" aria-atomic=\"true\"><div class=\"block-ui-message\" ng-class=\"$_blockUiMessageClass\">{{ state.message }}</div></div>');
        ///return date in `HE` formt
        $rootScope.LocalDateString = function (date) {
            var pad = function (val) { return val < 10 ? ('0' + String(val)) : val; }
            return [pad(date.getMonth() + 1), pad(date.getDate()), date.getFullYear()].join('/');
        }
        ///get the prev month
        $rootScope.getMonthDate = function (lastYear) {
            var date = new Date();
            date.setMonth(date.getMonth() - 1);
            if (date.getDate() >= 10) {
                date.setDate(10);
            } else {
                date.setMonth(date.getMonth() - 1);
            }
            if (lastYear) {
                date = new Date();
                date.setFullYear(date.getFullYear() - 1);
                date.setMonth(11);
                date.setDate(22);
            }
            return date;
        }
        ///parse date into `mm/yyyy` string
        $rootScope.parseMonth = function (date) {
            function pad(v) { return v < 10 ? '0' + v : v }
            if (!(date instanceof Date) || date === 'Invalid Date')
                date = new Date();
            return [pad(date.getMonth() + 1), date.getFullYear()].join('/');
        }
        ///set post model data
        $rootScope.getWSPostModel = function (isMonthDate, islastYear, formatDate, isFromRouting, role) {
            var credentials = typeof $rootScope.$$credentials !== 'undefined' ? $rootScope.$$credentials : $rootScope.lsGet('$$credentials');
            var _selectedDate = isMonthDate === true ? $rootScope.getMonthDate(islastYear) : new Date();

            if (credentials.currentTerminationDate !== null)
                _selectedDate = new Date(credentials.currentTerminationDate);

            WSPostModel = {
                idNumber: credentials.idNumber,
                confirmationToken: credentials.confirmationToken,
                calculatedIdNumber: credentials.calculatedIdNumber,
                currentMunicipalityId: credentials.currentMunicipalityId,
                currentRashutId: credentials.currentRashutId,
                selectedDate: formatDate === true ? $rootScope.parseMonth(_selectedDate) : _selectedDate,
                municipalities: credentials.municipalities,
                isAdmin: credentials.isAdmin,
                isMobileRequest: (isFromRouting === true ? window.innerWidth < 801 : null),
                isEncrypted: false,
                requiredRole: role
            };

            return WSPostModel;
        }
        ///get current form 101 year
        $rootScope.getForm101Year = function () {
            var credentials = typeof $rootScope.$$credentials !== 'undefined' ? $rootScope.$$credentials : $rootScope.lsGet('$$credentials'),
                testDate = new Date(new Date().getFullYear(), 11, 31), currentDate = new Date();

            if (testDate < currentDate)
                currentDate.setFullYear((testDate.getFullYear() + 1));

            WSPostModel = {
                idNumber: credentials.idNumber,
                confirmationToken: credentials.confirmationToken,
                calculatedIdNumber: credentials.calculatedIdNumber,
                currentMunicipalityId: credentials.currentMunicipalityId,
                selectedDate: currentDate,
                updateUserData: false,
                currentRashutId: credentials.currentRashutId
            };

            return WSPostModel;
        };
        ///file uploader filter for 101 form
        $rootScope.FilterFiles = function (modelFiles, $$scopeFiles, origin) {
            $.map($$scopeFiles, function (file) {
                if (!file.isError && file.isSuccess && file.isUploaded) {
                    var obj = {};
                    $.map(file.formData, function (item) {
                        $.map(item, function (v, i) {
                            obj[i] = v;
                        });
                    });
                    obj.imageId = file.imageId;
                    obj.origin = origin;

                    var O_uniq = _.groupBy(modelFiles, 'imageId');
                    if (typeof O_uniq[obj.imageId] === 'undefined') {
                        modelFiles.push(obj);
                    }
                }
            });
        };
        ///$stateChangeError occurs when user changes current municipality, this is en error in the resolve
        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
            error.data = {};
            if(toState.controller ===  fromState.controller)
                $state.go('Home');
        });
    }]);

})();
(function () {
    
    angular.module('WorkerPortfolioApp').config(function (blockUIConfig) {
        // Change the default overlay message
        blockUIConfig.message = 'מעבד נתונים ...';
        // Change the default delay to 100ms before the blocking is visible
        blockUIConfig.delay = 0;
        // Apply these classes to al block-ui elements
        blockUIConfig.cssClass = 'block-ui block-ui-anim-fade block-ui-overlay-dark';
        ///set custom block template
        blockUIConfig.templateUrl = 'angular-block-ui/angular-block-ui.ng.ie.html';
        // Change the displayed message based on the http verbs being used.    
        blockUIConfig.requestFilter = function (config) {
            if (config.headers["X-Search"] == String(1) || config.XSearch)
            {
                return false;
            }
        };
    });

})();
(function() {    

    angular.module('WorkerPortfolioApp').config(function ($httpProvider, $provide) {
        $provide.factory('httpInterceptor', function ($q, $rootScope) {
            return {
                'request': function (config) {
                    config.$canceller = $q.defer();
                    config.timeout = config.$canceller.promise;
                    return config;
                },
                'requestError': function (rejection) {
                    //console.log('requestError', rejection);
                    if (rejection.status < 0) return void [0];
                    return $q.reject(rejection);
                },
                'responseError': function (rejection) {
                    //console.log('responseError', rejection);
                    ///remove sensitive data from error response
                    if (rejection.data !== null && rejection.data.StackTrace !== null) rejection.data.StackTrace = {};
                    if (rejection.config !== null && rejection.config.data !== null) rejection.config.data = {};

                    ///custom rejection status
                    if (rejection.status == 406)
                        return rejection;
                    ///custom CryptographicException rejection status
                    if (rejection.status >= 511) {
                        $rootScope.publish('511_responseError', rejection.status);
                        return $q.reject(rejection);
                    }

                    if (rejection.status === 401 || (rejection.data != null && rejection.data.ExceptionMessage != null && rejection.data.ExceptionMessage === "UserdetailsAccessException" )) {
                        ///logout
                        $rootScope.publish('CatchTransmitLogout');
                    }
                    if (rejection.status < 0) return void [0];

                    return $q.reject(rejection);
                },
                'response': function (response) {
                    //console.log('response', response);
                    // Return the response or promise.
                    return response || $q.when(response);
                }
            };
        });

        $httpProvider.interceptors.push('httpInterceptor');
    });
    
})();
(function () {

    angular.module('WorkerPortfolioApp').config(['KeepaliveProvider', 'IdleProvider', function (KeepaliveProvider, IdleProvider) {
        ///for localhost set longer parameters
        var mul = location.hostname === "localhost" ? 1000 : 1;
        ///set the allowd idle time [5m]
        IdleProvider.idle(parseInt(300 * mul)); // in seconds
        ///set the worning timeout [45s]
        IdleProvider.timeout(parseInt(10 * mul)); // in seconds
        ///run check for login state every [1m]
        KeepaliveProvider.interval(parseInt(10 * mul)); // in seconds
    }]);

})();
(function () {
    
    angular.module('WorkerPortfolioApp').config(function (localStorageServiceProvider) {
        localStorageServiceProvider
            .setNotify(true, true);
    });

})();
(function () {

    angular.module('WorkerPortfolioApp').config(function (ngDialogProvider) {
        ngDialogProvider.setOpenOnePerName(true);
    });

})();
(function () {    
    String.prototype.isempty = function () {
        return this === null || this === undefined || typeof this === 'undefined' || this.length < 1;
    };
    Number.prototype.isempty = function () {
        return this === null || this === undefined || typeof this === 'undefined' || this.length < 1;
    };
    Number.prototype.roundTenUp = function () {
        return this % 10 === 0 ? parseInt(this) : ((parseInt(this / 10, 10) + 1) * 10);
    };
})();
(function () {
    /* 
    *** 
    general site permissions
    100 - DEVELOPER
    80 - SITEADMIN
    50 - ADMIN

    custom models permissions
    43 - SYSTEMSEMAILROLE
    42 - MUNICIPALITYMENUROLE
    41 - EMAILPAYCHECKROLE
    ***
    */
    angular.module('WorkerPortfolioApp').config(['$locationProvider', '$stateProvider', '$urlRouterProvider', function ($locationProvider, $stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            /*errors*/
            .state('Error', {
                url: "/error",
                templateUrl: "../../Scripts/views/error.html",
                controller: "ErrorController",
                resolve: {
                    SiteMail: ['UserService', function (UserService) {
                        return UserService.GetSiteMail();
                    }]
                },
                data: {
                    type: null,
                    name: 'שגיאת מערכת',
                    RequiresAuthentication: false,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('UserAuthenticationError', {
                url: "/error/{AuthenticationErrorToken:[0-9A-Za-z]{1,125}}",
                templateUrl: "../../Scripts/views/error.html",
                controller: "ErrorController",
                resolve: {
                    SiteMail: ['UserService', function (UserService) {
                        return UserService.GetSiteMail();
                    }]
                },
                data: {
                    type: null,
                    name: 'שגיאת אישור משתמש',
                    RequiresAuthentication: false,
                    disabled: false,
                    immunity: 0
                }
            })
            /*end errors*/

            /*home page*/
            .state('Home', {
                url: "/",
                templateUrl: "../../Scripts/views/Home.html",
                controller: "HomeController",
                data: {
                    type: null,                         /// master type on side menu, to what article enter this item
                    name: 'ברוכים הבאים',             /// side menu title
                    RequiresAuthentication: false,     /// if nead to be loged in
                    disabled: false,                   /// if to show in side menu if have this article main type
                    immunity: 0
                }
            })
            /*end home page*/

            /*site admin*/
            .state('Admin', {
                abstract: true,
                url: '/admin',
                template: '<div ui-view/>',
                location: 0,
                faIcon: 'fa-desktop fa-lg',
                data: {
                    type: 'Admin',
                    name: 'ניהול מערכת',
                    RequiresAuthentication: true,
                    disabled: true,
                    ShowAnyway: false,
                    color: 'admin',//008482
                    immunity: 50
                }
            })
            .state('Admin.SearchUsers', {
                url: "/search",
                templateUrl: "../../Scripts/views/admin.html",
                controller: "AdminController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'ADMIN'));
                    }],
                    $user: [function () {
                        return {};
                    }],
                    $passwordSettings: [function () {
                        return {};
                    }],
                    $groups: ['Admin', function (Admin) {
                        return {};
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'חיפוש משתמשים',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 50
                }
            })
            .state('Admin.EditUser', {
                url: "/show-user/{UserID}",
                templateUrl: "../../Scripts/views/Forms/admin.show-user.html",
                controller: "AdminController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'ADMIN'));
                    }],
                    $user: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'ADMIN'),
                            UserSearch = {
                                idNumber: WSPostModel.idNumber,
                                confirmationToken: WSPostModel.confirmationToken,
                                page: $stateParams.UserID,
                                searchIdNumber: null,
                                cell: null,
                                firstName: null,
                                lastName: null,
                                requiredRole: WSPostModel.requiredRole
                            };

                        return Admin.GetUserById(UserSearch);
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }],
                    $groups: ['Admin', function (Admin) {
                        return Admin.GetGroups($rootScope.getWSPostModel(null, null, null, null, 'ADMIN'));
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'צפיה במשתמש',
                    RequiresAuthentication: true,
                    disabled: true,
                    immunity: 50
                }
            })
            .state('Admin.ResetPassword', {
                url: "/reset-password/{IdNUmber}",
                templateUrl: "../../Scripts/views/Forms/admin.reset-password.html",
                controller: "AdminController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'ADMIN'));
                    }],
                    $user: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        return {};
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }],
                    $groups: ['Admin', function (Admin) {
                        return {};
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'איפוס סיסמה',
                    RequiresAuthentication: true,
                    disabled: true,
                    immunity: 50
                }
            })
            .state('Admin.MunicipalitiesSetUp', {
                url: "/municipalities-setup",
                templateUrl: "../../Scripts/views/Forms/admin.municipalities-setup.html",
                controller: "AdminMunicipalitiesController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'));
                    }],
                    municipalities: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetMunicipalities(UserSearch);
                    }],
                    $municipality: [function () {
                        return null;
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'ניהול ארגונים',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 70
                }
            })
            .state('Admin.EditMunicipality', {
                url: "/edit-municipality/{MunicipalityId}",
                templateUrl: "../../Scripts/views/Forms/admin.show-municipality.html",
                controller: "AdminMunicipalitiesController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'));
                    }],
                    municipalities: [function () {
                        return {};
                    }],
                    $municipality: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'),
                            UserSearch = {
                                idNumber: WSPostModel.idNumber,
                                confirmationToken: WSPostModel.confirmationToken,
                                page: $stateParams.MunicipalityId,
                                searchIdNumber: null,
                                cell: null,
                                firstName: null,
                                lastName: null,
                                requiredRole: WSPostModel.requiredRole
                            };
                        return Admin.GetSingleMunicipaly(UserSearch);
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'ניהול ארגונים',
                    RequiresAuthentication: true,
                    disabled: true,
                    immunity: 70
                }
            })
            /*end site admin*/

            /*site statistics*/
            .state('Statistics', {
                abstract: true,
                url: '/statistics',
                template: '<div ui-view/>',
                data: {
                    type: 'Admin',
                    name: 'סטטיסטיקות שימוש',
                    RequiresAuthentication: true,
                    disabled: true,
                    ShowAnyway: false,
                    color: 'admin',//008482
                    immunity: 41
                }
            })
            .state('Statistics.EmailPaycheck', {
                url: "/email-paycheck",
                templateUrl: "../../Scripts/views/statistics.email-paycheck.html",
                controller: "StatisticsController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'EMAILPAYCHECKROLE'));
                    }],
                    municipalities: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'EMAILPAYCHECKROLE'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetMunicipalities(UserSearch);
                    }],
                    statistics: [function () {
                        return new Array();
                    }],
                    $controllerState: [function () {
                        return 'EmailPaycheck';
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'תלוש במייל',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 41
                }
            })
            .state('Statistics.101Form', {
                url: "/form-101",
                templateUrl: "../../Scripts/views/statistics.email-paycheck.html",
                controller: "StatisticsController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'));
                    }],
                    municipalities: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetMunicipalities(UserSearch);
                    }],
                    statistics: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetForm101Statistics(UserSearch);
                    }],
                    $controllerState: [function () {
                        return '101Form';
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'טופס 101',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 70
                }
            })
            .state('Statistics.LogInLog', {
                url: "/log-in-log",
                templateUrl: "../../Scripts/views/statistics.email-paycheck.html",
                controller: "StatisticsController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'));
                    }],
                    municipalities: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetMunicipalities(UserSearch);
                    }],
                    statistics: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetLogInLogStatistics(UserSearch);
                    }],
                    $controllerState: [function () {
                        return 'LogInLog';
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'כניסות לארגון',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 70
                }
            })
            .state('Statistics.DailyUsage', {
                url: "/daily-usage",
                templateUrl: "../../Scripts/views/statistics.email-paycheck.html",
                controller: "StatisticsController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'));
                    }],
                    municipalities: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SITEADMIN'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetMunicipalities(UserSearch);
                    }],
                    statistics: [function () {
                        return new Array();
                    }],
                    $controllerState: [function () {
                        return 'DailyUsage';
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'כניסות לתאריך',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 70
                }
            })
            .state('Statistics.MunicipalityFormStatistics', {
                url: "/municipality-statistics",
                templateUrl: "../../Scripts/views/Municipality.Statistics.html",
                controller: "MunicipalityStatisticsController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'FORMSTATISTICSROLE'));
                    }],
                    municipalities: ['Admin', '$stateParams', function (Admin, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'FORMSTATISTICSROLE'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole
                        };
                        return Admin.GetMunicipalities(UserSearch);
                    }],
                    statistics: ['Statistics', '$stateParams', function (Statistics, $stateParams) {
                        var WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'FORMSTATISTICSROLE'),
                        UserSearch = {
                            idNumber: WSPostModel.idNumber,
                            confirmationToken: WSPostModel.confirmationToken,
                            page: 1,
                            hidden_value: false,
                            $hidden_value: '',
                            searchIdNumber: null,
                            cell: null,
                            firstName: null,
                            lastName: null,
                            password: null,
                            newPassword: null,
                            reEnterPassword: null,
                            municipalities: WSPostModel.municipalities.map(function (elem) {
                                return elem.ID;
                            }),
                            userToken: null,
                            searchMunicipalityName: null,
                            searchMunicipalitiesId: null,
                            requiredRole: WSPostModel.requiredRole,
                            searchDate: new Date()
                        };
                        return Statistics.MunicipalityStatistics(UserSearch);
                    }]
                },
                data: {
                    type: 'Admin',
                    name: 'דו`ח 101',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 44
                }
            })            
            /*end site statistics*/

            /*site Systems*/
            .state('Systems', {
                abstract: true,
                url: '/system',
                template: '<div ui-view/>',
                location: 1,
                faIcon: 'fa-desktop fa-lg',
                data: {
                    type: 'Systems',
                    name: 'כלי עבודה',
                    RequiresAuthentication: true,
                    disabled: true,
                    ShowAnyway: false,
                    color: 'admin',//008482
                    immunity: 42
                }
            })
            .state('Systems.Email-Templates', {
                url: "/email-templates",
                templateUrl: "../../Scripts/views/Email-Templates/admin.email-templates.html",
                controller: "SystemsEmailTemplatesController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'SYSTEMSEMAILROLE'));
                    }],
                    Templates: ['FileManager', '$rootScope', function (FileManager, $rootScope) {
                        return FileManager.GetEmailTemplates($rootScope.getWSPostModel(null, null, null, null, 'SYSTEMSEMAILROLE'));
                    }]
                },
                data: {
                    type: 'Systems',
                    name: 'תבניות מייל',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 43
                }
            })
            .state('Systems.Municipality-Pages', {
                url: "/municipality-pages",
                templateUrl: "../../Scripts/views/Email-Templates/admin.municipality-pages.html",
                controller: "MunicipalityPagesController",
                resolve: {
                    ValidAdminUser: ['Admin', '$rootScope', function (Admin, $rootScope) {
                        return Admin.ValidUser($rootScope.getWSPostModel(null, null, null, null, 'MUNICIPALITYMENUROLE'));
                    }],
                    Templates: ['FileManager', '$rootScope', function (FileManager, $rootScope) {
                        return FileManager.GetHtmlTemplates($rootScope.getWSPostModel(null, null, null, null, 'MUNICIPALITYMENUROLE'));
                    }]
                },
                data: {
                    type: 'Systems',
                    name: 'עמודי אירגון',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 42
                }
            })
            /*end site Systems*/

            /*site user authentication*/
            .state('Authentication', {
                abstract: true,
                url: '/authentication',
                template: '<div ui-view/>',
                data: {
                    type: null,
                    name: 'אימות פרטים',
                    RequiresAuthentication: false,
                    disabled: false,
                    color: 'reddish',
                    immunity: 0
                }
            })
            .state('Authentication.PasswordReset', {
                url: "/reset-password/{Token:[0-9A-Za-z-_=]{1,225}}",
                templateUrl: "../../Scripts/views/Forms/user.authentication.reset-password.html",
                controller: "AuthenticationController",
                resolve: {
                    $controllerState: [function () {
                        return 'PasswordReset';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: null,
                    name: 'איפוס סיסמה',
                    RequiresAuthentication: false,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Authentication.ResetPasswordFromSms', {
                url: "/sms-reset-password",
                templateUrl: "../../Scripts/views/Forms/user.authentication.reset-password.html",
                controller: "AuthenticationController",
                params: {
                    idNumber: null
                },
                resolve: {
                    $controllerState: [function () {
                        return 'SMSReset';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: null,
                    name: 'איפוס סיסמה',
                    RequiresAuthentication: false,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Authentication.AuthenticatedUserLogin', {
                url: "/authenticateduserlogin/{AuthenticationSuccessToken:[0-9A-Za-z]{1,125}}",
                templateUrl: "../../Scripts/views/user.authentication.html",
                controller: "AuthenticationController",
                resolve: {
                    $controllerState: [function () {
                        return 'AuthenticatedUserLogin';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: null,
                    name: 'אישור משתמש',
                    RequiresAuthentication: false,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Authentication.LoginToProcess', {
                url: "/logintoprocess/{AuthenticationSuccessToken:[0-9A-Za-z]{1,125}}/{rashut}/{mncplity}",
                templateUrl: "../../Scripts/views/user.authentication.html",
                controller: "AuthenticationController",
                resolve: {
                    $controllerState: [function () {
                        return 'LoginToProcess';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: null,
                    name: 'אישור משתמש',
                    RequiresAuthentication: false,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Authentication.UserRegister', {
                url: "/user-register",
                templateUrl: "../../Scripts/Views/Forms/user.authentication.register.html",
                controller: "AuthenticationController",
                resolve: {
                    $controllerState: [function () {
                        return 'UserRegister';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: null,
                    name: 'הרשמה לאתר',
                    RequiresAuthentication: false,
                    disabled: true,
                    immunity: 0
                }
            })
            .state('Authentication.InitialActivation', {
                url: "/initial-activation",
                templateUrl: "../../Scripts/Views/Forms/user.initial.activation.html",
                controller: "AuthenticationController",
                resolve: {
                    $controllerState: [function () {
                        return 'InitialActivation';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: null,
                    name: 'הרשמה לאתר',
                    RequiresAuthentication: false,
                    disabled: true,
                    immunity: 0
                }
            })
            /*end site user authentication*/

            /*user details update*/
            .state('UserDetails', {
                abstract: true,
                url: '/details',
                template: '<div ui-view/>',
                location: 2,
                faIcon: 'fa-user fa-lg',
                data: {
                    type: 'UserDetails',
                    name: 'פרטי עובד',
                    RequiresAuthentication: true,
                    disabled: false,
                    ShowAnyway: true,
                    color: 'reddish',
                    immunity: 0
                }
            })
            .state('UserDetails.UserDetails', {
                url: "/user",
                templateUrl: "../../Scripts/views/user.details.html",
                controller: "UserDetailsController",
                resolve: {
                    $controllerState: [function () {
                        return 'UpdateUser';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }],
                    $personalDetails: ['Credentials', '$rootScope', function (Credentials, $rootScope) {
                        return Credentials.GetUserdetails($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'UserDetails',
                    name: 'פרטים אישיים',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('UserDetails.Partner', {
                url: "/partner",
                templateUrl: "../../Scripts/views/user.details.html",
                controller: "UserDetailsController",
                resolve: {
                    $controllerState: [function () {
                        return 'UpdatePartner';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }],
                    $personalDetails: ['Credentials', '$rootScope', function (Credentials, $rootScope) {
                        return Credentials.GetUserPartnerDetails($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'UserDetails',
                    name: 'פרטי בן/בת זוג',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('UserDetails.Education', {
                url: "/education",
                templateUrl: "../../Scripts/views/user.details.html",
                controller: "UserDetailsController",
                resolve: {
                    $controllerState: [function () {
                        return 'UpdateEducation';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }],
                    $personalDetails: ['Credentials', '$rootScope', function (Credentials, $rootScope) {
                        return Credentials.GetEducationStatus($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'UserDetails',
                    name: 'השכלה',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('UserDetails.Children', {
                url: "/children",
                templateUrl: "../../Scripts/views/user.details.html",
                controller: "UserDetailsController",
                resolve: {
                    $controllerState: [function () {
                        return 'UpdateChildren';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }],
                    $personalDetails: ['Credentials', '$rootScope', function (Credentials, $rootScope) {
                        return Credentials.GetUserChildren($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'UserDetails',
                    name: 'ילדים',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            /*end user details update*/

            /*payroll data*/
            .state('Payroll', {
                abstract: true,
                url: '/payroll',
                template: '<div ui-view/>',
                location: 3,
                faIcon: 'fa-wpforms fa-lg',
                data: {
                    type: 'Payroll',
                    name: 'שכר',
                    RequiresAuthentication: true,
                    disabled: false,
                    ShowAnyway: true,
                    color: 'orange',
                    immunity: 0
                }
            })
            .state('Payroll.Paycheck', {
                url: "/paycheck",
                templateUrl: "../../Scripts/views/payroll.section.html",
                controller: "PayrollController",
                resolve: {
                    $controllerState: [function () {
                        return 'Paycheck';
                    }],
                    PayrollData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.GetPaycheck($rootScope.getWSPostModel(true, null, null, true));
                    }]
                },
                data: {
                    type: 'Payroll',
                    name: 'תלוש שכר',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Payroll.106Form', {
                url: "/form-106",
                templateUrl: "../../Scripts/views/payroll.section.html",
                controller: "PayrollController",
                resolve: {
                    $controllerState: [function () {
                        return '106Form';
                    }],
                    PayrollData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.Get106Form($rootScope.getWSPostModel(true, true, null, true));
                    }]
                },
                data: {
                    type: 'Payroll',
                    name: 'טופס 106',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Payroll.GeneralWagesData', {
                url: "/wages-data",
                templateUrl: "../../Scripts/views/payroll.section.html",
                controller: "PayrollController",
                resolve: {
                    $controllerState: [function () {
                        return 'GeneralWagesData';
                    }],
                    PayrollData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.GeneralPayrollData($rootScope.getWSPostModel(true));
                    }]
                },
                data: {
                    type: 'Payroll',
                    name: 'נתוני שכר כללי',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Payroll.TrainingAndProvidentFunds', {
                url: "/training-and-provident-funds",
                templateUrl: "../../Scripts/views/payroll.section.html",
                controller: "PayrollController",
                resolve: {
                    $controllerState: [function () {
                        return 'TrainingAndProvidentFunds';
                    }],
                    PayrollData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.GetFundsTable($rootScope.getWSPostModel(true));
                    }]
                },
                data: {
                    type: 'Payroll',
                    name: 'ריכוז הפרשות לגמל והשתלמות',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Payroll.PayrollBySymbol', {
                url: "/payroll-by-symbol",
                templateUrl: "../../Scripts/views/payroll.section.html",
                controller: "PayrollController",
                resolve: {
                    $controllerState: [function () {
                        return 'PayrollBySymbol';
                    }],
                    PayrollData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.PayrollBySymbol($rootScope.getWSPostModel(true));
                    }]
                },
                data: {
                    type: 'Payroll',
                    name: 'פירוט תשלומים וניכויים',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            /*end payroll data*/

            /*attendance data*/
            .state('Attendance', {
                abstract: true,
                url: '/attendance',
                template: '<div ui-view/>',
                location: 4,
                faIcon: 'fa-pencil fa-lg',
                data: {
                    type: 'Attendance',
                    name: 'נוכחות',
                    RequiresAuthentication: true,
                    disabled: false,
                    ShowAnyway: true,
                    color: 'green',
                    immunity: 0
                }
            })
            .state('Attendance.Vacation', {
                url: "/vacation",
                templateUrl: "../../Scripts/views/attendance.section.html",
                controller: "AttendanceController",
                resolve: {
                    $controllerState: [function () {
                        return 'Vacation';
                    }],
                    AttendanceData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.GetAttendanceFigures($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'Attendance',
                    name: 'נתוני חופשה',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Attendance.SickLeave', {
                url: "/sick-leave",
                templateUrl: "../../Scripts/views/attendance.section.html",
                controller: "AttendanceController",
                resolve: {
                    $controllerState: [function () {
                        return 'SickLeave';
                    }],
                    AttendanceData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.GetAttendanceFigures($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'Attendance',
                    name: 'נתוני מחלה',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Attendance.AdvancedStudy', {
                url: "/advanced-study",
                templateUrl: "../../Scripts/views/attendance.section.html",
                controller: "AttendanceController",
                resolve: {
                    $controllerState: [function () {
                        return 'AdvancedStudy';
                    }],
                    AttendanceData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.GetAttendanceFigures($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'Attendance',
                    name: 'נתוני השתלמות',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('Attendance.AttendanceSheet', {
                url: "/",
                template: "",
                controller: function () {
                    ///empty rout used only to redirect to new tab
                },
                data: {
                    type: 'Attendance',
                    name: 'גיליון נוכחות',
                    RequiresAuthentication: true,
                    disabled: false,
                    replaceUrl: 'Home/SynerionRedirect',
                    immunity: 0
                }
            })
            /*end attendance data*/

            /*user menu*/
            .state('UserMenu', {
                abstract: true,
                url: '/usermenu',
                template: '<div ui-view/>',
                location: 5,
                faIcon: 'fa-envelope-o fa-lg',
                data: {
                    type: 'UserMenu',
                    name: 'תפריט משתמש',
                    RequiresAuthentication: false,
                    disabled: false,
                    ShowAnyway: true,
                    color: 'azure',
                    immunity: 0
                }
            })
            .state('UserMenu.UPCEStatus', {
                url: "/update-upce",
                templateUrl: "../../Scripts/views/email.paycheck.html",
                controller: "UserEmailPayCheckController",
                resolve: {
                    UPCData: ['Credentials', function (Credentials) {
                        return Credentials.GetUPCData($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: 'תלוש במייל',
                    RequiresAuthentication: true,
                    disabled: false,
                    isAllowEPC: true,
                    immunity: 0
                }
            })
            .state('UserMenu.UpdateContactDetails', {
                url: "/update-contact-details",
                templateUrl: "../../Scripts/views/update.contact-details.html",
                controller: "UpdateContactDetailsController",
                resolve: {
                    UpdateTypes: ['Credentials', function (Credentials) {
                        return Credentials.GetUserUpdateTypes($rootScope.getWSPostModel());
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: 'עדכון פרטי התקשרות',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })

            /*user menu => processes*/
            .state('UserMenu.Processes', {
                url: "/processes",
                templateUrl: "../../Scripts/views/processes.html",
                controller: "ProcessesController",
                resolve: {
                    ProcessesData: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getRashutProcessesData($rootScope.getWSPostModel());
                    }],
                    VacationData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return PayrollServise.GetAttendanceFigures($rootScope.getWSPostModel());
                    }],
                    AcademicInst: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getAcademicInsts();
                    }],
                    YehidaIrgunit: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getYehidaIrgunit($rootScope.getWSPostModel());
                    }],
                    //Empty:
                    MyProcesses: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }],
                    MyRequestProcesses: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }],
                    UsersList: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: 'הגשת בקשה חדשה',
                    RequiresAuthentication: true,
                    disabled: false,
                    isAllowProcess: true,
                    immunity: 0
                }
            })
            .state('UserMenu.MyProcesses', {
                url: "/my-processes",
                templateUrl: "../../Scripts/views/my-processes.html",
                controller: "ProcessesController",
                resolve: {
                    MyProcesses: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getUserProcesses($rootScope.getWSPostModel());
                    }],
                    ProcessesData: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getRashutProcessesData($rootScope.getWSPostModel());
                    }],
                    YehidaIrgunit: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getYehidaIrgunit($rootScope.getWSPostModel());
                    }],
                    UsersList: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getUsers($rootScope.getWSPostModel());
                    }],
                    //Empty:
                    MyRequestProcesses: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }],
                    VacationData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return { data: {} };
                    }],
                    AcademicInst: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: 'בקשות שהופנו אליי',
                    RequiresAuthentication: true,
                    disabled: false,
                    isAllowProcess: true,
                    immunity: 0
                }
            })
            .state('UserMenu.MyRequestProcesses', {
                url: "/myrequestprocesses",
                templateUrl: "../../Scripts/views/my-request-processes.html",
                controller: "ProcessesController",
                resolve: {
                    MyRequestProcesses: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return Processes.getUserRequestProcesses($rootScope.getWSPostModel());
                    }],
                    //Empty:
                    MyProcesses: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }],
                    ProcessesData: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }],
                    UsersList: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }],
                    VacationData: ['PayrollServise', '$rootScope', function (PayrollServise, $rootScope) {
                        return { data: {} };
                    }],
                    AcademicInst: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }],
                    YehidaIrgunit: ['Processes', '$rootScope', function (Processes, $rootScope) {
                        return { data: {} };
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: 'הבקשות שלי',
                    RequiresAuthentication: true,
                    disabled: false,
                    isAllowProcess: true,
                    immunity: 0
                }
            })
            /*user menu => processes*/

            .state('UserMenu.ResetCurrentPassword', {
                url: "/reset-current-password",
                templateUrl: "../../Scripts/views/usermenu.section.html",
                controller: "UserMenuController",
                resolve: {
                    $controllerState: [function () {
                        return 'ResetCurrentPassword';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: 'עדכון סיסמה',
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })
            .state('UserMenu.HumanResourcesNotify', {
                url: "/notify-human-resources",
                templateUrl: "../../Scripts/views/usermenu.section.html",
                controller: "UserMenuController",
                resolve: {
                    $controllerState: [function () {
                        return 'HumanResourcesNotify';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return Credentials.GetLocalPasswordSettings();
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: "הודעה למח' שכר/משאבי אנוש",
                    RequiresAuthentication: true,
                    disabled: false,
                    immunity: 0
                }
            })

            /*keep as last item in `user menu` menu*/
            .state('UserMenu.ContactUs', {
                url: "/contact-us",
                templateUrl: "../../Scripts/views/usermenu.section.html",
                controller: "UserMenuController",
                resolve: {
                    $controllerState: [function () {
                        return 'ContactUs';
                    }],
                    $passwordSettings: ['Credentials', function (Credentials) {
                        return {};
                    }]
                },
                data: {
                    type: 'UserMenu',
                    name: 'צור קשר',
                    RequiresAuthentication: false,
                    disabled: false,
                    CustomDisabled: true,
                    immunity: 0
                }
            })
            /*end user menu*/

            /*custom municipality pages*/
            .state('CustomPages', {
                url: "/pages/{PageID:[0-9]{1,10}}",
                templateUrl: "../../Scripts/views/custom-pages.html",
                controller: "CustomPagesController",
                resolve: {
                    $Page: ['FileManager', '$stateParams', function (FileManager, $stateParams) {
                        return FileManager.GetPage({
                            wsModel: $rootScope.getWSPostModel(), template: {
                                ID: $stateParams.PageID,
                                municipalityId: $rootScope.getWSPostModel().currentMunicipalityId
                            }
                        });
                    }]
                },
                data: {
                    type: 'Custom',
                    name: 'דפים',
                    RequiresAuthentication: true,
                    disabled: false,
                    isAllowMunicipalityMenu: true,
                    immunity: 0
                }
            })
            /*end custom municipality pages*/

            /*101 form*/
            .state('Form', {
                abstract: true,
                url: '/form',
                template: '<div ui-view/>',
                data: {
                    type: 'Form101',
                    name: 'טופס 101',
                    RequiresAuthentication: true,
                    disabled: true,
                    color: 'purple',
                    immunity: 0
                }
            })
            /* first step - get the start up data from the server */
            .state('Form.employer-employee', {
                url: "/employee-details",
                templateUrl: "../../Scripts/views/employer-employee.details.html",
                controller: "TofesController",
                title: 'פרטי עובד',
                resolve: {
                    TofesModel: ['TofesDataService', function (TofesDataService) {
                        return TofesDataService.GetTofesControllerData();
                    }],
                    $Tofes: ['Tofes', function (Tofes) {
                        return Tofes.GetFormData($rootScope.getForm101Year());
                    }]
                },
                data: {
                    type: 'Form101',
                    name: 'פרטי עובד',
                    RequiresAuthentication: true,
                    disabled: false,
                    formType: 0,
                    immunity: 0
                }
            })
            /* second step - family */
            .state('Form.family-details', {
                url: "/family-details",
                templateUrl: "../../Scripts/views/partner-children.details.html",
                controller: "FamilyDetailsController",
                title: 'פרטי משפחה',
                resolve: {
                    TofesModel: ['TofesDataService', function (TofesDataService) {
                        return TofesDataService.GetTofesControllerData();
                    }]
                },
                data: {
                    type: 'Form101',
                    name: 'פרטי משפחה',
                    RequiresAuthentication: true,
                    disabled: false,
                    formType: 0,
                    immunity: 0
                }
            })
            /* third step - income */
            .state('Form.employer-other', {
                url: "/income",
                templateUrl: "../../Scripts/views/employer-other.income.html",
                controller: "IncomeController",
                title: 'הכנסות',
                resolve: {
                    TofesModel: ['TofesDataService', function (TofesDataService) {
                        return TofesDataService.GetTofesControllerData();
                    }]
                },
                data: {
                    type: 'Form101',
                    name: 'הכנסות',
                    RequiresAuthentication: true,
                    disabled: false,
                    formType: 2,
                    immunity: 0
                }
            })
            /* fourth step - tax-exemption */
            .state('Form.tax-exemption', {
                url: "/tax-exemption",
                templateUrl: "../../Scripts/views/tax.exemption.html",
                controller: "TaxExemptionController",
                title: 'זיכוי מס',
                resolve: {
                    TofesModel: ['TofesDataService', function (TofesDataService) {
                        return TofesDataService.GetTofesControllerData();
                    }]
                },
                data: {
                    type: 'Form101',
                    name: 'זיכוי מס',
                    RequiresAuthentication: true,
                    disabled: false,
                    formType: 0,
                    immunity: 0
                }
            })
            /* fifth step - tax-adjustment */
            .state('Form.tax-adjustment', {
                url: "/tax-adjustment",
                templateUrl: "../../Scripts/views/tax.adjustment.html",
                controller: "TaxAdjustmentController",
                title: 'תאום מס',
                resolve: {
                    TofesModel: ['TofesDataService', function (TofesDataService) {
                        return TofesDataService.GetTofesControllerData();
                    }]
                },
                data: {
                    type: 'Form101',
                    name: 'תאום מס',
                    RequiresAuthentication: true,
                    disabled: false,
                    formType: 2,
                    immunity: 0
                }
            })
            /* alternative step for `Retired` form in place of steps 3,5 */
            .state('Form.Other-Income-Adjustment', {
                url: "/other-income-adjustment",
                templateUrl: "../../Scripts/views/Other.Income-Adjustment.html",
                controller: "OtherIncomeAdjustmentController",
                title: 'הכנסות אחרות',
                resolve: {
                    TofesModel: ['TofesDataService', function (TofesDataService) {
                        return TofesDataService.GetTofesControllerData();
                    }]
                },
                data: {
                    type: 'Form101',
                    name: 'תאום מס',
                    RequiresAuthentication: true,
                    disabled: false,
                    formType: 3,
                    immunity: 0
                }
            })
            /* sixth step(last) - attestation */
            .state('Form.attestation', {
                url: "/attestation",
                templateUrl: "../../Scripts/views/attestation.html",
                controller: "AttestationController",
                title: 'הצהרה',
                resolve: {
                    TofesModel: ['TofesDataService', function (TofesDataService) {
                        return TofesDataService.GetTofesControllerData();
                    }]
                },
                data: {
                    type: 'Form101',
                    name: 'הצהרה',
                    RequiresAuthentication: true,
                    disabled: false,
                    formType: 0,
                    immunity: 0
                }
            });
        /*end 101 form*/

        $locationProvider.hashPrefix('');

    }]);

})();
(function () {
    angular.module('WorkerPortfolioApp').filter('MenuItemsFilter', MenuItemsFilter);

    MenuItemsFilter.$inject = [];

    function MenuItemsFilter() {
        return function (obj, vars) {
            if (typeof obj === 'object' && obj.length > 0) {
                return $.grep(obj, function (mi) {
                    if (vars.type === 'Admin' || vars.type === 'Statistics')
                        return mi.data.type === vars.type && (vars.isLoggedIn || !mi.data.RequiresAuthentication) && vars.immunity >= mi.data.immunity;
                    else
                        return mi.data.type === vars.type && (vars.isLoggedIn || !mi.data.RequiresAuthentication);
                });
            }
            return obj;
        }
    }
})();
(function () {
    angular.module('WorkerPortfolioApp').filter('dateOrder', function () {
        return function (obj, properties) {
            if (angular.isArray(obj)) {
                $.map(obj, function (item) {
                    if(item[properties] != null && item[properties].length > 8){
                        var d = item[properties].split('/');
                        item.dateProp = new Date(d[2], (parseInt(d[1]) - 1), d[0]).getTime();
                    }else{
                        item.dateProp = new Date();
                    }
                });
                var list = obj.sort(function (a, b) {
                    return a.dateProp == b.dateProp ? 0 : (a.dateProp > b.dateProp) || -1;
                });
                return list;
            }
            return obj;
        }
    });

})();
(function () {
    angular.module('WorkerPortfolioApp').filter('OrderMuneFilter', OrderMuneFilter);

    OrderMuneFilter.$inject = [];

    function OrderMuneFilter() {
        return function (obj, vars) {
            if (typeof obj === 'object' && obj.length > 0) {
                return obj.sort(function (a, b) {
                    // if they are equal, return 0 (no sorting)
                    if (a.location === b.location) { return 0; }
                    if (a.location > b.location) {
                        // if a should come after b, return 1
                        return 1;
                    }
                    else {
                        // if b should come after a, return -1
                        return -1;
                    }
                });
            }
            return obj;
        }
    }
})();
(function() {
    angular.module('WorkerPortfolioApp').filter('pagingFilter', function () {
        return function (obj, properties) {
            if (typeof obj === 'object') {
                
                var startAt = 0;
                var endAt = angular.copy(properties.MaxPages);

                if (properties.MaxPages > 4) {
                    startAt = properties.PageNumber - 4 > 0 ? properties.PageNumber - 4 : 0;
                    endAt = properties.PageNumber + 3 > properties.MaxPages ? properties.MaxPages : properties.PageNumber + 3;
                }

                return obj.slice(startAt, endAt);
            }
            return obj;
        }
    });
    
})();
(function() {

    angular.module('WorkerPortfolioApp').filter('ShowList', function () {
        return function (obj, vars) {
            if (typeof obj === 'object') {
                var start = vars.PageSize * (vars.PageNumber - 1);
                var end = vars.PageSize * vars.PageNumber;
                return obj.slice(start, end);
            }
            return obj;
        }
    });

})();
(function () {
    angular.module('WorkerPortfolioApp').filter('TableDataFilter', TableDataFilter);

    TableDataFilter.$inject = [];

    function TableDataFilter() {
        return function (obj, vars) {
            if (typeof obj === 'object' && obj.length > 0) {
                return obj.sort(function (a, b) { return (a[vars.sortItem] > b[vars.sortItem]); });
            }
            return obj;
        }
    }
})();
(function () {
    angular.module('WorkerPortfolioApp').filter('propsFilter', propsFilter);

    propsFilter.$inject = [];

    function propsFilter() {
        return function (items, props) {
            var out = [];

            if (angular.isArray(items)) {
                var keys = Object.keys(props);

                items.forEach(function (item) {
                    var itemMatches = false;

                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    }

})();



(function() {
    
    angular.module('WorkerPortfolioApp').directive('a', function () {
        return {
            restrict: 'E',
            link: function (scope, elem, attrs) {
                if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                    elem.on('click', function (e) {
                        e.preventDefault(); // prevent link click for above criteria
                    });
                }
            }
        };
    });

})();
(function() {
            
    angular.module('WorkerPortfolioApp').directive('autocomplete', function () {
        var index = -1;

        return {
            restrict: 'E',
            scope: {
                searchParam: '=ngModel',
                suggestions: '=data',
                onType: '=onType',
                onSelect: '=onSelect',
                autocompleteRequired: '=',
                noAutoSort: '=noAutoSort',
                idParam: "@itemParamId",
                nameParam: "@itemParamName",
                onDisabled: '=onDisabled',
            },
            controller: ['$scope', function ($scope) {
                // the index of the suggestions that's currently selected
                $scope.selectedIndex = -1;
                
                $scope.initLock = true;
                
                // set new index
                $scope.setIndex = function (i) {
                    $scope.selectedIndex = parseInt(i);
                };

                this.setIndex = function (i) {
                    $scope.setIndex(i);
                    $scope.$apply();
                };

                $scope.getIndex = function (i) {
                    return $scope.selectedIndex;
                };

                // watches if the parameter filter should be changed
                var watching = true;

                // autocompleting drop down on/off
                $scope.completing = false;

                // starts autocompleting on typing in something
                $scope.$watch('searchParam', function (newValue, oldValue) {
                    if (oldValue === newValue || (!oldValue && $scope.initLock)) {
                        return;
                    }

                    if (watching && typeof $scope.searchParam !== 'undefined' && $scope.searchParam !== null) {
                        $scope.completing = true;
                        $scope.searchFilter = $scope.searchParam;
                        $scope.selectedIndex = -1;
                    }

                    // function thats passed to on-type attribute gets executed
                    if ($scope.onType)
                        $scope.onType($scope.searchParam);
                });

                // for hovering over suggestions
                this.preSelect = function (suggestion) {

                    watching = false;

                    // this line determines if it is shown
                    // in the input field before it's selected:
                    //$scope.searchParam = suggestion;

                    $scope.$apply();
                    watching = true;

                };

                $scope.preSelect = this.preSelect;

                this.preSelectOff = function () {
                    watching = true;
                };

                $scope.preSelectOff = this.preSelectOff;

                // selecting a suggestion with RIGHT ARROW or ENTER
                $scope.select = function (suggestion) {
                    if (suggestion) {
                        if (typeof suggestion === 'string') {
                            var _suggestion = $.grep($scope.suggestions, function (seg) {
                                return typeof seg === 'object' && seg[$scope.nameParam] == suggestion;
                            });
                            if (_suggestion.length === 1)
                                suggestion = _suggestion[0];
                        }

                        $scope.searchParam = suggestion && typeof suggestion === 'object' && $scope.nameParam && $scope.nameParam.length > 1 ? suggestion[$scope.nameParam] : null;
                        $scope.searchFilter = suggestion;
                        if ($scope.onSelect) {
                            $scope.onSelect(suggestion);
                        }
                    }
                    watching = false;
                    $scope.completing = false;
                    setTimeout(function () { watching = true; }, 1000);
                    $scope.setIndex(-1);
                };

            }],
            link: function (scope, element, attrs) {
                setTimeout(function () {
                    scope.initLock = false;
                    scope.$apply();
                }, 250);

                var attr = '';
                
                // Default atts
                scope.attrs = {
                    "placeholder": "start typing...",
                    "class": "",
                    "id": "",
                    "inputclass": "",
                    "inputid": ""
                };
                //attr-ng-disabled
                for (var a in attrs) {
                    attr = a.replace('attr', '').toLowerCase();
                    // add attribute overriding defaults
                    // and preventing duplication
                    
                    if (a.indexOf('attr') === 0) {
                        scope.attrs[attr] = attrs[a];
                    }
                }

                if (attrs.clickActivation) {
                    element[0].onclick = function (e) {
                        if (!scope.searchParam) {
                            setTimeout(function () {
                                scope.completing = true;
                                scope.$apply();
                            }, 200);
                        }
                    };
                }

                var key = { left: 37, up: 38, right: 39, down: 40, enter: 13, esc: 27, tab: 9 };

                document.addEventListener("keydown", function (e) {
                    var keycode = e.keyCode || e.which;
                    
                    switch (keycode) {
                        case key.esc:
                            // disable suggestions on escape
                            scope.select();
                            scope.setIndex(-1);
                            scope.$apply();
                            e.preventDefault();
                    }
                }, true);

                document.addEventListener("blur", function (e) {
                    // disable suggestions on blur
                    // we do a timeout to prevent hiding it before a click event is registered
                    setTimeout(function () {
                        scope.select();
                        scope.setIndex(-1);
                        scope.$apply();
                    }, 150);
                }, true);

                element[0].addEventListener("keydown", function (e) {
                    var keycode = e.keyCode || e.which;

                    var l = angular.element(this).find('li').length;

                    // this allows submitting forms by pressing Enter in the autocompleted field
                    if (!scope.completing || l == 0) return;

                    // implementation of the up and down movement in the list of suggestions
                    switch (keycode) {
                        case key.up:

                            index = scope.getIndex() - 1;
                            if (index < -1) {
                                index = l - 1;
                            } else if (index >= l) {
                                index = -1;
                                scope.setIndex(index);
                                scope.preSelectOff();
                                break;
                            }
                            scope.setIndex(index);

                            if (index !== -1)
                                scope.preSelect(angular.element(angular.element(this).find('li')[index]).text());

                            scope.$apply();

                            break;
                        case key.down:
                            index = scope.getIndex() + 1;
                            if (index < -1) {
                                index = l - 1;
                            } else if (index >= l) {
                                index = -1;
                                scope.setIndex(index);
                                scope.preSelectOff();
                                scope.$apply();
                                break;
                            }
                            scope.setIndex(index);

                            if (index !== -1)
                                scope.preSelect(angular.element(angular.element(this).find('li')[index]).text());

                            break;
                        case key.left:
                            break;
                        case key.right:
                        case key.enter:
                        case key.tab:

                            index = scope.getIndex();
                            // scope.preSelectOff();
                            if (index !== -1) {
                                scope.select(angular.element(angular.element(this).find('li')[index]).text());
                                if (keycode == key.enter) {
                                    e.preventDefault();
                                }
                            } else {
                                if (keycode == key.enter) {
                                    scope.select();
                                }
                            }
                            scope.setIndex(-1);
                            scope.$apply();

                            break;
                        case key.esc:
                            // disable suggestions on escape
                            scope.select();
                            scope.setIndex(-1);
                            scope.$apply();
                            e.preventDefault();
                            break;
                        default:
                            return;
                    }

                });
            },
            template: '\
        <div class="autocomplete {{ attrs.class }}" id="{{ attrs.id }}">\
          <input\
            type="text"\
            ng-model="searchParam"\
            placeholder="{{ attrs.placeholder }}"\
            class="{{ attrs.inputclass }}"\
            tabindex="{{ attrs.tabindex }}"\
            id="{{ attrs.inputid }}"\
            name="{{ attrs.name }}"\
            ng-required="{{ attrs.autocompleteRequired }}" ng-disabled="onDisabled" autocomplete="new-password" />\
          <ul ng-if="!noAutoSort" ng-show="completing && (suggestions | filter:searchFilter).length > 0">\
            <li\
              suggestion\
              ng-repeat="suggestion in suggestions | filter:searchFilter | orderBy:\'toString()\' | limitTo:15 track by $index"\
              index="{{ $index }}"\
              val="{{ suggestion }}"\
              ng-class="{ active: ($index === selectedIndex) }"\
              ng-click="select(suggestion)"\
              ng-bind-html="suggestion | highlight:{searchParam:searchParam, nameParam:nameParam, idParam:idParam}"></li>\
          </ul>\
          <ul ng-if="noAutoSort" ng-show="completing && (suggestions | filter:searchFilter).length > 0">\
            <li\
              suggestion\
              ng-repeat="suggestion in suggestions | filter:searchFilter | limitTo:15 track by $index"\
              index="{{ $index }}"\
              val="{{ suggestion }}"\
              ng-class="{ active: ($index === selectedIndex) }"\
              ng-click="select(suggestion)"\
              ng-bind-html="suggestion | highlight:{searchParam:searchParam, nameParam:nameParam, idParam:idParam}"></li>\
          </ul>\
        </div>'
        };
    });
    
    angular.module('WorkerPortfolioApp').filter('highlight', ['$sce', function ($sce) {
        return function (input, params) {
            if (typeof input === 'function') return '';
            if (params.searchParam && params.searchParam.length > 1) {
                var words = params.searchParam.split(/\ /).join(' |') + '|' + params.searchParam.split(/\ /).join('|'),
                    words = words.replace(/\(/gi, '\\(').replace(/\)/gi, '\\)'),
                    exp = new RegExp('(' + words + ')', 'gi');
                if (words.length) {
                    input = params.nameParam && params.nameParam.length > 1 ? input[params.nameParam].replace(exp, "<span class=\"highlight\">$1</span>") : input.replace(exp, "<span class=\"highlight\">$1</span>");
                }
                return $sce.trustAsHtml(input);
            }
            
            return params.nameParam && params.nameParam.length > 1 ? $sce.trustAsHtml(input[params.nameParam]) : $sce.trustAsHtml(input);
        };
    }]);

    angular.module('WorkerPortfolioApp').directive('suggestion', function () {
        return {
            restrict: 'A',
            require: '^autocomplete', // ^look for controller on parents element
            link: function (scope, element, attrs, autoCtrl) {
                element.bind('mouseenter', function () {
                    autoCtrl.preSelect(attrs.val);
                    autoCtrl.setIndex(attrs.index);
                });

                element.bind('mouseleave', function () {
                    autoCtrl.preSelectOff();
                });
            }
        };
    });

})();
(function() {
    angular.module('WorkerPortfolioApp').directive('breadCrumbs', breadCrumbs);

    breadCrumbs.$inject = ['$window', '$rootScope', '$state', '$stateParams'];
    
    function breadCrumbs($window, $rootScope, $state, $stateParams) {
        var directive = {
            link: link,
            restrict: 'EA',
            templateUrl: '../../Scripts/views/ng-addons/bread-crumbs-pannel.html',
            replace: true,
            scope: true,
        };
        return directive;

        function link(scope, element, attrs) {
            scope.$state = $state;
            scope.$stateParams = $stateParams;
        }
    }

})();


(function () {
    angular.module('WorkerPortfolioApp').directive('buttons', Buttons);

    Buttons.$inject = ['$window', '$state', '$rootScope', '$timeout', 'TofesDataService'];

    function Buttons($window, $state, $rootScope, $timeout, TofesDataService) {
        var directive = {
            link: link,
            restrict: 'AE',
            templateUrl: '../../Scripts/views/ng-addons/buttons.component.html',
            replace: true,
            scope: true
        };
        return directive;

        function link(scope, element, attrs) {
            $timeout(function () {
                scope.states = scope.lsGet('states') || TofesDataService.GetComputedStates($state.get(), $state.current.name, parseInt(scope.$parent.tofesModel.FormType));
                
                scope.isFirstStep = false;
                scope.isLastStep = false;
                scope.showPdf = false;

                scope.states.filter(function (s) {
                    if (s.clas.isempty()) {
                        s.isCurrent = false;
                        if (s.goTo === $state.current.name) {
                            s.isCurrent = true;
                            scope.current = s.position;
                            scope.previous = s.position - 1;
                            scope.next = s.position + 1;
                            if (s.position === 0) scope.isFirstStep = true;
                            if (s.position === (scope.states.length - 1)) scope.isLastStep = true;
                        }
                    } else {
                        if (s.isCurrent) {
                            scope.previous = s.position - 1;
                            scope.next = s.position + 1;
                            if (s.position === 0) scope.isFirstStep = true;
                            if (s.position === (scope.states.length - 1)) scope.isLastStep = true;
                        }
                    }
                });

                scope.subscribe('returnToLastStep', function (hide) {
                    scope.showPdf = false;
                });

                scope.mockSubmit = function () {
                    $timeout(function () {
                        $('form[name=partForm] input').addClass('ng-submited');
                        $('form[name=partForm] button[id=submit]').trigger('click');
                    }, 10);
                }

                scope.onPrevious = function () {
                    ///send the direction off movment, prev is `false`
                    scope.publish('goto.step', scope.states[scope.previous].goTo, false);
                }

                scope.onNext = function () {
                    scope.$parent.goToStep = scope.states[scope.next].goTo;
                    scope.mockSubmit();
                }

                scope.onCompleteForm = function () {
                    scope.$parent.goToStep = '';
                    scope.mockSubmit();
                    scope.showPdf = true;
                };

                scope.hidePdf = function () {
                    scope.publish('hidePdf', true);
                    scope.showPdf = false;
                }
                scope.submitPdf = function () {
                    scope.publish('submitPdf', true);
                }

            }, 30);
        }
    }

})();
(function() {
    

    angular.module('WorkerPortfolioApp').directive('drawing', drawing);

    drawing.$inject = ['$window'];
    
    function drawing($window) {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
            var ctx = element[0].getContext('2d');

            // variable that decides if something should be drawn on mousemove
            var drawing = false;

            // the last coordinates before the current move
            var lastX, lastY, currentX, currentY;

            element.bind('mousedown', function (event) {
                if (event.offsetX !== undefined) {
                    lastX = event.offsetX;
                    lastY = event.offsetY;
                } else { // Firefox compatibility
                    lastX = event.layerX - event.currentTarget.offsetLeft;
                    lastY = event.layerY - event.currentTarget.offsetTop;
                }

                // begins new line
                ctx.beginPath();

                drawing = true;
            });
            element.bind('mousemove', function (event) {
                if (drawing) {
                    // get current mouse position
                    if (event.offsetX !== undefined) {
                        currentX = event.offsetX;
                        currentY = event.offsetY;
                    } else {
                        currentX = event.layerX - event.currentTarget.offsetLeft;
                        currentY = event.layerY - event.currentTarget.offsetTop;
                    }

                    draw(lastX, lastY, currentX, currentY);

                    // set current coordinates to last one
                    lastX = currentX;
                    lastY = currentY;
                }

            });
            element.bind('mouseup', function (event) {
                // stop drawing
                drawing = false;
            });

            // canvas reset
            function reset() {
                element[0].width = element[0].width;
            }

            function draw(lX, lY, cX, cY) {
                // line from
                ctx.moveTo(lX, lY);
                // to
                ctx.lineTo(cX, cY);
                // color
                ctx.strokeStyle = "#000";
                // draw it
                ctx.stroke();
            }
        }
    }

})();
(function () {

    angular.module('WorkerPortfolioApp').directive('dynamicHtml',['$compile', function ($compile) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                html: '<'
            },
            link: function (scope, element, attrs) {
                scope.html = scope.html.replace(/\<\/ul/igm, '</ol').replace(/\<ul/igm, '<ol');

                var content = $compile(scope.html)(scope);
                element.append(content);
            }
        };
    }]);

})();
(function () {
    angular.module('WorkerPortfolioApp').directive('exportExcel', exportExcel);

    exportExcel.$inject = ['FileSaver', '$timeout'];

    function exportExcel(FileSaver, $timeout) {
        var directive = {
            link: link,
            restrict: 'A',
            scope: {
                fileName: "=fileName",
                workSheet: "=workSheet",
                data: "=exportData",
                classes: "@"
            },
            replace: true,
            template: '<button class="btn btn-info btn-ef btn-ef-3 btn-ef-3c mb-10 {{AddindClass}}" ng-click="download()">הורדה לאקסל <i class="fa fa-download"></i></button>'
        };
        return directive;

        ///must function thet return list of data/list of data as JSON && file name/function thet returns file name && work sheet name/function thet returns work sheet name
        function link(scope, element) {
            scope.AddindClass = (typeof scope.classes !== 'undefined' && !scope.classes.isempty()) ? scope.classes : '';

            ///work seat name can b a function or string or null
            scope.getWorkSheetName = function () {
                switch (typeof scope.workSheet) {
                    case 'undefined':
                        return 'WORK_SHEET';
                    case 'function':
                        return scope.workSheet()
                    default:
                        return scope.workSheet;
                }
            }
            ///file name can b a function or string
            scope.getFileName = function () {
                switch (typeof scope.fileName) {
                    case 'function':
                        return scope.fileName()
                    default:
                        return scope.fileName;
                }
            }
            ///data can b a function or an array
            scope.getExportData = function () {
                switch (typeof scope.data) {
                    case 'function':
                        return scope.data()
                    default:
                        return scope.data;
                }
            }

            ///calc columns width
            scope.columnRange = function (workSheet) {
                var sheet2arr = function (sheet) {
                    var result = [];
                    var row;
                    var rowNum;
                    var colNum;
                    var range = XLSX.utils.decode_range(sheet['!ref']);
                    for (rowNum = range.s.r; rowNum <= range.e.r; rowNum++) {
                        row = [];
                        for (colNum = range.s.c; colNum <= range.e.c; colNum++) {
                            if (typeof result[colNum] === 'undefined') result[colNum] = 0;
                            var nextCell = sheet[
                                XLSX.utils.encode_cell({ r: rowNum, c: colNum })
                            ];
                            if (typeof nextCell !== 'undefined') {
                                if (nextCell.t === 's')
                                    result[colNum] = result[colNum] > nextCell.v.length ? result[colNum] : nextCell.v.length;
                                else {
                                    result[colNum] = result[colNum] > String(nextCell.v).length ? result[colNum] : String(nextCell.v).length;
                                }
                            }
                        }
                    }
                    return result;
                };

                var wscols = new Array(),
                range = sheet2arr(workSheet);
                for (var s in range)
                    wscols.push({ wch: (range[s] + 4) });
                return wscols;
            }

            scope.download = function () {
                /* generate a worksheet */
                var ws = XLSX.utils.json_to_sheet(scope.getExportData());
                /* set column width */
                ws['!cols'] = scope.columnRange(ws);
                /* add to workbook */
                var wb = XLSX.utils.book_new();

                XLSX.utils.book_append_sheet(wb, ws, scope.getWorkSheetName());

                /* write workbook and force a download */
                XLSX.writeFile(wb, scope.getFileName());
            };
        }
    }

})();


(function () {
    angular.module('WorkerPortfolioApp').directive('uploader', function () {
        return {
            restrict: 'EA',
            link: function (scope, elem, attrs) {
                elem.attr('accept', '.jpg, .png, .gif, .tif, .jpeg, .pdf');
                elem.attr('title', 'גודל מקסימאלי 4MB\r `jpg, png, gif, tif, jpeg, pdf` - סוגי קבצים מותרים');
            }
        };
    });

})();
(function () {
    angular.module('WorkerPortfolioApp').directive('folderStructure', folderStructure);

    folderStructure.$inject = [];

    function folderStructure() {
        var directive = {
            link: link,
            restrict: 'EA',
            templateUrl: '../../Scripts/views/Email-Templates/folder-structure-pannel.html',
            replace: true,
            scope: {
                folderStruct: '=folderStruct',
                depthStruct: '=depthStruct',
                inputValidation: '=inputValidation',
                currentDragOver: '=currentDragOver'
            }
        };
        return directive;

        function link(scope, element, attrs) {
            scope.depth_struct = parseInt(scope.depthStruct) + 1;
            
            scope.selectFolder = function (selectedFolder) {
                selectedFolder.selected = true;
                selectedFolder.isSelected = true;
                scope.publish('UpdateSelectedFolder', selectedFolder);
            }
            scope.updateFolder = function (folder) {
                scope.publish('UpdateFolderName', folder);
            }
            scope.addFolder = function (folder) {
                scope.publish('AddNewFolder', folder);
            }

            scope.OnDropFile = function (data, $dragElement, $dropElement, event, dropData) {
                scope.publish('FileDroped', data, dropData);
            };
            scope.onDragOver = function (data, $dragElement, $dropElement, event, dropData) {
                scope.publish('dragOver', $dropElement, dropData);
            };
            scope.onDragLeave = function (data, $dragElement, $dropElement, event, dropData) {
                scope.publish('dragLeave', data, dropData);
            };
        }
    }

})();


(function () {
    angular.module('WorkerPortfolioApp').directive('formHeader', formHeader);

    formHeader.$inject = ['$state', '$rootScope', '$window', 'TofesDataService'];

    function formHeader($state, $rootScope, $window, TofesDataService) {
        var directive = {
            link: link,
            restrict: 'AE',
            templateUrl: '../../Scripts/views/ng-addons/progress-bar.component.html',
            replace: true,
            scope: true
        };
        return directive;

        function link(scope, element, attrs) {
            scope.states = scope.lsGet('states') || TofesDataService.GetComputedStates($state.get(), $state.current.name, parseInt(scope.$parent.tofesModel.FormType));

            scope.lsBind(scope, 'states', scope.states);

            scope.states.filter(function (s) {
                s.isCurrent = false;
                if (s.goTo === $state.current.name) {
                    s.isCurrent = true;
                    scope.current = s.position;
                }
            });

            scope.states.filter(function (s) {
                if (s.position < scope.current) {
                    s.clas = 'complete';
                }
                if (s.position === scope.current) {
                    s.clas = 'active';
                }
                if (s.position > scope.current) {
                    s.clas = s.isCompleted ? '' : 'disabled';
                }
            });

            scope.$step = function (goTo, index) {
                ///if current step do nuthing
                if (index === scope.current) {
                    return void [0];
                }
                ///if go-to is next then sent `true` to validate alse send `false`
                scope.publish('goto.step', goTo, index > scope.current);
            }

            scope.subscribe('$$states', function (data) {
                scope.states[scope.current].isCompleted = true;
            });
        }
    }

})();
(function() {

    angular.module('WorkerPortfolioApp').directive('formPdfScale', formPdfScale);

    formPdfScale.$inject = ['$window', '$compile', '$rootScope', '$state'];
    
    function formPdfScale($window, $compile, $rootScope, $state) {
        var helper = {
            detectBrowser: function () {
                var userAgent = $window.navigator.userAgent;
                var browsers = { chrome: /chrome/i, safari: /safari/i, firefox: /firefox/i, ie: /internet explorer/i };
                for (var key in browsers) {
                    if (browsers[key].test(userAgent)) {
                        return key;
                    }
                };
                if (parseInt(document.documentMode) > 0)
                    return 'IE';

                return 'undefined';
            },
            //templateMobile: '<div ng-hide="pdfDataStatus == 406"><img ng-src="{{ngUrl}}" style="width:100%;height:100%;min-height:{{minHeight}}px;" /></div>',
            templateMobile: '<div ng-hide="pdfDataStatus == 406" template-mobile><img ng-src="data:image/png;base64,{{fileImageBlob}}" style="width:100%;height:100%;" /></div>',
            templateIE: '<div ng-hide="pdfDataStatus == 406" class="iframe-holder-div"><iframe id="template" ng-src="{{ngUrl}}" width="100%" height="100%" style="width:100%;height:100%;min-height:{{minHeight}}px;"></iframe></div>',
            templateChrom: '<object id="template" ng-bind="contentView" ng-show="pdfShow" data="{{pdfContent}}#{{contentView}}" style="width:100%;height:100%;min-height:{{minHeight}}px;" type="application/pdf" title="654">\
                                    <p>\
                                        It appears you don`t have a PDF plugin for this browser.\
                                        No problem though...\
                                        You can <a href="{{ngUrl}}">click here to download the PDF</a>.\
                                    </p>\
                                </object>',
            currentTemplate: undefined,
            getCurrentTemplate: function () {
                switch (helper.detectBrowser().toLowerCase()) {
                    case 'ie':
                        helper.currentTemplate = helper.templateIE;
                        break;
                    default:
                        helper.currentTemplate = helper.templateChrom;
                        break;
                }
                if ($window.innerWidth < 801)
                    helper.currentTemplate = helper.templateMobile;

                return helper.currentTemplate;
            },
            browserType: function () {
                if ($window.innerWidth < 801)
                    return 1;

                switch (helper.detectBrowser().toLowerCase()) {
                    case 'ie':
                        return 1;
                        break;
                    default:
                        return 0;
                        break;
                }
            }
        };
        var directive = {
            link: link,
            restrict: 'EA',
            replace: true,
            template: function () {
                return helper.currentTemplate || helper.getCurrentTemplate();
            },
            scope: {
                pdfContent: '=pdfContent',
                pdfShow: '=pdfShow',
                pdfDate: '=pdfDate',
                pdfDataStatus: '=pdfDataStatus',
                formPdfImageId: '=formPdfImageId',
                fileImageBlob: '=fileImageBlob',
            }
        };
        return directive;

        function link(scope, element, attrs) {
            window.IFRAME = element;

            scope.Browser = helper.browserType();
            var credentials = typeof $rootScope.$$credentials !== 'undefined' ? $rootScope.$$credentials : $rootScope.lsGet('$$credentials');
            scope.ngUrl = '';

            scope.contentView = '';
            scope.$Width = $window.outerWidth;
            scope.$Height = $window.outerHeight;
            
            scope.$watch('formPdfImageId', function (newValue, oldValue) {
                if (oldValue === newValue || (!oldValue && !newValue) || !newValue) {
                    return void [0];
                }
                
                scope.ngUrl = ($window.innerWidth < 801 ? '/home/Convert?' : '/home/FormIeShow?') + ['idNumber=' + credentials.idNumber, 'imageID=' + scope.formPdfImageId].join('&');

                if (scope.Browser === 1) {
                    element.children().remove();
                    setTimeout(function () {
                        scope.$apply(function () {
                            var content = $compile(helper.currentTemplate)(scope);
                            element.append(content);
                        });
                        pdfView();
                    }, 125);
                }
            }, true);

            var scaleWidthSwitch = function (width) {
                var w = parseInt(width / 50) - 28;
                w = w < 0 ? 0 : (w > 12 ? 12 : w);
                w = w > 0 ? (103 + (5 * (w - 1))) : 0;
                return w;
            }
            var pdfView = function (scale) {
                if (typeof scale === 'undefined')
                    scale = 'view=Fit';

                var w = scaleWidthSwitch(scope.$Width);
                if (w <= 0)
                    scale = 'view=Fit';
                else
                    scale = 'zoom=' + w;

                scope.contentView = scale;

                scope.minHeight = $window.innerWidth < 801 ? parseInt(scope.$Height / 1.15) : parseInt(scope.$Height * 1.15);
                if (scope.Browser === 1) {
                    element.find('iframe').attr('style', 'width:100%;height:100%;min-height:' + scope.minHeight + 'px;');
                }
            }

            $(window).on("resize.doResize", _.debounce(function () {
                scope.$Width = $window.outerWidth;
                scope.$Height = $window.outerHeight;
                if (scope.Browser !== 1) {
                    scope.$apply(function () {
                        var content = $compile(helper.currentTemplate)(scope);
                        element.append(content);
                    });
                }
                pdfView();
            }, 150));

            pdfView();
        }
    }

})();
(function() {
    
    angular.module('WorkerPortfolioApp').directive('liveCheckbox', liveCheckbox);

    liveCheckbox.$inject = [];
    
    function liveCheckbox() {
        var directive = {
            link: link,
            restrict: 'AE',
            require: 'ngModel',
            scope:
			{
			    classes: "@",
			    inputType: "@",
			    inputText: "@",
			    //inputDisabled: "=inputDisabled",
			    inputName: "@",
			    inputDisabled: '=inputDisabled',
			},
            templateUrl: '../../Scripts/views/ng-addons/live-checkbox.html',
            replace: true,
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            scope.$watch('inputDisabled', function (newValue, oldValue) {
                if (typeof newValue === 'undefined' || newValue === null || newValue === oldValue)
                    return void [0];
                scope.ngDisabled = parseInt(scope.inputDisabled) === 1;
            });

            scope.inputType = (scope.inputType !== undefined ? scope.inputType : 'button').toLowerCase();

            scope.inputName = typeof scope.inputName !== 'undefined' && !scope.inputName.isempty() ? scope.inputName : 'input_name';
            scope.AddindClass = typeof scope.classes !== 'undefined' && !scope.classes.isempty();
            scope.inputsText = typeof scope.inputText !== 'undefined' && !scope.inputText.isempty() ? scope.inputText.split(',') : ['כן', 'לא'];
            scope.ngDisabled = parseInt(scope.inputDisabled) === 1;
            
            scope.changekModelValue = function (val) {
                if (scope.ngDisabled)
                    return void[0];
                
                scope.ngModel.$viewValue = val;
                scope.ngModel.$commitViewValue();
                scope.ngModel.$setDirty();
            }
            
            scope.ngModel = ngModel;
        }
    }

})();
(function () {
    angular.module('WorkerPortfolioApp').directive('login', login);

    login.$inject = ['$compile', '$window', '$state', '$rootScope', 'Credentials', 'toastrService', '$templateRequest'];

    function login($compile, $window, $state, $rootScope, Credentials, toastrService, $templateRequest) {
        var helper = {
            _credentials: {
                idNumber: null,
                password: null,
                loggedIn: false,
                resetPassword: false,
                lastLogin: null,
                sendOption: 1,
                $sendVal: null,
                currentTerminationDate: null,
                mobileLogInDisplay: 0,
                currentMunicipalityId: null,
                isAllow101: false,
                isAllowProcess: false,
                isAllowMunicipalityMenu: false,
                isAllowSystemsEmail: false,
                isAllowEPC: false
            },
            parseLastLogIn: function (dateStr) {
                var dateArray = dateStr.split('T'), model = {};
                model.lastDate = dateArray[0].split('-').splice(1, 2).reverse().join('/');
                model.lastTime = dateArray[1].split('.')[0].split(':').splice(0, 2).join(':');

                return model;
            },
            _resetForm: function (loginForm) {
                loginForm.$setPristine();
                loginForm.$setUntouched();
                _.each(loginForm, function (item) {
                    if (typeof item !== 'undefined' && typeof item.$setValidity === 'function') {
                        _.each(item.$validators, function ($validator, validatorName) {
                            item.$setUntouched();
                            item.$setPristine();
                        });
                    }
                });
            },
            mobileTemplate: '../../Scripts/views/ng-addons/mobile-Login-pannel.html',
            pcTemplate: '../../Scripts/views/ng-addons/Login-pannel.html',
            getCurrentTemplate: function () {
                if ($window.innerWidth < 801) {
                    return { template: helper.mobileTemplate, isMobile: true };
                } else {
                    return { template: helper.pcTemplate, isMobile: false };
                }
            },
            doReCompile: function (scope) {
                if (scope._Width == undefined || scope._Width == null) {
                    scope._Width = 0;
                    scope._positionX = null;
                }

                if (($window.innerWidth < (scope._Width - 10) || $window.innerWidth > (scope._Width + 10))) {
                    scope._Width = $window.innerWidth
                    scope._positionX = scope._Width > 800? null : 'bottom';
                    return true;
                } else {
                    return false;
                }
            }
        };
        var directive = {
            link: link,
            restrict: 'E',
            scope: true,
        };
        return directive;

        function link(scope, element, attrs) {
            window.toastrService = toastrService;
            $rootScope.templateHandel = helper.getCurrentTemplate();
            scope.credentials = angular.copy(helper._credentials);

            scope.MobileLogIn = function (val) {
                if (scope.credentials.loggedIn) {
                    scope.credentials.mobileLogInDisplay = 0;
                    return;
                }
                switch (val) {
                    case 1:
                        if (scope.credentials.mobileLogInDisplay === 0)
                            scope.credentials.mobileLogInDisplay = 1;
                        else
                            scope.credentials.mobileLogInDisplay = 0;
                        break;
                    case 2:
                        if (scope.credentials.mobileLogInDisplay === 1)
                            scope.credentials.mobileLogInDisplay = 2;
                        else
                            scope.credentials.mobileLogInDisplay = 1;
                        break;
                    case 3:
                        scope.credentials.mobileLogInDisplay = 0;
                        break;
                }
            }

            ///select the CurrentMunicipality
            scope.linkCurrentMunicipality = function () {
                if (scope.credentials.municipalities && scope.credentials.municipalities.length > 0) {
                    scope.currentMunicipality = $.grep(scope.credentials.municipalities, function (item) {
                        return item.ID == scope.credentials.currentMunicipalityId;
                    })[0];
                }
            }

            if (typeof $rootScope.$$credentials === 'undefined') {
                $rootScope.$$credentials = scope.credentials;
                var ExistingLogIn = $rootScope.lsGet('$$credentials');
                if (ExistingLogIn && typeof ExistingLogIn === 'object') {///user is loged in
                    scope.credentials = ExistingLogIn;
                    $rootScope.lsBind($rootScope, '$$credentials', scope.credentials);
                } else {
                    $rootScope.lsBind($rootScope, '$$credentials', scope.credentials);
                }
            } else {
                scope.credentials = $rootScope.$$credentials;
            }

            scope.linkCurrentMunicipality();

            scope.setSendVal = function (value) {
                if (typeof value !== 'undefined') {
                    scope.credentials.sendOption = value;
                    scope.credentials.$sendVal = scope.credentials.sendOption !== null && scope.credentials.sendOption > 0 ? true : null;
                } else {
                    scope.credentials.$sendVal = scope.credentials.sendOption !== null && scope.credentials.sendOption > 0 ? true : null;
                }
            }

            scope.doLogin = function () {
                if (scope.loginForm.$invalid)
                    return void [0];
                var data = { idNumber: angular.copy(scope.credentials.idNumber), password: angular.copy(scope.credentials.password) };
                scope._logout(false, false);
                Credentials.Login(data)
                    .then(function successCallback(res) {
                        if (res.data.success) {
                            sessionStorage.removeItem("$$popUpWindow");
                            scope._login(res.data.userCredentials, true);
                        } else {
                            scope.credentials.idNumber = data.idNumber;
                            switch (res.data.exceptionId) {
                                case 11://"UserMunicipalityVerifyException"
                                    toastrService.error('הראשות אינה פעילה במערכת.', 'אין כניסה!', { allowHtml: true, timeOut: 7000 });
                                    break;
                                case 10://"UserVerifyException"
                                    toastrService.warning('', 'שם משתמש או סיסמה שגויים!');
                                    break;
                                case 9://UserNotConfirmedException"
                                    toastrService.warning('', 'המשתמש לא אישר את החשבון!');
                                    break;
                                case 7://"NullReferenceException"
                                    toastrService.error('יש לוודא מול נציג הארגון שפרטי המייל / טלפון נייד מוגדרים במערכת.', 'לא נמצא משתמש לפי הפרטים שסיפקת!', { allowHtml: true, timeOut: 8000 });
                                    break;
                                case 101:///user is locked out
                                    toastrService.error('אנא נסה להתחבר מחדש בעוד LOCKTIMELEFT דקות'.replace('LOCKTIMELEFT', res.data.lockTimeLeft), 'סליחה, ננעלת מחוץ למערכת.');
                                    break;
                                default:
                                    //console.log('default Error', res);
                                    toastrService.error('', 'תקלת מערכת!');
                                    break;
                            }
                        }
                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
            }
            scope.doLogout = function (showToast, goHome) {
                Credentials.LogOut()
                    .then(function successCallback(res) {
                        scope._logout(showToast, goHome);
                    }, function errorCallback() {
                        scope._logout(false, false);
                        toastrService.error('', 'תקלת מערכת!');
                    });
            }
            scope.doReset = function () {
                if (scope.resetForm.$invalid)
                    return void [0];

                Credentials.ResetPassword({ idNumber: scope.credentials.idNumber, mail: scope.credentials.sendOption === 1, sms: scope.credentials.sendOption === 2 })
                    .then(function successCallback(res) {
                        if (res.data.success) {
                            scope.credentials.resetPassword = false;
                            switch (scope.credentials.sendOption) {
                                case 1:///email
                                    toastrService.success('מייל עדכון סיסמה נשלח לכתובת המייל הרשומה, אנא עקוב אחר ההוראות במייל לאיפוס סימתך.', 'תודה, ' + res.data.userCredentials.fullName, { positionX: scope._positionX });
                                    break;
                                case 2:///sms
                                    toastrService.success('מזהה איפוס סיסמה נשלח לטלפון הנייד הרשום במערכת, אנא הכנס את הקוד לשדה המיועד.', 'תודה, ' + res.data.userCredentials.fullName, { positionX: scope._positionX });
                                    $state.go('Authentication.ResetPasswordFromSms', { idNumber: scope.credentials.idNumber });
                                    break;
                            }
                        } else {
                            switch (res.data.exceptionId) {
                                case 111:
                                    toastrService.error('אנא פנה לכ"א על מנת לעדכן פרטים.', 'למשתמש לא מוגדרת כתובת מייל במערכת!');
                                    break;
                                case 112:
                                    toastrService.error('אנא פנה לכ"א על מנת לעדכן פרטים.', 'למשתמש לא מוגדר מספר טלפון נייד במערכת!!');
                                    break;
                                case 58:
                                    toastrService.error('אנא פנה למנהל האתר על מנת להסדיר הרשמה או הרשם באתר.', 'המשתמש אינו מוגדר במערכת!');
                                    break;
                                default:
                                    toastrService.error(res.data.exception, 'תקלת מערכת!');
                                    break;
                            }
                        }
                        scope._logout(false, false, true);
                    }, function errorCallback(res) {
                        //console.log('error', res);
                        scope._logout(false, false, true);
                        toastrService.error('', 'תקלת מערכת!');
                    });
            }
            ///do the change municipality function
            scope.doChangeMunicipality = function () {
                if (!scope.currentMunicipality)
                    return void [0];
                scope.credentials.currentMunicipalityId = scope.currentMunicipality.ID;
                scope.credentials.isAllow101 = scope.currentMunicipality.allow101;
                scope.credentials.isAllowProcess = scope.currentMunicipality.isAllowProcess;
                scope.credentials.isAllowMunicipalityMenu = scope.currentMunicipality.isAllowMunicipalityMenu;
                scope.credentials.isAllowSystemsEmail = scope.currentMunicipality.isAllowSystemsEmail;
                scope.credentials.isAllowEPC = scope.currentMunicipality.isAllowEPC;

                $rootScope.$$credentials.currentMunicipalityId = scope.currentMunicipality.ID;
                $rootScope.$$credentials.isAllow101 = scope.currentMunicipality.allow101;
                $rootScope.$$credentials.isAllowProcess = scope.currentMunicipality.isAllowProcess;
                $rootScope.$$credentials.isAllowMunicipalityMenu = scope.currentMunicipality.isAllowMunicipalityMenu;
                $rootScope.$$credentials.isAllowSystemsEmail = scope.currentMunicipality.isAllowSystemsEmail;
                $rootScope.$$credentials.isAllowEPC = scope.currentMunicipality.isAllowEPC;

                Credentials.ChangeMunicipality({ currentMunicipalityId: scope.currentMunicipality.ID, idNumber: scope.credentials.idNumber, confirmationToken: scope.credentials.confirmationToken, calculatedIdNumber: scope.credentials.calculatedIdNumber, updateUserData: true })
                    .then(function successCallback(res) {
                        if (res.status === 200 && res.data.currentMunicipalityId === scope.currentMunicipality.ID) {
                            toastrService.success('', 'החלפת ארגון בהצלחה!', { positionX: scope._positionX });
                            $rootScope.lsRemove('$$tofesModel');
                            $rootScope.lsRemove('$$tofesModelCopy');
                            $rootScope.lsRemove('states');

                            $rootScope.$$credentials = res.data;
                            $rootScope.$$credentials.loggedIn = true;
                            $rootScope.$$credentials.lastLogin = helper.parseLastLogIn($rootScope.$$credentials.loginDate);

                            scope.credentials = $rootScope.$$credentials;
                            scope.publish('VerifayLoginState', scope.credentials);
                            scope.publish('ChangeMunicipality', scope.currentMunicipality.ID);
                            scope.publish('PdfContect.Changed', scope.credentials);

                            sessionStorage.removeItem("$$popUpWindow");

                            if ($state.current.data.type === 'Form101') {
                                $state.current.name == 'Form.employer-employee' ? $state.transitionTo($state.current, {}, { reload: true, inherit: true, notify: true }) : $state.go('Form.employer-employee');
                            } else {
                                $state.transitionTo($state.current, {}, { reload: true, inherit: true, notify: true });
                            }

                        } else {
                            toastrService.error('אנה נסה להיתחבר למערכת מחדש', 'תקלה!');
                            scope.doLogout(false, true);
                        }
                    }, function errorCallback(res) {
                        toastrService.error('', 'תקלת מערכת!');
                        scope.doLogout(false, true);
                    });
            }

            ///inner function to do the login/out 
            scope._login = function ($credentials, showToast) {
                $rootScope.$$credentials = $credentials;
                $rootScope.$$credentials.loggedIn = true;
                $rootScope.$$credentials.lastLogin = helper.parseLastLogIn($rootScope.$$credentials.loginDate);

                scope.credentials = $rootScope.$$credentials;
                scope.linkCurrentMunicipality();

                scope.publish('VerifayLoginState', scope.credentials);
                if (showToast) toastrService.success('ברוך הבא למערכת תיק עובד באינטרנט.', 'שלום, fullName'.replace('fullName', $rootScope.$$credentials.fullName), { positionX: scope._positionX });
                $state.go('Home');

                scope._refreshUserData();
            }
            scope._logout = function (showToast, goHome, doServer) {
                if (doServer) {
                    scope.doLogout(false, false);
                    return void [0];
                }
                if (showToast) toastrService.info('התנתקת מהמערכת בהצלחה.', 'תודה USERNAME'.replace('USERNAME', scope.credentials.fullName));
                $rootScope.$$credentials_unbind;
                $rootScope.lsRemove('$$credentials');
                $rootScope.$$credentials = scope.credentials = angular.copy(helper._credentials);
                scope.publish('VerifayLoginState', scope.credentials);
                if (goHome)
                    $state.go('Home');
            }

            ///inner function to refresh the user data 
            scope._refreshUserData = function () {
                Credentials.RefreshUserData({ currentMunicipalityId: scope.credentials.currentMunicipalityId, idNumber: scope.credentials.idNumber, confirmationToken: scope.credentials.confirmationToken, calculatedIdNumber: scope.credentials.calculatedIdNumber })
                    .then(function successCallback(res) {
                        $rootScope.$$credentials = res.data.userCredentials;
                        $rootScope.$$credentials.loggedIn = true;
                        $rootScope.$$credentials.lastLogin = helper.parseLastLogIn($rootScope.$$credentials.loginDate);
                        scope.credentials = $rootScope.$$credentials;
                        scope.publish('VerifayLoginState', scope.credentials);
                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                        scope._logout(false, false);
                    });
            }

            ///do login from client authentication - user starts account
            scope.subscribe('RefreshLoginState', function (userCredentials) {
                scope._login(userCredentials, false);
                toastrService.success('חשבונך אותחל בהצלחה!', 'תודה, fullName'.replace('fullName', $rootScope.$$credentials.fullName), { positionX: scope._positionX });
                $state.go('Home');
            });
            ///do login from client authentication - user reset password
            scope.subscribe('passwordResetLonin', function (userCredentials) {
                scope._login(userCredentials, false);
                $state.go('Home');
            });
            ///update the user data on client update
            scope.subscribe('UpdateShowLoginSatet', function (userCredentials) {
                scope._login(userCredentials, false);
            });
            ///subscribe to menu directive - transmit the login state
            scope.subscribe('TransmitLoginState', function () {
                if (scope.credentials.loggedIn) {
                    var login = new Date(scope.credentials.loginDate), now = new Date();
                    var diff = Math.ceil(Math.abs(now - login) / 36e5);
                    if (diff > 12)
                        scope.doLogout(false, true);
                }
                scope.publish('VerifayLoginState', scope.credentials);
            });
            ///subscribe to logout from controllers - when thers a problem in userdata
            scope.subscribe('CatchTransmitLogout', function (showToast, goHome) {
                if (typeof showToast === 'undefined')
                    showToast = false;
                if (typeof goHome === 'undefined')
                    goHome = true;
                scope.doLogout(showToast, goHome);
            });
            ///subscribe to logout from controllers - when thers a problem in userdata
            scope.subscribe('passwordResetLogout', function (val) {
                scope._logout(false, false, true);
            });

            scope.subscribe('showMobileMenu', function (val) {
                scope.credentials.mobileLogInDisplay = 0;
            });

            scope._compile = function () {
                if (!helper.doReCompile(scope))
                    return void [0];
                $rootScope.templateHandel = helper.getCurrentTemplate();

                $templateRequest($rootScope.templateHandel.template).then(function (html) {
                    var template = angular.element(html);
                    element.html('');
                    element.html(template);
                    $compile(template)(scope);
                }).then(function () {
                    scope.setSendVal(1);
                });
            }

            $(window).on("resize.doResize", _.debounce(function () {
                scope._compile();
            }, 150));

            scope._compile();
        }
    }
})();
(function () {
    angular.module('WorkerPortfolioApp').directive('menu', menu);

    menu.$inject = ['$window', '$rootScope', '$timeout', '$state', 'toastrService', '$http', '$q'];

    function menu($window, $rootScope, $timeout, $state, toastrService, $http, $q) {
        var helper = {
            municipalitiesList: [14009, 99964, 32600, 2405, 99208, 4428, 26300,
                    99375, 14000, 92720, 50831, 99957, 99170, 2404, 67800, 99166, 99316,
                    28400, 67000, 99156, 51020, 83570, 39000, 52610, 69800, 90541, 36700,
                    51015, 55599, 4340, 42620, 32630, 99165, 18600, 50099, 6242, 37300, 36800, 62999],
            menu: {
                newMenuItems: new Array(),
                logInState: null,
                MenuTimer: null
            },
            doReCompile: function (scope) {
                if (scope._Width == undefined || scope._Width == null)
                    scope._Width = 0;

                if (($window.innerWidth < (scope._Width - 20) || $window.innerWidth > (scope._Width + 20))) {
                    scope._Width = $window.innerWidth
                    return true;
                } else {
                    return false;
                }
            }
        };
        var directive = {
            link: link,
            restrict: 'AE',
            templateUrl: '../../Scripts/views/ng-addons/Menu-pannel.html',
            replace: true,
            scope: true
        };
        return directive;

        function link(scope, element, attrs) {
            scope.showMobileMenu = false;
            scope.GotUserSatet = false;
            scope.$state = $state;
            scope.newMenuItems = new Array();


            ///TODO=> remove after data fix
            /* *************************************************************************************************************************************************************** */
            ///custom TEMP function to stop `Attendance` propogation
            var removeByMunicipalities = function (state) {
                return !(state.data.type === 'Attendance' && state.name !== "Attendance.AttendanceSheet" && scope.$$credentials.loggedIn
                    && helper.municipalitiesList.indexOf(scope.$$credentials.currentMunicipalityId) >= 0);
            };
            /* *************************************************************************************************************************************************************** */


            ///checks => check witch menus the user hase
            var checkIsAllow = function (data, credentials) {
                if (!credentials.loggedIn)
                    return false;
                var isAllow = $.map(data, function (value, item) {
                    if (String(item).indexOf('isAllow') === 0 && value)
                        return String(item);
                });
                if (isAllow.length === 0)
                    return true;
                return data[isAllow[0]] === credentials[isAllow[0]];
            }
            var validateUserGroups = function (credentials, data) {
                if (!credentials.loggedIn || credentials.groups === undefined || typeof credentials.groups === 'undefined' || credentials.groups.length < 1)
                    return false;

                var ret = false;
                $.map(credentials.groups, function (g) {
                    if (g.immunity === data.immunity) ret = true;
                });
                return ret;
            }
            var validateAuthentication = function (data, credentials) {
                return !data.RequiresAuthentication || credentials.isAdmin > 70
                        || (credentials.loggedIn && data.immunity < 1 && checkIsAllow(data, credentials));

            }
            var validateMenuParents = function () {
                scope.newMenuItems = $.grep(scope.newMenuItems, function (state) {
                    state.inner = $.grep(state.inner, function (s) {
                        return (validateAuthentication(s.data, scope.$$credentials) || validateUserGroups(scope.$$credentials, s.data)) && removeByMunicipalities(s);
                    });
                    return state.inner.length > 0 || state.data.ShowAnyway === true;
                });
            }

            ///start bileding the menu
            var getMenuParents = function (states) {
                return $.grep(states, function (state) {
                    return (state.abstract && state.location !== undefined && (state.data.ShowAnyway === true || scope.$$credentials.loggedIn));
                });
            }
            var getChildrensForParent = function (parent, states) {
                return $.grep(states, function (state) {
                    return !state.abstract && state.data.type === parent.data.type && !state.data.disabled;
                });
            }
            var getInnreMenuItems = function (states) {
                scope.newMenuItems.filter(function (parent) {
                    parent.open = false;
                    parent.inner = getChildrensForParent(parent, states);
                    ///CustomDisabled => remove items if loged in
                    parent.inner = $.grep(parent.inner, function (inner) {
                        return inner.data.CustomDisabled === undefined || (inner.data.CustomDisabled === true && !scope.$$credentials.loggedIn);
                    });
                });
            }

            ///check the time from bild menu to current, allow 15m` till re-bild
            scope.checkMenuTimer = function (menuTimer) {
                var time = new Date(), time = (time.getHours() * 60) + time.getMinutes();
                if (menuTimer === null || menuTimer < 1 || (time - menuTimer) > 15) {
                    helper.menu.MenuTimer = time;
                    return false;
                }
                return true;
            }
            ///calc witch menu to show baset on window
            scope.subscribe('showMobileMenu', function (state) {
                if (typeof state !== 'undefined' && typeof state === 'boolean')
                    scope.showMobileMenu = state;
                else
                    scope.showMobileMenu = !scope.showMobileMenu;
            });
            ///subscribe to menu directive - get the login state
            scope.TransmitLoginState = function () {
                if (!scope.GotUserSatet) {
                    $timeout(function () { scope.publish('TransmitLoginState', scope.GotUserSatet); }, 15);
                }
            };
            ///on chenge of rout open/close the menu
            scope.$watchCollection('$state.$current.path', function (newValue, oldValue) {
                if (newValue === oldValue || newValue === null || newValue.abstract) {
                    return void 0;
                }
                var newRout = newValue[newValue.length - 1].data;
                if (scope.$$childHead.$open && newRout.type !== 'Custom') scope.$$childHead.$open = false;
                if (!scope.$$childHead.$open && newRout.type === 'Custom') scope.$$childHead.$open = true;

                scope.newMenuItems.filter(function (state) {
                    state.open = state.data.type === newRout.type ? true : false;
                });
            }, true);
            ///subscribe to login/logout event
            scope.subscribe('VerifayLoginState', function ($credentials) {
                scope.GotUserSatet = true;
                scope.buildMenuItems().then(function (result) { }, function (error) { });
            });
            scope.subscribe('ChangeMunicipality', function (data) {
                scope.buildMenuItems(true).then(function (result) { }, function (error) { });
            });
            ///main function for $state validation
            scope.$watchCollection('$state.current', function (newValue, oldValue) {
                if (newValue === oldValue || newValue === null || newValue.abstract) {
                    return void 0;
                }
                ///check solution version
                scope.publish('$$SVDcall', true);
                scope.showMobileMenu = false;
                ///if user is logged-in or open rout do nothing
                if (validateAuthentication(newValue.data, scope.$$credentials) || validateUserGroups(scope.$$credentials, newValue.data))
                    return void [0];

                angular.forEach($http.pendingRequests, function (request) {
                    request.$canceller.resolve();
                });
                //if user not logged-in AND is restricted rout return to safety
                if (!scope.$$credentials.loggedIn) {
                    $state.go('Home');
                    toastrService.info('כניסה מותרת למשתמש רשום בלבד!', 'שטח אסור');
                } else {
                    $state.go('Home');
                    toastrService.info('', 'הרשאה לא תקינה!');
                }

            }, true);
            ///open close the menu
            scope.OpemMenuControl = function (menuItem) {
                if ($window.outerWidth < 601) {
                    $.map(scope.newMenuItems, function (state) {
                        if (state.data.type === menuItem) {
                            state.open = !state.open;
                        } else {
                            state.open = false;
                        }
                    });
                } else {
                    $.map(scope.newMenuItems, function (state) {
                        if (state.data.type === menuItem)
                            state.open = !state.open;
                    });
                }
            };

            ///muin entry poit -> start the menu from hear
            scope.buildMenuItems = function (forceUpdate) {
                return $q(function (resolve, reject) {
                    if (helper.menu.logInState === scope.$$credentials.loggedIn && helper.menu.newMenuItems.length > 0 && scope.checkMenuTimer(helper.menu.MenuTimer) &&
                        scope.newMenuItems.length === helper.menu.newMenuItems.length && forceUpdate !== true) {
                        return reject('reject');
                    }
                    var states = angular.copy($state.get());
                    scope.newMenuItems = getMenuParents(states);
                    getInnreMenuItems(states);
                    validateMenuParents();

                    helper.menu.logInState = scope.$$credentials.loggedIn;
                    helper.menu.newMenuItems = scope.newMenuItems;
                    return resolve('resolve');
                });
            }

            $(window).on("resize.doResize", _.debounce(function () {
                if (!helper.doReCompile(scope))
                    return void [0];
                scope.buildMenuItems(true).then(function (result) { }, function (error) { });

            }, 150));

            ///setup the menu AND user state
            scope.buildMenuItems().then(function (result) { }, function (error) { });

            scope.TransmitLoginState();
        }
    }

})();
(function () {

    angular.module('WorkerPortfolioApp').directive('municipalityMenu', MunicipalityMenu);

    MunicipalityMenu.$inject = ['$window', '$rootScope', '$timeout', '$state', 'toastrService', '$http'];

    function MunicipalityMenu($window, $rootScope, $timeout, $state, toastrService, $http) {
        var helper = {
           
        };
        var directive = {
            link: link,
            restrict: 'AE',
            templateUrl: '../../Scripts/views/ng-addons/municipality-menu.panel.html',
            replace: true,
            scope: true
        };
        return directive;

        function link(scope, element, attrs) {
            scope.$open = false;
            scope.OpemMenu = function () {
                scope.$open = !scope.$open;
            }

            scope.currentMunicipality = new Object();
            scope.menuTitle = '';
            scope.templates = new Array();

            scope.getCurrentMunicipalityData = function (municipalities, currentMunicipalityId) {
                return municipalities.filter(function (municipality) {
                    return municipality.ID == currentMunicipalityId;
                })[0];
            }
            scope.StartDirective = function (credentials) {
                scope.templates = new Array();
                scope.AllowMunicipalityMenu = credentials.loggedIn && credentials.isAllowMunicipalityMenu;

                if (scope.AllowMunicipalityMenu) {
                    scope.currentMunicipality = scope.getCurrentMunicipalityData(credentials.municipalities, credentials.currentMunicipalityId);
                    scope.menuTitle = scope.currentMunicipality.municipalityMenuTitle === null || scope.currentMunicipality.municipalityMenuTitle.isempty() ? 'מיוחד !!!' : scope.currentMunicipality.municipalityMenuTitle;
                    scope.templates = scope.currentMunicipality.htmlTemplates !== null && scope.currentMunicipality.htmlTemplates.length > 0 ? scope.currentMunicipality.htmlTemplates : new Array();

                    if (scope.templates === null || scope.templates.length < 1) {
                        scope.AllowMunicipalityMenu = false;
                    }
                }
            }
            
            scope.subscribe('VerifayLoginState', function ($credentials) {
                scope.StartDirective($credentials)
            });

            scope.StartDirective(scope.$$credentials);
        }
    }

})();
(function() {    

    angular.module('WorkerPortfolioApp').directive('ngslider', NgSlider);

    NgSlider.$inject = ['$window'];
    
    function NgSlider($window) {
        var directive = {
            link: link,
            restrict: 'A',
            scope:
			{
			    rule: "=",
			    classes: "@"
			},
        };
        return directive;

        function link(scope, element, attrs) {
            scope.AddindClass = typeof scope.classes !== 'undefined' && !scope.classes.isempty();
                        
            scope.$watch('rule', function (newModel, oldModel) {
                if (newModel === true) {
                    element.removeClass('slideup').addClass("slidedown").delay(750).queue(function () {
                        scope.AddindClass ? $(this).addClass(scope.classes).addClass("overflowinherit").dequeue() : $(this).addClass("overflowinherit").dequeue();
                    });
                } else {
                    if (scope.AddindClass) {
                        element.removeClass(scope.classes).delay(50).queue(function () {
                            element.removeClass("overflowinherit").delay(50).queue(function () {
                                $(this).removeClass('slidedown').addClass("slideup").dequeue();
                            });
                            $(this).dequeue();
                        });
                    } else {
                        element.removeClass('slidedown').addClass("slideup").delay(750).queue(function () {
                            $(this).removeClass("overflowinherit").dequeue();
                        });
                    }
                    
                }
            });
        }
    }

})();
(function() {    

    angular.module('WorkerPortfolioApp').directive('ngThumb', ['$window', '$sce', function ($window, $sce) {
        var helper = {
            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
            isFile: function(item) {
                return angular.isObject(item) && item instanceof $window.File;
            },
            isImage: function(file) {
                var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            },
            isPdf: function (file) {
                var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|pdf|'.indexOf(type) !== -1;
            },
            b64toBlob: function (b64Data, contentType) {
                contentType = contentType || '';
                var sliceSize = 512;
                b64Data = b64Data.replace(/^[^,]+,/, '');
                b64Data = b64Data.replace(/\s/g, '');
                var byteCharacters = window.atob(b64Data);
                var byteArrays = [];

                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

                var blob = new Blob(byteArrays, { type: contentType });
                return blob;
            }
        };

        return {
            restrict: 'A',
            template: '<canvas/>',
            link: function (scope, element, attributes) {
                if (!helper.support) return;

                var params = scope.$eval(attributes.ngThumb);
                
                if (!helper.isFile(params.file)) return;
                if (!helper.isImage(params.file)) {
                    element.remove();
                    return;
                }

                var canvas = element.find('canvas');
                var embed = element.find('embed');
                var reader = new FileReader();
                
                reader.onload = onLoadFile;
                reader.readAsDataURL(params.file);

                function onLoadFile(event) {
                    var img = new Image();
                    img.onload = onLoadImage;
                    img.src = event.target.result;
                }
                function onLoadPdf(event) {
                    var file = helper.b64toBlob(PayrollData.data.fileString, 'application/pdf');
                    var fileURL = URL.createObjectURL(file);
                    scope.content = $sce.trustAsResourceUrl(fileURL);
                }
                function onLoadImage() {
                    var width = params.width || this.width / this.height * params.height;
                    var height = params.height || this.height / this.width * params.width;
                    canvas.attr({ width: width, height: height });
                    canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                }
            }
        };
    }]);


})();
(function () {

    angular.module('WorkerPortfolioApp').directive('pdfObjectScale', pdfObjectScale);

    pdfObjectScale.$inject = ['$window', '$compile', '$rootScope', '$state'];

    function pdfObjectScale($window, $compile, $rootScope, $state) {
        var helper = {
            detectBrowser: function () {
                var userAgent = $window.navigator.userAgent;
                var browsers = { chrome: /chrome/i, safari: /safari/i, firefox: /firefox/i, ie: /internet explorer/i };
                for (var key in browsers) {
                    if (browsers[key].test(userAgent)) {
                        return key;
                    }
                };
                if (parseInt(document.documentMode) > 0)
                    return 'IE';

                return 'undefined';
            },
            templateIE: '<div ng-hide="pdfDataStatus == 406"><iframe id="template" ng-src="{{ngUrl}}" width="100%" height="100%" style="width:100%;height:100%;min-height:{{minHeight}}px;"></iframe></div>',
            templateMobile: '<div ng-hide="pdfDataStatus == 406" template-mobile><img ng-src="data:image/png;base64,{{fileImageBlob}}" style="width:100%;height:100%;" /></div>',
            templateChrom: '<object id="template" ng-bind="contentView" ng-show="pdfShow" data="{{pdfContent}}#{{contentView}}" style="width:100%;height:100%;min-height:{{minHeight}}px;" type="application/pdf" title="654">\
                                    <p>\
                                        It appears you don`t have a PDF plugin for this browser.\
                                        No problem though...\
                                        You can <a href="{{ngUrl}}">click here to download the PDF</a>.\
                                    </p>\
                                </object>',
            currentTemplate: undefined,
            getCurrentTemplate: function () {
                if ($window.innerWidth < 801) {
                    helper.currentTemplate = helper.templateMobile;
                    return helper.currentTemplate;
                }
                switch (helper.detectBrowser().toLowerCase()) {
                    case 'ie':
                        helper.currentTemplate = helper.templateIE;
                        break;
                    default:
                        helper.currentTemplate = helper.templateChrom;
                        break;
                }
                return helper.currentTemplate;
            },
            browserType: function () {
                if ($window.innerWidth < 801)
                    return 5;

                switch (helper.detectBrowser().toLowerCase()) {
                    case 'ie':
                        return 1;
                    default:
                        return 0;
                }
            },
            setUpUrl: function (controllerState, browserType) {
                if (browserType == 0) {
                    return '';
                }
                var _url = '/home/' + (browserType == 5 ? 'Convert' : '');
                switch (controllerState.toLowerCase()) {
                    case 'paycheck':
                        _url += 'PaycheckResponse?';
                        break;
                    case '106form':
                        _url += 'AnnualFormResponse?';
                        break;
                }

                return _url;
            },
            randomizer: function () {
                return '&V=' + parseInt(Math.random() * 10000);
            }
        };
        var directive = {
            link: link,
            restrict: 'EA',
            replace: true,
            template: function () {
                return helper.currentTemplate || helper.getCurrentTemplate();
            },
            scope: {
                pdfContent: '=pdfContent',
                pdfShow: '=pdfShow',
                pdfDate: '=pdfDate',
                pdfControllerState: '=pdfControllerState',
                pdfDataStatus: '=pdfDataStatus',
                fileImageBlob: '=fileImageBlob',
            }
        };
        return directive;

        function link(scope, element, attrs) {
            scope.ngUrl = '';
            scope.doResize = true;
                                    
            scope.resetDoResize = function () {
                scope.doResize = true;
                var checkDate = scope.pdfControllerState === 'Paycheck' ? new Date(2015, 6, 31) : new Date(2014, 11, 31);
                if (scope.pdfDate < checkDate)
                    scope.doResize = false;
            }

            scope.Browser = helper.browserType();
            var credentials = typeof $rootScope.$$credentials !== 'undefined' ? $rootScope.$$credentials : $rootScope.lsGet('$$credentials');
            
            scope.ngUrl = helper.setUpUrl(scope.pdfControllerState, scope.Browser) + ['idNumber=' + credentials.idNumber, 'year=' + scope.pdfDate.getFullYear(), 'month=' + (scope.pdfDate.getMonth() + 1)].join('&') + helper.randomizer();
            
            scope.contentView = '';
            scope.$Width = $window.innerWidth;
            scope.$Height = $window.innerHeight;

            scope.$watch('pdfDate', function (newValue, oldValue) {
                if (oldValue === newValue || !oldValue || !newValue) {
                    return void [0];
                }
                
                scope.ngUrl = helper.setUpUrl(scope.pdfControllerState, scope.Browser) + ['idNumber=' + credentials.idNumber, 'year=' + scope.pdfDate.getFullYear(), 'month=' + (scope.pdfDate.getMonth() + 1)].join('&') + helper.randomizer();

                if (scope.Browser === 1) {
                    element.children().remove();
                    setTimeout(function () {
                        scope.$apply(function () {
                            var content = $compile(helper.currentTemplate)(scope);
                            element.append(content);
                        });
                        pdfView();
                    }, 125);
                }
            }, true);

            var scaleWidthSwitch = function (width) {
                var w = parseInt(width / 50) - 28;
                w = w < 0 ? 0 : (w > 12 ? 12 : w);
                w = w > 0 ? (103 + (5 * (w - 1))) : 0;
                return w;
            }
            var pdfView = function (scale) {
                if (typeof scale === 'undefined')
                    scale = 'view=Fit';

                var w = scaleWidthSwitch(scope.$Width);
                if (w <= 0)
                    scale = 'view=Fit';
                else if (scope.doResize)
                    scale = 'zoom=' + w;

                scope.contentView = scale;

                scope.minHeight = $window.innerWidth < 801 ? parseInt(scope.$Height / 1.15) : parseInt(scope.$Height * 1.15);
                if (scope.Browser === 1)
                    element.find('iframe').attr('style', 'width:100%;height:100%;min-height:' + scope.minHeight + 'px;');
            }

            $(window).on("resize.doResize", _.debounce(function () {
                scope.$Width = $window.innerWidth;
                scope.$Height = $window.innerHeight;
                if (scope.Browser === 1) {
                    scope.$apply(function () {
                        var content = $compile(helper.currentTemplate)(scope);
                        element.append(content);
                    });
                }
                pdfView();
            }, 150));

            scope.resetDoResize();
            pdfView();

            scope.subscribe('PdfContect.Changed', function (goTo) {
                scope.resetDoResize();
                pdfView();
            });
        }
    }
})();
(function () {
    SignaturePad.prototype._strokeWidth = function (velocity) {
        return 6;
    };

    angular.module('WorkerPortfolioApp').directive('digitalSignaturePad', DigitalSignaturePad);

    DigitalSignaturePad.$inject = ['$window', '$timeout'];

    function DigitalSignaturePad($window, $timeout) {
        var directive = {
            link: link,
            restrict: 'EA',
            templateUrl: '../../Scripts/views/ng-addons/signature-pad.html',
            replace: true,
            scope: {
                signatureModel: '='
            }
        };
        return directive;

        function link(scope, element, attrs) {
            var mobileSize = 1024;

            var wrapper = document.getElementById("signature-pad"),
            canvas = wrapper.querySelector("canvas.canvas"),
            bkCanvas = wrapper.querySelector("canvas.bk-canvas"),
            parent = $('#signature-pad');

            function setBackground() {
                var ctx = bkCanvas.getContext("2d");
                ctx.strokeStyle = "#CCCCCC";///stroke color
                ctx.lineWidth = 3;
                ctx.beginPath();
                ctx.moveTo(30, canvas.height - 30);
                ctx.lineTo(canvas.width - 30, canvas.height - 30);
                ctx.stroke();

                var x = canvas.width - 57, y = canvas.height - 57;
                ctx.lineWidth = 6;

                if (window.innerWidth >= mobileSize)
                    drawX(ctx, x, y);

            }
            function drawX(ctx, x, y) {
                ctx.beginPath();

                ctx.moveTo(x - 20, y - 20);
                ctx.lineTo(x + 20, y + 20);

                ctx.moveTo(x + 20, y - 20);
                ctx.lineTo(x - 20, y + 20);
                ctx.stroke();
            }

            scope.signatureModel = new SignaturePad(canvas, { dotSize: 30 });
            scope.BK_signatureModel = new SignaturePad(bkCanvas);

            scope.ClearPad = function () {
                scope.signatureModel.clear();
                setBackground();
            }

            // Adjust canvas coordinate space taking into account pixel ratio,
            // to make it look crisp on mobile devices.
            // This also causes canvas to be cleared.
            function resizeCanvas() {
                // When zoomed out to less than 100%, for some very strange reason,
                // some browsers report devicePixelRatio as less than 1
                // and only part of the canvas is cleared then.
                var ratio = 1,//Math.max(window.devicePixelRatio || 1, 1),
                    width = window.innerWidth < mobileSize ? (parent.width()) : (parent.width() - 42),
                    height = window.innerWidth < mobileSize ? (parent.height()) : (parent.height() - 84);
                
                if ((width / 2) > height && window.innerWidth < mobileSize)
                    height = (width / 2);
                
                canvas.width = width;
                canvas.height = height;
                canvas.style.width = width + 'px';
                canvas.style.height = height + 'px';

                canvas.getContext("2d").scale(ratio, ratio);

                bkCanvas.width = width;
                bkCanvas.height = height;
                bkCanvas.style.width = width + 'px';
                bkCanvas.style.height = height + 'px';
                bkCanvas.getContext("2d").scale(ratio, ratio);
                scope.ClearPad();
            }

            window.onresize = resizeCanvas;
            resizeCanvas();

        }
    }

})();
(function() {
    

    angular.module('WorkerPortfolioApp').directive('tooltip', tooltip);

    tooltip.$inject = ['$window'];
    
    function tooltip($window) {
        var directive = {
            link: link,
            restrict: 'EA',
            replace: true,
            template: '<div class=popover>\
                            <div class=arrow></div>\
                            <h3 class="popover-title" ng-bind-html="title" ng-show="title"></h3>\
                            <div class="popover-content" ng-bind-html=content></div>\
                        </div>',
            scope: {
                title: "@title",
                content: "@content",
            },
        };
        return directive;

        function link(scope, element, attrs) {
            
        }
    }

})();



(function () {
    angular.module('WorkerPortfolioApp').directive('uiDatepicker', uiDatepicker);

    uiDatepicker.$inject = ['$timeout'];

    function uiDatepicker($timeout) {
        var directive = {
            link: link,
            restrict: 'EA',
            scope: {
                starterDate: '=ngModel',
                isNewOptions: '=newOptions',
            },
            require: "ngModel",
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            ///remove autocomplete attr
            element.attr('autocomplete', 'off');

            var updateModel = function (dateText) {
                var date = dateText.split('/');
                date = new Date([date[1], date[0], date[2]].join('/'));
                var valid = !(date == 'Invalid Date');

                scope.$apply(function () {
                    ngModel.$setValidity('custom', valid);
                    ngModel.$setViewValue(dateText);
                });
            };

            var options = {
                constrainInput: true,
                changeYear: true,
                changeMonth: true,
                isRTL: true,
                autoSize: true,

                currentText: "Now",
                buttonImage: 'calendar.gif',
                dateFormat: "dd/mm/yy",
                nextText: '',
                prevText: '',
                yearRange: "-100:+100",
                dayNames: ["ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת"],
                dayNamesMin: ["א'", "ב'", "ג'", "ד'", "ה'", "ו'", "שבת"],
                monthNames: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"],
                monthNamesShort: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"],

                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };
            
            if (scope.isNewOptions != undefined) {
                $.map(scope.isNewOptions, function (val, key) {
                    options[key] = val;
                });
                
                element.datepicker(options)
                .on("input change", function (e) {
                    updateModel(e.target.value);
                });
            }
            else {
                element.datepicker(options)
                .on("input change", function (e) {
                    updateModel(e.target.value);
                });
            }
        }
    }

})();
(function () {    

    angular.module('WorkerPortfolioApp').directive('yearPicker', yearPicker);

    yearPicker.$inject = ['$parse'];

    function yearPicker($parse) {
        var directive = {
            link: link,
            restrict: 'E',
            templateUrl: '../../Scripts/views/ng-addons/year-picker.template.html',
            replace: true,
            require: 'ngModel',
            scope: {
                onSelect: '&',
                SlectedYear: '=slectedYear',
            }
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            scope.attrs = attrs
            scope.ngModel = ngModel;
            
            scope.shownYear = "";
            scope.open = false;
            scope.years = [];
            
            scope.openYearPicker = function () {
                scope.open = !scope.open;
                if (scope.years.length === 0) {
                    scope.years = _.range(scope.SlectedYear - 11, scope.SlectedYear + 1);
                }
            }

            scope.selectYear = function (y) {
                scope.SlectedYear = y;
                scope.ngModel.$viewValue = y;
                scope.ngModel.$commitViewValue();
                
                scope.onSelect()();

                scope.open = false;
            }
            scope.yearBefore = function () {
                var year = scope.years[scope.years.length - 2];
                scope.years = _.range(year - 11, year + 1);
            }
            scope.yearAfter = function () {
                var year = scope.years[scope.years.length - 1] + 1;
                if (year <= new Date().getFullYear()) {
                    scope.years = _.range(year - 11, year + 1);
                }
            }
            scope.clear = function () {
                scope.open = false;
            }
            scope.setYear = function (year) {
                scope.SlectedYear = parseInt(year);
                scope.shownYear = year.toString();
            }
            scope.yearChange = function (value) {
                value = (scope.SlectedYear + (value));
                if (value > new Date().getFullYear()) {
                    return void [0];
                }
                scope.selectYear(value);
            }
        }
    }

})();
(function() {

    angular.module('WorkerPortfolioApp').directive('ngdate', NgDate);

    NgDate.$inject = ['$timeout'];
    
    function NgDate($timeout) {
        var directive = {
            link: link,
            restrict: 'A',
            require: "ngModel",
            scope: {},
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            var validDate = function (val, isRequired) {
                try {
                    if (isRequired !== true && (val == null || String(val).trim() == ''))
                        return true;

                    var validity = new RegExp(/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/);
                    val = String(val);
                    if (!validity.test(val))
                        return false;

                    return true;
                } catch (e) {
                    return false;
                }
            }

            scope.validate = function () {
                ngModel.$setValidity('custom', true);
                
                if (!validDate(ngModel.$modelValue, ngModel.$$attr.required)) {
                    element.addClass('ng-invalid-custom');
                    ngModel.$setValidity('custom', false);
                }
                scope.$apply();
            }

            element.on('focus', function () {
                ngModel.$setValidity('custom', true);
                element.removeClass('ng-invalid-custom');
            });

            element.on('blur', function () {
                $timeout(scope.validate, 200);
            });
        }
    }

})();
(function() {

    angular.module('WorkerPortfolioApp').directive('idnumber', IDNumber);

    IDNumber.$inject = [];
    
    function IDNumber() {
        var directive = {
            link: link,
            restrict: 'A',
            require: "ngModel",
            scope: {},
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            var sumDigits = function (digits) {
                digits = String(digits).split('');
                var sum = 0;
                _.each(digits, function (elm) {
                    sum += Number(elm);
                });
                return sum;
            }
            ///validate is correct isreali id-number
            var validIDnumber = function (val) {
                try {
                    if (scope.requiredInput && (typeof val === 'undefined' || val === null || val.length == 0) || scope.InpDisabled)
                        return true;
                    var _val = String(val);
                    val = parseInt(val);
                    var validity = new RegExp(/^\d{5,9}$/);
                    if (isNaN(val) || !validity.test(_val) || _val.length < 9 || val <= 0)
                        return false;
                    while (_val.length < 9)
                        _val = '0' + _val;
                    var numbers = _val.split(''), checkDigit = numbers.splice(8, 1).join('');
                    var counter = 0, incNum;
                    for (var i in numbers) {
                        incNum = Number(numbers[i]) * ((i % 2) + 1);    ///multiply digit by 1 or 2
                        counter += sumDigits(incNum);                   ///sum the digits up and add to counter
                    }
                    ///check counter and checkDigit;
                    var res = ((counter.roundTenUp() % 10 == 0) && (counter.roundTenUp() === (Number(counter) + Number(checkDigit))));
                    return res;
                } catch (e) {
                    return false;
                }
            }
            
            element.on('focus', function () {
                scope.requiredInput = typeof ngModel.$validators.required === 'function';
                scope.InpDisabled = attrs.disabled;

                ngModel.$setValidity('custom', true);
                scope.$apply();
            });
                        
            element.on('blur', function () {
                ngModel.$setValidity('custom', true);
                scope.requiredInput = typeof ngModel.$validators.required === 'function';
                scope.InpDisabled = attrs.disabled;

                var value = ngModel.$modelValue;
                if (!validIDnumber(value)) {
                    ngModel.$setValidity('custom', false);
                }
                scope.$apply();
            });
        }
    }

})();
function IeVersion() {
    //Set defaults
    var value = {
        IsIE: false,
        TrueVersion: 0,
        ActingVersion: 0,
        CompatibilityMode: false
    };

    //Try to find the Trident version number
    var trident = navigator.userAgent.match(/Trident\/(\d+)/);
    if (trident) {
        value.IsIE = true;
        //Convert from the Trident version number to the IE version number
        value.TrueVersion = parseInt(trident[1], 10) + 4;
    }

    //Try to find the MSIE number
    var msie = navigator.userAgent.match(/MSIE (\d+)/);
    if (msie) {
        value.IsIE = true;
        //Find the IE version number from the user agent string
        value.ActingVersion = parseInt(msie[1]);
    } else {
        //Must be IE 11 in "edge" mode
        value.ActingVersion = value.TrueVersion;
    }

    //If we have both a Trident and MSIE version number, see if they're different
    if (value.IsIE && value.TrueVersion > 0 && value.ActingVersion > 0) {
        //In compatibility mode if the trident number doesn't match up with the MSIE number
        value.CompatibilityMode = value.TrueVersion != value.ActingVersion;
    }
    return value;
}
(function () {
    
    var ie = IeVersion();
    if (ie.IsIE && ((ie.CompatibilityMode && ie.ActingVersion < 10) || ie.TrueVersion < 10)) {
        if (window.location.pathname.toLowerCase() !== '/error') {
            var href = window.location.protocol + '//' + window.location.host + '/error';
            window.location.href = href;
        }
        var title1 = 'הנך משתמש בדפדפן `internet explorer` בגרסה : ' + ie.TrueVersion + ' , בתצוגת תאימות לגרסה: ' + ie.ActingVersion + '<br> עליך לבטל את תצוגת התאימות לאתר `תיק עובד`!';
        if (ie.TrueVersion < 10)
            title1 = 'עליך לעדכן את הדפדפן שברשותך לגרסה 10 לפחות על מנת לעבוד באתר `תיק עובד`!';
        var title2 = 'באפשרותך ליצור קשר עם מנהל האתר לעזרה בכתובת:';

        document.getElementById('title1').innerHTML = title1;
        document.getElementById('title2').innerHTML = title2;
    }
})();
(function() {
    angular.module('WorkerPortfolioApp').directive('ngmultiple', NgMultiple);

    NgMultiple.$inject = ['$window', '$rootScope'];
    
    function NgMultiple($window, $rootScope) {
        var directive = {
            link: link,
            restrict: 'A',
            require: "ngModel",
            scope: {},
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            $rootScope.lsBind($rootScope, '$$TofesModel', ngModel);

            element.on('change', function (e) {
                ngModel.$setValidity('custom', true);
            });
        }
    }

})();
(function() {
    angular.module('WorkerPortfolioApp').directive("ngSubmit", function () {
        return {
            require: "?form",
            priority: 10,
            link: {
                pre: function(scope, element, attrs, form) {
                    element.on("submit", function(event) {        
                        element.find('input[IDNumber]').each(function (i, e) {
                            $(e).trigger('blur');
                        });
                    })
                }
            }
        }
    });
})();
(function() {
    angular.module('WorkerPortfolioApp').directive('numbersinput', NumbersInput);

    NumbersInput.$inject = [];
    
    function NumbersInput() {
        var directive = {
            link: link,
            restrict: 'A',
            require: "ngModel"
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            var validateNumbersOnly = function (val) {
                if (typeof val === 'number' && !isNaN(val))
                    return false;
                if (typeof val === 'undefined' || val.toString().trim().isempty())
                    return true;
                val = val.toString().trim();
                return val.replace(/[0-9.-]/ig, '').length > 0 || val.replace(/[^-]/ig, '').length > 1 || val.replace(/[^.]/ig, '').length > 1;
            }
                        
            element.on('focus', function () {
                ngModel.$setValidity('custom', true);
            });

            element.on('blur', function () {
                ngModel.$setValidity('custom', true);
                
                if (validateNumbersOnly(ngModel.$modelValue)) {
                    ngModel.$setValidity('custom', false);
                }
                scope.$apply();
            });

        }
    }

})();
(function() {
    angular.module('WorkerPortfolioApp').directive('password', Password);

    Password.$inject = ['$window', 'toastrService'];
    
    function Password($window, toastrService) {
        var keyCode = {
            32 : "רווח",
            81: "/",
            87: "'",
            187 : "+",
            191: "/?",
            111: "/",
            192: "` ~",
            193 : "?, / or °",
            220 : "\\",
            222 : "' \"",
            223 : "`",
        };
        var directive = {
            link: link,
            restrict: 'A',
            scope: {},
        };
        return directive;

        function link(scope, element, attrs) {
            var EN = new RegExp("^[a-zA-Z;<>.,]+$");
            element.on('keydown', function (e) {
                if ([32, 51, 81, 87, 111, 197, 191, 192, 220, 222, 223].indexOf(e.keyCode) > -1) {
                    switch (e.key.toLowerCase()) {
                        case 'q':
                            break;
                        case 'w':
                            break;
                        case '3':
                            break;
                        default:
                            toastrService.error('ערך לא חוקי ` XXX `'.replace('XXX', e.key), null, { timeOut: 5500, extendedTimeOut: 150, progressBar: false });
                            e.preventDefault();
                            return;
                            break;
                    }
                }
                if ((e.keyCode >= 65 && e.keyCode <= 90 && !EN.test(e.key)) || ([186, 188, 190].indexOf(e.keyCode) > -1 && !EN.test(e.key))) {
                    toastrService.error('יש להכניס אותיות לועזיות בלבד.', null, { timeOut: 5500, extendedTimeOut: 150, progressBar: false });
                    e.preventDefault();
                    return;
                }                
            });
        }
    }

})();
(function() {
    angular.module('WorkerPortfolioApp').directive('phone', Phone);

    Phone.$inject = [];
    
    function Phone() {
        var directive = {
            link: link,
            restrict: 'A',
            require: "ngModel",
            scope: {},
        };
        return directive;

        function link(scope, element, attrs, ngModel) {            
            scope.IsRequired = typeof attrs.required !== 'undefined' && attrs.required === true

            var validPhonenumber = function (val) {
                try {
                    val = String(val);
                    if (!scope.IsRequired && val.trim() == '')
                        return true;

                    var validity = new RegExp(/^((\+972|972)|0)( |-)?([1-468-9]( |-)?\d{7}|(5|7)[0-9]( |-)?\d{7})$/);
                    if (!validity.test(val))
                        return false;

                    return true;
                } catch (e) {
                    return false;
                }
            }

            element.on('focus', function () {
                ngModel.$setValidity('custom', true);
            });

            element.on('blur', function () {
                ngModel.$setValidity('custom', true);
                var value = ngModel.$modelValue;
                if (!validPhonenumber(value)) {
                    ngModel.$setValidity('custom', false);
                }
                scope.$apply();
            });
        }
    }

})();
(function () {
    angular.module('WorkerPortfolioApp').directive('solutionVersionDirective', solutionVersionDirective);

    solutionVersionDirective.$inject = ['$window', '$timeout', '$cookies'];

    function solutionVersionDirective($window, $timeout, $cookies) {
        var directive = {
            link: link,
            restrict: 'A',
            scope: true,
        };
        return directive;

        function link(scope, element, attrs) {
            scope.subscribe('511_responseError', function (action) {
                $rootScope.lsClearAll();
                scope.GetSBVN();
            });

            scope.GetSBVN = function () {
                var sbvn = $rootScope.lsGet('$SolutionBuildVersionNumber');
                if (sbvn === null || sbvn == undefined || sbvn !== _SolutionBuildVersionNumber) {
                    $rootScope.lsClearAll();
                    var obj = $cookies.getAll();
                    $.map(obj, function (value, key) {
                        $cookies.remove(key);
                    });
                    var obj = $cookies.getAll();
                    $.map(obj, function (value, key) {
                        $cookies.remove(key);
                    });

                    $rootScope.lsSet('$SolutionBuildVersionNumber', _SolutionBuildVersionNumber);
                    scope.publish('CatchTransmitLogout', false, false);
                    window.location.reload(true);
                }
            }
            
            $timeout(function () {
                scope.GetSBVN();
            }, 15);

            scope.subscribe('$$SVDcall', function (start) {
                scope.GetSBVN();
            });
        }
    }

})();
(function () {
    

    angular.module('WorkerPortfolioApp').service('localStorageServiceExtenders', ['$window', 'localStorageService', 'FileUploader', function ($window, localStorageService, FileUploader) {

        return { Initialize: Initialize };

        function Initialize(scope) {
            ///Directly adds a value to local storage, Returns: Boolean
            scope.constructor.prototype.lsSet = scope.constructor.prototype.lsSet
             || function (key, val) {
                 return localStorageService.set(key, val);
             }
            ///Directly get a value from local storage, Returns: value from local storage
            scope.constructor.prototype.lsGet = scope.constructor.prototype.lsGet
             || function (key) {
                 return localStorageService.get(key);
             }

            ///Return array of keys for local storage, ignore keys that not owned, Returns: value from local storage
            scope.constructor.prototype.lsKeys = scope.constructor.prototype.lsKeys
             || function () {
                 return localStorageService.keys();
             }

            ///Remove an item(s) from local storage by key, Returns: Boolean
            scope.constructor.prototype.lsRemove = scope.constructor.prototype.lsRemove
             || function (key) {
                 return localStorageService.remove(key);
             }
            ///Remove all data for this app from local storage, Returns: Boolean
            ///Note: Optionally takes a regular expression string and removes matching.
            scope.constructor.prototype.lsClearAll = scope.constructor.prototype.lsClearAll
             || function (key) {
                 typeof key !== 'undefined' ? localStorageService.clearAll(key) : localStorageService.clearAll();
             }
            ///Bind $scope key to localStorageService, Returns: deregistration function for this listener.
            ///Usage: localStorageService.bind(property, value[optional], key[optional]) key: The corresponding key used in local storage 
            scope.constructor.prototype.lsBind = scope.constructor.prototype.lsBind
             || function ($scope, property, value) {
                 localStorageService.set(property, value);
                 ///set the coresponding unbind prop
                 $scope[property + '_unbind'] = localStorageService.bind($scope, property);
             }
            
        }
    }]).run(function ($rootScope, localStorageServiceExtenders) {
        localStorageServiceExtenders.Initialize($rootScope);
    });
})();

(function () {
    angular.module('WorkerPortfolioApp').service('AccessabilityService', ['$rootScope', 'Title', function ($rootScope, Title) {

        return { Initialize: Initialize };

        function Initialize(scope) {
            scope.Title = Title;
            window.$rootScope = $rootScope;

            scope.constructor.prototype.accessability = scope.constructor.prototype.accessability
             || {
                 show: false,
                 definitions: (function () {
                     var def = $rootScope.lsGet('accessability');
                     if (!!def)
                         return def;
                     return {
                         Size: 'regular',
                     };
                 })(),
                 open: function () {
                     this.show = !this.show;
                 },
                 handleFontsLarg: function () {
                     var that = this;
                     this.definitions.Size = 'large';
                     $rootScope.lsSet("definitions", that.definitions);
                     this.doAccessabilityAction();
                 },
                 handleFontsMedium: function () {
                     var that = this;
                     this.definitions.Size = 'medium';
                     $rootScope.lsSet("definitions", that.definitions);
                     this.doAccessabilityAction();
                 },
                 handleFontsRegular: function () {
                     var that = this;
                     this.definitions.Size = 'regular';
                     $rootScope.lsSet("definitions", that.definitions);
                     this.doAccessabilityAction();
                 },
                 handleContrast: function () {
                     var that = this;
                     this.definitions.contrast = !this.definitions.contrast;
                     $rootScope.lsSet("definitions", that.definitions);
                     this.doAccessabilityAction();
                 }
             }

        }
    }]).run(function ($rootScope, AccessabilityService) {
        AccessabilityService.Initialize($rootScope);
    });
})();
(function () {
    var AdminFactory = angular.module('AdminFactory', []);

    AdminFactory.factory('Admin', ['$http', '$cookies',
      function ($http, $cookies) {
          var setAction = function (action, params, type) {
              var baseUrl = 'api/admin';
              type = typeof type !== 'undefined' && type !== null ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              } else {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return baseUrl + (typeof action !== 'undefined' && action !== null && action.length > 0 ? '/' + action : '');
          },
          encryptInformation = function (text, actionType) {
              if (actionType !== false)
                  actionType = true;
              var keys = $cookies.get('.MVCSPECN' + _SolutionBuildVersionNumber).split(',');
              text = String(text);
              var key = CryptoJS.enc.Utf8.parse(keys[0]);
              var iv = CryptoJS.enc.Utf8.parse(keys[1]);
              var encrypted = actionType !== false ? CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), key,
                  {
                      keySize: 128 / 8,
                      iv: iv,
                      mode: CryptoJS.mode.CBC,
                      padding: CryptoJS.pad.Pkcs7
                  }) : null;

              var decrypted = actionType === false ? CryptoJS.AES.decrypt(text, key, {
                  keySize: 128 / 8,
                  iv: iv,
                  mode: CryptoJS.mode.CBC,
                  padding: CryptoJS.pad.Pkcs7
              }).toString(CryptoJS.enc.Utf8) : null;

              return actionType ? String(encrypted) : String(decrypted);
          },
          encryptIDnumber = function (data) {
              var _data = angular.copy(data);
              _data.idNumber = encryptInformation(_data.idNumber);
              _data.calculatedIdNumber = encryptInformation(_data.calculatedIdNumber);
              if (_data.requiredRole != undefined && _data.requiredRole != null) _data.requiredRole = encryptInformation(_data.requiredRole);
              _data.isEncrypted = true;
              return _data;
          }

          return {
              SearchByIdNumber: function (data, query, results) {
                  data.idNumber = encryptInformation(data.idNumber);

                  return $http({
                      method: 'POST',
                      url: setAction('Idnumber/[query]/[results]', { results: results, query: query }, 'rest'),
                      data: JSON.stringify(data),
                      headers: {
                          'Accept': "application/json; charset=utf-8",
                          "X-Search": '1'
                      }
                  });
              },
              ValidUser: function (data) {
                  return $http.post(setAction("ValidUser"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              SearchUsers: function (data) {
                  return $http.post(setAction("SearchUsers"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetUserById: function (data) {
                  return $http.post(setAction("GetUserById"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              ResetPassword: function (data) {
                  var _data = encryptIDnumber(data);
                  _data.password = encryptInformation(_data.password);
                  return $http.post(setAction("ResetPassword"), JSON.stringify(_data), "json", "application/json; charset=utf-8");
              },
              SendUserReset: function (data) {
                  return $http.post(setAction("SendUserReset"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetMunicipalities: function (data) {
                  return $http.post(setAction("GetMunicipalities"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetSingleMunicipaly: function (data) {
                  return $http.post(setAction("GetSingleMunicipaly"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              UpdateAuthorizationList: function (data) {
                  return $http.put(setAction("UpdateAuthorizations"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              UpdateMunicipalities: function (data) {
                  return $http.put(setAction("UpdateMunicipalities"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetGroups: function (data) {
                  return $http.post(setAction("GetGroups"), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              UpdateUserGroups: function (data) {
                  return $http.post(setAction("UpdateUserGroups"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetUEPStatistics: function (data) {
                  return $http.post(setAction("UEPStatistics"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetForm101Statistics: function (data) {
                  return $http.post(setAction("Form101Statistics"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetLogInLogStatistics: function (data) {
                  return $http.post(setAction("LogInLogStatistics"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              GetDailyUsagetatistics: function (data) {
                  return $http.post(setAction("DailyUsagetatistics"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              ResetPayrollDisplay: function (data) {
                  return $http.patch(setAction("ResetUserPayrollDisplay"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              DeleteHRContact: function (data) {
                  data.idNumber = encryptInformation(angular.copy(data.idNumber));
                  return $http.delete(setAction('DeleteHRContact/[idNumber]/[contactId]', data, 'rest'), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              UpdateContact: function (data) {
                  return $http.put(setAction("UpdateHRContact"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              CheckUserSmsMessages: function (data) {
                  return $http.post(setAction("UserMessages"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              CheckUserMailMessages: function (data) {
                  return $http.post(setAction("UserMailMessages"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              }
          }

      }]);

})();
(function() {
    

    angular.module('WorkerPortfolioApp').service('BusService', function () {

        return { Initialize: Initialize };

        function Initialize(scope) {
            //Keep a dictionary to store the events and its subscriptions
            var publishEventMap = {};

            //Register publish events
            scope.constructor.prototype.publish = scope.constructor.prototype.publish
             || function () {
                 var _thisScope = this,
                     handlers,
                     args,
                     evnt;
                 //Get event and rest of the data
                 args = [].slice.call(arguments);
                 evnt = args.splice(0, 1);
                 //Loop though each handlerMap and invoke the handler
                 angular.forEach((publishEventMap[evnt] || []), function (handlerMap) {
                     handlerMap.handler.apply(_thisScope, args);
                 })
             }

            //Register Subscribe events
            scope.constructor.prototype.subscribe = scope.constructor.prototype.subscribe
               || function (evnt, handler) {
                   var _thisScope = this,
                       handlers = (publishEventMap[evnt] = publishEventMap[evnt] || []);

                   //Just keep the scopeid for reference later for cleanup
                   handlers.push({ $id: _thisScope.$id, handler: handler });
                   //When scope is destroy remove the handlers that it has subscribed.  
                   _thisScope.$on('$destroy', function () {
                       for (var i = 0, l = handlers.length; i < l; i++) {
                           if (handlers[i].$id === _thisScope.$id) {
                               handlers.splice(i, 1);
                               break;
                           }
                       }
                   });
               }

        }
    }).run(function ($rootScope, BusService) {
        BusService.Initialize($rootScope);
    });

})();
(function () {

    var FileManagerFactory = angular.module('FileManagerFactory', []);

    FileManagerFactory.factory('FileManager', ['$http', '$cookies',
      function ($http, $cookies) {
          var setAction = function (action, params, type) {
              var baseUrl = 'filemanager';
              type = typeof type !== 'undefined' && type !== null ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              } else {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return baseUrl + (typeof action !== 'undefined' && action !== null && action.length > 0 ? '/' + action : '');
          },
          encryptInformation = function (text, actionType) {
              if (actionType !== false)
                  actionType = true;
              var keys = $cookies.get('.MVCSPECN' + _SolutionBuildVersionNumber).split(',');
              text = String(text);
              var key = CryptoJS.enc.Utf8.parse(keys[0]);
              var iv = CryptoJS.enc.Utf8.parse(keys[1]);
              var encrypted = actionType !== false ? CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), key,
                  {
                      keySize: 128 / 8,
                      iv: iv,
                      mode: CryptoJS.mode.CBC,
                      padding: CryptoJS.pad.Pkcs7
                  }) : null;

              var decrypted = actionType === false ? CryptoJS.AES.decrypt(text, key, {
                  keySize: 128 / 8,
                  iv: iv,
                  mode: CryptoJS.mode.CBC,
                  padding: CryptoJS.pad.Pkcs7
              }).toString(CryptoJS.enc.Utf8) : null;

              return actionType ? String(encrypted) : String(decrypted);
          },
          encryptWSModel = function (data) {
              var _data = angular.copy(data);
              _data.idNumber = encryptInformation(data.idNumber);
              _data.calculatedIdNumber = encryptInformation(data.calculatedIdNumber);
              if (_data.requiredRole != undefined && _data.requiredRole != null) _data.requiredRole = encryptInformation(_data.requiredRole);
              _data.isEncrypted = true;
              return _data;
          }

          return {
              encrypt: function (text) {
                  return encryptInformation(angular.copy(text));
              },
              GetFileStructure: function (data) {
                  return $http.post(setAction('GetFileStructure'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              UpdateFolder: function (data) {
                  data.wsModel = encryptWSModel(data.wsModel);
                  return $http.post(setAction('UpdateFolder'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              AddFolder: function (data) {
                  data.wsModel = encryptWSModel(data.wsModel);
                  return $http.put(setAction('AddNewFolder'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              DeleteFolder: function (data) {
                  data.wsModel = encryptWSModel(data.wsModel);
                  return $http.patch(setAction('DeleteFolder'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              DeleteImage: function (data) {
                  data.idNumber = encryptInformation(angular.copy(data.idNumber));
                  return $http.delete(setAction('DeleteImage/[imageId]/[idNumber]', data, 'rest'), "json", "application/json; charset=utf-8");
              },
              UpdateImage: function (data) {
                  data.wsModel = encryptWSModel(data.wsModel);
                  return $http.post(setAction('UpdateImage'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetEmailTemplates: function (data) {
                  return $http.post(setAction('GetEmailTemplates'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              UpdateAddTemplate: function (data) {
                  data.wsModel = encryptWSModel(data.wsModel);
                  return $http.post(setAction('UpdateTemplate'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              DeleteTemplate: function (data) {
                  data.wsModel = encryptWSModel(data.wsModel);
                  return $http.patch(setAction('DeleteTemplate'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetHtmlTemplates: function (data) {
                  return $http.post(setAction('GetHtmlTemplates'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              GetPage: function (data) {
                  data.wsModel = encryptWSModel(data.wsModel);
                  return $http.post(setAction('GetPageById'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              }
          }

      }]);

})();
(function () {
    angular.module('WorkerPortfolioApp').factory('fileUploadeFactory', FileUploadeFactory);

    FileUploadeFactory.$inject = ['$http'];

    function FileUploadeFactory($http) {
        ///set action and get/rest params
        var setAction = function (action, params, type) {
            var baseUrl = 'api/FilesUploades';
            type = typeof type !== 'undefined' ? type.toLowerCase() : 'get';
            if (!params instanceof Object) params = new Object();
            if (type === 'get') {
                for (var i in params)
                    action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

            } else {
                for (var i in params)
                    action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                action = action.replace(/\/\[([^)]+)\]/ig, '');
            }

            return baseUrl + (typeof action !== 'undefined' && action.length > 0 ? '/' + action : '');
        }
        ///do the ajax request
        var request = function (req) {
            return $.ajax(req);
        }

        return {
            Signature: function (idNumber, year, base64, token, municipalityId) {
                var SignatureModel = {
                    idNumber: idNumber,
                    year: year,
                    base64: base64,
                    fileName: 'userSignature.png',
                    Token: token,
                    municipalityId: municipalityId
                };
                return $http.post(setAction('SaveBase64'), JSON.stringify(SignatureModel), "json", "application/json; charset=utf-8");
            },
            SaveForm: function (data) {
                return $http.put(setAction('createForm101'), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            SendForm: function (data) {
                return $http.post(setAction('SendFormToWS'), JSON.stringify(data), "json", "application/json; charset=utf-8");
            }
        }
    }
})();
(function () {
    
    /// GENERAL FILTERS AND FUNCTION for the `FileUploader` element
    angular.module('WorkerPortfolioApp').service('FileUploaderServiceExtenders', ['toastrService', 'UserService', function (toastrService, UserService) {

        return { Initialize: Initialize };

        function Initialize(scope) {
            
            /* **** FUNCTIONS **** */
            ///file uploader validation
            scope.constructor.prototype._UploaderValidation = scope.constructor.prototype._UploaderValidation
            || function () {
                if (this.getNotUploadedItems().length > 0) {
                    toastrService.info('יש לבצע שמירה/הסרה של הקבצים.', 'שם לב! יש קבצים שלא הועלו לשרת!',
                        { clearThenShow: true, allowHtml: true, timeOut: 9999, closeButton: true });                    
                    return true;
                }
                return false;
            }
            ///function to call after file add fail
            scope.constructor.prototype._onWhenAddingFileFailed = scope.constructor.prototype._onWhenAddingFileFailed
             || function (item, filter, options) {
                 var errorText = filter.errorText, errorTitle = filter.errorTitle;
                 switch (filter.name) {
                     case 'sizeFilter':
                         this.maxFileSize = typeof this.maxFileSize === 'undefined' ? 4 : this.maxFileSize;
                         errorTitle = errorTitle.replace(/\{0}/gi, item.name);
                         errorText = errorText.replace(/\{0}/gi, this.maxFileSize);
                         break;
                     case 'uniqueKey':
                         errorTitle = errorTitle.replace(/\{0}/gi, item.name);
                         break;
                     case 'queueLimit':
                         errorTitle = errorTitle.replace(/\{0}/gi, this.queueLimit);
                         break;
                     case 'FileType':
                         errorTitle = errorTitle.replace(/\{0}/gi, item.name.split('.')[1].trim().toLowerCase());
                         errorText = errorText.replace(/\{0}/gi, this.fileTypes);
                         break;
                     default:
                         break;
                 }
                 toastrService.error(errorText, errorTitle);
             }
            ///function to call after file saved to server
            scope.constructor.prototype._onCompleteItem = scope.constructor.prototype._onCompleteItem
             || function (fileItem, response, status, headers) {
                 ///if getting user access exception do logout
                 if (status == 500 && response.ExceptionMessage === "UserdetailsAccessException") {
                     $rootScope.publish('CatchTransmitLogout');
                 }

                 if (typeof this.successItemsList === 'undefined')
                     this.successItemsList = [];
                 if (typeof this.successList === 'undefined')
                     this.successList = [];
                 if (typeof this.errorList === 'undefined')
                     this.errorList = [];
                 if (typeof response.imageId !== 'undefined')
                     fileItem.imageId = response.imageId;
                 ///add/remove files from `idNumberFiles`
                 if (response.success) {///file save success
                     this.successList.push(fileItem.file.name);
                     this.successItemsList.push(response.path);
                 } else {///file save error
                     fileItem.isUploaded = false;
                     this.errorList.push(fileItem.file.name);
                 }
             }
            ///function to call after all files saved to server
            scope.constructor.prototype._onCompleteAll = scope.constructor.prototype._onCompleteAll
             || function (fileItem, response, status, headers) {
                 var msg = '';
                 if (this.successList && this.successList.length > 0) {
                     msg = this.successList.join(', ') + (this.successList.length > 1 ? '\n\r נטענו בהצלחה\n\r' : '\n\r נטען בהצלחה\n\r');
                     toastrService.success(msg, 'הצלחה!', { timeOut: 3322 });
                 }
                 if (this.errorList && this.errorList.length > 0) {
                     msg = this.errorList.join(', ') + (this.errorList.length > 1 ? '\n\r נכשלו בטעינה\n\r' : '\n נכשל בטעינה\n\r');
                     toastrService.warning(msg, 'שגיאה!', { timeOut: 3322 });
                 }
                 this.successList = [];
                 this.errorList = [];
             }
            scope.constructor.prototype._removeFromQueue = scope.constructor.prototype._removeFromQueue
             || function (value) {
                 var self = this;
                 var index = self.getIndexOfItem(value);
                 var item = self.queue[index];
                 if (item.isUploading) item.cancel();
                 self.queue.splice(index, 1);
                 ///function to remove from server
                 if (typeof self.removeFromServer === 'undefined')
                     self.removeFromServer = scope._removeFromServer;
                 
                 if (typeof self.successItemsList !== 'undefined') {
                     self.successItemsList = $.grep(this.successItemsList, function (file) {
                         var fl = file.split('\\');
                         if (item.file.name === fl[fl.length - 1])
                            self.removeFromServer(file);
                         return item.file.name !== fl[fl.length - 1];
                     });                     
                 }

                 item._destroy();
                 self.progress = self._getTotalProgress();
             }
            scope.constructor.prototype._removeFromServer = scope.constructor.prototype._removeFromServer
             || function (item) {
                 UserService.DeleteFile({ file: item })
                    .then(function successCallback(response) {
                    }, function errorCallback(response) {
                        //console.log('full-error', response);
                        toastrService.error('הקובץ לא נמחק מהשרת', 'תקלת מערכת!');
                    });
             }
            /* **** FORM 101 FUNCTIONS **** */
            ///function to call after add file
            scope.constructor.prototype._onAfterAddingFile = scope.constructor.prototype._onAfterAddingFile
             || function (fileItem) {
                 if (this.Files !== undefined) this.Files++;
                 else this.Files = 1;

                 var WSPostModel = $rootScope.getForm101Year();

                 fileItem.formData = [
                     { filename: this.alias + '_' + (this.queue.length) + '' },
                     { userIDNumber: WSPostModel.idNumber },
                     { year: WSPostModel.selectedDate.getFullYear() },
                     { originalName: fileItem.file.name }
                 ];
             }
            /* **** FORM 101 FUNCTIONS **** */
            /* **** FUNCTIONS **** */

            /* **** FILTERS **** */
            ///limit the size of the FileUploader queue
            scope.constructor.prototype.queueLimitFilter = {
                name: 'queueLimit',
                errorTitle: 'ניתן לטעון עד {0} קבצים!',
                errorText: 'יש להסיר קובץ מהרשימה לפני הוספת קובץ חדש.',
                fn: function (item) {
                    return this.queue.length < this.queueLimit;
                }
            }
            ///check that each file is unique
            scope.constructor.prototype.uniqueKeyFilter = {
                name: 'uniqueKey',
                errorTitle: '{0}',
                errorText: 'כבר מסומן לטעינה לשרת!',
                fn: function (item) {
                    var insert = true;
                    this.queue.filter(function (FileItem) {
                        if (item.lastModified === FileItem._file.lastModified && item.size === FileItem._file.size && item.type === FileItem._file.type && item.name === FileItem._file.name)
                            insert = false;
                    });
                    return insert;
                }
            }
            ///check file size limit
            scope.constructor.prototype.sizeFilter = {
                name: 'sizeFilter',
                errorTitle: 'גודל הקובץ המצורף {0} חורג מן הגבול המותר',
                errorText: 'גודל מותר הינו עד {0}MB',
                fn: function (item) {
                    this.maxFileSize = typeof this.maxFileSize === 'undefined' ? 4 : this.maxFileSize;
                    return (item.size / 1024 / 1024).toFixed(2) <= this.maxFileSize;
                }
            }
            ///file type filter
            scope.constructor.prototype.typeFilter = {
                name: 'FileType',
                errorTitle: 'סוג הקובץ [{0}] לא נתמך!',
                errorText: 'ניתן לטעון לשרת קבצים \n מסוגים {0} בלבד.',
                fn: function (item) {
                    
                    if (typeof this.fileTypes === 'undefined')
                        this.fileTypes = ["jpg", "png", "gif", "tif", "jpeg", "pdf"];
                    if (this.fileTypes === true)
                        return true;
                    var type = '';
                    if (item.type && item.type.indexOf('/') > 0) {
                        type = item.type.split('/')[1].trim().toLowerCase();
                    }
                    else {
                        var count = (item.name.match(/\./g) || []).length;
                        type = item.name.split('.');
                        type = type[count].trim().toLowerCase();
                    }
                    return this.fileTypes.indexOf(type) >= 0;
                }
            }
            ///file type filter
            scope.constructor.prototype.imagesFilter = {
                name: 'FileType',
                errorTitle: 'סוג הקובץ [{0}] לא נתמך!',
                errorText: 'ניתן לטעון לשרת קבצים \n מסוגים {0} בלבד.',
                fn: function (item) {

                    if (typeof this.fileTypes === 'undefined')
                        this.fileTypes = ["jpg", "png", "gif", "jpeg"];
                    if (this.fileTypes === true)
                        return true;
                    var type = '';
                    if (item.type && item.type.indexOf('/') > 0) {
                        type = item.type.split('/')[1].trim().toLowerCase();
                    }
                    else {
                        var count = (item.name.match(/\./g) || []).length;
                        type = item.name.split('.');
                        type = type[count].trim().toLowerCase();
                    }
                    return this.fileTypes.indexOf(type) >= 0;
                }
            }
            /* **** FILTERS **** */
        }
    }]).run(function ($rootScope, FileUploaderServiceExtenders) {
        FileUploaderServiceExtenders.Initialize($rootScope);
    });

})();
(function () {
    angular.module('WorkerPortfolioApp').factory('fileUploade', FileUploade);

    FileUploade.$inject = ['$http'];

    function FileUploade($http) {
        var setAction = function (action, params, type) {
            type = typeof type !== 'undefined' ? type.toLowerCase() : 'get';
            if (!params instanceof Object) params = new Object();
            if (type === 'get') {
                for (var i in params)
                    action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

            } else {
                for (var i in params)
                    action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                action = action.replace(/\/\[([^)]+)\]/ig, '');
            }

            return '/api/FilesUploades' + (typeof action !== 'undefined' && action.length > 0 ? '/' + action : '');
        }

        return {
            SaveSignature: function (_idNumber, _year, _base64) {
                var SignatureModel = { idNumber: _idNumber, year: parseInt(_year), base64: _base64 };
                return $http.post(setAction('PostB64Test'), JSON.stringify(SignatureModel), "json", "application/json; charset=utf-8");
            },
            _Get: function (pageId) {
                return $http.get(setAction('GetQuestions', { pageId: pageId }), "json", "application/json; charset=utf-8");
            }
        }
    }
})();
(function () {
    angular.module('WorkerPortfolioApp').service('IdleService', ['Idle', 'Title', '$timeout', '$cookies', function (Idle, Title, $timeout, $cookies) {

        return { Initialize: Initialize };
        function pad(val) {
            return val < 10 ? ('0' + String(val)) : String(val);
        }
        function timeStamp() {
            var t = new Date();
            return pad(t.getMinutes()) + ':' + pad(t.getSeconds()) + '.' + t.getMilliseconds();
        }

        function Initialize(scope) {
            var DecryptCooky = function (reTry) {
                var tiket = $cookies.get('.MVCTET' + _SolutionBuildVersionNumber);
                var keys = $cookies.get('.MVCSPECN' + _SolutionBuildVersionNumber);
                if (tiket === undefined || tiket === null || keys === undefined || keys === null) {
                    ///try to wait for cooky data once
                    if (reTry !== true) {
                        $timeout(function () {
                            DecryptCooky(true);
                        }, 450);
                        return void [0];
                    }
                    ///logout
                    scope.publish('CatchTransmitLogout', false, true);
                    return void [0];
                }
                keys = keys.split(',');
                var key = CryptoJS.enc.Utf8.parse(keys[0]);
                var iv = CryptoJS.enc.Utf8.parse(keys[1]);
                try {
                    var decrypted = CryptoJS.AES.decrypt(tiket, key, {
                        keySize: 128 / 8,
                        iv: iv,
                        mode: CryptoJS.mode.CBC,
                        padding: CryptoJS.pad.Pkcs7
                    }).toString(CryptoJS.enc.Utf8);
                    
                    if (new Date(decrypted) <= new Date()) {
                        ///logout
                        scope.publish('CatchTransmitLogout', false, true);
                        return void [0];
                    }
                } catch (ex) {
                    console.log('ex', ex);
                }
            };
            scope.gotUserState = false;

            ///start the directive
            scope.getLoginState = function () {
                if (!scope.gotUserState) {
                    $timeout(function () {
                        scope.publish('TransmitLoginState', scope.gotUserState);
                    }, 100);
                }
            }
            scope.subscribe('VerifayLoginState', function ($credentials) {
                scope.gotUserState = true;
                if ($credentials.loggedIn && !Idle.running()) {
                    ///start the `idle` directive
                    Idle.watch();
                    ///check server side login     
                    $timeout(function () {
                        DecryptCooky();
                    }, 450);

                } else if (!$credentials.loggedIn) {
                    ///stop the `idle` directive
                    Idle.unwatch();
                }
            });

            scope.$on('IdleTimeout', function (e) {
                // the user has timed out transmit logout msg                
                scope.publish('CatchTransmitLogout', false, true);
                $timeout(function () { Title.restore(); }, 5000);
            });

            scope.$on('Keepalive', function (e) {
                ///check every [5m] the log in state
                scope.publish('TransmitLoginState', scope.gotUserState);
            });

            scope.getLoginState();
            Title.idleMessage("{{minutes}}:{{seconds}} לניתוק מהמערכת!");
            Title.timedOutMessage('נותקת מהמערכת!');
        }
    }]).run(function ($rootScope, IdleService) {
        IdleService.Initialize($rootScope);
    });

})();
(function () {
    var LoginService = angular.module('LoginService', []);

    LoginService.factory('Credentials', ['$http', '$cookies',
      function ($http, $cookies) {
          var setAction = function (action, params, type, isAnonymous) {
              var baseUrl = '/api/' + (isAnonymous ? 'AnonymousAccount' : 'Account');
              type = typeof type !== 'undefined' ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  for (var i in params)
                      action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              } else {
                  for (var i in params)
                      action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return baseUrl + (typeof action !== 'undefined' && action.length > 0 ? '/' + action : '');
          },
          encryptInformation = function (text, actionType) {
              if (actionType !== false)
                  actionType = true;
              var keys = $cookies.get('.MVCSPECN' + _SolutionBuildVersionNumber).split(',');
              text = String(text);
              var key = CryptoJS.enc.Utf8.parse(keys[0]);
              var iv = CryptoJS.enc.Utf8.parse(keys[1]);
              var encrypted = actionType !== false ? CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), key,
                  {
                      keySize: 128 / 8,
                      iv: iv,
                      mode: CryptoJS.mode.CBC,
                      padding: CryptoJS.pad.Pkcs7
                  }) : null;

              var decrypted = actionType === false ? CryptoJS.AES.decrypt(text, key, {
                  keySize: 128 / 8,
                  iv: iv,
                  mode: CryptoJS.mode.CBC,
                  padding: CryptoJS.pad.Pkcs7
              }).toString(CryptoJS.enc.Utf8) : null;

              return actionType ? String(encrypted) : String(decrypted);
          },
          encryptLogIn = function (data) {
              data.idNumber = encryptInformation(data.idNumber);
              data.password = encryptInformation(data.password);
              return {
                  key: '',
                  iv: '',
                  idNumber: data.idNumber,
                  password: data.password
              };
          },
          encryptWSModel = function (data) {
              var _data = angular.copy(data);
              _data.idNumber = encryptInformation(data.idNumber);
              _data.calculatedIdNumber = encryptInformation(data.calculatedIdNumber);
              if (_data.requiredRole != undefined && _data.requiredRole != null) _data.requiredRole = encryptInformation(_data.requiredRole);
              _data.isEncrypted = true;
              return _data;
          }

          return {
              EncryptData: function (text) {
                  return encryptInformation(text);
              },
              GetFullUserdetails: function (data) {
                  return $http.post(setAction('GetFullUserdetails'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              UpdateUserDetails: function (data) {
                  return $http.put(setAction('UpdateUserDetails'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetUserdetails: function (data) {
                  return $http.post(setAction('Userdetails'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              GetUserPartnerDetails: function (data) {
                  return $http.post(setAction('PartnerDetails'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              GetEducationStatus: function (data) {
                  return $http.post(setAction('EducationStatus'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              GetUserChildren: function (data) {
                  return $http.post(setAction('UserChildren'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              Login: function (data) {
                  return $http.post(setAction('login', undefined, undefined, true), JSON.stringify(encryptLogIn(angular.copy(data))), "json", "application/json; charset=utf-8");
              },
              LogOut: function (data) {
                  return $http.get(setAction('LogOut', undefined, undefined, true), "json", "application/json; charset=utf-8");
              },
              ResetPassword: function (data) {
                  return $http.post(setAction('[idNumber]/[mail]/[sms]', data, 'rest', true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              ChangePasswordFromToken: function (data) {
                  return $http.post(setAction('ChangePasswordFromToken', undefined, undefined, true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetLocalPasswordSettings: function (data) {
                  return $http.get(setAction('GetLocalPasswordSettings', undefined, undefined, true), "json", "application/json; charset=utf-8");
              },
              GetCurrentUser: function (data) {
                  return $http.post(setAction('[authenticationToken]', data, 'rest', true), "json", "application/json; charset=utf-8");
              },
              GetCurrentUserForProcesses: function (data) {
                  return $http.post(setAction('Kad/' + '[authenticationToken]/[rashut]/[mncplity]', data, 'rest', true), "json", "application/json; charset=utf-8");
              },
              AuthenticateUser: function (data) {
                  return $http.get(setAction('[authenticationToken]', data, 'rest', true), "json", "application/json; charset=utf-8");
              },
              CreateUser: function (data) {
                  return $http.put(setAction(undefined, undefined, undefined, true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              RegisterFromInitialPassword: function (data) {
                  return $http.put(setAction('RegisterFromInitialPassword', undefined, undefined, true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              RegisterFromSms: function (data) {
                  return $http.put(setAction('RegisterFromSms', undefined, undefined, true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              VerifySmsToken: function (data) {
                  return $http.post(setAction('VerifySmsToken', undefined, undefined, true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              RegisterFromEmail: function (data) {
                  return $http.put(setAction('RegisterFromEmail', undefined, undefined, true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              CheckValidUser: function (data) {
                  return $http.put(setAction('CheckRegister', undefined, undefined, true), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              ChangeMunicipality: function (data) {
                  return $http.post(setAction("ChangeMunicipality"), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              RefreshUserData: function (data) {
                  return $http({
                      method: 'POST',
                      url: setAction('UpdateUserData'),
                      data: JSON.stringify(encryptWSModel(data)),
                      headers: {
                          'Accept': "application/json; charset=utf-8",
                          "X-Search": '1'
                      }
                  });
              },
              InitialActivationCheck: function (data) {
                  data.idNumber = encryptInformation(data.idNumber);
                  return $http.post(setAction('CheckInitialActivation', undefined, undefined, true), JSON.stringify(angular.copy(data)), "json", "application/json; charset=utf-8");
              },
              GetUPCData: function (data) {
                  return $http.post(setAction('GetUPCData', undefined, undefined, false), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              GetUserUpdateTypes: function (data) {
                  return $http.post(setAction('UserUpdateTypes', undefined, undefined, false), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              },
              GetSynerionRedirect: function (data) {
                  return $http.patch(setAction('GetRedirectInfo'), JSON.stringify(encryptWSModel(data)), "json", "application/json; charset=utf-8");
              }
          }

      }]);

})();
(function () {

    var PayCheckEmailFactory = angular.module('PayCheckEmailFactory', []);

    PayCheckEmailFactory.factory('PayCheckEmail', ['$http', '$cookies',
      function ($http, $cookies) {
          var setAction = function (action, params, type) {
              var baseUrl = 'api/WebService';
              type = typeof type !== 'undefined' && type !== null ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              }
              if (type === 'rest') {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return baseUrl + (typeof action !== 'undefined' && action !== null && action.length > 0 ? '/' + action : '');
          },
          encryptInformation = function (text, actionType) {
              if (actionType !== false)
                  actionType = true;
              var keys = $cookies.get('.MVCSPECN' + _SolutionBuildVersionNumber).split(',');
              text = String(text);
              var key = CryptoJS.enc.Utf8.parse(keys[0]);
              var iv = CryptoJS.enc.Utf8.parse(keys[1]);
              var encrypted = actionType !== false ? CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), key,
                  {
                      keySize: 128 / 8,
                      iv: iv,
                      mode: CryptoJS.mode.CBC,
                      padding: CryptoJS.pad.Pkcs7
                  }) : null;

              var decrypted = actionType === false ? CryptoJS.AES.decrypt(text, key, {
                  keySize: 128 / 8,
                  iv: iv,
                  mode: CryptoJS.mode.CBC,
                  padding: CryptoJS.pad.Pkcs7
              }).toString(CryptoJS.enc.Utf8) : null;

              return actionType ? String(encrypted) : String(decrypted);
          },
          encryptIDnumber = function (data) {
              var _data = angular.copy(data);
              _data.idNumber = encryptInformation(_data.idNumber);
              _data.calculatedIdNumber = encryptInformation(_data.calculatedIdNumber);
              if (_data.requiredRole != undefined && _data.requiredRole != null) _data.requiredRole = encryptInformation(_data.requiredRole);
              return _data;
          },
          encryptWSPostExtensionModel = function (data) {
              var _data = angular.copy(data);
              _data.idNumber = encryptInformation(_data.idNumber);
              _data.calculatedIdNumber = encryptInformation(_data.calculatedIdNumber);
              if (_data.Cell != undefined && _data.Cell != null)
                  _data.Cell = encryptInformation(_data.Cell);
              if (_data.Email != undefined && _data.Email != null)
                  _data.Email = encryptInformation(_data.Email);
              if (_data.Token != undefined && _data.Token != null)
                  _data.Token = encryptInformation(_data.Token);
              if (_data.Password != undefined && _data.Password != null)
                  _data.Password = encryptInformation(_data.Password);
              if (_data.requiredRole != undefined && _data.requiredRole != null)
                  _data.requiredRole = encryptInformation(_data.requiredRole);

              return _data;
          }

          return {
              UserSendPayCheckToEmail: function (data) {
                  data.isEncrypted = true;
                  return $http.put(setAction("USPCTE"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              ValidateUserAndSendToken: function (data) {
                  data.isEncrypted = true;
                  return $http.put(setAction("ValidateUserAndSendToken"), JSON.stringify(encryptWSPostExtensionModel(data)), "json", "application/json; charset=utf-8");
              },
              UpdateUserData: function (data) {
                  data.isEncrypted = true;
                  return $http.put(setAction("UpdateUserData"), JSON.stringify(encryptWSPostExtensionModel(data)), "json", "application/json; charset=utf-8");
              },
              ValidateTokenAndUpdate: function (data) {
                  data.isEncrypted = true;
                  return $http.put(setAction("UpdateUserData"), JSON.stringify(encryptWSPostExtensionModel(data)), "json", "application/json; charset=utf-8");
              }
          }

      }]);

})();
(function () {
    

    var PayrollDataServise = angular.module('PayrollDataServise', []);

    PayrollDataServise.factory('PayrollServise', ['$http', '$q',
      function ($http, $q) {
          var setAction = function (action, params, type, baseUrl) {
              baseUrl = typeof baseUrl === 'string' ? baseUrl : '/api/WebService';
              type = typeof type !== 'undefined' ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  for (var i in params)
                      action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              } else {
                  for (var i in params)
                      action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return baseUrl + (typeof action !== 'undefined' && action.length > 0 ? '/' + action : '');
          }

          return {
              GeneralPayrollData: function (data) {
                  return $http.post(setAction('General'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              PayrollBySymbol: function (data) {
                  return $http.post(setAction('BySymbol'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetFundsTable: function (data) {
                  return $http.post(setAction('Funds'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              Get106Form: function (data) {
                  return $http.post(setAction('Form106'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetPaycheck: function (data) {
                  return $http.post(setAction('Paycheck'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetPersonalDetails: function (data) {
                  return $http.post(setAction('PersonalDetails'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetAttendanceFigures: function (data) {
                  return $http.post(setAction('AttendanceFigures'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              }
          }

      }]);

})();
(function () {

    var ProcessesFactory = angular.module('ProcessesService', []);

    ProcessesFactory.factory('Processes', ['$http', '$cookies', function ($http, $cookies) {

        var setAction = function (action, params, type) {
            var baseUrl = 'api/process';
            type = typeof type !== 'undefined' && type !== null ? type.toLowerCase() : 'get';
            if (!params instanceof Object) params = new Object();
            if (type === 'get') {
                if (typeof params !== 'undefined' && params !== null)
                    for (var i in params)
                        action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

            } else {
                if (typeof params !== 'undefined' && params !== null)
                    for (var i in params)
                        action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                action = action.replace(/\/\[([^)]+)\]/ig, '');
            }

            return baseUrl + (typeof action !== 'undefined' && action !== null && action.length > 0 ? '/' + action : '');
        },
         encryptInformation = function (text, actionType) {
             if (actionType !== false)
                 actionType = true;
             var keys = $cookies.get('.MVCSPECN' + _SolutionBuildVersionNumber).split(',');
             text = String(text);
             var key = CryptoJS.enc.Utf8.parse(keys[0]);
             var iv = CryptoJS.enc.Utf8.parse(keys[1]);
             var encrypted = actionType !== false ? CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), key,
                 {
                     keySize: 128 / 8,
                     iv: iv,
                     mode: CryptoJS.mode.CBC,
                     padding: CryptoJS.pad.Pkcs7
                 }) : null;

             var decrypted = actionType === false ? CryptoJS.AES.decrypt(text, key, {
                 keySize: 128 / 8,
                 iv: iv,
                 mode: CryptoJS.mode.CBC,
                 padding: CryptoJS.pad.Pkcs7
             }).toString(CryptoJS.enc.Utf8) : null;

             return actionType ? String(encrypted) : String(decrypted);
         },
          encryptIDnumber = function (data) {
              var _data = angular.copy(data);
              _data.idNumber = encryptInformation(_data.idNumber);
              _data.calculatedIdNumber = encryptInformation(_data.calculatedIdNumber);
              if (_data.requiredRole != undefined && _data.requiredRole != null) _data.requiredRole = encryptInformation(_data.requiredRole);
              _data.isEncrypted = true;
              return _data;
          }

        return {
            //new 11.06.2018:
            getRashutProcessesData: function (data) {
                return $http.post(setAction("GetAllProcessesData"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            //new 11.06.2018:
            getMishtamsheiKvuzaRegila: function (wsPostmodel, data) {
                return $http.post(setAction("GetMishtamsheiKvuza/[confirmationToken]/[idNumber]/[calculatedIdNumber]/[currentMunicipalityId]/[currentRashutId]", wsPostmodel, "rest"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            //new 11.06.2018:
            getMishtamsheiKvuzaDinamit: function (wsPostmodel, data) {
                return $http.post(setAction("GetMishtamsheiKvuzaDinamit/[confirmationToken]/[idNumber]/[calculatedIdNumber]/[currentMunicipalityId]/[currentRashutId]", wsPostmodel, "rest"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getRashutProcesses: function (data) {
                return $http.post(setAction("GetProcessesData"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getRashutSteps: function (data) {
                return $http.post(setAction("GetStepsData"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getUserProcesses: function (data) {
                return $http.post(setAction("GetMyProcessesData"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getUserRequestProcesses: function (data) {
                return $http.post(setAction("GetMyRequestProcesses"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getUsers: function (data) {
                return $http.post(setAction("GetUsers"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getUserOrganizationUnitsManagers: function (data) {
                return $http.post(setAction("getOrganizationUnitsManagers"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            postEmployeeVacationForm: function (vacationFormData) {
                return $http.post(setAction("PostVacationForm"), JSON.stringify(vacationFormData), "json", "application/json; charset=utf-8");
            },
            postEmployeeCourseForm: function (courseFormData) {
                return $http.post(setAction("PostCourseForm"), JSON.stringify(courseFormData), "json", "application/json; charset=utf-8");
            },
            saveProcess: function (process, wsPostmodel) {
                return $http.post(setAction("saveProcess/[confirmationToken]/[idNumber]/[calculatedIdNumber]", wsPostmodel, "rest"), JSON.stringify(process), "json", "application/json; charset=utf-8");
            },
            getAcademicInsts: function () {
                return $http.post(setAction("getAcademicInsts"), null, "json", "application/json; charset=utf-8");
            },
            getYehidaIrgunit: function (data) {
                return $http.post(setAction("getYehidaIrgunit"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getUserManagers: function (unitAndTree, user) {
                var data = {
                    model: user,
                    yehida: unitAndTree
                }
                return $http.post(setAction("getUserManagers"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getRepresentedWorkers: function (unitAndTree, user) {
                var data = {
                    model: user,
                    yehida: unitAndTree
                }
                return $http.post(setAction("getRepresentedWorkers"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getWorkerVacationData: function (wsModel, worker) {
                var data = {
                    model: wsModel,
                    worker: worker
                }
                return $http.post(setAction("getWorkerVacationData"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getWorkerManagers: function (wsModel, worker, wokerYahidaIrgunit) {
                var data = {
                    model: wsModel,
                    worker: worker,
                    units: wokerYahidaIrgunit
                }
                return $http.post(setAction("getWorkerManagers"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getWorkerYehidaIrgunit: function (wsModel, worker) {
                var data = {
                    model: wsModel,
                    worker: worker
                }
                return $http.post(setAction("getWorkerYehidaIrgunit"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getMyRecIdFromUsers: function (data) {
                return $http.post(setAction("getUserRecord"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            }
        }

    }]);

})();
(function () {
    

    angular.module('WorkerPortfolioApp').factory('formDataService', formDataService);

    formDataService.$inject = ['$http'];

    function formDataService($http) {

            var EmployerModel = {
                ID: 0, name: null, address: null, phoneNumber: null, deductionFileID: null, email: null, displayEmail: false, emailEmployee: false, emailEmployer: false, salary: 0, taxDeduction: 0, incomeType: null
            }
            , CityModel = {
                ID: 0, Name: null, EnglishName: null, Streets: new Array()
            }
            , StreetModel = {
                ID: 0, Name: null, Cities: new Array()
            }
            , ChildModel = {
                possession: false, childBenefit: false, ID: 0, firstName: null, lastName: null, idNumber: null, birthDate: null, immigrationDate: null, phone: null, cell: null, email: null
            }
            , IncomeModel = {
                startDate: null, incomeType: null, incomeTaxCredits: null, trainingFund: false, workDisability: false
            }
            , SignatureModel = {
                idNumber: null, year: null, base64: null, fileName: null, saved: false
            }
            , taxCoordinationModel = {
                requestCoordination: false, requestReason: 0, employers: new Array()
            }
            , taxExemptionModel = {
                blindDisabled: false, isResident: false, isImmigrant: false, immigrationStatus: 0, immigrationStatusDate: null, immigrationNoIncomeDate: null, spouseNoIncome: false, separatedParent: false, childrenInCare: false, infantchildren: false, singleParent: false, exAlimony: false, partnersUnder18: false, exServiceman: false, serviceStartDate: null, serviceEndDate: null, graduation: false, alimonyParticipates: false, settlement: null, startSettlementDate: null
            }
            , IncomeTypeModel = {
                monthSalary: false, anotherSalary: false, partialSalary: false, daySalary: false, allowance: false, stipend: false, anotherSource: false, anotherSourceDetails: null
            },
            EmployeeModel = {
                employerID: 0,
                employer: EmployerModel,
                cityID: 0,
                city: CityModel,
                streetID: 0,
                street: StreetModel,
                houseNumber: 0,
                zip: null,
                gender: false,
                maritalStatus: 0,
                ilResident: false,
                kibbutzResident: false,
                hmoMember: false,
                hmoName: null,
                year: null,
                signatureDate: null,
                signaturePath: null,
                children: new Array(),
                thisIncome: IncomeModel,
                otherIncomes: IncomeModel,
                signature: SignatureModel,
                taxCoordination: taxCoordinationModel,
                taxExemption: taxExemptionModel,
                ID: 0,
                firstName: null,
                lastName: null,
                idNumber: null,
                birthDate: null,
                immigrationDate: null,
                phone: null,
                cell: null,
                email: null
            }

            function updateFull(data, model) { }
            function updateEmployer(data, model) { }
            function updateCity(data, model) { }
            function updateStreet(data, model) { }
            function updateChild(data, model) { }
            function updateIncomeModel(data, model) { }
            function updateSignature(data, model) { }
            function updatetaxCoordination(data, model) { }
            function updatetaxExemptionModel(data, model) { }
            function updateIncomeType(data, model) { }
        return {
            GetFullModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateFull(data, EmployeeModel);
                return EmployeeModel;
            },
            GetEmployerModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateEmployer(data, EmployerModel);
                return EmployerModel;
            },
            GetCityModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateCity(data, CityModel);
                return CityModel;
            },
            GetStreetModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateStreet(data, StreetModel);
                return StreetModel;
            },
            GetChildModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateChild(data, ChildModel);
                return ChildModel;
            },
            GetIncomeModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateIncomeModel(data, IncomeModel);
                return IncomeModel;
            },
            GetSignatureModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateSignature(data, SignatureModel);
                return SignatureModel;
            },
            GettaxCoordinationModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updatetaxCoordination(data, taxCoordinationModel);
                return taxCoordinationModel;
            },
            GettaxExemptionModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updatetaxExemptionModel(data, taxExemption);
                return taxExemptionModel;
            },
            GetIncomeTypeModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateIncomeType(data, IncomeTypeModel);
                return IncomeTypeModel;
            }
        }
    }
})();
(function () {

    var SearchService = angular.module('SearchService', []);

    SearchService.factory('Search', ['$http',
      function ($http) {
          var setAction = function (action, params, type) {
              type = typeof type !== 'undefined' ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  for (var i in params)
                      action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              } else {
                  for (var i in params)
                      action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return '/api' + (typeof action !== 'undefined' && action.length > 0 ? '/' + action : '');
          },
          setUrl = function (action, params) {
              if (!params instanceof Object) params = new Object();
              for (var i in params) {
                  var prm = typeof params[i] === 'object' ? params[i].title : params[i];
                  action += '&' + i + '=' + prm;
              }
              return action;
          }

          return {
              citySearch: function (data) {
                  return $http({
                      method: 'POST',
                      url: setAction('CitySearch/[query]/[results]', data, 'rest'),
                      data: JSON.stringify(data),
                      headers: {
                          'Accept': "application/json; charset=utf-8",
                          "X-Search": '1'
                      }
                  });
              },
              streetByCityID: function (data) {
                  return $http({
                      method: 'POST',
                      url: setAction('StreetSearch/ByID/[query]/[cityid]/[results]', data, 'rest'),
                      data: JSON.stringify(data),
                      headers: {
                          'Accept': "application/json; charset=utf-8",
                          "X-Search": '1'
                      }
                  });
              },
              streetSearch: function (data) {
                  return $http({
                      method: 'POST',
                      url: setAction('StreetSearch/ByName/[city]/[query]/[results]', data, 'rest'),
                      data: JSON.stringify(data),
                      headers: {
                          'Accept': "application/json; charset=utf-8",
                          "X-Search": '1'
                      }
                  });
              },
              MunicipalitiesSearch: function (data) {
                  return $http({
                      method: 'POST',
                      url: setAction('CitySearch/GetMunicipalities/[query]/[results]', data, 'rest'),
                      data: JSON.stringify(data),
                      headers: {
                          'Accept': "application/json; charset=utf-8",
                          "X-Search": '1'
                      }
                  });
              },
              ZipSearch: function (data) {
                  return $http({
                      method: 'GET',
                      url: setUrl('https://www.israelpost.co.il/zip_data.nsf/SearchZip?OpenAgent', data),
                      headers: {
                          'Accept': "application/json; charset=utf-8"
                      },
                      XSearch: true
                  });
              }
          }

      }]);

})();
(function () {
    var StatisticsFactory = angular.module('StatisticsFactory', []);

    StatisticsFactory.factory('Statistics', ['$http', '$cookies',
      function ($http, $cookies) {
          var setAction = function (action, params, type) {
              var baseUrl = 'AdminStatistics';
              type = typeof type !== 'undefined' && type !== null ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              } else {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return baseUrl + (typeof action !== 'undefined' && action !== null && action.length > 0 ? '/' + action : '');
          },
          encryptInformation = function (text, actionType) {
              if (actionType !== false)
                  actionType = true;
              var keys = $cookies.get('.MVCSPECN' + _SolutionBuildVersionNumber).split(',');
              text = String(text);
              var key = CryptoJS.enc.Utf8.parse(keys[0]);
              var iv = CryptoJS.enc.Utf8.parse(keys[1]);
              var encrypted = actionType !== false ? CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), key,
                  {
                      keySize: 128 / 8,
                      iv: iv,
                      mode: CryptoJS.mode.CBC,
                      padding: CryptoJS.pad.Pkcs7
                  }) : null;

              var decrypted = actionType === false ? CryptoJS.AES.decrypt(text, key, {
                  keySize: 128 / 8,
                  iv: iv,
                  mode: CryptoJS.mode.CBC,
                  padding: CryptoJS.pad.Pkcs7
              }).toString(CryptoJS.enc.Utf8) : null;

              return actionType ? String(encrypted) : String(decrypted);
          },
          encryptIDnumber = function (data) {
              var _data = angular.copy(data);
              _data.idNumber = encryptInformation(_data.idNumber);
              _data.calculatedIdNumber = encryptInformation(_data.calculatedIdNumber);
              if (_data.requiredRole != undefined && _data.requiredRole != null) _data.requiredRole = encryptInformation(_data.requiredRole);
              _data.isEncrypted = true;
              return _data;
          }

          return {
              MunicipalityStatistics: function (data) {
                  return $http.post(setAction("MunicipalityFormStatistics"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              }
          }

      }]);

})();
(function () {
    angular.module('WorkerPortfolioApp').factory('TaxFromModelService', TaxFromModelService);

    TaxFromModelService.$inject = ['$window'];

    function TaxFromModelService($window) {
        var parseDate = function (val) {
            if (!checkIfHaveValue(val))
                return null;
            val = val.split('/');
            return new Date([val[1], val[0], val[2]].join('/'));
        },
        parsePostDate = function (val) {
            if (!checkIfHaveValue(val))
                return '';
            val = val.split('/');
            return new Date([val[1], val[0], val[2]].join('/') + ' 12:12');
        },
        checkIfHaveValue = function (value) {
            return typeof value != 'undefined' && value != null && value != '';
        }

        function copyGeneralData(model, modelData, currentMunicipalityId) {
            model.year = modelData.year;
            model.FormType = modelData.FormType;
            model.date = new Date();
            model.sign = modelData.sign;
            model.version = modelData.version;

            model.signatureDate = parsePostDate(modelData.signatureDate);
            model.signatureImageID = modelData.signatureImageID;
            model.emailEmployee = modelData.emailEmployee;
            model.municipalityId = currentMunicipalityId;

            model.FilesList = modelData.TofesControllerFilesList.concat(modelData.TaxExemptionControllerFilesList);
            model.FilesList = model.FilesList.concat(modelData.TaxAdjustmentControllerFilesList);

            model.pdf = modelData.formPdf;
            model.connectedImages = [];
            model.FilesList.filter(function (file) {
                model.connectedImages.push(
                    {
                        ID: file.imageId,
                        fileBlob: null,
                        fileDate: new Date(file.year, 0, 1),
                        fileName: file.filename,
                        fileString: null,
                        municipalityId: null,
                        type: null,
                        userId: null
                    }
                );
            });

            return model;
        }
        function copyChildren(childrenList) {
            var children = new Array();
            if (!checkIfHaveValue(childrenList) || childrenList.length <= 0)
                return children;
            for (var i in childrenList) {
                var child = angular.copy(childModel);
                child.ID = childrenList[i].ID;
                child.birthDate = parsePostDate(childrenList[i].birthDate);
                child.childBenefit = childrenList[i].childBenefit;
                child.firstName = childrenList[i].firstName;
                child.gender = checkIfHaveValue(childrenList[i].gender) ? childrenList[i].gender : null;
                child.homeLiving = childrenList[i].homeLiving;
                child.idNumber = childrenList[i].idNumber;
                child.possession = checkIfHaveValue(childrenList[i].possession) ? childrenList[i].possession : childrenList[i].homeLiving;
                child.PrevIdNumber = childrenList[i].PrevIdNumber;

                ///data check status
                newItem = parseInt(childrenList[i].newItem);
                child.CheckbirthDate = newItem === 1 ? 1 : childrenList[i].CheckbirthDate;
                child.CheckchildBenefit = newItem === 1 ? 1 : childrenList[i].CheckchildBenefit;
                child.CheckfirstName = newItem === 1 ? 1 : childrenList[i].CheckfirstName;
                child.Checkgender = newItem === 1 ? 1 : childrenList[i].Checkgender;
                child.CheckhomeLiving = newItem === 1 ? 1 : childrenList[i].CheckhomeLiving;
                child.CheckidNumber = newItem === 1 ? 1 : childrenList[i].CheckidNumber;
                child.Checkpossession = newItem === 1 ? 1 : childrenList[i].Checkpossession;

                children.push(child);
            }
            return children;
        }
        function copyEmployer(employer) {
            return {
                ID: 0,
                name: employer.name,
                address: employer.address,
                phoneNumber: employer.phoneNumber,
                deductionFileID: employer.deductionFileID,
                email: employer.email,
                displayEmail: employer.displayEmail,
                emailEmployee: true,
                emailEmployer: employer.displayEmail,
                salary: null,
                taxDeduction: null,
                incomeType: null,
                mifal: employer.mifal,
                rashut: employer.rashut,
            }
        }
        function copyThisIncome(thisIncome) {
            return {
                startDate: parsePostDate(thisIncome.startDate),
                CheckstartDate: thisIncome.CheckstartDate,
                incomeType: {
                    monthSalary: checkIfHaveValue(thisIncome.monthSalary),
                    anotherSalary: checkIfHaveValue(thisIncome.anotherSalary),
                    partialSalary: checkIfHaveValue(thisIncome.partialSalary),
                    daySalary: checkIfHaveValue(thisIncome.daySalary),
                    allowance: checkIfHaveValue(thisIncome.allowance),
                    stipend: checkIfHaveValue(thisIncome.stipend),
                    anotherSource: false,
                    anotherSourceDetails: null,

                    ///data check status
                    Checkstipend: thisIncome.Checkstipend,
                    CheckpartialSalary: thisIncome.CheckpartialSalary,
                    CheckmonthSalary: thisIncome.CheckmonthSalary,
                    CheckdaySalary: thisIncome.CheckdaySalary,
                    CheckanotherSalary: thisIncome.CheckanotherSalary,
                    Checkallowance: thisIncome.Checkallowance,
                },
                incomeTaxCredits: null,
                trainingFund: null,
                workDisability: null
            }
        }
        function copyTaxExemption(taxExemption) {
            return {
                blindDisabled: taxExemption.blindDisabled,
                isResident: taxExemption.isResident,
                isImmigrant: taxExemption.isImmigrant,
                immigrationStatus: taxExemption.immigration.immigrantStatus,
                immigrationStatusDate: parsePostDate(taxExemption.immigration.immigrantStatusDate),
                immigrationNoIncomeDate: parsePostDate(taxExemption.immigration.noIncomeDate),
                spouseNoIncome: taxExemption.spouseNoIncome,
                separatedParent: taxExemption.separatedParent,
                childrenInCare: taxExemption.childrenInCare,
                infantchildren: taxExemption.infantchildren,
                singleParent: taxExemption.singleParent,
                exAlimony: taxExemption.exAlimony,
                partnersUnder18: taxExemption.partnersUnder18,
                exServiceman: taxExemption.exServiceman,
                serviceStartDate: parsePostDate(taxExemption.servicemanStartDate),
                serviceEndDate: parsePostDate(taxExemption.servicemanEndDate),
                graduation: taxExemption.graduation,
                alimonyParticipates: taxExemption.alimonyParticipates,
                incompetentChild: taxExemption.incompetentChild,
                settlement: taxExemption.settlementCredits.settlement,
                startSettlementDate: parsePostDate(taxExemption.settlementCredits.startResidentDate),

                ///data check status
                CheckalimonyParticipates: taxExemption.CheckalimonyParticipates,
                CheckincompetentChild: taxExemption.CheckincompetentChild,
                CheckblindDisabled: taxExemption.CheckblindDisabled,
                CheckchildrenInCare: taxExemption.CheckchildrenInCare,
                CheckexAlimony: taxExemption.CheckexAlimony,
                CheckexServiceman: taxExemption.CheckexServiceman,
                Checkgraduation: taxExemption.Checkgraduation,
                Checkinfantchildren: taxExemption.Checkinfantchildren,
                CheckisImmigrant: taxExemption.CheckisImmigrant,
                CheckisResident: taxExemption.CheckisResident,
                CheckpartnersUnder18: taxExemption.CheckpartnersUnder18,
                CheckseparatedParent: taxExemption.CheckseparatedParent,
                CheckservicemanEndDate: taxExemption.CheckservicemanEndDate,
                CheckservicemanStartDate: taxExemption.CheckservicemanStartDate,
                ChecksingleParent: taxExemption.ChecksingleParent,
                CheckspouseNoIncome: taxExemption.CheckspouseNoIncome,
                ///immigration data check status
                CheckimmigrantStatus: taxExemption.immigration.CheckimmigrantStatus,
                CheckimmigrantStatusDate: taxExemption.immigration.CheckimmigrantStatusDate,
                ChecknoIncomeDate: taxExemption.immigration.ChecknoIncomeDate,
                ///settlementCredits data check status
                Checksettlement: taxExemption.settlementCredits.Checksettlement,
                CheckstartResidentDate: taxExemption.settlementCredits.CheckstartResidentDate
            }//, 
        }
        function copyEmployers(employers) {
            var employersList = new Array();
            if (!checkIfHaveValue(employers) || employers.length <= 0)
                return employersList;
            for (var i in employers) {
                var newItem = parseInt(employers[i].newItem);
                employersList.push({
                    ID: employers[i].$id,
                    name: employers[i].name,
                    address: employers[i].address,
                    phoneNumber: null,
                    deductionFileID: employers[i].deductionFileID,
                    email: null,
                    displayEmail: false,
                    emailEmployee: false,
                    emailEmployer: false,
                    salary: employers[i].salary,
                    taxDeduction: employers[i].taxDeduction,
                    incomeType: {
                        monthSalary: employers[i].incomeType == 1,
                        anotherSalary: false,
                        partialSalary: false,
                        daySalary: false,
                        allowance: employers[i].incomeType == 2,
                        stipend: employers[i].incomeType == 3,
                        anotherSource: employers[i].incomeType == 4,
                        anotherSourceDetails: '',

                        ///PrevIncomeType
                        Checkallowance: (newItem === 1 ? 1 : (employers[i].PrevIncomeType == 2 && employers[i].incomeType != 2 ? 2 : 0)), //2
                        CheckanotherSalary: 0,
                        CheckanotherSource: (newItem === 1 ? 1 : (employers[i].PrevIncomeType == 4 && employers[i].incomeType != 4 ? 2 : 0)), //4
                        CheckanotherSourceDetails: 0,
                        CheckdaySalary: 0,
                        CheckmonthSalary: (newItem === 1 ? 1 : (employers[i].PrevIncomeType == 1 && employers[i].incomeType != 1 ? 2 : 0)), //1
                        CheckpartialSalary: 0,
                        Checkstipend: (newItem === 1 ? 1 : (employers[i].PrevIncomeType == 3 && employers[i].incomeType != 3 ? 2 : 0)) //3
                    },

                    ///data check status -> TODO add existing employer and check
                    Checkaddress: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].Checkaddress),
                    CheckdeductionFileID: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].CheckdeductionFileID),
                    CheckincomeType: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].CheckincomeType),
                    Checkmifal: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].Checkmifal),
                    Checkname: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].Checkname),
                    Checkrashut: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].Checkrashut),
                    Checksalary: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].Checksalary),
                    ChecktaxDeduction: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].ChecktaxDeduction)
                });
            }
            return employersList;
        }
        function copyTaxCoordination(taxCoordination) {
            return {
                requestCoordination: taxCoordination.request,
                requestReason: taxCoordination.reason,
                employers: copyEmployers(taxCoordination.employers),

                ///data check status
                Checkreason: taxCoordination.Checkreason,
                Checkrequest: taxCoordination.Checkrequest,
            }
        }
        function copySpouse(spouse) {
            if (!checkIfHaveValue(spouse))
                return null;
            
            return {
                hasIncome: spouse.hasIncome,
                incomeType: spouse.incomeType,
                ID: null,
                firstName: spouse.firstName,
                lastName: spouse.lastName,
                idNumber: spouse.idNumber,
                birthDate: parsePostDate(spouse.birthDate),
                immigrationDate: parsePostDate(spouse.immigrationDate),
                phone: null,
                cell: null,
                email: null,
                createdDate: null,
                isDeleted: false,
                isConfirmed: false,
                password: null,
                calculatedIdNumber: null,
                fathersName: null,
                previousLastName: null,
                previousFirstName: null,
                city: null,
                street: null,
                houseNumber: null,
                entranceNumber: null,
                apartmentNumber: null,
                poBox: null,
                countryOfBirth: null,
                gender: false,
                maritalStatus: null,
                maritalStatusDate: null,
                isSpouse: true,
                spouseId: null,
                jobTitle: null,
                spouse: null,
                membership: null,
                groups: null,
                municipalities: null,
                userImages: null,
                PrevIdNumber: spouse.PrevIdNumber,

                ///data check status
                CheckbirthDate: spouse.CheckbirthDate,
                CheckfirstName: spouse.CheckfirstName,
                CheckidNumber: spouse.CheckidNumber,
                CheckimmigrationDate: spouse.CheckimmigrationDate,
                CheckincomeType: spouse.CheckincomeType,
                ChecklastName: spouse.ChecklastName,
                CheckhasIncome: spouse.CheckhasIncome
            };
        }
        function copyEmployee(employee) {
            return {//employee
                employerID: null,
                zip: employee.zip !== null? employee.zip : '',
                ilResident: employee.ilResident,
                kibbutzResident: employee.kibbutzResident,
                hmoMember: employee.hmoMember,
                hmoName: employee.hmoName,
                taxCoordination: null,
                taxExemption: null,
                ID: null,
                firstName: employee.firstName,
                lastName: employee.lastName,
                idNumber: employee.idNumber,
                birthDate: parsePostDate(employee.birthDate),
                immigrationDate: parsePostDate(employee.immigrationDate),
                phone: employee.phone,
                cell: employee.cell,
                email: employee.email,
                createdDate: null,
                isDeleted: false,
                isConfirmed: false,
                password: null,
                calculatedIdNumber: null,
                fathersName: null,
                previousLastName: null,
                previousFirstName: null,
                city: employee.city,
                street: employee.street,
                houseNumber: employee.houseNumber,
                entranceNumber: null,
                apartmentNumber: null,
                poBox: null,
                countryOfBirth: null,
                gender: employee.gender,
                maritalStatus: employee.maritalStatus,
                maritalStatusDate: null,
                isSpouse: false,
                spouseId: null,
                jobTitle: null,
                spouse: null,
                membership: null,
                groups: null,
                municipalities: null,
                userImages: null,

                ///data check status
                CheckbirthDate: employee.CheckbirthDate,
                Checkcell: employee.Checkcell,
                Checkcity: employee.Checkcity,
                Checkemail: employee.Checkemail,
                CheckfirstName: employee.CheckfirstName,
                Checkgender: employee.Checkgender,
                CheckhmoMember: employee.CheckhmoMember,
                CheckhmoName: employee.CheckhmoName,
                CheckhouseNumber: employee.CheckhouseNumber,
                CheckidNumber: employee.CheckidNumber,
                CheckilResident: employee.CheckilResident,
                CheckimmigrationDate: employee.CheckimmigrationDate,
                CheckkibbutzResident: employee.CheckkibbutzResident,
                ChecklastName: employee.ChecklastName,
                CheckmaritalStatus: employee.CheckmaritalStatus,
                Checkphone: employee.Checkphone,
                Checkstreet: employee.Checkstreet,
                Checkzip: employee.Checkzip
            };
        }
        function copyOtherIncomes(otherIncomes) {
            return {
                startDate: parsePostDate(otherIncomes.startDate),
                incomeType: {
                    monthSalary: checkIfHaveValue(otherIncomes.monthSalary),
                    anotherSalary: checkIfHaveValue(otherIncomes.anotherSalary),
                    partialSalary: checkIfHaveValue(otherIncomes.partialSalary),
                    daySalary: checkIfHaveValue(otherIncomes.daySalary),
                    allowance: checkIfHaveValue(otherIncomes.allowance),
                    stipend: checkIfHaveValue(otherIncomes.stipend),
                    anotherSource: checkIfHaveValue(otherIncomes.anotherSource),
                    anotherSourceDetails: otherIncomes.anotherSourceDetails,

                    ///data check status
                    Checkallowance: otherIncomes.Checkallowance,
                    CheckanotherSalary: otherIncomes.CheckanotherSalary,
                    CheckanotherSource: otherIncomes.CheckanotherSource,
                    CheckanotherSourceDetails: otherIncomes.CheckanotherSourceDetails,
                    CheckdaySalary: otherIncomes.CheckdaySalary,
                    CheckmonthSalary: otherIncomes.CheckmonthSalary,
                    CheckpartialSalary: otherIncomes.CheckpartialSalary,
                    Checkstipend: otherIncomes.Checkstipend
                },
                incomeTaxCredits: otherIncomes.incomeTaxCredits,
                anotherSourceDetails: otherIncomes.anotherSourceDetails,
                trainingFund: checkIfHaveValue(otherIncomes.Provisions.trainingFund),
                workDisability: checkIfHaveValue(otherIncomes.Provisions.workDisability),
                hasIncomes: checkIfHaveValue(otherIncomes.hasIncomes),

                ///data check status
                CheckhasIncomes: otherIncomes.CheckhasIncomes,
                CheckincomeTaxCredits: otherIncomes.CheckincomeTaxCredits,
                ChecktrainingFund: otherIncomes.Provisions.ChecktrainingFund,
                CheckworkDisability: otherIncomes.Provisions.CheckworkDisability,
                CheckanotherSourceDetails: otherIncomes.CheckanotherSourceDetails,
            }
        }

        function setSendModel(modelData, currentMunicipalityId) {
            var model = copyGeneralData(angular.copy(taxfrommodel), modelData, currentMunicipalityId);
            model.children = copyChildren(modelData.children);
            model.employer = copyEmployer(modelData.employer);
            model.income = copyThisIncome(modelData.thisIncome);
            model.taxExemption = copyTaxExemption(modelData.taxExemption);
            model.taxCoordination = copyTaxCoordination(modelData.taxCoordination);
            model.spouse = copySpouse(modelData.spouse);
            model.employee = copyEmployee(modelData.employee);
            model.otherIncomes = copyOtherIncomes(modelData.otherIncomes);
            ///for pdf mobile display
            model.showMobile = $window.innerWidth < 801 ? true : null;

            return model;
        }

        var childModel = {
            ID: null,
            possession: false,
            childBenefit: false,
            homeLiving: false,
            firstName: null,
            idNumber: null,
            oldIdNumber: null,
            gender: true,
            birthDate: null,
            userID: 0
        },

        incomeTypeModel = {
            monthSalary: false,
            anotherSalary: false,
            partialSalary: false,
            daySalary: false,
            allowance: false,
            stipend: false,
            anotherSource: false,
            anotherSourceDetails: null
        },

        taxfrommodel = {
            municipalityId: null,
            year: null,
            date: null,
            sign: false,
            signatureDate: null,
            signatureImageID: null,
            emailEmployee: false,
            employee: {},
            employer: {},
            children: new Array(),
            income: {},
            otherIncomes: {},
            signature: {},
            taxCoordination: {},
            taxExemption: {},
            incomeType: {
                monthSalary: false,
                anotherSalary: false,
                partialSalary: false,
                daySalary: false,
                allowance: false,
                stipend: false,
                anotherSource: false,
                anotherSourceDetails: null
            },
            spouse: {},
            FilesList: [],
            connectedImages: [],
            formPdfImageID: null,
            formPdf: null,
        }

        , getModel = function () {
            return taxfrommodel;
        }

        , setMOdel = function (modelData, currentMunicipalityId) {
            return setSendModel(modelData, currentMunicipalityId);
        }

        return {
            GetModel: getModel,
            SetModel: setMOdel
        }
    }
})();
(function () {

    angular.module('WorkerPortfolioApp').factory('toastrService', toastrService);

    toastrService.$inject = ['toastr', 'toastrConfig', '$timeout'];

    function toastrService(toastr, toastrConfig, $timeout) {

        var setOptions = function (opts) {
            if (typeof opts == 'undefined')
                opts = {};
            if (opts.clearThenShow) {
                toastr.clear();
            }
            
            toastrConfig.autoDismiss = typeof opts.autoDismiss !== 'undefined' && opts.autoDismiss !== true ? opts.autoDismiss : true;
            toastrConfig.allowHtml = opts.allowHtml ? opts.allowHtml : false;
            toastrConfig.extendedTimeOut = parseInt((opts.extendedTimeOut ? opts.extendedTimeOut : 10), 10);
            toastrConfig.positionClass = 'toast-' + (opts.positionX ? opts.positionX : 'top') + '-' + (opts.positionY ? opts.positionY : 'center'), /// X 'top, bottom', Y  'Right, Left, Full Width, Center';
            toastrConfig.timeOut = parseInt((opts.timeOut ? opts.timeOut : 3500), 10);
            toastrConfig.closeButton = opts.closeButton ? opts.closeButton : false;
            toastrConfig.tapToDismiss = typeof opts.tapToDismiss !== 'undefined' && opts.tapToDismiss !== true ? opts.tapToDismiss : true;
            toastrConfig.progressBar = typeof opts.progressBar !== 'undefined' && opts.progressBar !== true ? opts.progressBar : true;
            toastrConfig.closeHtml = opts.closeHtml ? opts.closeHtml : null;
            toastrConfig.newestOnTop = typeof opts.newestOnTop !== 'undefined' && opts.newestOnTop !== true ? opts.newestOnTop : true;
            toastrConfig.maxOpened = opts.maxOpened ? opts.maxOpened : 0;
            toastrConfig.preventDuplicates = opts.preventDuplicates ? opts.preventDuplicates : false;
            toastrConfig.preventOpenDuplicates = typeof opts.preventOpenDuplicates !== 'undefined' && opts.preventOpenDuplicates !== true ? opts.preventOpenDuplicates : true;

            toastrConfig.onHidden = typeof opts.onHidden === 'function' ? opts.onHidden : null;
            toastrConfig.onShown = typeof opts.onShown === 'function' ? opts.onShown : null;
            toastrConfig.onTap = typeof opts.onTap === 'function' ? opts.onTap : null;
        }

        return {
            clear: function () {
                toastr.clear();
            },
            remove: function () {
                toastr.remove();
            },
            error: function (message, title, optionsOverride) {
                setOptions(optionsOverride);
                toastr.error(message, title);
            },
            info: function (message, title, optionsOverride) {
                setOptions(optionsOverride);
                toastr.info(message, title);
            },
            success: function (message, title, optionsOverride) {
                setOptions(optionsOverride);
                toastr.success(message, title);
            },
            warning: function (message, title, optionsOverride) {
                setOptions(optionsOverride);
                toastr.warning(message, title);
            },
            custom: function (message, title, optionsOverride) {
                setOptions(optionsOverride);
                toastr.info(message, title, optionsOverride);
            }
        }
    }
})();

(function () {
    

    angular.module('WorkerPortfolioApp').factory('TofesDataService', TofesDataService);

    TofesDataService.$inject = ['$rootScope'];

    function TofesDataService($rootScope) {
        var checkIfHaveValue = function (value) {
            return typeof value != 'undefined' && value != null && value != '';
        }
        , validateOrigin = function (origin, update) {
            if (!checkIfHaveValue(origin.date))
                origin.date = new Date();
            var now = new Date();
            var timeDiff = Math.abs(origin.date.getTime() - now.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            if (diffDays > 14 || (checkIfHaveValue(origin.employee.idNumber) && checkIfHaveValue(update.employee.idNumber) && origin.employee.idNumber != update.employee.idNumber)) {
                $rootScope.lsRemove('$$tofesModel');
                $rootScope.lsRemove('states');
                update = angular.copy(baseModel);
                update.date = new Date();
            }
            return update;
        }
        , checkObjectVal = function (obj, attr) {
            return obj != undefined && typeof obj !== 'undefined' && obj != null &&
                    obj[attr] != undefined && typeof obj[attr] !== 'undefined' && obj[attr] != null && !String(obj[attr]).isempty();
        }
        ///private function to parse the `TofesModel` and update it.
        , checkIfDate = function (attr) {
            return (String(attr).toLowerCase().indexOf('date') > -1);
        }
        , transformDate = function (val, attr) {
            var pad = function (val) { return parseInt(val) < 10 ? ('0' + String(val)) : val; }

            if (String(val).indexOf('T') > 3 && !(val instanceof Date)) {    
                val = new Date(val);
            }
            
            if (val instanceof Date) {
                return [pad(val.getDate()), pad(val.getMonth() + 1), val.getFullYear()].join('/');
            }
            return val;
        }
        /* *** update only matching attributes in both objects *** */
        , updateMatchingAttributes = function (original, data) {
            var matching = objectsCompare(original, data);
            $.map(matching, function (item) {
                original[item] = data[item];
            });
        }
        /* *** compare two objects and return matching attributes *** */
        , objectsCompare = function (original, data) {
            var originAray = _.keys(original), dataArray = _.keys(data), intersect = _.intersection(originAray, dataArray);
            return $.map(original, function (val, attr) {
                if (!(original[attr] instanceof Object) && !(original[attr] instanceof Array) && intersect.indexOf(attr) > -1) return attr;
            });
        }
        
        , doUpdatePrimitives = function (keysToUpdate, origin, update, override) {
            $.map(keysToUpdate, function (item) {
                if ((update[item] || typeof update[item] === 'boolean') && !String(update[item]).isempty() && (override || origin[item] === null || !origin[item] || String(origin[item]).isempty()) && (origin[item] != null || update[item] != null)) {
                    origin[item] = update[item];
                }
            });
        }
        , doUpdateDates = function (keysToUpdate, origin, update, override) {
            $.map(keysToUpdate, function (item) {
                if (checkObjectVal(update, item) && update[item] !== '0001-01-01T00:00:00' && (override || !checkObjectVal(origin, item))) {
                    origin[item] = transformDate(update[item]);
                }
            });
        }
        , doUpdateArray = function (model, origin, update, override, attr) {
            var O_uniq = [], U_uniq = [];
            switch (attr) {
                case 'children':
                    O_uniq = _.groupBy(origin, 'idNumber');
                    U_uniq = _.groupBy(update, 'idNumber');
                    break;
                case 'employers':
                    O_uniq = _.groupBy(origin, '$id');
                    U_uniq = _.groupBy(update, '$id');
                    break;
                case 'TofesControllerFilesList':
                case 'TaxExemptionControllerFilesList':
                case 'TaxAdjustmentControllerFilesList':
                    O_uniq = _.groupBy(origin, 'imageId');
                    U_uniq = _.groupBy(update, 'imageId');
                    break;
            }
            $.map(U_uniq, function (item, index) {
                if (typeof O_uniq[index] === 'undefined') {
                    origin.push(UpdateEntireObject(angular.copy(model), U_uniq[index][0], override));
                }
            });        
        }
        , UpdateEntireObject = function (origin, update, override) {
            var originKeys = _.keys(origin), updateKeys = _.keys(update), intersect = _.intersection(originKeys, updateKeys);
            ///update only primitive types
            var primitiveKeys = $.map(origin, function (val, attr) {
                if (!(origin[attr] instanceof Object) && !(origin[attr] instanceof Array) && intersect.indexOf(attr) > -1 && !checkIfDate(attr)) return attr;
            });
            doUpdatePrimitives(primitiveKeys, origin, update, override);

            var dateKeys = $.map(origin, function (val, attr) {
                if (checkIfDate(attr)) return attr;
            });
            doUpdateDates(dateKeys, origin, update, override);
            
            ///update arrays
            $.map(origin, function (val, attr) {
                if ((origin[attr] instanceof Array) && intersect.indexOf(attr) > -1) {
                    doUpdateArray(origin[attr + 'ArrayType'], origin[attr], update[attr], override, attr);
                }
            });
            ///update inner objects
            $.map(origin, function (val, attr) {
                if ((origin[attr] instanceof Object) && intersect.indexOf(attr) > -1)
                    UpdateEntireObject(origin[attr], update[attr], override);
            });

            return origin;
        }

        ,CheckIncomeType = function (newModel, oldModel) {
            for (var i in newModel)
                CheckRunTimeDifferences(newModel[i], oldModel[i]);
        }
        , CheckPrimitiveKeys = function (primitiveKeys, newModel, oldModel) {
            $.map(primitiveKeys, function (attr) {

                if (attr.indexOf('Check') != 0 && attr.indexOf('$') != 0) {
                    var od = oldModel != null && oldModel[attr] != null && !String(oldModel[attr]).isempty();
                    var nd = newModel != null && newModel[attr] != null && !String(newModel[attr]).isempty();
                    
                    ///no change id data
                    newModel['Check' + attr] = 0;
                    if (!od && nd) {
                        ///new data
                        newModel['Check' + attr] = 1;
                    }
                    if (od && nd && newModel[attr] != oldModel[attr]) {
                        ///data changed
                        newModel['Check' + attr] = 2;
                    }
                    if (attr == 'incomeType') {
                        newModel['PrevIncomeType'] = oldModel[attr];
                    }
                    if (attr === 'idNumber') {
                        newModel['PrevIdNumber'] = oldModel[attr];
                    }
                }
            });
        }
        , CheckDateFields = function (DateKeys, newModel, oldModel) {
            $.map(DateKeys, function (attr) {
                if (attr.indexOf('Check') != 0 && typeof newModel[attr] != 'function') {
                    ///no change id data
                    newModel['Check' + attr] = 0;
                    if (((oldModel == null ? null : oldModel[attr]) == null || String((oldModel == null ? null : oldModel[attr])).isempty()) && (newModel[attr] != null && !String(newModel[attr]).isempty())) {
                        ///new data
                        newModel['Check' + attr] = 1;
                    }
                    if ((oldModel == null ? null : oldModel[attr]) != null && !String((oldModel == null ? null : oldModel[attr])).isempty() && newModel[attr] != null && !String(newModel[attr]).isempty()
                        && transformDate(newModel[attr]) != transformDate((oldModel == null ? null : oldModel[attr]))) {
                        ///data changed
                        newModel['Check' + attr] = 2;
                    }
                }
            });
        }
        , CheckArrays = function (ArrayType, newArray, oldArray, attr) {
            var old_uniq = [], new_uniq = [];
            switch (attr) {
                case 'children':
                    old_uniq = _.groupBy(oldArray, 'ID');
                    new_uniq = _.groupBy(newArray, 'ID');
                    break;
                case 'employers':
                    old_uniq = _.groupBy(oldArray, '$id');
                    new_uniq = _.groupBy(newArray, '$id');
                    break;
                case 'TofesControllerFilesList':
                case 'TaxExemptionControllerFilesList':
                case 'TaxAdjustmentControllerFilesList':
                    old_uniq = _.groupBy(oldArray, 'imageId');
                    new_uniq = _.groupBy(newArray, 'imageId');
                    break;
            }
            $.map(new_uniq, function (item, index) {
                ///no new data
                new_uniq[index][0].newItem = 0;
                ///check if new child
                if (typeof old_uniq[index] === 'undefined') {
                    new_uniq[index][0].newItem = 1;
                } else {
                    CheckRunTimeDifferences(new_uniq[index][0], old_uniq[index][0])
                }
            });

        }
        , CheckRunTimeDifferences = function (newModel, oldModel) {
            var newKeys = _.keys(newModel), oldKeys = _.keys(oldModel), intersect = _.intersection(newKeys, oldKeys);
            ///check only primitive types
            var primitiveKeys = $.map(newModel, function (val, attr) {
                if (!(newModel[attr] instanceof Object) && !(newModel[attr] instanceof Array) && intersect.indexOf(attr) > -1 && !checkIfDate(attr)) return attr;
            });
            CheckPrimitiveKeys(primitiveKeys, newModel, oldModel);
            ///check date fields
            var dateKeys = $.map(newModel, function (val, attr) {
                if (checkIfDate(attr)) return attr;
            });
            
            CheckDateFields(dateKeys, newModel, oldModel);

            ///check arrays
            $.map(newModel, function (val, attr) {
                if ((newModel[attr] instanceof Array) && intersect.indexOf(attr) > -1) {
                    CheckArrays(newModel[attr + 'ArrayType'], newModel[attr], oldModel[attr], attr);
                }
            });
            
            ///check inner objects
            $.map(newModel, function (val, attr) {
                if ((newModel[attr] instanceof Object) && intersect.indexOf(attr) > -1 && attr.indexOf('ArrayType') < 0) {
                    
                    //if (attr == 'employers') {
                    //    CheckIncomeType(newModel[attr], oldModel[attr]);
                    //}

                    CheckRunTimeDifferences(newModel[attr], oldModel[attr]);
                }
            });
            return newModel;
        }
        
        , ComputeStates = ComputeStates || function (list, current, formType) {
            var state = { position: 0, isCurrent: false, clas: '', text: '', goTo: '', isCompleted: false }, copy = {}, index = 0;
            ComputeStates.states = [];
            ComputeStates.current = -1;
            list.filter(function (s) {
                if (s.abstract !== true && s.data.type === 'Form101' && (s.data.formType === 0 || (formType !== 3 && s.data.formType === 2) || (formType === 3 && s.data.formType === 3))) {
                    copy = angular.copy(state);
                    copy.text = s.title;
                    copy.goTo = s.name;
                    copy.position = index++;
                    ComputeStates.states.push(copy);
                }                    
            });
            return ComputeStates.states;
        }

        , Genders = [ 
            {
                id:true,
                name: 'זכר',
                selected: false
            },
            {
                id: false,
                name: 'נקבה',
                selected: false
            }
        ]

        , MaritalStates = [
            {
                id: 1,
                name: 'רווק/ה',
                selected: false
            },
            {
                id: 2,
                name: 'נשוי/אה',
                selected: false
            },
            {
                id: 3,
                name: 'גרוש/ה',
                selected: false
            },
            {
                id: 4,
                name: 'אלמן/ה',
                selected: false
            },
            {
                id: 5,
                name: 'פרוד/ה',
                selected: false
            },
        ]

        , ResidentStates = [
            {
                id: 1,
                name: 'כן',
                selected: true
            },
            {
                id: 2,
                name: 'לא',
                selected: false
            }
        ]

        , KibbutzStates = [
            {
                id: 1,
                name: 'כן',
                selected: true
            },
            {
                id: 2,
                name: 'לא',
                selected: false
            }
        ]

        , IncomeStates = [
            {
                id: 1,
                name: 'אין לבן/בת הזוג כל הכנסה',
                selected: false
            },
            {
                id: 2,
                name: 'יש לבן/בת הזוג הכנסה',
                selected: true
            }
        ]

        , IncomeTypeStates = [
            {
                id: 1,
                name: 'עבודה/קיצבה/עסק',
                selected: false
            },
            {
                id: 2,
                name: 'הכנסה אחרת',
                selected: false
            }
        ]

        , OtherIncomesStates = [
            {
                id: 1,
                name: 'אין לי הכנסות אחרות לרבות מלגות',
                selected: false
            },
            {
                id: 2,
                name: 'יש לי הכנסות נוספות כמפורט להלן',
                selected: true
            }
        ]

        , IncomeTaxCreditsStates = [
            {
                id: 1,
                name: 'אבקש לקבל נקודות זיכוי ומדרגות מס כנגד הכנסתי זו (סעיף ד). איני מקבל/ת אותם בהכנסה אחרת.',
                selected: false
            },
            {
                id: 2,
                name: 'אני מקבל/ת נקודות זיכוי ומדרגות מס בהכנסה אחרת ועל כן איני זכאי/ת להם כנגד הכנסה זו.',
                selected: false
            }
        ]

        , ImmigrantStatusStates = [
            {
                id: 1,
                name: 'עולה חדש/ה',
                selected: false
            },
            {
                id: 2,
                name: 'תושב/ת חוזר/ת',
                selected: false
            }
        ]

        , TaxCoordinationReasonStates = [
            {
                id: 1,
                name: 'לא הייתה לי הכנסה מתחילת שנת המס הנוכחית עד לתחילת עבודתי אצל מעביד זה.',
                selected: false
            },
            {
                id: 2,
                name: 'יש לי הכנסות נוספות ממשכורת כמפורט להלן.',
                selected: false
            },
            {
                id: 3,
                name: 'פקיד השומה אישר תיאום לפי אישור מצורף.',
                selected: false
            }
        ]

        , TaxCoordinationIncomeTypeStates = [
            {
                id: 1,
                name: 'עבודה',
                selected: false
            },
            {
                id: 2,
                name: 'קיצבה',
                selected: false
            },
            {
                id: 3,
                name: 'מילגה',
                selected: false
            },
            {
                id: 4,
                name: 'אחר',
                selected: false
            }
        ]
        
        , Hmos = [
            {
                id: 2,
                name: 'כללית'
            },
            {
                id: 3,
                name: 'לאומית'
            },
            {
                id: 4,
                name: 'מכבי'
            },
            {
                id: 23,
                name: 'מאוחדת'
            }
        ]
        
        , FileModel = {
            filename: null,
            userIDNumber: null
        }

        , ChildModel = {
            ID: null,
            firstName: null,
            idNumber: null,
            birthDate: null,
            homeLiving: false,
            childBenefit: false,
            possession: null,
            gender: null,
        }
        
        , FileListModel = {
            filename: null,
            imageId: null,
            userIDNumber: null,
            year: null,
            origin: null,
            originalName:null
        }

        , EmployerModel = {
            $id: 0,
            name: null,
            address: null,
            deductionFileID: null,
            salary: null,
            taxDeduction: null,
            incomeType: null,
            mifal: null,
            rashut: null
        }

        , TofesModel = {
            year: null,
            FormType: null,
            date: null,
            signatureDate: null,
            signature: null,
            signatureDataUrl: null,
            emailEmployee: null,

            employer: {
                name: null,
                address: null,
                phoneNumber: null,
                deductionFileID: null,
                email: null,
                displayEmail: false,
                mifal: null,
                rashut: null,
            },

            employee: {
                firstName: null,
                lastName: null,
                idNumber: null,
                birthDate: null,
                immigrationDate: null,
                city: null,
                street: null,
                houseNumber: null,
                zip: null,
                phone: null,
                gender: null,
                maritalStatus: null,
                ilResident: null,
                kibbutzResident: null,
                hmoMember: null,
                hmoName: null
            },
            
            children: [/*Array of ChildModel*/],
            childrenArrayType: ChildModel,

            thisIncome: {
                startDate: null,
                monthSalary: null,
                anotherSalary: null,
                partialSalary: null,
                daySalary: null,
                allowance: null,
                stipend: null,
            },

            otherIncomes: {
                hasIncomes: null,
                monthSalary: null,
                anotherSalary: null,
                partialSalary: null,
                daySalary: null,
                allowance: null,
                stipend: null,
                anotherSource: null,
                anotherSourceDetails: '',
                incomeTaxCredits: null,
                Provisions: {
                    trainingFund: null,
                    workDisability: null
                }
            },

            spouse: {
                idNum: null,
                firstName: null,
                lastName: null,
                birthDate: null,
                immigrationDate: null,
                hasIncome: null,
                incomeType: null
            },

            taxExemption: {
                blindDisabled: null,
                isResident: null,
                settlementCredits: {
                    startResidentDate: null,
                    settlement: null,
                },
                isImmigrant: null,
                immigration: {
                    immigrantStatus: null,
                    immigrantStatusDate: null,
                    noIncomeDate: null,
                },
                spouseNoIncome: null,
                separatedParent: null,
                childrenInCare: null,
                infantchildren: null,
                singleParent: null,
                alimonyParticipates: null,
                exAlimony: null,
                partnersUnder18: null,
                exServiceman: null,
                servicemanStartDate: null,
                servicemanEndDate: null,
                graduation: null,
                incompetentChild: null
            },

            taxCoordination: {
                request: null,
                reason: null,
                employers: [/*Array of EmployerModel*/],
                employersArrayType: EmployerModel,
            }
        }
        
        , baseModel = {
            TofesControllerFilesList: [],
            TofesControllerFilesListArrayType: FileListModel,
            TaxExemptionControllerFilesList: [],
            TaxExemptionControllerFilesListArrayType: FileListModel,
            TaxAdjustmentControllerFilesList: [],
            TaxAdjustmentControllerFilesListArrayType: FileListModel,
            children: [],
            childrenArrayType: ChildModel,
            employee: {
                firstName: null,
                lastName: null,
                idNumber: null,
                birthDate: null,
                immigrationDate: null,
                city: null,
                street: null,
                houseNumber: null,
                zip: null,
                phone: null,
                cell: null,
                email: null,
                gender: null,
                maritalStatus: null,
                ilResident: null,
                kibbutzResident: null,
                hmoMember: null,
                hmoName: null,
                startWorkTime: null
            },
            employer: {
                name: null,
                address: null,
                phoneNumber: null,
                deductionFileID: null,
                displayEmail: null,
                email: null,
                mifal: null,
                rashut: null,
            },
            spouse: {
                idNumber: null,
                firstName: null,
                lastName: null,
                birthDate: null,
                immigrationDate: null,
                hasIncome:null,
                incomeType: null
            },
            taxCoordination: {
                request: null,
                reason: null,
                employers: [/*Array of EmployerModel*/],
                employersArrayType: EmployerModel,
            },
            taxExemption: {
                blindDisabled: null,
                isResident: null,
                settlementCredits: {
                    startResidentDate: null,
                    settlement: null,
                },
                isImmigrant: null,
                immigration: {
                    immigrantStatus: null,
                    immigrantStatusDate: null,
                    noIncomeDate: null,
                },
                spouseNoIncome: null,
                separatedParent: null,
                childrenInCare: null,
                infantchildren: null,
                singleParent: null,
                alimonyParticipates: null,
                exAlimony: null,
                partnersUnder18: null,
                exServiceman: null,
                servicemanStartDate: null,
                servicemanEndDate: null,
                graduation: null,
                incompetentChild: null,
            },
            year: null,
            date: null,
            signatureDate: null,
            signatureDataUrl: null,
            signature: null,
            emailEmployee: null,
            sign: null,
            signatureImageID: null,
            thisIncome: {
                startDate: null,
                monthSalary: null,
                anotherSalary: null,
                partialSalary: null,
                daySalary: null,
                allowance: null,
                stipend: null,
            },
            otherIncomes: {
                hasIncomes: null,
                monthSalary: null,
                anotherSalary: null,
                partialSalary: null,
                daySalary: null,
                allowance: null,
                stipend: null,
                anotherSource: null,
                anotherSourceDetails: '',
                incomeTaxCredits: null,
                Provisions: {
                    trainingFund: null,
                    workDisability: null
                }
            },
            formPdfImageID: null,
            formPdf: null,
            FormType: null
        }
          
        , baseModelCopy = {}

        return {
            GetStartWorkInYearDate: function(startWorkDate, year){
                var startOfYear = new Date(year, 0, 1);
                if (!checkIfHaveValue(startWorkDate))
                    return startOfYear;
                var d = new Date(startWorkDate);
                startOfYear = d > startOfYear ? d : startOfYear;
                return transformDate(startOfYear);
            },
            TransformToDate: transformDate,
            SetFirstOfYear: function(model){
                if (!checkIfHaveValue(model)) {
                    return '01/01/' + new Date().getFullYear();
                }
                return model;
            },
            GetExistingData: function (origin, type, override) {
                if (override !== true)
                    override = false;
                var preExisting = $rootScope.lsGet(type);
                ///INSERT PRE EXISTING DATA INTO FORM
                if (preExisting && typeof preExisting === 'object') {
                    preExisting = validateOrigin(origin, preExisting);
                    return UpdateEntireObject(origin, preExisting, override);
                }
                return origin;
            },
            GetTofes: function() {
                return TofesModel;
            },
            GetRadiosStates: function () {
                return {
                    genders: Genders,
                    maritalStates: MaritalStates,
                    residentStates: ResidentStates,
                    hmos: Hmos,
                    incomeStates: IncomeStates,
                    incomeTypeStates: IncomeTypeStates,
                    kibbutzStates: KibbutzStates,
                    otherIncomesStates: OtherIncomesStates,
                    incomeTaxCreditsStates: IncomeTaxCreditsStates,
                    immigrantStatusStates: ImmigrantStatusStates,
                    taxCoordinationReasonStates: TaxCoordinationReasonStates,
                    taxCoordinationIncomeTypeStates: TaxCoordinationIncomeTypeStates
                };
            },
            GetCustomModel: function () {
                return {
                    fileModel: FileModel,
                    childModel: ChildModel,
                    employerModel: EmployerModel
                };
            },
            GetComputedStates: function (list, current, formType) {
                return ComputeStates(list, current, formType);
            },
            UpdateObject: function (origin, update, toOverride) {
                if (toOverride !== true)
                    toOverride = false;
                update = validateOrigin(origin, update);
                return UpdateEntireObject(origin, update, toOverride);
            },
            GetTofesControllerData: function () {
                return angular.copy(baseModel);
            },
            SetTofesControllerData: function (obj) {
                TofesControllerData = obj;
            },
            GetFamilyDetailsControllerData: function () {
                return FamilyDetailsControllerData;
            },
            GetIncomeControllerData: function () {
                return IncomeControllerData;
            },
            GetTaxExemptionControllerData: function () {
                return TaxExemptionControllerData;
            },
            GetTaxAdjustmentControllerData: function () {
                return TaxAdjustmentControllerData;
            },
            GetAttestationControllerData: function () {
                return AttestationControllerData;
            },
            CopyRunTimeModel: function (model) {
                if (model !== null && Object.keys(model).length > 10)
                    $rootScope.lsSet('$$tofesModelCopy', angular.copy(model));
            },
            CheckRunTimeCopy: function (model) {
                var old = $rootScope.lsGet('$$tofesModelCopy');
                return CheckRunTimeDifferences(model, old);                 
            }
        }
    }
})();
(function () {
    var TofesService = angular.module('TofesService', []);

    TofesService.factory('Tofes', ['$http', '$q',
      function ($http, $q) {
          var setAction = function (action, params, type) {
              type = typeof type !== 'undefined' ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  for (var i in params)
                      action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              } else {
                  for (var i in params)
                      action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return '/api/tofes101' + (typeof action !== 'undefined' && action.length > 0 ? '/' + action : '');
          }

          return {
              query: function () {
                  return $http.get(setAction('get'), "json", "application/json; charset=utf-8");
              },
              GetFormData: function (data) {
                  return $http.post(setAction('GetFormData'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveChild: function (data) {
                  return $http.post(setAction('Child'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveStreet: function (data) {
                  return $http.post(setAction('Street'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveCity: function (data) {
                  return $http.post(setAction('City'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveIncome: function (data) {
                  return $http.post(setAction('Income'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveSignature: function (data) {
                  return $http.post(setAction('Signature'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveTaxCoordination: function (data) {
                  return $http.post(setAction('taxCoordination'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveTaxExemption: function (data) {
                  return $http.post(setAction('taxExemption'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveIncomeType: function (data) {
                  return $http.post(setAction('IncomeType'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveEmployee: function (data) {
                  return $http.post(setAction('Employee'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveEmployer: function (data) {
                  return $http.post(setAction('Employer'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              }
          }
      }]);
})();
(function () {
    
    
    var UserDataService = angular.module('UserDataService', []);

    UserDataService.factory('UserService', ['$http', '$q',
      function ($http, $q) {
          var setAction = function (action, params, type, baseUrl) {
              baseUrl = typeof baseUrl === 'string' ? baseUrl : '/api/ContactUs';
              type = typeof type !== 'undefined' && type !== null ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              switch (type) {
                  case 'get':
                      for (var i in params)
                          action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');
                      break;
                  case 'rest':
                      for (var i in params)
                          action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                      action = action.replace(/\/\[([^)]+)\]/ig, '');
                      break;
              }

              return baseUrl + (typeof action !== 'undefined' && action !== null && action.length > 0 ? '/' + action : '');
          }

          return {
              ResetPassword: function (data) {
                  return $http.patch(setAction(null, null, null, '/api/Account'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetToMailItems: function (data) {
                  return $http.post(setAction('ToMailItems'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SendNotificationMail: function (data) {
                  return $http.post(setAction('SendNotificationMail'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              DeleteFile: function (data) {
                  return $http.get(setAction('DeleteFile', data, 'get', 'api/FilesUploades'), JSON.stringify(data), "application/json; charset=utf-8");
              },
              SendContactUsMail: function (data) {
                  return $http.post(setAction('ContactUs'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetSiteMail: function () {
                  return $http.put(setAction(), "json", "application/json; charset=utf-8");
              },
          }

      }]);
})();
(function () {
    angular.module('WorkerPortfolioApp').controller('AdminController', AdminController);

    AdminController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', 'toastrService', 'Admin', 'ValidAdminUser', '$user', '$passwordSettings', '$groups'];
    
    function AdminController($scope, $rootScope, $state, $stateParams, toastrService, Admin, ValidAdminUser, $user, $passwordSettings, $groups) {
        
        if (ValidAdminUser.data !== true || ValidAdminUser.status !== 200) {
            $state.go('Home');
            toastrService.info('', 'הרשאה לא תקינה!', { onHidden: function () { $state.go('Home'); } });
            return void [0];
        }

        var mailType = {
            1: 'איפוס סיסמה',
            2: 'הרשמה לאתר',
            3: 'ברוך הבא למערכת',
            4: 'כללי'
        }
        var pad = function (val) {
            return val > 9 ? String(val) : ('0' + String(val));
        }
        var parseDateToString = function (date) {
            if (date === undefined || date === null)
                return null;
            date = new Date(date);
            return pad(date.getDate()) + '/' + pad(date.getMonth() + 1) + '/' + date.getFullYear() + ' ' + pad(date.getHours()) + ':' + pad(date.getMinutes()) + ':' + pad(date.getSeconds());
        }
        var last7 = function () {
            var now = new Date();
            return new Date(now.getFullYear(), now.getMonth(), now.getDate() - 7);
        }
        var parseSmsMessages = function (data) {
            $.map(data, function (msg) {
                msg.sendDate = parseDateToString(msg.sendDate);
                msg.response = JSON.parse(msg.response);
                msg.anser = returnAnser(msg.response.Msg);
            });
            return data;
        }
        var returnAnser = function (Msg) {
            var anser = '';
            switch(Msg.RcType){
                case 1:
                    switch(Msg.RcNumber){
                        case 0:
                            anser = 'נשלח תקין';
                            break;
                        case 200:
                            anser = 'מספר לא תקין';
                            break;

                        default:
                            anser = 'שגיאת מערכת שליחה';
                            break;
                    }
                    break;
                case 9:
                    switch (Msg.RcNumber) {
                        case -99:
                            anser = 'תשובה לא תקינה';
                            break;
                        case 0:
                            anser = 'מספר לא תקין';
                            break;
                        case 1:
                        case 380000:
                            anser = 'שגיאת מערכת שליחה';
                            break;
                        case 500:
                            anser = 'משלוח נכשל';
                            break;

                        default:
                            anser = 'שגיאת מערכת שליחה';
                            break;
                    }
                    break;
                default:
                    anser = 'שגיאת מערכת שליחה';
                    break;
            }
            
            return anser;
        }
        var parseMailMessages = function (data) {
            $.map(data, function (msg) {
                console.log(msg);
                msg.MailType = mailType[msg.MailType];
                msg.createdDate = parseDateToString(msg.createdDate);
            });

            return data;
        }

        $scope.$user = $user && $user.data ? $user.data : {};
        $scope.$passwordSettings = $passwordSettings.data;
        
        $scope.getWSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'ADMIN');
        $scope.UsersList = [];
        $scope.SmsMessages = new Array();
        $scope.EmailMessages = new Array();
        $scope.UserSearch = {
            idNumber: $scope.getWSPostModel.idNumber,
            confirmationToken: $scope.getWSPostModel.confirmationToken,
            page: 1,
            hidden_value: false,
            $hidden_value:'',
            searchIdNumber: $stateParams.IdNUmber,
            cell: null,
            firstName: null,
            lastName: null,
            password: null,
            newPassword: null,
            reEnterPassword: null,
            municipalities: null,
            userToken: null,
            searchMunicipalityName: null,
            searchMunicipalitiesId: null,
            requiredRole: WSPostModel.requiredRole
        };
        
        if (typeof $scope.$user !== 'undefined' && $scope.$user !== null && $scope.$user.ID > 0) {
            $scope.$user.municipalities = $scope.$user.municipalities.map(function (elem) {
                                                return elem.name;
            }).join(", ");
        }

        ///pager
        $scope.$Pager = {
            PageSize: 15,
            PageNumber: 1,
            Counter:0,
            MaxPages: 0,
            pagerList: []
        }
        ///validate password
        $scope.validatePasswords = function ($form) {
            $scope.UserSearch.newPassword = $scope.UserSearch.newPassword.replace(/[\/]/gi, '');
            $form.newPassword.$setValidity('custom', true);
            $form.reEnterPassword.$setValidity('custom', true);
            
            if ($rootScope.$$credentials.isAdmin < 100) {
                ///return if not valid on other checks
                if ($form.reEnterPassword.$error.required || $form.reEnterPassword.$error.minlength || $form.reEnterPassword.$error.maxlength
                    || $form.newPassword.$error.required || $form.newPassword.$error.minlength || $form.newPassword.$error.maxlength)
                    return void [0];
                ///check minRequiredNonAlphanumericCharacters
                if ($scope.UserSearch.newPassword.replace(/[^\W|_]/gi, '').length < $scope.$passwordSettings.minNonAlphanumeric) {
                    $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] תווים מיוחדים!'.replace('[0]', $scope.$passwordSettings.minNonAlphanumeric);
                    $form.newPassword.$setValidity('custom', false);
                    return void [0];
                }
                ///check minRequiredNumericCharacters
                if ($scope.UserSearch.newPassword.replace(/[\D|_]/gi, '').length < $scope.$passwordSettings.minNumeric) {
                    $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] מספרים!'.replace('[0]', $scope.$passwordSettings.minNumeric);
                    $form.newPassword.$setValidity('custom', false);
                    return void [0];
                }
            }

            ///passwords dont match
            if ($scope.UserSearch.newPassword !== $scope.UserSearch.reEnterPassword) {
                $form.reEnterPassword.$setValidity('custom', false);
                return void [0];
            }
        }
        ///check if search form is empty
        $scope.emptyForm = function () {
            if ($scope.UserSearchForm.searchIdNumber.$pristine && $scope.UserSearchForm.cell.$pristine &&
                    $scope.UserSearchForm.firstName.$pristine && $scope.UserSearchForm.lastName.$pristine) {
                $scope.UserSearch.hidden_value = true;
                $scope.UserSearchForm.hidden_value.$setValidity('required', false);
                return true;
            }

            if ($scope.UserSearchForm.searchIdNumber.$modelValue == null && $scope.UserSearchForm.cell.$modelValue == null &&
                    $scope.UserSearchForm.firstName.$modelValue == null && $scope.UserSearchForm.lastName.$modelValue == null &&
                    $scope.UserSearchForm.searchIdNumber.$modelValue.isempty() && $scope.UserSearchForm.cell.$modelValue.isempty() &&
                    $scope.UserSearchForm.firstName.$modelValue.isempty() && $scope.UserSearchForm.lastName.$modelValue.isempty()) {
                $scope.UserSearch.hidden_value = true;
                $scope.UserSearchForm.hidden_value.$setValidity('required', false);
                return true;
            }
            
            return false;
        }

        $scope.GetUserMessages = function () {
            if ($scope.$user.cell !== null && $scope.$user.cell.length > 5) {
                $scope.CheckSms();
            }
            if ($scope.$user.email !== null && $scope.$user.email.length > 5) {
                $scope.CheckMails();
            }
        }
        $scope.CheckSms = function () {
            $scope.UserSearch.page = $scope.$user.ID;
            $scope.UserSearch.searchDate = last7();

            Admin.CheckUserSmsMessages($scope.UserSearch)
                .then(function successCallback(res) {
                    $scope.SmsMessages = parseSmsMessages(res.data);
                    if ($scope.SmsMessages === null || $scope.SmsMessages.length === 0) {
                        toastrService.info('ב-7 הימים האחרונים.', 'לא נשלחו הודעות SMS,', { timeOut: 5500 });
                    }
                    
                }, function errorCallback(res) {
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.CheckMails = function () {
            $scope.UserSearch.searchIdNumber = $scope.$user.idNumber;
            $scope.UserSearch.searchDate = last7();

            Admin.CheckUserMailMessages($scope.UserSearch)
                .then(function successCallback(res) {
                    $scope.EmailMessages = parseMailMessages(res.data);
                    if ($scope.EmailMessages === null || $scope.EmailMessages.length === 0) {
                        toastrService.info('ב-7 הימים האחרונים,', 'לא נשלחו הודעות דוא``ל.', { timeOut: 5500 });
                    }

                }, function errorCallback(res) {
                    console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.Search = function () {
            $scope.UserSearch.hidden_value = false;
            $scope.UserSearchForm.hidden_value.$setValidity('required', true);
            if ($scope.emptyForm() || $scope.UserSearchForm.$invalid)
                return void [0];

            $scope.$Pager.PageNumber = 1;
            
            Admin.SearchUsers($scope.UserSearch)
                .then(function successCallback(res) {
                    
                    $scope.$Pager.Counter = res.data.length;
                    $scope.$Pager.MaxPages = Math.ceil(res.data.length / $scope.$Pager.PageSize);
                    var num = 1;

                    $scope.$Pager.pagerList = [];
                    for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                        $scope.$Pager.pagerList.push(1 + i);
                    }

                    $scope.UsersList = res.data;

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        
        $scope.SendUserResetToken = function (user, type) {///1-mail, 0-sms
            $scope.UserSearch.page = type;
            $scope.UserSearch.searchIdNumber = user.idNumber;
           
            Admin.SendUserReset($scope.UserSearch)
                .then(function successCallback(res) {
                    if (res.data)
                        type == 0 ?
                            toastrService.success('הקוד נשלח בהודעת טקסט למספר הנייד של המנוי.', 'נשלח קוד אימות!', { allowHtml: true, timeOut: 5000, onHidden: function () { $state.go('Admin.ResetPassword', { IdNUmber: user.idNumber }); } })
                            : toastrService.success('הקוד נשלח בהודעת אימייל לכתובת המנוי.', 'נשלח קוד אימות!', { allowHtml: true, timeOut: 7000 });
                    else
                        toastrService.error('אנא נסה שנית לאחר טעינת הדף מחדש.', 'שגיאת תהליך!', { allowHtml: true, timeOut: 7000, onHidden: function () { $state.reload(); } });
                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!', { onHidden: function () { $state.reload(); } });
                });
        }

        $scope.ResetPayrollDisplay = function (user) {
            $scope.UserSearch.userToken = user.ID;
            Admin.ResetPayrollDisplay($scope.UserSearch)
                .then(function successCallback(res) {

                    if (res.data && res.status === 200) {
                        toastrService.success('תצוגת התלושים אופסה!', '');
                    } else {
                        toastrService.error('אנא נסה שנית.', 'תקלה!');
                    }

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });

        }

        $scope.ResetPassword = function () {
            $scope.validatePasswords($scope.UserSearchForm);
            if ($scope.UserSearchForm.$invalid)
                return void [0];

            $scope.UserSearch.password = $scope.UserSearch.newPassword;
            $scope.UserSearch.page = $stateParams.UserID;
            $scope.UserSearch.searchIdNumber = $stateParams.IdNUmber;

            Admin.ResetPassword($scope.UserSearch)
                .then(function successCallback(res) {
                    if (res.data.success && res.data.exceptionId === 0)
                        toastrService.success('', 'הסיסמה אופסה בהצלחה', { allowHtml: true, timeOut: 5000 });
                    else {
                        switch (res.data.exceptionId) {
                            case 7: //"NullReferenceException", 7
                            case 61:
                                toastrService.error('אנא וודע כי קוד האימות<br/> תואם ל SMS שנשלח ללקוח.', 'שגיאת זיהוי!', { allowHtml: true, timeOut: 5000 });
                                break;
                            case 8: //"verificationTokenExpirationException", 8
                                toastrService.error('פג תוקף אסימון האימות,<br/>אנא הפק אסימון אימות מחדש.', 'שגיאת אימות!', { allowHtml: true, timeOut: 5000, onHidden: function () { $state.go('Admin.SearchUsers'); } });
                                break;
                            case 55: //"UpdatePasswordException", 55
                                toastrService.error('אנא נסה שנית לאחר טעינת הדף מחדש.', 'שגיאת תהליך!', { allowHtml: true, timeOut: 5000, onHidden: function () { $state.reload(); } });
                                break;
                            default:
                                toastrService.error('אנא נסה שנית לאחר טעינת הדף מחדש.', 'שגיאת תהליך!', { allowHtml: true, timeOut: 5000, onHidden: function () { $state.reload(); } });
                                break;
                        }
                        
                    }
                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!', { onHidden: function () { $state.reload(); } });
                });
        }

        $scope.UpdateGroups = function () {
            $scope.UserSearch.searchIdNumber = $scope.$user.idNumber;
            $scope.UserSearch.searchMunicipalityName = $.map($.grep($scope.$groups, function (group) { return group.active }), function (group) { return group.ID }).join(",");
            $scope.UserSearch.searchMunicipalitiesId = $.map($.grep($scope.$groups, function (group) { return !group.active }), function (group) { return group.ID }).join(",");
            
            Admin.UpdateUserGroups($scope.UserSearch)
                .then(function successCallback(res) {
                    if(res.data && res.status === 200)
                        toastrService.success('הרשאות עודכנו בהצלחה', '', { allowHtml: true, timeOut: 5000 });
                    else
                        toastrService.error('','שגיאה!', { allowHtml: true, timeOut: 5000 });
                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!', { onHidden: function () { $state.reload(); } });
                });
        }

        $scope.parseUserGroups = function (groups, user) {
            groups = $.grep(groups, function (group) {
                group.name = group.name.toLowerCase();
                return $scope.getWSPostModel.isAdmin > group.immunity && group.immunity > 1
            });
            var _groups = $.grep(groups, function (group) {
                var _g = $.grep(user.groups, function (g) { return g.ID == group.ID });
                if (_g !== null && _g.length > 0) {
                    group.active = true;
                } else {
                    group.active = false;
                }
                return true;
            });
            return _groups;
        }
        $scope.$groups = $groups && $groups.data ? $scope.parseUserGroups($groups.data, $scope.$user) : {};

    }
})();

(function () {
    angular.module('WorkerPortfolioApp').controller('AdminMunicipalitiesController', AdminMunicipalitiesController);

    AdminMunicipalitiesController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$timeout', 'FileSaver', 'toastrService', 'Admin', 'ValidAdminUser', 'municipalities', '$municipality'];

    function AdminMunicipalitiesController($scope, $rootScope, $state, $stateParams, $timeout, FileSaver, toastrService, Admin, ValidAdminUser, municipalities, $municipality) {

        if (ValidAdminUser.data !== true || ValidAdminUser.status !== 200) {
            $state.go('Home');
            toastrService.info('', 'הרשאה לא תקינה!', { onHidden: function () { $state.go('Home'); } });
            return void [0];
        }


        $scope.getWSPostModel = $rootScope.getWSPostModel();
        $scope.MunicipalitiesList = [];
        $scope.UserSearch = {
            idNumber: $scope.getWSPostModel.idNumber,
            confirmationToken: $scope.getWSPostModel.confirmationToken,
            page: 1,
            hidden_value: false,
            $hidden_value: '',
            searchIdNumber: $stateParams.IdNUmber,
            cell: null,
            firstName: null,
            lastName: null,
            password: null,
            newPassword: null,
            reEnterPassword: null,
            municipalities: null,
            userToken: null,
            searchMunicipalityName: null,
            searchMunicipalitiesId: null,
            active: null,
            has101: null,
            hasProcess: null,
            hasEmailPaycheck: null,
            hasMunicipalityMenu: null,
            hasSystemsEmail: null,
            municipalitiesList: [],
        };
        $scope.Municipality = $municipality !== null ? $municipality.data : null;

        $scope.UserSearch.municipalities = $scope.getWSPostModel.municipalities.map(function (elem) {
            return elem.ID;
        }).join(",");

        $scope.DeleteContact = function ($index, contact) {
            Admin.DeleteHRContact({ idNumber: $scope.UserSearch.idNumber, contactId: contact.ID }).then(function successCallback(res) {
                if (res.status === 200 && res.data) {
                    $scope.Municipality.humanResourcesContacts.splice($index, 1);
                    toastrService.success('', 'רשומה ' + contact.ID + ' נמחקה!');

                    $scope.$Pager.Counter = $scope.municipality.data.humanResourcesContacts.length;
                    $scope.$Pager.MaxPages = Math.ceil($scope.municipality.data.humanResourcesContacts.length / $scope.$Pager.PageSize);
                    for (var i = 0; i <= $scope.$Pager.MaxPages; i++)
                        $scope.$Pager.pagerList.push(i + 1);
                } else {
                    toastrService.error('רשומה לא נמחקה.', 'תקלת מערכת!');
                }
            }, function errorCallback(res) {
                toastrService.error('', 'תקלת מערכת!');
            });
        }
        $scope.StartUpdateContact = function ($index, type) {
            if ($index !== null && type !== null && $index < $scope.Municipality.humanResourcesContacts.length) {
                $scope.Municipality.humanResourcesContacts[$index].Editing = type;
            }
            $scope.Municipality.humanResourcesContacts.filter(function (hr, index) {
                if ($index === null || index !== $index) hr.Editing = false;
            });
            switch (type) {
                case true:
                    $scope.UserSearch.contact = angular.copy($scope.Municipality.humanResourcesContacts[$index]);
                    $scope.UserSearch.contact.$index = $index;
                    break;
                case false:
                    $scope.UserSearch.contact = null;
                    break;
                case null:
                    $scope.UserSearch.contact = {
                        Editing: true,
                        $index: $scope.Municipality.humanResourcesContacts.length,
                        email: '',
                        job: '',
                        name: '',
                        phone: '',
                        municipalityID: $stateParams.MunicipalityId,
                    }
                    break;
            }
        }
        $scope.UpdateContact = function ($index) {
            $scope.MunicipalityForm.$setSubmitted();
            if ($scope.MunicipalityForm.$invalid || !$scope.MunicipalityForm.$valid)
                return void [0];

            Admin.UpdateContact($scope.UserSearch).then(function successCallback(res) {
                if (res.status === 200) {
                    if ($index < $scope.Municipality.humanResourcesContacts.length) {
                        $scope.Municipality.humanResourcesContacts[$index] = res.data;

                        toastrService.success('', 'רשומה ' + res.data.ID + ' עודכנה בהצלחה!');
                    } else {
                        $scope.UserSearch.contact = null;
                        $scope.Municipality.humanResourcesContacts.push(res.data);
                        $scope.$Pager.Counter = $scope.Municipality.humanResourcesContacts.length;
                        $scope.$Pager.MaxPages = Math.ceil($scope.Municipality.humanResourcesContacts.length / $scope.$Pager.PageSize);
                        for (var i = 0; i <= $scope.$Pager.MaxPages; i++)
                            $scope.$Pager.pagerList.push(i + 1);

                        toastrService.success('', 'רשומה ' + res.data.ID + ' נוספה בהצלחה!');
                    }
                    $scope.Municipality.humanResourcesContacts[$index].Editing = false;
                    $scope.UserSearch.contact = null;

                } else {
                    $scope.Municipality.humanResourcesContacts[$index].Editing = false;
                    $scope.UserSearch.contact = null;
                    $scope.MunicipalityForm.$setSubmitted();
                    toastrService.error('רשומה לא התעדכנה.', 'תקלה!');
                }
            }, function errorCallback(res) {
                toastrService.error('', 'תקלת מערכת!');
            });
        }
        $scope.Update = function () {
            $scope.UserSearch.municipalitiesList = [];
            $scope.UserSearch.municipalitiesList.push($scope.Municipality);
            Admin.UpdateMunicipalities($scope.UserSearch)
               .then(function successCallback(res) {
                   res.data ? toastrService.success('העדכון נשמר בהצלחה!', '') : toastrService.error('', 'תקלת מערכת!');

               }, function errorCallback(res) {
                   toastrService.error('', 'תקלת מערכת!');
               });
        }

        ///pager
        $scope.$Pager = {
            PageSize: 15,
            PageNumber: 1,
            Counter: 0,
            MaxPages: 0,
            pagerList: []
        }

        if (municipalities.data && municipalities.data.length > 1) {
            $scope.$Pager.Counter = municipalities.data.length;
            $scope.$Pager.MaxPages = Math.ceil(municipalities.data.length / $scope.$Pager.PageSize);
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++)
                $scope.$Pager.pagerList.push(i + 1);

            $scope.MunicipalitiesList = municipalities.data;
        }
        if ($municipality && $municipality.data && $municipality.data.humanResourcesContacts && $municipality.data.humanResourcesContacts.length > 1) {
            $scope.$Pager.Counter = $municipality.data.humanResourcesContacts.length;
            $scope.$Pager.MaxPages = Math.ceil($municipality.data.humanResourcesContacts.length / $scope.$Pager.PageSize);
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++)
                $scope.$Pager.pagerList.push(i + 1);
        }


        $scope.allow101Change = function (municipality) {
            if (municipality.allow101)
                municipality.active = true;
            municipality.$pristine = false;
        }

        $scope.SearchMunicipality = function () {
            $scope.$Pager.PageNumber = 1;
            Admin.GetMunicipalities($scope.UserSearch)
                .then(function successCallback(res) {
                    $scope.MunicipalitiesList = [];
                    $scope.$Pager.Counter = res.data.length;
                    $scope.$Pager.MaxPages = Math.ceil(res.data.length / $scope.$Pager.PageSize);

                    $scope.$Pager.pagerList = [];
                    for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                        $scope.$Pager.pagerList.push(i + 1);
                    }

                    $scope.MunicipalitiesList = res.data;

                }, function errorCallback(res) {
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.updateMunicipalitiesList = function () {
            $scope.UserSearch.municipalitiesList = [];
            $scope.UserSearch.municipalitiesList = $.grep($scope.MunicipalitiesList, function (item) {
                return item.$pristine === false;
            });

            Admin.UpdateMunicipalities($scope.UserSearch)
               .then(function successCallback(res) {
                   res.data ? toastrService.success('העדכון נשמר בהצלחה!', '') : toastrService.error('', 'תקלת מערכת!');

               }, function errorCallback(res) {
                   toastrService.error('', 'תקלת מערכת!');
               });
        }

        $scope.updateMunicipality = function (municipality) {
            $scope.UserSearch.municipality = municipality;

            Admin.UpdateAuthorizationList($scope.UserSearch)
                .then(function successCallback(res) {

                    toastrService.success('העדכון נשמר בהצלחה!', '');

                }, function errorCallback(res) {
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.Export = function () {
            var dataObj = [];
            $.map($scope.MunicipalitiesList, function (item) {
                dataObj.push({
                    'מספר_ארגון': item.rashut,
                    'שם_ארגון': item.name,
                    'פעיל': item.active ? 'כן' : 'לא',
                    '101_דיגיטאלי': item.allow101 ? 'כן' : 'לא',
                    'דפי_ארגון': item.allowMunicipalityMenu ? 'כן' : 'לא',
                    'מיילר': item.allowSystemsEmail ? 'כן' : 'לא',
                    'תלוש_במייל': item.allowemailpaycheck ? 'כן' : 'לא',
                    'תהליכים': item.allowprocess ? 'כן' : 'לא',
                });
            });
            return dataObj;
        }
    }
})();

(function () {    

    angular.module('WorkerPortfolioApp').controller('AttendanceController', AttendanceController);

    AttendanceController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$location', 'PayrollServise', 'toastrService', '$controllerState', 'AttendanceData'];

    function AttendanceController($scope, $rootScope, $state, $stateParams, $location, PayrollServise, toastrService, $controllerState, AttendanceData) {
        ///scope functions
        var setTitels = function (status) {
            $scope.pageTitle = null;
            $scope.pageSubTitle = null;
            var title = 'פירוט נתוני TYPE בהתפלגות שנתית';
            var subtitle = 'לא קיימים נתוני TYPE לשנת YEAR'
            switch ($controllerState) {
                case 'Vacation':
                    $scope.pageTitle = title.replace('TYPE', 'חופשה');
                    $scope.pageSubTitle = status === 406 ? subtitle.replace('TYPE', 'חופשה').replace('YEAR', $scope.currentYear) : null;
                    break;
                case 'SickLeave':
                    $scope.pageTitle = title.replace('TYPE', 'מחלה');
                    $scope.pageSubTitle = status === 406 ? subtitle.replace('TYPE', 'מחלה').replace('YEAR', $scope.currentYear) : null;
                    break;
                case 'AdvancedStudy':
                    $scope.pageTitle = title.replace('TYPE', 'השתלמות');
                    $scope.pageSubTitle = status === 406 ? subtitle.replace('TYPE', 'השתלמות').replace('YEAR', $scope.currentYear) : null;
                    break;

                default:
                    $state.go('Home');
                    break;
            }
        }
        var replaceInItem = function (collection, item, replace, replaceWidth) {
            if (typeof replaceWidth === 'undefined' || replaceWidth == null)
                replaceWidth = '';
            collection.filter(function (data) { data[item] = data[item].replace(replace, replaceWidth).trim(); });
        }
       
        $scope.WSPostModel = $rootScope.getWSPostModel();
        
        $scope.AttendanceData = {
            Titles: new Array(),
            Data: new Array()
        }
        $scope.pageTitle = null;
        $scope.pageSubTitle = null;
        $scope.currentYear = $scope.WSPostModel.selectedDate.getFullYear();

        $scope.$controllerState = $controllerState;
        
        $scope.changeYear = function () {
            $scope.WSPostModel.selectedDate = new Date($scope.currentYear, 3, 3);
            PayrollServise.GetAttendanceFigures($scope.WSPostModel)
                .then(function successCallback(res) {
                    $scope.AttendanceData = {
                        Titles: new Array(),
                        Data: new Array(),
                    };

                    switch ($controllerState) {
                        case 'Vacation':
                            setTitels(res.status);
                            if (res.status === 406) {
                                return;
                            }
                            $scope.AttendanceData.Titles = res.data[0].Titles.sort(function (a, b) { return (a.number > b.number); });
                            replaceInItem($scope.AttendanceData.Titles, 'title', 'חופש');
                            replaceInItem($scope.AttendanceData.Titles, 'title', 'זיכוי', 'זיכוי חודשי');
                            replaceInItem($scope.AttendanceData.Titles, 'title', 'ניצול', 'ניצול חודשי');

                            res.data[0].Data.filter(function (row) {
                                $scope.AttendanceData.Data.push(row.sort(function (a, b) { return (a.columnNumber > b.columnNumber); }));
                            });
                            break;
                        case 'SickLeave':
                            setTitels(res.status);
                            if (res.status === 406) {
                                return;
                            }
                            $scope.AttendanceData.Titles = res.data[1].Titles.sort(function (a, b) { return (a.number > b.number); });
                            replaceInItem($scope.AttendanceData.Titles, 'title', 'ניצול', 'ניצול חודשי');

                            res.data[1].Data.filter(function (row) {
                                $scope.AttendanceData.Data.push(row.sort(function (a, b) { return (a.columnNumber > b.columnNumber); }));
                            });
                            break;
                        case 'AdvancedStudy':
                            setTitels(res.status);
                            if (res.status === 406) {
                                return;
                            }
                            $scope.AttendanceData.Titles = res.data[2].Titles.sort(function (a, b) { return (a.number > b.number); });
                            res.data[2].Data.filter(function (row) {
                                $scope.AttendanceData.Data.push(row.sort(function (a, b) { return (a.columnNumber > b.columnNumber); }));
                            });
                            break;

                        default:
                            $state.go('Home');
                            break;
                    }

                }, function errorCallback(res) {
                    //console.log('error', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
            
        }

        switch ($controllerState) {
            case 'Vacation':
                setTitels();
                if (AttendanceData.status === 406 || typeof AttendanceData.data === 'undefined' || AttendanceData.data.length < 1) {
                    return;
                }

                $scope.AttendanceData.Titles = AttendanceData.data[0].Titles.sort(function (a, b) { return (a.number > b.number); });
                replaceInItem($scope.AttendanceData.Titles, 'title', 'חופש');
                replaceInItem($scope.AttendanceData.Titles, 'title', 'זיכוי', 'זיכוי חודשי');
                replaceInItem($scope.AttendanceData.Titles, 'title', 'ניצול', 'ניצול חודשי');

                AttendanceData.data[0].Data.filter(function (row) {
                    $scope.AttendanceData.Data.push(row.sort(function (a, b) { return (a.columnNumber > b.columnNumber); }));
                });

                break;
            case 'SickLeave':
                setTitels();
                if (AttendanceData.status === 406 || typeof AttendanceData.data === 'undefined' || AttendanceData.data.length < 1) {
                    return;
                }

                $scope.AttendanceData.Titles = AttendanceData.data[1].Titles.sort(function (a, b) { return (a.number > b.number); });
                replaceInItem($scope.AttendanceData.Titles, 'title', 'ניצול', 'ניצול חודשי');

                AttendanceData.data[1].Data.filter(function (row) {
                    $scope.AttendanceData.Data.push(row.sort(function (a, b) { return (a.columnNumber > b.columnNumber); }));
                });

                break;
            case 'AdvancedStudy':
                setTitels(AttendanceData.status);
                if (AttendanceData.status === 406 || typeof AttendanceData.data === 'undefined' || AttendanceData.data.length < 1) {
                    return;
                }

                $scope.AttendanceData.Titles = AttendanceData.data[2].Titles.sort(function (a, b) { return (a.number > b.number); });
                replaceInItem($scope.AttendanceData.Titles, 'title', 'השתלמות');

                AttendanceData.data[2].Data.filter(function (row) {
                    $scope.AttendanceData.Data.push(row.sort(function (a, b) { return (a.columnNumber > b.columnNumber); }));
                });

                break;
            case 'AttendanceSheet':
                ///TODO -> OPEN AttendanceSheet FROM OUTER SITE AND GO TO HOME PAGE!

                break;

            default:
                $state.go('Home');
                break;
        }

        $scope.tableType = $scope.AttendanceData.Data[0].length < 4 ? 'short' : 'full';
    }
})();

(function () {
    angular.module('WorkerPortfolioApp').controller('AttestationController', AttestationController);

    AttestationController.$inject = ['$window', '$scope', '$rootScope', '$state', '$timeout', '$sce', 'TofesDataService', 'TaxFromModelService', 'TofesModel', 'FileUploader', 'toastrService', 'Search', 'fileUploade', 'fileUploadeFactory', 'formDataService', 'Tofes'];

    function AttestationController($window, $scope, $rootScope, $state, $timeout, $sce, TofesDataService, TaxFromModelService, TofesModel, FileUploader, toastrService, Search, fileUploade,
        fileUploadeFactory, formDataService, Tofes) {
        
        var b64toBlob = function (b64Data, contentType) {
            contentType = contentType || '';
            var sliceSize = 512;
            b64Data = b64Data.replace(/^[^,]+,/, '');
            b64Data = b64Data.replace(/\s/g, '');
            var byteCharacters = window.atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }

        $scope.pdfContent = '';
        $scope.$pdfContent = false;
        $scope.pdfDataStatus = false;

        ///private variables
        ///get radio buttons data
        $scope.radionStates = TofesDataService.GetRadiosStates();
        ///get custom models for tofesModel lists
        $scope.customModel = TofesDataService.GetCustomModel();
        $scope.MaxDateLimit = $rootScope.LocalDateString(new Date());
        $scope.tofesModel = TofesModel;
        $scope.LocalCredentials = typeof $rootScope.$$credentials !== 'undefined' ? $rootScope.$$credentials : $rootScope.lsGet('$$credentials');
        ///update from saved data if have
        TofesDataService.GetExistingData($scope.tofesModel, '$$tofesModel', true);
        ///set the signatur date for today
        $scope.tofesModel.signatureDate = TofesDataService.TransformToDate(new Date());

        ///go-to next/prev form stap, published from Header directive
        $scope.subscribe('goto.step', function (goTo, direction) {
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            ///go to the step
            $state.go(goTo);
        });
        ///hide the form 101 pdf
        $scope.subscribe('hidePdf', function (hide) {
            $scope.pdfContent = '';
            $scope.$pdfContent = false;
        });
        ///submit form 101 data to WS
        $scope.subscribe('submitPdf', function (hide) {
            fileUploadeFactory.SendForm(TaxFromModelService.SetModel(TofesDataService.CheckRunTimeCopy($scope.tofesModel), $scope.LocalCredentials.currentMunicipalityId))
                .then(function successCallback(response) {
                    if (response.status == 200 && response.data.success) {
                        toastrService.custom('בסיום התהליך תשלח אליך לנייד/אימייל הודעה.', 'הטופס נשלח לארכיון בהצלחה', { iconClass: 'toast-grean-full toast-success' });
                        $rootScope.lsRemove('$$tofesModel');
                        $rootScope.lsRemove('$$tofesModelCopy');
                        $rootScope.lsRemove('states');
                        $state.go('Home');
                    }
                    else {
                        switch (response.data.exceptionId) {
                            case -1:///archive error only
                                toastrService.error('אנא נסה שנית.', 'התרחשה תקלה בשליחת הטופס לארכיון!');
                                $scope.publish('returnToLastStep', true);
                                break;
                            case -2:///post data error only
                                toastrService.error('אנא נסה שנית.', 'התרחשה תקלה בשמירת המידע בטופס!');
                                $scope.publish('returnToLastStep', true);
                                break;
                            case -3:///archive AND post data error
                                toastrService.error('אנא צור את הטופס מהתחלה.', 'התרחשה תקלת שרת!');
                                $rootScope.lsRemove('$$tofesModel');
                                $rootScope.lsRemove('$$tofesModelCopy');
                                $rootScope.lsRemove('states');
                                $state.go('Home');
                                break;
                        }
                    }
                }, function errorCallback(response) {
                    $scope.publish('returnToLastStep', true);
                    //console.log('submitPdf errorCallback', response);
                    toastrService.error('אנא נסה שנית מאוחר יותר', 'תקלת מערכת!');
                });
        });
        $scope.validateSignature = function () {
            $scope.partForm.hidden_signature.$setValidity('required', true);
            if (!$scope.tofesModel.signature.isEmpty()) {
                $scope.tofesModel.hidden_signature = 1;
            } else {
                $scope.tofesModel.hidden_signature = null;
                $scope.partForm.hidden_signature.$setValidity('required', false);
            }
        }
        ///main submit form function
        $scope.saveEntireForm = function () {
            $scope.validateSignature();
            ///form is invalid
            if ($scope.partForm.$invalid) {
                $scope.ErrorFocus();
                $scope.publish('returnToLastStep', true);
                return void [0];
            }
            
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            $scope.publish('$$states', 'current');

            ///first step-> save the signature;
            fileUploadeFactory.Signature($scope.tofesModel.employee.idNumber, $scope.tofesModel.year, $scope.tofesModel.signature.toDataURL(), $scope.LocalCredentials.confirmationToken, $scope.LocalCredentials.currentMunicipalityId)
                .then(function successCallback(response) {
                    if (response.data.saved) {
                        $scope.fileimageblob = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXY3j//j0ABZ4CzkTR0PgAAAAASUVORK5CYII=';

                        $scope.tofesModel.signatureImageID = response.data.image.ID;
                        fileUploadeFactory.SaveForm(TaxFromModelService.SetModel(TofesDataService.CheckRunTimeCopy($scope.tofesModel), $scope.LocalCredentials.currentMunicipalityId))
                            .then(function successCallback(response) {
                                if (response.data.sign) {
                                    $scope.pdfContent = '';
                                    $scope.$pdfContent = false;
                                    $scope.formMsg = '';
                                    $scope.pdfDataStatus = response.status;
                                    if (response.status === 406)
                                        return void [0];
                                    $scope.tofesModel.formPdfImageID = response.data.pdf.ID;
                                    $scope.tofesModel.formPdf = response.data.pdf;
                                    $scope.tofesModel.version = response.data.version;
                                    ///save the form state
                                    $rootScope.lsSet('$$tofesModel', $scope.tofesModel);

                                    var file = b64toBlob(response.data.pdf.fileString, 'application/pdf');
                                    var fileURL = URL.createObjectURL(file);
                                    $scope.pdfContent = $sce.trustAsResourceUrl(fileURL);
                                    $scope.$pdfContent = true;
                                    $scope.formMsg = 'תצוגה מקדימה - טופס 101.';

                                    $scope.fileimageblob = response.data.pdf.fileimageblob;
                                } else {
                                    $scope.publish('returnToLastStep', true);
                                    //console.log('SaveForm error', response);
                                    toastrService.error('המערכת לא הצליחה לשמור טופס 101!', 'התרחשה תקלה!');
                                }
                            }, function errorCallback(response) {
                                $scope.publish('returnToLastStep', true);
                                //console.log('SaveForm errorCallback', response);
                                toastrService.error('המערכת לא הצליחה לשמור טופס 101!', 'התרחשה תקלה!');
                            });
                    } else {
                        $scope.publish('returnToLastStep', true);
                        //console.log('Signature error', response);
                        toastrService.error('המערכת לא הצליחה לשמור את החתימה!', 'התרחשה תקלה!');
                        return void [0];
                    }
                }, function errorCallback(response) {
                    $scope.publish('returnToLastStep', true);
                    //console.log('Signature errorCallback', response);
                    toastrService.error('המערכת לא הצליחה לשמור את החתימה!', 'התרחשה תקלה!');
                });

            return void [0];
        }

        $scope.ErrorFocus = function () {
            if (typeof $scope.partForm.$error.required == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
            if (typeof $scope.partForm.$error.custom == 'object') {
                $scope.partForm.$error.custom[0].$$element.focus();
                return;
            }
        }
    }
})();
(function () {
    angular.module('WorkerPortfolioApp').controller('AuthenticationController', AuthenticationController);

    AuthenticationController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$location', 'Credentials', 'Search', 'toastrService', '$controllerState', '$passwordSettings', '$window'];

    function AuthenticationController($scope, $rootScope, $state, $stateParams, $location, Credentials, Search, toastrService, $controllerState, $passwordSettings, $window) {

        ///scope params
        $scope.$passwordSettings = $passwordSettings.data;
        $scope.$controllerState = $controllerState;
        $scope.UserCredentials = {};
        ///scope functions
        $scope.parseLastLogIn = function (dateStr) {
            var dateArray = dateStr.split('T'), model = {};
            model.lastDate = dateArray[0].split('-').splice(1, 2).reverse().join('/');
            model.lastTime = dateArray[1].split('.')[0].split(':').splice(0, 2).join(':');

            return model;
        }
        $scope.parseFullUserData = function (user) {
            return {
                ID: user.ID,
                birthDate: $scope.parseIntoDate(user.birthDate),
                set_birthDate: $scope.parseIntoDate(user.birthDate, true),
                createdDate: user.createdDate,
                cell: user.cell,
                email: user.email,
                firstName: user.firstName,
                idNumber: user.idNumber,
                immigrationDate: $scope.parseIntoDate(user.immigrationDate),
                set_immigrationDate: $scope.parseIntoDate(user.immigrationDate, true),
                lastName: user.lastName,
                phone: user.phone
            }
        }
        $scope.parseIntoDate = function (d, isSet) {
            try {
                var date = d && typeof d === 'string' && d.length > 10 ? new Date(d) : null, returnDate = null;
                if (date !== null) {
                    returnDate = date.getDate() + '/' + (date.getMonth() + 1 < 10 ? ('0' + (date.getMonth() + 1)) : (date.getMonth() + 1)) + '/' + date.getFullYear();
                    if (isSet) returnDate = (date.getMonth() + 1 < 10 ? ('0' + (date.getMonth() + 1)) : (date.getMonth() + 1)) + '/' + date.getDate() + '/' + date.getFullYear();
                }

                return returnDate;
            } catch (e) {
                //console.log('e', e);
                return null;
            }
        }
        $scope.ReSetDate = function (d) {
            if (!d || typeof d !== 'string' || d.length < 8)
                return null;
            var sd = d.split('/'), returnDate = new Date();
            returnDate.setMonth((parseInt(sd[1]) - 1)); returnDate.setFullYear(sd[2]); returnDate.setDate(sd[0]);
            return returnDate;
        }
        $scope.setRequired = function () {
            $scope.UserCredentials.initialPasswordRequired = true;
            $scope.UserCredentials.emailRequired = true;
            $scope.UserCredentials.cellRequired = true;
            if ($scope.UserCredentials.initialPassword != null && !$scope.UserCredentials.initialPassword.isempty() && $scope.RegisterForm.initialPassword.$valid) {
                $scope.UserCredentials.emailRequired = false;
                $scope.UserCredentials.cellRequired = false;
            }
            else if ($scope.UserCredentials.cell != null && !$scope.UserCredentials.cell.isempty() && $scope.RegisterForm.cell.$valid) {
                $scope.UserCredentials.emailRequired = false;
                $scope.UserCredentials.initialPasswordRequired = false;
            }
            else if ($scope.UserCredentials.email != null && !$scope.UserCredentials.email.isempty() && $scope.RegisterForm.email.$valid) {
                $scope.UserCredentials.initialPasswordRequired = false;
                $scope.UserCredentials.cellRequired = false;
            }
        }

        /* ****** AUTOCOMPLETE ****** */
        $scope.selected = { item: null };
        $scope.selectedMunicipality = [];
        $scope.municipalities = [];
        $scope.searchMunicipality = function (m) {
            Search.MunicipalitiesSearch({ query: m, results: 10 })
                .then(function (res) {
                    $scope.municipalities = res.data;
                });
        }
        /* ****** AUTOCOMPLETE ****** */

        switch ($controllerState) {
            case 'PasswordReset':
                $scope.publish('passwordResetLogout');
                $scope.UserCredentials = {
                    newPassword: null,
                    reEnterPassword: null,
                    token: $stateParams.Token,
                    passwordResetType: 1
                };
                break;
            case 'UpdateDetails':
                $scope.UserCredentials = $rootScope.lsGet('$$credentials');

                Credentials.GetFullUserdetails({ Token: $scope.UserCredentials.confirmationToken, newPassword: null })
                    .then(function successCallback(res) {
                        $scope.UserCredentials = $scope.parseFullUserData(res.data);
                    }, function errorCallback(res) {
                        //console.log('full-error', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
                break;
            case 'SMSReset':
                $scope.publish('passwordResetLogout');
                $scope.UserCredentials = {
                    newPassword: null,
                    reEnterPassword: null,
                    token: null,
                    passwordResetType: 2,
                    idNumber: $stateParams.idNumber || null
                };
                break;
            case 'AuthenticatedUserLogin':

                $rootScope.$$credentials_unbind;
                $rootScope.lsRemove('$$credentials');

                Credentials.GetCurrentUser({ authenticationToken: $stateParams.AuthenticationSuccessToken })
                    .then(function successCallback(res) {
                        $scope.publish('RefreshLoginState', res.data);
                    }, function errorCallback(res) {
                        //console.log('error', res);
                        toastrService.error('', 'תקלת מערכת!');
                        $state.go('Error');
                    });

                break;
            case 'UserRegister':
                ///remove the option to register
                $state.go('Home');

                $scope.UserCredentials = {
                    municipalities: null,
                    idNumber: null,
                    isValidToRegister: false,
                    returnObjID: -1,
                    showSmsField: false,
                    token: null,
                    initialPassword: "",
                    initialPasswordRequired: true,
                    email: "",
                    emailRequired: true,
                    cell: "",
                    cellRequired: true,
                };
                break;
            case 'InitialActivation':
                $scope.UserCredentials = {
                    cell: false,
                    email: false,
                    idNumber: $stateParams.idNumber || null,
                    showError: false,
                    sendOption: null,
                    $sendVal: null,
                };
                break;
            case 'LoginToProcess':
                
                $rootScope.$$credentials_unbind;
                $rootScope.lsRemove('$$credentials');
                Credentials.GetCurrentUserForProcesses({ authenticationToken: $stateParams.AuthenticationSuccessToken, rashut: $stateParams.rashut, mncplity: $stateParams.mncplity })
                    .then(function successCallback(res) {
                        $scope.publish('RefreshLoginState', res.data);
                        $state.go('UserMenu.MyProcesses');
                    }, function errorCallback(res) {
                        //console.log('error', res);
                        toastrService.error('', 'תקלת מערכת!');
                        $state.go('Error');
                    });
                break;
            default:
                $state.go('Home');
                break;
        }
        $scope.setSendVal = function (val) {
            if (val == 'undefined' || val == null) {
                $scope.ActivationForm.credentials_sendVal.$setValidity('required', false);
                return void [0];
            }
            $scope.UserCredentials.sendOption = val;
            $scope.ActivationForm.credentials_sendVal.$setValidity('required', true);
        }
        ///InitialActivation
        $scope.activate = function () {
            $scope.setSendVal($scope.UserCredentials.sendOption);
            if ($scope.ActivationForm.$invalid)
                return void [0];
            var data = {
                idNumber: $scope.UserCredentials.idNumber,
                password: $scope.UserCredentials.sendOption,
                key: $scope.UserCredentials.sendOption === 1,
                iv: $scope.UserCredentials.sendOption === 2
            }
            Credentials.InitialActivationCheck(data)
                .then(function successCallback(res) {
                    switch (res.data.exceptionId) {
                        case 200:///IdNumberNullException
                            toastrService.error('יש לוודא מול נציג הארגון שפרטי המייל / טלפון נייד מוגדרים במערכת.', 'לא נמצא משתמש לפי הפרטים שסיפקת!', { allowHtml: true, timeOut: 8000 });
                            $scope.UserCredentials.showError = 1;
                            break;
                        case 201:///InitialCheckException
                            toastrService.error('לא ניתן לבצע אימות ראשוני בשנית!', 'אימות ראשוני כבר בוצע!', { allowHtml: true, timeOut: 7000 });
                            $scope.UserCredentials.showError = 2;
                            break;
                        case 111:///NullEmailException
                            toastrService.error('אנא פנה לכ"א על מנת לעדכן פרטים.<br/>ניתן לבצע אימות נתונים ע"י SMS.', 'למשתמש לא מוגדרת כתובת מייל במערכת!', { allowHtml: true, timeOut: 8000 });
                            break;
                        case 112:
                            toastrService.error('אנא פנה לכ"א על מנת לעדכן פרטים.<br/>ניתן לבצע אימות נתונים ע"י שליחת מייל.', 'למשתמש לא מוגדר מספר טלפון נייד במערכת!', { allowHtml: true, timeOut: 8000 });
                            break;
                        case 0:
                            switch ($scope.UserCredentials.sendOption) {
                                case 1:///email
                                    toastrService.success('מייל עדכון סיסמה נשלח לכתובת המייל הרשומה, אנא עקוב אחר ההוראות במייל לאיפוס סימתך.', 'תודה, ' + res.data.userCredentials.fullName, { allowHtml: true, timeOut: 8000 });
                                    $state.go('Home');
                                    break;
                                case 2:///sms
                                    toastrService.success('מזהה איפוס סיסמה נשלח לטלפון הנייד הרשום במערכת, אנא הכנס את הקוד לשדה המיועד.', 'תודה, ' + res.data.userCredentials.fullName, { allowHtml: true, timeOut: 8000 });
                                    $state.go('Authentication.ResetPasswordFromSms', { idNumber: $scope.UserCredentials.idNumber });
                                    break;
                            }
                            break;
                        default:
                            break;

                    }

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('אנא נסא שנית מאוחר יותר,<br/>במקרה שהתקלה חוזרת ניתן לפנות למנהל האתר בצור קשר באתר.', 'התרחשה שגיאת שרת פנימית!', { allowHtml: true, timeOut: 8000 });
                    $scope.UserCredentials.showError = 3;
                });
        }
        ///user register
        $scope.register = function () {
            if ($scope.UserCredentials.isValidToRegister) $scope.validatePasswords($scope.RegisterForm);

            if ($scope.RegisterForm.$invalid)
                return void [0];

            if (!$scope.UserCredentials.isValidToRegister) {
                Credentials.CheckValidUser($scope.setUserData())
                    .then(function successCallback(res) {
                        var data = res.data;
                        if (!data.success) {
                            toastrService.error('לא נמצא משתמש לפי הפרטים שסיפקת', 'פרטים שגויים');
                            return void [0];
                        }
                        $scope.UserCredentials.returnObjID = data.exceptionId;
                        switch (data.exceptionId) {
                            case 0:///validated by initialPassword
                                $scope.UserCredentials.isValidToRegister = true;
                                break;
                            case 1:
                                $scope.UserCredentials.isValidToRegister = true;
                                break;
                            case 2:
                                $scope.UserCredentials.isValidToRegister = true;
                                break;
                            default:
                                toastrService.error('', 'תקלת מערכת!');
                                return void [0];
                                break;
                        }
                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
            } else {
                switch ($scope.UserCredentials.returnObjID) {
                    case 0:
                        $scope.registerFromInitialPassword();
                        break;
                    case 1:
                        $scope.RegisterFromSms();
                        break;
                    case 2:
                        $scope.RegisterFromEmail();
                        break;
                }
            }

        }
        $scope.verifyToken = function () {
            if ($scope.verifyTokenForm.$invalid)
                return void [0];
            Credentials.VerifySmsToken($scope.setSmsUserData())
                .then(function successCallback(res) {
                    var data = res.data;
                    switch (data.exceptionId) {
                        case 0:
                            $scope.publish('RefreshLoginState', res.data.userCredentials);
                            break;

                        case 7://NullReferenceException
                            toastrService.error('הקוד שסיפקת אינו קיים במערכת!', 'תקלה!');
                            break;
                        case 8://verificationTokenExpirationException
                            toastrService.error('תוקף קוד האימות פקע, אנא צור חדש.', 'תקלה!');
                            break;
                        default:
                            toastrService.error('', 'תקלת מערכת!');
                            return void [0];
                            break;
                    }

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        ///reset password scope functions
        $scope.resetPassword = function () {
            $scope.validatePasswords($scope.PasswordResetForm);
            if ($scope.PasswordResetForm.$invalid)
                return void [0];
            ///reset password
            Credentials.ChangePasswordFromToken({ Token: $scope.UserCredentials.token, newPassword: $scope.UserCredentials.newPassword, passwordResetType: $scope.UserCredentials.passwordResetType, idNumber: $scope.UserCredentials.idNumber })
                .then(function successCallback(res) {

                    if (res.data.success) {
                        ///remove old login data
                        $rootScope.$$credentials_unbind;
                        $rootScope.lsRemove('$$credentials');
                        toastrService.success('סיסמתך עודכנה בהצלחה!', 'תודה.');
                        $scope.publish('passwordResetLonin', res.data.userCredentials);
                        $state.go('Home');
                    } else {
                        switch (res.data.exceptionId) {
                            case 61:///GetUserFromToken
                                toastrService.error('תוקף אסימון אימות הסיסמה פג, אנא נסה ליצור חדש.', 'שגיאת אימות נתונים!');
                                break;

                            default:
                                //console.log('error', res);
                                toastrService.error('', 'תקלת מערכת!');
                                break;
                        }
                    }
                }, function errorCallback(res) {
                    //console.log('error', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.validatePasswords = function ($form) {
            $scope.UserCredentials.newPassword = $scope.UserCredentials.newPassword.replace(/[\/]/gi, '');
            $form.newPassword.$setValidity('custom', true);
            $form.reEnterPassword.$setValidity('custom', true);
            ///return if not valid on other checks
            if ($form.reEnterPassword.$error.required || $form.reEnterPassword.$error.minlength || $form.reEnterPassword.$error.maxlength
                || $form.newPassword.$error.required || $form.newPassword.$error.minlength || $form.newPassword.$error.maxlength)
                return void [0];
            ///check minRequiredNonAlphanumericCharacters
            if ($scope.UserCredentials.newPassword.replace(/[^\W|_]/gi, '').length < $scope.$passwordSettings.minNonAlphanumeric) {
                $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] תווים מיוחדים!'.replace('[0]', $scope.$passwordSettings.minNonAlphanumeric);
                $form.newPassword.$setValidity('custom', false);
                return void [0];
            }
            ///check minRequiredNumericCharacters
            if ($scope.UserCredentials.newPassword.replace(/[\D|_]/gi, '').length < $scope.$passwordSettings.minNumeric) {
                $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] מספרים!'.replace('[0]', $scope.$passwordSettings.minNumeric);
                $form.newPassword.$setValidity('custom', false);
                return void [0];
            }
            ///passwords dont match
            if ($scope.UserCredentials.newPassword !== $scope.UserCredentials.reEnterPassword) {
                $form.reEnterPassword.$setValidity('custom', false);
                return void [0];
            }
        }
        ///update user details scope functions
        $scope.updateUser = function () {
            if ($scope.UpdateDetailsForm.$invalid)
                return void [0];

            Credentials.UpdateUserDetails($scope.setSendData())
                .then(function successCallback(res) {
                    toastrService.success('נתוניך התעדכנו בהצלחה!', 'תודה.');
                    $scope.publish('UpdateShowLoginSatet', res.data);
                }, function errorCallback(res) {
                    //console.log('full-error', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.setSendData = function () {
            var UserModel = angular.copy($scope.UserCredentials);
            UserModel.birthDate = $scope.ReSetDate(UserModel.birthDate);
            UserModel.immigrationDate = $scope.ReSetDate(UserModel.immigrationDate);
            return UserModel;
        }
        $scope.setUserData = function () {
            return {
                idNumber: $scope.UserCredentials.idNumber,
                password: $scope.UserCredentials.newPassword,
                initialPassword: $scope.UserCredentials.initialPassword,
                municipalities: $scope.UserCredentials.municipalities,
                email: $scope.UserCredentials.email,
                cell: $scope.UserCredentials.cell,
            }
        }
        $scope.setSmsUserData = function () {
            return {
                idNumber: $scope.UserCredentials.idNumber,
                initialPassword: $scope.UserCredentials.token,
            }
        }
        $scope.registerFromInitialPassword = function () {
            Credentials.RegisterFromInitialPassword($scope.setUserData())
                    .then(function successCallback(res) {
                        var data = res.data;
                        switch (data.exceptionId) {
                            case 0:
                                $scope.publish('RefreshLoginState', res.data.userCredentials);
                                break;

                            case 2://DuplicateIdNumber
                                toastrService.error('תעודת הזהות כבר קיימת במערכת!', 'תקלה!');
                                break;
                            case 3://DuplicateEmail
                                toastrService.error('האימייל כבר קיים במערכת!', 'תקלה!');
                                break;
                            case 4://InvalidPassword
                                toastrService.error('הסיסמה שסיפקת אינה תקינה!', 'תקלה!');
                                break;
                            case 5://InvalidEmail
                                toastrService.error('האימייל שסיפקת אינו תקין!', 'תקלה!');
                                break;
                            case 6://InvalidIDNumber
                                toastrService.error('תעודת הזהות שסיפקת אינה תקינה!', 'תקלה!');
                                break;

                            case 103:///"UserExistsException"
                                toastrService.error('המשתמש כבר קיים במערכת!', 'תקלה!');
                                break;
                            case 104:///"NullSystemUserException"
                                toastrService.error('לא נמצאו נתונים עבור הפרטים שהועברו!', 'תקלה!');
                                break;
                            case 105:///"InvalidIDNumber"
                                toastrService.error('תעודת הזהות שסיפקת אינה תקינה!', 'תקלה!');
                                break;
                            case 106:///"InvalidReturnDataException"
                                toastrService.error('לא נמצאו נתונים עבור הפרטים שהועברו!', 'תקלה!');
                                break;
                            case 108:///"initialPasswordException"
                                toastrService.error('סיסמת הרישום שסיפקת אינה נכונה!', 'תקלה!');
                                break;
                            default:
                                toastrService.error('', 'תקלת מערכת!');
                                return void [0];
                                break;
                        }

                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
        }
        $scope.RegisterFromSms = function () {
            Credentials.RegisterFromSms($scope.setUserData())
                    .then(function successCallback(res) {
                        var data = res.data;
                        switch (data.exceptionId) {
                            case 0:
                                $scope.UserCredentials.showSmsField = true;
                                break;

                            case 2://DuplicateIdNumber
                                toastrService.error('תעודת הזהות כבר קיימת במערכת!', 'תקלה!');
                                break;
                            case 3://DuplicateEmail
                                toastrService.error('האימייל כבר קיים במערכת!', 'תקלה!');
                                break;
                            case 4://InvalidPassword
                                toastrService.error('הסיסמה שסיפקת אינה תקינה!', 'תקלה!');
                                break;
                            case 5://InvalidEmail
                                toastrService.error('האימייל שסיפקת אינו תקין!', 'תקלה!');
                                break;
                            case 6://InvalidIDNumber
                                toastrService.error('תעודת הזהות שסיפקת אינה תקינה!', 'תקלה!');
                                break;

                            case 103:///"UserExistsException"
                                toastrService.error('המשתמש כבר קיים במערכת!', 'תקלה!');
                                break;
                            case 104:///"NullSystemUserException"
                                toastrService.error('לא נמצאו נתונים עבור הפרטים שהועברו!', 'תקלה!');
                                break;
                            case 105:///"InvalidIDNumber"
                                toastrService.error('תעודת הזהות שסיפקת אינה תקינה!', 'תקלה!');
                                break;
                            case 106:///"InvalidReturnDataException"
                                toastrService.error('לא נמצאו נתונים עבור הפרטים שהועברו!', 'תקלה!');
                                break;
                            case 109:///"EmailException"
                                toastrService.error('מספר הנייד שסיפקת אינו תואם!', 'תקלה!');
                                break;
                            default:
                                toastrService.error('', 'תקלת מערכת!');
                                return void [0];
                                break;
                        }

                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
        }
        $scope.RegisterFromEmail = function () {
            Credentials.RegisterFromEmail($scope.setUserData())
                    .then(function successCallback(res) {
                        var data = res.data;
                        switch (data.exceptionId) {
                            case 0:
                                toastrService.success('להשלמת תהליך ההרשמה אנא פנה למייל שנישלח לכתובת שסיפקת.', 'תודה על ההרשמה לאתר.');
                                break;

                            case 2://DuplicateIdNumber
                                toastrService.error('תעודת הזהות כבר קיימת במערכת!', 'תקלה!');
                                break;
                            case 3://DuplicateEmail
                                toastrService.error('האימייל כבר קיים במערכת!', 'תקלה!');
                                break;
                            case 4://InvalidPassword
                                toastrService.error('הסיסמה שסיפקת אינה תקינה!', 'תקלה!');
                                break;
                            case 5://InvalidEmail
                                toastrService.error('האימייל שסיפקת אינו תקין!', 'תקלה!');
                                break;
                            case 6://InvalidIDNumber
                                toastrService.error('תעודת הזהות שסיפקת אינה תקינה!', 'תקלה!');
                                break;

                            case 103:///"UserExistsException"
                                toastrService.error('המשתמש כבר קיים במערכת!', 'תקלה!');
                                break;
                            case 104:///"NullSystemUserException"
                                toastrService.error('לא נמצאו נתונים עבור הפרטים שהועברו!', 'תקלה!');
                                break;
                            case 105:///"InvalidIDNumber"
                                toastrService.error('תעודת הזהות שסיפקת אינה תקינה!', 'תקלה!');
                                break;
                            case 106:///"InvalidReturnDataException"
                                toastrService.error('לא נמצאו נתונים עבור הפרטים שהועברו!', 'תקלה!');
                                break;
                            case 110:///"EmailException"
                                toastrService.error('המייל שסיפקת אינו קיים במערכת!', 'תקלה!');
                                break;
                            default:
                                toastrService.error('', 'תקלת מערכת!');
                                return void [0];
                                break;
                        }

                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
        }
    }
})();
(function () {

    angular.module('WorkerPortfolioApp').controller('CustomPagesController', CustomPagesController);

    CustomPagesController.$inject = ['$scope', '$rootScope', '$Page'];

    function CustomPagesController($scope, $rootScope, $Page) {
        $scope.Page = $Page.data;
        $scope.Page.subject = $scope.Page.subject !== null && $scope.Page.subject.length > 1 ? $scope.Page.subject : $scope.Page.name;
    }
})();

(function () {
    
    angular.module('WorkerPortfolioApp').controller('ErrorController', ErrorController);

    ErrorController.$inject = ['$scope', '$rootScope', '$stateParams', 'SiteMail'];

    function ErrorController($scope, $rootScope, $stateParams, SiteMail) {

        $scope.$errorType = 0;
        if (typeof $stateParams.AuthenticationErrorToken !== 'undefined')
            $scope.$errorType = 1;
    }
})();

(function () {
    angular.module('WorkerPortfolioApp').controller('FamilyDetailsController', FamilyDetailsController);

    FamilyDetailsController.$inject = ['$scope', '$rootScope', '$state', 'TofesDataService', 'TofesModel', 'FileUploader', 'toastrService', 'Search'];

    function FamilyDetailsController($scope, $rootScope, $state, TofesDataService, TofesModel, FileUploader, toastrService, Search) {
        
        ///private variables
        $scope.childTouched = false;
        $scope.tofesModel = TofesModel;
        ///update from saved data if have        
        TofesDataService.GetExistingData($scope.tofesModel, '$$tofesModel', false);
        
        $scope.NewOptions = {
            nextText: 'הבא',
            prevText: 'הקודם',
            maxDate: new Date(),
        }

        $scope.partnetTitle = $scope.tofesModel.FormType === 3 ? 'ג' : 'ו';
        $scope.childrenTitle = $scope.tofesModel.FormType === 3 ? 'ד' : 'ג';

        $scope.decimalToHexString = function (number) {
            if (number < 0) {
                number = 0xFFFFFFFF + number + 1;
            }

            return number.toString(16).toUpperCase();
        }

        $scope.childId = -1;
        $scope.goToStep = '';
        ///get radio buttons data
        $scope.radionStates = TofesDataService.GetRadiosStates();
        ///get custom models for tofesModel lists
        $scope.customModel = TofesDataService.GetCustomModel();

        ///private function - copy child object
        $scope.copyChild = function () {
            var child = angular.copy($scope.customModel.childModel);
            child.ID = $scope.childId--;
            return child;
        }
        ///add/remove child from list
        $scope.addChild = function () {
            $scope.tofesModel.children.push($scope.copyChild());
            $rootScope.$$FamilyDetailsControllerData = $scope.tofesModel;
            $scope.addedChildCounter++;
        }
        $scope.removeChild = function ($id) {
            $scope.tofesModel.children = _.filter($scope.tofesModel.children, function (item) {
                return item.ID !== $id
            });
            $rootScope.$$FamilyDetailsControllerData = $scope.tofesModel;
            if ($id < 0)
                $scope.addedChildCounter--;
        }

        ///go-to next/prev form stap, published from Header directive
        $scope.subscribe('goto.step', function (goTo, direction) {
            if ($scope.validatUploders() || $scope.partForm.$invalid && direction) {
                $scope.ErrorFocus();
                return void [0];
            }
            
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            ///go to the step
            $state.go(goTo);
        });

        ///main submit form function
        $scope.nextForm = function () {
            ///form is invalid
            if ($scope.validatUploders() || $scope.partForm.$invalid || $scope.validatChildren()) {
                $scope.ErrorFocus();
                return void [0];
            }

            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            $scope.publish('$$states', 'current');

            return $scope.goToStep.isempty() ? (void [0]) : $state.go($scope.goToStep);
        }

        $scope.ErrorFocus = function () {
            if (typeof $scope.partForm.$error.required == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
            if (typeof $scope.partForm.$error.custom == 'object') {
                $scope.partForm.$error.custom[0].$$element.focus();
                return;
            }
        }

        $scope.HaveIdNumberFile = function () {
            var found = $scope.tofesModel.TofesControllerFilesList.find(function (file) {
                return file.filename.indexOf('idNumberFile') >= 0;
            });
            return (found != null && found != undefined);
        }

        $scope.addedChildCounter = 0;
        $.map($scope.tofesModel.children,function (c) {
            if (c.ID < 0)
                $scope.addedChildCounter++;
        });
        
        $scope.validatUploders = function () {
            $scope.partForm.idNumberFiles.$setValidity('required', true);
            if (!$scope.HaveIdNumberFile() && $scope.addedChildCounter > 0) {
                if ($scope.IdNumberuploader.queue.length < 1 || ($scope.IdNumberuploader.queue.length > 0 && (isNaN($scope.IdNumberuploader.progress) || $scope.IdNumberuploader.progress < 100))) {
                    $scope.partForm.idNumberFiles.$setValidity('required', false);
                    return true;
                }
            }

            for (var i in $scope) {
                if ($scope[i] instanceof FileUploader) {
                    ///SAVE UPLOADED FILES TO BE SENT
                    $rootScope.FilterFiles($scope.tofesModel.TofesControllerFilesList, $scope[i].queue, i);
                    $scope[i].Files = $scope.tofesModel.TofesControllerFilesList.filter(function (item) {
                        return item.origin === i;
                    }).length;

                    if ($scope[i].UploaderValidation())
                        return true;
                }
            }
            return false;
        }

        $scope.validatChildren = function () {
            $scope.childTouched = false;
            var list = $.map($scope.partForm, function (control, name) {
                if (name.indexOf('child_') == 0 && (!control.$pristine && !control.$untouched)) {
                    return true;
                }
            });
            if (list && list.length > 0 && !$scope.HaveIdNumberFile())
                $scope.childTouched = true;
            return list && list.length > 0 && !$scope.HaveIdNumberFile();
        }

        ///$scope.IdNumberuploader file uploader & filters
        $scope.IdNumberuploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'idNumberFile',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.IdNumberuploader.filters.splice(1, 1);
        $scope.IdNumberuploader.filters.push($scope.queueLimitFilter);
        $scope.IdNumberuploader.filters.push($scope.uniqueKeyFilter);
        $scope.IdNumberuploader.filters.push($scope.sizeFilter);
        $scope.IdNumberuploader.filters.push($scope.typeFilter);
        $scope.IdNumberuploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.IdNumberuploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.IdNumberuploader.onCompleteItem = $scope._onCompleteItem;
        $scope.IdNumberuploader.onCompleteAll = $scope._onCompleteAll;
        $scope.IdNumberuploader.removeFromQueue = $scope._removeFromQueue;
        $scope.IdNumberuploader.UploaderValidation = $scope._UploaderValidation;
        ///end $scope.IdNumberuploader file uploader & filters
    }
})();

(function () {
    angular.module('WorkerPortfolioApp').controller('FileManagerController', FileManagerController);

    FileManagerController.$inject = ['$scope', '$rootScope', 'ngDialog', '$timeout', 'toastrService', 'FileManager', 'FileUploader', 'fileStructure', 'options'];

    function FileManagerController($scope, $rootScope, ngDialog, $timeout, toastrService, FileManager, FileUploader, fileStructure, options) {
        /* ***controller vars*** */
        $scope.inputValidation = {
            required: true,
            minlength: 2,
            maxlength: 25
        }
        $scope.WSPostModel = $rootScope.getWSPostModel();
        $scope.options = options;
        $scope.fileStructure = fileStructure.data;
        $scope.fileStructure.selected = true;
        $scope.fileStructure.isSelected = true;
        $scope.directoryModel = {
            name: '',
            parentId: null,
            municipalityId: $scope.WSPostModel.currentMunicipalityId,
            subDirectories: [],
            images: []
        };
        ///selected file to work on
        $scope.isSelected = $scope.fileStructure;
        $scope.tempSelected = null;
        $scope.SelectedFile = null;
        $scope.tempFile = null;
        $scope.CurrentDragOver = null;
        ///hold list of selected folder images
        $scope.filesList = $scope.fileStructure.images;
        /* ***controller vars*** */

        /* ***folder functions*** */
        ///manage the folder selection
        $scope.selectMaster = function (mf) {
            mf.selected = true;
            mf.isSelected = true;
            mf.Editing = false;
            mf.Eding = false;

            $scope.isSelected = mf;

            $scope.traverseFolders($scope.fileStructure, mf.ID);
            $scope.CurrentDragOver = null;
            $scope.iteratDropOver(0);

            $scope.filesList = mf.selected ? mf.images : null;
            $scope.parseImages();
        }
        $scope.subscribe('UpdateSelectedFolder', function (sF) {
            sF.Editing = false;
            sF.Eding = false;
            $scope.isSelected = sF;

            $scope.traverseFolders($scope.fileStructure, sF.ID);
            $scope.CurrentDragOver = null;
            $scope.iteratDropOver(0);

            $scope.filesList = sF.selected ? sF.images : null;
            $scope.parseImages();
        });

        ///delete folder AND subfolders AND files
        $scope.deleteFolder = function (selected) {
            if (selected == null || selected === $scope.fileStructure)
                return void [0];

            ngDialog.openConfirm({
                template: '\
                <p><b>האם למחוק את</b> "' + selected.name + '"?<\p>\
                <p><b>שים לב</b>, מחיקת תיקייה תמחוק את כל תיקיות המשנה והקבצים!</p>\
                <div class="ngdialog-buttons">\
                    <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">בטל</button>\
                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(' + selected.ID + ')">מחק</button>\
                </div>',
                plain: true,

                scope: $scope,
            }).then(function (success) {
                var selectedFolder = $scope.getSingleSelectedFolderById(success);
                FileManager.DeleteFolder({ wsModel: $scope.WSPostModel, directory: selectedFolder })
                    .then(function successCallback(res) {
                        $scope.fileStructure = res.data;
                        $scope.filesList = $scope.fileStructure.images;
                        $scope.SetSelected(selectedFolder.parentId);

                        toastrService.success('התיקיה נמחקה!', '');

                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });

            }, function (error) {
                ///do nothing, user canceld
            });

        }

        ///add new folder
        $scope.addFolder = function (selected) {
            if (selected == null)
                return void [0];

            selected.Eding = true;
            $scope.directoryModel.parentId = selected.ID;
            $scope.directoryModel.name = null;

            $timeout(function () {
                $('[name=directory_' + selected.ID + ']').focus();
            }, 15);
        }
        $scope.addFolderOut = function (folder) {
            var inp = $scope.FileManagerForm['directory_' + folder.ID];
            if (folder.Eding !== true || inp.$invalid || !inp.$valid) {
                $scope.SetSelected(folder.ID);
                return void [0];
            }

            $scope.directoryModel.name = $scope.directoryModel.name === undefined || $scope.directoryModel.name === null || $scope.directoryModel.name.length === 0 ? 'תיקייה חדשה' : $scope.directoryModel.name;

            FileManager.AddFolder({ wsModel: $scope.WSPostModel, directory: $scope.directoryModel })
                .then(function successCallback(res) {
                    $scope.fileStructure = res.data;
                    $scope.filesList = $scope.fileStructure.images;
                    $scope.SetSelected(folder.ID);

                    toastrService.success('הוספה תיקיה חדשה!', '');

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.subscribe('AddNewFolder', function (folder) {
            if (folder.Eding == undefined || folder.Eding !== true)
                return void [0];

            $scope.directoryModel.name = folder.newFolderName;
            $scope.directoryModel.parentId = folder.ID;
            $scope.addFolderOut(folder);
        });

        ///update folder name/parent
        $scope.updateFolder = function (selected) {
            if (selected == null)
                return void [0];

            selected.Editing = true;
            $timeout(function () {
                $('[name=folder_' + selected.ID + ']').focus();
            }, 15);

            $scope.tempSelected = angular.copy(selected);
        }
        $scope.updateFolderOut = function (folder) {
            var inp = $scope.FileManagerForm['folder_' + folder.ID];
            if (folder.Editing !== true || inp.$invalid || !inp.$valid) {
                folder.name = $scope.tempSelected !== undefined && $scope.tempSelected !== null ? $scope.tempSelected.name : folder.name;
                folder.parentId = $scope.tempSelected !== undefined && $scope.tempSelected !== null ? $scope.tempSelected.parentId : folder.parentId;
                $scope.tempSelected = null;
                $scope.SetSelected(folder.ID);
                return void [0];
            }

            FileManager.UpdateFolder({ wsModel: $scope.WSPostModel, directory: folder })
                .then(function successCallback(res) {
                    $scope.fileStructure = res.data;
                    $scope.filesList = $scope.fileStructure.images;
                    $scope.SetSelected(folder.ID);

                    toastrService.success('התיקיה התעדכנה!', '');

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });

        }
        $scope.subscribe('UpdateFolderName', function (folder) {
            if (folder.Editing == undefined || folder.Editing !== true)
                return void [0];

            $scope.updateFolderOut(folder);
        });
        /* ***folder functions*** */

        /* ***image functions*** */
        ///set up img src
        $scope.parseImages = function () {
            if ($scope.filesList == null || $scope.filesList.length < 1)
                return void [0];

            $scope.filesList.filter(function (f) {
                if (f.fileBlobString.indexOf('data:image') < 0)
                    f.fileBlobString = 'data:image/png;base64,' + f.fileBlobString;
            });
        }
        
        ///click on selected img
        $scope.selectFile = function (file) {
            file.selected = !file.selected;
            $scope.SelectedFile = file.selected ? file : null;
            $scope.filesList.filter(function (f) {
                if (f !== file)
                    f.selected = false;
            });
        }
        ///select img and close popup
        $scope.insertFile = function (file) {
            file.selected = true;
            $scope.SelectedFile = file;
            $("#" + $scope.options.field_name).val("/Home/displayImage/" + file.ID);
            ngDialog.close();
        }

        ///etit image
        $scope.aditImage = function (folder) {
            if (folder == null || $scope.SelectedFile == null || !$scope.SelectedFile.selected)
                return void [0];

            $scope.SelectedFile.Editing = true;
            $scope.tempFile = angular.copy($scope.SelectedFile);
            $timeout(function () {
                $('[name=file_' + $scope.SelectedFile.ID + ']').focus();
            }, 15);
        }
        $scope.EditFileOut = function (file) {
            if (file == null || !file.selected || !file.Editing || $scope.SelectedFile === null)
                return void [0];
            var inp = $scope.FileManagerForm['file_' + file.ID];
            if (inp.$invalid || !inp.$valid) {
                file.name = $scope.tempFile.name;
                file.directoryId = $scope.tempFile.directoryId;
                $scope.tempFile = null;

                $scope.selectFile(file);
                return void [0];
            }

            FileManager.UpdateImage({ wsModel: $scope.WSPostModel, image: file })
                .then(function successCallback(res) {
                    res.data.fileBlobString = 'data:image/png;base64,' + res.data.fileBlobString;
                    $scope.isSelected.images[$scope.isSelected.images.indexOf(file)] = res.data;

                    toastrService.success('התמונה התעדכנה!', '');

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        ///delete image
        $scope.deleteImage = function (folder) {
            if (folder == null || $scope.SelectedFile == null)
                return void [0];

            FileManager.DeleteImage({ idNumber: $scope.WSPostModel.idNumber, imageId: $scope.SelectedFile.ID })
                .then(function successCallback(res) {
                    folder.images.splice(folder.images.indexOf($scope.SelectedFile), 1);

                    toastrService.success('התמונה נמחקה!', '');

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });

        }
        ///add new image to selected folder
        $scope.addImage = function (folder) {
            if (folder == null)
                return void [0];

            $('[name=_imagesUploader]').trigger('click');
        }
        ///new image uploader & filters
        $scope.imagesUploader = new FileUploader({
            url: 'filemanager/PostFile',
            queueLimit: 1,
            removeAfterUpload: true,
            //maxFileSize: 3,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.imagesUploader.filters.splice(1, 1);
        $scope.imagesUploader.filters.push($scope.queueLimitFilter);
        $scope.imagesUploader.filters.push($scope.sizeFilter);
        $scope.imagesUploader.filters.push($scope.imagesFilter);
        $scope.imagesUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.imagesUploader.onAfterAddingFile = function (fileItem) {
            fileItem.formData = [{ directoryId: $scope.isSelected.ID }, { userIDNumber: FileManager.encrypt($scope.WSPostModel.idNumber) }];
        }
        $scope.imagesUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.imagesUploader.onSuccessItem = function (item, response, status, headers) {
            var selectedFolder = $scope.getSingleSelectedFolderById($scope.isSelected.ID);
            selectedFolder.images.push(response);
            $scope.SetSelected($scope.isSelected.ID);
        }
        /* ***image functions*** */

        /* ***folder helper functions*** */
        ///re-set the selected folder after update/add/delete
        $scope.getSingleSelectedFolderById = function (folderId) {
            var selectedFolder = $scope.getSelectedFolderById(folderId);
            if (selectedFolder instanceof Array) {
                return selectedFolder[0];
            }
            return selectedFolder;
        }
        $scope.getSelectedFolderById = function (folderId, folder, _folder) {
            if (_folder !== undefined && _folder !== null) {
                return _folder;
            }

            if ($scope.fileStructure.ID === folderId)
                return $scope.getSelectedFolderById(null, null, $scope.fileStructure);

            if (folder !== undefined && folder.ID === folderId)
                return $scope.getSelectedFolderById(null, null, folder);

            return $.map(folder !== undefined ? folder.subDirectories : $scope.fileStructure.subDirectories, function (file) {
                if (file.ID !== folderId) {
                    return $scope.getSelectedFolderById(folderId, file);
                }
                if (file.ID === folderId) {
                    return $scope.getSelectedFolderById(null, null, file);
                }
            });
        }
        $scope.SetSelected = function (folderId) {
            $scope.iteratDropOver(0);

            var selectedFolder = $scope.getSingleSelectedFolderById(folderId);
            selectedFolder.selected = true;
            selectedFolder.isSelected = true;
            $scope.publish('UpdateSelectedFolder', selectedFolder);
        }
        $scope.traverseFolders = function (folder, sFID) {
            if (folder.ID != sFID) {
                folder.selected = false;
                folder.isSelected = false;
                folder.Editing = false;
                folder.Eding = false;
            }
            folder.subDirectories.filter(function (f) {
                if (f.ID == sFID) {
                    $scope.iteratParents(f.parentId);
                }

                if (f.ID != sFID) {
                    f.selected = false;
                    f.isSelected = false;
                    f.Editing = false;
                    f.Eding = false;
                }
                $scope.traverseFolders(f, sFID);
            });
        }
        $scope.iteratParents = function (parentId, folder) {
            if (parentId == undefined || parentId == null) {
                return;
            }
            if (folder == undefined) {///first loop!
                $scope.fileStructure.subDirectories.filter(function (f) {
                    if (f.ID == parentId) {
                        f.selected = true;
                    } else {
                        $scope.iteratParents(parentId, f);
                    }
                });
            }
            else {///inner loops
                folder.subDirectories.filter(function (f) {
                    if (f.ID == parentId) {
                        f.selected = true;
                        $scope.iteratParents(f.parentId);
                    } else {
                        $scope.iteratParents(parentId, f);
                    }
                });
            }
        }
        /* ***folder helper functions*** */

        /* ***drag and drop functions*** */
        ///open folder structure on drag over
        $scope.iteratDropOver = function (parentId, folder) {
            if (parentId == undefined || parentId == null) {
                return;
            }
            if (folder == undefined) {///first loop!
                $scope.fileStructure.subDirectories.filter(function (f) {
                    if (f.ID == parentId) {
                        f.dragOver = true;
                    } else {
                        f.dragOver = false;
                        $scope.iteratDropOver(parentId, f);
                    }
                });
            }
            else {///inner loops
                folder.subDirectories.filter(function (f) {
                    if (f.ID == parentId) {
                        f.dragOver = true;
                        $scope.iteratDropOver(f.parentId);
                    } else {
                        f.dragOver = false;
                        $scope.iteratDropOver(parentId, f);
                    }
                });
            }
        }

        ///drag over functions
        $scope.subscribe('dragOver', function ($dropElement, dropData) {
            var parents = $($dropElement.el).parents('ul:first');
            if ($dropElement.el.prop('nodeName') !== 'H5' && (parents.length !== 1 || parents.hasClass('deep-file')))
                return void [0];

            $scope.iteratDropOver(dropData.ID);
            dropData.dragOver = true;
            $timeout(function () {
                $scope.CurrentDragOver = dropData.ID;
            }, 5);
        });
        $scope.OnDragOver = function (data, $dragElement, $dropElement, event, dropData) {
            $scope.publish('dragOver', $dropElement, dropData);
        }
        ///drag leav functions
        $scope.subscribe('dragLeave', function (file, folder) {
            $scope.CurrentDragOver = null;
        });
        $scope.OnDragLeave = function (data, $dragElement, $dropElement, event, dropData) {
            $scope.publish('dragLeave', data, dropData);
        };
        ///file drop functions
        $scope.OnDropFile = function (data, $dragElement, $dropElement, event, dropData) {
            $scope.publish('FileDroped', data, dropData);
        };
        $scope.subscribe('FileDroped', function (file, folder) {
            switch (file.dataType) {
                case 'file':
                    $scope.FileDroped(file, folder);
                    break;
                case 'folder':
                    $scope.FolderDroped(file, folder);
                    break;
                default:
                    $scope.iteratDropOver(0);
                    break;
            }

        });
        ///when directory(folder) is droped
        $scope.FolderDroped = function (folder, Directory) {
            if ($scope.CurrentDragOver == null || folder.dataType !== 'folder' || Directory.ID !== $scope.CurrentDragOver || $scope.CurrentDragOver === folder.parentId) {
                $scope.iteratDropOver(0);
                return void [0];
            }
            
            var parentDirectory = $scope.getSingleSelectedFolderById(folder.parentId);

            parentDirectory.subDirectories.splice(parentDirectory.subDirectories.indexOf(folder), 1);
            Directory.subDirectories.push(folder);
            folder.parentId = Directory.ID;
            
            FileManager.UpdateFolder({ wsModel: $scope.WSPostModel, directory: folder })
                .then(function successCallback(res) {
                    $scope.fileStructure = res.data;
                    $scope.SetSelected(folder.ID);

                    toastrService.success('התיקיה התעדכנה', '');

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
            

        }
        ///when file(image) is droped
        $scope.FileDroped = function (file, folder) {
            if ($scope.CurrentDragOver == null || file.dataType !== 'file') {
                $scope.iteratDropOver(0);
                return void [0];
            }
            var directory = $scope.getSingleSelectedFolderById(file.directoryId);
            folder.images.push(file);
            directory.images.splice(directory.images.indexOf(file), 1);
            $scope.SetSelected(directory.ID);
            file.directoryId = folder.ID;
            $scope.CurrentDragOver = null;

            FileManager.UpdateImage({ wsModel: $scope.WSPostModel, image: file })
                .then(function successCallback(res) {
                    $scope.filesList.filter(function (f) {
                        f.selected = false;
                    });

                    toastrService.success('התמונה התעדכנה', '');

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        /* ***drag and drop functions*** */

        /* ***start controller*** */
        $scope.parseImages();
        
    }
})();



(function () {

    angular.module('WorkerPortfolioApp').controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', '$rootScope', '$window', 'ngDialog', 'toastrService'];

    function HomeController($scope, $rootScope, $window, ngDialog, toastrService) {

        ///TODO=> remove after data fix
        /* *************************************************************************************************************************************************************** */
        $scope.municipalitiesList = [14009, 99964, 32600, 2405, 99208, 4428, 26300,
                    99375, 14000, 92720, 50831, 99957, 99170, 2404, 67800, 99166, 99316,
                    28400, 67000, 99156, 51020, 83570, 39000, 52610, 69800, 90541, 36700,
                    51015, 55599, 4340, 42620, 32630, 99165, 18600, 50099, 6242, 37300, 36800, 62999];
        $scope.removeByMunicipalities = function () {
            return !($scope.$$credentials.loggedIn && $scope.municipalitiesList.indexOf($scope.$$credentials.currentMunicipalityId) >= 0);
        }
        /* *************************************************************************************************************************************************************** */

        $scope.linkClick = function (evnt, uiSref) {
            ///open the popup only 4 mobile\tablet apps
            if ($window.innerWidth < 801) {
                evnt.preventDefault();
                evnt.stopPropagation();

                $scope.target = evnt.currentTarget;
                $scope.popTitle = $scope.target.firstChild.textContent;
                $scope.popText = $scope.target.lastChild.textContent;
                $scope.uiSref = $rootScope.$$credentials.loggedIn ? $scope.target.parentNode.attributes.href.nodeValue : '#';

                ngDialog.open({
                    template: '../../Scripts/Views/Ng-Templates/mobile.home-page-popup.html',
                    closeByNavigation: true,
                    closeByDocument: true,
                    closeByEscape: true,
                    scope: $scope,
                    backdrop: 'static',
                    showClose: true,
                    appendClassName: 'ngdialog-custom',
                    name: 'linkClickPopUp'
                });
            }
        }

        $scope.subscribe('VerifayLoginState', function (val) {
            $scope.removeByMunicipalities();

            $scope.$$popUpWindow = sessionStorage.getItem("$$popUpWindow");
            if ($scope.$$popUpWindow === '1')
                return void [0];

            if (val.loggedIn && ngDialog.getOpenDialogs().length === 0 && val.email !== null && val.email.length > 4) {
                sessionStorage.setItem("$$popUpWindow", '1');
                ngDialog.open({
                    template: '../../Scripts/Views/Ng-Templates/email.paycheck-template.html',
                    closeByNavigation: true,
                    closeByDocument: true,
                    closeByEscape: true,
                    backdrop: 'static',
                    width: ($window.innerWidth < 801 ? '85%' : '45%'),
                    showClose: true,
                    appendClassName: 'ngdialog-custom-email-paycheck',
                    name: 'EmailPaycheckClickPopUp',
                    controller: ['$scope', '$rootScope', 'PayCheckEmail', function ($scope, $rootScope, PayCheckEmail) {
                        $scope.iAgree = true;
                        $scope.WSPostModel = $rootScope.getWSPostModel();

                        $scope.AgreeToMail = function () {
                            var $$pdf = sessionStorage.getItem("$$pdf");
                            if (!$scope.iAgree) {
                                $scope.closeThisDialog('value');
                                return void [0];
                            }
                            $scope.WSPostModel.isMobileRequest = $scope.iAgree;
                            var height = {
                                outHeight: Math.round(($window.innerHeight / 100) * 60),
                                inHeight: 0
                            };
                            height.inHeight = height.outHeight > 180 ? (height.outHeight - 60) : 120;

                            if ($$pdf !== '1' && $window.innerWidth > 800) {
                                sessionStorage.setItem("$$pdf", '1');
                                ngDialog.open({
                                    template: '<h5 style="margin: 4px 0;text-align: center;">תקנות הגנת השכר</h3><div><iframe id="myIframe" src="/Content/PDFs/תלוש_במייל.pdf" height="' + height.inHeight + 'px" width="100%"></iframe></div>',
                                    plain: true,
                                    width: ($window.innerWidth < 801 ? '85%' : '45%'),
                                    height: height.outHeight + 'px',
                                    showClose: true,
                                    closeByNavigation: true,
                                    closeByDocument: true,
                                    closeByEscape: true,
                                });
                                return void [0];
                            }
                            
                            PayCheckEmail.UserSendPayCheckToEmail($scope.WSPostModel)
                                .then(function successCallback(res) {
                                    if (res.data) {
                                        $rootScope.$$credentials.email = '';
                                    }
                                    $scope.closeThisDialog('value');

                                    toastrService.success('', 'בקשתך להצטרף לקבלת תלוש במייל התקבלה בהצלחה!');

                                }, function errorCallback(res) {
                                    //console.log('errorCallback', res);
                                    toastrService.error('', 'תקלת מערכת!');
                                    $scope.closeThisDialog('value');
                                });

                        }
                    }]
                });
            }
        });

        $scope.publish('TransmitLoginState', null);
        $scope.removeByMunicipalities();
    }
})();

(function () {
    angular.module('WorkerPortfolioApp').controller('IncomeController', IncomeController);

    IncomeController.$inject = ['$scope', '$rootScope', '$state', 'TofesDataService', 'TofesModel', 'toastrService', 'Search'];

    function IncomeController($scope, $rootScope, $state, TofesDataService, TofesModel, toastrService, Search) {
        ///private variables
        ///get radio buttons data
        $scope.radionStates = TofesDataService.GetRadiosStates();
        $scope.tofesModel = TofesModel;
        ///update from saved data if have
        TofesDataService.GetExistingData($scope.tofesModel, '$$tofesModel', true);
        $scope.tofesModel.thisIncome.startDate = TofesDataService.GetStartWorkInYearDate($scope.tofesModel.employee.startWorkTime, $scope.tofesModel.year);
        
        ///go-to next/prev form stap, published from Header directive
        $scope.subscribe('goto.step', function (goTo, direction) {
            ///check income selection validity
            $scope.checkedValidity();
            $scope.otherIncomesValidity();
            ///form is invalid
            if ($scope.partForm.$invalid && direction) {
                $scope.ErrorFocus();
                return void [0];
            }
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            ///go to the step
            $state.go(goTo);
        });

        ///main submit form function
        $scope.nextForm = function () {
            ///check income selection validity
            $scope.checkedValidity();
            $scope.otherIncomesValidity();
            ///form is invalid
            if ($scope.partForm.$invalid) {
                $scope.ErrorFocus();
                return void [0];
            }

            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);

            $scope.publish('$$states', 'current');
            return $scope.goToStep.isempty() ? (void [0]) : $state.go($scope.goToStep);
        }

        $scope.checkedValidity = function () {
            $scope.partForm.incomeMultiple.$setValidity('custom', true);
            $scope.partForm.incomeMultiple.$setValidity('length', true);
            $scope.incomeMultiple = $.map($scope.tofesModel.thisIncome, function (value, item) {
                if (['monthSalary', 'anotherSalary', 'partialSalary', 'daySalary'].indexOf(item) > -1 && value === true) return item;
            });
            
            if($scope.incomeMultiple.length < 1)
                $scope.partForm.incomeMultiple.$setValidity('length', false);
            if ($scope.incomeMultiple.length > 1)
                $scope.partForm.incomeMultiple.$setValidity('custom', false);
        }

        $scope.otherIncomesValidity = function () {
            $scope.partForm.hidden_otherIncomesValidity.$setValidity('custom', true);
            if ($scope.tofesModel.otherIncomes.hasIncomes) {
                var otherIncomes = $.map($scope.tofesModel.otherIncomes, function (value, item) {
                    if (['monthSalary', 'anotherSalary', 'partialSalary', 'daySalary', 'allowance', 'stipend', 'anotherSource'].indexOf(item) > -1 && value === true) return item;
                });
                if(otherIncomes.length <= 0)
                    $scope.partForm.hidden_otherIncomesValidity.$setValidity('custom', false);
            }
        }

        $scope.ErrorFocus = function () {
            if (typeof $scope.partForm.$error.required == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
            if (typeof $scope.partForm.$error.custom == 'object') {
                $scope.partForm.$error.custom[0].$$element.focus();
                return;
            }
        }
    }
})();

(function () {
    angular.module('WorkerPortfolioApp').controller('MunicipalityPagesController', MunicipalityPagesController);

    MunicipalityPagesController.$inject = ['$scope', '$rootScope', '$state', 'toastrService', 'FileManager', 'ngDialog', 'ValidAdminUser', 'Templates'];

    function MunicipalityPagesController($scope, $rootScope, $state, toastrService, FileManager, ngDialog, ValidAdminUser, Templates) {

        if (ValidAdminUser.data !== true || ValidAdminUser.status !== 200) {
            $state.go('Home');
            toastrService.info('', 'הרשאה לא תקינה!', { onHidden: function () { $state.go('Home'); } });
            return void [0];
        }
        $scope.WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'MUNICIPALITYMENUROLE');

        $scope.HtmlTemplates = [];
        $scope.HtmlTemplateModel = {
            Editing: false,
            ID: null,
            municipalityId: $scope.WSPostModel.currentMunicipalityId,
            createdDate: new Date(),
            updateDate: new Date(),
            updateUserId: null,
            template: null,
            subject: null,
            name: null,
            fromEmail: null,
            municipality: null,
            user: null,
            type: 2,
            active: false,
        }
        ///pager
        $scope.$Pager = {
            PageSize: 15,
            PageNumber: 1,
            Counter: 0,
            MaxPages: 0,
            pagerList: [],
            sizes: [15, 30, 45]
        }
        $scope.changePagerSize = function () {
            $scope.setUpController($scope.HtmlTemplates);
        }

        $scope.getItem = function (list, num) {
            var items = list.filter(function (s) {
                return s.ID === num;
            });

            return (items instanceof Array) ? items[0] : items;
        }

        $scope.DeleteTemplate = function (tmplt) {

            ngDialog.openConfirm({
                template: '\
                <p><b>האם למחוק את</b> "' + tmplt.subject + '"?<\p>\
                <p><b>שים לב</b>, לא ניתן לשחזרת דף שנמחק!</p>\
                <div class="ngdialog-buttons">\
                    <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">בטל</button>\
                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(' + tmplt.ID + ')">מחק</button>\
                </div>',
                plain: true,

                scope: $scope,
            }).then(function (success) {
                
                FileManager.DeleteTemplate({ wsModel: $scope.WSPostModel, template: tmplt })
                    .then(function successCallback(res) {
                        if (res.data) {
                            $scope.HtmlTemplates.splice($scope.HtmlTemplates.indexOf(tmplt), 1);
                            $scope.setUpController($scope.HtmlTemplates);
                            if (tmplt.ID === $scope.HtmlTemplateModel.ID) {
                                $scope.createNewTemplate(false);
                            }

                            toastrService.success('התבנית נמחקה!', '');
                        } else {
                            toastrService.error('', 'תקלה!');
                        }                        
                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });

            }, function (error) {
                ///do nothing, user canceld
            });

        }
        $scope.SaveTemplate = function () {
            $scope.TemplateFrom.$setSubmitted();
            if ($scope.TemplateFrom.$invalid || !$scope.TemplateFrom.$valid)
                return void [0];

            $scope.HtmlTemplateModel.updateDate = new Date();

            FileManager.UpdateAddTemplate({ wsModel: $scope.WSPostModel, template: $scope.HtmlTemplateModel })
                    .then(function successCallback(res) {
                        var item = $scope.getItem($scope.HtmlTemplates, res.data.ID);
                         if (item === undefined) {
                             $scope.HtmlTemplates.push(res.data);
                         } else {
                             $scope.HtmlTemplates.splice($scope.HtmlTemplates.indexOf(item), 1);
                             $scope.HtmlTemplates.push(res.data);
                         }
                         $scope.setUpController($scope.HtmlTemplates);
                         $scope.EditTemplate(res.data);
                         toastrService.success('התבנית נשמרה!', '');

                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
        }
        $scope.EditTemplate = function (template) {
            $scope.HtmlTemplateModel = angular.copy(template);
            $scope.HtmlTemplateModel.Editing = true;
        }
        $scope.createNewTemplate = function (state) {
            $scope.HtmlTemplateModel = {
                Editing: state,
                ID: null,
                municipalityId: $scope.WSPostModel.currentMunicipalityId,
                createdDate: new Date(),
                updateDate: new Date(),
                updateUserId: null,
                template: null,
                subject: null,
                name: null,
                fromEmail: null,
                type: 2,
                active: false,
            }
        }
        $scope.activetTemplate = function (template) {
            template.active = !template.active;

            FileManager.UpdateAddTemplate({ wsModel: $scope.WSPostModel, template: template })
                    .then(function successCallback(res) {
                        if ($scope.HtmlTemplateModel.ID === template.ID)
                            $scope.EditTemplate(res.data);

                        toastrService.success('התבנית נשמרה!', '');

                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
        }

        $scope.file_browser_callbackFunction = function (field_name, url, type, win) {
            try {
                $('#mce-modal-block').attr('style', '');
                var style = $('.mce-container.mce-panel.mce-floatpanel.mce-window.mce-in').attr('style');
                style = style.split(';');
                style.filter(function (s) {
                    if (s.indexOf('z-index') >= 0) {
                        style[style.indexOf(s)] = 'z-index: 123';
                    }
                });
                $('.mce-container.mce-panel.mce-floatpanel.mce-window.mce-in').attr('style', style.join(';'));
            } catch (e) {
                //console.log(e);
            }

            ngDialog.open({
                template: '../../Scripts/views/Email-Templates/file-manager.html',
                closeByNavigation: true,
                closeByDocument: true,
                closeByEscape: true,
                scope: $scope,
                backdrop: 'static',
                showClose: true,
                appendClassName: 'file-manager-custom',
                name: 'fileManagerPopUp',
                resolve: {
                    fileStructure: ['FileManager', function (FileManager) {
                        return FileManager.GetFileStructure($scope.WSPostModel);
                    }],
                    options: [function () {
                        return { field_name: field_name, url: url, type: type, win: win };
                    }],
                },
                controller: 'FileManagerController',
            });
        }
        $scope.tinymceOptions = {
            selector: "textarea", theme: "modern", height: 400,
            plugins: [
                 "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                 "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                 "table contextmenu directionality emoticons paste textcolor code",
                 "directionality"
            ],
            toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | link unlink anchor | image media | forecolor backcolor  | ltr rtl",

            directionality: "rtl",

            file_browser_callback: $scope.file_browser_callbackFunction,
        };

        /* *** set up controller *** */
        $scope.setUpController = function (templates) {
            $scope.HtmlTemplates = templates;
            $scope.$Pager.PageNumber = 1;

            $scope.$Pager.Counter = templates.length;
            $scope.$Pager.MaxPages = Math.ceil(templates.length / $scope.$Pager.PageSize);
            var num = 1;

            $scope.$Pager.pagerList = [];
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                $scope.$Pager.pagerList.push(1 + i);
            }
        }
        $scope.setUpController(Templates.data);
    }
})();

(function () {
    angular.module('WorkerPortfolioApp').controller('MunicipalityStatisticsController', MunicipalityStatisticsController);

    MunicipalityStatisticsController.$inject = ['$scope', '$rootScope', '$window', '$timeout', 'toastrService', 'Statistics', 'ValidAdminUser', 'municipalities', 'statistics'];

    function MunicipalityStatisticsController($scope, $rootScope, $window, $timeout, toastrService, Statistics, ValidAdminUser, municipalities, statistics) {

        var _requiredRole = 'EMAILPAYCHECKROLE';

        if (ValidAdminUser.data !== true || ValidAdminUser.status !== 200) {
            $state.go('Home');
            toastrService.info('', 'הרשאה לא תקינה!', { onHidden: function () { $state.go('Home'); } });
            return void [0];
        }

        ///data set up functions
        var getYears = function (year) {
            var ya = [];
            for (var i = 2018; i <= year; i++) {
                ya.push(i);
            }
            $scope.selectedYear = year;
            return ya;
        }
        var SetUpPager = function (list) {
            $scope.$Pager.PageNumber = 1;
            $scope.$Pager.Counter = list.length
            $scope.$Pager.MaxPages = Math.ceil(list.length / $scope.$Pager.PageSize);
            $scope.$Pager.pagerList = [];
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                $scope.$Pager.pagerList.push(1 + i);
            }
        }
        var pad = function (val) {
            return val > 9 ? String(val) : ('0' + String(val));
        }
        var parseDateToString = function (date, toDate) {
            if (date === undefined || date === null)
                return null;
            date = new Date(date);
            if (toDate === true) {
                return date;
            }
            return pad(date.getDate()) + '/' + pad(date.getMonth() + 1) + '/' + date.getFullYear();
        }

        $scope.ParseStatisticsData = function (sd) {
            return $.map(sd, function (item, index) {
                item._TERMINATIONDATE = parseDateToString(item.TERMINATIONDATE);
                item.TERMINATIONDATE = parseDateToString(item.TERMINATIONDATE, true);
                
                item._ACTUALCREATIONDATE = parseDateToString(item.ACTUALCREATIONDATE);
                item.ACTUALCREATIONDATE = parseDateToString(item.ACTUALCREATIONDATE, true);

                item._CREATEDDATE = parseDateToString(item.CREATEDDATE);
                item.CREATEDDATE = parseDateToString(item.CREATEDDATE, true);

                item._DELIVERYSTATUS = item.DELIVERYSTATUS === 0 ? 'נשלח בהצלחה' : (item.DELIVERYSTATUS === null ? 'לא נשלח' : 'נכשל');
                return item;
            });
        }

        $scope.WSPostModel = $rootScope.getWSPostModel(true); $rootScope.getWSPostModel(null, null, null, null, 'EMAILPAYCHECKROLE')
        $scope.WSPostModel.isMobileRequest = $scope.isMobile;

        $scope.isMobile = ($window.innerWidth < 801);
        $scope.selectedMunicipality = undefined;
        $scope.selectedYear = new Date().getFullYear();
        $scope.yearsList = getYears($scope.selectedYear);
        $scope.STHolderList = $scope.ParseStatisticsData(statistics.data);
        $scope.STList = angular.copy($scope.STHolderList);
        ///pager
        $scope.$Pager = {
            PageSize: 25,
            PageNumber: 1,
            Counter: 0,
            MaxPages: 0,
            pagerList: []
        }

        $scope.MunicipalitiesList = $.grep(municipalities.data, function (municipality) {
            return municipality.active;
        });

        $scope.selectedMunicipalityCallback = function (Selected) {
            $scope.selectedMunicipality = Selected !== undefined ? (Selected.description !== undefined ? Selected.description.ID : Selected.originalObject.ID) : undefined;
            if (Selected !== undefined && Selected.description !== undefined) {
                $scope.STList = new Array();
                $scope.STHolderList = new Array();
                $scope.$Pager = {
                    PageSize: 25,
                    PageNumber: 1,
                    Counter: 0,
                    MaxPages: 0,
                    pagerList: []
                }
            }
        }

        $scope.InitialValue = $.grep($scope.MunicipalitiesList, function (item) {
            return item.ID === $scope.WSPostModel.currentMunicipalityId;
        })[0];
        $scope.disableInput = $scope.MunicipalitiesList.length < 2;
        ///UserSearch
        $scope.UserSearch = {
            idNumber: $scope.WSPostModel.idNumber,
            confirmationToken: $scope.WSPostModel.confirmationToken,
            page: 1,
            searchMunicipalitiesId: $scope.InitialValue.ID,
            requiredRole: _requiredRole,
            searchDate: new Date()
        };

        $scope.GetStatistics = function () {
            $scope.municipality_hidden_value = null;
            $scope.StatisticsForm.hidden_value.$setValidity('required', false);
            if ($scope.selectedMunicipality !== undefined) {
                $scope.municipality_hidden_value = 1;
                $scope.StatisticsForm.hidden_value.$setValidity('required', true);
            }

            if ($scope.StatisticsForm.$invalid)
                return void [0];

            $scope.UserSearch.page = $scope.selectedMunicipality;
            $scope.UserSearch.searchDate = new Date($scope.selectedYear, 0, 1);

            Statistics.MunicipalityStatistics($scope.UserSearch)
                .then(function successCallback(res) {
                    $scope.STHolderList = $scope.ParseStatisticsData(res.data);
                    $scope.STList = angular.copy($scope.STHolderList);
                    SetUpPager(res.data);
                }, function errorCallback(res) {
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.fileNameFunction = function () {
            var current = $.grep($scope.MunicipalitiesList, function (item) {
                return item.ID === $scope.selectedMunicipality;
            })[0];
            return current.name.replace(/[^א-ת0-9. -]/igm, '').replace(/[ -]/gi, '_').replace(/__/gim, '_').replace(/__/gim, '_') + '_' + String($scope.selectedYear) + '_' + String('.xlsx');
        }
        $scope.workSheetFunction = function () {
            return String('דוח 101 לשנת ') + String($scope.selectedYear);
        }

        $scope.Export = function () {
            var dataObj = [];
            $.map($scope.STList, function (item) {
                dataObj.push({
                    'תעודת זהות': item.IDNUMBER,
                    'שם מלא': item.USER_NAME,
                    'קוד סיום': item.TERMINATIONCAUSE,
                    'סיבת סיום': item.TERMINATIONREASON,
                    'תאריך סיום': item._TERMINATIONDATE,
                    'מצב 101': item._DELIVERYSTATUS,
                    'תאריך שליחה': item._ACTUALCREATIONDATE,
                    'גירסה': item.VERSION
                });
            });
            return dataObj;
        }


        ///Filter101Statistics
        $scope.Search = {
            IdNUmber: null,
            Status: null,
            name: null,
            tCause: 0,
            tDate: 0,
            SendDate: 0
        }
        $scope.Filter101Statistics = function () {
            $scope.STList = angular.copy($scope.STHolderList);
            var regex = null;
            ///filter idnumner
            if ($scope.Search.IdNUmber !== null && $scope.Search.IdNUmber.length > 0) {
                regex = RegExp($scope.Search.IdNUmber);
                $scope.STList = $.grep($scope.STList, function (item) {
                    return regex.test(item.IDNUMBER);
                });
            }
            ///filter user name
            if ($scope.Search.name !== null && $scope.Search.name.length > 0) {
                regex = RegExp($scope.Search.name);
                $scope.STList = $.grep($scope.STList, function (item) {
                    return regex.test(item.USER_NAME);
                });
            }
            ///filter DELIVERYSTATUS
            if (parseInt($scope.Search.Status) > 0) {
                switch (parseInt($scope.Search.Status)) {
                    case 1:
                        $scope.STList = $.grep($scope.STList, function (item) {
                            return item.DELIVERYSTATUS === null;
                        });
                        break;
                    case 2:
                        $scope.STList = $.grep($scope.STList, function (item) {
                            return item.DELIVERYSTATUS !== null && item.DELIVERYSTATUS !== 0;
                        });
                        break;
                    case 3:
                        $scope.STList = $.grep($scope.STList, function (item) {
                            return item.DELIVERYSTATUS === 0;
                        });
                        break;
                }
            }

            ///order by TERMINATIONCAUSE
            if ($scope.Search.tCause > 0) {
                switch ($scope.Search.tCause) {
                    case 1:///desc
                        $scope.STList.sort(function (a, b) {
                            return a.TERMINATIONCAUSE < b.TERMINATIONCAUSE ? 1 : a.TERMINATIONCAUSE > b.TERMINATIONCAUSE ? -1 : 0
                        });
                        break;
                    case 2:///asc
                        $scope.STList.sort(function (a, b) {
                            return a.TERMINATIONCAUSE > b.TERMINATIONCAUSE ? 1 : a.TERMINATIONCAUSE < b.TERMINATIONCAUSE ? -1 : 0
                        });
                        break;
                }
            }
            ///order by TERMINATIONDATE
            if ($scope.Search.tDate > 0) {
                switch ($scope.Search.tDate) {
                    case 1:///desc
                        $scope.STList.sort(function (a, b) {
                            return a.TERMINATIONDATE < b.TERMINATIONDATE ? 1 : a.TERMINATIONDATE > b.TERMINATIONDATE ? -1 : 0
                        });
                        break;
                    case 2:///asc
                        $scope.STList.sort(function (a, b) {
                            return a.TERMINATIONDATE > b.TERMINATIONDATE ? 1 : a.TERMINATIONDATE < b.TERMINATIONDATE ? -1 : 0
                        });
                        break;
                }
            }
            ///order by ACTUALCREATIONDATE
            if ($scope.Search.SendDate > 0) {
                switch ($scope.Search.SendDate) {
                    case 1:///desc
                        $scope.STList.sort(function (a, b) {
                            return a.ACTUALCREATIONDATE < b.ACTUALCREATIONDATE ? 1 : a.ACTUALCREATIONDATE > b.ACTUALCREATIONDATE ? -1 : 0
                        });
                        break;
                    case 2:///asc
                        $scope.STList.sort(function (a, b) {
                            return a.ACTUALCREATIONDATE > b.ACTUALCREATIONDATE ? 1 : a.ACTUALCREATIONDATE < b.ACTUALCREATIONDATE ? -1 : 0
                        });
                        break;
                }
            }

            SetUpPager($scope.STList);
        }
        $scope.selectTerminationCause = function () {
            $scope.reSetOtherVariables('tCause');
            switch ($scope.Search.tCause) {
                case 0:
                    $scope.Search.tCause = 1;
                    break;
                case 1:
                    $scope.Search.tCause = 2;
                    break;
                case 2:
                    $scope.Search.tCause = 0;
                    break;
                default:
                    $scope.Search.tCause = 0;
            }
            $scope.Filter101Statistics();
        }
        $scope.selectTerminationDate = function () {
            $scope.reSetOtherVariables('tDate');
            switch ($scope.Search.tDate) {
                case 0:
                    $scope.Search.tDate = 1;
                    break;
                case 1:
                    $scope.Search.tDate = 2;
                    break;
                case 2:
                    $scope.Search.tDate = 0;
                    break;
                default:
                    $scope.Search.tDate = 0;
            }
            $scope.Filter101Statistics();
        }
        $scope.selectDeliveryStatus = function () {
            $scope.reSetOtherVariables('Status');
            switch ($scope.Search.Status) {
                case 0:
                    $scope.Search.Status = 1;
                    break;
                case 1:
                    $scope.Search.Status = 2;
                    break;
                case 2:
                    $scope.Search.Status = 0;
                    break;
                default:
                    $scope.Search.Status = 0;
            }
            $scope.Filter101Statistics();
        }
        $scope.selectSendDate = function () {
            $scope.reSetOtherVariables('SendDate');
            switch ($scope.Search.SendDate) {
                case 0:
                    $scope.Search.SendDate = 1;
                    break;
                case 1:
                    $scope.Search.SendDate = 2;
                    break;
                case 2:
                    $scope.Search.SendDate = 0;
                    break;
                default:
                    $scope.Search.SendDate = 0;
            }
            $scope.Filter101Statistics();
        }

        $scope.reSetOtherVariables = function (Me) {
            switch (Me) {
                case 'tCause':
                    $scope.Search.tDate = 0;
                    $scope.Search.SendDate = 0;
                    break;
                case 'tDate':
                    $scope.Search.tCause = 0;
                    $scope.Search.SendDate = 0;
                    break;
                case 'SendDate':
                    $scope.Search.tCause = 0;
                    $scope.Search.tDate = 0;
                    break;

                default:
                    $scope.Search.tCause = 0;
                    $scope.Search.tDate = 0;
                    $scope.Search.SendDate = 0;
                    break;
            }
        }

        ///set up functions
        SetUpPager(statistics.data);
    }
})();
(function () {
    angular.module('WorkerPortfolioApp').controller('OtherIncomeAdjustmentController', OtherIncomeAdjustmentController);

    OtherIncomeAdjustmentController.$inject = ['$scope', '$rootScope', '$state', 'FileUploader', 'TofesDataService', 'TofesModel', 'toastrService', 'Search'];

    function OtherIncomeAdjustmentController($scope, $rootScope, $state, FileUploader, TofesDataService, TofesModel, toastrService, Search) {

        ///private variables
        ///get radio buttons data
        $scope.radionStates = TofesDataService.GetRadiosStates();
        $scope.tofesModel = TofesModel;
        ///update from saved data if have
        TofesDataService.GetExistingData($scope.tofesModel, '$$tofesModel', true);
        
        ///go-to next/prev form stap, published from Header directive
        $scope.subscribe('goto.step', function (goTo, direction) {
            ///check income selection validity
            $scope.otherIncomesValidity();
            
            ///form is invalid
            if ($scope.validatUploders() || $scope.partForm.$invalid && direction) {
                $scope.ErrorFocus();
                return void [0];
            }
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);

            ///go to the step
            $state.go(goTo);
        });

        ///main submit form function
        $scope.nextForm = function () {
            ///check income selection validity
            $scope.otherIncomesValidity();

            ///form is invalid
            if ($scope.partForm.$invalid) {
                $scope.ErrorFocus();
                return void [0];
            }
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);

            $scope.publish('$$states', 'current');
            return $scope.goToStep.isempty() ? (void [0]) : $state.go($scope.goToStep);
        }
        $scope.ChengeRequestRule = function () {
            if (!$scope.tofesModel.taxCoordination.request) {
                $scope.tofesModel.taxCoordination.reason = null;
            } else {
                $scope.tofesModel.taxCoordination.reason = 3;
            }
        }

        $scope.coordinationUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'coordinationFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.coordinationUploader.filters.splice(1, 1);
        $scope.coordinationUploader.filters.push($scope.queueLimitFilter);
        $scope.coordinationUploader.filters.push($scope.uniqueKeyFilter);
        $scope.coordinationUploader.filters.push($scope.sizeFilter);
        $scope.coordinationUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.coordinationUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.coordinationUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.coordinationUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.coordinationUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.coordinationUploader.UploaderValidation = $scope._UploaderValidation;
        ///END FILE UPLOADERS
        
        $scope.otherIncomesValidity = function () {
            $scope.partForm.hidden_otherIncomesValidity.$setValidity('custom', true);
            if ($scope.tofesModel.otherIncomes.hasIncomes) {
                var otherIncomes = $.map($scope.tofesModel.otherIncomes, function (value, item) {
                    if (['monthSalary', 'anotherSalary', 'partialSalary', 'daySalary', 'allowance', 'stipend', 'anotherSource'].indexOf(item) > -1 && value === true) return item;
                });
                if (otherIncomes.length <= 0)
                    $scope.partForm.hidden_otherIncomesValidity.$setValidity('custom', false);
            }
        }
        ///validate all the uploaders in the scope
        $scope.validatUploders = function () {
            for (var i in $scope) {
                if ($scope[i] instanceof FileUploader) {
                    ///SAVE UPLOADED FILES TO BE SENT
                    $rootScope.FilterFiles($scope.tofesModel.TaxAdjustmentControllerFilesList, $scope[i].queue, i);
                    $scope[i].Files = $scope.tofesModel.TaxAdjustmentControllerFilesList.filter(function (item) {
                        return item.origin === i;
                    }).length;

                    if ($scope[i].UploaderValidation()) {
                        return true;
                    }
                }
            }
            return false;
        }
        $scope.ErrorFocus = function () {
            if (typeof $scope.partForm.$error.required == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
            if (typeof $scope.partForm.$error.custom == 'object') {
                $scope.partForm.$error.custom[0].$$element.focus();
                return;
            }
        }
        
    }
})();

(function () {    

    angular.module('WorkerPortfolioApp').controller('PayrollController', PayrollController);

    PayrollController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$location', '$interval', '$window', '$sce', 'PayrollServise', 'toastrService', '$controllerState', 'PayrollData', '$timeout'];

    function PayrollController($scope, $rootScope, $state, $stateParams, $location, $interval, $window, $sce, PayrollServise, toastrService, $controllerState, PayrollData, $timeout) {

        $scope.isMobile = ($window.innerWidth < 801);
        ///local functions
        var parseMonth = function (date) {
            function pad(v) {return v < 10? '0'+v : v }
            if (!(date instanceof Date) || date == 'Invalid Date')
                date = new Date();
            return [pad(date.getMonth() + 1), date.getFullYear()].join('/');
        }
        var b64toBlob = function (b64Data, contentType) {
            contentType = contentType || '';
            var sliceSize = 512;
            b64Data = b64Data.replace(/^[^,]+,/, '');
            b64Data = b64Data.replace(/\s/g, '');
            var byteCharacters = window.atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }
        var setStartMonthSelect = function (formatDate) {
            var date = $rootScope.getMonthDate();
            return formatDate? date : parseMonth(date);
        }
 
        ///local parameters
        $scope.PayrollData = PayrollData || {};
        $scope.$controllerState = $controllerState;
        $scope.credentials = typeof $rootScope.$$credentials !== 'undefined' ? $rootScope.$$credentials : $scope.lsGet('$$credentials');
        $scope.pageTitle = null;
        $scope.pageSubTitle = null;
        $scope.WSPostModel = $rootScope.getWSPostModel(true);
        $scope.WSPostModel.isMobileRequest = $scope.isMobile;

        $scope.pdfContent = '';
        $scope.$pdfContent = false;
        $scope.selectedMonth = $scope.WSPostModel.selectedDate;
        $scope.selectedMonth.setDate($scope.selectedMonth.getMonth() + 1);

        $scope.currentYear = $scope.WSPostModel.selectedDate.getFullYear();
        
        switch ($controllerState) {
            case 'Paycheck':
                $scope.pageTitle = 'תלוש שכר';
                $scope.pdfContent = '';
                $scope.$pdfContent = false;
                $scope.formMsg = '';
                $scope.pdfDataStatus = PayrollData.status;
                if (PayrollData.status === 406) {
                    $scope.formMsg = 'אין תלוש שכר לתאריך CURRENTDATE'.replace('CURRENTDATE', parseMonth($scope.WSPostModel.selectedDate));
                } else {
                    try {
                        var file = b64toBlob(PayrollData.data.fileString, 'application/pdf');
                        var fileURL = window.URL.createObjectURL(file);
                        $scope.pdfContent = $sce.trustAsResourceUrl(fileURL);
                    } catch (e) {
                        //console.log(e);
                    }

                    $scope.$pdfContent = true;
                    $scope.formMsg = 'תלוש שכר לתאריך CURRENTDATE'.replace('CURRENTDATE', parseMonth($scope.WSPostModel.selectedDate));
                    $scope.fileimageblob = PayrollData.data.fileimageblob;
                }
                
                $timeout(function () {

                    $('#Month').MonthPicker({
                        MaxMonth: 0, IsRTL: true, UseInputMask: true, ShowOn: 'both', buttonText: 'בחר חודש',
                        OnAfterChooseMonth: function (selectedDate) {
                            // Do something with selected JavaScript date.
                            selectedDate.setDate(15);
                            $scope.WSPostModel.selectedDate = selectedDate;
                            $scope.showPaycheckPDF();
                        }
                    });

                }, 55);

                break;
            case 'PayrollBySymbol':
            case 'GeneralWagesData':
                $scope.pageTitle = $controllerState === 'PayrollBySymbol' ? 'פירוט נתוני שכר נבחרים בהתפלגות שנתית' : 'פירוט נתוני שכר כלליים בהתפלגות שנתית';
                $scope.pageSubTitle = $controllerState === 'PayrollBySymbol' ? "המערכת מקבצת את התשלומים / ניכויים הרלוונטיים (ההלוואות, שעות נוספות וכו'..). " : null;
                ///sort the lists
                $scope.PayrollData = {
                        Titles: [],
                        Data:[],
                    };
                $scope.PayrollData.Titles = [];
                $.map(PayrollData.data.Titles, function (item) {
                    $scope.PayrollData.Titles[item.number] = item;
                });

                PayrollData.data.Data.filter(function (row) {
                    var _row = [];
                    $.map(row, function (item) {
                        _row[item.columnNumber] = item;
                    });
                    $scope.PayrollData.Data.push(_row);
                });
                break;
            case 'TrainingAndProvidentFunds':
                $scope.pageTitle = 'פירוט נתוני שכר נבחרים בהתפלגות שנתית - גמל והשתלמות';
                $scope.pageSubTitle = 'המערכת מציגה את נתוני הניכויים והפרשות לכלל קופות הגמל שהתבצעו בשנה הנתונה';
                
                $scope.PayrollData = PayrollData.data;                
                break;
            case '106Form':
                if ($scope.credentials.currentTerminationDate === null)
                    $scope.currentYear--;
                $scope.pageTitle = 'טופס 106';
                $scope.pdfContent = '';
                $scope.$pdfContent = false;
                $scope.formMsg = '';
                $scope.pdfDataStatus = PayrollData.status;
                $scope.WSPostModel.selectedDate = new Date($scope.currentYear, 11, 11);

                if (PayrollData.status === 406) {
                    $scope.formMsg = 'אין טופס 106 לשנת YEAR.'.replace('YEAR', $scope.currentYear);
                } else {
                    try {
                        var file = b64toBlob(PayrollData.data.fileString, 'application/pdf');
                        var fileURL = URL.createObjectURL(file);
                        $scope.pdfContent = $sce.trustAsResourceUrl(fileURL);
                    } catch (e) {
                        //console.log(e);
                    }

                    $scope.$pdfContent = true;
                    $scope.formMsg = 'טופס 106 לשנת YEAR.'.replace('YEAR', $scope.currentYear);
                    $scope.fileimageblob = PayrollData.data.fileimageblob;
                }
                break;

            default:
                $state.go('Home');
                break;
        }

        $scope.changeYear = function () {
            $scope.WSPostModel.selectedDate = new Date($scope.currentYear, 3, 3);
            switch ($scope.$controllerState) {
                case 'GeneralWagesData':
                    PayrollServise.GeneralPayrollData($scope.WSPostModel)
                        .then(function successCallback(res) {
                            $scope.PayrollData = {
                                Titles: [],
                                Data: [],
                            };
                            $scope.PayrollData.Titles = [];
                            $.map(res.data.Titles, function (item) {
                                $scope.PayrollData.Titles[item.number] = item;
                            });

                            res.data.Data.filter(function (row) {
                                var _row = [];
                                $.map(row, function (item) {
                                    _row[item.columnNumber] = item;
                                });
                                $scope.PayrollData.Data.push(_row);
                            });
                        }, function errorCallback(res) {
                            //console.log('error', res);
                            toastrService.error('', 'תקלת מערכת!');
                        });
                    break;
                case 'PayrollBySymbol':
                    PayrollServise.PayrollBySymbol($scope.WSPostModel)
                        .then(function successCallback(res) {
                            $scope.PayrollData = {
                                Titles: [],
                                Data: [],
                            };
                            $scope.PayrollData.Titles = [];
                            $.map(res.data.Titles, function (item) {
                                $scope.PayrollData.Titles[item.number] = item;
                            });

                            res.data.Data.filter(function (row) {
                                var _row = [];
                                $.map(row, function (item) {
                                    _row[item.columnNumber] = item;
                                });
                                $scope.PayrollData.Data.push(_row);
                            });
                        }, function errorCallback(res) {
                            //console.log('error', res);
                            toastrService.error('', 'תקלת מערכת!');
                        });
                    break;
                case 'TrainingAndProvidentFunds':
                    PayrollServise.GetFundsTable($scope.WSPostModel)
                        .then(function successCallback(res) {
                            $scope.PayrollData = res.data;
                        }, function errorCallback(res) {
                            //console.log('error', res);
                            toastrService.error('', 'תקלת מערכת!');
                        });
                    break;
                case '106Form':
                    $scope.show106PDF();
                    break;
                case 'Paycheck':
                    $scope.showPaycheckPDF();
                    break;
            }            
        }
        
        $scope.show106PDF = function () {
            $scope.fileimageblob = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXY3j//j0ABZ4CzkTR0PgAAAAASUVORK5CYII=';
            $scope.WSPostModel.selectedDate = new Date($scope.currentYear, 11, 11);
            
            PayrollServise.Get106Form($scope.WSPostModel)
                .then(function successCallback(res) {
                    $scope.pdfContent = '';
                    $scope.$pdfContent = false;
                    $scope.formMsg = '';
                    $scope.pdfDataStatus = res.status;
                    if (res.status === 406) {
                        $scope.formMsg = 'אין טופס 106 לשנת YEAR.'.replace('YEAR', $scope.currentYear);
                    } else {
                        $scope.formMsg = 'טופס 106 לשנת YEAR.'.replace('YEAR', $scope.currentYear);
                        
                        try {
                            var file = b64toBlob(res.data.fileString, 'application/pdf');
                            var fileURL = URL.createObjectURL(file);
                            $scope.pdfContent = $sce.trustAsResourceUrl(fileURL);
                        } catch (e) {
                            //console.log(e);
                        }

                        $scope.$pdfContent = true;
                        $scope.fileimageblob = res.data.fileimageblob;
                        $scope.publish('PdfContect.Changed', { pdfControllerState: $controllerState, pdfDate: $scope.WSPostModel.selectedDate });
                    }
                    
                }, function errorCallback(res) {
                    //console.log('error', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.showPaycheckPDF = function () {
            $scope.fileimageblob = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXY3j//j0ABZ4CzkTR0PgAAAAASUVORK5CYII=';

            PayrollServise.GetPaycheck($scope.WSPostModel)
                .then(function successCallback(res) {
                    $scope.pdfContent = '';
                    $scope.$pdfContent = false;
                    $scope.formMsg = '';
                    $scope.pdfDataStatus = res.status;
                    if (res.status === 406) {
                        $scope.formMsg = 'אין תלוש שכר לתאריך CURRENTDATE'.replace('CURRENTDATE', parseMonth($scope.WSPostModel.selectedDate));
                    } else {
                        try {
                            var file = b64toBlob(res.data.fileString, 'application/pdf');
                            var fileURL = URL.createObjectURL(file);
                            $scope.pdfContent = $sce.trustAsResourceUrl(fileURL);
                        } catch (e) {
                            //console.log(e);
                        }
                        $scope.fileimageblob = res.data.fileimageblob;

                        $scope.$pdfContent = true;
                        $scope.formMsg = 'תלוש שכר לתאריך CURRENTDATE'.replace('CURRENTDATE', parseMonth($scope.WSPostModel.selectedDate));                        
                        $scope.publish('PdfContect.Changed', { pdfControllerState: $controllerState, pdfDate: $scope.WSPostModel.selectedDate });
                    }
                }, function errorCallback(res) {
                    //console.log('error', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        
    }
})();
(function () {
    angular.module('WorkerPortfolioApp').controller('ProcessesController', ProcessesController);

    ProcessesController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$location', '$interval', '$window', 'toastrService', 'ProcessesData', 'Processes', 'MyProcesses', 'UsersList', 'PayrollServise', 'VacationData', 'FileSaver', 'Blob', 'AcademicInst', 'YehidaIrgunit', 'MyRequestProcesses'];

    function ProcessesController($scope, $rootScope, $state, $stateParams, $location, $interval, $window, toastrService, ProcessesData, Processes, MyProcesses, UsersList, PayrollServise, VacationData, FileSaver, Blob, AcademicInst, YehidaIrgunit, MyRequestProcesses) {

        $scope.processesTypes = [{ id: 7, name: "בקשת יציאה לחופשה" }, { id: 5, name: "בקשת יציאה לחופשה בחול" }, { id: 3, name: "בקשת יציאה להשתלמות/קורס" }];
        
        $scope.processesStatus = [
            { id: 0, name: "טרם טופלו" },
            { id: 1, name: "בקשות שאושרו" },
            { id: 2, name: "בקשות שנדחו" }
        ];

        //as defined in KA_TAVLA -> table 1627
        $scope.stepTikOvedTemplates = [
            { id: 1, action: "בקשת יציאה בתפקיד", rashut: 500, template: "/Scripts/views/partials/Process-Templates/role-form.html", isDefault: true },
            { id: 2, action: "אישור מנהל יציאה בתפקיד", rashut: 500, template: null, isDefault: true },
            { id: 3, action: "בקשת יציאה ללימודים", rashut: 500, template: "/Scripts/views/partials/Process-Templates/course-form.html", isDefault: true },
            { id: 4, action: "אישור בקשה ללימודים", rashut: 500, template: null, isDefault: true },
            { id: 5, action: "בקשת חופשה בחול", rashut: 500, template: "/Scripts/views/partials/Process-Templates/vacation-form.html", isDefault: true },
            { id: 6, action: "אישור חופשה בחול", rashut: 500, template: null, isDefault: true },
            { id: 7, action: "בקשת חופשה", rashut: 500, template: "/Scripts/views/partials/Process-Templates/vacation-form.html", isDefault: true },
            { id: 8, action: "הודעה על קריאה", rashut: 500, template: null, isDefault: true },
            { id: 9, action: "אישור קריאה", rashut: 500, template: null, isDefault: true },
            { id: 10, action: "טופס טיולים לעזיבה", rashut: 500, template: "/Scripts/views/partials/Process-Templates/termination-form.html", isDefault: true },
            { id: 11, action: "בקשת משרה חדשה", rashut: 500, template: "/Scripts/views/partials/Process-Templates/new-job-form.html", isDefault: true },
            { id: 12, action: "אישור משרה", rashut: 500, template: null, isDefault: true },
            { id: 13, action: "אישור דרגה חדשה", rashut: 500, template: null, isDefault: true },
            { id: 14, action: "בקשת שעות נוספות", rashut: 500, template: "/Scripts/views/partials/Process-Templates/extra-hours-form.html", isDefault: true },
            { id: 15, action: "אישור שעות נוספות", rashut: 500, template: null, isDefault: true },
            { id: 16, action: "בקשת החזרי נסיעה", rashut: 500, template: "/Scripts/views/partials/Process-Templates/travel-refunds-form.html", isDefault: true },
            { id: 17, action: "אישור החזרי נסיעה", rashut: 500, template: null, isDefault: true }
        ];
                
        $scope.resetFormFlags = function () {
            $scope.formFlags = {
                isACoureseRequestForm: false,
                isVacationForm: false,
                isAboardVacationForm: false,
                isARoleForm: false,
                isATerminationForm: false,
                isNewJobForm: false,
                isExtraHoursForm: false,
                isTravelRefundsForm: false
            }
        }
    
        var FORM_ENUM = {
            ROLE_FORM: 1,
            COURSE_FORM: 3,
            ABOARD_VACATION_FORM: 5,
            VACATION_FORM: 7,
            TERMINATION_FORM: 10,
            NEW_JOB_FORM: 11,
            EXTRA_HOURS_FORM: 14,
            TRAVEL_REFUND_FORM: 16
        }

        var KVUZOT_ENUM = {
            KVUZAT_MISHTAMSHIM_REGILA: 1,
            KVUZAT_MISHTAMSHIM_DINAMIM: 2
        }

        $scope.isMobile = function () {
            return $window.innerWidth < 801;
        }

        var getTypeOfFirstShalav = function(selectedProcessIndex) {
            
            for (var i = 0; i < $scope.allProcessesData.processes[selectedProcessIndex].shlavim.length;i++) {
                if ($scope.allProcessesData.processes[selectedProcessIndex].shlavim[i].misparShalav == 1) {
                    return $scope.allProcessesData.processes[selectedProcessIndex].shlavim[i].rechivId;
                }
            }
            
        }

        $scope.onProcessChange = function () {
            $scope.selectedProcessIndex = event.currentTarget.selectedIndex;
            $scope.selectedProcess = {
                id: $scope.allProcessesData.processes[$scope.selectedProcessIndex].recId,
                name: $scope.allProcessesData.processes[$scope.selectedProcessIndex].shemTahalich,
                type: getTypeOfFirstShalav($scope.selectedProcessIndex)//,
            }
            
            $scope.setCurrentSteps();
            $scope.setProcessStepTemplate();
        }

        $scope.setCurrentSteps = function () {
            if ($scope.selectedProcess != null && $scope.selectedProcessIndex != null) {
                $scope.currentSteps = angular.copy($scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim);
            }
        }

        $scope.setProcessStepTemplate = function () {
            
            $scope.currentRunningStep = null;
            $scope.currentRunningStepTemplate = null;

            $scope.nextStepOfCurrent = null;

            if($scope.currentSteps.length > 0) {
                $scope.currentRunningStep = $scope.currentSteps.find(function(step) {
                    return step.status == 0 && step.misparShalav == 1;
                });
                if ($scope.currentRunningStep) {
                    //get the next step of the current running step for setting the mngrs (kvuzat mishtamshim) that the request will be sent to
                    $scope.nextStepOfCurrent = $scope.currentSteps.find(function (step) {
                        return step.misparShalav == $scope.currentRunningStep.misparShalav + 1;
                    });
                }
            }
            if ($scope.currentRunningStep) {

                if ($scope.currentRunningStep.modulId != TIK_OVED_SYSTEM) {
                    $scope.resetFormFlags();
                    return;
                }

                switch ($scope.currentRunningStep.rechivId) {

                    case FORM_ENUM.ROLE_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: false,
                            isVacationForm: false,
                            isAboardVacationForm: false,
                            isARoleForm: true,
                            isATerminationForm: false,
                            isNewJobForm: false,
                            isExtraHoursForm: false,
                            isTravelRefundsForm: false
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.ROLE_FORM);
                        break;

                    case FORM_ENUM.COURSE_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: true,
                            isVacationForm: false,
                            isAboardVacationForm: false,
                            isARoleForm: false,
                            isATerminationForm: false,
                            isNewJobForm: false,
                            isExtraHoursForm: false,
                            isTravelRefundsForm: false
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.COURSE_FORM);
                        break;

                    case FORM_ENUM.ABOARD_VACATION_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: false,
                            isVacationForm: false,
                            isAboardVacationForm: true,
                            isARoleForm: false,
                            isATerminationForm: false,
                            isNewJobForm: false,
                            isExtraHoursForm: false,
                            isTravelRefundsForm: false
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.ABOARD_VACATION_FORM);
                        break;

                    case FORM_ENUM.VACATION_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: false,
                            isVacationForm: true,
                            isAboardVacationForm: false,
                            isARoleForm: false,
                            isATerminationForm: false,
                            isNewJobForm: false,
                            isExtraHoursForm: false,
                            isTravelRefundsForm: false
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.VACATION_FORM);
                        break;

                    case FORM_ENUM.TERMINATION_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: false,
                            isVacationForm: false,
                            isAboardVacationForm: false,
                            isARoleForm: false,
                            isATerminationForm: true,
                            isNewJobForm: false,
                            isExtraHoursForm: false,
                            isTravelRefundsForm: false
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.TERMINATION_FORM);
                        break;

                    case FORM_ENUM.NEW_JOB_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: false,
                            isVacationForm: false,
                            isAboardVacationForm: false,
                            isARoleForm: false,
                            isATerminationForm: false,
                            isNewJobForm: true,
                            isExtraHoursForm: false,
                            isTravelRefundsForm: false
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.NEW_JOB_FORM);
                        break;

                    case FORM_ENUM.EXTRA_HOURS_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: false,
                            isVacationForm: false,
                            isAboardVacationForm: false,
                            isARoleForm: false,
                            isATerminationForm: false,
                            isNewJobForm: false,
                            isExtraHoursForm: true,
                            isTravelRefundsForm: false
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.EXTRA_HOURS_FORM);
                        break;

                    case FORM_ENUM.TRAVEL_REFUND_FORM:
                        $scope.formFlags = {
                            isACoureseRequestForm: false,
                            isVacationForm: false,
                            isAboardVacationForm: false,
                            isARoleForm: false,
                            isATerminationForm: false,
                            isNewJobForm: false,
                            isExtraHoursForm: false,
                            isTravelRefundsForm: true
                        }
                        $scope.currentRunningStepTemplate = $scope.getStepTemplate(FORM_ENUM.TRAVEL_REFUND_FORM);
                        break;

                    default:
                        $scope.resetFormFlags();
                        $scope.currentRunningStepTemplate = null;
                        break;
                }
                
                if ($scope.currentRunningStepTemplate != null && $scope.nextStepOfCurrent != null) {//only if there is a template rendered - so we set step mngrs that will get the request

                    if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_DINAMIM) {//read the mngrs of next step and send them the request. should be menaalim yeshirim
                        if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.mishtamsheiKvuzaModel[0].mishtameshId == 2) {//mishtamesh id 2 in kvuzot dinamiyot is menaalim yeshirim. see table 1637 in ka_tavla
                            Processes.getUserManagers($scope.currentYehidaIrgunit, $scope.getWSPostModel).then(function successCallback(res) {
                                if (res.status == 200) {
                                    $scope.organizationUnitsManagers = [];
                                    $scope.organizationUnitsManagers = res.data;
                                    removeDuplicateMngrs();
                                }
                                else {
                                    toastrService.error('', 'לא ניתן היה למצוא משתמשים אשר הבקשה תופנה אליהם');
                                    $scope.organizationUnitsManagers = [];
                                }
                            }, function errorCallback(res) {
                                toastrService.error('', 'תקלת מערכת!');
                                $scope.organizationUnitsManagers = [];
                            });
                        }
                        else {
                            Processes.getMishtamsheiKvuzaDinamit($scope.getWSPostModel, $scope.nextStepOfCurrent.kvuzatMishtamshimModel.mishtamsheiKvuzaModel).then(function successCallback(res) {
                                if (res.status == 200) {
                                    $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                                    $scope.currentMishtamsheiKvuzaOfCurrentStep = res.data;
                                }
                                else {
                                    toastrService.error('', 'לא ניתן היה למצוא משתמשים אשר הבקשה תופנה אליהם');
                                    $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                                }
                            }, function errorCallback(res) {
                                toastrService.error('', 'תקלת מערכת!');
                                $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                            });
                         }
                    }
                    else if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_REGILA) {

                            Processes.getMishtamsheiKvuzaRegila($scope.getWSPostModel, $scope.nextStepOfCurrent.kvuzatMishtamshimModel.mishtamsheiKvuzaModel).then(function successCallback(res) {
                                if (res.status == 200) {
                                    $scope.currentMishtamsheiKvuzaOfCurrentStep = [];//this is the mishtamshei kvuza of next step (currentStep is 1, so mishtamshei kvuza of step 2)
                                    $scope.currentMishtamsheiKvuzaOfCurrentStep = res.data;
                                    reArrangeMishtamsehiKvuzaRegila();//to be the same structure as kvuza dinamit
                                }
                                else {
                                    toastrService.error('', 'לא ניתן היה למצוא משתמשים אשר הבקשה תופנה אליהם');
                                    $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                                }
                            }, function errorCallback(res) {
                                toastrService.error('', 'תקלת מערכת!');
                                $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                            });
                    }


                    //if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_DINAMIM || $scope.currentRunningStep.misparShalav == 1) {//read the mngrs of next step and send them the request. should be menaalim yeshirim
                    //    if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.mishtamsheiKvuzaModel[0].mishtameshId == 2 || $scope.currentRunningStep.misparShalav == 1) {//mishtamesh id 2 in kvuzot dinamiyot is menaalim yeshirim. see table 1637 in ka_tavla
                    //        Processes.getUserManagers($scope.currentYehidaIrgunit, $scope.getWSPostModel).then(function successCallback(res) {
                                
                    //            $scope.organizationUnitsManagers = [];
                    //            $scope.organizationUnitsManagers = res.data;
                    //            //reArrangeOrganizationUnitsManagers();
                    //            removeDuplicateMngrs();
                    //        }, function errorCallback(res) {
                                
                    //        });
                    //    }
                    //}
                    //else {

                    //    //REGULAR GROUP MNGRS
                    //    if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_REGILA) {
                    //        Processes.getMishtamsheiKvuzaRegila($scope.getWSPostModel, $scope.nextStepOfCurrent.kvuzatMishtamshimModel.mishtamsheiKvuzaModel).then(function successCallback(res) {
                    //            if (res.status == 200) {
                    //                $scope.currentMishtamsheiKvuzaOfCurrentStep = [];//this is the mishtamshei kvuza of next step (currentStep is 1, so mishtamshei kvuza of step 2)
                    //                $scope.currentMishtamsheiKvuzaOfCurrentStep = res.data;
                    //                reArrangeMishtamsehiKvuzaRegila();//to be the same structure as kvuza dinamit
                    //            }
                    //            else {
                    //                toastrService.error('', 'לא ניתן היה למצוא משתמשים אשר הבקשה תופנה אליהם');
                    //                $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                    //            }
                    //        }, function errorCallback(res) {
                    //            toastrService.error('', 'תקלת מערכת!');
                    //            $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                    //        });
                    //    }
                    //    //DINAMIC GROUP MNGRS
                    //    else if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_DINAMIM) {
                    //        Processes.getMishtamsheiKvuzaDinamit($scope.getWSPostModel, $scope.nextStepOfCurrent.kvuzatMishtamshimModel.mishtamsheiKvuzaModel).then(function successCallback(res) {
                    //            if (res.status == 200) {
                    //                $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                    //                $scope.currentMishtamsheiKvuzaOfCurrentStep = res.data;
                    //            }
                    //            else {
                    //                toastrService.error('', 'לא ניתן היה למצוא משתמשים אשר הבקשה תופנה אליהם');
                    //                $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                    //            }
                    //        }, function errorCallback(res) {
                    //            toastrService.error('', 'תקלת מערכת!');
                    //            $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                    //        });
                    //    }
                    //}

                }
            }
        }

        var reArrangeMishtamsehiKvuzaRegila = function () {
            //הגשת בקשה חדשה 
            if ($scope.currentMishtamsheiKvuzaOfCurrentStep.length > 0) {
                var tempArr = angular.copy($scope.currentMishtamsheiKvuzaOfCurrentStep);
                $scope.currentMishtamsheiKvuzaOfCurrentStep = [];
                _.map(tempArr, function (mishtamesh) {
                    var newVal = {
                        shem_prati: mishtamesh.user.firstName,
                        shem_mishpaha: mishtamesh.user.lastName,
                        rashut: mishtamesh.rashut,
                        zehut: mishtamesh.user.idNumber,
                        kodMedaveach: mishtamesh.kodMedaveach
                    }
                    $scope.currentMishtamsheiKvuzaOfCurrentStep.push(newVal);
                });
            }
        }

        var reArrangeMishtamsehiKvuzaRegilaInMyProcessesTab = function (index) {
            if ($scope.mishtamsheiKvuzaPerProcess[index].length > 0) {
                var tempArr = angular.copy($scope.mishtamsheiKvuzaPerProcess[index]);
                $scope.mishtamsheiKvuzaPerProcess[index] = [];
                _.map(tempArr, function (mishtamesh) {
                    var newVal = {
                        shem_prati: mishtamesh.user.firstName,
                        shem_mishpaha: mishtamesh.user.lastName,
                        rashut: mishtamesh.rashut,
                        zehut: mishtamesh.user.idNumber,
                        kodMedaveach: mishtamesh.kodMedaveach
                    }
                    $scope.mishtamsheiKvuzaPerProcess[index].push(newVal);
                });
            }
        }

        var reArrangeMyReqProcesses = function () {
            $scope.processesOftheUser = [];
            _.map($scope.myRequestProcesses, function (processOfUser) {
                var procIndx = isExistUserProcess(processOfUser, $scope.processesOftheUser);
                if (procIndx < 0) {
                    var len = $scope.processesOftheUser.push({ process: processOfUser, targetUsers: [] });
                    $scope.processesOftheUser[len-1].targetUsers.push(processOfUser.TARGET_USER);
                }
                else {
                    $scope.processesOftheUser[procIndx].targetUsers.push(processOfUser.TARGET_USER);
                }
            });
            $scope.showProcessDetails = new Array($scope.processesOftheUser.length);
            $scope.isUpdatedProcess = new Array($scope.processesOftheUser.length);
            for (var indx = 0; indx < $scope.processesOftheUser.length; indx++) {
                $scope.showProcessDetails[indx] = false;
                $scope.isUpdatedProcess[indx] = true;
            }
        }

        var isExistUserProcess = function (processOfUser, arrayOfAllProcessesOfTheUser) {
            for (var indx = 0; indx < arrayOfAllProcessesOfTheUser.length; indx++) {
                if (arrayOfAllProcessesOfTheUser[indx].process.TM_TAHALICH_ID == processOfUser.TM_TAHALICH_ID) {
                    return indx;
                }
            }
            return -1;
        }

        $scope.getStepTemplate = function (templateID) {
            return $scope.stepTikOvedTemplates.find(function (step) {
                return step.id == templateID;
            })
        }

        //values for below types defined in KA_TAVLA -> table: 1627
        const explicitProcessTypes = {
            vacationType: 7,
            aboardVacationType: 5,
            courseRequestType: 3
        }

        const SACHAR_SYSTEM = 1;//for comparing modulId field in the process steps
        const TIK_OVED_SYSTEM = 2;//for comparing modulId field in the process steps

        var dates = {
            convert: function (d) {
                return (
                    d.constructor === Date ? d :
                    d.constructor === Array ? new Date(d[0], d[1], d[2]) :
                    d.constructor === Number ? new Date(d) :
                    d.constructor === String ? new Date(d) :
                    typeof d === "object" ? new Date(d.year, d.month, d.date) :
                    NaN
                );
            },
            compare: function (a, b) {
                return (
                    isFinite(a = this.convert(a).valueOf()) &&
                    isFinite(b = this.convert(b).valueOf()) ?
                    (a > b) - (a < b) :
                    NaN
                );
            },
            inRange: function (d, start, end) {
                return (
                     isFinite(d = this.convert(d).valueOf()) &&
                     isFinite(start = this.convert(start).valueOf()) &&
                     isFinite(end = this.convert(end).valueOf()) ?
                     start <= d && d <= end :
                     NaN
                 );
            }
        }

        $scope.compareDates = function(date1, date2) {
            return dates.compare(date1, date2);
        }

        $scope.autoGrow = function (event) {
            var element = event.target;
            element.style.height = "5px";
            element.style.height = (element.scrollHeight) + "px";
        }

        $scope.updateProcess = function (prcsIndex, process) {

            var currentUpdatedProcess = angular.copy(process);

            Processes.saveProcess(process, $scope.getWSPostModel).then(function successCallback(res) {
                $scope.$broadcast('angucomplete-alt:clearInput');//important => fix autocomplete bug
                if (res.status == 200) {
                    if (currentUpdatedProcess.STATUS == 1) {
                        if (currentUpdatedProcess.curr_mispar_shalav + 1 == currentUpdatedProcess.shlavim_counter) {
                            toastrService.success('הבקשה אושרה סופית!');
                        }
                        else toastrService.success('הבקשה אושרה והועברה לטיפול הגורמים המתאימים!');
                    }
                    else if(currentUpdatedProcess.STATUS == 2) {
                        toastrService.success('הבקשה נדחתה!');
                    }
                    else if (currentUpdatedProcess.STATUS == 3) {
                        toastrService.success('הבקשה אושרה סופית בהצלחה!');
                    }
                    else if (currentUpdatedProcess.TARGET_USERID != $scope.getWSPostModel.calculatedIdNumber) {
                        toastrService.success('הבקשה הועברה בהצלחה!');
                    }
                    
                    $scope.allMyProcessesData = res.data;
                    if ($scope.allMyProcessesData.length > 0) {
                        $scope.myProcessesIsEmpty = false;
                        $scope.filters.selectedStatusFilter = 0;
                        $scope.filterMyProcesses('status');
                    }
                    else {
                        $scope.myProcessesData = new Array();
                        $scope.myProcessesIsEmpty = true;
                    }
                }
                else {
                    toastrService.error('', 'משהו השתבש והעדכון נכשל. אנא נסה שנית..');
                }
            }, function errorCallback(res) {
                toastrService.error('', 'תקלת מערכת!');
            });
        }
        
        $scope.courseForm = {
            empFirstName: '',
            empLastName: '',
            empId: '',
            empRole: '',
            empDepartment: '',
            empYearsOfExperiance: '',
            coursePlan:'',
            courseInstitution: '',
            courseInstitutionID: '',
            courseNumberOfDays: '',
            courseNumberOfHours:''
        }

        $scope.datesRange = {
            from: null,
            to: null
        }

        $scope.newOptions = {
            yearRange: "0:+100",
            minDate: new Date(),
        }

        $scope.myProcessesIsEmpty = false;
        $scope.myRequestProcessesIsEmpty = false;
        $scope.vacationReason = "";
        $scope.vacationLeft = "אין מידע";
        $scope.vacationAmount = new Array();
        $scope.representedWorkers = new Array();
        $scope.selectedRepresentWorker = {
            idNumber: '',
            fullName: '',
            email: ''
        }
        $scope.usersAndUnitMngrs = new Array();
        $scope.showProcessDetails = new Array();
        $scope.isNewTargetUser = new Array();
        $scope.isUpdatedProcess = new Array();
        $scope.myProcessesData = new Array();
        $scope.myRequestProcesses = new Array();

        //for dates-filter models
        var today = new Date(), month, day, year;
        year = today.getFullYear()
        month = today.getMonth()
        date = today.getDate()
        if ((month-1) <= 0) year = today.getFullYear() - 1;

        var formatDate = function (date) {
            if (date) {
                return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + "/" + ((date.getMonth()+1) < 10 ? "0" + (date.getMonth()+1) : (date.getMonth()+1)) + "/" + date.getFullYear();
            }
            else return formatdate(new Date());
        }

        $scope.filters = {
            isDatesFilter: false,
            isEmployeeFilter: false,
            isTypesFilter: false,
            isStatusFilter: false,
            isStatusOpenFilter: false,
            isAllFilter: false,
            selectedFromDateFilter: formatDate(new Date(year, month-1, date)),
            selectedToDateFilter: formatDate(new Date()),
            selectedTypeFilter: null,
            selectedStatusFilter: null,
            selectedEmployeeFilter: null
        }

        $scope.displayEmployeeFilter = function () {

            $scope.filters.isDatesFilter = false;
            $scope.filters.isEmployeeFilter = true;
            $scope.filters.isTypesFilter = false;
            $scope.filters.isStatusFilter = false;
            $scope.filters.isStatusOpenFilter = false;
            $scope.filters.isAllFilter = false;
        }

        $scope.displayDatesFilter = function () {
            if (!$scope.filters.isDatesFilter) {
                $scope.filterMyProcesses('dates');
            }
            $scope.filters.isDatesFilter = true;
            $scope.filters.isEmployeeFilter = false;
            $scope.filters.isTypesFilter = false;
            $scope.filters.isStatusFilter = false;
            $scope.filters.isStatusOpenFilter = false;
            $scope.filters.isAllFilter = false;
        }

        $scope.displayTypesFilter = function () {
            $scope.filters.isDatesFilter = false;
            $scope.filters.isEmployeeFilter = false;
            $scope.filters.isTypesFilter = true;
            $scope.filters.isStatusFilter = false;
            $scope.filters.isStatusOpenFilter = false;
            $scope.filters.isAllFilter = false;
        }

        $scope.displayStatusFilter = function () {
            $scope.filters.isDatesFilter = false;
            $scope.filters.isEmployeeFilter = false;
            $scope.filters.isTypesFilter = false;
            $scope.filters.isStatusFilter = true;
            $scope.filters.isStatusOpenFilter = false;
            $scope.filters.isAllFilter = false;
        }

        $scope.resetSelectedFilters = function () {
            $scope.filters.selectedFromDateFilter = formatDate(new Date(year, month - 1, date));
            $scope.filters.selectedToDateFilter = formatDate(new Date());
            $scope.filters.selectedTypeFilter = null;
            $scope.filters.selectedStatusFilter = null;
            $scope.filters.selectedEmployeeFilter = null;
        }

        $scope.exportExcelTable = function () {
            var dataForExcel = new Array();
            dataForExcel.push('', 'שם הבקשה', 'מתאריך', 'עד תאריך', 'שם העובד', 'ת.ז', 'סטטוס הבקשה', 'תאריך הגשת הבקשה', 'סיבה', 'יתרת חופשה', 'תפקיד העובד', 'מחלקה', 'שנות ותק', 'מוסד השכלה', 'צפי ימים', 'צפי שעות', 'גורם מטפל');
            dataForExcel.push("\n");

            if ($scope.myProcessesData.length > 0) {
                _.map($scope.myProcessesData, function (myProcess) {
                    dataForExcel.push(myProcess.PROCESS_DESCRIPTION);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? myProcess.FROM_DATE : "אין");
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? myProcess.TO_DATE : "אין");
                    dataForExcel.push(myProcess.USERMODEL.firstName + " " + myProcess.USERMODEL.lastName);
                    dataForExcel.push(myProcess.USERMODEL.idNumber);
                    dataForExcel.push(myProcess.STATUS == 0 ? "טרם התקבל" : myProcess.STATUS == 1 ? "אושר בשלב ביניים" : myProcess.STATUS == 3 ? "אושר" : "נדחה");
                    dataForExcel.push(myProcess.CREATION_DATE);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? myProcess.CONTENT1 : myProcess.CONTENT4);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? myProcess.CONTENT2 : "");
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? "" : myProcess.CONTENT1);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? "" : myProcess.CONTENT2);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? "" : myProcess.CONTENT3);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? "" : myProcess.CONTENT5);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? "" : myProcess.CONTENT6);
                    dataForExcel.push(myProcess.PROCESS_TYPE == 5 || myProcess.PROCESS_TYPE == 7 ? "" : myProcess.CONTENT7);
                    dataForExcel.push();
                    dataForExcel.push("\n");
                });
            }
            else if ($scope.myRequestProcesses.length > 0) {
                _.map($scope.processesOftheUser, function (myReqProcess) {//processesOftheUser is new order array of myRequestProcesses 
                    var gormim = getGormimForProcess(myReqProcess);
                    dataForExcel.push(myReqProcess.process.PROCESS_DESCRIPTION);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? myReqProcess.process.FROM_DATE : "אין");
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? myReqProcess.process.TO_DATE : "אין");
                    dataForExcel.push(myReqProcess.process.USERMODEL.firstName + " " + myReqProcess.process.USERMODEL.lastName);
                    dataForExcel.push(myReqProcess.process.USERMODEL.idNumber);
                    dataForExcel.push(myReqProcess.process.STATUS == 0 ? "טרם התקבל" : myReqProcess.process.STATUS == 1 ? "אושר בשלב ביניים" : myReqProcess.process.STATUS == 3 ? "אושר" : "נדחה");
                    dataForExcel.push(myReqProcess.process.CREATION_DATE);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? myReqProcess.process.CONTENT1 : myReqProcess.process.CONTENT4);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? myReqProcess.process.CONTENT2 : "");
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? "" : myReqProcess.process.CONTENT1);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? "" : myReqProcess.process.CONTENT2);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? "" : myReqProcess.process.CONTENT3);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? "" : myReqProcess.process.CONTENT5);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? "" : myReqProcess.process.CONTENT6);
                    dataForExcel.push(myReqProcess.process.PROCESS_TYPE == 5 || myReqProcess.process.PROCESS_TYPE == 7 ? "" : myReqProcess.process.CONTENT7);
                    dataForExcel.push(gormim);
                    dataForExcel.push("\n");
                });
            }
            try {//chrome
                var blob = new Blob([dataForExcel], {
                    type: 'application/xml;charset=utf-8'
                });
                FileSaver.saveAs(blob, "Report" + Date.now() + ".xls");
            } catch (e) {//explorer
                if (e.name == "InvalidStateError") {

                    //for hebrew support
                    var BOM = "\ufeff";
                    dataForExcel = BOM + dataForExcel;

                    var bb = new MSBlobBuilder();
                    bb.append(dataForExcel);
                    var blob = bb.getBlob("text/csv;charset=utf-8");
                    window.navigator.msSaveOrOpenBlob(blob, "Report" + Date.now() + ".xls");
                }
            }
        }

        var getGormimForProcess = function (reqProcess) {
            var ans = '';
            for (var i = 0; i < reqProcess.targetUsers.length; i++) {
                ans += reqProcess.targetUsers[i].firstName + " " + reqProcess.targetUsers[i].lastName + "   ";
            }
            return ans;
        }

        var isInRange = function (processCreationDate) {
            if (processCreationDate) {
                var y, m, d, y1, m1, d1, y2, m2, d2;
                var fields = processCreationDate.split('/');
                d = fields[0];
                m = fields[1];
                y = fields[2];

                fields = $scope.filters.selectedFromDateFilter.split('/');
                d1 = fields[0];
                m1 = fields[1];
                y1 = fields[2];

                fields = $scope.filters.selectedToDateFilter.split('/');
                d2 = fields[0];
                m2 = fields[1];
                y2 = fields[2];

                return dates.inRange(new Date(y,m-1,d), new Date(y1,m1-1,d1), new Date(y2,m2-1,d2));
            }
            return true;
        }

        $scope.filterMyProcesses = function (filterBy) {
            
            switch (filterBy) {
                case 'dates':
                    $scope.filters.isStatusFilter = false;
                    $scope.filters.isEmployeeFilter = false;
                    $scope.filters.isTypesFilter = false;
                    $scope.filters.isStatusOpenFilter = false;
                    $scope.filters.isAllFilter = false;
                    $scope.myProcessesData = new Array();
                    $scope.myRequestProcesses = new Array();

                    if ($scope.allMyProcessesData.length > 0) {
                        _.map($scope.allMyProcessesData, function (myProcess) {
                            if (isInRange(myProcess.CREATION_DATE)) {
                                $scope.myProcessesData.push(myProcess);
                            }
                        });
                    }
                    else if ($scope.myRequestProcessesData.length > 0) {
                        _.map($scope.myRequestProcessesData, function (myReqProcess) {
                            if (isInRange(myReqProcess.CREATION_DATE)) {
                                $scope.myRequestProcesses.push(myReqProcess);
                            }
                        });
                    }
                    break;

                case 'all':
                    $scope.filters.isDatesFilter = false;
                    $scope.filters.isEmployeeFilter = false;
                    $scope.filters.isTypesFilter = false;
                    $scope.filters.isStatusFilter = false;
                    $scope.filters.isStatusOpenFilter = false;
                    $scope.filters.isAllFilter = true;
                    $scope.myProcessesData = new Array();
                    $scope.myRequestProcesses = new Array();

                    if ($scope.allMyProcessesData.length > 0) {
                        $scope.myProcessesData = angular.copy($scope.allMyProcessesData);
                    }
                    else if ($scope.myRequestProcessesData.length > 0) {
                        $scope.myRequestProcesses = angular.copy($scope.myRequestProcessesData);
                    }
                    break;

                case 'type':
                    $scope.filters.isDatesFilter = false;
                    $scope.filters.isEmployeeFilter = false;
                    $scope.filters.isStatusFilter = false;
                    $scope.filters.isStatusOpenFilter = false;
                    $scope.filters.isAllFilter = false;
                    $scope.myProcessesData = new Array();
                    $scope.myRequestProcesses = new Array();

                    if ($scope.allMyProcessesData.length > 0) {
                        _.map($scope.allMyProcessesData, function (myProcess) {
                            if (myProcess.PROCESS_TYPE == $scope.filters.selectedTypeFilter) {
                                $scope.myProcessesData.push(myProcess);
                            }
                        });
                    }
                    else if ($scope.myRequestProcessesData.length > 0) {
                        _.map($scope.myRequestProcessesData, function (myReqProcess) {
                            if (myReqProcess.PROCESS_TYPE == $scope.filters.selectedTypeFilter) {
                                $scope.myRequestProcesses.push(myReqProcess);
                            }
                        });
                    }
                    break;

                case 'status':
                    $scope.filters.isDatesFilter = false;
                    $scope.filters.isEmployeeFilter = false;
                    $scope.filters.isStatusFilter = true;
                    $scope.filters.isStatusOpenFilter = false;
                    $scope.filters.isAllFilter = false;
                    $scope.filters.isTypesFilter = false;
                    $scope.myProcessesData = new Array();
                    $scope.myRequestProcesses = new Array();

                    if ($scope.allMyProcessesData.length > 0) {
                        _.map($scope.allMyProcessesData, function (myProcess) {
                            if (myProcess.STATUS == $scope.filters.selectedStatusFilter) {//3 is new status for final approve from sachar system
                                $scope.myProcessesData.push(myProcess);
                            }
                            if ($scope.filters.selectedStatusFilter == 1) {//if filter is approval processes(1) so show also processes with final approve(3) together with temp approve
                                if(myProcess.STATUS == 3) {
                                    $scope.myProcessesData.push(myProcess);
                                }
                            }
                            if ($scope.filters.selectedStatusFilter == 0) {//if filter is approval processes(1) so show also processes with final approve(3) together with temp approve
                                if (myProcess.STATUS == 1) {
                                    $scope.myProcessesData.push(myProcess);
                                }
                            }
                        });
                    }
                    else if ($scope.myRequestProcessesData.length > 0) {
                        _.map($scope.myRequestProcessesData, function (myReqProcess) {
                            if (myReqProcess.STATUS == $scope.filters.selectedStatusFilter) {//3 is new status for final approve from sachar system
                                $scope.myRequestProcesses.push(myReqProcess);
                            }
                            if ($scope.filters.selectedStatusFilter == 1) {//if filter is approval processes(1) so show also processes with final approve(3) together with temp approve
                                if (myReqProcess.STATUS == 3) {
                                    $scope.myRequestProcesses.push(myReqProcess);
                                }
                            }
                            if ($scope.filters.selectedStatusFilter == 0) {//if filter is approval processes(1) so show also processes with final approve(3) together with temp approve
                                if (myReqProcess.STATUS == 1) {
                                    $scope.myRequestProcesses.push(myReqProcess);
                                }
                            }
                        });
                    }
                    break;
                case 'employee':
                    $scope.filters.isStatusOpenFilter = false;
                    $scope.filters.isEmployeeFilter = true;
                    $scope.filters.isDatesFilter = false;
                    $scope.filters.isTypesFilter = false;
                    $scope.filters.isStatusFilter = false;
                    $scope.filters.isAllFilter = false;
                    $scope.myProcessesData = new Array();

                    _.map($scope.allMyProcessesData, function (myProcess) {
                        if (myProcess.OVED_NESU_BAKASHA != "" || myProcess.OVED_NESU_BAKASHA != '') {
                            if (myProcess.OVED_NESU_BAKASHA == $scope.filters.selectedEmployeeFilter) {
                                $scope.myProcessesData.push(myProcess);
                            }   
                        }
                        else if (myProcess.USERMODEL.calculatedIdNumber == $scope.filters.selectedEmployeeFilter) {
                            $scope.myProcessesData.push(myProcess);
                        }
                    });
                    break;
                default: break;
            }
            
            //for all cases:
            if($scope.allMyProcessesData.length > 0) {
                $scope.showProcessDetails = new Array($scope.myProcessesData.length);
                $scope.isUpdatedProcess = new Array($scope.myProcessesData.length);
                $scope.isNewTargetUser = new Array($scope.myProcessesData.length);
            }
            else if ($scope.myRequestProcessesData.length > 0) {
                $scope.showProcessDetails = new Array($scope.myRequestProcesses.length);
                $scope.isUpdatedProcess = new Array($scope.myRequestProcesses.length);
                $scope.isNewTargetUser = new Array($scope.myRequestProcesses.length);
                $scope.processesOftheUser = new Array();
            }

            if ($scope.myProcessesData.length > 0) {
                for (var i = 0; i < $scope.myProcessesData.length; i++) {
                    $scope.showProcessDetails[i] = false;
                    $scope.isNewTargetUser[i] = false;

                    if ($scope.myProcessesData[i].STATUS != 0) {
                        $scope.isUpdatedProcess[i] = true;
                    }
                    else {
                        $scope.isUpdatedProcess[i] = false;
                    }
                }
                $scope.myProcessesIsEmpty = false;
            }
            else $scope.myProcessesIsEmpty = true;

            if ($scope.myRequestProcesses.length > 0) {

                //new 24.06.18
                $scope.processesOftheUser = [];
                reArrangeMyReqProcesses();
                //new ends here

                for (var i = 0; i < $scope.myRequestProcesses.length; i++) {
                    $scope.showProcessDetails[i] = false;
                    $scope.isNewTargetUser[i] = false;
                }
                $scope.myRequestProcessesIsEmpty = false;
            }
            else $scope.myRequestProcessesIsEmpty = true;
        }

        $scope.processesOftheUser = [];

        $scope.selectedProcess = {
            id: '',
            name: '',
            type: ''
        }
        $scope.selectedProcessIndex = null;

        $scope.selectedUnitManager = {
            idNumber: '',
            fullName: '',
            rashutNumber: ''
        }

        $scope.instfocusOut = function (selected) {
            if(selected == undefined) {
                $scope.courseForm.courseInstitution = $('#academicInst_value')[0].value;
                $scope.courseForm.courseInstitutionID = '';
                $scope.$broadcast('angucomplete-alt:changeInput', 'academicInst', $scope.courseForm.courseInstitution);
            }
        }

        $scope.onRepresentWorkerChange = function (selected) {
            if(selected != null && selected != undefined) {
                $scope.selectedRepresentWorker = {
                    idNumber: selected.originalObject.idNumber,
                    fullName: selected.originalObject.fullName,
                    email: selected.originalObject.email
                }
                Processes.getWorkerVacationData($scope.getWSPostModel, $scope.selectedRepresentWorker).then(function successCallback(res) {
                    if (res.data.length > 0) {
                        $scope.vacationData = res.data;
                        if($scope.vacationData.length > 0) {
                            reArrangeVacationData();
                        }
                    }
                    else {
                        scope.vacationLeft = "אין מידע";
                    }
                }, function errorCallback(res) {
                    scope.vacationLeft = "אין מידע";
                });
                Processes.getWorkerYehidaIrgunit($scope.getWSPostModel, $scope.selectedRepresentWorker).then(function successCallback(res) {
                    var workerYehidaIrgunit = null
                    if (res.data.length > 0) {
                        workerYehidaIrgunit = res.data.length > 0 ? res.data : null;
                    }
                    else {
                        workerYehidaIrgunit = null;
                    }
                    if (workerYehidaIrgunit != null) {
                        Processes.getWorkerManagers($scope.getWSPostModel, $scope.selectedRepresentWorker, workerYehidaIrgunit).then(function successCallback(res) {
                            if (res.data.length > 0) {
                                
                                $scope.organizationUnitsManagers =[];
                                $scope.organizationUnitsManagers = res.data;
                                $scope.organizationUnitsManagers.map(function (currentValue, index, organizationUnitsManagers) {

                                    if (currentValue.idNumber != $scope.getWSPostModel.calculatedIdNumber) {

                                        currentValue = {

                                            fName: currentValue.fName,
                                            idNumber: currentValue.idNumber,
                                            lName: currentValue.lName,
                                            ramaIrgunit: currentValue.ramaIrgunit,
                                            rashutNumber: currentValue.rashutNumber,
                                            supervisorUnitNumber: currentValue.supervisorUnitNumber,
                                            tekenKoachAdam: currentValue.tekenKoachAdam,
                                            tekenMenaelYahidaIrgunit: currentValue.tekenMenaelYahidaIrgunit,
                                            treeNumber: currentValue.treeNumber,
                                            unitDescription: currentValue.unitDescription,
                                            unitNumber: currentValue.unitNumber,
                                            zehutNatzigKoachAdam: currentValue.zehutNatzigKoachAdam,

                                                //fullName: currentValue.fName + " " +currentValue.lName,
                                                //idNumber: currentValue.idNumber,
                                                //rashutNumber: currentValue.rashutNumber,
                                                //zehutNatzigKoachAdam: currentValue.zehutNatzigKoachAdam,
                                                //tekenKoachAdam: currentValue.tekenKoachAdam,
                                                //tekenMenaelYahidaIrgunit: currentValue.tekenMenaelYahidaIrgunit
                                        }
                                        $scope.organizationUnitsManagers[index]= currentValue;
                                    }
                                });
                                //reArrangeUsersVsUnitsManagers();
                                removeDuplicateMngrs();
                            }
                            else { 
                                toastrService.error('', 'לא ניתן היה למצוא משתמשים אשר הבקשה תופנה אליהם');
                            }
                        }, function errorCallback(res) {
                            toastrService.error('', 'לא ניתן היה למצוא משתמשים אשר הבקשה תופנה אליהם');
                        });
                    }
                }, function errorCallback(res) {
                    toastrService.error('', 'העובד שנבחר לא שובץ ליחידה ארגונית.');
            });
            }
        }

        $scope.setEmptyRepresentWorker = function (selected) {
            if (selected == '' || selected == "" || selected.originalObject == undefined) {
                $scope.selectedRepresentWorker = {
                    idNumber: '',
                    fullName: '',
                    email: ''
                }
                PayrollServise.GetAttendanceFigures($rootScope.getWSPostModel()).then(function successCallback(res) {
                    if (res.data.length > 0) {
                        $scope.vacationData = res.data;
                        if ($scope.vacationData.length > 0) {
                            reArrangeVacationData();
                        }
                    }
                    else {
                        scope.vacationLeft = "אין מידע";
                    }
                }, function errorCallback(res) {
                    scope.vacationLeft = "אין מידע";
                });
                if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_DINAMIM) {//read the mngrs of next step and send them the request. should be menaalim yeshirim
                    if ($scope.nextStepOfCurrent.kvuzatMishtamshimModel.mishtamsheiKvuzaModel[0].mishtameshId == 2) {//mishtamesh id 2 in kvuzot dinamiyot is menaalim yeshirim. see table 1637 in ka_tavla
                        Processes.getUserManagers($scope.currentYehidaIrgunit, $scope.getWSPostModel).then(function successCallback(res) {
                            $scope.organizationUnitsManagers = [];
                            $scope.organizationUnitsManagers = res.data;
                            removeDuplicateMngrs();
                        }, function errorCallback(res) {
                            
                        });
                    }
                }
            }
        }

        $scope.onUnitManagerChange = function (selected) {
            $scope.selectedUnitManager.idNumber = selected.originalObject.idNumber;
            $scope.selectedUnitManager.fullName = selected.originalObject.fullName;
            $scope.selectedUnitManager.rashutNumber = selected.originalObject.rashutNumber;
        }

        $scope.onAcademicInstChange = function (selected) {
            $scope.courseForm.courseInstitution = selected.originalObject.NAME;
            $scope.courseForm.courseInstitutionID = selected.originalObject.ID;
        }

        $scope.setEmptyAcademicInst = function (selected) {
            if(selected == '' || selected == "") {
                $scope.courseForm.courseInstitution = '';
                $scope.courseForm.courseInstitutionID = '';
            }
        }

        $scope.setEmptyMngr = function (selected) {
            if (selected =='' || selected == "") {
                $scope.selectedUnitManager.idNumber = '';
                $scope.selectedUnitManager.fullName = '';
                $scope.selectedUnitManager.rashutNumber = '';
            }
        }

        $scope.setNewEmployeeFilter = function (selected) {
            if (selected !== undefined) {
                $scope.filters.selectedEmployeeFilter = selected.originalObject.user.calculatedIdNumber;
                $scope.filterMyProcesses('employee');
            }
        }

        $scope.setEmptyEmployeeFilter = function (selected) {
            if (selected == '') {
                $scope.filters.selectedEmployeeFilter = null;
            }
        }

        $scope.setNewTargetUserId = function (selected) {
            if (selected !== undefined) {
                $scope.myProcessesData[this.$parent.$index].TARGET_USERID = selected.originalObject.zehut;
                $scope.isNewTargetUser[this.$parent.$index] = true;
            }
        }

        $scope.setEmptyTargetUserId = function (selected) {
                
            if (selected == '') {
                $scope.myProcessesData[this.$parent.$index].TARGET_USERID = $rootScope.getWSPostModel().idNumber;
                $scope.isNewTargetUser[this.$parent.$index] = false;
            }
        }

        $scope.onDateFocus = function () {
            $scope.ProcessForm.from.$setValidity('range', true);
            $scope.ProcessForm.from.$setValidity('custom', true);
            $scope.ProcessForm.from.$setValidity('required', true);

            $scope.ProcessForm.to.$setValidity('range', true);
            $scope.ProcessForm.to.$setValidity('custom', true);
            $scope.ProcessForm.to.$setValidity('required', true);
        }

        $scope.onDateChange = function () {
            if ($scope.datesRange.from != undefined && $scope.datesRange.to != undefined) {

                var year = $scope.datesRange.from.slice(6, 10), month = $scope.datesRange.from.slice(3, 5), day = $scope.datesRange.from.slice(0, 2);
                var fromDate = new Date(year, parseInt(month)-1, day);

                year = $scope.datesRange.to.slice(6, 10), month = $scope.datesRange.to.slice(3, 5), day = $scope.datesRange.to.slice(0, 2);
                var toDate = new Date(year, parseInt(month)-1, day);

                if ($scope.compareDates(fromDate, toDate) > 0) {
                    $scope.ProcessForm.to.$setValidity('range', false);
                }
            }
        }
        
        /*submit the form function*/
        $scope.postAForm = function () {

            if ($scope.ProcessForm.$invalid)
                return void 0;

            if ($scope.currentRunningStep.rechivId == 5 || $scope.currentRunningStep.rechivId == 7) {//is a vacation form

                if (!$scope.ProcessForm.from.$dirty) {
                    $scope.ProcessForm.from.$setValidity('required', false);
                }
                if (!$scope.ProcessForm.to.$dirty) {
                    $scope.ProcessForm.to.$setValidity('required', false);
                }
                var vacationFormDataList = new Array();
                if ($scope.currentRunningStep.misparShalav == 1) {
                    _.map($scope.organizationUnitsManagers, function (mishtamesh) {
                        var vacationFormData = {
                            selectedProcess: angular.copy($scope.selectedProcess),
                            datesRange: angular.copy($scope.datesRange),
                            vacationReason: angular.copy($scope.ProcessForm.vacationReason.$modelValue),
                            vacationLeft: angular.copy($scope.vacationLeft),
                            ovedNesuBakasha: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.idNumber : null,
                            ovedNesuBakashaName: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.fullName : null,
                            ovedModel: angular.copy($scope.getWSPostModel),
                            targetUser: zehutToTargetUserModel(mishtamesh.idNumber, mishtamesh.fName, mishtamesh.lName, mishtamesh.rashutNumber),
                            tt_tahalich_id: $scope.selectedProcess.id,
                            tt_shalav_id: $scope.nextStepOfCurrent != null ? $scope.nextStepOfCurrent.recId : $scope.currentRunningStep.recId, /*new 18.06.18 was $scope.currentRunningStep.recId,*/
                            curr_mispar_shalav: $scope.currentRunningStep.misparShalav + 1 <= $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length ? $scope.currentRunningStep.misparShalav + 1 : $scope.currentRunningStep.misparShalav,
                            shlavim_counter: $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length
                        }
                        vacationFormDataList.push(vacationFormData);
                    });
                }
                else {
                    _.map($scope.currentMishtamsheiKvuzaOfCurrentStep, function (mishtamesh) {
                        var vacationFormData = {
                            selectedProcess: angular.copy($scope.selectedProcess),
                            datesRange: angular.copy($scope.datesRange),
                            vacationReason: angular.copy($scope.ProcessForm.vacationReason.$modelValue),
                            vacationLeft: angular.copy($scope.vacationLeft),
                            ovedNesuBakasha: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.idNumber : null,
                            ovedNesuBakashaName: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.fullName : null,
                            ovedModel: angular.copy($scope.getWSPostModel),
                            targetUser: zehutToTargetUserModel(mishtamesh.zehut, mishtamesh.shem_prati, mishtamesh.shem_mishpaha, mishtamesh.rashut),
                            tt_tahalich_id: $scope.selectedProcess.id,
                            tt_shalav_id: $scope.nextStepOfCurrent != null ? $scope.nextStepOfCurrent.recId : $scope.currentRunningStep.recId, /*new 18.06.18 was $scope.currentRunningStep.recId,*/
                            curr_mispar_shalav: $scope.currentRunningStep.misparShalav + 1 <= $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length ? $scope.currentRunningStep.misparShalav + 1 : $scope.currentRunningStep.misparShalav,
                            shlavim_counter: $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length
                        }
                        vacationFormDataList.push(vacationFormData);
                    });
                }

                Processes.postEmployeeVacationForm(vacationFormDataList).then(function successCallback(res) {
                    if(res.status == 200) {
                        if(res.data.mailRc > 0 && res.data.processRc > 0) {
                            toastrService.success('בקשתך התקבלה במערכת!');
                            $state.go("Home");
                        }
                        else if(res.data.mailRc == 0) {
                            toastrService.warning("שים לב, מייל לא נשלח כי חסרים נתונים", "בקשתך התקבלה במערכת!");
                            $state.go("Home");
                        }
                        if (res.data.processRc == 0) {
                            toastrService.warning("נסה שוב", "לא נוצר תהליך לניהול המשך הבקשה עקב שגיאה.");
                        }
                    }
                    else {
                        toastrService.warning("אנא נסה שוב", "התרחשה שגיאה במהלך יצירת הבקשה");
                    }
                }, function errorCallback(res) {
                    toastrService.error('', 'תקלת מערכת!');
                });
            }
            else if ($scope.currentRunningStep.rechivId == 3) {//is a course form
                
                if (isEmpty($scope.courseForm.empFirstName)) {
                    $scope.courseForm.empFirstName = $scope.$$credentials.firstName;
                }
                if (isEmpty($scope.courseForm.empLastName)) {
                    $scope.courseForm.empLastName = $scope.$$credentials.lastName;
                }
                if (isEmpty($scope.courseForm.empId)) {
                    $scope.courseForm.empId = $scope.$$credentials.idNumber;
                }

                var courseFormDataList = [];
                if ($scope.currentRunningStep.misparShalav == 1) {
                    _.map($scope.organizationUnitsManagers, function (mishtamesh) {
                        var courseFormData = {
                            selectedProcess: angular.copy($scope.selectedProcess),
                            formFields: angular.copy($scope.courseForm),
                            ovedNesuBakasha: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.idNumber : null,
                            ovedNesuBakashaName: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.fullName : null,
                            ovedModel: angular.copy($scope.getWSPostModel),
                            targetUser: zehutToTargetUserModel(mishtamesh.idNumber, mishtamesh.fName, mishtamesh.lName, mishtamesh.rashutNumber),
                            tt_tahalich_id: $scope.selectedProcess.id,
                            tt_shalav_id: $scope.nextStepOfCurrent != null ? $scope.nextStepOfCurrent.recId : $scope.currentRunningStep.recId, /*new 18.06.18 was $scope.currentRunningStep.recId,*/
                            curr_mispar_shalav: $scope.currentRunningStep.misparShalav + 1 <= $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length ? $scope.currentRunningStep.misparShalav + 1 : $scope.currentRunningStep.misparShalav,
                            shlavim_counter: $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length
                        }
                        courseFormDataList.push(courseFormData);
                    });
                }
                else {
                    _.map($scope.currentMishtamsheiKvuzaOfCurrentStep, function (mishtamesh) {
                        var courseFormData = {
                            selectedProcess: angular.copy($scope.selectedProcess),
                            formFields: angular.copy($scope.courseForm),
                            ovedNesuBakasha: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.idNumber : null,
                            ovedNesuBakashaName: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.fullName : null,
                            ovedModel: angular.copy($scope.getWSPostModel),
                            targetUser: zehutToTargetUserModel(mishtamesh.zehut, mishtamesh.shem_prati, mishtamesh.shem_mishpaha, mishtamesh.rashut),
                            tt_tahalich_id: $scope.selectedProcess.id,
                            tt_shalav_id: $scope.nextStepOfCurrent != null ? $scope.nextStepOfCurrent.recId : $scope.currentRunningStep.recId, /*new 18.06.18 was $scope.currentRunningStep.recId,*/
                            curr_mispar_shalav: $scope.currentRunningStep.misparShalav + 1 <= $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length ? $scope.currentRunningStep.misparShalav + 1 : $scope.currentRunningStep.misparShalav,
                            shlavim_counter: $scope.allProcessesData.processes[$scope.selectedProcessIndex].shlavim.length
                        }
                        courseFormDataList.push(courseFormData);
                    });
                }

                //var courseFormData = {
                //    selectedProcess: angular.copy($scope.selectedProcess),
                //    formFields: angular.copy($scope.courseForm),
                //    //new 30.5.18
                //    ovedNesuBakasha: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.idNumber : 0,
                //    ovedNesuBakashaName: $scope.selectedRepresentWorker.idNumber != '' ? $scope.selectedRepresentWorker.fullName : null,
                //    ovedModel: angular.copy($scope.getWSPostModel),
                //    targetUser: angular.copy($scope.selectedUnitManager)
                //}

                Processes.postEmployeeCourseForm(courseFormDataList).then(function successCallback(res) {
                    if (res.status == 200) {
                        if (res.data.mailRc > 0 && res.data.processRc > 0) {
                            toastrService.success('בקשתך התקבלה במערכת!');
                            $state.go("Home");
                        }
                        else if (res.data.mailRc == 0) {
                            toastrService.warning("שים לב, מייל לא נשלח כי חסרים נתונים", "בקשתך התקבלה במערכת!");
                            $state.go("Home");
                        }
                        if (res.data.processRc == 0) {
                            toastrService.warning("נסה שוב", "לא נוצר תהליך לניהול המשך הבקשה עקב שגיאה.");
                        }
                    }
                    else {
                        toastrService.warning("אנא נסה שוב", "התרחשה שגיאה במהלך יצירת הבקשה");
                    }
                }, function errorCallback(res) {
                    toastrService.error('', 'תקלת מערכת!');
                });
            }
        }
        
        var zehutToTargetUserModel = function (zehut, prati, mishpaha, rashut) {
            return {
                idNumber: zehut,
                fullName: prati + ' ' + mishpaha,
                rashutNumber: rashut
            }
        }

        var reArrangeOrganizationUnitsManagers = function () {
            var temp = angular.copy($scope.organizationUnitsManagers);
            $scope.organizationUnitsManagers = [];
            temp.map(function (currentValue, index, temp) {
                
                if (currentValue.idNumber != $scope.getWSPostModel.calculatedIdNumber) {
                    currentValue = {
                        fullName: currentValue.fName + " " + currentValue.lName,
                        idNumber: currentValue.idNumber,
                        rashutNumber: currentValue.rashutNumber,
                        zehutNatzigKoachAdam: currentValue.zehutNatzigKoachAdam,
                        tekenKoachAdam: currentValue.tekenKoachAdam,
                        tekenMenaelYahidaIrgunit: currentValue.tekenMenaelYahidaIrgunit
                    }
                    $scope.organizationUnitsManagers.push(currentValue);
                }
            });
        }

        var removeDuplicateMngrs = function () {
            if ($scope.organizationUnitsManagers.length > 0) {
                var temp = angular.copy($scope.organizationUnitsManagers);
                $scope.organizationUnitsManagers = new Array();
                temp.map(function (currentValue, index, organizationUnitsManagers) {
                    if (!isExistMngr($scope.organizationUnitsManagers, currentValue)) {
                        currentValue = {

                            fName:currentValue.fName,
                            idNumber:currentValue.idNumber,
                            lName:currentValue.lName,
                            ramaIrgunit:currentValue.ramaIrgunit,
                            rashutNumber: currentValue.rashutNumber,
                            supervisorUnitNumber: currentValue.supervisorUnitNumber,
                            tekenKoachAdam: currentValue.tekenKoachAdam,
                            tekenMenaelYahidaIrgunit: currentValue.tekenMenaelYahidaIrgunit,
                            treeNumber: currentValue.treeNumber,
                            unitDescription: currentValue.unitDescription,
                            unitNumber: currentValue.unitNumber,
                            zehutNatzigKoachAdam: currentValue.zehutNatzigKoachAdam,


                            //fullName: currentValue.fullName,
                            //idNumber: currentValue.idNumber,
                            //rashutNumber: currentValue.rashutNumber,
                            //zehutNatzigKoachAdam: currentValue.zehutNatzigKoachAdam,
                            //tekenKoachAdam: currentValue.tekenKoachAdam,
                            //tekenMenaelYahidaIrgunit: currentValue.tekenMenaelYahidaIrgunit
                        }
                        $scope.organizationUnitsManagers[index] = currentValue;
                    }
                });
            }
        }

        var isExistMngr = function (arr, mngrRec) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].idNumber == mngrRec.idNumber) {
                    return true;
                }
            }
            return false;
        }

        var reArrangeRepresentedWorkers = function () {
            if ($scope.representedWorkers.length > 0) {
                var copyArr = new Array();
                _.map($scope.representedWorkers, function (worker) {
                    if (!isExistWorker(copyArr, worker.idNumber)) {
                        copyArr.push(worker);
                    }
                    //below add the represented workers to the users list, to be displayed inside employee filter element
                    if ($scope.usersList.length > 0) {
                        $scope.usersList.push(
                            {
                                id: worker.idNumber,
                                rashut: "",
                                kodMedaveach: -1,
                                user: {
                                    lastName: worker.fullName.substr(0, worker.fullName.indexOf(' ')),
                                    firstName: worker.fullName.substr(worker.fullName.indexOf(' ') + 1)
                                }
                            }
                        );
                    }
                });
                removeDuplicatesFromUsersList();

                $scope.representedWorkers = new Array();
                $scope.representedWorkers = angular.copy(copyArr);
            }
        }

        var isExistWorker = function (arr, workerId) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].idNumber == workerId) {
                    return true;
                }
            }
            return false;
        }

        var reArrangeVacationData = function () {
            if($scope.vacationData.length > 0) {
                $scope.vacationData[0].Data.filter(function (row) {
                    $scope.vacationAmount.push(row.sort(function (a, b) { return (a.columnNumber > b.columnNumber); }));
                });
                $scope.vacationLeft = $scope.vacationAmount[$scope.vacationAmount.length - 1][4].columnNumber == 4 ? $scope.vacationAmount[$scope.vacationAmount.length - 1][4].value : "אין מידע";
            }
        }

        var isExist = function (arr, value) {
            for (var i = 0; i < arr.length; i++ ) {
                if(arr[i].recId == value.recId) {
                    return true;
                }
            }
            return false;
        }

        var isEmpty = function (str) {
            return str == "" || str == '' || str == null || str == undefined;
        }
        
        var getSuitableRequestProcessStep = function (requestProcessItem) {//הבקשות שהופנו אלי
            _.map($scope.allProcessesData.processes, function (process) {
                if (process.recId == requestProcessItem.TT_TAHALICH_ID)
                    _.map(process.shlavim, function (step) {
                        //NEW - get the next step mishtamshim (if current shalav is 2 so get mishtamshim of shalav 3)
                        var misparShalav = requestProcessItem.CURR_MISPAR_SHALAV + 1 < requestProcessItem.SHLAVIM_COUNTER ? requestProcessItem.CURR_MISPAR_SHALAV + 1 : requestProcessItem.SHLAVIM_COUNTER;
                        if (step.misparShalav == misparShalav)
                            $scope.stepToProcess.push(step);
                    });
            });
        }

        var setMenaalimYeshirimAsMishtamsheiKvuza = function (menaalimYeshirim) {
            if (menaalimYeshirim.length <= 0) {
                return [];
            }
            var temp = new Array();
            for (var i = 0; i < menaalimYeshirim.length; i++) {
                temp.push(
                    {
                        shem_prati: menaalimYeshirim[i].fName,
                        shem_mishpaha: menaalimYeshirim[i].lName,
                        idNumber: menaalimYeshirim[i].idNumber
                    }
                );
            }
            return temp;
        }

        var removeDuplicatesInMenaalimYeshirim = function (menaalimYeshirim) {
            var temp = menaalimYeshirim;
            menaalimYeshirim = [];
            _.map(temp, function (menael, index) {
                if (!isExistMenael(menael, menaalimYeshirim)) {
                    menaalimYeshirim.push(
                        {
                            shem_prati: menael.shem_prati,
                            shem_mishpaha: menael.shem_mishpaha,
                            idNumber: menael.idNumber
                        }
                    );
                }
            });
            return menaalimYeshirim;
        }

        var isExistMenael = function (menael, menaalimYeshirim) {
            if(menaalimYeshirim.length <= 0)
                return false;
            for (var i = 0; i < menaalimYeshirim.length; i++) {
                if (menaalimYeshirim[i].idNumber == menael.idNumber) {
                    return true;
                }
            }
            return false;
        }

        var removeDuplicatesFromUsersList = function () {
            var temp = angular.copy($scope.usersList);
            $scope.usersList = [];
            _.map(temp, function (user, index) {
                if (!isExistUser(user, $scope.usersList)) {
                    $scope.usersList.push(user);
                }
            });
        }

        var isExistUser = function (user, users) {
            if (users.length <= 0)
                return false;
            for (var i = 0; i < users.length; i++) {
                if (users[i].user.idNumber == user.user.idNumber) {
                    return true;
                }
            }
            return false;
        }

        var setMishtamsheiKvuzaPerProcess = function (stepOfReqProcess, index) {
            //KVUZA REGILA MNGRS
            if (stepOfReqProcess.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_REGILA) {
                Processes.getMishtamsheiKvuzaRegila($scope.getWSPostModel, stepOfReqProcess.kvuzatMishtamshimModel.mishtamsheiKvuzaModel).then(function successCallback(res) {
                    if (res.status == 200) {
                        $scope.mishtamsheiKvuzaPerProcess[index] = res.data;
                        reArrangeMishtamsehiKvuzaRegilaInMyProcessesTab(index);//to be the same structure as kvuza dinamit
                    }
                    else {
                        $scope.mishtamsheiKvuzaPerProcess[index] = {};
                    }
                }, function errorCallback(res) {
                    $scope.mishtamsheiKvuzaPerProcess[index] = {};
                });
            }
            //DKVUZA DINAMIT MNGRS
            else if (stepOfReqProcess.kvuzatMishtamshimModel.kvuzaType == KVUZOT_ENUM.KVUZAT_MISHTAMSHIM_DINAMIM) {
                if(stepOfReqProcess.kvuzatMishtamshimModel.mishtamsheiKvuzaModel[0].mishtameshId == 2) {//if it is menaalim regilim
                    Processes.getUserManagers($scope.currentYehidaIrgunit, $scope.getWSPostModel).then(function successCallback(res) {
                        
                        $scope.mishtamsheiKvuzaPerProcess[index] = setMenaalimYeshirimAsMishtamsheiKvuza(res.data);
                        $scope.mishtamsheiKvuzaPerProcess[index] = removeDuplicatesInMenaalimYeshirim($scope.mishtamsheiKvuzaPerProcess[index]);
                    }, function errorCallback(res) {
                        
                    });
                }
                else {
                    Processes.getMishtamsheiKvuzaDinamit($scope.getWSPostModel, stepOfReqProcess.kvuzatMishtamshimModel.mishtamsheiKvuzaModel).then(function successCallback(res) {
                    if (res.status == 200) {
                        $scope.mishtamsheiKvuzaPerProcess[index] = res.data;
                    }
                    else {
                        $scope.mishtamsheiKvuzaPerProcess[index] = {};
                    }
                }, function errorCallback(res) {
                    $scope.mishtamsheiKvuzaPerProcess[index] = {};
                });
                }
            }
        }

        $scope.amITargetUser = function (targetUserId) {

            Processes.getMyRecIdFromUsers($scope.getWSPostModel).then(function successCallback(res) {
                 
                return res.data.ID == targetUserId;
                
            }, function errorCallback(res) {
                return false;
            });
        }

        $scope.amIPartOfProcess = function(process) {
            return process.TARGET_USER.calculatedIdNumber == $scope.getWSPostModel.calculatedIdNumber;
        }
        
        //object that hold the current user details
        $scope.getWSPostModel = $rootScope.getWSPostModel();

        //originally arrived as a promise that resolved in route_config file
        $scope.processesData = ProcessesData.data.length > 0 ? ProcessesData.data : {};
        $scope.allMyProcessesData = MyProcesses.data.length > 0 ? MyProcesses.data : {};
        $scope.usersList = UsersList.data.length > 0 ? UsersList.data : {};
        $scope.vacationData = VacationData.data.length > 0 ? VacationData.data : {};
        $scope.academicInsts = AcademicInst.data.length > 0 ? AcademicInst.data : {};
        $scope.currentYehidaIrgunit = YehidaIrgunit.data.length > 0 ? YehidaIrgunit.data : {};
        $scope.myRequestProcessesData = MyRequestProcesses.data.length > 0 ? MyRequestProcesses.data : {};

        $scope.allProcessesData = {
            processes: []
        }

        if ($scope.processesData.length > 0) {
            $scope.allProcessesData.processes = $scope.processesData;
        }
        if ($scope.allMyProcessesData.length > 0) {
            $scope.filters.selectedStatusFilter = 0;
            $scope.filterMyProcesses('status');//set the default filter viewed

            //NEW 18.06.18 - create a suitable array that hold the correct step of the process using index (בקשות שהופנו אלי)
            $scope.stepToProcess = [];
            if ($scope.processesData.length > 0) {
                _.map($scope.allMyProcessesData, function (ReqProcess) {
                    getSuitableRequestProcessStep(ReqProcess);
                });

                //get the mishtamshei kvuza for each process 
                if ($scope.stepToProcess.length == $scope.allMyProcessesData.length) {//must happen!
                    $scope.mishtamsheiKvuzaPerProcess = [];
                    _.map($scope.allMyProcessesData, function (process, index) {
                        setMishtamsheiKvuzaPerProcess($scope.stepToProcess[index], index);
                    });
                }
            }
        }
        else {
            $scope.myProcessesIsEmpty = true;
        }
        if ($scope.vacationData.length > 0) {
            reArrangeVacationData();
        }

        //NEW FOR YEHIDOT IRGUNIYOT MNGRS
        if ($scope.currentYehidaIrgunit.length > 0) {
            //bolow request ask if the wspostmodel is a secretary and if so, fill represented workers list. if not, the returned list is empty
            Processes.getRepresentedWorkers($scope.currentYehidaIrgunit, $scope.getWSPostModel).then(function successCallback(res) {
                if(res.data.length > 0) {
                    $scope.representedWorkers = res.data;
                    reArrangeRepresentedWorkers();
                }
            }, function errorCallback(res) {
                
            });
        }
        
        if ($scope.usersList.length > 0) {
            removeDuplicatesFromUsersList();
        }

        //new for הבקשות שלי
        if ($scope.myRequestProcessesData.length > 0) {
            $scope.filterMyProcesses('all');
        }
        else {
            $scope.myRequestProcessesIsEmpty = true;
        }
    }
})();

(function () {
    angular.module('WorkerPortfolioApp').controller('StatisticsController', StatisticsController);

    StatisticsController.$inject = ['$scope', '$rootScope', '$window', '$timeout', 'toastrService', 'FileSaver', 'Admin', 'ValidAdminUser', 'municipalities', 'statistics', '$controllerState'];

    function StatisticsController($scope, $rootScope, $window, $timeout, toastrService, FileSaver, Admin, ValidAdminUser, municipalities, statistics, $controllerState) {

        if (ValidAdminUser.data !== true || ValidAdminUser.status !== 200) {
            $state.go('Home');
            toastrService.info('', 'הרשאה לא תקינה!', { onHidden: function () { $state.go('Home'); } });
            return void [0];
        }

        $scope.controllerState = $controllerState;

        $scope.WSPostModel = $rootScope.getWSPostModel(true); $rootScope.getWSPostModel(null, null, null, null, 'EMAILPAYCHECKROLE')
        $scope.WSPostModel.isMobileRequest = $scope.isMobile;

        var _requiredRole = 'SITEADMIN';
        switch ($scope.controllerState) {
            case 'EmailPaycheck':
                _requiredRole = 'EMAILPAYCHECKROLE';
                break;
            case '101Form':
            case 'LogInLog':
            case 'DailyUsage':
                _requiredRole = 'SITEADMIN';
                break;
        }

        $scope.fileNameFunction = function () {
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

            return String(monthNames[$scope.selectedMonth.getMonth()]).toUpperCase() + '_' + String($scope.selectedMonth.getFullYear()) + '_ARCHIVE_' + String($scope.WSPostModel.currentRashutId) + '.xlsx';
        }
        var parseDate = function (date, isFull) {
            if (isFull !== true)
                isFull = false;
            if (isFull) {
                ///day              ///month (add 1)            ///year
                return [pad(date.getDate()), pad((date.getMonth() + 1)), date.getFullYear()].join('/') + ' ' + [pad(date.getHours()), pad(date.getMinutes())].join(':');
            }
            return [pad(date.getDate()), pad((date.getMonth() + 1)), date.getFullYear()].join('/');
        }
        var pad = function (num) {
            if (num <= 9) {
                return '0' + num.toString();
            } else {
                return num.toString();
            }
        }
        var dateRangeStart = function () {
            var base = new Date(), s = new Date(base.getFullYear(), base.getMonth(), 1), e = new Date(base.getFullYear(), base.getMonth() + 1, 1);
            return [parseDate(s), parseDate(e), s, e];
        }

        $scope.isMobile = ($window.innerWidth < 801);

        $scope.pcueList = new Array();
        $scope.yearsList = new Array();
        $scope.pcueHolderList = new Array();
        $scope.Search = {
            Name: '',
            Email: '',
            RegistrationDate: '',
            UpdateDate: '',
            IsActive: 0,
            WasSent: 0,
            total: 0,
            IdNumber: '',
            Year: '0',
            MunicipalityIsActive: null,
            ActiveForm: null,
            T_Cause: 0,
            T_Reason: 0,
            T_Date: 0
        };

        $scope.MunicipalitiesList = $.grep(municipalities.data, function (municipality) {
            return municipality.active;
        });

        $scope.selectedMunicipality = undefined;
        $scope.$SM = {
            selectedList: new Array()
        }

        $scope.selectedMunicipalityCallback = function (Selected) {
            $scope.selectedMunicipality = Selected !== undefined ? (Selected.description !== undefined ? Selected.description.ID : Selected.originalObject.ID) : undefined;
            if (Selected !== undefined && Selected.description !== undefined) {
                $scope.pcueList = new Array();
                $scope.pcueHolderList = new Array();
                $scope.$Pager = {
                    PageSize: 25,
                    PageNumber: 1,
                    Counter: 0,
                    MaxPages: 0,
                    pagerList: []
                }
            }
        }

        $scope.selectedMonth = $scope.WSPostModel.selectedDate;
        $scope.selectedMonth.setDate($scope.selectedMonth.getMonth() + 1);

        $scope.InitialValue = $.grep($scope.MunicipalitiesList, function (item) {
            return item.ID == $scope.WSPostModel.currentMunicipalityId;
        })[0];
        $scope.disableInput = $scope.MunicipalitiesList.length < 2;

        $timeout(function () {

            $('#Month').MonthPicker({
                MaxMonth: 0, IsRTL: true, UseInputMask: true, ShowOn: 'both', buttonText: 'בחר חודש',
                OnAfterChooseMonth: function (selectedDate) {
                    selectedDate.setDate(15);
                    $scope.selectedMonth = selectedDate;
                    $scope.UserSearch.searchDate = selectedDate;
                }
            });

        }, 55);

        $scope.UserSearch = {
            idNumber: $scope.WSPostModel.idNumber,
            confirmationToken: $scope.WSPostModel.confirmationToken,
            page: 1,
            hidden_value: false,
            $hidden_value: '',
            searchIdNumber: null,
            cell: null,
            firstName: null,
            lastName: null,
            password: null,
            newPassword: null,
            reEnterPassword: null,
            municipalities: null,
            userToken: null,
            searchMunicipalityName: null,
            searchMunicipalitiesId: $scope.InitialValue.ID,
            searchDate: $scope.selectedMonth,
            endSearchDate: $scope.selectedMonth,
            requiredRole: _requiredRole,
        };

        if ($scope.controllerState === 'DailyUsage') {
            var r = dateRangeStart();
            $scope.UserSearch.searchDate = r[2];
            $scope.UserSearch.endSearchDate = r[3];

            $timeout(function () {

                $('input[name="daterange"]').daterangepicker({
                    "showCustomRangeLabel": false,
                    "timePicker": false,
                    "startDate": r[0],
                    "endDate": r[1],
                    "minDate": "01/01/2010",
                    "maxDate": "01/01/2050",
                    "opens": "center",
                    "autoApply": true,
                    "autoUpdateInput": true,

                    "locale": {
                        "format": "DD/MM/YYYY",
                        "separator": " - ",
                        "applyLabel": "החל",
                        "cancelLabel": "בטל",
                        "fromLabel": "מ-",
                        "toLabel": "עד-",
                        "customRangeLabel": "Custom",
                        "weekLabel": "ש",
                        "daysOfWeek": [
                            "א'",
                            "ב'",
                            "ג'",
                            "ד'",
                            "ה'",
                            "ו'",
                            "שבת"
                        ],
                        "monthNames": [
                            "ינואר",
                            "פברואר",
                            "מרץ",
                            "אפריל",
                            "מאי",
                            "יוני",
                            "יולי",
                            "אוגוסט",
                            "ספטמבר",
                            "אוקטובר",
                            "נובמבר",
                            "דצמבר"
                        ],
                        "firstDay": 0
                    },

                }, function (start, end, label) {
                    $scope.UserSearch.searchDate = start._d;
                    $scope.UserSearch.endSearchDate = end._d;
                });

            }, 55);

        }

        ///pager
        $scope.$Pager = {
            PageSize: 25,
            PageNumber: 1,
            Counter: 0,
            MaxPages: 0,
            pagerList: []
        }

        $scope.GetStatistics = function () {
            $scope.municipality_hidden_value = null;
            $scope.StatisticsForm.hidden_value.$setValidity('required', false);
            if ($scope.selectedMunicipality !== undefined) {
                $scope.municipality_hidden_value = 1;
                $scope.StatisticsForm.hidden_value.$setValidity('required', true);
            }

            if ($scope.StatisticsForm.$invalid)
                return void [0];

            $scope.$Pager.PageNumber = 1;
            $scope.Search = {
                Name: '',
                Email: '',
                RegistrationDate: '',
                UpdateDate: '',
                IsActive: 0,
                WasSent: 0,
                total: 0,
                IdNumber: '',
                Year: '0',
                MunicipalityIsActive: null,
                ActiveForm: null,
                T_Cause: 0,
                T_Reason: 0,
                T_Date: 0
            };

            $scope.UserSearch.searchMunicipalitiesId = $scope.selectedMunicipality;

            Admin.GetUEPStatistics($scope.UserSearch)
                .then(function successCallback(res) {
                    $scope.pcueList = res.data;

                    $scope.$Pager.Counter = res.data.length;
                    $scope.$Pager.MaxPages = Math.ceil(res.data.length / $scope.$Pager.PageSize);
                    var num = 1;

                    $scope.$Pager.pagerList = [];
                    for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                        $scope.$Pager.pagerList.push(1 + i);
                    }

                    $.map($scope.pcueList, function (item) {
                        item._createdDate = item.createdDate != null ? parseDate(new Date(item.createdDate), (item.updateDate != null && item.updateDate !== item.createdDate)) : null;
                        item.createdDate = item.createdDate != null ? new Date(item.createdDate) : null;

                        item._updateDate = item.updateDate != null && item.createdDate != null && item.updateDate !== item.createdDate ? parseDate(new Date(item.updateDate), true) : null;
                        item.updateDate = item.updateDate != null ? new Date(item.updateDate) : null;

                        item._senddate = item.sendDate != null ? parseDate(new Date(item.sendDate), true) : null;
                        item.sendDate = item.sendDate != null ? new Date(item.sendDate) : null;

                        item._terminationDate = item.terminationDate != null ? parseDate(new Date(item.terminationDate), false) : null;
                        item.terminationDate = item.terminationDate != null ? new Date(item.terminationDate) : null;
                    });
                    $scope.pcueHolderList = angular.copy($scope.pcueList);

                }, function errorCallback(res) {
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.FilterStatistics = function () {
            $scope.pcueList = angular.copy($scope.pcueHolderList);

            ///filter by active
            if ($scope.Search.IsActive !== 0)
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    switch ($scope.Search.IsActive) {
                        case 0:///all
                            return true;
                        case 1:///no
                            return !item.activeRecord
                        case 2:///yes
                            return item.activeRecord
                        default:
                            return true;
                    }
                });

            ///filter by sent mail
            if ($scope.Search.WasSent !== 0)
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    switch ($scope.Search.WasSent) {
                        case 0:
                            return true;
                        case 1:
                            return item.sendDate === null;
                        case 2:
                            return item.sendDate !== null;
                        default:
                            return true;
                    }
                });

            ///filter by name
            if ($scope.Search.Name !== null && $scope.Search.Name.length > 0) {
                var regex = RegExp($scope.Search.Name);
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    return regex.test(item.user_Name);
                });
            }

            ///filter by email
            if ($scope.Search.Email !== null && $scope.Search.Email.length > 0) {
                var regex = RegExp($scope.Search.Email);
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    return regex.test(item.email);
                });
            }

            ///filter by id number
            if ($scope.Search.IdNumber !== null && $scope.Search.IdNumber.length > 0) {
                var regex = RegExp($scope.Search.IdNumber);
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    return regex.test(item.idNumber);
                });
            }

            $scope.$Pager.PageNumber = 1;
            $scope.$Pager.Counter = $scope.pcueList.length;
            $scope.$Pager.MaxPages = Math.ceil($scope.pcueList.length / $scope.$Pager.PageSize);
            var num = 1;

            $scope.$Pager.pagerList = [];
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                $scope.$Pager.pagerList.push(1 + i);
            }
        }
        $scope.selectTotal = function () {
            $scope.reSetOtherVariables('total');
            switch ($scope.Search.total) {
                case 0:
                    $scope.Search.total = 1;
                    break;
                case 1:
                    $scope.Search.total = 2;
                    break;
                case 2:
                    $scope.Search.total = 0;
                    break;
                default:
                    $scope.Search.total = 0;
            }
            switch ($scope.controllerState) {
                case '101Form':
                case 'LogInLog':
                    $scope.Filter101Statistics();
                    break;
                case 'EmailPaycheck':
                    $scope.FilterStatistics();
                    break;
                case 'DailyUsage':
                    $scope.FilterDailyUsage();
                    break;
            }
        }
        $scope.selectActive = function () {
            $scope.reSetOtherVariables('IsActive');
            switch ($scope.Search.IsActive) {
                case 0:
                    $scope.Search.IsActive = 1;
                    break;
                case 1:
                    $scope.Search.IsActive = 2;
                    break;
                case 2:
                    $scope.Search.IsActive = 0;
                    break;
                default:
                    $scope.Search.IsActive = 0;
            }
            switch ($scope.controllerState) {
                case '101Form':
                case 'LogInLog':
                    $scope.Filter101Statistics();
                    break;
                case 'EmailPaycheck':
                    $scope.FilterStatistics();
                    break;
                case 'DailyUsage':
                    $scope.FilterDailyUsage();
                    break;
            }
        }
        $scope.selectSent = function () {
            $scope.reSetOtherVariables('WasSent');
            switch ($scope.Search.WasSent) {
                case 0:
                    $scope.Search.WasSent = 1;
                    break;
                case 1:
                    $scope.Search.WasSent = 2;
                    break;
                case 2:
                    $scope.Search.WasSent = 0;
                    break;
                default:
                    $scope.Search.WasSent = 0;
            }
            switch ($scope.controllerState) {
                case '101Form':
                case 'LogInLog':
                    $scope.Filter101Statistics();
                    break;
                case 'EmailPaycheck':
                    $scope.FilterStatistics();
                    break;
                case 'DailyUsage':
                    $scope.FilterDailyUsage();
                    break;
            }
        }
        $scope.Filter101Statistics = function () {
            var tval;
            $scope.pcueList = angular.copy($scope.pcueHolderList);
            ///filter by name
            if ($scope.Search.Name !== null && $scope.Search.Name.length > 0) {
                var regex = RegExp($scope.Search.Name);
                if ($scope.controllerState === '101Form') {
                    $scope.pcueList = $.grep($scope.pcueList, function (item) {
                        return regex.test(item.Name) || regex.test(item.municipalityId);
                    });
                }
                if ($scope.controllerState === 'LogInLog') {
                    $scope.pcueList = $.grep($scope.pcueList, function (item) {
                        return regex.test(item.name) || regex.test(item.id);
                    });
                }
            }

            ///filter 101 forms by year
            if ($scope.Search.Year !== '0') {
                var val = parseInt($scope.Search.Year);
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    return item.Form_Year === val;
                });
            }

            ///filter by active municipality
            if ($scope.Search.MunicipalityIsActive !== null) {
                tval = $scope.Search.MunicipalityIsActive ? 1 : 0;
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    return item.Active === tval;
                });
            }

            ///filter by active 101 form
            if ($scope.Search.ActiveForm !== null) {
                tval = $scope.Search.ActiveForm ? 1 : 0;
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    return item.Allow101 === tval;
                });
            }

            ///order by total forms
            if ($scope.Search.total > 0) {
                switch ($scope.Search.total) {
                    case 1:///desc
                        $scope.pcueList.sort(function (a, b) {
                            return a.prospective_users < b.prospective_users ? 1 : a.prospective_users > b.prospective_users ? -1 : 0
                        });
                        break;
                    case 2:///asc
                        $scope.pcueList.sort(function (a, b) {
                            return a.prospective_users > b.prospective_users ? 1 : a.prospective_users < b.prospective_users ? -1 : 0
                        });
                        break;
                }
            }
            ///order by good forms
            if ($scope.Search.IsActive > 0) {
                switch ($scope.Search.IsActive) {
                    case 1:///desc
                        if ($scope.controllerState === '101Form') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.Oks < b.Oks ? 1 : a.Oks > b.Oks ? -1 : 0
                            });
                        }
                        if ($scope.controllerState === 'LogInLog') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.active_users < b.active_users ? 1 : a.active_users > b.active_users ? -1 : 0
                            });
                        }
                        break;
                    case 2:///asc
                        if ($scope.controllerState === '101Form') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.Oks > b.Oks ? 1 : a.Oks < b.Oks ? -1 : 0
                            });
                        }
                        if ($scope.controllerState === 'LogInLog') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.active_users > b.active_users ? 1 : a.active_users < b.active_users ? -1 : 0
                            });
                        }
                        break;
                }
            }
            ///order by bad forms
            if ($scope.Search.WasSent > 0) {
                switch ($scope.Search.WasSent) {
                    case 1:///desc
                        if ($scope.controllerState === '101Form') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.Not_Ok < b.Not_Ok ? 1 : a.Not_Ok > b.Not_Ok ? -1 : 0
                            });
                        }
                        if ($scope.controllerState === 'LogInLog') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.usegPercentage < b.usegPercentage ? 1 : a.usegPercentage > b.usegPercentage ? -1 : 0
                            });
                        }
                        break;
                    case 2:///asc
                        if ($scope.controllerState === '101Form') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.Not_Ok > b.Not_Ok ? 1 : a.Not_Ok < b.Not_Ok ? -1 : 0
                            });
                        }
                        if ($scope.controllerState === 'LogInLog') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.usegPercentage > b.usegPercentage ? 1 : a.usegPercentage < b.usegPercentage ? -1 : 0
                            });
                        }
                        break;
                }
            }
            ///order by terminationCause
            if ($scope.Search.T_Cause > 0) {
                switch ($scope.Search.T_Cause) {
                    case 1:///desc
                        $scope.pcueList.sort(function (a, b) {
                            return a.terminationCause < b.terminationCause ? 1 : a.terminationCause > b.terminationCause ? -1 : 0
                        });
                        break;
                    case 2:///asc
                        $scope.pcueList.sort(function (a, b) {
                            return a.terminationCause > b.terminationCause ? 1 : a.terminationCause < b.terminationCause ? -1 : 0
                        });
                        break;
                }
            }
            ///order by terminationDate
            if ($scope.Search.T_Date > 0) {
                switch ($scope.Search.T_Date) {
                    case 1:///desc
                        $scope.pcueList.sort(function (a, b) {
                            return a.terminationDate < b.terminationDate ? 1 : a.terminationDate > b.terminationDate ? -1 : 0
                        });
                        break;
                    case 2:///asc
                        $scope.pcueList.sort(function (a, b) {
                            return a.terminationDate > b.terminationDate ? 1 : a.terminationDate < b.terminationDate ? -1 : 0
                        });
                        break;
                }
            }

            $scope.$Pager.PageNumber = 1;
            $scope.$Pager.Counter = $scope.pcueList.length;
            $scope.$Pager.MaxPages = Math.ceil($scope.pcueList.length / $scope.$Pager.PageSize);
            var num = 1;

            $scope.$Pager.pagerList = [];
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                $scope.$Pager.pagerList.push(1 + i);
            }
        }
        $scope.FilterDailyUsage = function () {
            $scope.pcueList = angular.copy($scope.pcueHolderList);
            ///filter by name
            if ($scope.Search.Name !== null && $scope.Search.Name.length > 0) {
                var regex = RegExp($scope.Search.Name);
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    return regex.test(item.name) || regex.test(item.id);
                });
            }
            ///order by totalUsersCounter
            switch ($scope.Search.total) {
                case 1:///desc
                    $scope.pcueList.sort(function (a, b) {
                        return a.totalUsersCounter < b.totalUsersCounter ? 1 : a.totalUsersCounter > b.totalUsersCounter ? -1 : 0
                    });
                    break;
                case 2:///asc
                    $scope.pcueList.sort(function (a, b) {
                        return a.totalUsersCounter > b.totalUsersCounter ? 1 : a.totalUsersCounter < b.totalUsersCounter ? -1 : 0
                    });
                    break;
            }
            ///order by dailyUsage
            switch ($scope.Search.WasSent) {
                case 1:///desc
                    $scope.pcueList.sort(function (a, b) {
                        return a.dailyUsage < b.dailyUsage ? 1 : a.dailyUsage > b.dailyUsage ? -1 : 0
                    });
                    break;
                case 2:///asc
                    $scope.pcueList.sort(function (a, b) {
                        return a.dailyUsage > b.dailyUsage ? 1 : a.dailyUsage < b.dailyUsage ? -1 : 0
                    });
                    break;
            }
            ///order by dailyUsage
            switch ($scope.Search.IsActive) {
                case 1:///desc
                    $scope.pcueList.sort(function (a, b) {
                        return a.actionDate < b.actionDate ? 1 : a.actionDate > b.actionDate ? -1 : 0
                    });
                    break;
                case 2:///asc
                    $scope.pcueList.sort(function (a, b) {
                        return a.actionDate > b.actionDate ? 1 : a.actionDate < b.actionDate ? -1 : 0
                    });
                    break;
            }

            $scope.$Pager.PageNumber = 1;
            $scope.$Pager.Counter = $scope.pcueList.length;
            $scope.$Pager.MaxPages = Math.ceil($scope.pcueList.length / $scope.$Pager.PageSize);
            var num = 1;

            $scope.$Pager.pagerList = [];
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                $scope.$Pager.pagerList.push(1 + i);
            }
        }
        $scope.FilterActiveMunicipality = function (val) {
            $scope.Search.MunicipalityIsActive = val;
            $scope.Search.ActiveForm = null;
            $scope.Filter101Statistics();
        }
        $scope.FilterActiveForm = function (val) {
            $scope.Search.ActiveForm = val;
            $scope.Search.MunicipalityIsActive = null;
            $scope.Filter101Statistics();
        }
        $scope.selectCause = function () {
            $scope.reSetOtherVariables('T_Cause');
            switch ($scope.Search.T_Cause) {
                case 0:
                    $scope.Search.T_Cause = 1;
                    break;
                case 1:
                    $scope.Search.T_Cause = 2;
                    break;
                case 2:
                    $scope.Search.T_Cause = 0;
                    break;
                default:
                    $scope.Search.T_Cause = 0;
            }
            $scope.Filter101Statistics();
        }
        $scope.selectReason = function () { }
        $scope.selectDate = function () {
            $scope.reSetOtherVariables('T_Date');
            switch ($scope.Search.T_Date) {
                case 0:
                    $scope.Search.T_Date = 1;
                    break;
                case 1:
                    $scope.Search.T_Date = 2;
                    break;
                case 2:
                    $scope.Search.T_Date = 0;
                    break;
                default:
                    $scope.Search.T_Date = 0;
            }
            $scope.Filter101Statistics();
        }

        $scope.reSetOtherVariables = function (Me) {
            switch (Me) {
                case 'IsActive':
                    if ($scope.controllerState !== "EmailPaycheck")
                        $scope.Search.WasSent = 0;
                    $scope.Search.total = 0;
                    $scope.Search.T_Cause = 0;
                    $scope.Search.T_Reason = 0;
                    $scope.Search.T_Date = 0;
                    break;
                case 'total':
                    $scope.Search.IsActive = 0;
                    $scope.Search.WasSent = 0;
                    $scope.Search.T_Cause = 0;
                    $scope.Search.T_Reason = 0;
                    $scope.Search.T_Date = 0;
                    break;
                case 'WasSent':
                    if ($scope.controllerState !== "EmailPaycheck")
                        $scope.Search.IsActive = 0;
                    $scope.Search.total = 0;
                    $scope.Search.T_Cause = 0;
                    $scope.Search.T_Reason = 0;
                    $scope.Search.T_Date = 0;
                    break;
                case 'T_Cause':
                    $scope.Search.IsActive = 0;
                    $scope.Search.total = 0;
                    $scope.Search.WasSent = 0;
                    $scope.Search.T_Reason = 0;
                    $scope.Search.T_Date = 0;
                    break;
                case 'T_Reason':
                    $scope.Search.IsActive = 0;
                    $scope.Search.total = 0;
                    $scope.Search.WasSent = 0;
                    $scope.Search.T_Cause = 0;
                    $scope.Search.T_Date = 0;
                    break;
                case 'T_Date':
                    $scope.Search.IsActive = 0;
                    $scope.Search.total = 0;
                    $scope.Search.WasSent = 0;
                    $scope.Search.T_Cause = 0;
                    $scope.Search.T_Reason = 0;
                    break;

                default:
                    $scope.Search.IsActive = 0;
                    $scope.Search.total = 0;
                    $scope.Search.WasSent = 0;
                    $scope.Search.T_Cause = 0;
                    $scope.Search.T_Reason = 0;
                    $scope.Search.T_Date = 0;
                    break;
            }
        }

        if ($scope.controllerState === '101Form' || $scope.controllerState === 'LogInLog') {
            $scope.pcueList = statistics.data;
            $scope.$Pager.Counter = $scope.pcueList.length;
            $scope.$Pager.MaxPages = Math.ceil($scope.pcueList.length / $scope.$Pager.PageSize);
            var num = 1;

            $scope.$Pager.pagerList = [];
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                $scope.$Pager.pagerList.push(1 + i);
            }
            ///usegPercentage
            if ($scope.controllerState === 'LogInLog') {
                $.map($scope.pcueList, function (item) {
                    item.usegPercentage = parseFloat(((item.active_users / item.prospective_users) * 100).toFixed(2));
                });
            }

            $scope.pcueHolderList = angular.copy($scope.pcueList);

            var list = new Array();
            $.map($scope.pcueList, function (item) {
                if (list.indexOf(item.Form_Year) < 0) {
                    list.push(item.Form_Year);
                    $scope.yearsList.push({ name: item.Form_Year, id: item.Form_Year });
                }
            });
            if (list.length <= 1)
                $scope.yearsList = new Array();
        }

        $scope.selectMulti = function () {
            $scope.StatisticsForm.hidden_value.$setValidity('required', true);
            $scope.StatisticsForm.hidden_value.$setValidity('custom', true);
            if ($scope.$SM.selectedList.length >= 9) {
                $scope.municipality_hidden_value = 9;
                $scope.StatisticsForm.hidden_value.$setDirty();
                $scope.StatisticsForm.hidden_value.$setValidity('custom', false);
            }
        }

        $scope.GetDailyUsageStatistics = function () {
            $scope.StatisticsForm.hidden_value.$setDirty();
            $scope.municipality_hidden_value = null;

            $scope.StatisticsForm.hidden_value.$setValidity('required', false);
            if ($scope.$SM.selectedList != undefined && $scope.$SM.selectedList.length >= 1) {
                $scope.municipality_hidden_value = 1;
                $scope.StatisticsForm.hidden_value.$setValidity('required', true);
            }
            $scope.StatisticsForm.hidden_value.$setValidity('custom', true);
            if ($scope.$SM.selectedList.length > 9) {
                $scope.municipality_hidden_value = 9;
                $scope.StatisticsForm.hidden_value.$setDirty();
                $scope.StatisticsForm.hidden_value.$setValidity('custom', false);
            }

            if ($scope.StatisticsForm.$invalid)
                return void [0];

            $scope.$Pager.PageNumber = 1;
            $scope.Search = {
                Name: '',
                Email: '',
                RegistrationDate: '',
                UpdateDate: '',
                IsActive: 0,
                WasSent: 0,
                total: 0,
                IdNumber: '',
                Year: '0',
                MunicipalityIsActive: null,
                ActiveForm: null,
                T_Cause: 0,
                T_Reason: 0,
                T_Date: 0
            };

            var _l = [];
            $.map($scope.$SM.selectedList, function (item) {
                _l.push(item.ID);
            });
            $scope.UserSearch.searchMunicipalitiesId = _l.join(',');

            Admin.GetDailyUsagetatistics($scope.UserSearch)
                .then(function successCallback(res) {
                    $scope.pcueList = res.data;

                    $scope.$Pager.Counter = res.data.length;
                    $scope.$Pager.MaxPages = Math.ceil(res.data.length / $scope.$Pager.PageSize);
                    var num = 1;

                    $scope.$Pager.pagerList = [];
                    for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                        $scope.$Pager.pagerList.push(1 + i);
                    }

                    $.map($scope.pcueList, function (item) {
                        item._actionDate = parseDate(new Date(item.actionDate), false);
                    });
                    $scope.pcueHolderList = angular.copy($scope.pcueList);

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.Export = function () {
            var dataObj = [];
            $.map($scope.pcueList, function (item) {
                dataObj.push({
                    'תעודת זהות': item.idNumber,
                    'שם מלא': item.user_Name,
                    'כתובת מייל': item.email,
                    'תאריך רישום': item._createdDate,
                    'תאריך עדכון': item._updateDate,
                    'אישור תלוש במייל': item.activeRecord ? 1 : 0,
                    'תאריך שליחה': item._senddate,
                    'מקור רישום': item.creationSource !== null ? (item.creationSource ? 'מכלול' : 'תיק עובד') : null,
                    'קוד סיום': item.terminationCause,
                    'סיבת סיום': item.terminationReason,
                    'תאריך סיום': item._terminationDate
                });
            });
            return dataObj;
        }
    }
})();
(function () {
    angular.module('WorkerPortfolioApp').controller('SystemsEmailTemplatesController', SystemsEmailTemplatesController);

    SystemsEmailTemplatesController.$inject = ['$scope', '$rootScope', '$state', 'toastrService', 'FileManager', 'ngDialog', 'ValidAdminUser', 'Templates'];

    function SystemsEmailTemplatesController($scope, $rootScope, $state, toastrService, FileManager, ngDialog, ValidAdminUser, Templates) {

        if (ValidAdminUser.data !== true || ValidAdminUser.status !== 200) {
            $state.go('Home');
            toastrService.info('', 'הרשאה לא תקינה!', { onHidden: function () { $state.go('Home'); } });
            return void [0];
        }

        $scope.WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SYSTEMSEMAILROLE')

        $scope.EmailTemplates = [];
        $scope.EmailTemplateModel = {
            Editing: false,
            ID: null,
            municipalityId: $scope.WSPostModel.currentMunicipalityId,
            createdDate: new Date(),
            updateDate: new Date(),
            updateUserId: null,
            template: null,
            subject: null,
            name: null,
            fromEmail: null,
            municipality: null,
            user: null,
            type: 1
        };
        ///pager
        $scope.$Pager = {
            PageSize: 15,
            PageNumber: 1,
            Counter: 0,
            MaxPages: 0,
            pagerList: [],
            sizes: [15, 30, 45]
        };
        $scope.changePagerSize = function () {
            $scope.setUpController($scope.EmailTemplates);
        };

        $scope.getItem = function (list, num) {
            var items = list.filter(function (s) {
                return s.ID === num;
            });

            return (items instanceof Array) ? items[0] : items;
        };

        $scope.DeleteTemplate = function (tmplt) {

            ngDialog.openConfirm({
                template: '\
                <p><b>האם למחוק את</b> "' + tmplt.subject + '"?<\p>\
                <p><b>שים לב</b>, לא ניתן לשחזרת תבנית שנמחקה!</p>\
                <div class="ngdialog-buttons">\
                    <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">בטל</button>\
                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(' + tmplt.ID + ')">מחק</button>\
                </div>',
                plain: true,

                scope: $scope
            }).then(function (success) {

                FileManager.DeleteTemplate({ wsModel: $scope.WSPostModel, template: tmplt })
                    .then(function successCallback(res) {
                        if (res.data) {
                            $scope.EmailTemplates.splice($scope.EmailTemplates.indexOf(tmplt), 1);
                            $scope.setUpController($scope.EmailTemplates);
                            if (tmplt.ID === $scope.EmailTemplateModel.ID) {
                                $scope.createNewTemplate(false);
                            }

                            toastrService.success('התבנית נמחקה!', '');
                        } else {
                            toastrService.error('', 'תקלה!');
                        }
                    }, function errorCallback(res) {
                        toastrService.error('', 'תקלת מערכת!');
                    });

            }, function (error) {
                ///do nothing, user canceld
            });

        };
        $scope.SaveTemplate = function () {
            $scope.TemplateFrom.$setSubmitted();
            if ($scope.TemplateFrom.$invalid || !$scope.TemplateFrom.$valid)
                return void [0];

            $scope.EmailTemplateModel.updateDate = new Date();

            FileManager.UpdateAddTemplate({ wsModel: $scope.WSPostModel, template: $scope.EmailTemplateModel })
                    .then(function successCallback(res) {
                        var item = $scope.getItem($scope.EmailTemplates, res.data.ID);
                        if (item === undefined) {
                            $scope.EmailTemplates.push(res.data);
                        } else {
                            $scope.EmailTemplates.splice($scope.EmailTemplates.indexOf(item), 1);
                            $scope.EmailTemplates.push(res.data);
                        }
                        $scope.setUpController($scope.EmailTemplates);
                        $scope.EditTemplate(res.data);
                        toastrService.success('התבנית נשמרה!', '');

                    }, function errorCallback(res) {
                        toastrService.error('', 'תקלת מערכת!');
                    });
        };
        $scope.EditTemplate = function (template) {
            $scope.EmailTemplateModel = angular.copy(template);
            $scope.EmailTemplateModel.Editing = true;
        };
        $scope.createNewTemplate = function (state) {
            $scope.EmailTemplateModel = {
                Editing: state,
                ID: null,
                municipalityId: $scope.WSPostModel.currentMunicipalityId,
                createdDate: new Date(),
                updateDate: new Date(),
                updateUserId: null,
                template: null,
                subject: null,
                name: null,
                fromEmail: null,
                type: 1
            };
        };

        $scope.file_browser_callbackFunction = function (field_name, url, type, win) {
            try {
                $('#mce-modal-block').attr('style', '');
                var style = $('.mce-container.mce-panel.mce-floatpanel.mce-window.mce-in').attr('style');
                style = style.split(';');
                style.filter(function (s) {
                    if (s.indexOf('z-index') >= 0) {
                        style[style.indexOf(s)] = 'z-index: 123';
                    }
                });
                $('.mce-container.mce-panel.mce-floatpanel.mce-window.mce-in').attr('style', style.join(';'));
            } catch (e) {
                
            }

            ngDialog.open({
                template: '../../Scripts/views/Email-Templates/file-manager.html',
                closeByNavigation: true,
                closeByDocument: true,
                closeByEscape: true,
                scope: $scope,
                backdrop: 'static',
                showClose: true,
                appendClassName: 'file-manager-custom',
                name: 'fileManagerPopUp',
                resolve: {
                    fileStructure: ['FileManager', function (FileManager) {
                        return FileManager.GetFileStructure($scope.WSPostModel);
                    }],
                    options: [function () {
                        return { field_name: field_name, url: url, type: type, win: win };
                    }]
                },
                controller: 'FileManagerController'
            });
        };
        $scope.tinymceOptions = {
            selector: "textarea", theme: "modern", height: 400,
            plugins: [
                 "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                 "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                 "table contextmenu directionality emoticons paste textcolor code",
                 "directionality"
            ],
            toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | link unlink anchor | image media | forecolor backcolor  | ltr rtl",

            directionality: "rtl",

            file_browser_callback: $scope.file_browser_callbackFunction
        };

        /* *** set up controller *** */
        $scope.setUpController = function (templates) {
            $scope.EmailTemplates = templates;
            $scope.$Pager.PageNumber = 1;

            $scope.$Pager.Counter = templates.length;
            $scope.$Pager.MaxPages = Math.ceil(templates.length / $scope.$Pager.PageSize);
            var num = 1;

            $scope.$Pager.pagerList = [];
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                $scope.$Pager.pagerList.push(1 + i);
            }
        };
        $scope.setUpController(Templates.data);
    }
})();

(function () {
    angular.module('WorkerPortfolioApp').controller('TaxAdjustmentController', TaxAdjustmentController);

    TaxAdjustmentController.$inject = ['$scope', '$rootScope', '$state', 'TofesDataService', 'TofesModel', 'FileUploader', 'toastrService', 'Search'];

    function TaxAdjustmentController($scope, $rootScope, $state, TofesDataService, TofesModel, FileUploader, toastrService, Search) {
        ///private variables
        ///get radio buttons data
        $scope.radionStates = TofesDataService.GetRadiosStates();
        ///get custom models for tofesModel lists
        $scope.customModel = TofesDataService.GetCustomModel();
        $scope.tofesModel = TofesModel;

        ///update from saved data if have
        TofesDataService.GetExistingData($scope.tofesModel, '$$tofesModel', true);

        $scope.ChengeRequestRule = function () {
            if (!$scope.tofesModel.taxCoordination.request) {
                $scope.tofesModel.taxCoordination.reason = null;
            }
        }

        ///go-to next/prev form stap, published from Header directive
        $scope.subscribe('goto.step', function (goTo, direction) {
            if ($scope.validatUploders() || $scope.partForm.$invalid && direction) {
                $scope.ErrorFocus();
                return void [0];
            }
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            ///go to the step
            $state.go(goTo);
        });

        ///main submit form function
        $scope.nextForm = function () {
            ///form is invalid
            if ($scope.partForm.$invalid || $scope.validatUploders()) {
                $scope.ErrorFocus();
                return void [0];
            }
            
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);

            $scope.publish('$$states', 'current');
            return $scope.goToStep.isempty() ? (void [0]) : $state.go($scope.goToStep);
        }
        ///validate all the uploaders in the scope
        $scope.validatUploders = function () {
            for (var i in $scope) {
                if ($scope[i] instanceof FileUploader) {
                    ///SAVE UPLOADED FILES TO BE SENT
                    $rootScope.FilterFiles($scope.tofesModel.TaxAdjustmentControllerFilesList, $scope[i].queue, i);
                    $scope[i].Files = $scope.tofesModel.TaxAdjustmentControllerFilesList.filter(function (item) {
                        return item.origin === i;
                    }).length;

                    if ($scope[i].UploaderValidation())
                        return true;
                }
            }
            return false;
        }

        ///employers list
        $scope.copyEmployer = function () {
            var employer = angular.copy($scope.customModel.employerModel);
            employer.$id = $scope.tofesModel.taxCoordination.employers.length + 1;
            return employer;
        }
        ///add/remove child from list
        $scope.addEmployer = function () {
            $scope.tofesModel.taxCoordination.employers.push($scope.copyEmployer());
        }
        $scope.removeEmployer = function ($id) {
            $scope.tofesModel.taxCoordination.employers = _.filter($scope.tofesModel.taxCoordination.employers, function (item) {
                return item.$id !== $id
            });
            $.map($scope.tofesModel.taxCoordination.employers, function (item, index) {
                item.$id = (index + 1);
            });
        }
        if ($scope.tofesModel.taxCoordination.employers && $scope.tofesModel.taxCoordination.employers.length === 0)
            $scope.addEmployer();

        ///FILE UPLOADERS
        $scope.reasonUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'reasonFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.reasonUploader.filters.splice(1, 1);
        $scope.reasonUploader.filters.push($scope.queueLimitFilter);
        $scope.reasonUploader.filters.push($scope.uniqueKeyFilter);
        $scope.reasonUploader.filters.push($scope.sizeFilter);
        $scope.reasonUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.reasonUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.reasonUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.reasonUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.reasonUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.reasonUploader.UploaderValidation = $scope._UploaderValidation;

        $scope.coordinationUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'coordinationFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.coordinationUploader.filters.splice(1, 1);
        $scope.coordinationUploader.filters.push($scope.queueLimitFilter);
        $scope.coordinationUploader.filters.push($scope.uniqueKeyFilter);
        $scope.coordinationUploader.filters.push($scope.sizeFilter);
        $scope.coordinationUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.coordinationUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.coordinationUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.coordinationUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.coordinationUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.coordinationUploader.UploaderValidation = $scope._UploaderValidation;
        ///END FILE UPLOADERS

        $scope.ErrorFocus = function () {
            if (typeof $scope.partForm.$error.required == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
            if (typeof $scope.partForm.$error.custom == 'object') {
                $scope.partForm.$error.custom[0].$$element.focus();
                return;
            }
        }
    }
})();

(function () {
    angular.module('WorkerPortfolioApp').controller('TaxExemptionController', TaxExemptionController);

    TaxExemptionController.$inject = ['$scope', '$rootScope', '$state', 'TofesDataService', 'TofesModel', 'FileUploader', 'toastrService', 'Search', '$timeout'];

    function TaxExemptionController($scope, $rootScope, $state, TofesDataService, TofesModel, FileUploader, toastrService, Search, $timeout) {

        ///private variables
        ///get radio buttons data
        $scope.radionStates = TofesDataService.GetRadiosStates();
        $scope.tofesModel = TofesModel;
        ///update from saved data if have
        TofesDataService.GetExistingData($scope.tofesModel, '$$tofesModel', true);

        $scope.taxSectionTitle = $scope.tofesModel.FormType === 3 ? 'ז' : 'ח';
        $scope.childrenInCareTitle = $scope.tofesModel.FormType === 3 ? 'ד' : 'ג';
        $scope.infantchildrenTitle = $scope.tofesModel.FormType === 3 ? '4' : '7';
        $scope.singleParentTitle = $scope.tofesModel.FormType === 3 ? '4 ו-5' : '7 ו-8';

        $scope.Qindex = 1;
        $scope.Qlist = new Array();

        var yearPart = function (d) {
            return parseInt(d.split('/')[2]);
        }
        ///calc children
        $scope.childrenCountsObject = {
            ///מספר ילדים שנולדו בשנת המס ו/או שימלאו להם 18 שנים בשנת המס
            child18: 0,
            ///מספר ילדים שימלאו להם שנה אחת עד חמש שנים בשנת המס
            ander5: 0,
            ///מספר ילדים אחרים שטרם מלאו להם 19 שנים
            ander19: 0,
            ///מספר ילדים שנולדו בשנת המס ו/או שימלאו להם 3 שנים בשנת המס
            upto3: 0,
            ///מספר ילדים שימלאו להם שנה אחת ו/או שנתיים בשנת המס
            upto2: 0,

            ///מס' ילדים שנולדו בשנת המס
            thisYear: 0,
            ///מס' ילדים שימלאו להם 6 שנים עד 17 שנים בשנת המס
            sixTo17: 0,
            ///סך הכל ילדים תחת גיל 19
            totalAnder19: 0,
        }
        $scope.countChildrens = function () {
            var year = parseInt($scope.tofesModel.year);

            $.map($scope.tofesModel.children, function (child) {
                if (child.birthDate == null || typeof child.birthDate == 'undefined')
                    return;

                var y = parseInt(yearPart(child.birthDate));

                if ((year - y) <= 5 && (year - y) > 0)
                    $scope.childrenCountsObject.ander5++;
                if ((year - y) < 19)
                    $scope.childrenCountsObject.totalAnder19++;

                switch ($scope.tofesModel.FormType) {
                    case 1:
                        if (y === year || (year - y) === 18)
                            $scope.childrenCountsObject.child18++;
                        if ((year - y) > 5 && (year - y) < 19 && (year - y) !== 18)
                            $scope.childrenCountsObject.ander19++;
                        if ((year - y) === 3 || (year - y) === 0)
                            $scope.childrenCountsObject.upto3++;
                        if ((year - y) < 3 && (year - y) > 0)
                            $scope.childrenCountsObject.upto2++;
                        break;
                    case 2:
                    case 3:
                        if (y === year)
                            $scope.childrenCountsObject.thisYear++;
                        if ((year - y) >= 6 && (year - y) <= 17)
                            $scope.childrenCountsObject.sixTo17++;
                        if ((year - y) === 18)
                            $scope.childrenCountsObject.child18++;
                        break;
                    default:
                        if (y === year || (year - y) === 18)
                            $scope.childrenCountsObject.child18++;
                        if ((year - y) > 5 && (year - y) < 19 && (year - y) !== 18)
                            $scope.childrenCountsObject.ander19++;
                        if ((year - y) === 3 || (year - y) === 0)
                            $scope.childrenCountsObject.upto3++;
                        if ((year - y) < 3 && (year - y) > 0)
                            $scope.childrenCountsObject.upto2++;
                        break;
                }
            });
        }
        $scope.countChildrens();

        ///go-to next/prev form stap, published from Header directive
        $scope.subscribe('goto.step', function (goTo, direction) {
            ///SAVE UPLOADED FILES TO BE SENT
            $scope.PreValidatUploders();
            if ($scope.validatUploders() || $scope.partForm.$invalid && direction) {
                $scope.ErrorFocus();
                return void [0];
            }
                        
            ///remove autocomplete options
            if (typeof $scope.tofesModel.taxExemption.settlementCredits.settlement === 'object' && $scope.tofesModel.taxExemption.settlementCredits.settlement != null) {
                $scope.tofesModel.taxExemption.settlementCredits.settlementID = $scope.tofesModel.taxExemption.settlementCredits.settlement.originalObject.ID;
                $scope.tofesModel.taxExemption.settlementCredits.settlement = $scope.tofesModel.taxExemption.settlementCredits.settlement.originalObject.Name;
            }

            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            ///go to the step
            $state.go(goTo);
        });

        ///main submit form function
        $scope.nextForm = function (reSend) {
            ///SAVE UPLOADED FILES TO BE SENT
            $scope.PreValidatUploders();
            ///form is invalid
            if ($scope.validatUploders() || $scope.partForm.$invalid) {
                $scope.ErrorFocus();
                return void [0];
            }

            ///remove autocomplete options
            if ($scope.tofesModel.taxExemption.settlementCredits.settlement !== null && typeof $scope.tofesModel.taxExemption.settlementCredits.settlement === 'object') {
                $scope.tofesModel.taxExemption.settlementCredits.settlement = $scope.tofesModel.taxExemption.settlementCredits.settlement.originalObject.Name;
            }

            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);

            $scope.publish('$$states', 'current');
            return $scope.goToStep.isempty() ? (void [0]) : $state.go($scope.goToStep);
        }
        ///validate all the uploaders in the scope
        $scope.PreValidatUploders = function () {
            for (var i in $scope) {
                if ($scope[i] instanceof FileUploader) {
                    ///SAVE UPLOADED FILES TO BE SENT
                    $rootScope.FilterFiles($scope.tofesModel.TaxExemptionControllerFilesList, $scope[i].queue, i);
                    $scope[i].Files = $scope.tofesModel.TaxExemptionControllerFilesList.filter(function (item) {
                        return item.origin === i;
                    }).length;
                }
            }
        }

        $scope.validatUploders = function () {
            for (var i in $scope) {
                if ($scope[i] instanceof FileUploader) {
                    if ($scope[i].UploaderValidation()) {
                        return true;
                    }
                }
            }
            return false;
        }

        $scope.ErrorFocus = function () {
            if (typeof $scope.partForm.$error.required == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
            else if (typeof $scope.partForm.$error.custom == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
        }

        ///FILE UPLOADERS
        $scope.blindDisabledUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'blindDisabledFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.blindDisabledUploader.filters.splice(1, 1);
        $scope.blindDisabledUploader.filters.push($scope.queueLimitFilter);
        $scope.blindDisabledUploader.filters.push($scope.uniqueKeyFilter);
        $scope.blindDisabledUploader.filters.push($scope.sizeFilter);
        $scope.blindDisabledUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.blindDisabledUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.blindDisabledUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.blindDisabledUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.blindDisabledUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.blindDisabledUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.blindDisabledUploader.Files = 0;

        $scope.isaUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'isaFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.isaUploader.filters.splice(1, 1);
        $scope.isaUploader.filters.push($scope.queueLimitFilter);
        $scope.isaUploader.filters.push($scope.uniqueKeyFilter);
        $scope.isaUploader.filters.push($scope.sizeFilter);
        $scope.isaUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.isaUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.isaUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.isaUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.isaUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.isaUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.isaUploader.Files = 0;

        $scope.certificateUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'immigrantCertificateFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.certificateUploader.filters.splice(1, 1);
        $scope.certificateUploader.filters.push($scope.queueLimitFilter);
        $scope.certificateUploader.filters.push($scope.uniqueKeyFilter);
        $scope.certificateUploader.filters.push($scope.sizeFilter);
        $scope.certificateUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.certificateUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.certificateUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.certificateUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.certificateUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.certificateUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.certificateUploader.Files = 0;

        $scope.returningUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'returningResidentFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.returningUploader.filters.splice(1, 1);
        $scope.returningUploader.filters.push($scope.queueLimitFilter);
        $scope.returningUploader.filters.push($scope.uniqueKeyFilter);
        $scope.returningUploader.filters.push($scope.sizeFilter);
        $scope.returningUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.returningUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.returningUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.returningUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.returningUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.returningUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.returningUploader.Files = 0;

        $scope.spouseNoIncomeUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'spouseNoIncomeFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.spouseNoIncomeUploader.filters.splice(1, 1);
        $scope.spouseNoIncomeUploader.filters.push($scope.queueLimitFilter);
        $scope.spouseNoIncomeUploader.filters.push($scope.uniqueKeyFilter);
        $scope.spouseNoIncomeUploader.filters.push($scope.sizeFilter);
        $scope.spouseNoIncomeUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.spouseNoIncomeUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.spouseNoIncomeUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.spouseNoIncomeUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.spouseNoIncomeUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.spouseNoIncomeUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.spouseNoIncomeUploader.Files = 0;

        $scope.alimonyUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'alimonyParticipatesFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.alimonyUploader.filters.splice(1, 1);
        $scope.alimonyUploader.filters.push($scope.queueLimitFilter);
        $scope.alimonyUploader.filters.push($scope.uniqueKeyFilter);
        $scope.alimonyUploader.filters.push($scope.sizeFilter);
        $scope.alimonyUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.alimonyUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.alimonyUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.alimonyUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.alimonyUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.alimonyUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.alimonyUploader.Files = 0;

        $scope.exAlimonyUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'exAlimonyFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.exAlimonyUploader.filters.splice(1, 1);
        $scope.exAlimonyUploader.filters.push($scope.queueLimitFilter);
        $scope.exAlimonyUploader.filters.push($scope.uniqueKeyFilter);
        $scope.exAlimonyUploader.filters.push($scope.sizeFilter);
        $scope.exAlimonyUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.exAlimonyUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.exAlimonyUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.exAlimonyUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.exAlimonyUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.exAlimonyUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.exAlimonyUploader.Files = 0;

        $scope.incompetentChildUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'incompetentChildFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.incompetentChildUploader.filters.splice(1, 1);
        $scope.incompetentChildUploader.filters.push($scope.queueLimitFilter);
        $scope.incompetentChildUploader.filters.push($scope.uniqueKeyFilter);
        $scope.incompetentChildUploader.filters.push($scope.sizeFilter);
        $scope.incompetentChildUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.incompetentChildUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.incompetentChildUploader.onCompleteItem = $scope._onCompleteItem;
        
        $scope.incompetentChildUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.incompetentChildUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.incompetentChildUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.incompetentChildUploader.Files = 0;

        $scope.servicemanUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'servicemanFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.servicemanUploader.filters.splice(1, 1);
        $scope.servicemanUploader.filters.push($scope.queueLimitFilter);
        $scope.servicemanUploader.filters.push($scope.uniqueKeyFilter);
        $scope.servicemanUploader.filters.push($scope.sizeFilter);
        $scope.servicemanUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.servicemanUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.servicemanUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.servicemanUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.servicemanUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.servicemanUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.servicemanUploader.Files = 0;

        var graduationUploader = $scope.graduationUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'graduationFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.graduationUploader.filters.splice(1, 1);
        $scope.graduationUploader.filters.push($scope.queueLimitFilter);
        $scope.graduationUploader.filters.push($scope.uniqueKeyFilter);
        $scope.graduationUploader.filters.push($scope.sizeFilter);
        $scope.graduationUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.graduationUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.graduationUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.graduationUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.graduationUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.graduationUploader.UploaderValidation = $scope._UploaderValidation;
        $scope.graduationUploader.Files = 0;
        ///END FILE UPLOADERS

        ///AUTOCOMPLETE
        $scope.searchSettlement = function (typed) {
            if (!typed || typed.length < 1)
                return void [0];
            return Search.citySearch({ query: typed });
        }
        ///END AUTOCOMPLETE
        var getD = function(){
            var d = new Date();
            d.setFullYear(d.getFullYear() - 1);
            return d;
        }
        $scope.sRdOptions = {
            maxDate: getD()
        }

        $scope.QuestionsNumbers = function (Q) {
            var _q = $scope.Qlist.filter(function (qObj) {
                return qObj !== null && qObj.ID === Q;
            });

            if (_q == null || _q.length === 0) {
                _q = { ID: parseInt(Q), Name: $scope.Qindex++ };
                $scope.Qlist.push(_q);

                $scope.Qlist = $scope.Qlist.sort(function (a, b) {
                    return a.ID > b.ID ? 1 : a.ID < b.ID ? -1 : 0
                });
                $scope.Qlist.filter(function (item, index) {
                    item.Name = (index + 1);
                });

            } else {
                _q = _q[0];
            }
            return _q.Name;
        }

        $scope.PreValidatUploders();
    }
})();

(function () {
    angular.module('WorkerPortfolioApp').controller('TofesController', TofesController);

    TofesController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'TofesDataService', 'TofesModel', '$Tofes', 'FileUploader', 'toastrService', 'Search'];

    function TofesController($scope, $rootScope, $state, $timeout, TofesDataService, TofesModel, $Tofes, FileUploader, toastrService, Search) {
        
        ///get the base data from api and insert into model
        $scope.tofesModel = TofesModel;

        TofesDataService.UpdateObject($scope.tofesModel, $Tofes.data);

        TofesDataService.CopyRunTimeModel($scope.tofesModel);

        ///update from saved data if have
        TofesDataService.GetExistingData($scope.tofesModel, '$$tofesModel', true);

        ///private variables
        ///get radio buttons data
        $scope.radionStates = TofesDataService.GetRadiosStates();
        ///get custom models for tofesModel lists
        $scope.customModel = TofesDataService.GetCustomModel();
        $scope.childId = 0;
        $scope.goToStep = '';
        ///private variables

        ///go-to next/prev form stap, published from Header directive
        $scope.subscribe('goto.step', function (goTo, direction) {
            if ($scope.validatUploders() || $scope.partForm.$invalid) {
                $scope.ErrorFocus();
                return void [0];
            }

            ///remove autocomplete options
            if ($scope.tofesModel.employee.city !== null && typeof $scope.tofesModel.employee.city === 'object') {
                $scope.tofesModel.employee.city = $scope.tofesModel.employee.city.originalObject.Name;
            }
            if ($scope.tofesModel.employee.street !== null && typeof $scope.tofesModel.employee.street === 'object') {
                $scope.tofesModel.employee.street = $scope.tofesModel.employee.street.originalObject.Name;
            }

            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            ///go to the step
            $state.go(goTo);
        });

        ///main submit form function
        $scope.nextForm = function () {
            ///form is invalid
            if ($scope.validatUploders() || $scope.partForm.$invalid) {
                $scope.ErrorFocus();
                return void [0];
            }

            ///remove autocomplete options
            if (typeof $scope.tofesModel.employee.city === 'object') {
                $scope.tofesModel.employee.cityID = $scope.tofesModel.employee.city.originalObject.ID;
                $scope.tofesModel.employee.city = $scope.tofesModel.employee.city.originalObject.Name;
            }
            if (typeof $scope.tofesModel.employee.street === 'object') {
                $scope.tofesModel.employee.streetID = $scope.tofesModel.employee.street.originalObject.ID;
                $scope.tofesModel.employee.street = $scope.tofesModel.employee.street.originalObject.Name;
            }

            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);

            $scope.publish('$$states', 'current');
            return $scope.goToStep.isempty() ? (void [0]) : $state.go($scope.goToStep);
        }
        ///validate all the uploaders in the scope
        $scope.validatUploders = function () {
            $scope.partForm.idNumberFiles.$setValidity('required', true);
            if (!($scope.partForm.firstName.$pristine && $scope.partForm.lastName.$pristine && $scope.partForm.idNumber.$pristine && $scope.partForm.birthDate.$pristine &&
                    $scope.partForm.immigrationDate.$pristine && $scope.partForm.gender.$pristine && $scope.partForm.maritalStatus.$pristine && $scope.partForm.city.$pristine && $scope.partForm.street.$pristine)) {

                if ($scope.IdNumberuploader.queue.length < 1 || ($scope.IdNumberuploader.queue.length > 0 && (isNaN($scope.IdNumberuploader.progress) || $scope.IdNumberuploader.progress < 100))) {
                    $scope.partForm.idNumberFiles.$setValidity('required', false);
                    return true;
                }
            }

            for (var i in $scope) {
                if ($scope[i] instanceof FileUploader) {
                    ///SAVE UPLOADED FILES TO BE SENT
                    $rootScope.FilterFiles($scope.tofesModel.TofesControllerFilesList, $scope[i].queue, i);
                    $scope[i].Files = $scope.tofesModel.TofesControllerFilesList.filter(function (item) {
                        return item.origin === i;
                    }).length;

                    if ($scope[i].UploaderValidation())
                        return true;
                }
            }
            return false;
        }
        $scope.ChangeRadioStatus = function (radioName) {
            $scope.partForm[radioName].$pristine = false;
        }
        ///AUTOCOMPLETE
        $scope.streets = [];
        $scope.searchCities = function (typed) {
            if (!typed || typed.length < 1)
                return void [0];
            return Search.citySearch({ query: typed })
        }
        $scope.cityFocusOut = function ($event) {
            var text = $('#city_value').val();

            if (!$scope.tofesModel.employee.city) {
                ///ALLOW FREE TEXT ON STREET!
                if (text !== null && text.length > 1) {
                    $scope.$broadcast('angucomplete-alt:changeInput', 'city', text);
                    $scope.tofesModel.employee.city = text;

                    $scope.$broadcast('angucomplete-alt:clearInput', 'street');
                } else {
                    $scope.$broadcast('angucomplete-alt:clearInput');
                }
            } else {
                $scope.streets = typeof $scope.tofesModel.employee.city.originalObject !== 'undefined' ? $scope.tofesModel.employee.city.originalObject.Streets : new Array();
            }
        }

        $scope.streetFocusOut = function () {
            ///ALLOW FREE TEXT ON STREET!
            var text = $('#street_value').val();

            if ($scope.tofesModel.employee.street == undefined && text !== null && text.length > 1) {
                ///ALLOW FREE TEXT ON STREET!
                $scope.$broadcast('angucomplete-alt:changeInput', 'street', text);
                $scope.tofesModel.employee.street = text;
            }
        }
        $scope.streetsSearch = function (str) {
            if ($scope.streets == undefined || $scope.streets.length <= 0) {
                Search.citySearch({ query: $scope.tofesModel.employee.city }).then(function (res) {
                    $scope.streets = res.data.data[0].Streets;
                    return $scope.streetsSearch(str);
                });
            } else {
                var matches = [];
                $scope.streets.forEach(function (street) {
                    if (street.Name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0 && matches.length < 12) {
                        matches.push(street);
                    }
                });
                return matches;
            }
        };
        ///AUTOCOMPLETE

        $scope.$watch('tofesModel.employee.street', function (newValue, oldValue) {
            if (typeof newValue === 'undefined' || newValue === null || newValue === oldValue)
                return void [0];
            $scope.CheckZip(true);
        });
        $scope.$watch('tofesModel.employee.city', function (newValue, oldValue) {
            if (typeof newValue === 'undefined' || newValue === null || newValue === oldValue)
                return void [0];
            $scope.CheckZip(true);
        });

        //zip-code-validation city="{{tofesModel.employee.city}}" street="{{tofesModel.employee.street}}"
        $scope.CheckZip = function (Do) {
            if (($scope.tofesModel.employee.zip === null || $scope.tofesModel.employee.zip.length < 4 || Do) &&
                (typeof $scope.tofesModel.employee.city !== 'undefined' && $scope.tofesModel.employee.city !== null && typeof $scope.tofesModel.employee.street !== 'undefined' && $scope.tofesModel.employee.street !== null))
                $scope.doZipCheck($scope.tofesModel.employee.city, $scope.tofesModel.employee.street, $scope.tofesModel.employee.houseNumber).then(function (res) {
                    var parser = new DOMParser();
                    var doc = parser.parseFromString(res.data, 'text/html');
                    var response = doc.body.innerText.trim();
                    if (response.indexOf('RES8') > -1) {
                        $scope.tofesModel.employee.zip = response.replace('RES8', '');
                    } else {
                        $scope.doZipCheck($scope.tofesModel.employee.city, $scope.tofesModel.employee.street).then(function (res) {
                            var parser = new DOMParser();
                            var doc = parser.parseFromString(res.data, 'text/html');
                            var response = doc.body.innerText.trim();
                            if (response.indexOf('RES8') > -1) {
                                $scope.tofesModel.employee.zip = response.replace('RES8', '');
                            } else {
                                toastrService.info('תקן על מנת לקבל מיקוד.', 'כתובת לא תקינה');
                            }
                        });
                    }
                });

        }
        $scope.doZipCheck = function (city, street, houseNumber) {
            var obj = { Location: city, Street: street };
            if (houseNumber !== undefined && houseNumber !== null)
                obj.House = houseNumber;
            return Search.ZipSearch(obj).then(function (res) {
                return res;
            });
        }

        ///$scope.IdNumberuploader file uploader & filters
        $scope.IdNumberuploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'idNumberFile',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.IdNumberuploader.filters.splice(1, 1);
        $scope.IdNumberuploader.filters.push($scope.queueLimitFilter);
        $scope.IdNumberuploader.filters.push($scope.uniqueKeyFilter);
        $scope.IdNumberuploader.filters.push($scope.sizeFilter);
        $scope.IdNumberuploader.filters.push($scope.typeFilter);
        $scope.IdNumberuploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.IdNumberuploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.IdNumberuploader.onCompleteItem = $scope._onCompleteItem;
        $scope.IdNumberuploader.onCompleteAll = $scope._onCompleteAll;
        $scope.IdNumberuploader.removeFromQueue = $scope._removeFromQueue;
        $scope.IdNumberuploader.UploaderValidation = $scope._UploaderValidation;
        ///end $scope.IdNumberuploader file uploader & filters
        ///$scope.assessorUploader file uploader & filters
        $scope.assessorUploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'assessorFiles',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.assessorUploader.filters.splice(1, 1);
        $scope.assessorUploader.filters.push($scope.queueLimitFilter);
        $scope.assessorUploader.filters.push($scope.uniqueKeyFilter);
        $scope.assessorUploader.filters.push($scope.sizeFilter);
        $scope.assessorUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.assessorUploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.assessorUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.assessorUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.assessorUploader.removeFromQueue = $scope._removeFromQueue;
        $scope.assessorUploader.UploaderValidation = $scope._UploaderValidation;
        ///end $scope.assessorUploader file uploader & filters
        ///FILE UPLOADER

        $scope.ErrorFocus = function () {
            if (typeof $scope.partForm.$error.required == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
            if (typeof $scope.partForm.$error.custom == 'object') {
                $scope.partForm.$error.custom[0].$$element.focus();
                return;
            }
        }

        if ($scope.streets == undefined || $scope.streets.length <= 0) {
            Search.citySearch({ query: $scope.tofesModel.employee.city }).then(function (res) {
                $scope.streets = res.data.data[0].Streets;
            });
        }
        var GetMaxDate = function () {
            var _d = new Date();
            _d.setDate(_d.getDate() - 1);
            _d.setFullYear((_d.getFullYear() - 14));
            return _d;
        }
        $scope.bdOptions = {
            maxDate: GetMaxDate()
        }
        $scope.idOptions = {
            maxDate: new Date()
        }

        $scope.CheckZip();
    }
})();
(function () {

    angular.module('WorkerPortfolioApp').controller('UpdateContactDetailsController', UpdateContactDetailsController);

    UpdateContactDetailsController.$inject = ['$scope', '$rootScope', '$state', 'PayCheckEmail', 'toastrService', 'UpdateTypes'];

    function UpdateContactDetailsController($scope, $rootScope, $state, PayCheckEmail, toastrService, UpdateTypes) {

        var goHome = function () {
            $state.go('Home');
        }

        $scope.UpdateTypes = UpdateTypes.data;

        $scope.WSPostModel = angular.copy($rootScope.getWSPostModel());
        $scope.UserDetails = {
            Cell: '',
            Email: '',
            token: '',
            ToUpDate: null,
            showValidation: false
        };

        switch ($scope.UpdateTypes) {
            case 1:///user only have cell
                $scope.UserDetails.ToUpDate = true;
                break;
            case 2:///user only have mail
                $scope.UserDetails.ToUpDate = false;
                break;
            case 4://user have both cell & mail
                $scope.UserDetails.ToUpDate = null;
                break;
            default:
                toastrService.error('לא ניתן לעדכן נתונים כרגע.', 'שגיאת מערכת!', { onTap: function () { $state.go('Home'); }, onHidden: function () { $state.go('Home'); } });
                break;
        }

        $scope.Submit = function () {
            if (!$scope.UserDetailsForm.$valid || $scope.UserDetailsForm.$invalid) {
                return void [0];
            }

            if (!$scope.UserDetails.showValidation)///first step check password and send token
                $scope.CheckPasswordAndSendToken();
            else ///second step update the data in db AND java
                $scope.UpdateNewData();
        }
        $scope.ReSendToken = function () {
            PayCheckEmail.ValidateUserAndSendToken($scope.WSPostModel)
                .then(function successCallback(res) {
                    if (!res.data.success || res.data.exceptionId > 0) {///en error has accured in the process
                        switch (res.data.exceptionId) {
                            case 10:
                                toastrService.error('המערכת לא הצליחה לאמת את סיסמתך!', '');
                                break;
                            case 20:
                                toastrService.error('המערכת לא הצליחה לשלוח את קוד האימות, אנא נסה שנית.', 'התרחשה שגיאה!');
                                break;
                            default:
                                toastrService.error('', 'תקלת מערכת!');
                                break;
                        }

                        return void [0];
                    }

                    toastrService.success('קוד האימות התקף הינו הקוד החדש ביותר, אנא המתן מספר רגעים לפני משלוח חוזר.', 'קוד אימות נשלח שנית!', { timeOut: 9000 });

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.CheckPasswordAndSendToken = function () {

            /* ACTIVATE TO ADD PASSWORD CHECK */
            //$scope.WSPostModel.Password = $scope.UserDetailsForm.password.$modelValue;

            if ($scope.UserDetails.ToUpDate) {///update `email` field, send sms
                $scope.WSPostModel.Email = $scope.UserDetailsForm.Email.$modelValue;
                $scope.WSPostModel.ToSendType = 1;

            } else {///update `cell` field, send email
                $scope.WSPostModel.Cell = $scope.UserDetailsForm.Cell.$modelValue;
                $scope.WSPostModel.ToSendType = 2;
            }
            $scope.PostFirstAction();
        }
        $scope.UpdateNewData = function () {
            if (!$scope.UserDetailsForm.$valid || $scope.UserDetailsForm.$invalid) {
                return void [0];
            }

            $scope.WSPostModel.Token = $scope.UserDetailsForm.token.$modelValue;
            $scope.WSPostModel.Email = $scope.UserDetails.Email;
            $scope.WSPostModel.Cell = $scope.UserDetails.Cell;
            $scope.PostSecondAction();
        }
        $scope.PostFirstAction = function () {
            PayCheckEmail.ValidateUserAndSendToken($scope.WSPostModel)
                .then(function successCallback(res) {
                    if (!res.data.success || res.data.exceptionId > 0) {///en error has accured in the process
                        switch (res.data.exceptionId) {
                            case 10:
                                toastrService.error('המערכת לא הצליחה לאמת את סיסמתך!', '');
                                break;
                            case 20:
                                toastrService.error('המערכת לא הצליחה לשלוח את קוד האימות, אנא נסה שנית.', 'התרחשה שגיאה!');
                                break;
                            default:
                                toastrService.error('', 'תקלת מערכת!');
                                break;
                        }

                        return void [0];
                    }

                    $scope.UserDetails.showValidation = true;

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.PostSecondAction = function () {
            PayCheckEmail.ValidateTokenAndUpdate($scope.WSPostModel)
                .then(function successCallback(res) {
                    if (!res.data.success || res.data.exceptionId > 0) {///en error has accured in the process
                        switch (res.data.exceptionId) {
                            case 30:
                                toastrService.error('המערכת לא הצליחה לאמת את קוד האימות.', 'שגיאה!');
                                break;
                            case 40:
                                toastrService.error('עדכון הנתונים נכשל.', 'שגיאה!');
                                break;
                            default:
                                toastrService.error('', 'תקלת מערכת!');
                                break;
                        }

                        return void [0];
                    }

                    toastrService.success('', 'הנתונים התעדכנו במערכת.', { timeOut: 2000, onTap: goHome, onHidden: goHome });

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

    }
})();
(function () {
    angular.module('WorkerPortfolioApp').controller('UserDetailsController', UserDetailsController);

    UserDetailsController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$location', 'Credentials', 'toastrService', '$controllerState', '$passwordSettings', '$personalDetails'];

    function UserDetailsController($scope, $rootScope, $state, $stateParams, $location, Credentials, toastrService, $controllerState, $passwordSettings, $personalDetails) {

        var maritalStatusArray = ["", "רווק/ה", "נשוי/אה", "גרוש/ה", "אלמן/ה", "פרוד/ה"];
        $scope.$passwordSettings = $passwordSettings.data;
        $scope.$controllerState = $controllerState;
        $scope.parseFullUserData = function (user) {
            return {
                ID: user.ID,
                birthDate: $scope.parseIntoDate(user.birthDate),
                set_birthDate: $scope.parseIntoDate(user.birthDate, true),
                createdDate: user.createdDate,
                cell: user.cell,
                email: user.email,
                firstName: user.firstName,
                idNumber: user.idNumber,
                immigrationDate: $scope.parseIntoDate(user.immigrationDate),
                set_immigrationDate: $scope.parseIntoDate(user.immigrationDate, true),
                lastName: user.lastName,
                phone: user.phone,
                fathersName: user.fathersName,
                previousLastName: user.previousLastName,
                previousFirstName: user.previousFirstName,
                gender: user.gender,
                maritalStatus: maritalStatusArray[user.maritalStatus],
                maritalStatusDate: $scope.parseIntoDate(user.maritalStatusDate),
                set_maritalStatusDate: $scope.parseIntoDate(user.maritalStatusDate, true),
                countryOfBirth: user.countryOfBirth,
                city: user.city,
                zip: user.zip,
                street: user.street,
                poBox: user.poBox,
                houseNumber: user.houseNumber,
                entranceNumber: user.entranceNumber,
                apartmentNumber: user.apartmentNumber,
                hasIncome: user.hasIncome,
                incomeType: user.incomeType,
                hmoCode: user.hmoCode,
                jobTitle: user.jobTitle,
                kibbutzMember: user.kibbutzMember
            }
        }
        $scope.parseIntoDate = function (d, isSet) {
            try {
                var date = d && typeof d === 'string' && d.length > 10 ? new Date(d) : null, returnDate = null;
                if (date !== null) {
                    if (date.getFullYear() < 1900)
                        return '';
                    function pad(val) { return val < 10 ? '0' + val.toString() : val.toString(); }

                    returnDate = pad(date.getDate()) + '/' + pad(date.getMonth() + 1) + '/' + date.getFullYear();
                    if (isSet) returnDate = pad(date.getMonth() + 1) + '/' + pad(date.getDate()) + '/' + date.getFullYear();
                }

                return returnDate;
            } catch (e) {
                //console.log('e', e);
                return null;
            }
        }
        $scope.ReSetDate = function (d) {
            if (!d || typeof d !== 'string' || d.length < 8)
                return null;
            var sd = d.split('/'), returnDate = new Date();
            returnDate.setMonth((parseInt(sd[1]) - 1)); returnDate.setFullYear(sd[2]); returnDate.setDate(sd[0]);
            return returnDate;
        }
        $scope.UserDetails = {};
        $scope.$personalDetails = $personalDetails.data;

        switch ($controllerState) {
            case 'UpdateUser':
                $scope.UserDetails = $scope.parseFullUserData($personalDetails.data);
                break;
            case 'UpdatePartner':
                if (typeof $personalDetails.data !== 'undefined' && $personalDetails.data !== null) $scope.UserDetails = $scope.parseFullUserData($personalDetails.data);
                break;
            case 'UpdateEducation':
                $scope.EducationList = $personalDetails.data;
                if($scope.EducationList.length > 0){
                    $scope.EducationList.filter(function (education) {
                        education.degreeDate = $scope.parseIntoDate(education.degreeDate); 
                    });
                }
                break;
            case 'UpdateChildren':
                $scope.ChildrenList = $personalDetails.data;
                if ($scope.ChildrenList.length > 0) {
                    $scope.ChildrenList.filter(function (child) {
                        child.birthDate = $scope.parseIntoDate(child.birthDate);
                        child.gender = child.gender ? 'זכר' : 'נקבה';
                        child.homeLiving = child.homeLiving ? 'כן' : 'לא';
                    });
                }
                break;
            
            default:
                $state.go('Home');
                break;
        }

        $scope.validatePasswords = function () {
            $scope.PasswordResetForm.newPassword.$setValidity('custom', true);
            $scope.PasswordResetForm.reEnterPassword.$setValidity('custom', true);
            ///return if not valid on other checks
            if ($scope.PasswordResetForm.reEnterPassword.$error.required || $scope.PasswordResetForm.reEnterPassword.$error.minlength || $scope.PasswordResetForm.reEnterPassword.$error.maxlength
                || $scope.PasswordResetForm.newPassword.$error.required || $scope.PasswordResetForm.newPassword.$error.minlength || $scope.PasswordResetForm.newPassword.$error.maxlength)
                return void [0];
            ///check minRequiredNonAlphanumericCharacters
            if ($scope.UserDetails.newPassword.replace(/[^\W|_]/gi, '').length < $scope.$passwordSettings.minNonAlphanumeric) {
                $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] תווים מיוחדים!'.replace('[0]', $scope.$passwordSettings.minNonAlphanumeric);
                $scope.PasswordResetForm.newPassword.$setValidity('custom', false);
                return void [0];
            }
            ///check minRequiredNumericCharacters
            if ($scope.UserDetails.newPassword.replace(/[\D|_]/gi, '').length < $scope.$passwordSettings.minNumeric) {
                $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] מספרים!'.replace('[0]', $scope.$passwordSettings.minNumeric);
                $scope.PasswordResetForm.newPassword.$setValidity('custom', false);
                return void [0];
            }
            ///passwords dont match
            if ($scope.UserDetails.newPassword !== $scope.UserDetails.reEnterPassword) {
                $scope.PasswordResetForm.reEnterPassword.$setValidity('custom', false);
                return void [0];
            }
        }
        ///update user details scope functions
        $scope.updateUser = function () {
            if ($scope.UpdateDetailsForm.$invalid)
                return void [0];

            Credentials.UpdateUserDetails($scope.setSendData())
                .then(function successCallback(res) {
                    toastrService.success('נתוניך התעדכנו בהצלחה!', 'תודה.');
                    $scope.publish('UpdateShowLoginSatet', res.data);
                }, function errorCallback(res) {
                    //console.log('full-error', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.updatePartner = function () {
            if ($scope.UpdateDetailsForm.$invalid)
                return void [0];
            ///TODO -> FIX TO UPDATE PARTNER!!!
            Credentials.UpdateUserDetails($scope.setSendData())
                .then(function successCallback(res) {
                    toastrService.success('נתוניך התעדכנו בהצלחה!', 'תודה.');
                    $scope.publish('UpdateShowLoginSatet', res.data);
                }, function errorCallback(res) {
                    //console.log('full-error', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.updateEducation = function () {
            if ($scope.UpdateEducationForm.$invalid)
                return void [0];
            ///TODO -> FIX TO UPDATE EDUCATION!!!
            
        }
        $scope.updateChildren = function () {
            if ($scope.UpdateChildrenForm.$invalid)
                return void [0];
            ///TODO -> FIX TO UPDATE CHILDREN!!!

        }

        $scope.setSendData = function () {
            var UserModel = angular.copy($scope.UserDetails);
            UserModel.birthDate = $scope.ReSetDate(UserModel.birthDate);
            UserModel.immigrationDate = $scope.ReSetDate(UserModel.immigrationDate);
            UserModel.maritalStatusDate = $scope.ReSetDate(UserModel.maritalStatusDate);
            return UserModel;
        }

    }
})();

(function () {

    angular.module('WorkerPortfolioApp').controller('UserEmailPayCheckController', UserEmailPayCheckController);

    UserEmailPayCheckController.$inject = ['$scope', '$rootScope', '$state', 'ngDialog', 'PayCheckEmail', 'UPCData', 'toastrService'];

    function UserEmailPayCheckController($scope, $rootScope, $state, ngDialog, PayCheckEmail, UPCData, toastrService) {

        var goHome = function () {
            $state.go('Home');
        }
        
        $scope.$email = UPCData.data.Email;
        $scope.payCheck = UPCData.data.PayCheck;
        $scope.WSPostModel = $rootScope.getWSPostModel();

        ///calc the user UEPC state
        $scope.USPCTE = {
            iAgree: $scope.payCheck != null && $scope.payCheck.activeRecord && $scope.payCheck.sendMail,
            haveMail: $scope.$email != null && $scope.$email.email != null && $scope.$email.email.length > 3,
            email: $scope.$email != null ? $scope.$email.email : null,
            PanelToShow: 0
        }

        if ($scope.USPCTE.iAgree && $scope.USPCTE.haveMail)///have USPCTE
            $scope.USPCTE.PanelToShow = 1;
        if (!$scope.USPCTE.iAgree && $scope.USPCTE.haveMail)///can have USPCTE
            $scope.USPCTE.PanelToShow = 2;
        if (!$scope.USPCTE.haveMail)///cant have USPCTE
            $scope.USPCTE.PanelToShow = 3;

        if ($scope.USPCTE.PanelToShow === 2)
            $scope.USPCTE.iAgree = true;

        $scope.UEPCStatusSubmit = function () {
            if ($scope.UpdateUEPCStatus.removeUSPCTE == undefined && !$scope.UpdateUEPCStatus.iAgree.$modelValue)
                return void [0];

            $scope.WSPostModel.isMobileRequest = ($scope.UpdateUEPCStatus.removeUSPCTE != undefined && $scope.UpdateUEPCStatus.removeUSPCTE.$modelValue) ? false : $scope.UpdateUEPCStatus.iAgree.$modelValue;
            $scope.UpdateUEPCStatus.iAgree.$setViewValue($scope.WSPostModel.isMobileRequest);

            PayCheckEmail.UserSendPayCheckToEmail($scope.WSPostModel)
                .then(function successCallback(res) {
                    if (res.status == 200) {
                        if ($scope.USPCTE.PanelToShow == 1 && !$scope.WSPostModel.isMobileRequest)
                            $scope.USPCTE.PanelToShow = 2;
                        if ($scope.USPCTE.PanelToShow == 2 && $scope.WSPostModel.isMobileRequest)
                            $scope.USPCTE.PanelToShow = 1;

                        if ($scope.WSPostModel.isMobileRequest)
                            toastrService.success('בקשתך להצטרף לקבלת תלוש במייל התקבלה בהצלחה!', '', { timeOut: 3500, onTap: goHome, onHidden: goHome });
                        else
                            toastrService.warning('בקשתך להפסיק את השירות התקבלה!', '', { timeOut: 3500, onTap: goHome, onHidden: goHome });

                    } else {
                        toastrService.error('אנא עדכן את הדף ונסה שנית.', 'התרחשה תקלה!', { timeOut: 3500, onTap: goHome, onHidden: goHome });
                    }
                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!', { timeOut: 3500, onTap: goHome, onHidden: goHome });
                });
        }
    }
})();

(function () {
    

    angular.module('WorkerPortfolioApp').controller('UserMenuController', UserMenuController);

    UserMenuController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', 'FileUploader', 'toastrService', '$controllerState', '$passwordSettings', 'UserService', 'Credentials'];

    function UserMenuController($scope, $rootScope, $state, $stateParams, FileUploader, toastrService, $controllerState, $passwordSettings, UserService, Credentials) {
        
        $scope.$$UserCredentials = typeof $rootScope.$$credentials !== 'undefined' ? $rootScope.$$credentials : $scope.lsGet('$$credentials');
        $scope.$passwordSettings = $passwordSettings.data;
        $scope.$controllerState = $controllerState;
        $scope.toMailItems = new Array();
        $scope.Notification = {
            toMailID: null,
            jobTitle: null,
            phone: null,
            department: null,
            userName: $scope.$$UserCredentials.loggedIn? ($scope.$$UserCredentials.firstName + ' ' + $scope.$$UserCredentials.lastName) : '',
            subject: null,
            text: null,
            files: '',
            userIdNumber: $scope.$$UserCredentials.idNumber,
            senderEmail: null,
            recipientEmail:null
        };
        
        $scope.ReturnNotification = { ID: -1 };
        $scope.WSPostModel = {
            idNumber: $scope.$$UserCredentials.idNumber,
            confirmationToken: $scope.$$UserCredentials.confirmationToken,
            calculatedIdNumber: $scope.$$UserCredentials.calculatedIdNumber,
            currentMunicipalityId: $scope.$$UserCredentials.currentMunicipalityId,
            selectedDate: new Date(),
        };

        ///NotificationUploader file uploader & filters
        $scope.NotificationUploader = new FileUploader({
            url: 'api/FilesUploades/NotificationUploader',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.NotificationUploader.filters.splice(1, 1);
        $scope.NotificationUploader.filters.push($scope.queueLimitFilter);
        $scope.NotificationUploader.filters.push($scope.uniqueKeyFilter);
        $scope.NotificationUploader.filters.push($scope.sizeFilter);
        $scope.NotificationUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.NotificationUploader.onAfterAddingFile = function (fileItem) {
            fileItem.formData = [{ idNumber: $scope.$$UserCredentials.idNumber, filename: fileItem.file.name }];
        };
        $scope.NotificationUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.NotificationUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.NotificationUploader.removeFromQueue = $scope._removeFromQueue;
        ///end NotificationUploader file uploader & filters

        switch ($controllerState) {
            case 'ResetCurrentPassword':
                $scope.UserCredentials = {
                    newPassword: null,
                    reEnterPassword: null,
                    oldPassword: null,
                    token: $scope.$$UserCredentials.confirmationToken,
                    passwordResetType: 0
                };
                
                break;
            case 'HumanResourcesNotify':
                //$scope.Notification = {};
                UserService.GetToMailItems($scope.WSPostModel)
                    .then(function successCallback(response) {
                       $scope.toMailItems = response.data;                       
                    }, function errorCallback(response) {
                        //console.log('full-error', response);
                        toastrService.error('', 'תקלת מערכת!');
                    });
                if ($scope.$$UserCredentials.loggedIn)
                    Credentials.GetFullUserdetails({ Token: $scope.$$UserCredentials.confirmationToken, newPassword: null })
                        .then(function successCallback(res) {
                            $scope.Notification.jobTitle = res.data.jobTitle;
                            $scope.Notification.phone = res.data.cell;
                            $scope.Notification.jobTitle = res.data.jobTitle;
                            $scope.Notification.userIdNumber = res.data.idNumber;
                            
                        }, function errorCallback(res) {
                            //console.log('full-error', res);
                            toastrService.error('', 'תקלת מערכת!');
                        });
                break;
            case 'ContactUs':
                if ($scope.$$UserCredentials.loggedIn)
                    Credentials.GetFullUserdetails({ Token: $scope.$$UserCredentials.confirmationToken, newPassword: null })
                        .then(function successCallback(res) {
                            $scope.Notification.senderEmail = res.data.email;
                            $scope.Notification.phone = res.data.cell;
                            $scope.Notification.userIdNumber = res.data.idNumber;
                            
                        }, function errorCallback(res) {
                            //console.log('full-error', res);
                            toastrService.error('', 'תקלת מערכת!');
                        });
                break;
            default:
                $state.go('Home');
                break;
        }
        
        $scope.validatePasswords = function () {
            $scope.UpdatePasswordForm.newPassword.$setValidity('custom', true);
            $scope.UpdatePasswordForm.reEnterPassword.$setValidity('custom', true);
            ///return if not valid on other checks
            if ($scope.UpdatePasswordForm.reEnterPassword.$error.required || $scope.UpdatePasswordForm.reEnterPassword.$error.minlength || $scope.UpdatePasswordForm.reEnterPassword.$error.maxlength
                || $scope.UpdatePasswordForm.newPassword.$error.required || $scope.UpdatePasswordForm.newPassword.$error.minlength || $scope.UpdatePasswordForm.newPassword.$error.maxlength)
                return void [0];
            ///check minRequiredNonAlphanumericCharacters
            if ($scope.UserCredentials.newPassword.replace(/[^\W|_]/gi, '').length < $scope.$passwordSettings.minNonAlphanumeric) {
                $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] תווים מיוחדים!'.replace('[0]', $scope.$passwordSettings.minNonAlphanumeric);
                $scope.UpdatePasswordForm.newPassword.$setValidity('custom', false);
                return void [0];
            }
            ///check minRequiredNumericCharacters
            if ($scope.UserCredentials.newPassword.replace(/[\D|_]/gi, '').length < $scope.$passwordSettings.minNumeric) {
                $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] מספרים!'.replace('[0]', $scope.$passwordSettings.minNumeric);
                $scope.UpdatePasswordForm.newPassword.$setValidity('custom', false);
                return void [0];
            }
            ///passwords dont match
            if ($scope.UserCredentials.newPassword !== $scope.UserCredentials.reEnterPassword) {
                $scope.UpdatePasswordForm.reEnterPassword.$setValidity('custom', false);
                return void [0];
            }
        }

        $scope.updatePassword = function () {
            $scope.validatePasswords();
            if ($scope.UpdatePasswordForm.$invalid)
                return void [0];

            UserService.ResetPassword($scope.UserCredentials)
                .then(function successCallback(response) {
                    if (response.data.success) {
                        toastrService.success('סיסמתך עודכנה בהצלחה!', 'תודה.');
                    } else {
                        switch (response.data.exceptionId) {
                            case 61:///GetUserFromToken
                                toastrService.error('סיסמתך אינה נכונה!', 'שגיאת אימות נתונים!');
                                break;
                            case 1:///InvalidPassworException
                                toastrService.error('סיסמתך אינה נכונה!', 'שגיאת אימות נתונים!');
                                break;
                            case 102:///"NullUserReferenceException" error in token
                                $scope.publish('CatchTransmitLogout');
                                toastrService.error('אנא בצע כניסה מחודשת לאתר.', 'שגיאת אימות נתונים!');
                                break;
                            case 101:///user is locked out
                                $scope.publish('CatchTransmitLogout');
                                toastrService.error('אנא נסה להתחבר מחדש בעוד LOCKTIMELEFT דקות'.replace('LOCKTIMELEFT', response.data.lockTimeLeft), 'סליחה, ננעלת מחוץ למערכת.');
                                break;
                            default:
                                //console.log('error', response);
                                toastrService.error('', 'תקלת מערכת!');
                                break;
                        }
                    }
                }, function errorCallback(response) {
                    //console.log('full-error', response);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.sendNotification = function () {
            if ($scope.NotificationForm.$invalid)
                return void [0];

            if ($scope.NotificationUploader.getNotUploadedItems().length > 0) {
                toastrService.info('על מנת לשלוח את ההודעה:<br/>יש לבצע שמירה/הסרה של הקבצים.<br/>לחלופין ניתן לבצע שליחה חוזרת של הטופס.', 'שם לב!<br/> יש קבצים שלא הועלו לשרת!',
                    { clearThenShow: true, allowHtml: true, timeOut: 9999, closeButton: true });
                return void [0];
            }

            $scope.Notification.files = typeof $scope.NotificationUploader.successItemsList !== 'undefined' && $scope.NotificationUploader.successItemsList.length > 0 ? $scope.NotificationUploader.successItemsList.join(',') : '';
            $scope.Notification.toMailID = $scope.NotificationForm.toMail.$modelValue.ID;
                        
            UserService.SendNotificationMail($scope.Notification)
                .then(function successCallback(response) {
                    if (response.data != null && response.data.ID > 0) {///success
                        toastrService.success('ההודעה נשלחה בהצלחה');
                        $scope.ReturnNotification = response.data;
                        $scope.ReturnNotification.toMail = $scope.NotificationForm.toMail.$modelValue;
                    } else {///error
                        toastrService.error('התרחשה תקלה במשלוח המייל.', 'תקלה');
                    }
                }, function errorCallback(response) {
                    //console.log('full-error', response);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.sendContactUs = function () {
            if ($scope.ContactUsForm.$invalid)
                return void [0];

            UserService.SendContactUsMail($scope.Notification)
                .then(function successCallback(response) {
                    if (response.data != null && response.data.ID > 0) {///success
                        toastrService.success('ההודעה נשלחה בהצלחה');
                        $scope.ReturnNotification = response.data;
                    } else {///error
                        toastrService.error('התרחשה תקלה במשלוח המייל.', 'תקלה');
                    }
                }, function errorCallback(response) {
                    //console.log('full-error', response);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.ReturnFromNotification = function () {
            $scope.ReturnNotification.ID = -1;
            $scope.Notification.subject = null;
            $scope.Notification.text = null;

            switch ($controllerState) {
                case 'ContactUs':                    
                    break;
                case 'HumanResourcesNotify':
                    //$scope.NotificationUploader.clearQueue();
                    $scope.NotificationUploader.queue = [];
                    $scope.NotificationUploader.progress = 0;
                    $scope.NotificationUploader.successItemsList = [];
                    break;
            }
        }
    }
})();

//# sourceMappingURL=app.js.map