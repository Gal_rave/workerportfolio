﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Web;
using System.Threading;
using System.Configuration;
using System.Web.Routing;

using Logger;
using WebServiceProvider;
using DataAccess.Models;
using AsyncHelpers;
using CustomMembership;
using DataAccess.Extensions;
using DataAccess.BLLs;
using WorkerPortfolio.App_Start;
using Mail;

namespace WorkerPortfolio.Controllers
{
    [RoutePrefix("DataMigration")]
    public class DataMigrationController : ApiController
    {
        #region private vars
        private SetUpBll setUpBll;
        private AdminBll adminBll;

        private readonly string baseContent = "~/Content";
        private readonly string baseFolder = "uploads";
        private readonly string excels = "excels";

        private static object GetTerminationCause_Lock = new object();
        private static object UPCEReport_Lock = new object();
        #endregion

        public DataMigrationController()
        {
            this.setUpBll = new SetUpBll();
            this.adminBll = new AdminBll();

            excels = Path.Combine(HttpContext.Current.Server.MapPath(baseContent), baseFolder, excels);
            FolderExtensions.CreateEmptyFolder(excels);
        }

        [HttpGet]
        [Route("UpdateTerminations")]
        public IHttpActionResult UpdateTerminations()
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "UpdateTerminations");
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(GetTerminationCause_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("GetTerminationCause_Lock!");
                    }
                    try
                    {
                        lock (GetTerminationCause_Lock)
                        {
                            List<TERMINATION> terminations = FormsProvider.GetTerminationCauseList();
                            this.setUpBll.UpdateTerminationCause(terminations);
                        }
                    }
                    catch (Exception Ie)
                    {
                        return this.ServerError(Ie);
                    }
                    finally
                    {
                        Monitor.Exit(GetTerminationCause_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("UPCEReport")]
        public IHttpActionResult SendUPCEReport()
        {
            Log.PcuLog(HttpContext.Current.Request.Url, "SendUPCEReport");
            
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(UPCEReport_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("UPCEReport_Lock!");
                    }

                    List<int> municipalities = new List<int>();
                    ExcelBll exc = new ExcelBll();
                    FolderExtensions.CleanDirectory(excels);
                    CreateMobileImage imager = new CreateMobileImage();
                    string filePath = string.Empty;
                    try
                    {
                        lock (UPCEReport_Lock)
                        {
                            DateTime pcDate;
                            DateTime reportDate = DateTime.Now.AddDays(-1);
                            List<PayCheckReportModel> report;
                            MunicipalityUpcbmModel municipalityUpcbmModel;

                            municipalities = this.setUpBll.GetMunicipalitiesForUPCEReport(reportDate);
                            foreach (int municipality in municipalities)
                            {
                                municipalityUpcbmModel = FormsProvider.CheckRashutLastPayCheckMonth(municipality);
                                pcDate = DateExtension.maxDateToDate(municipalityUpcbmModel);
                                report = this.adminBll.GenerateUPCBM_Report(pcDate, reportDate.AddMonths(-1), municipality);
                                if (report != null && report.Count > 0)
                                {
                                    Log.PcuLog(new { report = report.Count , pcDate = pcDate }, "SendUPCEReport");
                                    exc.CreatePCUE_Excel(report, excels, municipality.ToString(), string.Format("{0}-{1}", pcDate.Year, pcDate.Month));
                                    filePath = string.Format(@"{1}\{0}.xlsx", municipality, Path.Combine(baseContent, baseFolder, "excels"));
                                    
                                    List<ManagerModel> managers = FormsProvider.GetManagersContacts(municipality);
                                    if(managers != null && managers.Count > 0)
                                    {
                                        MunicipalityExtensionModel municipalityModel = this.adminBll.GetMunicipality(new UserSearchModel() { municipalityId = municipality });
                                        managers.ForEach(m =>
                                        {
                                            Log.PcuLog(m, "ManagerModel");
                                            imager.SendPCUE_ReportMail(m, pcDate, reportDate, municipalityModel, filePath);
                                        });
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception Ie)
                    {
                        return this.ServerError(Ie);
                    }
                    finally
                    {
                        Monitor.Exit(UPCEReport_Lock);
                    }
                    return Ok(municipalities);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
    }
}
