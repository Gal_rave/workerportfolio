﻿using System;
using System.Web;
using System.Linq;
using System.Web.Http;
using System.Web.Routing;
using System.Configuration;
using System.Collections.Generic;

using Mail;
using Logger;
using DataAccess.BLLs;
using CustomMembership;
using DataAccess.Models;
using DataAccess.Extensions;
using WorkerPortfolio.App_Start;

namespace WorkerPortfolio.Controllers
{
    [Authorize]
    [RoutePrefix("Mailer")]
    [InitializeSimpleMembership]
    public class MailerController : ApiController
    {
        #region private vars
        private bool isDebugeMode;
        private string debugeModeEmails;
        private string defaultSiteEmail;
        private MailerBll mailerBll;
        private MailBll mailBll;
        #endregion

        public MailerController()
        {
            this.isDebugeMode = ConfigurationManager.AppSettings["isDebugeMode"].ToBool();
            this.debugeModeEmails = !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["debugeModeEmails"]) ? ConfigurationManager.AppSettings["debugeModeEmails"] : "galr@ladpc.co.il";
            this.defaultSiteEmail = !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["smtpDefaultEmail"]) ? ConfigurationManager.AppSettings["smtpDefaultEmail"] : "tikoved@iula.org.il";
            this.mailerBll = new MailerBll();
            this.mailBll = new MailBll();
        }
        [HttpGet]
        [Route("Send_NessZionaFestival")]
        [AllowAnonymous]
        public IHttpActionResult Send_NessZionaFestival(int municipalityId)
        {
            try
            {
                List<UserModel> users = mailerBll.GetUsersByMunicipalityId(municipalityId);
                if (!isDebugeMode)
                    users.ForEach(u =>
                    {
                        if (SendEmail(u, "Ness-Ziona-Festival", "פסטיבל נס המזרח", "הזמנה לעובדי עיריית נס-ציונה"))
                        {
                            mailBll.SaveNotification(new PostNotificationMail
                            {
                                department = "עיריית נס-ציונה",
                                subject = "הזמנה לעובדי עיריית נס-ציונה",
                                userName = string.Format("{0} {1}", u.firstName, u.lastName),
                                userIdNumber = u.idNumber,
                                recipientEmail = u.email
                            }
                            );
                        }
                    });

                return Ok(users.Count);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        
        #region private functions
        public bool SendEmail(UserModel user, string templateName , string subject = "", string displayName = "")
        {
            try
            {
                string template = string.Empty;
                RouteData routedata = new RouteData();

                UserCredentialsModel ucm = new UserCredentialsModel
                {
                    firstName = user.firstName,
                    lastName = user.lastName,
                    fullName = string.Format("{0} {1}", user.firstName, user.lastName),
                    idNumber = user.idNumber,
                    municipalities = user.municipalities,
                    email = user.email
                };

                // send the mail
                if (!isDebugeMode)
                {
                    template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", templateName, ucm);
                    MailSender.Send(user.email, template, false, subject, null, displayName, defaultSiteEmail);

                    Log.FormLog(new { email = user.email, idNumber = user.idNumber }, templateName);
                }
                else
                {
                    template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", templateName, ucm);
                    MailSender.Send(this.debugeModeEmails, template, false, subject, null, displayName, defaultSiteEmail);

                    Log.FormLog(new { email = user.email, idNumber = user.idNumber }, templateName);
                }
                return true;
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return false;
            }
        }
        #endregion
    }
}
