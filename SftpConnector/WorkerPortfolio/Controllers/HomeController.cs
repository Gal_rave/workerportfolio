﻿using System;
using System.IO;
using System.Web.Mvc;

using DataAccess.BLLs;
using CustomMembership;
using DataAccess.Models;
using WebServiceProvider;
using DataAccess.Extensions;
using System.Diagnostics;
using System.Configuration;
using System.Drawing;

using Logger;
using System.Collections.Generic;
using System.Threading;

namespace WorkerPortfolio.Controllers
{
    public class HomeController : Controller
    {
        private UserBll userBll;
        private RegisterBll registerBll;
        private readonly string baseContent = "~/Content";
        private readonly string baseFolder = "uploads";
        private string ghostScriptPath = string.Empty;
        private string uploads = string.Empty;
        public HomeController()
        {
            this.userBll = new UserBll();
            this.registerBll = new RegisterBll();
            ghostScriptPath = ConfigurationManager.AppSettings["GhostScriptDllPath"];
        }

        public ActionResult Index()
        {
            ViewData.Add("Title", "תיק עובד באינטרנט");
            return View();
        }
        /// <summary>
        /// show paycheck in iframe
        /// </summary>
        /// <param name="idNumber"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public ActionResult PaycheckResponse(string idNumber, int year, int month)
        {
            if (!WebSecurity.IsCurrentUser(idNumber))
                throw new FieldAccessException("UserdetailsAccessException");

            try
            {
                DateTime requestTime = new DateTime(year, month, 14);
                UserModel user = this.userBll.GetUserByID(idNumber);

                UserImageModel image = FormsProvider.GetPaycheck(user.idNumber, user.calculatedIdNumber.ToInt(), WebSecurity.GetSetCurrentMunicipality(), requestTime);

                MemoryStream ms = new MemoryStream(image.fileBlob);
                return new FileStreamResult(ms, "application/pdf");
            }
            catch (Exception e)
            {
                this.ServerError(e);
                Response.StatusCode = 406;
            }
            return new EmptyResult();
        }
        /// <summary>
        /// show paycheck as jpeg - for mobile
        /// </summary>
        /// <param name="idNumber"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public ActionResult ConvertPaycheckResponse(string idNumber, int year, int month)
        {
            if (!WebSecurity.IsCurrentUser(idNumber))
                throw new FieldAccessException("UserdetailsAccessException");
            try
            {
                createUploadsFolder();
                string tempUserFolder = Path.Combine(uploads, idNumber);
                string outputPath = Path.Combine(tempUserFolder, "TEMP_");
                string inputPath = Path.Combine(tempUserFolder, string.Format("{0}.pdf", idNumber));

                #region delete/create the folder
                deleteCreateFolder(tempUserFolder);
                #endregion

                #region get the image and save to file
                DateTime requestTime = new DateTime(year, month, 14);
                UserModel user = this.userBll.GetUserByID(idNumber);
                UserImageModel image = FormsProvider.GetPaycheck(user.idNumber, user.calculatedIdNumber.ToInt(), WebSecurity.GetSetCurrentMunicipality(), requestTime);
                if (image.fileimageblob == null || image.fileimageblob.Length < 1000)
                {
                    System.IO.File.WriteAllBytes(inputPath, image.fileBlob);


                    #region perse pdf into images then combine images
                    perseImagesAndCombine(tempUserFolder, outputPath, inputPath, idNumber);
                    #endregion

                    #region save the new image into DB
                    image.fileimageblob = System.IO.File.ReadAllBytes(Path.Combine(tempUserFolder, string.Format("{0}.jpeg", idNumber)));
                    userBll.CreateUserImage(image);
                    FolderExtensions.DeletFolder(tempUserFolder);
                    #endregion
                }
                #endregion
                MemoryStream ms = new MemoryStream(image.fileimageblob);
                return new FileStreamResult(ms, "image/jpeg");
            }
            catch (Exception e)
            {
                this.ServerError(e);
                Response.StatusCode = 406;
            }
            return new EmptyResult();

        }
        /// <summary>
        /// show 106 form in iframe
        /// </summary>
        /// <param name="idNumber"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public ActionResult AnnualFormResponse(string idNumber, int year, int month)
        {
            if (!WebSecurity.IsCurrentUser(idNumber))
                throw new FieldAccessException("UserdetailsAccessException");

            try
            {
                DateTime requestTime = new DateTime(year, month, 14);
                UserModel user = this.userBll.GetUserByID(idNumber);

                UserImageModel image = FormsProvider.Get106(user.idNumber, user.calculatedIdNumber.ToInt(), WebSecurity.GetSetCurrentMunicipality(), requestTime);

                MemoryStream ms = new MemoryStream(image.fileBlob);
                return new FileStreamResult(ms, "application/pdf");
            }
            catch (Exception e)
            {
                this.ServerError(e);
                Response.StatusCode = 406;
            }
            return new EmptyResult();
        }
        public ActionResult ConvertAnnualFormResponse(string idNumber, int year, int month)
        {
            if (!WebSecurity.IsCurrentUser(idNumber))
                throw new FieldAccessException("UserdetailsAccessException");

            try
            {
                createUploadsFolder();
                string tempUserFolder = Path.Combine(uploads, idNumber);
                string outputPath = Path.Combine(tempUserFolder, "TEMP_");
                string inputPath = Path.Combine(tempUserFolder, string.Format("{0}.pdf", idNumber));

                #region delete/create the folder
                deleteCreateFolder(tempUserFolder);
                #endregion

                #region get the image and save to file
                DateTime requestTime = new DateTime(year, month, 14);
                UserModel user = this.userBll.GetUserByID(idNumber);
                UserImageModel image = FormsProvider.Get106(user.idNumber, user.calculatedIdNumber.ToInt(), WebSecurity.GetSetCurrentMunicipality(), requestTime);
                if (image.fileimageblob == null || image.fileimageblob.Length < 1000)
                {
                    System.IO.File.WriteAllBytes(inputPath, image.fileBlob);
                    #endregion

                    #region perse pdf into images then combine images
                    perseImagesAndCombine(tempUserFolder, outputPath, inputPath, idNumber);
                    #endregion

                    #region save the new image into DB
                    image.fileimageblob = System.IO.File.ReadAllBytes(Path.Combine(tempUserFolder, string.Format("{0}.jpeg", idNumber)));
                    userBll.CreateUserImage(image);
                    FolderExtensions.DeletFolder(tempUserFolder);
                    #endregion
                }

                MemoryStream ms = new MemoryStream(image.fileimageblob);
                return new FileStreamResult(ms, "image/jpeg");
            }
            catch (Exception e)
            {
                this.ServerError(e);
                Response.StatusCode = 406;
            }
            return new EmptyResult();
        }
        /// <summary>
        /// show 101 form in iframe
        /// </summary>
        /// <param name="idNumber"></param>
        /// <param name="imageID"></param>
        /// <returns></returns>
        public ActionResult FormIeShow(string idNumber, int imageID)
        {
            if (!WebSecurity.IsCurrentUser(idNumber))
                throw new FieldAccessException("UserdetailsAccessException");

            try
            {
                UserImageModel image = this.userBll.GetImageByID(imageID);
                MemoryStream ms = new MemoryStream(image.fileBlob);
                return new FileStreamResult(ms, "application/pdf");
            }
            catch (Exception e)
            {
                this.ServerError(e);
                Response.StatusCode = 406;
            }
            return new EmptyResult();
        }
        /// <summary>
        /// show 101 form as jpeg - for mobile
        /// </summary>
        /// <param name="idNumber"></param>
        /// <param name="imageID"></param>
        /// <returns></returns>
        public ActionResult Convert(string idNumber, int imageID)
        {
            if (!WebSecurity.IsCurrentUser(idNumber))
                throw new FieldAccessException("UserdetailsAccessException");

            try
            {
                createUploadsFolder();
                string tempUserFolder = Path.Combine(uploads, idNumber);
                string outputPath = Path.Combine(tempUserFolder, "TEMP_");
                string inputPath = Path.Combine(tempUserFolder, string.Format("{0}.pdf", idNumber));

                #region delete/create the folder
                deleteCreateFolder(tempUserFolder);
                #endregion

                #region get the image and save to file
                UserImageModel image = this.userBll.GetImageByID(imageID);
                if (image.fileimageblob == null || image.fileimageblob.Length < 1000)
                {
                    System.IO.File.WriteAllBytes(inputPath, image.fileBlob);

                    #region perse pdf into images then combine images
                    perseImagesAndCombine(tempUserFolder, outputPath, inputPath, idNumber);
                    #endregion

                    #region save the new image into DB
                    image.fileimageblob = System.IO.File.ReadAllBytes(Path.Combine(tempUserFolder, string.Format("{0}.jpeg", idNumber)));
                    userBll.CreateUserImage(image);
                    FolderExtensions.DeletFolder(tempUserFolder);
                    #endregion
                }
                #endregion
                MemoryStream ms = new MemoryStream(image.fileimageblob);
                return new FileStreamResult(ms, "image/jpeg");
            }
            catch (Exception e)
            {
                this.ServerError(e);
                Response.StatusCode = 406;
            }
            return new EmptyResult();
        }
        [Route("displayImage/{ID}")]
        public ActionResult displayImage(int ID)
        {
            FilesBll filesBll = new FilesBll();
            var img = filesBll.GetImageById(ID);
            MemoryStream ms = new MemoryStream(img.fileBlob);
            return new FileStreamResult(ms, string.Format("image/{0}", img.type));
        }
        [Route("displayGuid/{ID}")]
        public ActionResult displayGuid(string ID)
        {
            FilesBll filesBll = new FilesBll();
            var img = filesBll.GetImageByGuidName(ID);
            MemoryStream ms = new MemoryStream(img.fileBlob);
            string v = string.Format("{0}/{1}", img.type.ToLower() == "pdf" ? "application" : "image", img.type);
            if (v == null) { }
            return new FileStreamResult(ms, string.Format("{0}/{1}", img.type.ToLower() == "pdf"? "application" : "image", img.type));
        }
        public ActionResult SynerionRedirect()
        {
            try
            {
                string passwordVerificationToken = WebSecurity.GeneratePasswordResetToken(WebSecurity.CurrentUserName, -20);

                if (string.IsNullOrWhiteSpace(passwordVerificationToken))
                    throw new MembershipException("NullUserReferenceException", 102);

                string synerionUrl = registerBll.getSynerionLinkByMunicipalityId(WebSecurity.GetLoginMunicipalityId(WebSecurity.CurrentUserName));

                if (synerionUrl == null)
                    return Redirect("/");

                string url = string.Format(synerionUrl, passwordVerificationToken);

                Log.TestLog(url, "SynerionRedirect");

                return Redirect(url);
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return Redirect("/");
            }
        }
        public ActionResult StopUPCM(string token, int cmid)
        {
            Log.FormLog(new { _token = token, _cmid = cmid }, "StopUPCM");
            if (string.IsNullOrWhiteSpace(token))
                throw new FieldAccessException("UserdetailsAccessException");

            UserModel user = userBll.GetUserByConfirmationToken(token);
            if (user == null)
                throw new FieldAccessException("UserdetailsAccessException");

            WebSecurityModel wsm = new WebSecurityModel();
            wsm.user = user;
            wsm.userIdNumber = user.idNumber;

            WebSecurity.LoginFromToken(wsm);
            ///TODO=> update confirmationToken
            if (!wsm.success)
                throw new FieldAccessException("UserdetailsAccessException");

            PayCheckUsersByMailModel pcubm = userBll.UpdateUserPayCheckToEmailStatus(new WSPostModel { isMobileRequest = false, idNumber = user.idNumber, currentMunicipalityId = cmid });
            Log.FormLog(pcubm, "StopUPCM");

            return View(wsm);
        }
        
        #region private
        /// <summary>
        /// take the pdf and transform it into single jpeg file
        /// </summary>
        /// <param name="path"></param>
        /// <param name="output"></param>
        /// <param name="input"></param>
        /// <param name="idNumber"></param>
        private void perseImagesAndCombine(string path, string output, string input, string idNumber)
        {
            string ars = string.Format(@"-dBATCH -dNOPAUSE -dSAFER -sPAPERSIZE=a0 -sDEVICE=jpeg -dJPEGQ=90 -r400x400  -dDOINTERPOLATE -sOutputFile={0}-%03d.jpeg {1}", output, input);
            Process proc = new Process();
            proc.StartInfo.FileName = ghostScriptPath;
            proc.StartInfo.Arguments = ars;
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.Start();
            proc.WaitForExit(60000);
            proc.Close();
            Thread.Sleep(5);
            CombineImages(path, idNumber);
        }
        /// <summary>
        /// create a new folder for user
        /// </summary>
        /// <param name="path"></param>
        private void deleteCreateFolder(string path)
        {
            FolderExtensions.DeletFolder(path);
            Thread.Sleep(10);
            FolderExtensions.CreateFolder(path);
            Thread.Sleep(10);
        }
        /// <summary>
        /// take oll seperate images from original pdf and create single image
        /// </summary>
        /// <param name="tempUserFolder"></param>
        /// <param name="idNumber"></param>
        private void CombineImages(string tempUserFolder, string idNumber)
        {
            List<String> imagesList = getAllImages(tempUserFolder);
            int width = 795;
            int height = 1125 * imagesList.Count;
            int imageIndex = 0;
            Image image;

            string Last_jpg = Path.Combine(tempUserFolder, string.Format("{0}.jpeg", idNumber));
            Bitmap Final_img = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(Final_img);
            g.Clear(Color.Black);

            imagesList.ForEach(i =>
            {
                image = Image.FromFile(i);
                g.DrawImage(image, new Point(0, (1125 * imageIndex)));
                imageIndex++;
                image.Dispose();
            });

            g.Dispose();
            Final_img.Save(Last_jpg, System.Drawing.Imaging.ImageFormat.Jpeg);
            Final_img.Dispose();
        }
        /// <summary>
        /// get all `jpeg` in a folder
        /// </summary>
        /// <param name="JpgFolder"></param>
        /// <returns></returns>
        private List<String> getAllImages(string JpgFolder)
        {
            DirectoryInfo Folder;
            FileInfo[] Images;

            Folder = new DirectoryInfo(JpgFolder);
            Images = Folder.GetFiles("*.jpeg", SearchOption.AllDirectories);
            List<String> imagesList = new List<String>();

            for (int i = 0; i < Images.Length; i++)
            {
                imagesList.Add(String.Format(@"{0}/{1}", JpgFolder, Images[i].Name));
            }

            return imagesList;
        }
        private void createUploadsFolder()
        {
            uploads = Path.Combine(HttpContext.Server.MapPath(baseContent), baseFolder);
            FolderExtensions.CreateFolder(uploads);
        }
        #endregion
    }
}