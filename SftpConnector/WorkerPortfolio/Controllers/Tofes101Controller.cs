﻿using System;
using System.Web.Http;

using DataAccess.Models;
using DataAccess.BLLs;
using WorkerPortfolio.App_Start;
using CustomMembership;
using WebServiceProvider;
using DataAccess.Extensions;
using Logger;
using DataAccess.Enums;

namespace WorkerPortfolio.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    [RoutePrefix("api/Tofes101")]
    public class Tofes101Controller : ApiController
    {
        private static readonly DateTime typeSplitDate = new DateTime(2018, 12, 31);
        private UserBll userBll;
        private FormBll formBll;

        public Tofes101Controller()
        {
            userBll = new UserBll();
            formBll = new FormBll();
        }

        [HttpPost]
        [Route("GetFormData")]
        public IHttpActionResult GetForm101Data(WSPostModel model)
        {
            try
            {
                int i = 0;
                if (!WebSecurity.CheckAuthorizetion(model.idNumber, model.confirmationToken))
                    throw new FieldAccessException("UserdetailsAccessException");

                Log.FormLog(string.Format("GetForm101Data => {0}, idNumber => {1}", i++, model.idNumber));
                MunicipalityContactModel municipalitycontactmodel;

                int FactoryId = WebSecurity.GetFactoryId;
                if (FactoryId <= 1)
                {
                    FactoryId = MegaWSProvider.GetMifal(model.calculatedIdNumber.ToInt(), model.currentMunicipalityId, DateTime.Now);
                }
                municipalitycontactmodel = formBll.GetMunicipalityContactByFactoryId(model.currentMunicipalityId, FactoryId, true);

                model.updateUserData = FormsProvider.GetRetirementStatus(new WSPostModel() { calculatedIdNumber = model.calculatedIdNumber, currentMunicipalityId = FactoryId, currentRashutId = model.currentRashutId });

                Log.FormLog(string.Format("GetForm101Data => {0}, idNumber => {1}", i++, model.idNumber));

                EmployerModel employer = new EmployerModel()
                {
                    address = string.Format("{0} {1} {2}", municipalitycontactmodel.city, municipalitycontactmodel.street, municipalitycontactmodel.houseNumber),
                    deductionFileID = municipalitycontactmodel.deductionFileID,
                    displayEmail = !string.IsNullOrWhiteSpace(municipalitycontactmodel.email),
                    email = municipalitycontactmodel.email,
                    emailEmployee = true,
                    emailEmployer = !string.IsNullOrWhiteSpace(municipalitycontactmodel.email),
                    name = municipalitycontactmodel.municipality.name,
                    phoneNumber = municipalitycontactmodel.phone,
                    mifal = municipalitycontactmodel.mifal.ToInt(),
                    rashut = municipalitycontactmodel.rashut.HasValue ? municipalitycontactmodel.rashut.Value : municipalitycontactmodel.mifal.Value
                };
                Log.FormLog(string.Format("GetForm101Data => {0}, idNumber => {1}", i++, model.idNumber));

                TaxFromModel taxfrommodel = formBll.GetBaseForm101Data(model);
                Log.FormLog(string.Format("GetForm101Data => {0}, idNumber => {1}", i++, model.idNumber));

                taxfrommodel.employer = employer;
                taxfrommodel.employee.startWorkTime = formBll.GetUserStartWorkDate(taxfrommodel.employee.ID, model.currentMunicipalityId);

                ///the form type to use
                taxfrommodel.FormType = getFormType(model);
                Log.FormLog(string.Format("GetForm101Data Last => {0}, idNumber => {1}", i++, model.idNumber));

                return Ok(taxfrommodel);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("GetSeventyNineEvent")]
        [AllowAnonymous]
        public IHttpActionResult GetSeventyNineEvent(UserEventModel model)
        {
            try
            {
                Log.FormLog("GetSeventyNineEvent");
                Log.FormLog(model);
                return Ok(userBll.UpdateUserSeventyNineEvent(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }

        #region
        /// <summary>
        /// return the correct form type for the user
        /// current types are `2018`/`2019`
        /// all forms b-4 01/12/2019 are type =>`2018`
        /// after 01/12/2019 form type is =>`2019`
        /// *** TODO=> add `Retired` type
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        private int getFormType(WSPostModel model)
        {
            if (model.updateUserData)
                return (int)FormTypeEnum._Retired;

            if (model.selectedDate < typeSplitDate)
                return (int)FormTypeEnum._2018;

            return (int)FormTypeEnum._2019;
        }
        #endregion
    }
}
