﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DataAccess.Models;
using DataAccess.BLLs;

namespace WorkerPortfolio.Controllers
{
    [RoutePrefix("api/StreetSearch")]
    public class StreetSearchController : ApiController
    {
        private CityBll cityBll;
        private StreetBll streetBll;
        public StreetSearchController()
        {
            cityBll = new CityBll();
            streetBll = new StreetBll();
        }

        [HttpPost]
        [Route("ByID/{query}/{cityid}/{results}")]
        public IHttpActionResult Post(string query, int cityid, int results)
        {
            List<StreetModel> streetList = new List<StreetModel>();
            if (string.IsNullOrWhiteSpace(query))
                return Ok(streetList);
            try
            {
                streetList = cityBll.StreetSearch(cityid, query, results);
            }
            catch{ }

            return Ok(streetList);
        }
        [HttpPost]
        [Route("ByID/{query}/{cityid}")]
        public IHttpActionResult Post(string query, int cityid)
        {
            List<StreetModel> streetList = new List<StreetModel>();
            if (string.IsNullOrWhiteSpace(query))
                return Ok(streetList);
            try
            {
                streetList = cityBll.StreetSearch(cityid, query);
            }
            catch{ }

            return Ok(streetList);
        }
        [HttpPost]
        [Route("ByName/{city}/{query}/{results}")]
        public IHttpActionResult Post(string city, string query, int results)
        {
            List<StreetModel> streetList = new List<StreetModel>();
            if (string.IsNullOrWhiteSpace(query))
                return Ok(streetList);
            try
            {
                streetList = streetBll.StreetSearch(city, query, results);
            }
            catch{ }

            return Ok(streetList);
        }
        [HttpPost]
        [Route("ByName/{city}/{query}")]
        public IHttpActionResult Post(string city, string query)
        {
            List<StreetModel> streetList = new List<StreetModel>();
            if (string.IsNullOrWhiteSpace(query))
                return Ok(streetList);
            try
            {
                streetList = streetBll.StreetSearch(city, query);
            }
            catch{ }

            return Ok(streetList);
        }
    }
}