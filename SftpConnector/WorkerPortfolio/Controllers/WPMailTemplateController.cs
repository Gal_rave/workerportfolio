﻿using System.Web.Mvc;
using DataAccess.Models;
using System;

namespace WorkerPortfolio.Controllers
{
    public class WPMailTemplateController : Controller
    {
        public ActionResult Index(UserCredentialsModel model)
        {
            return View(model);
        }
        public ActionResult PasswordReset(UserCredentialsModel model)
        {
            return View(model);
        }
        public ActionResult NewUserRegister(UserCredentialsModel model)
        {
            return View(model);
        }
        public ActionResult Form101SeccessEmail(UserCredentialsModel model)
        {
            return View(model);
        }
    }
}