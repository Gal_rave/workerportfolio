﻿using CustomMembership;
using DataAccess.BLLs;
using DataAccess.Models;
using Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WorkerPortfolio.Controllers
{
    public class AuthenticationController : Controller
    {
        public ActionResult success()
        {
            return View();
        }
        public ActionResult Error()
        {
            return View();
        }
        public ActionResult ConfirmAccount(string confirmationToken)
        {
            try
            {
                WebSecurityModel model = WebSecurity.ConfirmAccount(confirmationToken);
                if (model.success)
                {
                    return Redirect("/#/authentication/authenticateduserlogin/" + confirmationToken);
                }
                return Redirect("/#/error/" + confirmationToken);
            }
            catch
            {
                return Redirect("/#/error/" + confirmationToken);
            }
        }
        public ActionResult RegisterAccount(string confirmationToken)
        {
            MembershipProviderBll bll = new MembershipProviderBll();
            UserModel model = bll.GetUserByConfirmationToken(confirmationToken);
            if (model != null)
            {
                return Redirect("/#/authentication/authenticateduserlogin/" + confirmationToken);
            }
            return Redirect("/#/error/" + confirmationToken);
        }
        public ActionResult AuthenticateKadAccount(string Token, int rashut, int mncpility)
        {
            try
            {
                WebSecurityModel model = new WebSecurityModel(); 
                UserModel user = CustomMembershipProvider.ValidateUserWithToken(Token);
                if (user != null)
                {
                    model.updateByUser(user);
                    WebSecurity.LoginFromToken(model);
                    if(model.success)
                    {
                        return Redirect("/#/authentication/logintoprocess/" + user.membership.confirmationToken + "/" + rashut + "/" + mncpility);
                    }
                }
                return Redirect("/#/error/" + user.membership.confirmationToken);
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return Redirect("/#/error/");
            }
        }
    }
}