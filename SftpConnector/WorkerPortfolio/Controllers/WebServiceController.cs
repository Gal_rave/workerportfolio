﻿using System;
using System.IO;
using System.Net;
using System.Web.Http;
using System.Configuration;
using System.Collections.Generic;

using WorkerPortfolio.App_Start;
using DataAccess.Extensions;
using WebServiceProvider;
using DataAccess.Models;
using CustomMembership;
using DataAccess.BLLs;
using Logger;
using System.Web;
using System.Drawing;
using System.Threading;
using System.Diagnostics;
using AsyncHelpers;

namespace WorkerPortfolio.Controllers
{
    [Authorize]
    [RoutePrefix("api/WebService")]
    [InitializeSimpleMembership]
    public class WebServiceController : ApiController
    {
        private UserBll userBll;
        private readonly string baseContent = "~/Content";
        private readonly string baseFolder = "uploads";
        private string ghostScriptPath = string.Empty;
        private string uploads = string.Empty;

        public WebServiceController()
        {
            this.userBll = new UserBll();
            ghostScriptPath = ConfigurationManager.AppSettings["GhostScriptDllPath"];
        }
        
        [HttpPost]
        [Route("General")]
        public IHttpActionResult GeneralPayrollData(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                WSTableModel wsRes = WSProvider.GetNetuneSacharTable(model.calculatedIdNumber.ToInt(), model.currentMunicipalityId, model.selectedDate.Year);
                return Ok(wsRes);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("BySymbol")]
        public IHttpActionResult PayrollBySymbol(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                WSTableModel wsRes = WSProvider.GetNetuneSacharNivharimTable(model.calculatedIdNumber.ToInt(), model.currentMunicipalityId, model.selectedDate.Year);
                return Ok(wsRes);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("Funds")]
        public IHttpActionResult GetFundsTable(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                List<WSTableModel> wsRes = WSProvider.GetFundsTable(model.calculatedIdNumber.ToInt(), model.currentMunicipalityId, model.selectedDate.Year);
                return Ok(wsRes);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("Form106")]
        public IHttpActionResult Get106Form(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                UserImageModel image = FormsProvider.Get106(model.idNumber, model.calculatedIdNumber.ToInt(), model.currentMunicipalityId, model.selectedDate);
                if (model.isMobileRequest.HasValue && model.isMobileRequest.Value && (image.fileimageblob == null || image.fileimageblob.Length < 1000))
                {
                    createUploadsFolder();
                    string tempUserFolder = Path.Combine(uploads, model.idNumber);
                    string outputPath = Path.Combine(tempUserFolder, "TEMP_");
                    string inputPath = Path.Combine(tempUserFolder, string.Format("{0}.pdf", model.idNumber));

                    #region delete/create the folder
                    deleteCreateFolder(tempUserFolder);
                    #endregion

                    System.IO.File.WriteAllBytes(inputPath, image.fileBlob);

                    #region perse pdf into images then combine images
                    perseImagesAndCombine(tempUserFolder, outputPath, inputPath, model.idNumber);
                    #endregion

                    #region save the new image into DB
                    image.fileimageblob = System.IO.File.ReadAllBytes(Path.Combine(tempUserFolder, string.Format("{0}.jpeg", model.idNumber)));
                    this.userBll.CreateUserImage(image);
                    deletFolder(tempUserFolder);
                    #endregion
                }
                return Ok(image);
            }
            catch
            {
                return Content((HttpStatusCode)406, "Form106");
            }
        }
        [HttpPost]
        [Route("Paycheck")]
        public IHttpActionResult GetPaycheck(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                UserImageModel image = FormsProvider.GetPaycheck(model.idNumber, model.calculatedIdNumber.ToInt(), model.currentMunicipalityId, model.selectedDate);
                if (model.isMobileRequest.HasValue && model.isMobileRequest.Value && (image.fileimageblob == null || image.fileimageblob.Length < 1000))
                {
                    createUploadsFolder();
                    string tempUserFolder = Path.Combine(uploads, model.idNumber);
                    string outputPath = Path.Combine(tempUserFolder, "TEMP_");
                    string inputPath = Path.Combine(tempUserFolder, string.Format("{0}.pdf", model.idNumber));

                    #region delete/create the folder
                    deleteCreateFolder(tempUserFolder);
                    #endregion

                    #region perse pdf into images then combine images
                    System.IO.File.WriteAllBytes(inputPath, image.fileBlob);
                    perseImagesAndCombine(tempUserFolder, outputPath, inputPath, model.idNumber);
                    #endregion

                    #region save the new image into DB
                    image.fileimageblob = System.IO.File.ReadAllBytes(Path.Combine(tempUserFolder, string.Format("{0}.jpeg", model.idNumber)));
                    this.userBll.CreateUserImage(image);
                    deletFolder(tempUserFolder);
                    #endregion
                }
                return Ok(image);
            }
            catch
            {
                return Content((HttpStatusCode)406, "Paycheck");
            }
        }
        [HttpPost]
        [Route("PersonalDetails")]
        public IHttpActionResult GetPersonalDetails(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");
                EmployeeDada employee = SystemUsersProvider.GetPersonalInformation(model.calculatedIdNumber, model.currentRashutId);
                return Ok(employee);
            }
            catch
            {
                return Content((HttpStatusCode)406, "PersonalDetails");
            }
        }
        [HttpPost]
        [Route("AttendanceFigures")]
        public IHttpActionResult GetAttendanceFigures(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                var obj = WSProvider.GetEmployeeAttendanceFiguresData(model.calculatedIdNumber.ToInt(), model.currentMunicipalityId, model.selectedDate.Year);
                return Ok(obj);
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return Content((HttpStatusCode)406, "AttendanceFigures");
            }
        }
        [HttpPut]
        [Route("USPCTE")]
        public IHttpActionResult UserSendPayCheckToEmail(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                PayCheckUsersByMailModel pc = this.userBll.UpdateUserPayCheckToEmailStatus(model);
                if (pc != null)
                {
                    try
                    {
                        FormsProvider.SynchronizeUPCBMFromSite(pc);
                    }
                    catch (Exception Ie)
                    {
                        this.ServerError(Ie);
                    }
                }

                return Ok(pc);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPut]
        [Route("ValidateUserAndSendToken")]
        public IHttpActionResult ValidateUserAndSendToken(WSPostExtensionModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");

                /* ACTIVATE TO ADD PASSWORD CHECK */
                /*
                WebSecurityModel securityModel = CustomMembershipProvider.ValidateUser(model.idNumber, model.Password);
                if (!securityModel.success)
                {
                    model.exceptionId = 10;
                    model.success = false;
                    return Ok(model);
                }
                */

                WebSecurityModel securityModel = WebSecurity.SendTokenToUser(model);
                if (!securityModel.success)
                {
                    model.exceptionId = 20;
                    model.success = false;
                    return Ok(model);
                }

                model.exceptionId = 0;
                model.success = true;
                return Ok(model);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPut]
        [Route("UpdateUserData")]
        public IHttpActionResult UpdateUserData(WSPostExtensionModel model)
        {
            try
            {
                if (!WebSecurity.CheckAuthorizetion(model))
                    throw new FieldAccessException("UserdetailsAccessException");
                if (!CustomMembershipProvider.ValidateUserToken(model))
                {
                    model.exceptionId = 30;
                    model.success = false;
                    return Ok(model);
                }
                RegisterBll registerBll = new RegisterBll();

                if (!registerBll.UpdateUserMunicipalityContact(model))
                {
                    model.exceptionId = 40;
                    model.success = false;
                    return Ok(model);
                }

                AsyncFunctionCaller.RunAsync(HttpContext.Current, () =>
                {
                    try
                    {
                        List<UPDATEUSERMUNICIPALITYCONTACT> uumc = registerBll.GetOutStandingUUMC();
                        uumc.ForEach(u => {
                            try
                            {
                                if (FormsProvider.PostUserDataToOperationalSystem(u))
                                {
                                    u.UPDATESTATUS = true;
                                    registerBll.UpdateUUMCStatus(u);
                                }
                            }
                            catch (Exception IIe)
                            {
                                this.ServerError(IIe);
                            }
                        });
                    }
                    catch (Exception Ie)
                    {
                        this.ServerError(Ie);
                    }
                });

                model.exceptionId = 0;
                model.success = true;
                return Ok(model);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        
        #region private
        /// <summary>
        /// take the pdf and transform it into single jpeg file
        /// </summary>
        /// <param name="path"></param>
        /// <param name="output"></param>
        /// <param name="input"></param>
        /// <param name="idNumber"></param>
        private void perseImagesAndCombine(string path, string output, string input, string idNumber)
        {
            string ars = string.Format(@"-dBATCH -dNOPAUSE -dSAFER -sPAPERSIZE=a0 -sDEVICE=jpeg -dJPEGQ=100 -r600x600  -dDOINTERPOLATE -sOutputFile={0}-%03d.jpeg {1}", output, input);
            Process proc = new Process();
            proc.StartInfo.FileName = ghostScriptPath;
            proc.StartInfo.Arguments = ars;
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.Start();
            proc.WaitForExit(60000);
            proc.Close();
            Thread.Sleep(5);
            CombineImages(path, idNumber);
        }
        /// <summary>
        /// create a new folder for user
        /// </summary>
        /// <param name="path"></param>
        private void deleteCreateFolder(string path)
        {
            deletFolder(path);
            Thread.Sleep(5);
            createFolder(path);
            Thread.Sleep(5);
        }
        private bool createFolder(string path)
        {
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }
        private bool deletFolder(string path)
        {
            if (Directory.Exists(path))
            {
                try
                {
                    Directory.Delete(path, true);
                    Thread.Sleep(5);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }
        /// <summary>
        /// take oll seperate images from original pdf and create single image
        /// </summary>
        /// <param name="tempUserFolder"></param>
        /// <param name="idNumber"></param>
        private void CombineImages(string tempUserFolder, string idNumber)
        {
            List<String> imagesList = getAllImages(tempUserFolder);

            int width = 795;
            int height = 1125 * imagesList.Count;
            int imageIndex = 0;
            Image image;

            string Last_jpg = Path.Combine(tempUserFolder, string.Format("{0}.jpeg", idNumber));
            Bitmap Final_img = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(Final_img);
            g.Clear(Color.Black);

            imagesList.ForEach(i =>
            {
                image = Image.FromFile(i);
                g.DrawImage(image, new Point(0, (1125 * imageIndex)));
                imageIndex++;
                image.Dispose();
            });

            g.Dispose();
            Final_img.Save(Last_jpg, System.Drawing.Imaging.ImageFormat.Jpeg);
            Final_img.Dispose();
        }
        /// <summary>
        /// get all `jpeg` in a folder
        /// </summary>
        /// <param name="JpgFolder"></param>
        /// <returns></returns>
        private List<String> getAllImages(string JpgFolder)
        {
            DirectoryInfo Folder;
            FileInfo[] Images;

            Folder = new DirectoryInfo(JpgFolder);
            Images = Folder.GetFiles("*.jpeg", SearchOption.AllDirectories);
            List<String> imagesList = new List<String>();

            for (int i = 0; i < Images.Length; i++)
            {
                imagesList.Add(String.Format(@"{0}/{1}", JpgFolder, Images[i].Name));
            }

            return imagesList;
        }
        private void createUploadsFolder()
        {
            uploads = Path.Combine(HttpContext.Current.Server.MapPath(baseContent), baseFolder);
            createFolder(uploads);
        }
        #endregion
    }
}