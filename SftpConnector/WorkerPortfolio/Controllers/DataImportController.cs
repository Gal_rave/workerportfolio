﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Web;
using System.Threading;
using System.Configuration;
using System.Web.Routing;

using Logger;
using WebServiceProvider;
using DataAccess.Models;
using AsyncHelpers;
using CustomMembership;
using DataAccess.Extensions;
using DataAccess.BLLs;
using WorkerPortfolio.App_Start;
using Mail;

namespace WorkerPortfolio.Controllers
{
    [RoutePrefix("DataImporter")]
    public class DataImportController : ApiController
    {
        #region private vars
        private UserBll userbll;
        private RegisterBll registerBll;
        private SetUpBll setUpBll;
        private FormBll formBll;
        private ProcessBll processBll;
        private int PeyCheckStartMonth;
        private bool isDebugeMode;

        private static int fileCounter;
        private static object startDataImportFromWS_Lock = new object();
        private static object RunUsersUpdateSequence_Lock = new object();
        private static object InternalBulkRegister_Lock = new object();
        private static object InternalBulkRegisterByMunicipality_Lock = new object();
        private static object reDoSendEvent_Lock = new object();
        private static object customImport_Lock = new object();
        private static object schoolImport_Lock = new object();
        private static object processesImport_Lock = new object();
        private static object updateProcesses_Lock = new object();
        private static object sacharUsersImport_Lock = new object();
        private static object orgUnitMangrsImport_Lock = new object();
        private static object rashutNumberImport_Lock = new object();
        private static object SynchronizeUserPayCheckByMail_Lock = new object();
        private static object CreateMobileImages_Lock = new object();
        private static object ProcessesStoredProcedures_Lock = new object();
        private static object BirthdayGreetings_Lock = new object();
        #endregion

        public DataImportController()
        {
            this.userbll = new UserBll();
            this.registerBll = new RegisterBll();
            this.setUpBll = new SetUpBll();
            this.formBll = new FormBll();
            this.processBll = new ProcessBll();

            PeyCheckStartMonth = ConfigurationManager.AppSettings["PeyCheckStartMonth"].ToInt();
        }

        [HttpGet]
        [Route("ImportCustomMunicipalities/{municipalities}")]
        public IHttpActionResult ImportCustomMunicipalities(string municipalities)
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "ImportCustomMunicipalities");
            fileCounter = 0;
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(startDataImportFromWS_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (startDataImportFromWS_Lock)
                        {
                            SystemUsersProvider.StartImportCustomMunicipalitiesFromWS(municipalities);
                        }
                    }
                    finally
                    {
                        Monitor.Exit(startDataImportFromWS_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("StartImportSingle/{municipality}")]
        public IHttpActionResult ImportSingle(int municipality)
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "StartImportSingle");
            fileCounter = 0;
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(startDataImportFromWS_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (startDataImportFromWS_Lock)
                        {
                            SystemUsersProvider.StartImportmunicipalityProcessFromWS(municipality);
                        }
                    }
                    finally
                    {
                        Monitor.Exit(startDataImportFromWS_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("ImportCitiesStreets/{importType}")]
        public IHttpActionResult ImportCitiesStreets(bool importType)
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "ImportCitiesStreets");
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(customImport_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (customImport_Lock)
                        {
                            if (importType)
                            {
                                CityBll city = new CityBll();
                                city.ImportCitiesFromWS(SystemUsersProvider.GetCitiesFromWS());
                            }
                            else
                            {
                                StreetBll street = new StreetBll();
                                street.ImportStreetsFromWS(SystemUsersProvider.GetStreetsFromWS());
                            }
                        }
                    }
                    finally
                    {
                        Monitor.Exit(customImport_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("ImportProcessesData")]
        public IHttpActionResult ImportProcessesData()
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "ImportProcessesData");
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(processesImport_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (processesImport_Lock)
                        {
                            RegisterBll bll = new RegisterBll();
                            List<int> municipalities = bll.GetAllmunicipalities(true, true);

                            List<TahalichModel> tahalichim;
                            List<StepModel> shlavim;
                            List<ReshimatTiyugModel> reshimotTiyug;
                            List<MismachBereshimaModel> mismachimBeReshima;
                            List<MismachModel> mismachim;
                            List<KvuzatMishtamshimModel> kvuzotMishtamshim;
                            List<MishtamsheiKvuzaModel> mishtamsheiKvuza;


                            foreach (int mun in municipalities)
                            {
                                Log.ApplicationLog(string.Join(",", mun, "StartImportProcessesData"));
                                List<PackedProcessModel> packedProcessModel = SystemUsersProvider.GetPackedProcessesData(mun);
                                Log.ApplicationLog(string.Join(",", mun, "EndImportProcessesData"));

                                if (packedProcessModel.Count() > 0 && packedProcessModel != null)
                                {
                                    tahalichim = new List<TahalichModel>();
                                    shlavim = new List<StepModel>();

                                    reshimotTiyug = new List<ReshimatTiyugModel>();
                                    mismachimBeReshima = new List<MismachBereshimaModel>();
                                    mismachim = new List<MismachModel>();
                                    kvuzotMishtamshim = new List<KvuzatMishtamshimModel>();
                                    mishtamsheiKvuza = new List<MishtamsheiKvuzaModel>();

                                    reArrangeProcessesData(packedProcessModel, tahalichim, shlavim, reshimotTiyug, mismachimBeReshima, mismachim, kvuzotMishtamshim, mishtamsheiKvuza);//only processes and steps 

                                    Log.ApplicationLog(string.Join(",", mun, "EndImportProcessesData"));

                                    int rashut = tahalichim.Count > 0 ? tahalichim.First().rashut : 0;
                                    processBll.SaveTahalichimData(tahalichim, shlavim, reshimotTiyug, mismachimBeReshima, mismachim, kvuzotMishtamshim, mishtamsheiKvuza, rashut);
                                }
                            }
                        }
                    }
                    catch (Exception Ie)
                    {
                        return this.ServerError(Ie);
                    }
                    finally
                    {
                        Monitor.Exit(processesImport_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("ImportSacharUsers")]
        public IHttpActionResult ImportSacharUsers()
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "ImportSacharUsers");
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(sacharUsersImport_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (sacharUsersImport_Lock)
                        {
                            RegisterBll bll = new RegisterBll();
                            List<int> municipalities = bll.GetAllmunicipalities(true, true);

                            List<SACHAR_USERS_GELEM> sacharUsers = SystemUsersProvider.GetSacharSystemUsers(municipalities);

                            Log.ApplicationLog(string.Join(",", municipalities, "ImportSacharUsers"));

                            if (sacharUsers.Count() > 0 && sacharUsers != null)
                            {
                                Log.ApplicationLog(string.Join(",", "stored procedure insert sachar users", "ImportSacharUsers"));
                                processBll.synchronizeUsersAndSave(sacharUsers);
                            }
                        }
                    }
                    catch (Exception Ie)
                    {
                        return this.ServerError(Ie);
                    }
                    finally
                    {
                        Monitor.Exit(sacharUsersImport_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("ImportAcademicInst")]
        public IHttpActionResult ImportAcademicInst()
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "ImportAcademicInst");
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(schoolImport_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (schoolImport_Lock)
                        {
                            //academicWrraper allAcademicInsts = SystemUsersProvider.GetAcademicInstFromWS();
                            //processBll.saveAcademicInsts(allAcademicInsts);
                        }
                    }
                    catch (Exception Ie)
                    {
                        return this.ServerError(Ie);
                    }
                    finally
                    {
                        Monitor.Exit(schoolImport_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("ImportOrganizationUnitManagers")]
        public IHttpActionResult ImportOrganizationUnitManagers()
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "ImportOrganizationUnitManagers");
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(orgUnitMangrsImport_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (orgUnitMangrsImport_Lock)
                        {
                            RegisterBll bll = new RegisterBll();
                            List<int> municipalities = bll.GetAllmunicipalities(true, true);

                            foreach (int mun in municipalities)
                            {
                                Log.ApplicationLog(string.Join(",", mun, "StartImportOrganizationUnitManagers"));
                                List<UnitManagerModel> unitManagers = SystemUsersProvider.getOrganizationUnitsManagers(mun);
                                Log.ApplicationLog(string.Join(",", mun, "EndImportOrganizationUnitManagers"));
                                int rashut = unitManagers.Count > 0 ? unitManagers.First().rashutNumber : 0;
                                processBll.SaveUnitMngrs(unitManagers, rashut);
                            }
                            processBll.SyncUnitMngrsAndUsers();
                        }
                    }
                    catch (Exception Ie)
                    {
                        return this.ServerError(Ie);
                    }
                    finally
                    {
                        Monitor.Exit(orgUnitMangrsImport_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("UpdateProcessesStatus")]
        public IHttpActionResult UpdateProcessesStatus()
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "UpdateProcessesStatus");
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(updateProcesses_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (updateProcesses_Lock)
                        {
                            RegisterBll bll = new RegisterBll();
                            ProcessBll processBll = new ProcessBll();
                            List<int> municipalities = bll.GetAllmunicipalities(true, true);//get the active municipalities which have processes module

                            List<UpdateProcessModel> processesResults = SystemUsersProvider.GetProcessesStatus(municipalities);

                            if (processesResults.Count() > 0 && processesResults != null)
                            {
                                List<int> sentMailProcesses = new List<int>();

                                foreach (UpdateProcessModel process in processesResults)
                                {
                                    List<PROCESS> prcs = processBll.getProcessesByTmIdAndRashut(process.tmProcessId, process.customerNumber);//
                                    if (prcs != null)
                                    {
                                        foreach (PROCESS pr in prcs)
                                        {
                                            if (pr.STATUS == 3)//if the process already completed -> do nothing
                                            {
                                                continue;
                                            }
                                            else//update new status section
                                            {
                                                if (process.processStatus == 2)//if new status is completed then send suitable emails (2 is completed in java)
                                                {
                                                    if (!mailSentAlready(sentMailProcesses, process.tmProcessId))
                                                    {
                                                        ProcessModel p = processBll.getProcessRecord(pr.ID);
                                                        sendProcessCompletedEmails(p, process);
                                                        sentMailProcesses.Add(process.tmProcessId);
                                                    }
                                                }
                                                processBll.updateProcessRecordStatus(pr, process);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception Ie)
                    {
                        return this.ServerError(Ie);
                    }
                    finally
                    {
                        Monitor.Exit(updateProcesses_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("StartDataImportFromWS")]
        public IHttpActionResult StartDataImportFromWS()
        {
            Log.LogLoad(HttpContext.Current.Request.Url, "startDataImportFromWS");
            fileCounter = 0;
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(startDataImportFromWS_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (startDataImportFromWS_Lock)
                        {
                            ///set extended timeout
                            HttpContext.Current.Server.ScriptTimeout = 3600;

                            SystemUsersProvider.StartImportProcessFromWS();
                            ///delete old directories
                            FolderExtensions.CleanDirectory(Path.Combine(HttpContext.Current.Server.MapPath("~/Content"), "uploads"), 90);
                        }
                    }
                    finally
                    {
                        Monitor.Exit(startDataImportFromWS_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("RunUsersUpdateSequence")]
        public IHttpActionResult RunUsersUpdateSequence()
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "RunUsersUpdateSequence");
            fileCounter = 0;
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {

                    if (!Monitor.TryEnter(RunUsersUpdateSequence_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (RunUsersUpdateSequence_Lock)
                        {
                            ///set extended timeout
                            HttpContext.Current.Server.ScriptTimeout = 3600;

                            SystemUsersProvider.RunImportSystemUsers();
                            DistinctContactData();
                        }
                    }
                    finally
                    {
                        Monitor.Exit(RunUsersUpdateSequence_Lock);
                    }
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpGet]
        [Route("InternalBulkRegister")]
        public bool InternalBulkRegister()
        {
            bool s = false;
            Log.LogLoad(HttpContext.Current.Request.Url, "InternalBulkRegister");
            fileCounter = 0;
            if (HttpContext.Current.Request.IsLocal)
            {

                if (!Monitor.TryEnter(InternalBulkRegister_Lock, new TimeSpan(0)))
                {
                    throw new Exception("ServiceBusyException_Locked!");
                }
                try
                {
                    lock (InternalBulkRegister_Lock)
                    {
                        ///set extended timeout
                        HttpContext.Current.Server.ScriptTimeout = 3600;

                        registerBll.RemoveEmptyUsers();
                        List<int> municipalitis = registerBll.GetAllmunicipalities(true);
                        Log.LogLoad(string.Join(",", municipalitis), "InternalBulkRegister -> municipalitis");
                        foreach (int municipality in municipalitis)
                        {
                            registerByMunicipality(municipality);
                        }
                        registerBll.ConnectEmailsByMunicipality();
                        DistinctContactData();
                        WebSecurity.SendWellcomeEmails();
                        Log.LogLoad("after SendWellcomeEmails");
                        registerBll.RemoveEmptyUsers();
                        s = true;
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(InternalBulkRegister_Lock);
                }
            }
            return s;
        }
        [HttpGet]
        [Route("BulkRegisterByMunicipality")]
        public bool InternalBulkRegisterByMunicipality(string municipality)
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "BulkRegisterByMunicipality");
            fileCounter = 0;
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {
                    if (!Monitor.TryEnter(InternalBulkRegisterByMunicipality_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("ServiceBusyException_Locked!");
                    }
                    try
                    {
                        lock (InternalBulkRegisterByMunicipality_Lock)
                        {
                            registerBll.RemoveEmptyUsers();

                            if (municipality.IndexOf(",") < 0)
                            {
                                registerByMunicipality(municipality.ToInt());
                            }
                            else
                            {
                                foreach (int num in municipality.ToIntLIst())
                                {
                                    registerByMunicipality(num.ToInt());
                                }
                            }

                            registerBll.RemoveEmptyUsers();
                            WebSecurity.SendWellcomeEmails();
                        }
                    }
                    finally
                    {
                        Monitor.Exit(InternalBulkRegisterByMunicipality_Lock);
                    }
                }
            }
            catch (Exception e)
            {
                this.ServerError(e);
            }
            return true;
        }
        [HttpPost]
        [Route("GetFile")]
        public async Task<bool> GetFilesFromApi()
        {
            Log.ApplicationLog(string.Format("GetFile ({0})", fileCounter));
            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

                var provider = new MultipartMemoryStreamProvider();
                string uploads = Path.Combine(HttpContext.Current.Server.MapPath("~/Content"), "uploads");
                FolderExtensions.CreateFolder(uploads);
                await Request.Content.ReadAsMultipartAsync(provider);

                foreach (HttpContent file in provider.Contents)
                {
                    string filename = file.Headers.ContentDisposition.FileName.Trim('\"');
                    byte[] buffer = await file.ReadAsByteArrayAsync();
                    string result = System.Text.Encoding.UTF8.GetString(buffer);

                    SystemUsersProvider.SaveTemporerySystemUsers(result, Path.Combine(uploads, filename), fileCounter++);
                }

                await Task.Delay(350);
                return true;
            }
            catch (Exception e)
            {
                throw e.LogError();
            }
        }
        [HttpGet]
        [Route("ReDoSendEvent")]
        public IHttpActionResult ReDoSendEvent()
        {
            int index = 0;
            Log.LogLoad("ReDoSendEvent Start");
            try
            {
                if (HttpContext.Current.Request.IsLocal)
                {
                    if (!Monitor.TryEnter(reDoSendEvent_Lock, new TimeSpan(0)))
                    {
                        throw new Exception("reDoSendEvent_Lock!");
                    }
                    try
                    {
                        lock (reDoSendEvent_Lock)
                        {

                            try
                            {
                                DateTime start = DateTime.Now;
                                string year = (start.Year).ToString();
                                Log.LogLoad(string.Format("ReDoSendEvent get Year => {0}", year));

                                List<Form101DataExtensionModel> ErroneousForm = formBll.GetErroneousForm101(year);
                                Create101FormAction CreateForm = new Create101FormAction();

                                ErroneousForm.ForEach(f =>
                                {
                                    try
                                    {
                                        Log.LogLoad(string.Format("ReDoSendEvent Start Loop Step {0}, isLast=> {1}", index, ((index + 1) == ErroneousForm.Count)));

                                        CreateForm.ReCreateFull101Form(f, ((index + 1) == ErroneousForm.Count));

                                        Log.LogLoad(string.Format("ReDoSendEvent End Loop Step {0}", index++));
                                    }
                                    catch (Exception IIe)
                                    {
                                        this.ServerError(IIe);
                                    }
                                });
                                AsyncFunctionCaller.RunAsyncAll();

                                Log.LogLoad(new { _start = start, now = DateTime.Now, count = ErroneousForm.Count });

                                return Ok(index);
                            }
                            catch (Exception Ie)
                            {
                                return this.ServerError(Ie);
                            }
                        }
                    }
                    finally
                    {
                        Monitor.Exit(reDoSendEvent_Lock);
                    }
                }
                Log.LogLoad("ReDoSendEvent End");
                return Ok(index);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }

        }
        [HttpGet]
        [Route("SynchronizeUPCBM")]
        public bool SynchronizeUserPayCheckByMail()
        {
            bool s = false;
            Log.PcuLog(HttpContext.Current.Request.Url, "SynchronizeUserPayCheckByMail");
            if (HttpContext.Current.Request.IsLocal)
            {

                if (!Monitor.TryEnter(SynchronizeUserPayCheckByMail_Lock, new TimeSpan(0)))
                {
                    throw new Exception("ServiceBusyException_Locked!");
                }
                try
                {
                    lock (SynchronizeUserPayCheckByMail_Lock)
                    {
                        ///set extended timeout
                        HttpContext.Current.Server.ScriptTimeout = 3600;

                        int index = 0;
                        DateTime NOW = DateTime.Now;
                        Log.PcuLog(string.Format("start import, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds));

                        List<Tlush_MailModel> wsUsers = FormsProvider.GetUsersForEmailPayCheckFromWS(0);

                        Log.PcuLog(new { counter = wsUsers.Count }, "GetUsersForEmailPayCheckFromWS");

                        wsUsers.ForEach(gu =>
                        {
                            Log.PcuLog(gu, "UpdateUserStatus");
                            this.userbll.UpdateUserPayCheckToEmailStatus(gu);
                        });
                        Log.PcuLog(string.Format("end import, wsUsers => {1}, time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, wsUsers.Count));

                        List<PayCheckUsersByMailModel> siteUsers = setUpBll.SynchronizeUsersPayCheckByMail();
                        siteUsers.ForEach(u =>
                        {
                            try
                            {
                                Log.PcuLog(u, "SynchronizeUPCBMFromSite");

                                AsyncFunctionCaller.RunAsync(HttpContext.Current, () => { FormsProvider.SynchronizeUPCBMFromSite(u); });
                                index++;
                                if (index % 25 == 0)
                                {
                                    Log.PcuLog(string.Format("index => {0}, time => {1}", index, DateTime.Now.Subtract(NOW).TotalSeconds));
                                    AsyncFunctionCaller.RunAsyncAll();
                                }
                            }
                            catch (Exception Ie)
                            {
                                this.ServerError(Ie);
                            }
                        });
                        AsyncFunctionCaller.RunAsyncAll();
                        Log.PcuLog(string.Format("index => {0}, siteUsers => {2}, time => {1}", index, DateTime.Now.Subtract(NOW).TotalSeconds, siteUsers.Count));
                        s = true;
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(SynchronizeUserPayCheckByMail_Lock);
                }
            }
            return s;
        }
        [HttpGet]
        [Route("PreCollectPayChecks")]
        public bool PreCollectPayChecks()
        {
            Log.PcuLog(HttpContext.Current.Request.Url, "PreCollectPayChecks");
            try
            {
                if (!HttpContext.Current.Request.IsLocal)
                    throw new EntryPointNotFoundException();

                if (!Monitor.TryEnter(SynchronizeUserPayCheckByMail_Lock, new TimeSpan(0)))
                    throw new Exception("ServiceBusyException_Locked!");
                try
                {
                    lock (SynchronizeUserPayCheckByMail_Lock)
                    {
                        ///set extended timeout
                        HttpContext.Current.Server.ScriptTimeout = 3600;

                        this.isDebugeMode = ConfigurationManager.AppSettings["isDebugeMode"].ToBool();
                        DateTime NOW = DateTime.Now;
                        int index = 0;
                        MunicipalityUpcbmModel municipalityUpcbmModel;
                        DateTime currentPayMonth;

                        Log.PcuLog(string.Format("step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "PreCollectPayChecks");
                        List<int> factorPCmunicipalities = setUpBll.GetMunicipalitiesForUPCBM(true);
                        factorPCmunicipalities.AddRange(setUpBll.GetMunicipalitiesForUPCBM());
                        Log.PcuLog(string.Format("step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "PreCollectPayChecks");
                        foreach (int municipality in factorPCmunicipalities)
                        {
                            Log.PcuLog(string.Format("step {1} time => {0}, start CheckRashutLastPayCheckMonth municipality => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, municipality), "PreCollectPayChecks");
                            municipalityUpcbmModel = FormsProvider.CheckRashutLastPayCheckMonth(municipality);
                            Log.PcuLog(municipalityUpcbmModel, "municipalityUpcbmModel");
                            if (municipalityUpcbmModel.success && municipalityUpcbmModel.PayCounter > 0)
                            {
                                Log.PcuLog(string.Format("step {1} time => {0}, start preCollectPayChecks municipality => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, municipality), "PreCollectPayChecks");

                                currentPayMonth = DateExtension.maxDateToDate(municipalityUpcbmModel);
                                preCollectPayChecks(currentPayMonth, municipality);
                                Log.PcuLog(string.Format("step {1} time => {0}, end preCollectPayChecks municipality => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, municipality), "PreCollectPayChecks");
                            }
                        }

                        AsyncFunctionCaller.RunAsyncAll();
                        Log.PcuLog(string.Format("Last step! {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "PreCollectPayChecks");
                    }
                }
                finally
                {
                    Monitor.Exit(SynchronizeUserPayCheckByMail_Lock);
                }
                return true;
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return false;
            }
        }
        [HttpGet]
        [Route("SendToRegisterdUsers")]
        public bool SendPayCheckToRegisterdUsers()
        {
            Log.PcuLog(HttpContext.Current.Request.Url, "SendPayCheckToRegisterdUsers");
            try
            {
                if (!HttpContext.Current.Request.IsLocal)
                    throw new EntryPointNotFoundException();

                if (!Monitor.TryEnter(SynchronizeUserPayCheckByMail_Lock, new TimeSpan(0)))
                    throw new Exception("ServiceBusyException_Locked!");

                try
                {
                    lock (SynchronizeUserPayCheckByMail_Lock)
                    {
                        ///set extended timeout
                        HttpContext.Current.Server.ScriptTimeout = 3600;

                        this.isDebugeMode = ConfigurationManager.AppSettings["isDebugeMode"].ToBool();
                        DateTime NOW = DateTime.Now;
                        int index = 0;
                        Log.PcuLog(string.Format("step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "SendPayCheckToRegisterdUsers");

                        List<PayCheckUsersByMailModel> siteUsers;
                        List<UserImageModel> payChecks;
                        MunicipalityUpcbmModel municipalityUpcbmModel;
                        DateTime currentPayMonth;

                        Log.PcuLog(string.Format("step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "SendPayCheckToRegisterdUsers");

                        #region send by factory flag
                        List<int> factorPCmunicipalities = setUpBll.GetMunicipalitiesForUPCBM(true);
                        Log.PcuLog(string.Join(",", factorPCmunicipalities), "MUNICIPALITIES=>> send by factory flag");
                        if (!this.isDebugeMode)
                        {
                            foreach (int municipality in factorPCmunicipalities)
                            {
                                payChecks = new List<UserImageModel>();
                                siteUsers = new List<PayCheckUsersByMailModel>();

                                Log.PcuLog(string.Format("step {1} time => {0}, start CheckRashutLastPayCheckMonth municipality => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, municipality), "SendPayCheckToRegisterdUsers");

                                municipalityUpcbmModel = FormsProvider.CheckRashutLastPayCheckMonth(municipality);
                                Log.PcuLog(municipalityUpcbmModel, "municipalityUpcbmModel");

                                Log.PcuLog(string.Format("step {1} time => {0}, end CheckRashutLastPayCheckMonth municipality => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, municipality), "SendPayCheckToRegisterdUsers");

                                if (municipalityUpcbmModel.success && municipalityUpcbmModel.PayCounter > 0)
                                {
                                    Log.PcuLog(string.Format("step {1} time => {0}, start collectPayChecks municipality => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, municipality), "SendPayCheckToRegisterdUsers");

                                    currentPayMonth = DateExtension.maxDateToDate(municipalityUpcbmModel);
                                    collectPayChecks(payChecks, siteUsers, currentPayMonth, municipality, true);

                                    Log.PcuLog(string.Format("step {1} time => {0}, end collectPayChecks municipality => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, municipality), "SendPayCheckToRegisterdUsers");

                                    Log.PcuLog(new { _payChecks = payChecks.Count, _siteUsers = siteUsers.Count, _currentPayMonth = currentPayMonth, _municipality = municipality });
                                    siteUsers.ForEach(su => Log.PcuLog(new { id = su.ID, idNumber = su.idNumber, municipalityId = su.municipalityId, userId = su.userId }, "send by factory flag"));

                                    Log.PcuLog(string.Format("step {1} time => {0}, start sendUPCemail municipality => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, municipality), "SendPayCheckToRegisterdUsers");

                                    sendUPCemail(payChecks, siteUsers, currentPayMonth, true);

                                    Log.PcuLog(string.Format("step {1} time => {0}, end sendUPCemail municipality => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, municipality), "SendPayCheckToRegisterdUsers");
                                }
                            }
                        }
                        #endregion send by factory flag

                        #region send to registerd users
                        factorPCmunicipalities = setUpBll.GetMunicipalitiesForUPCBM();
                        Log.PcuLog(string.Join(",", factorPCmunicipalities), "MUNICIPALITIES=>> send to registerd users");
                        if (!this.isDebugeMode)
                        {
                            foreach (int municipality in factorPCmunicipalities)
                            {
                                payChecks = new List<UserImageModel>();
                                siteUsers = new List<PayCheckUsersByMailModel>();

                                Log.PcuLog(string.Format("step {1} time => {0}, start CheckRashutLastPayCheckMonth municipality => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, municipality), "SendPayCheckToRegisterdUsers");

                                municipalityUpcbmModel = FormsProvider.CheckRashutLastPayCheckMonth(municipality);
                                Log.PcuLog(municipalityUpcbmModel, "municipalityUpcbmModel");

                                Log.PcuLog(string.Format("step {1} time => {0}, end CheckRashutLastPayCheckMonth municipality => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, municipality), "SendPayCheckToRegisterdUsers");

                                if (municipalityUpcbmModel.success && municipalityUpcbmModel.PayCounter > 0)
                                {
                                    Log.PcuLog(string.Format("step {1} time => {0}, start collectPayChecks municipality => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, municipality), "SendPayCheckToRegisterdUsers");

                                    currentPayMonth = DateExtension.maxDateToDate(municipalityUpcbmModel);
                                    collectPayChecks(payChecks, siteUsers, currentPayMonth, municipality, false);

                                    Log.PcuLog(string.Format("step {1} time => {0}, end collectPayChecks municipality => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, municipality), "SendPayCheckToRegisterdUsers");

                                    Log.PcuLog(new { _payChecks = payChecks.Count, _siteUsers = siteUsers.Count, _currentPayMonth = currentPayMonth, _municipality = municipality });

                                    siteUsers.ForEach(su => Log.PcuLog(new { id = su.ID, idNumber = su.idNumber, municipalityId = su.municipalityId, userId = su.userId }, "send to registerd users"));

                                    Log.PcuLog(string.Format("step {1} time => {0}, start sendUPCemail municipality => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, municipality), "SendPayCheckToRegisterdUsers");

                                    sendUPCemail(payChecks, siteUsers, currentPayMonth, false);

                                    Log.PcuLog(string.Format("step {1} time => {0}, end sendUPCemail municipality => {2}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, municipality), "SendPayCheckToRegisterdUsers");
                                }
                            }
                        }
                        #endregion send to registerd users

                    }
                }
                finally
                {
                    Monitor.Exit(SynchronizeUserPayCheckByMail_Lock);
                }
                return true;
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return false;
            }
        }
        [HttpGet]
        [Route("CreateMobileImages")]
        public bool CreateMobileImages()
        {
            bool s = false;
            CreateMobileImage createImage = new CreateMobileImage();
            Log.ApplicationLog(HttpContext.Current.Request.Url, "CreateMobileImages");
            if (HttpContext.Current.Request.IsLocal)
            {

                if (!Monitor.TryEnter(CreateMobileImages_Lock, new TimeSpan(0)))
                {
                    throw new Exception("ServiceBusyException_Locked!");
                }
                try
                {
                    lock (CreateMobileImages_Lock)
                    {
                        ///set extended timeout
                        HttpContext.Current.Server.ScriptTimeout = 3600;

                        DateTime NOW = DateTime.Now;
                        int index = 0;
                        Log.ApplicationLog(string.Format("step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");

                        List<UserImageModel> payChecks = userbll.GetUserImagesForCreateMobileImages(300, "TLUSH");
                        payChecks.AddRange(userbll.GetUserImagesForCreateMobileImages(50, "T106"));

                        Log.ApplicationLog(string.Format("step {1} time => {0}, Images count => {2}, ids => {3}", DateTime.Now.Subtract(NOW).TotalSeconds, index++, payChecks.Count, string.Join(",", payChecks.Select(i => i.ID).ToList())), "CreateMobileImages");

                        foreach (UserImageModel image in payChecks)
                        {
                            Log.ApplicationLog(string.Format("step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");
                            AsyncFunctionCaller.RunAsync(HttpContext.Current, () =>
                            {
                                try
                                {
                                    userbll.CreateUserImage(createImage.CreateImage(image));
                                }
                                catch (Exception Ie)
                                {
                                    this.ServerError(Ie);
                                    userbll.RemoveImageById(image.ID);
                                }
                            });
                            if (index % 50 == 0)
                            {
                                Thread.Sleep(750);
                                Log.ApplicationLog(string.Format("Sleep 750, step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");
                            }
                        }

                        Log.ApplicationLog(string.Format("RunAsyncAll step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");
                        AsyncFunctionCaller.RunAsyncAll();
                        Log.ApplicationLog(string.Format("End RunAsyncAll step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");

                        Log.ApplicationLog(string.Format("EmptyDirectoryEraser step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");
                        createImage.EmptyDirectoryEraser();
                        Log.ApplicationLog(string.Format("End EmptyDirectoryEraser step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");

                        Log.ApplicationLog(string.Format("CleanRuningProcesses step {0} time => {1}", index++, DateTime.Now.Subtract(NOW).TotalSeconds), "CreateMobileImages");
                        createImage.CleanRuningProcesses();
                        Log.ApplicationLog(string.Format("End CleanRuningProcesses step {0} time => {1}", index++, DateTime.Now.Subtract(NOW).TotalSeconds), "CreateMobileImages");
                        createImage.CleanUploadsFolder();

                        Log.ApplicationLog(string.Format("last step {1} time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds, index++), "CreateMobileImages");
                        s = true;
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(CreateMobileImages_Lock);
                }
            }
            return s;
        }
        [HttpGet]
        [Route("CleanRuningProcesses")]
        public bool CleanRuningProcesses()
        {
            bool s = false;
            CreateMobileImage createImage = new CreateMobileImage();
            Log.ApplicationLog(HttpContext.Current.Request.Url, "CleanRuningProcesses");
            if (HttpContext.Current.Request.IsLocal)
            {

                if (!Monitor.TryEnter(CreateMobileImages_Lock, new TimeSpan(0)))
                {
                    throw new Exception("ServiceBusyException_Locked!");
                }
                try
                {
                    lock (CreateMobileImages_Lock)
                    {
                        DateTime NOW = DateTime.Now;
                        int index = 0;
                        Log.ApplicationLog(string.Format("step {0} time => {1}", index++, DateTime.Now.Subtract(NOW).TotalSeconds), "CleanRuningProcesses");
                        createImage.CleanRuningProcesses();
                        Log.ApplicationLog(string.Format("step {0} time => {1}", index++, DateTime.Now.Subtract(NOW).TotalSeconds), "CleanRuningProcesses");
                        createImage.CleanUploadsFolder();
                        Log.ApplicationLog(string.Format("step {0} time => {1}", index++, DateTime.Now.Subtract(NOW).TotalSeconds), "CleanRuningProcesses");

                        s = true;
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(CreateMobileImages_Lock);
                }
            }
            return s;
        }
        [HttpGet]
        [Route("UpdateMunicipalityStatus")]
        public bool UpdateMunicipalityStatus()
        {
            bool s = false;
            Log.ApplicationLog(HttpContext.Current.Request.Url, "UpdateMunicipalityStatus");
            if (HttpContext.Current.Request.IsLocal)
            {

                if (!Monitor.TryEnter(rashutNumberImport_Lock, new TimeSpan(0)))
                {
                    throw new Exception("rashutNumberImport_Lock!");
                }
                try
                {
                    lock (rashutNumberImport_Lock)
                    {
                        List<MunicipalitySchemaExtension> mun = FormsProvider.GetMunicipalitySchema();
                        s = setUpBll.UpdateMunicipalities(mun);
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(rashutNumberImport_Lock);
                }
            }
            Log.ApplicationLog("END UpdateMunicipalityStatus");
            return s;
        }
        [HttpGet]
        [Route("FireStoredProceduresForProcessesDinamicGroups")]
        public bool FireStoredProceduresForProcessesDinamicGroups()
        {
            bool ans = false;
            Log.ApplicationLog(HttpContext.Current.Request.Url, "FireStoredProceduresForProcessesDinamicGroups");
            if (HttpContext.Current.Request.IsLocal)
            {
                if (!Monitor.TryEnter(ProcessesStoredProcedures_Lock, new TimeSpan(0)))
                {
                    throw new Exception("ProcessesStoredProcedures_Lock!");
                }
                try
                {
                    lock (ProcessesStoredProcedures_Lock)
                    {
                        RegisterBll bll = new RegisterBll();
                        List<int> municipalities = bll.GetAllmunicipalities(true);
                        string res = SystemUsersProvider.fireDinamicGroupsProcedures(municipalities);
                        if (res != null)
                            ans = true;
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(ProcessesStoredProcedures_Lock);
                }
            }
            return ans;
        }
        [HttpGet]
        [Route("BirthdayGreetings/{day?}")]
        public bool BirthdayGreetings(string day = null)
        {
            bool ans = false;
            Log.ApplicationLog(HttpContext.Current.Request.Url, "BirthdayGreetings");

            if (HttpContext.Current.Request.IsLocal)
            {
                if (!Monitor.TryEnter(BirthdayGreetings_Lock, new TimeSpan(0)))
                {
                    throw new Exception("BirthdayGreetings_Lock!");
                }
                try
                {
                    lock (BirthdayGreetings_Lock)
                    {
                        FolderExtensions.CleanDirectory(Path.Combine(HttpContext.Current.Server.MapPath("~/Content"), "B_D"));

                        List<UserModel> users = string.IsNullOrWhiteSpace(day) ? this.registerBll.GetUsersBirthdays() : this.registerBll.GetUsersBirthdays(day.ToInt());

                        Log.ApplicationLog(new { count = users.Count, day = string.IsNullOrWhiteSpace(day) ? "NULL" : day, ids = string.Join(",", users.Select(u => u.ID).ToList()) }, "BirthdayGreetings");

                        CreateMobileImage sm = new CreateMobileImage();
                        users.ForEach(u =>
                            {
                                try
                                {
                                    if (!sm.SendBirthdayGreetingEmail(u))
                                        throw new Exception("SendBirthdayGreetingEmail_Exception");
                                }
                                catch (Exception Ie)
                                {
                                    this.ServerError(Ie);
                                    ans = false;
                                }
                            }
                        );

                        Thread.Sleep(1000);
                        FolderExtensions.CleanDirectory(Path.Combine(HttpContext.Current.Server.MapPath("~/Content"), "B_D"));

                        ans = true;
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(BirthdayGreetings_Lock);
                }
            }
            return ans;
        }
        [HttpGet]
        [Route("DistinctData")]
        public bool DistinctData()
        {
            Log.ApplicationLog(HttpContext.Current.Request.Url, "DistinctData");

            if (HttpContext.Current.Request.IsLocal)
            {
                if (!Monitor.TryEnter(InternalBulkRegister_Lock, new TimeSpan(0)))
                {
                    throw new Exception("DistinctData_Lock!");
                }
                try
                {
                    lock (InternalBulkRegister_Lock)
                    {
                        DistinctContactData();
                        return true;
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                    throw e;
                }
                finally
                {
                    Monitor.Exit(InternalBulkRegister_Lock);
                }
            }
            return false;
        }

        #region private
        private byte[] create101forms(int userId, int municipalityId, int formYear)
        {
            try
            {
                int i = 0;
                Log.PcuLog(string.Format("create101forms {0}", i++));
                Create101FormAction CreateForm = new Create101FormAction();
                TaxFromModel taxFromModel = CreateForm.GetBaseFormData(userId, municipalityId, formYear);
                Log.PcuLog(string.Format("create101forms {0}", i++));
                byte[] form101 = CreateForm.CreateForm101(taxFromModel);
                Log.PcuLog(string.Format("create101forms {0}", i++));

                return form101;
            }
            catch (Exception e)
            {
                this.ServerError(e);
                throw e;
            }
        }
        private bool mailSentAlready(List<int> sentMailProcesses, int tmProcessId)
        {
            if (sentMailProcesses.Count < 1)
            {
                return false;
            }
            foreach (int tmId in sentMailProcesses)
            {
                if (tmId == tmProcessId)
                {
                    return true;
                }
            }
            return false;
        }
        private void sendProcessCompletedEmails(ProcessModel processModel, UpdateProcessModel process)
        {
            string template = string.Empty;
            string ans = process.processStatus == 2 ? "בקשתך אושרה סופית!" : "הבקשה אושרה זמנית, והועברה להמשך טיפול הגורמים הרלונטיים";//2 completed in java

            MailDataModel data = new MailDataModel
            {
                userid = processModel.USERID,
                target_userid = processModel.TARGET_USERID,
                user_name = processModel.OVED_NESU_BAKASHA != null && processModel.OVED_NESU_BAKASHA > 0 ? processModel.OVED_NESU_BAKASHA_NAME : processModel.USERMODEL.firstName + " " + processModel.USERMODEL.lastName,
                target_user_name = processModel.OVED_NESU_BAKASHA != null && processModel.OVED_NESU_BAKASHA > 0 ? processModel.OVED_NESU_BAKASHA_NAME : processModel.TARGET_USER != null ? processModel.TARGET_USER.firstName + " " + processModel.TARGET_USER.lastName : getTargetUsername(processModel.TARGET_USERID),
                by_user_name = processModel.TARGET_USER != null ? processModel.TARGET_USER.firstName + " " + processModel.TARGET_USER.lastName : getTargetUsername(processModel.TARGET_USERID),
                tahalich_name = processModel.PROCESS_DESCRIPTION,
                from_date = processModel.FROM_DATE,
                to_date = processModel.TO_DATE,
                description = processModel.PROCESS_TYPE != 3 ? processModel.CONTENT1 : processModel.CONTENT4,//סיבת בקשת חופשה או תוכנית קורס
                answer = ans,
                reason = string.IsNullOrEmpty(processModel.COMMENTS) ? "ללא סיבה" : processModel.COMMENTS
            };

            Log.FormLog(data, "MailDataModel");
            RouteData routedata = HttpContext.Current.Request.RequestContext.RouteData;
            template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "ProcessAnswer", data);

            string theEmail = processModel.OVED_NESU_BAKASHA != null && processModel.OVED_NESU_BAKASHA > 0 ? getOvedNesuEmail(processModel.OVED_NESU_BAKASHA) : processModel.USERMODEL.email;

            // send the mail
            MailSender.Send(theEmail/*"itayi@ladpc.co.il"*/, template, false, "tikoved.co.il תשובה סופית לבקשה", null, "", "");
        }
        private void DistinctContactData()
        {
            try
            {
                DateTime NOW = DateTime.Now;
                Log.ApplicationLog(string.Format("start distinct user cells function ,time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds));
                setUpBll.DistinctUserCells(NOW);
                Log.ApplicationLog(string.Format("end distinct user cells function ,time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds));
                NOW = DateTime.Now;
                Log.ApplicationLog(string.Format("start distinct user emails function ,time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds));
                setUpBll.DistinctUserEmails(NOW);
                Log.ApplicationLog(string.Format("end distinct user emails function ,time => {0}", DateTime.Now.Subtract(NOW).TotalSeconds));
            }
            catch (Exception e)
            {
                this.ServerError(e);
                throw e;
            }
        }
        private void collectPayChecks(List<UserImageModel> payChecks, List<PayCheckUsersByMailModel> siteUsers, DateTime currentPayMonth, int municipality, bool factoryUsers)
        {
            List<PayCheckUsersByMailModel> lUsers = factoryUsers ? setUpBll.GetUsersByFactoryPC(currentPayMonth, municipality) : setUpBll.GetUsersToSendMail(currentPayMonth, municipality);
            Log.PcuLog(string.Format("PayCheckUsersByMailModel=>, count => {1},  \n users to send => {0}", string.Join(",", lUsers.Select(iu => iu.userId).ToList()), lUsers.Count));
            if (lUsers.Count == 0)
                return;
            siteUsers.AddRange(lUsers);

            AsyncFunctionCaller.RunAsync(HttpContext.Current, () =>
            {
                ///first step => get all the pay-checks into the db....
                foreach (PayCheckUsersByMailModel pcu in lUsers)
                {
                    try
                    {
                        payChecks.Add(FormsProvider.GetPaycheck(pcu.user.idNumber, pcu.user.calculatedIdNumber.ToInt(), municipality, currentPayMonth));
                    }
                    catch (Exception e)
                    {
                        this.ServerError(e);
                    }
                }
            }, true);

            AsyncFunctionCaller.RunAsyncAll();
        }
        private void preCollectPayChecks(DateTime currentPayMonth, int municipality)
        {
            Log.PcuLog(string.Format("Start preCollectPayChecks for municipality =>{2},  Date => {1}/{0}", currentPayMonth.Year, currentPayMonth.Month, municipality), "preCollectPayChecks function");

            List<USER> toCollect = setUpBll.GetAllMunicipalityUsers(currentPayMonth, municipality);
            var t = toCollect.Select(u => u.USERSTOFACTORIES.FirstOrDefault(f => f.MUNICIPALITYID == municipality)).ToList();
            Log.PcuLog(new { toCollect_counter = toCollect.Count }, "preCollectPayChecks counter");

            AsyncFunctionCaller.RunAsync(HttpContext.Current, () =>
            {
                foreach (USER usr in toCollect)
                {
                    try
                    {
                        FormsProvider.GetPaycheck(usr.IDNUMBER, municipality, currentPayMonth, usr.USERSTOFACTORIES.First(f => f.MUNICIPALITYID == municipality).FACTORYID.ToInt());
                    }
                    catch (Exception ex)
                    {
                        ex.LogError(new { IDNUMBER = usr.IDNUMBER, municipality = municipality, currentPayMonth = currentPayMonth, FACTORY = usr.USERSTOFACTORIES.FirstOrDefault(f => f.MUNICIPALITYID == municipality) });
                    }
                }

            }, true);
            //AsyncFunctionCaller.RunAsyncAll();
            Log.PcuLog(string.Format("End preCollectPayChecks for municipality =>{2},  Date => {1}/{0}", currentPayMonth.Year, currentPayMonth.Month, municipality), "preCollectPayChecks function");
        }
        private void sendUPCemail(List<UserImageModel> payChecks, List<PayCheckUsersByMailModel> siteUsers, DateTime currentPayMonth, bool factoryUsers)
        {
            CreateMobileImage createImage = new CreateMobileImage();
            payChecks.ForEach(p =>
            {
                try
                {
                    PayCheckUsersByMailModel pcu = siteUsers.First(u => u.userId == p.userId);
                    //if (currentPayMonth.Month == 12 || currentPayMonth.Month == 1)
                    //{
                    //    int Year = (currentPayMonth.Month == 1 ? currentPayMonth.Year : (currentPayMonth.Year + 1));
                    //    Log.PcuLog(new { userId = p.userId, municipalityId = pcu.municipalityId, Year = Year });

                    //    if (createImage.SendPayCheckEmail(p, pcu, create101forms(p.userId, pcu.municipalityId, Year), Year))
                    //    {
                    //        pcu.lastSendDate = new DateTime(currentPayMonth.Year, currentPayMonth.Month, 1);
                    //        pcu.lastSendMonth = currentPayMonth.Month;
                    //        pcu.lastSendYear = currentPayMonth.Year;
                    //        if (!this.isDebugeMode)
                    //        {
                    //            if (factoryUsers)
                    //                setUpBll.UpdateFactoryPCU(pcu);
                    //            else
                    //                setUpBll.UpdatePCU(pcu);
                    //        }
                    //    }
                    //}
                    //else 
                    if (createImage.SendPayCheckEmail(p, pcu))
                    {
                        pcu.lastSendDate = new DateTime(currentPayMonth.Year, currentPayMonth.Month, 1);
                        pcu.lastSendMonth = currentPayMonth.Month;
                        pcu.lastSendYear = currentPayMonth.Year;
                        if (!this.isDebugeMode)
                        {
                            if (factoryUsers)
                                setUpBll.UpdateFactoryPCU(pcu);
                            else
                                setUpBll.UpdatePCU(pcu);
                        }
                    }
                }
                catch (Exception e)
                {
                    this.ServerError(e);
                }
            });
        }
        private void registerByMunicipality(int municipalityId)
        {
            List<SystemUserModel> users = registerBll.GetBulkUsersToInternalRegister(municipalityId);
            if (users.Count <= 0)
            {
                Log.LogLoad(new { municipalityId = municipalityId, usersCount = users.Count }, "counter id 0");
                return;
            }

            Log.LogLoad(new { municipalityId = municipalityId, usersCount = users.Count }, "start register all users");
            List<WebSecurityModel> securityModels = new List<WebSecurityModel>();

            foreach (IEnumerable<SystemUserModel> sysusers in users.Chunk(150))
            {
                foreach (SystemUserModel su in sysusers)
                {
                    try
                    {
                        AsyncFunctionCaller.RunAsync(() =>
                        {
                            securityModels.Add(WebSecurity.InternalRegisterFromSystem(
                                new RegisterUserModel()
                                {
                                    idNumber = su.fullIdNumber,
                                    calculatedIdNumber = su.idNumber,
                                    cell = su.cellNumber,
                                    password = su.initialPassword,
                                    initialPassword = su.initialPassword,
                                    municipality = su.municipality,
                                    email = su.email
                                }
                            ));
                        }, true);
                    }
                    catch (Exception e)
                    {
                        this.ServerError(e);
                    }
                }
                AsyncFunctionCaller.RunAsyncAll();
                Log.LogLoad(new { municipalityId = municipalityId, sysUsersCount = sysusers.Count() }, "End register Chunk");
            }
            DistinctContactData();
            Log.LogLoad(new { municipalityId = municipalityId, success = securityModels.Where(s => s.success).Count(), error = securityModels.Where(s => !s.success).Count() }, "End register all users");
        }
        private void reArrangeProcessesData(List<PackedProcessModel> data, List<TahalichModel> tahalichim, List<StepModel> shlavim, List<ReshimatTiyugModel> reshimotTiyug, List<MismachBereshimaModel> mismachimBeReshima, List<MismachModel> mismachim, List<KvuzatMishtamshimModel> kvuzotMishtamshim, List<MishtamsheiKvuzaModel> mishtamsheiKvuza)
        {

            for (var i = 0; i < data.Count(); i++)
            {
                if (!isExist(tahalichim, data[i].process))
                {
                    tahalichim.Add(data[i].process);
                }
                if (!isExist(shlavim, data[i].step))
                {
                    shlavim.Add(data[i].step);
                }
                if (!isExist(kvuzotMishtamshim, data[i].kvuzatMishtamshim))
                {
                    kvuzotMishtamshim.Add(data[i].kvuzatMishtamshim);
                }
                if (!isExist(mishtamsheiKvuza, data[i].mishtamsheyKvuza))
                {
                    mishtamsheiKvuza.Add(data[i].mishtamsheyKvuza);
                }
                if (data[i].reshimatTiyug != null)
                    if (!isExist(reshimotTiyug, data[i].reshimatTiyug))
                    {
                        reshimotTiyug.Add(data[i].reshimatTiyug);
                    }
                if (data[i].mismachBeReshima != null)
                    if (!isExist(mismachimBeReshima, data[i].mismachBeReshima))
                    {
                        mismachimBeReshima.Add(data[i].mismachBeReshima);
                    }
                if (data[i].mismach != null)
                    if (!isExist(mismachim, data[i].mismach))
                    {
                        mismachim.Add(data[i].mismach);
                    }
            }
        }
        private bool isExist(List<TahalichModel> listTahalichim, TahalichModel tahalich)
        {
            for (var i = 0; i < listTahalichim.Count(); i++)
            {
                if (listTahalichim[i].recId == tahalich.recId)
                {
                    return true;
                }
            }
            return false;
        }
        private bool isExist(List<StepModel> listShlavim, StepModel shalav)
        {
            for (var i = 0; i < listShlavim.Count(); i++)
            {
                if (listShlavim[i].recId == shalav.recId)
                {
                    return true;
                }
            }
            return false;
        }
        private bool isExist(List<ReshimatTiyugModel> listReshimot, ReshimatTiyugModel reshima)
        {
            for (var i = 0; i < listReshimot.Count(); i++)
            {
                if (listReshimot[i].recId == reshima.recId)
                {
                    return true;
                }
            }
            return false;
        }
        private bool isExist(List<MismachBereshimaModel> listMismachimBereshima, MismachBereshimaModel mismachBereshima)
        {
            for (var i = 0; i < listMismachimBereshima.Count(); i++)
            {
                if (listMismachimBereshima[i].recId == mismachBereshima.recId)
                {
                    return true;
                }
            }
            return false;
        }
        private bool isExist(List<MismachModel> listMismachim, MismachModel mismach)
        {
            for (var i = 0; i < listMismachim.Count(); i++)
            {
                if (listMismachim[i].recId == mismach.recId)
                {
                    return true;
                }
            }
            return false;
        }
        private bool isExist(List<KvuzatMishtamshimModel> listKvuzatMishtamshim, KvuzatMishtamshimModel kvuzatMishtamshim)
        {
            for (var i = 0; i < listKvuzatMishtamshim.Count(); i++)
            {
                if (listKvuzatMishtamshim[i].recId == kvuzatMishtamshim.recId)
                {
                    return true;
                }
            }
            return false;
        }
        private bool isExist(List<MishtamsheiKvuzaModel> listMishtamsheyKvuza, MishtamsheiKvuzaModel mishtameshkvuza)
        {
            for (var i = 0; i < listMishtamsheyKvuza.Count(); i++)
            {
                if (listMishtamsheyKvuza[i].recId == mishtameshkvuza.recId)
                {
                    return true;
                }
            }
            return false;
        }
        private string getOvedNesuEmail(decimal? ovedId)
        {
            if (ovedId != null)
            {
                return processBll.getEmployeeEmail(ovedId);
            }
            return string.Empty;
        }
        private string getTargetUsername(decimal userId)
        {
            return processBll.getTargetUsername(userId);
        }
        #endregion
    }
}