﻿using CustomMembership;
using DataAccess.BLLs;
using DataAccess.Models;
using Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WorkerPortfolio.App_Start;

namespace WorkerPortfolio.Controllers
{
    [Authorize]
    [RoutePrefix("AdminStatistics")]
    [InitializeSimpleMembership]
    public class StatisticsController : ApiController
    {
        private StatisticsBll stBll;
        public StatisticsController()
        {
            this.stBll = new StatisticsBll();
        }

        [HttpPost]
        [Route("MunicipalityFormStatistics")]
        public IHttpActionResult GetMunicipalityFormStatistics(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");

                return Ok(this.stBll.GetFormStatisticsForMunicipality(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }

        #region private
        private bool validateUser(UserSearchModel model)
        {
            if (!WebSecurity.CheckAdminAuthorizetion(model))
                throw new AccessViolationException("AccessViolationException");

            model.municipalityId = WebSecurity.GetSetCurrentMunicipality();
            model.maxImmunity = WebSecurity.GetMaxImmunity(model.idNumber);
            return true;
        }
        #endregion
    }
}
