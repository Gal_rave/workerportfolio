﻿using System.Configuration;
using System.Web.Mvc;

namespace WorkerPortfolio.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.email = ConfigurationManager.AppSettings["smtpDefaultEmail"];
            return View();
        }
        public ActionResult post()
        {
            return View();
        }
        public ActionResult Error()
        {
            return View();
        }
    }
}
