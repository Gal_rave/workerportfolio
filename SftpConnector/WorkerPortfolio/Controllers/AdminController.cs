﻿using System;
using System.Web;
using System.Web.Http;
using System.Collections.Generic;

using DataAccess.Extensions;
using CustomMembership;
using DataAccess.BLLs;
using DataAccess.Models;
using Logger;
using WorkerPortfolio.App_Start;
using DataAccess.Enums;

namespace WorkerPortfolio.Controllers
{
    [Authorize]
    [RoutePrefix("api/admin")]
    [InitializeSimpleMembership]
    public class AdminController : ApiController
    {
        private AdminBll adminBll;
        private UserGroupsBll groupsBll;
        public AdminController()
        {
            this.adminBll = new AdminBll();
            this.groupsBll = new UserGroupsBll();
        }
        
        [HttpPost]
        [Route("ValidUser")]
        public IHttpActionResult ValidUser(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAdminAuthorizetion(model))
                    return Ok(false);
                return Ok(true);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("SearchUsers")]
        public IHttpActionResult SearchUsers(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");
                return Ok(adminBll.Search(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("GetUserById")]
        public IHttpActionResult GetUserById(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");
                return Ok(adminBll.GetUserById(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("ResetPassword")]
        public IHttpActionResult ResetPassword(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");
                return Ok(CustomMembershipProvider.ResetPassword(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("SendUserReset")]
        public IHttpActionResult SendUserReset(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");
                WebSecurityModel sm = WebSecurity.UserResetPassword(model.searchIdNumber, model.page == 1, model.page == 0);
                return Ok(sm.success);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("GetMunicipalities")]
        public IHttpActionResult GetMunicipalities(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");

                return Ok(adminBll.GetMunicipalities(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("GetGroups")]
        public IHttpActionResult GetGroups(WSPostModel model)
        {
            try
            {
                if (!WebSecurity.CheckAdminAuthorizetion(model.idNumber, model.confirmationToken))
                    throw new AccessViolationException("AccessViolationException");

                return Ok(groupsBll.GetAllGroups());
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPut]
        [Route("UpdateAuthorizations")]
        public IHttpActionResult UpdateAuthorizations(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");

                return Ok(adminBll.UpdateMunicipalityAuthorizations(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPut]
        [Route("UpdateMunicipalities")]
        public IHttpActionResult UpdateMunicipalities(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");

                return Ok(adminBll.UpdateMunicipalities(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("UpdateUserGroups")]
        public IHttpActionResult UpdateUserGroups(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");
                
                ///add groups
                model.searchMunicipalityName.ToIntList().ForEach(a=> groupsBll.AddUserToGroup(model.searchIdNumber,a));
                ///remove groups
                model.searchMunicipalitiesId.ToIntList().ForEach(r=> groupsBll.RemoveUserFromGroup(model.searchIdNumber, r));

                return Ok(true);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("UEPStatistics")]
        public IHttpActionResult GetUEP_Statistics(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");
                return Ok(adminBll.Get_UPCBM_Statistics(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("Form101Statistics")]
        public IHttpActionResult GetForm101_Statistics(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");

                return Ok(adminBll.Get_Form101_Statistics(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("LogInLogStatistics")]
        public IHttpActionResult GetLogInLog_Statistics(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");

                return Ok(adminBll.Parse_LogInLog_Statistics(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("DailyUsagetatistics")]
        public IHttpActionResult GetDailyUsageLogInLog_Statistics(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");

                return Ok(adminBll.Parse_DailyUsageLogIn_Statistics(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPatch]
        [Route("ResetUserPayrollDisplay")]
        public IHttpActionResult ResetUserPayrollDisplay(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");
                bool res = adminBll.DeleteUserImagesByType(model.userToken.ToInt(), (int)UserImageEnum.TLUSH);
                if(res)
                    res = adminBll.DeleteUserImagesByType(model.userToken.ToInt(), (int)UserImageEnum.T106);
                return Ok(res);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("GetSingleMunicipaly")]
        public IHttpActionResult GetSingleMunicipaly(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");

                return Ok(adminBll.GetMunicipality(model));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpDelete]
        [Route("DeleteHRContact/{idNumber}/{contactId}")]
        public bool DeleteHumanResourcesContact(string idNumber, int contactId)
        {
            try
            {
                if (!WebSecurity.IsCurrentUser(idNumber.DecrypString()))
                    throw new AccessViolationException("AccessViolationException");

                return adminBll.DeleteHRContactByID(contactId);
            }
            catch (Exception e)
            {
                this.ServerError(e);
                return false;
            }
        }
        [HttpPut]
        [Route("UpdateHRContact")]
        public IHttpActionResult UpdateHumanResourcesContact(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");
                
                return Ok(adminBll.UpdateHRContactByID(model.contact));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("UserMessages")]
        public IHttpActionResult UserSmsMessages(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");
                return Ok(new MailBll().GetUserSmsMessages(model.page, model.searchDate));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("UserMailMessages")]
        public IHttpActionResult UserMailMessages(UserSearchModel model)
        {
            try
            {
                if (!validateUser(model))
                    throw new AccessViolationException("AccessViolationException");
                return Ok(new MailBll().GetUserResetNotifications(model.searchIdNumber, MailTypeEnum.PasswordReset, model.searchDate));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }

        #region private
        private bool validateUser(UserSearchModel model)
        {
            if (!WebSecurity.CheckAdminAuthorizetion(model))
                throw new AccessViolationException("AccessViolationException");

            model.municipalityId = WebSecurity.GetSetCurrentMunicipality();
            model.maxImmunity = WebSecurity.GetMaxImmunity(model.idNumber);
            return true;
        }
        #endregion
    }
}