﻿using System;
using System.Collections.Generic;
using System.Web.Http;

using DataAccess.Models;
using DataAccess.BLLs;
using Logger;

namespace WorkerPortfolio.Controllers
{
    [RoutePrefix("api/CitySearch")]
    public class CitySearchController : ApiController
    {
        private CityBll cityBll;
        public CitySearchController()
        {
            cityBll = new CityBll();
        }
        
        [HttpPost]
        [Route("{query}/{results}")]
        public IHttpActionResult Post(string query, int results)
        {
            try
            {
                List<CityModel> cityList = new List<CityModel>();
                if (string.IsNullOrWhiteSpace(query))
                    throw new FormatException();
                cityList = cityBll.CitySearch(query, results);
                return Ok(cityList);
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
        [HttpPost]
        [Route("{query}")]
        public IHttpActionResult Post(string query)
        {
            try
            {
                List<CityModel> cityList = new List<CityModel>();
                if (string.IsNullOrWhiteSpace(query))
                    throw new FormatException();
                cityList = cityBll.CitySearch(query);
                return Ok(new { data = cityList });
            }
            catch(Exception e)
            {
                return this.ServerError(e);
            }            
        }
        [HttpPost]
        [Route("GetMunicipalities/{query}/{results}")]
        public IHttpActionResult GetMunicipalities(string query, int results)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(query))
                    throw new FormatException();

                return Ok(cityBll.GetMunicipalities(query, results));
            }
            catch (Exception e)
            {
                return this.ServerError(e);
            }
        }
    }
}