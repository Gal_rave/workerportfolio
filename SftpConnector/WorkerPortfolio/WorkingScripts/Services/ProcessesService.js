﻿(function () {

    var ProcessesFactory = angular.module('ProcessesService', []);

    ProcessesFactory.factory('Processes', ['$http', '$cookies', function ($http, $cookies) {

        var setAction = function (action, params, type) {
            var baseUrl = 'api/process';
            type = typeof type !== 'undefined' && type !== null ? type.toLowerCase() : 'get';
            if (!params instanceof Object) params = new Object();
            if (type === 'get') {
                if (typeof params !== 'undefined' && params !== null)
                    for (var i in params)
                        action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

            } else {
                if (typeof params !== 'undefined' && params !== null)
                    for (var i in params)
                        action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                action = action.replace(/\/\[([^)]+)\]/ig, '');
            }

            return baseUrl + (typeof action !== 'undefined' && action !== null && action.length > 0 ? '/' + action : '');
        },
         encryptInformation = function (text, actionType) {
             if (actionType !== false)
                 actionType = true;
             var keys = $cookies.get('.MVCSPECN' + _SolutionBuildVersionNumber).split(',');
             text = String(text);
             var key = CryptoJS.enc.Utf8.parse(keys[0]);
             var iv = CryptoJS.enc.Utf8.parse(keys[1]);
             var encrypted = actionType !== false ? CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), key,
                 {
                     keySize: 128 / 8,
                     iv: iv,
                     mode: CryptoJS.mode.CBC,
                     padding: CryptoJS.pad.Pkcs7
                 }) : null;

             var decrypted = actionType === false ? CryptoJS.AES.decrypt(text, key, {
                 keySize: 128 / 8,
                 iv: iv,
                 mode: CryptoJS.mode.CBC,
                 padding: CryptoJS.pad.Pkcs7
             }).toString(CryptoJS.enc.Utf8) : null;

             return actionType ? String(encrypted) : String(decrypted);
         },
          encryptIDnumber = function (data) {
              var _data = angular.copy(data);
              _data.idNumber = encryptInformation(_data.idNumber);
              _data.calculatedIdNumber = encryptInformation(_data.calculatedIdNumber);
              if (_data.requiredRole != undefined && _data.requiredRole != null) _data.requiredRole = encryptInformation(_data.requiredRole);
              _data.isEncrypted = true;
              return _data;
          }

        return {
            //new 11.06.2018:
            getRashutProcessesData: function (data) {
                return $http.post(setAction("GetAllProcessesData"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            //new 11.06.2018:
            getMishtamsheiKvuzaRegila: function (wsPostmodel, data) {
                return $http.post(setAction("GetMishtamsheiKvuza/[confirmationToken]/[idNumber]/[calculatedIdNumber]/[currentMunicipalityId]/[currentRashutId]", wsPostmodel, "rest"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            //new 11.06.2018:
            getMishtamsheiKvuzaDinamit: function (wsPostmodel, data) {
                return $http.post(setAction("GetMishtamsheiKvuzaDinamit/[confirmationToken]/[idNumber]/[calculatedIdNumber]/[currentMunicipalityId]/[currentRashutId]", wsPostmodel, "rest"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getRashutProcesses: function (data) {
                return $http.post(setAction("GetProcessesData"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getRashutSteps: function (data) {
                return $http.post(setAction("GetStepsData"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getUserProcesses: function (data) {
                return $http.post(setAction("GetMyProcessesData"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getUserRequestProcesses: function (data) {
                return $http.post(setAction("GetMyRequestProcesses"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getUsers: function (data) {
                return $http.post(setAction("GetUsers"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getUserOrganizationUnitsManagers: function (data) {
                return $http.post(setAction("getOrganizationUnitsManagers"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            postEmployeeVacationForm: function (vacationFormData) {
                return $http.post(setAction("PostVacationForm"), JSON.stringify(vacationFormData), "json", "application/json; charset=utf-8");
            },
            postEmployeeCourseForm: function (courseFormData) {
                return $http.post(setAction("PostCourseForm"), JSON.stringify(courseFormData), "json", "application/json; charset=utf-8");
            },
            saveProcess: function (process, wsPostmodel) {
                return $http.post(setAction("saveProcess/[confirmationToken]/[idNumber]/[calculatedIdNumber]", wsPostmodel, "rest"), JSON.stringify(process), "json", "application/json; charset=utf-8");
            },
            getAcademicInsts: function () {
                return $http.post(setAction("getAcademicInsts"), null, "json", "application/json; charset=utf-8");
            },
            getYehidaIrgunit: function (data) {
                return $http.post(setAction("getYehidaIrgunit"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getUserManagers: function (unitAndTree, user) {
                var data = {
                    model: user,
                    yehida: unitAndTree
                }
                return $http.post(setAction("getUserManagers"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getRepresentedWorkers: function (unitAndTree, user) {
                var data = {
                    model: user,
                    yehida: unitAndTree
                }
                return $http.post(setAction("getRepresentedWorkers"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getWorkerVacationData: function (wsModel, worker) {
                var data = {
                    model: wsModel,
                    worker: worker
                }
                return $http.post(setAction("getWorkerVacationData"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getWorkerManagers: function (wsModel, worker, wokerYahidaIrgunit) {
                var data = {
                    model: wsModel,
                    worker: worker,
                    units: wokerYahidaIrgunit
                }
                return $http.post(setAction("getWorkerManagers"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getWorkerYehidaIrgunit: function (wsModel, worker) {
                var data = {
                    model: wsModel,
                    worker: worker
                }
                return $http.post(setAction("getWorkerYehidaIrgunit"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            },
            getMyRecIdFromUsers: function (data) {
                return $http.post(setAction("getUserRecord"), JSON.stringify(data), "json", "application/json; charset=utf-8");
            }
        }

    }]);

})();