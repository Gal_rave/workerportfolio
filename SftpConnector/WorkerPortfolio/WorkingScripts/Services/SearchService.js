﻿(function () {

    var SearchService = angular.module('SearchService', []);

    SearchService.factory('Search', ['$http',
      function ($http) {
          var setAction = function (action, params, type) {
              type = typeof type !== 'undefined' ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  for (var i in params)
                      action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              } else {
                  for (var i in params)
                      action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return '/api' + (typeof action !== 'undefined' && action.length > 0 ? '/' + action : '');
          },
          setUrl = function (action, params) {
              if (!params instanceof Object) params = new Object();
              for (var i in params) {
                  var prm = typeof params[i] === 'object' ? params[i].title : params[i];
                  action += '&' + i + '=' + prm;
              }
              return action;
          }

          return {
              citySearch: function (data) {
                  return $http({
                      method: 'POST',
                      url: setAction('CitySearch/[query]/[results]', data, 'rest'),
                      data: JSON.stringify(data),
                      headers: {
                          'Accept': "application/json; charset=utf-8",
                          "X-Search": '1'
                      }
                  });
              },
              streetByCityID: function (data) {
                  return $http({
                      method: 'POST',
                      url: setAction('StreetSearch/ByID/[query]/[cityid]/[results]', data, 'rest'),
                      data: JSON.stringify(data),
                      headers: {
                          'Accept': "application/json; charset=utf-8",
                          "X-Search": '1'
                      }
                  });
              },
              streetSearch: function (data) {
                  return $http({
                      method: 'POST',
                      url: setAction('StreetSearch/ByName/[city]/[query]/[results]', data, 'rest'),
                      data: JSON.stringify(data),
                      headers: {
                          'Accept': "application/json; charset=utf-8",
                          "X-Search": '1'
                      }
                  });
              },
              MunicipalitiesSearch: function (data) {
                  return $http({
                      method: 'POST',
                      url: setAction('CitySearch/GetMunicipalities/[query]/[results]', data, 'rest'),
                      data: JSON.stringify(data),
                      headers: {
                          'Accept': "application/json; charset=utf-8",
                          "X-Search": '1'
                      }
                  });
              },
              ZipSearch: function (data) {
                  return $http({
                      method: 'GET',
                      url: setUrl('https://www.israelpost.co.il/zip_data.nsf/SearchZip?OpenAgent', data),
                      headers: {
                          'Accept': "application/json; charset=utf-8"
                      },
                      XSearch: true
                  });
              }
          }

      }]);

})();