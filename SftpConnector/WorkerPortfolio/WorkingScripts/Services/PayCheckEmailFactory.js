﻿(function () {

    var PayCheckEmailFactory = angular.module('PayCheckEmailFactory', []);

    PayCheckEmailFactory.factory('PayCheckEmail', ['$http', '$cookies',
      function ($http, $cookies) {
          var setAction = function (action, params, type) {
              var baseUrl = 'api/WebService';
              type = typeof type !== 'undefined' && type !== null ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              }
              if (type === 'rest') {
                  if (typeof params !== 'undefined' && params !== null)
                      for (var i in params)
                          action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return baseUrl + (typeof action !== 'undefined' && action !== null && action.length > 0 ? '/' + action : '');
          },
          encryptInformation = function (text, actionType) {
              if (actionType !== false)
                  actionType = true;
              var keys = $cookies.get('.MVCSPECN' + _SolutionBuildVersionNumber).split(',');
              text = String(text);
              var key = CryptoJS.enc.Utf8.parse(keys[0]);
              var iv = CryptoJS.enc.Utf8.parse(keys[1]);
              var encrypted = actionType !== false ? CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), key,
                  {
                      keySize: 128 / 8,
                      iv: iv,
                      mode: CryptoJS.mode.CBC,
                      padding: CryptoJS.pad.Pkcs7
                  }) : null;

              var decrypted = actionType === false ? CryptoJS.AES.decrypt(text, key, {
                  keySize: 128 / 8,
                  iv: iv,
                  mode: CryptoJS.mode.CBC,
                  padding: CryptoJS.pad.Pkcs7
              }).toString(CryptoJS.enc.Utf8) : null;

              return actionType ? String(encrypted) : String(decrypted);
          },
          encryptIDnumber = function (data) {
              var _data = angular.copy(data);
              _data.idNumber = encryptInformation(_data.idNumber);
              _data.calculatedIdNumber = encryptInformation(_data.calculatedIdNumber);
              if (_data.requiredRole != undefined && _data.requiredRole != null) _data.requiredRole = encryptInformation(_data.requiredRole);
              return _data;
          },
          encryptWSPostExtensionModel = function (data) {
              var _data = angular.copy(data);
              _data.idNumber = encryptInformation(_data.idNumber);
              _data.calculatedIdNumber = encryptInformation(_data.calculatedIdNumber);
              if (_data.Cell != undefined && _data.Cell != null)
                  _data.Cell = encryptInformation(_data.Cell);
              if (_data.Email != undefined && _data.Email != null)
                  _data.Email = encryptInformation(_data.Email);
              if (_data.Token != undefined && _data.Token != null)
                  _data.Token = encryptInformation(_data.Token);
              if (_data.Password != undefined && _data.Password != null)
                  _data.Password = encryptInformation(_data.Password);
              if (_data.requiredRole != undefined && _data.requiredRole != null)
                  _data.requiredRole = encryptInformation(_data.requiredRole);

              return _data;
          }

          return {
              UserSendPayCheckToEmail: function (data) {
                  data.isEncrypted = true;
                  return $http.put(setAction("USPCTE"), JSON.stringify(encryptIDnumber(data)), "json", "application/json; charset=utf-8");
              },
              ValidateUserAndSendToken: function (data) {
                  data.isEncrypted = true;
                  return $http.put(setAction("ValidateUserAndSendToken"), JSON.stringify(encryptWSPostExtensionModel(data)), "json", "application/json; charset=utf-8");
              },
              UpdateUserData: function (data) {
                  data.isEncrypted = true;
                  return $http.put(setAction("UpdateUserData"), JSON.stringify(encryptWSPostExtensionModel(data)), "json", "application/json; charset=utf-8");
              },
              ValidateTokenAndUpdate: function (data) {
                  data.isEncrypted = true;
                  return $http.put(setAction("UpdateUserData"), JSON.stringify(encryptWSPostExtensionModel(data)), "json", "application/json; charset=utf-8");
              }
          }

      }]);

})();