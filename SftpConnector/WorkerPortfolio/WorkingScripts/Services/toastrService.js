﻿(function () {

    angular.module('WorkerPortfolioApp').factory('toastrService', toastrService);

    toastrService.$inject = ['toastr', 'toastrConfig', '$timeout'];

    function toastrService(toastr, toastrConfig, $timeout) {

        var setOptions = function (opts) {
            if (typeof opts == 'undefined')
                opts = {};
            if (opts.clearThenShow) {
                toastr.clear();
            }
            
            toastrConfig.autoDismiss = typeof opts.autoDismiss !== 'undefined' && opts.autoDismiss !== true ? opts.autoDismiss : true;
            toastrConfig.allowHtml = opts.allowHtml ? opts.allowHtml : false;
            toastrConfig.extendedTimeOut = parseInt((opts.extendedTimeOut ? opts.extendedTimeOut : 10), 10);
            toastrConfig.positionClass = 'toast-' + (opts.positionX ? opts.positionX : 'top') + '-' + (opts.positionY ? opts.positionY : 'center'), /// X 'top, bottom', Y  'Right, Left, Full Width, Center';
            toastrConfig.timeOut = parseInt((opts.timeOut ? opts.timeOut : 3500), 10);
            toastrConfig.closeButton = opts.closeButton ? opts.closeButton : false;
            toastrConfig.tapToDismiss = typeof opts.tapToDismiss !== 'undefined' && opts.tapToDismiss !== true ? opts.tapToDismiss : true;
            toastrConfig.progressBar = typeof opts.progressBar !== 'undefined' && opts.progressBar !== true ? opts.progressBar : true;
            toastrConfig.closeHtml = opts.closeHtml ? opts.closeHtml : null;
            toastrConfig.newestOnTop = typeof opts.newestOnTop !== 'undefined' && opts.newestOnTop !== true ? opts.newestOnTop : true;
            toastrConfig.maxOpened = opts.maxOpened ? opts.maxOpened : 0;
            toastrConfig.preventDuplicates = opts.preventDuplicates ? opts.preventDuplicates : false;
            toastrConfig.preventOpenDuplicates = typeof opts.preventOpenDuplicates !== 'undefined' && opts.preventOpenDuplicates !== true ? opts.preventOpenDuplicates : true;

            toastrConfig.onHidden = typeof opts.onHidden === 'function' ? opts.onHidden : null;
            toastrConfig.onShown = typeof opts.onShown === 'function' ? opts.onShown : null;
            toastrConfig.onTap = typeof opts.onTap === 'function' ? opts.onTap : null;
        }

        return {
            clear: function () {
                toastr.clear();
            },
            remove: function () {
                toastr.remove();
            },
            error: function (message, title, optionsOverride) {
                setOptions(optionsOverride);
                toastr.error(message, title);
            },
            info: function (message, title, optionsOverride) {
                setOptions(optionsOverride);
                toastr.info(message, title);
            },
            success: function (message, title, optionsOverride) {
                setOptions(optionsOverride);
                toastr.success(message, title);
            },
            warning: function (message, title, optionsOverride) {
                setOptions(optionsOverride);
                toastr.warning(message, title);
            },
            custom: function (message, title, optionsOverride) {
                setOptions(optionsOverride);
                toastr.info(message, title, optionsOverride);
            }
        }
    }
})();
