﻿(function () {
    angular.module('WorkerPortfolioApp').factory('fileUploade', FileUploade);

    FileUploade.$inject = ['$http'];

    function FileUploade($http) {
        var setAction = function (action, params, type) {
            type = typeof type !== 'undefined' ? type.toLowerCase() : 'get';
            if (!params instanceof Object) params = new Object();
            if (type === 'get') {
                for (var i in params)
                    action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

            } else {
                for (var i in params)
                    action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                action = action.replace(/\/\[([^)]+)\]/ig, '');
            }

            return '/api/FilesUploades' + (typeof action !== 'undefined' && action.length > 0 ? '/' + action : '');
        }

        return {
            SaveSignature: function (_idNumber, _year, _base64) {
                var SignatureModel = { idNumber: _idNumber, year: parseInt(_year), base64: _base64 };
                return $http.post(setAction('PostB64Test'), JSON.stringify(SignatureModel), "json", "application/json; charset=utf-8");
            },
            _Get: function (pageId) {
                return $http.get(setAction('GetQuestions', { pageId: pageId }), "json", "application/json; charset=utf-8");
            }
        }
    }
})();