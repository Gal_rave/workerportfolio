﻿(function () {
    

    angular.module('WorkerPortfolioApp').factory('formDataService', formDataService);

    formDataService.$inject = ['$http'];

    function formDataService($http) {

            var EmployerModel = {
                ID: 0, name: null, address: null, phoneNumber: null, deductionFileID: null, email: null, displayEmail: false, emailEmployee: false, emailEmployer: false, salary: 0, taxDeduction: 0, incomeType: null
            }
            , CityModel = {
                ID: 0, Name: null, EnglishName: null, Streets: new Array()
            }
            , StreetModel = {
                ID: 0, Name: null, Cities: new Array()
            }
            , ChildModel = {
                possession: false, childBenefit: false, ID: 0, firstName: null, lastName: null, idNumber: null, birthDate: null, immigrationDate: null, phone: null, cell: null, email: null
            }
            , IncomeModel = {
                startDate: null, incomeType: null, incomeTaxCredits: null, trainingFund: false, workDisability: false
            }
            , SignatureModel = {
                idNumber: null, year: null, base64: null, fileName: null, saved: false
            }
            , taxCoordinationModel = {
                requestCoordination: false, requestReason: 0, employers: new Array()
            }
            , taxExemptionModel = {
                blindDisabled: false, isResident: false, isImmigrant: false, immigrationStatus: 0, immigrationStatusDate: null, immigrationNoIncomeDate: null, spouseNoIncome: false, separatedParent: false, childrenInCare: false, infantchildren: false, singleParent: false, exAlimony: false, partnersUnder18: false, exServiceman: false, serviceStartDate: null, serviceEndDate: null, graduation: false, alimonyParticipates: false, settlement: null, startSettlementDate: null
            }
            , IncomeTypeModel = {
                monthSalary: false, anotherSalary: false, partialSalary: false, daySalary: false, allowance: false, stipend: false, anotherSource: false, anotherSourceDetails: null
            },
            EmployeeModel = {
                employerID: 0,
                employer: EmployerModel,
                cityID: 0,
                city: CityModel,
                streetID: 0,
                street: StreetModel,
                houseNumber: 0,
                zip: null,
                gender: false,
                maritalStatus: 0,
                ilResident: false,
                kibbutzResident: false,
                hmoMember: false,
                hmoName: null,
                year: null,
                signatureDate: null,
                signaturePath: null,
                children: new Array(),
                thisIncome: IncomeModel,
                otherIncomes: IncomeModel,
                signature: SignatureModel,
                taxCoordination: taxCoordinationModel,
                taxExemption: taxExemptionModel,
                ID: 0,
                firstName: null,
                lastName: null,
                idNumber: null,
                birthDate: null,
                immigrationDate: null,
                phone: null,
                cell: null,
                email: null
            }

            function updateFull(data, model) { }
            function updateEmployer(data, model) { }
            function updateCity(data, model) { }
            function updateStreet(data, model) { }
            function updateChild(data, model) { }
            function updateIncomeModel(data, model) { }
            function updateSignature(data, model) { }
            function updatetaxCoordination(data, model) { }
            function updatetaxExemptionModel(data, model) { }
            function updateIncomeType(data, model) { }
        return {
            GetFullModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateFull(data, EmployeeModel);
                return EmployeeModel;
            },
            GetEmployerModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateEmployer(data, EmployerModel);
                return EmployerModel;
            },
            GetCityModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateCity(data, CityModel);
                return CityModel;
            },
            GetStreetModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateStreet(data, StreetModel);
                return StreetModel;
            },
            GetChildModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateChild(data, ChildModel);
                return ChildModel;
            },
            GetIncomeModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateIncomeModel(data, IncomeModel);
                return IncomeModel;
            },
            GetSignatureModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateSignature(data, SignatureModel);
                return SignatureModel;
            },
            GettaxCoordinationModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updatetaxCoordination(data, taxCoordinationModel);
                return taxCoordinationModel;
            },
            GettaxExemptionModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updatetaxExemptionModel(data, taxExemption);
                return taxExemptionModel;
            },
            GetIncomeTypeModel: function (data) {
                if (typeof data === 'object' && data !== null)
                    return updateIncomeType(data, IncomeTypeModel);
                return IncomeTypeModel;
            }
        }
    }
})();