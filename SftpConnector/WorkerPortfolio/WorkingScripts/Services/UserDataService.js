﻿(function () {
    
    
    var UserDataService = angular.module('UserDataService', []);

    UserDataService.factory('UserService', ['$http', '$q',
      function ($http, $q) {
          var setAction = function (action, params, type, baseUrl) {
              baseUrl = typeof baseUrl === 'string' ? baseUrl : '/api/ContactUs';
              type = typeof type !== 'undefined' && type !== null ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              switch (type) {
                  case 'get':
                      for (var i in params)
                          action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');
                      break;
                  case 'rest':
                      for (var i in params)
                          action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                      action = action.replace(/\/\[([^)]+)\]/ig, '');
                      break;
              }

              return baseUrl + (typeof action !== 'undefined' && action !== null && action.length > 0 ? '/' + action : '');
          }

          return {
              ResetPassword: function (data) {
                  return $http.patch(setAction(null, null, null, '/api/Account'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetToMailItems: function (data) {
                  return $http.post(setAction('ToMailItems'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SendNotificationMail: function (data) {
                  return $http.post(setAction('SendNotificationMail'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              DeleteFile: function (data) {
                  return $http.get(setAction('DeleteFile', data, 'get', 'api/FilesUploades'), JSON.stringify(data), "application/json; charset=utf-8");
              },
              SendContactUsMail: function (data) {
                  return $http.post(setAction('ContactUs'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              GetSiteMail: function () {
                  return $http.put(setAction(), "json", "application/json; charset=utf-8");
              },
          }

      }]);
})();