﻿(function () {
    var TofesService = angular.module('TofesService', []);

    TofesService.factory('Tofes', ['$http', '$q',
      function ($http, $q) {
          var setAction = function (action, params, type) {
              type = typeof type !== 'undefined' ? type.toLowerCase() : 'get';
              if (!params instanceof Object) params = new Object();
              if (type === 'get') {
                  for (var i in params)
                      action += (action.indexOf('?') < 0 ? '?' : '&') + i + '=' + (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : '');

              } else {
                  for (var i in params)
                      action = action.replace('[' + i + ']', (typeof params[i] !== 'undefined' && params[i] !== null ? params[i] : ''));
                  action = action.replace(/\/\[([^)]+)\]/ig, '');
              }

              return '/api/tofes101' + (typeof action !== 'undefined' && action.length > 0 ? '/' + action : '');
          }

          return {
              query: function () {
                  return $http.get(setAction('get'), "json", "application/json; charset=utf-8");
              },
              GetFormData: function (data) {
                  return $http.post(setAction('GetFormData'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveChild: function (data) {
                  return $http.post(setAction('Child'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveStreet: function (data) {
                  return $http.post(setAction('Street'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveCity: function (data) {
                  return $http.post(setAction('City'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveIncome: function (data) {
                  return $http.post(setAction('Income'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveSignature: function (data) {
                  return $http.post(setAction('Signature'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveTaxCoordination: function (data) {
                  return $http.post(setAction('taxCoordination'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveTaxExemption: function (data) {
                  return $http.post(setAction('taxExemption'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveIncomeType: function (data) {
                  return $http.post(setAction('IncomeType'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveEmployee: function (data) {
                  return $http.post(setAction('Employee'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              },
              SaveEmployer: function (data) {
                  return $http.post(setAction('Employer'), JSON.stringify(data), "json", "application/json; charset=utf-8");
              }
          }
      }]);
})();