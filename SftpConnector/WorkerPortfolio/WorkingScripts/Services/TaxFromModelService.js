﻿(function () {
    angular.module('WorkerPortfolioApp').factory('TaxFromModelService', TaxFromModelService);

    TaxFromModelService.$inject = ['$window'];

    function TaxFromModelService($window) {
        var parseDate = function (val) {
            if (!checkIfHaveValue(val))
                return null;
            val = val.split('/');
            return new Date([val[1], val[0], val[2]].join('/'));
        },
        parsePostDate = function (val) {
            if (!checkIfHaveValue(val))
                return '';
            val = val.split('/');
            return new Date([val[1], val[0], val[2]].join('/') + ' 12:12');
        },
        checkIfHaveValue = function (value) {
            return typeof value != 'undefined' && value != null && value != '';
        }

        function copyGeneralData(model, modelData, currentMunicipalityId) {
            model.year = modelData.year;
            model.FormType = modelData.FormType;
            model.date = new Date();
            model.sign = modelData.sign;
            model.version = modelData.version;

            model.signatureDate = parsePostDate(modelData.signatureDate);
            model.signatureImageID = modelData.signatureImageID;
            model.emailEmployee = modelData.emailEmployee;
            model.municipalityId = currentMunicipalityId;

            model.FilesList = modelData.TofesControllerFilesList.concat(modelData.TaxExemptionControllerFilesList);
            model.FilesList = model.FilesList.concat(modelData.TaxAdjustmentControllerFilesList);

            model.pdf = modelData.formPdf;
            model.connectedImages = [];
            model.FilesList.filter(function (file) {
                model.connectedImages.push(
                    {
                        ID: file.imageId,
                        fileBlob: null,
                        fileDate: new Date(file.year, 0, 1),
                        fileName: file.filename,
                        fileString: null,
                        municipalityId: null,
                        type: null,
                        userId: null
                    }
                );
            });

            return model;
        }
        function copyChildren(childrenList) {
            var children = new Array();
            if (!checkIfHaveValue(childrenList) || childrenList.length <= 0)
                return children;
            for (var i in childrenList) {
                var child = angular.copy(childModel);
                child.ID = childrenList[i].ID;
                child.birthDate = parsePostDate(childrenList[i].birthDate);
                child.childBenefit = childrenList[i].childBenefit;
                child.firstName = childrenList[i].firstName;
                child.gender = checkIfHaveValue(childrenList[i].gender) ? childrenList[i].gender : null;
                child.homeLiving = childrenList[i].homeLiving;
                child.idNumber = childrenList[i].idNumber;
                child.possession = checkIfHaveValue(childrenList[i].possession) ? childrenList[i].possession : childrenList[i].homeLiving;
                child.PrevIdNumber = childrenList[i].PrevIdNumber;

                ///data check status
                newItem = parseInt(childrenList[i].newItem);
                child.CheckbirthDate = newItem === 1 ? 1 : childrenList[i].CheckbirthDate;
                child.CheckchildBenefit = newItem === 1 ? 1 : childrenList[i].CheckchildBenefit;
                child.CheckfirstName = newItem === 1 ? 1 : childrenList[i].CheckfirstName;
                child.Checkgender = newItem === 1 ? 1 : childrenList[i].Checkgender;
                child.CheckhomeLiving = newItem === 1 ? 1 : childrenList[i].CheckhomeLiving;
                child.CheckidNumber = newItem === 1 ? 1 : childrenList[i].CheckidNumber;
                child.Checkpossession = newItem === 1 ? 1 : childrenList[i].Checkpossession;

                children.push(child);
            }
            return children;
        }
        function copyEmployer(employer) {
            return {
                ID: 0,
                name: employer.name,
                address: employer.address,
                phoneNumber: employer.phoneNumber,
                deductionFileID: employer.deductionFileID,
                email: employer.email,
                displayEmail: employer.displayEmail,
                emailEmployee: true,
                emailEmployer: employer.displayEmail,
                salary: null,
                taxDeduction: null,
                incomeType: null,
                mifal: employer.mifal,
                rashut: employer.rashut,
            }
        }
        function copyThisIncome(thisIncome) {
            return {
                startDate: parsePostDate(thisIncome.startDate),
                CheckstartDate: thisIncome.CheckstartDate,
                incomeType: {
                    monthSalary: checkIfHaveValue(thisIncome.monthSalary),
                    anotherSalary: checkIfHaveValue(thisIncome.anotherSalary),
                    partialSalary: checkIfHaveValue(thisIncome.partialSalary),
                    daySalary: checkIfHaveValue(thisIncome.daySalary),
                    allowance: checkIfHaveValue(thisIncome.allowance),
                    stipend: checkIfHaveValue(thisIncome.stipend),
                    anotherSource: false,
                    anotherSourceDetails: null,

                    ///data check status
                    Checkstipend: thisIncome.Checkstipend,
                    CheckpartialSalary: thisIncome.CheckpartialSalary,
                    CheckmonthSalary: thisIncome.CheckmonthSalary,
                    CheckdaySalary: thisIncome.CheckdaySalary,
                    CheckanotherSalary: thisIncome.CheckanotherSalary,
                    Checkallowance: thisIncome.Checkallowance,
                },
                incomeTaxCredits: null,
                trainingFund: null,
                workDisability: null
            }
        }
        function copyTaxExemption(taxExemption) {
            return {
                blindDisabled: taxExemption.blindDisabled,
                isResident: taxExemption.isResident,
                isImmigrant: taxExemption.isImmigrant,
                immigrationStatus: taxExemption.immigration.immigrantStatus,
                immigrationStatusDate: parsePostDate(taxExemption.immigration.immigrantStatusDate),
                immigrationNoIncomeDate: parsePostDate(taxExemption.immigration.noIncomeDate),
                spouseNoIncome: taxExemption.spouseNoIncome,
                separatedParent: taxExemption.separatedParent,
                childrenInCare: taxExemption.childrenInCare,
                infantchildren: taxExemption.infantchildren,
                singleParent: taxExemption.singleParent,
                exAlimony: taxExemption.exAlimony,
                partnersUnder18: taxExemption.partnersUnder18,
                exServiceman: taxExemption.exServiceman,
                serviceStartDate: parsePostDate(taxExemption.servicemanStartDate),
                serviceEndDate: parsePostDate(taxExemption.servicemanEndDate),
                graduation: taxExemption.graduation,
                alimonyParticipates: taxExemption.alimonyParticipates,
                incompetentChild: taxExemption.incompetentChild,
                settlement: taxExemption.settlementCredits.settlement,
                startSettlementDate: parsePostDate(taxExemption.settlementCredits.startResidentDate),

                ///data check status
                CheckalimonyParticipates: taxExemption.CheckalimonyParticipates,
                CheckincompetentChild: taxExemption.CheckincompetentChild,
                CheckblindDisabled: taxExemption.CheckblindDisabled,
                CheckchildrenInCare: taxExemption.CheckchildrenInCare,
                CheckexAlimony: taxExemption.CheckexAlimony,
                CheckexServiceman: taxExemption.CheckexServiceman,
                Checkgraduation: taxExemption.Checkgraduation,
                Checkinfantchildren: taxExemption.Checkinfantchildren,
                CheckisImmigrant: taxExemption.CheckisImmigrant,
                CheckisResident: taxExemption.CheckisResident,
                CheckpartnersUnder18: taxExemption.CheckpartnersUnder18,
                CheckseparatedParent: taxExemption.CheckseparatedParent,
                CheckservicemanEndDate: taxExemption.CheckservicemanEndDate,
                CheckservicemanStartDate: taxExemption.CheckservicemanStartDate,
                ChecksingleParent: taxExemption.ChecksingleParent,
                CheckspouseNoIncome: taxExemption.CheckspouseNoIncome,
                ///immigration data check status
                CheckimmigrantStatus: taxExemption.immigration.CheckimmigrantStatus,
                CheckimmigrantStatusDate: taxExemption.immigration.CheckimmigrantStatusDate,
                ChecknoIncomeDate: taxExemption.immigration.ChecknoIncomeDate,
                ///settlementCredits data check status
                Checksettlement: taxExemption.settlementCredits.Checksettlement,
                CheckstartResidentDate: taxExemption.settlementCredits.CheckstartResidentDate
            }//, 
        }
        function copyEmployers(employers) {
            var employersList = new Array();
            if (!checkIfHaveValue(employers) || employers.length <= 0)
                return employersList;
            for (var i in employers) {
                var newItem = parseInt(employers[i].newItem);
                employersList.push({
                    ID: employers[i].$id,
                    name: employers[i].name,
                    address: employers[i].address,
                    phoneNumber: null,
                    deductionFileID: employers[i].deductionFileID,
                    email: null,
                    displayEmail: false,
                    emailEmployee: false,
                    emailEmployer: false,
                    salary: employers[i].salary,
                    taxDeduction: employers[i].taxDeduction,
                    incomeType: {
                        monthSalary: employers[i].incomeType == 1,
                        anotherSalary: false,
                        partialSalary: false,
                        daySalary: false,
                        allowance: employers[i].incomeType == 2,
                        stipend: employers[i].incomeType == 3,
                        anotherSource: employers[i].incomeType == 4,
                        anotherSourceDetails: '',

                        ///PrevIncomeType
                        Checkallowance: (newItem === 1 ? 1 : (employers[i].PrevIncomeType == 2 && employers[i].incomeType != 2 ? 2 : 0)), //2
                        CheckanotherSalary: 0,
                        CheckanotherSource: (newItem === 1 ? 1 : (employers[i].PrevIncomeType == 4 && employers[i].incomeType != 4 ? 2 : 0)), //4
                        CheckanotherSourceDetails: 0,
                        CheckdaySalary: 0,
                        CheckmonthSalary: (newItem === 1 ? 1 : (employers[i].PrevIncomeType == 1 && employers[i].incomeType != 1 ? 2 : 0)), //1
                        CheckpartialSalary: 0,
                        Checkstipend: (newItem === 1 ? 1 : (employers[i].PrevIncomeType == 3 && employers[i].incomeType != 3 ? 2 : 0)) //3
                    },

                    ///data check status -> TODO add existing employer and check
                    Checkaddress: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].Checkaddress),
                    CheckdeductionFileID: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].CheckdeductionFileID),
                    CheckincomeType: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].CheckincomeType),
                    Checkmifal: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].Checkmifal),
                    Checkname: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].Checkname),
                    Checkrashut: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].Checkrashut),
                    Checksalary: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].Checksalary),
                    ChecktaxDeduction: (parseInt(employers[i].newItem) == 1 ? 1 : employers[i].ChecktaxDeduction)
                });
            }
            return employersList;
        }
        function copyTaxCoordination(taxCoordination) {
            return {
                requestCoordination: taxCoordination.request,
                requestReason: taxCoordination.reason,
                employers: copyEmployers(taxCoordination.employers),

                ///data check status
                Checkreason: taxCoordination.Checkreason,
                Checkrequest: taxCoordination.Checkrequest,
            }
        }
        function copySpouse(spouse) {
            if (!checkIfHaveValue(spouse))
                return null;
            
            return {
                hasIncome: spouse.hasIncome,
                incomeType: spouse.incomeType,
                ID: null,
                firstName: spouse.firstName,
                lastName: spouse.lastName,
                idNumber: spouse.idNumber,
                birthDate: parsePostDate(spouse.birthDate),
                immigrationDate: parsePostDate(spouse.immigrationDate),
                phone: null,
                cell: null,
                email: null,
                createdDate: null,
                isDeleted: false,
                isConfirmed: false,
                password: null,
                calculatedIdNumber: null,
                fathersName: null,
                previousLastName: null,
                previousFirstName: null,
                city: null,
                street: null,
                houseNumber: null,
                entranceNumber: null,
                apartmentNumber: null,
                poBox: null,
                countryOfBirth: null,
                gender: false,
                maritalStatus: null,
                maritalStatusDate: null,
                isSpouse: true,
                spouseId: null,
                jobTitle: null,
                spouse: null,
                membership: null,
                groups: null,
                municipalities: null,
                userImages: null,
                PrevIdNumber: spouse.PrevIdNumber,

                ///data check status
                CheckbirthDate: spouse.CheckbirthDate,
                CheckfirstName: spouse.CheckfirstName,
                CheckidNumber: spouse.CheckidNumber,
                CheckimmigrationDate: spouse.CheckimmigrationDate,
                CheckincomeType: spouse.CheckincomeType,
                ChecklastName: spouse.ChecklastName,
                CheckhasIncome: spouse.CheckhasIncome
            };
        }
        function copyEmployee(employee) {
            return {//employee
                employerID: null,
                zip: employee.zip !== null? employee.zip : '',
                ilResident: employee.ilResident,
                kibbutzResident: employee.kibbutzResident,
                hmoMember: employee.hmoMember,
                hmoName: employee.hmoName,
                taxCoordination: null,
                taxExemption: null,
                ID: null,
                firstName: employee.firstName,
                lastName: employee.lastName,
                idNumber: employee.idNumber,
                birthDate: parsePostDate(employee.birthDate),
                immigrationDate: parsePostDate(employee.immigrationDate),
                phone: employee.phone,
                cell: employee.cell,
                email: employee.email,
                createdDate: null,
                isDeleted: false,
                isConfirmed: false,
                password: null,
                calculatedIdNumber: null,
                fathersName: null,
                previousLastName: null,
                previousFirstName: null,
                city: employee.city,
                street: employee.street,
                houseNumber: employee.houseNumber,
                entranceNumber: null,
                apartmentNumber: null,
                poBox: null,
                countryOfBirth: null,
                gender: employee.gender,
                maritalStatus: employee.maritalStatus,
                maritalStatusDate: null,
                isSpouse: false,
                spouseId: null,
                jobTitle: null,
                spouse: null,
                membership: null,
                groups: null,
                municipalities: null,
                userImages: null,

                ///data check status
                CheckbirthDate: employee.CheckbirthDate,
                Checkcell: employee.Checkcell,
                Checkcity: employee.Checkcity,
                Checkemail: employee.Checkemail,
                CheckfirstName: employee.CheckfirstName,
                Checkgender: employee.Checkgender,
                CheckhmoMember: employee.CheckhmoMember,
                CheckhmoName: employee.CheckhmoName,
                CheckhouseNumber: employee.CheckhouseNumber,
                CheckidNumber: employee.CheckidNumber,
                CheckilResident: employee.CheckilResident,
                CheckimmigrationDate: employee.CheckimmigrationDate,
                CheckkibbutzResident: employee.CheckkibbutzResident,
                ChecklastName: employee.ChecklastName,
                CheckmaritalStatus: employee.CheckmaritalStatus,
                Checkphone: employee.Checkphone,
                Checkstreet: employee.Checkstreet,
                Checkzip: employee.Checkzip
            };
        }
        function copyOtherIncomes(otherIncomes) {
            return {
                startDate: parsePostDate(otherIncomes.startDate),
                incomeType: {
                    monthSalary: checkIfHaveValue(otherIncomes.monthSalary),
                    anotherSalary: checkIfHaveValue(otherIncomes.anotherSalary),
                    partialSalary: checkIfHaveValue(otherIncomes.partialSalary),
                    daySalary: checkIfHaveValue(otherIncomes.daySalary),
                    allowance: checkIfHaveValue(otherIncomes.allowance),
                    stipend: checkIfHaveValue(otherIncomes.stipend),
                    anotherSource: checkIfHaveValue(otherIncomes.anotherSource),
                    anotherSourceDetails: otherIncomes.anotherSourceDetails,

                    ///data check status
                    Checkallowance: otherIncomes.Checkallowance,
                    CheckanotherSalary: otherIncomes.CheckanotherSalary,
                    CheckanotherSource: otherIncomes.CheckanotherSource,
                    CheckanotherSourceDetails: otherIncomes.CheckanotherSourceDetails,
                    CheckdaySalary: otherIncomes.CheckdaySalary,
                    CheckmonthSalary: otherIncomes.CheckmonthSalary,
                    CheckpartialSalary: otherIncomes.CheckpartialSalary,
                    Checkstipend: otherIncomes.Checkstipend
                },
                incomeTaxCredits: otherIncomes.incomeTaxCredits,
                anotherSourceDetails: otherIncomes.anotherSourceDetails,
                trainingFund: checkIfHaveValue(otherIncomes.Provisions.trainingFund),
                workDisability: checkIfHaveValue(otherIncomes.Provisions.workDisability),
                hasIncomes: checkIfHaveValue(otherIncomes.hasIncomes),

                ///data check status
                CheckhasIncomes: otherIncomes.CheckhasIncomes,
                CheckincomeTaxCredits: otherIncomes.CheckincomeTaxCredits,
                ChecktrainingFund: otherIncomes.Provisions.ChecktrainingFund,
                CheckworkDisability: otherIncomes.Provisions.CheckworkDisability,
                CheckanotherSourceDetails: otherIncomes.CheckanotherSourceDetails,
            }
        }

        function setSendModel(modelData, currentMunicipalityId) {
            var model = copyGeneralData(angular.copy(taxfrommodel), modelData, currentMunicipalityId);
            model.children = copyChildren(modelData.children);
            model.employer = copyEmployer(modelData.employer);
            model.income = copyThisIncome(modelData.thisIncome);
            model.taxExemption = copyTaxExemption(modelData.taxExemption);
            model.taxCoordination = copyTaxCoordination(modelData.taxCoordination);
            model.spouse = copySpouse(modelData.spouse);
            model.employee = copyEmployee(modelData.employee);
            model.otherIncomes = copyOtherIncomes(modelData.otherIncomes);
            ///for pdf mobile display
            model.showMobile = $window.innerWidth < 801 ? true : null;

            return model;
        }

        var childModel = {
            ID: null,
            possession: false,
            childBenefit: false,
            homeLiving: false,
            firstName: null,
            idNumber: null,
            oldIdNumber: null,
            gender: true,
            birthDate: null,
            userID: 0
        },

        incomeTypeModel = {
            monthSalary: false,
            anotherSalary: false,
            partialSalary: false,
            daySalary: false,
            allowance: false,
            stipend: false,
            anotherSource: false,
            anotherSourceDetails: null
        },

        taxfrommodel = {
            municipalityId: null,
            year: null,
            date: null,
            sign: false,
            signatureDate: null,
            signatureImageID: null,
            emailEmployee: false,
            employee: {},
            employer: {},
            children: new Array(),
            income: {},
            otherIncomes: {},
            signature: {},
            taxCoordination: {},
            taxExemption: {},
            incomeType: {
                monthSalary: false,
                anotherSalary: false,
                partialSalary: false,
                daySalary: false,
                allowance: false,
                stipend: false,
                anotherSource: false,
                anotherSourceDetails: null
            },
            spouse: {},
            FilesList: [],
            connectedImages: [],
            formPdfImageID: null,
            formPdf: null,
        }

        , getModel = function () {
            return taxfrommodel;
        }

        , setMOdel = function (modelData, currentMunicipalityId) {
            return setSendModel(modelData, currentMunicipalityId);
        }

        return {
            GetModel: getModel,
            SetModel: setMOdel
        }
    }
})();