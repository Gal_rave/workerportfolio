﻿(function () {

    angular.module('WorkerPortfolioApp', [
        // Angular modules 
        'ngRoute', 'ngSanitize', 'ngAnimate', 'ui.router', 'LocalStorageModule', 'ngDialog', 'ngResource', 'ui.select', 'ngCookies'

        // Custom modules 
        , 'TofesService', 'SearchService', 'LoginService', 'UserDataService', 'PayrollDataServise', 'AdminFactory', 'ProcessesService', 'PayCheckEmailFactory', 'FileManagerFactory', 'StatisticsFactory'

        // 3rd Party Modules
        , 'angularFileUpload', 'toastr', 'blockUI', 'wt.responsive', 'ngIdle', 'ngIdle.title', 'angucomplete-alt', 'ngFileSaver', 'ui.tinymce', 'adaptv.adaptStrap'

    ]).run(['$rootScope', '$templateCache', '$timeout', '$state', function ($rootScope, $templateCache, $timeout, $state) {
        ///bind anderscore
        $rootScope._ = _;
        ///add custom block ui template
        $templateCache.put('angular-block-ui/angular-block-ui.ng.ie.html', '<div class=\"block-ui-overlay\"></div><iframe class="block-ui-overlay-iframe" frameborder="0" tabindex="-1" src="" /><div class=\"block-ui-message-container\" aria-live=\"assertive\" aria-atomic=\"true\"><div class=\"block-ui-message\" ng-class=\"$_blockUiMessageClass\">{{ state.message }}</div></div>');
        ///return date in `HE` formt
        $rootScope.LocalDateString = function (date) {
            var pad = function (val) { return val < 10 ? ('0' + String(val)) : val; }
            return [pad(date.getMonth() + 1), pad(date.getDate()), date.getFullYear()].join('/');
        }
        ///get the prev month
        $rootScope.getMonthDate = function (lastYear) {
            var date = new Date();
            date.setMonth(date.getMonth() - 1);
            if (date.getDate() >= 10) {
                date.setDate(10);
            } else {
                date.setMonth(date.getMonth() - 1);
            }
            if (lastYear) {
                date = new Date();
                date.setFullYear(date.getFullYear() - 1);
                date.setMonth(11);
                date.setDate(22);
            }
            return date;
        }
        ///parse date into `mm/yyyy` string
        $rootScope.parseMonth = function (date) {
            function pad(v) { return v < 10 ? '0' + v : v }
            if (!(date instanceof Date) || date === 'Invalid Date')
                date = new Date();
            return [pad(date.getMonth() + 1), date.getFullYear()].join('/');
        }
        ///set post model data
        $rootScope.getWSPostModel = function (isMonthDate, islastYear, formatDate, isFromRouting, role) {
            var credentials = typeof $rootScope.$$credentials !== 'undefined' ? $rootScope.$$credentials : $rootScope.lsGet('$$credentials');
            var _selectedDate = isMonthDate === true ? $rootScope.getMonthDate(islastYear) : new Date();

            if (credentials.currentTerminationDate !== null)
                _selectedDate = new Date(credentials.currentTerminationDate);

            WSPostModel = {
                idNumber: credentials.idNumber,
                confirmationToken: credentials.confirmationToken,
                calculatedIdNumber: credentials.calculatedIdNumber,
                currentMunicipalityId: credentials.currentMunicipalityId,
                currentRashutId: credentials.currentRashutId,
                selectedDate: formatDate === true ? $rootScope.parseMonth(_selectedDate) : _selectedDate,
                municipalities: credentials.municipalities,
                isAdmin: credentials.isAdmin,
                isMobileRequest: (isFromRouting === true ? window.innerWidth < 801 : null),
                isEncrypted: false,
                requiredRole: role
            };

            return WSPostModel;
        }
        ///get current form 101 year
        $rootScope.getForm101Year = function () {
            var credentials = typeof $rootScope.$$credentials !== 'undefined' ? $rootScope.$$credentials : $rootScope.lsGet('$$credentials'),
                testDate = new Date(new Date().getFullYear(), 11, 31), currentDate = new Date();

            if (testDate < currentDate)
                currentDate.setFullYear((testDate.getFullYear() + 1));

            WSPostModel = {
                idNumber: credentials.idNumber,
                confirmationToken: credentials.confirmationToken,
                calculatedIdNumber: credentials.calculatedIdNumber,
                currentMunicipalityId: credentials.currentMunicipalityId,
                selectedDate: currentDate,
                updateUserData: false,
                currentRashutId: credentials.currentRashutId
            };

            return WSPostModel;
        };
        ///file uploader filter for 101 form
        $rootScope.FilterFiles = function (modelFiles, $$scopeFiles, origin) {
            $.map($$scopeFiles, function (file) {
                if (!file.isError && file.isSuccess && file.isUploaded) {
                    var obj = {};
                    $.map(file.formData, function (item) {
                        $.map(item, function (v, i) {
                            obj[i] = v;
                        });
                    });
                    obj.imageId = file.imageId;
                    obj.origin = origin;

                    var O_uniq = _.groupBy(modelFiles, 'imageId');
                    if (typeof O_uniq[obj.imageId] === 'undefined') {
                        modelFiles.push(obj);
                    }
                }
            });
        };
        ///$stateChangeError occurs when user changes current municipality, this is en error in the resolve
        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
            error.data = {};
            if(toState.controller ===  fromState.controller)
                $state.go('Home');
        });
    }]);

})();