﻿(function () {
    angular.module('WorkerPortfolioApp').filter('OrderMuneFilter', OrderMuneFilter);

    OrderMuneFilter.$inject = [];

    function OrderMuneFilter() {
        return function (obj, vars) {
            if (typeof obj === 'object' && obj.length > 0) {
                return obj.sort(function (a, b) {
                    // if they are equal, return 0 (no sorting)
                    if (a.location === b.location) { return 0; }
                    if (a.location > b.location) {
                        // if a should come after b, return 1
                        return 1;
                    }
                    else {
                        // if b should come after a, return -1
                        return -1;
                    }
                });
            }
            return obj;
        }
    }
})();