﻿(function() {

    angular.module('WorkerPortfolioApp').filter('ShowList', function () {
        return function (obj, vars) {
            if (typeof obj === 'object') {
                var start = vars.PageSize * (vars.PageNumber - 1);
                var end = vars.PageSize * vars.PageNumber;
                return obj.slice(start, end);
            }
            return obj;
        }
    });

})();