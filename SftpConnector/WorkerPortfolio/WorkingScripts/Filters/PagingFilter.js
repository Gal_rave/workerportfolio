﻿(function() {
    angular.module('WorkerPortfolioApp').filter('pagingFilter', function () {
        return function (obj, properties) {
            if (typeof obj === 'object') {
                
                var startAt = 0;
                var endAt = angular.copy(properties.MaxPages);

                if (properties.MaxPages > 4) {
                    startAt = properties.PageNumber - 4 > 0 ? properties.PageNumber - 4 : 0;
                    endAt = properties.PageNumber + 3 > properties.MaxPages ? properties.MaxPages : properties.PageNumber + 3;
                }

                return obj.slice(startAt, endAt);
            }
            return obj;
        }
    });
    
})();