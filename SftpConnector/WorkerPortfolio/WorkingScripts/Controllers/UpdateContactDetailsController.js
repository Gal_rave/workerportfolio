﻿(function () {

    angular.module('WorkerPortfolioApp').controller('UpdateContactDetailsController', UpdateContactDetailsController);

    UpdateContactDetailsController.$inject = ['$scope', '$rootScope', '$state', 'PayCheckEmail', 'toastrService', 'UpdateTypes'];

    function UpdateContactDetailsController($scope, $rootScope, $state, PayCheckEmail, toastrService, UpdateTypes) {

        var goHome = function () {
            $state.go('Home');
        }

        $scope.UpdateTypes = UpdateTypes.data;

        $scope.WSPostModel = angular.copy($rootScope.getWSPostModel());
        $scope.UserDetails = {
            Cell: '',
            Email: '',
            token: '',
            ToUpDate: null,
            showValidation: false
        };

        switch ($scope.UpdateTypes) {
            case 1:///user only have cell
                $scope.UserDetails.ToUpDate = true;
                break;
            case 2:///user only have mail
                $scope.UserDetails.ToUpDate = false;
                break;
            case 4://user have both cell & mail
                $scope.UserDetails.ToUpDate = null;
                break;
            default:
                toastrService.error('לא ניתן לעדכן נתונים כרגע.', 'שגיאת מערכת!', { onTap: function () { $state.go('Home'); }, onHidden: function () { $state.go('Home'); } });
                break;
        }

        $scope.Submit = function () {
            if (!$scope.UserDetailsForm.$valid || $scope.UserDetailsForm.$invalid) {
                return void [0];
            }

            if (!$scope.UserDetails.showValidation)///first step check password and send token
                $scope.CheckPasswordAndSendToken();
            else ///second step update the data in db AND java
                $scope.UpdateNewData();
        }
        $scope.ReSendToken = function () {
            PayCheckEmail.ValidateUserAndSendToken($scope.WSPostModel)
                .then(function successCallback(res) {
                    if (!res.data.success || res.data.exceptionId > 0) {///en error has accured in the process
                        switch (res.data.exceptionId) {
                            case 10:
                                toastrService.error('המערכת לא הצליחה לאמת את סיסמתך!', '');
                                break;
                            case 20:
                                toastrService.error('המערכת לא הצליחה לשלוח את קוד האימות, אנא נסה שנית.', 'התרחשה שגיאה!');
                                break;
                            default:
                                toastrService.error('', 'תקלת מערכת!');
                                break;
                        }

                        return void [0];
                    }

                    toastrService.success('קוד האימות התקף הינו הקוד החדש ביותר, אנא המתן מספר רגעים לפני משלוח חוזר.', 'קוד אימות נשלח שנית!', { timeOut: 9000 });

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.CheckPasswordAndSendToken = function () {

            /* ACTIVATE TO ADD PASSWORD CHECK */
            //$scope.WSPostModel.Password = $scope.UserDetailsForm.password.$modelValue;

            if ($scope.UserDetails.ToUpDate) {///update `email` field, send sms
                $scope.WSPostModel.Email = $scope.UserDetailsForm.Email.$modelValue;
                $scope.WSPostModel.ToSendType = 1;

            } else {///update `cell` field, send email
                $scope.WSPostModel.Cell = $scope.UserDetailsForm.Cell.$modelValue;
                $scope.WSPostModel.ToSendType = 2;
            }
            $scope.PostFirstAction();
        }
        $scope.UpdateNewData = function () {
            if (!$scope.UserDetailsForm.$valid || $scope.UserDetailsForm.$invalid) {
                return void [0];
            }

            $scope.WSPostModel.Token = $scope.UserDetailsForm.token.$modelValue;
            $scope.WSPostModel.Email = $scope.UserDetails.Email;
            $scope.WSPostModel.Cell = $scope.UserDetails.Cell;
            $scope.PostSecondAction();
        }
        $scope.PostFirstAction = function () {
            PayCheckEmail.ValidateUserAndSendToken($scope.WSPostModel)
                .then(function successCallback(res) {
                    if (!res.data.success || res.data.exceptionId > 0) {///en error has accured in the process
                        switch (res.data.exceptionId) {
                            case 10:
                                toastrService.error('המערכת לא הצליחה לאמת את סיסמתך!', '');
                                break;
                            case 20:
                                toastrService.error('המערכת לא הצליחה לשלוח את קוד האימות, אנא נסה שנית.', 'התרחשה שגיאה!');
                                break;
                            default:
                                toastrService.error('', 'תקלת מערכת!');
                                break;
                        }

                        return void [0];
                    }

                    $scope.UserDetails.showValidation = true;

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.PostSecondAction = function () {
            PayCheckEmail.ValidateTokenAndUpdate($scope.WSPostModel)
                .then(function successCallback(res) {
                    if (!res.data.success || res.data.exceptionId > 0) {///en error has accured in the process
                        switch (res.data.exceptionId) {
                            case 30:
                                toastrService.error('המערכת לא הצליחה לאמת את קוד האימות.', 'שגיאה!');
                                break;
                            case 40:
                                toastrService.error('עדכון הנתונים נכשל.', 'שגיאה!');
                                break;
                            default:
                                toastrService.error('', 'תקלת מערכת!');
                                break;
                        }

                        return void [0];
                    }

                    toastrService.success('', 'הנתונים התעדכנו במערכת.', { timeOut: 2000, onTap: goHome, onHidden: goHome });

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

    }
})();