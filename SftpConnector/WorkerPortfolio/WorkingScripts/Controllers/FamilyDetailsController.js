﻿(function () {
    angular.module('WorkerPortfolioApp').controller('FamilyDetailsController', FamilyDetailsController);

    FamilyDetailsController.$inject = ['$scope', '$rootScope', '$state', 'TofesDataService', 'TofesModel', 'FileUploader', 'toastrService', 'Search'];

    function FamilyDetailsController($scope, $rootScope, $state, TofesDataService, TofesModel, FileUploader, toastrService, Search) {
        
        ///private variables
        $scope.childTouched = false;
        $scope.tofesModel = TofesModel;
        ///update from saved data if have        
        TofesDataService.GetExistingData($scope.tofesModel, '$$tofesModel', false);
        
        $scope.NewOptions = {
            nextText: 'הבא',
            prevText: 'הקודם',
            maxDate: new Date(),
        }

        $scope.partnetTitle = $scope.tofesModel.FormType === 3 ? 'ג' : 'ו';
        $scope.childrenTitle = $scope.tofesModel.FormType === 3 ? 'ד' : 'ג';

        $scope.decimalToHexString = function (number) {
            if (number < 0) {
                number = 0xFFFFFFFF + number + 1;
            }

            return number.toString(16).toUpperCase();
        }

        $scope.childId = -1;
        $scope.goToStep = '';
        ///get radio buttons data
        $scope.radionStates = TofesDataService.GetRadiosStates();
        ///get custom models for tofesModel lists
        $scope.customModel = TofesDataService.GetCustomModel();

        ///private function - copy child object
        $scope.copyChild = function () {
            var child = angular.copy($scope.customModel.childModel);
            child.ID = $scope.childId--;
            return child;
        }
        ///add/remove child from list
        $scope.addChild = function () {
            $scope.tofesModel.children.push($scope.copyChild());
            $rootScope.$$FamilyDetailsControllerData = $scope.tofesModel;
            $scope.addedChildCounter++;
        }
        $scope.removeChild = function ($id) {
            $scope.tofesModel.children = _.filter($scope.tofesModel.children, function (item) {
                return item.ID !== $id
            });
            $rootScope.$$FamilyDetailsControllerData = $scope.tofesModel;
            if ($id < 0)
                $scope.addedChildCounter--;
        }

        ///go-to next/prev form stap, published from Header directive
        $scope.subscribe('goto.step', function (goTo, direction) {
            if ($scope.validatUploders() || $scope.partForm.$invalid && direction) {
                $scope.ErrorFocus();
                return void [0];
            }
            
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            ///go to the step
            $state.go(goTo);
        });

        ///main submit form function
        $scope.nextForm = function () {
            ///form is invalid
            if ($scope.validatUploders() || $scope.partForm.$invalid || $scope.validatChildren()) {
                $scope.ErrorFocus();
                return void [0];
            }

            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            $scope.publish('$$states', 'current');

            return $scope.goToStep.isempty() ? (void [0]) : $state.go($scope.goToStep);
        }

        $scope.ErrorFocus = function () {
            if (typeof $scope.partForm.$error.required == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
            if (typeof $scope.partForm.$error.custom == 'object') {
                $scope.partForm.$error.custom[0].$$element.focus();
                return;
            }
        }

        $scope.HaveIdNumberFile = function () {
            var found = $scope.tofesModel.TofesControllerFilesList.find(function (file) {
                return file.filename.indexOf('idNumberFile') >= 0;
            });
            return (found != null && found != undefined);
        }

        $scope.addedChildCounter = 0;
        $.map($scope.tofesModel.children,function (c) {
            if (c.ID < 0)
                $scope.addedChildCounter++;
        });
        
        $scope.validatUploders = function () {
            $scope.partForm.idNumberFiles.$setValidity('required', true);
            if (!$scope.HaveIdNumberFile() && $scope.addedChildCounter > 0) {
                if ($scope.IdNumberuploader.queue.length < 1 || ($scope.IdNumberuploader.queue.length > 0 && (isNaN($scope.IdNumberuploader.progress) || $scope.IdNumberuploader.progress < 100))) {
                    $scope.partForm.idNumberFiles.$setValidity('required', false);
                    return true;
                }
            }

            for (var i in $scope) {
                if ($scope[i] instanceof FileUploader) {
                    ///SAVE UPLOADED FILES TO BE SENT
                    $rootScope.FilterFiles($scope.tofesModel.TofesControllerFilesList, $scope[i].queue, i);
                    $scope[i].Files = $scope.tofesModel.TofesControllerFilesList.filter(function (item) {
                        return item.origin === i;
                    }).length;

                    if ($scope[i].UploaderValidation())
                        return true;
                }
            }
            return false;
        }

        $scope.validatChildren = function () {
            $scope.childTouched = false;
            var list = $.map($scope.partForm, function (control, name) {
                if (name.indexOf('child_') == 0 && (!control.$pristine && !control.$untouched)) {
                    return true;
                }
            });
            if (list && list.length > 0 && !$scope.HaveIdNumberFile())
                $scope.childTouched = true;
            return list && list.length > 0 && !$scope.HaveIdNumberFile();
        }

        ///$scope.IdNumberuploader file uploader & filters
        $scope.IdNumberuploader = new FileUploader({
            url: 'api/FilesUploades/PostFile',
            alias: 'idNumberFile',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.IdNumberuploader.filters.splice(1, 1);
        $scope.IdNumberuploader.filters.push($scope.queueLimitFilter);
        $scope.IdNumberuploader.filters.push($scope.uniqueKeyFilter);
        $scope.IdNumberuploader.filters.push($scope.sizeFilter);
        $scope.IdNumberuploader.filters.push($scope.typeFilter);
        $scope.IdNumberuploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.IdNumberuploader.onAfterAddingFile = $scope._onAfterAddingFile;
        $scope.IdNumberuploader.onCompleteItem = $scope._onCompleteItem;
        $scope.IdNumberuploader.onCompleteAll = $scope._onCompleteAll;
        $scope.IdNumberuploader.removeFromQueue = $scope._removeFromQueue;
        $scope.IdNumberuploader.UploaderValidation = $scope._UploaderValidation;
        ///end $scope.IdNumberuploader file uploader & filters
    }
})();
