﻿(function () {
    angular.module('WorkerPortfolioApp').controller('SystemsEmailTemplatesController', SystemsEmailTemplatesController);

    SystemsEmailTemplatesController.$inject = ['$scope', '$rootScope', '$state', 'toastrService', 'FileManager', 'ngDialog', 'ValidAdminUser', 'Templates'];

    function SystemsEmailTemplatesController($scope, $rootScope, $state, toastrService, FileManager, ngDialog, ValidAdminUser, Templates) {

        if (ValidAdminUser.data !== true || ValidAdminUser.status !== 200) {
            $state.go('Home');
            toastrService.info('', 'הרשאה לא תקינה!', { onHidden: function () { $state.go('Home'); } });
            return void [0];
        }

        $scope.WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'SYSTEMSEMAILROLE')

        $scope.EmailTemplates = [];
        $scope.EmailTemplateModel = {
            Editing: false,
            ID: null,
            municipalityId: $scope.WSPostModel.currentMunicipalityId,
            createdDate: new Date(),
            updateDate: new Date(),
            updateUserId: null,
            template: null,
            subject: null,
            name: null,
            fromEmail: null,
            municipality: null,
            user: null,
            type: 1
        };
        ///pager
        $scope.$Pager = {
            PageSize: 15,
            PageNumber: 1,
            Counter: 0,
            MaxPages: 0,
            pagerList: [],
            sizes: [15, 30, 45]
        };
        $scope.changePagerSize = function () {
            $scope.setUpController($scope.EmailTemplates);
        };

        $scope.getItem = function (list, num) {
            var items = list.filter(function (s) {
                return s.ID === num;
            });

            return (items instanceof Array) ? items[0] : items;
        };

        $scope.DeleteTemplate = function (tmplt) {

            ngDialog.openConfirm({
                template: '\
                <p><b>האם למחוק את</b> "' + tmplt.subject + '"?<\p>\
                <p><b>שים לב</b>, לא ניתן לשחזרת תבנית שנמחקה!</p>\
                <div class="ngdialog-buttons">\
                    <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">בטל</button>\
                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(' + tmplt.ID + ')">מחק</button>\
                </div>',
                plain: true,

                scope: $scope
            }).then(function (success) {

                FileManager.DeleteTemplate({ wsModel: $scope.WSPostModel, template: tmplt })
                    .then(function successCallback(res) {
                        if (res.data) {
                            $scope.EmailTemplates.splice($scope.EmailTemplates.indexOf(tmplt), 1);
                            $scope.setUpController($scope.EmailTemplates);
                            if (tmplt.ID === $scope.EmailTemplateModel.ID) {
                                $scope.createNewTemplate(false);
                            }

                            toastrService.success('התבנית נמחקה!', '');
                        } else {
                            toastrService.error('', 'תקלה!');
                        }
                    }, function errorCallback(res) {
                        toastrService.error('', 'תקלת מערכת!');
                    });

            }, function (error) {
                ///do nothing, user canceld
            });

        };
        $scope.SaveTemplate = function () {
            $scope.TemplateFrom.$setSubmitted();
            if ($scope.TemplateFrom.$invalid || !$scope.TemplateFrom.$valid)
                return void [0];

            $scope.EmailTemplateModel.updateDate = new Date();

            FileManager.UpdateAddTemplate({ wsModel: $scope.WSPostModel, template: $scope.EmailTemplateModel })
                    .then(function successCallback(res) {
                        var item = $scope.getItem($scope.EmailTemplates, res.data.ID);
                        if (item === undefined) {
                            $scope.EmailTemplates.push(res.data);
                        } else {
                            $scope.EmailTemplates.splice($scope.EmailTemplates.indexOf(item), 1);
                            $scope.EmailTemplates.push(res.data);
                        }
                        $scope.setUpController($scope.EmailTemplates);
                        $scope.EditTemplate(res.data);
                        toastrService.success('התבנית נשמרה!', '');

                    }, function errorCallback(res) {
                        toastrService.error('', 'תקלת מערכת!');
                    });
        };
        $scope.EditTemplate = function (template) {
            $scope.EmailTemplateModel = angular.copy(template);
            $scope.EmailTemplateModel.Editing = true;
        };
        $scope.createNewTemplate = function (state) {
            $scope.EmailTemplateModel = {
                Editing: state,
                ID: null,
                municipalityId: $scope.WSPostModel.currentMunicipalityId,
                createdDate: new Date(),
                updateDate: new Date(),
                updateUserId: null,
                template: null,
                subject: null,
                name: null,
                fromEmail: null,
                type: 1
            };
        };

        $scope.file_browser_callbackFunction = function (field_name, url, type, win) {
            try {
                $('#mce-modal-block').attr('style', '');
                var style = $('.mce-container.mce-panel.mce-floatpanel.mce-window.mce-in').attr('style');
                style = style.split(';');
                style.filter(function (s) {
                    if (s.indexOf('z-index') >= 0) {
                        style[style.indexOf(s)] = 'z-index: 123';
                    }
                });
                $('.mce-container.mce-panel.mce-floatpanel.mce-window.mce-in').attr('style', style.join(';'));
            } catch (e) {
                
            }

            ngDialog.open({
                template: '../../Scripts/views/Email-Templates/file-manager.html',
                closeByNavigation: true,
                closeByDocument: true,
                closeByEscape: true,
                scope: $scope,
                backdrop: 'static',
                showClose: true,
                appendClassName: 'file-manager-custom',
                name: 'fileManagerPopUp',
                resolve: {
                    fileStructure: ['FileManager', function (FileManager) {
                        return FileManager.GetFileStructure($scope.WSPostModel);
                    }],
                    options: [function () {
                        return { field_name: field_name, url: url, type: type, win: win };
                    }]
                },
                controller: 'FileManagerController'
            });
        };
        $scope.tinymceOptions = {
            selector: "textarea", theme: "modern", height: 400,
            plugins: [
                 "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                 "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                 "table contextmenu directionality emoticons paste textcolor code",
                 "directionality"
            ],
            toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | link unlink anchor | image media | forecolor backcolor  | ltr rtl",

            directionality: "rtl",

            file_browser_callback: $scope.file_browser_callbackFunction
        };

        /* *** set up controller *** */
        $scope.setUpController = function (templates) {
            $scope.EmailTemplates = templates;
            $scope.$Pager.PageNumber = 1;

            $scope.$Pager.Counter = templates.length;
            $scope.$Pager.MaxPages = Math.ceil(templates.length / $scope.$Pager.PageSize);
            var num = 1;

            $scope.$Pager.pagerList = [];
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                $scope.$Pager.pagerList.push(1 + i);
            }
        };
        $scope.setUpController(Templates.data);
    }
})();
