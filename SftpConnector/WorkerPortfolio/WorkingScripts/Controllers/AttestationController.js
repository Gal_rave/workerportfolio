﻿(function () {
    angular.module('WorkerPortfolioApp').controller('AttestationController', AttestationController);

    AttestationController.$inject = ['$window', '$scope', '$rootScope', '$state', '$timeout', '$sce', 'TofesDataService', 'TaxFromModelService', 'TofesModel', 'FileUploader', 'toastrService', 'Search', 'fileUploade', 'fileUploadeFactory', 'formDataService', 'Tofes'];

    function AttestationController($window, $scope, $rootScope, $state, $timeout, $sce, TofesDataService, TaxFromModelService, TofesModel, FileUploader, toastrService, Search, fileUploade,
        fileUploadeFactory, formDataService, Tofes) {
        
        var b64toBlob = function (b64Data, contentType) {
            contentType = contentType || '';
            var sliceSize = 512;
            b64Data = b64Data.replace(/^[^,]+,/, '');
            b64Data = b64Data.replace(/\s/g, '');
            var byteCharacters = window.atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }

        $scope.pdfContent = '';
        $scope.$pdfContent = false;
        $scope.pdfDataStatus = false;

        ///private variables
        ///get radio buttons data
        $scope.radionStates = TofesDataService.GetRadiosStates();
        ///get custom models for tofesModel lists
        $scope.customModel = TofesDataService.GetCustomModel();
        $scope.MaxDateLimit = $rootScope.LocalDateString(new Date());
        $scope.tofesModel = TofesModel;
        $scope.LocalCredentials = typeof $rootScope.$$credentials !== 'undefined' ? $rootScope.$$credentials : $rootScope.lsGet('$$credentials');
        ///update from saved data if have
        TofesDataService.GetExistingData($scope.tofesModel, '$$tofesModel', true);
        ///set the signatur date for today
        $scope.tofesModel.signatureDate = TofesDataService.TransformToDate(new Date());

        ///go-to next/prev form stap, published from Header directive
        $scope.subscribe('goto.step', function (goTo, direction) {
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            ///go to the step
            $state.go(goTo);
        });
        ///hide the form 101 pdf
        $scope.subscribe('hidePdf', function (hide) {
            $scope.pdfContent = '';
            $scope.$pdfContent = false;
        });
        ///submit form 101 data to WS
        $scope.subscribe('submitPdf', function (hide) {
            fileUploadeFactory.SendForm(TaxFromModelService.SetModel(TofesDataService.CheckRunTimeCopy($scope.tofesModel), $scope.LocalCredentials.currentMunicipalityId))
                .then(function successCallback(response) {
                    if (response.status == 200 && response.data.success) {
                        toastrService.custom('בסיום התהליך תשלח אליך לנייד/אימייל הודעה.', 'הטופס נשלח לארכיון בהצלחה', { iconClass: 'toast-grean-full toast-success' });
                        $rootScope.lsRemove('$$tofesModel');
                        $rootScope.lsRemove('$$tofesModelCopy');
                        $rootScope.lsRemove('states');
                        $state.go('Home');
                    }
                    else {
                        switch (response.data.exceptionId) {
                            case -1:///archive error only
                                toastrService.error('אנא נסה שנית.', 'התרחשה תקלה בשליחת הטופס לארכיון!');
                                $scope.publish('returnToLastStep', true);
                                break;
                            case -2:///post data error only
                                toastrService.error('אנא נסה שנית.', 'התרחשה תקלה בשמירת המידע בטופס!');
                                $scope.publish('returnToLastStep', true);
                                break;
                            case -3:///archive AND post data error
                                toastrService.error('אנא צור את הטופס מהתחלה.', 'התרחשה תקלת שרת!');
                                $rootScope.lsRemove('$$tofesModel');
                                $rootScope.lsRemove('$$tofesModelCopy');
                                $rootScope.lsRemove('states');
                                $state.go('Home');
                                break;
                        }
                    }
                }, function errorCallback(response) {
                    $scope.publish('returnToLastStep', true);
                    //console.log('submitPdf errorCallback', response);
                    toastrService.error('אנא נסה שנית מאוחר יותר', 'תקלת מערכת!');
                });
        });
        $scope.validateSignature = function () {
            $scope.partForm.hidden_signature.$setValidity('required', true);
            if (!$scope.tofesModel.signature.isEmpty()) {
                $scope.tofesModel.hidden_signature = 1;
            } else {
                $scope.tofesModel.hidden_signature = null;
                $scope.partForm.hidden_signature.$setValidity('required', false);
            }
        }
        ///main submit form function
        $scope.saveEntireForm = function () {
            $scope.validateSignature();
            ///form is invalid
            if ($scope.partForm.$invalid) {
                $scope.ErrorFocus();
                $scope.publish('returnToLastStep', true);
                return void [0];
            }
            
            ///save the form state
            $rootScope.lsSet('$$tofesModel', $scope.tofesModel);
            $scope.publish('$$states', 'current');

            ///first step-> save the signature;
            fileUploadeFactory.Signature($scope.tofesModel.employee.idNumber, $scope.tofesModel.year, $scope.tofesModel.signature.toDataURL(), $scope.LocalCredentials.confirmationToken, $scope.LocalCredentials.currentMunicipalityId)
                .then(function successCallback(response) {
                    if (response.data.saved) {
                        $scope.fileimageblob = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXY3j//j0ABZ4CzkTR0PgAAAAASUVORK5CYII=';

                        $scope.tofesModel.signatureImageID = response.data.image.ID;
                        fileUploadeFactory.SaveForm(TaxFromModelService.SetModel(TofesDataService.CheckRunTimeCopy($scope.tofesModel), $scope.LocalCredentials.currentMunicipalityId))
                            .then(function successCallback(response) {
                                if (response.data.sign) {
                                    $scope.pdfContent = '';
                                    $scope.$pdfContent = false;
                                    $scope.formMsg = '';
                                    $scope.pdfDataStatus = response.status;
                                    if (response.status === 406)
                                        return void [0];
                                    $scope.tofesModel.formPdfImageID = response.data.pdf.ID;
                                    $scope.tofesModel.formPdf = response.data.pdf;
                                    $scope.tofesModel.version = response.data.version;
                                    ///save the form state
                                    $rootScope.lsSet('$$tofesModel', $scope.tofesModel);

                                    var file = b64toBlob(response.data.pdf.fileString, 'application/pdf');
                                    var fileURL = URL.createObjectURL(file);
                                    $scope.pdfContent = $sce.trustAsResourceUrl(fileURL);
                                    $scope.$pdfContent = true;
                                    $scope.formMsg = 'תצוגה מקדימה - טופס 101.';

                                    $scope.fileimageblob = response.data.pdf.fileimageblob;
                                } else {
                                    $scope.publish('returnToLastStep', true);
                                    //console.log('SaveForm error', response);
                                    toastrService.error('המערכת לא הצליחה לשמור טופס 101!', 'התרחשה תקלה!');
                                }
                            }, function errorCallback(response) {
                                $scope.publish('returnToLastStep', true);
                                //console.log('SaveForm errorCallback', response);
                                toastrService.error('המערכת לא הצליחה לשמור טופס 101!', 'התרחשה תקלה!');
                            });
                    } else {
                        $scope.publish('returnToLastStep', true);
                        //console.log('Signature error', response);
                        toastrService.error('המערכת לא הצליחה לשמור את החתימה!', 'התרחשה תקלה!');
                        return void [0];
                    }
                }, function errorCallback(response) {
                    $scope.publish('returnToLastStep', true);
                    //console.log('Signature errorCallback', response);
                    toastrService.error('המערכת לא הצליחה לשמור את החתימה!', 'התרחשה תקלה!');
                });

            return void [0];
        }

        $scope.ErrorFocus = function () {
            if (typeof $scope.partForm.$error.required == 'object') {
                $scope.partForm.$error.required[0].$$element.focus();
                return;
            }
            if (typeof $scope.partForm.$error.custom == 'object') {
                $scope.partForm.$error.custom[0].$$element.focus();
                return;
            }
        }
    }
})();