﻿(function () {

    angular.module('WorkerPortfolioApp').controller('UserEmailPayCheckController', UserEmailPayCheckController);

    UserEmailPayCheckController.$inject = ['$scope', '$rootScope', '$state', 'ngDialog', 'PayCheckEmail', 'UPCData', 'toastrService'];

    function UserEmailPayCheckController($scope, $rootScope, $state, ngDialog, PayCheckEmail, UPCData, toastrService) {

        var goHome = function () {
            $state.go('Home');
        }
        
        $scope.$email = UPCData.data.Email;
        $scope.payCheck = UPCData.data.PayCheck;
        $scope.WSPostModel = $rootScope.getWSPostModel();

        ///calc the user UEPC state
        $scope.USPCTE = {
            iAgree: $scope.payCheck != null && $scope.payCheck.activeRecord && $scope.payCheck.sendMail,
            haveMail: $scope.$email != null && $scope.$email.email != null && $scope.$email.email.length > 3,
            email: $scope.$email != null ? $scope.$email.email : null,
            PanelToShow: 0
        }

        if ($scope.USPCTE.iAgree && $scope.USPCTE.haveMail)///have USPCTE
            $scope.USPCTE.PanelToShow = 1;
        if (!$scope.USPCTE.iAgree && $scope.USPCTE.haveMail)///can have USPCTE
            $scope.USPCTE.PanelToShow = 2;
        if (!$scope.USPCTE.haveMail)///cant have USPCTE
            $scope.USPCTE.PanelToShow = 3;

        if ($scope.USPCTE.PanelToShow === 2)
            $scope.USPCTE.iAgree = true;

        $scope.UEPCStatusSubmit = function () {
            if ($scope.UpdateUEPCStatus.removeUSPCTE == undefined && !$scope.UpdateUEPCStatus.iAgree.$modelValue)
                return void [0];

            $scope.WSPostModel.isMobileRequest = ($scope.UpdateUEPCStatus.removeUSPCTE != undefined && $scope.UpdateUEPCStatus.removeUSPCTE.$modelValue) ? false : $scope.UpdateUEPCStatus.iAgree.$modelValue;
            $scope.UpdateUEPCStatus.iAgree.$setViewValue($scope.WSPostModel.isMobileRequest);

            PayCheckEmail.UserSendPayCheckToEmail($scope.WSPostModel)
                .then(function successCallback(res) {
                    if (res.status == 200) {
                        if ($scope.USPCTE.PanelToShow == 1 && !$scope.WSPostModel.isMobileRequest)
                            $scope.USPCTE.PanelToShow = 2;
                        if ($scope.USPCTE.PanelToShow == 2 && $scope.WSPostModel.isMobileRequest)
                            $scope.USPCTE.PanelToShow = 1;

                        if ($scope.WSPostModel.isMobileRequest)
                            toastrService.success('בקשתך להצטרף לקבלת תלוש במייל התקבלה בהצלחה!', '', { timeOut: 3500, onTap: goHome, onHidden: goHome });
                        else
                            toastrService.warning('בקשתך להפסיק את השירות התקבלה!', '', { timeOut: 3500, onTap: goHome, onHidden: goHome });

                    } else {
                        toastrService.error('אנא עדכן את הדף ונסה שנית.', 'התרחשה תקלה!', { timeOut: 3500, onTap: goHome, onHidden: goHome });
                    }
                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!', { timeOut: 3500, onTap: goHome, onHidden: goHome });
                });
        }
    }
})();
