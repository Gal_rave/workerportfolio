﻿(function () {
    

    angular.module('WorkerPortfolioApp').controller('UserMenuController', UserMenuController);

    UserMenuController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', 'FileUploader', 'toastrService', '$controllerState', '$passwordSettings', 'UserService', 'Credentials'];

    function UserMenuController($scope, $rootScope, $state, $stateParams, FileUploader, toastrService, $controllerState, $passwordSettings, UserService, Credentials) {
        
        $scope.$$UserCredentials = typeof $rootScope.$$credentials !== 'undefined' ? $rootScope.$$credentials : $scope.lsGet('$$credentials');
        $scope.$passwordSettings = $passwordSettings.data;
        $scope.$controllerState = $controllerState;
        $scope.toMailItems = new Array();
        $scope.Notification = {
            toMailID: null,
            jobTitle: null,
            phone: null,
            department: null,
            userName: $scope.$$UserCredentials.loggedIn? ($scope.$$UserCredentials.firstName + ' ' + $scope.$$UserCredentials.lastName) : '',
            subject: null,
            text: null,
            files: '',
            userIdNumber: $scope.$$UserCredentials.idNumber,
            senderEmail: null,
            recipientEmail:null
        };
        
        $scope.ReturnNotification = { ID: -1 };
        $scope.WSPostModel = {
            idNumber: $scope.$$UserCredentials.idNumber,
            confirmationToken: $scope.$$UserCredentials.confirmationToken,
            calculatedIdNumber: $scope.$$UserCredentials.calculatedIdNumber,
            currentMunicipalityId: $scope.$$UserCredentials.currentMunicipalityId,
            selectedDate: new Date(),
        };

        ///NotificationUploader file uploader & filters
        $scope.NotificationUploader = new FileUploader({
            url: 'api/FilesUploades/NotificationUploader',
            queueLimit: 5,
            removeAfterUpload: false,
            //maxFileSize: 2,
            autoUpload: true
        });
        ///replace queueLimit
        $scope.NotificationUploader.filters.splice(1, 1);
        $scope.NotificationUploader.filters.push($scope.queueLimitFilter);
        $scope.NotificationUploader.filters.push($scope.uniqueKeyFilter);
        $scope.NotificationUploader.filters.push($scope.sizeFilter);
        $scope.NotificationUploader.onWhenAddingFileFailed = $scope._onWhenAddingFileFailed;
        $scope.NotificationUploader.onAfterAddingFile = function (fileItem) {
            fileItem.formData = [{ idNumber: $scope.$$UserCredentials.idNumber, filename: fileItem.file.name }];
        };
        $scope.NotificationUploader.onCompleteItem = $scope._onCompleteItem;
        $scope.NotificationUploader.onCompleteAll = $scope._onCompleteAll;
        $scope.NotificationUploader.removeFromQueue = $scope._removeFromQueue;
        ///end NotificationUploader file uploader & filters

        switch ($controllerState) {
            case 'ResetCurrentPassword':
                $scope.UserCredentials = {
                    newPassword: null,
                    reEnterPassword: null,
                    oldPassword: null,
                    token: $scope.$$UserCredentials.confirmationToken,
                    passwordResetType: 0
                };
                
                break;
            case 'HumanResourcesNotify':
                //$scope.Notification = {};
                UserService.GetToMailItems($scope.WSPostModel)
                    .then(function successCallback(response) {
                       $scope.toMailItems = response.data;                       
                    }, function errorCallback(response) {
                        //console.log('full-error', response);
                        toastrService.error('', 'תקלת מערכת!');
                    });
                if ($scope.$$UserCredentials.loggedIn)
                    Credentials.GetFullUserdetails({ Token: $scope.$$UserCredentials.confirmationToken, newPassword: null })
                        .then(function successCallback(res) {
                            $scope.Notification.jobTitle = res.data.jobTitle;
                            $scope.Notification.phone = res.data.cell;
                            $scope.Notification.jobTitle = res.data.jobTitle;
                            $scope.Notification.userIdNumber = res.data.idNumber;
                            
                        }, function errorCallback(res) {
                            //console.log('full-error', res);
                            toastrService.error('', 'תקלת מערכת!');
                        });
                break;
            case 'ContactUs':
                if ($scope.$$UserCredentials.loggedIn)
                    Credentials.GetFullUserdetails({ Token: $scope.$$UserCredentials.confirmationToken, newPassword: null })
                        .then(function successCallback(res) {
                            $scope.Notification.senderEmail = res.data.email;
                            $scope.Notification.phone = res.data.cell;
                            $scope.Notification.userIdNumber = res.data.idNumber;
                            
                        }, function errorCallback(res) {
                            //console.log('full-error', res);
                            toastrService.error('', 'תקלת מערכת!');
                        });
                break;
            default:
                $state.go('Home');
                break;
        }
        
        $scope.validatePasswords = function () {
            $scope.UpdatePasswordForm.newPassword.$setValidity('custom', true);
            $scope.UpdatePasswordForm.reEnterPassword.$setValidity('custom', true);
            ///return if not valid on other checks
            if ($scope.UpdatePasswordForm.reEnterPassword.$error.required || $scope.UpdatePasswordForm.reEnterPassword.$error.minlength || $scope.UpdatePasswordForm.reEnterPassword.$error.maxlength
                || $scope.UpdatePasswordForm.newPassword.$error.required || $scope.UpdatePasswordForm.newPassword.$error.minlength || $scope.UpdatePasswordForm.newPassword.$error.maxlength)
                return void [0];
            ///check minRequiredNonAlphanumericCharacters
            if ($scope.UserCredentials.newPassword.replace(/[^\W|_]/gi, '').length < $scope.$passwordSettings.minNonAlphanumeric) {
                $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] תווים מיוחדים!'.replace('[0]', $scope.$passwordSettings.minNonAlphanumeric);
                $scope.UpdatePasswordForm.newPassword.$setValidity('custom', false);
                return void [0];
            }
            ///check minRequiredNumericCharacters
            if ($scope.UserCredentials.newPassword.replace(/[\D|_]/gi, '').length < $scope.$passwordSettings.minNumeric) {
                $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] מספרים!'.replace('[0]', $scope.$passwordSettings.minNumeric);
                $scope.UpdatePasswordForm.newPassword.$setValidity('custom', false);
                return void [0];
            }
            ///passwords dont match
            if ($scope.UserCredentials.newPassword !== $scope.UserCredentials.reEnterPassword) {
                $scope.UpdatePasswordForm.reEnterPassword.$setValidity('custom', false);
                return void [0];
            }
        }

        $scope.updatePassword = function () {
            $scope.validatePasswords();
            if ($scope.UpdatePasswordForm.$invalid)
                return void [0];

            UserService.ResetPassword($scope.UserCredentials)
                .then(function successCallback(response) {
                    if (response.data.success) {
                        toastrService.success('סיסמתך עודכנה בהצלחה!', 'תודה.');
                    } else {
                        switch (response.data.exceptionId) {
                            case 61:///GetUserFromToken
                                toastrService.error('סיסמתך אינה נכונה!', 'שגיאת אימות נתונים!');
                                break;
                            case 1:///InvalidPassworException
                                toastrService.error('סיסמתך אינה נכונה!', 'שגיאת אימות נתונים!');
                                break;
                            case 102:///"NullUserReferenceException" error in token
                                $scope.publish('CatchTransmitLogout');
                                toastrService.error('אנא בצע כניסה מחודשת לאתר.', 'שגיאת אימות נתונים!');
                                break;
                            case 101:///user is locked out
                                $scope.publish('CatchTransmitLogout');
                                toastrService.error('אנא נסה להתחבר מחדש בעוד LOCKTIMELEFT דקות'.replace('LOCKTIMELEFT', response.data.lockTimeLeft), 'סליחה, ננעלת מחוץ למערכת.');
                                break;
                            default:
                                //console.log('error', response);
                                toastrService.error('', 'תקלת מערכת!');
                                break;
                        }
                    }
                }, function errorCallback(response) {
                    //console.log('full-error', response);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.sendNotification = function () {
            if ($scope.NotificationForm.$invalid)
                return void [0];

            if ($scope.NotificationUploader.getNotUploadedItems().length > 0) {
                toastrService.info('על מנת לשלוח את ההודעה:<br/>יש לבצע שמירה/הסרה של הקבצים.<br/>לחלופין ניתן לבצע שליחה חוזרת של הטופס.', 'שם לב!<br/> יש קבצים שלא הועלו לשרת!',
                    { clearThenShow: true, allowHtml: true, timeOut: 9999, closeButton: true });
                return void [0];
            }

            $scope.Notification.files = typeof $scope.NotificationUploader.successItemsList !== 'undefined' && $scope.NotificationUploader.successItemsList.length > 0 ? $scope.NotificationUploader.successItemsList.join(',') : '';
            $scope.Notification.toMailID = $scope.NotificationForm.toMail.$modelValue.ID;
                        
            UserService.SendNotificationMail($scope.Notification)
                .then(function successCallback(response) {
                    if (response.data != null && response.data.ID > 0) {///success
                        toastrService.success('ההודעה נשלחה בהצלחה');
                        $scope.ReturnNotification = response.data;
                        $scope.ReturnNotification.toMail = $scope.NotificationForm.toMail.$modelValue;
                    } else {///error
                        toastrService.error('התרחשה תקלה במשלוח המייל.', 'תקלה');
                    }
                }, function errorCallback(response) {
                    //console.log('full-error', response);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.sendContactUs = function () {
            if ($scope.ContactUsForm.$invalid)
                return void [0];

            UserService.SendContactUsMail($scope.Notification)
                .then(function successCallback(response) {
                    if (response.data != null && response.data.ID > 0) {///success
                        toastrService.success('ההודעה נשלחה בהצלחה');
                        $scope.ReturnNotification = response.data;
                    } else {///error
                        toastrService.error('התרחשה תקלה במשלוח המייל.', 'תקלה');
                    }
                }, function errorCallback(response) {
                    //console.log('full-error', response);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.ReturnFromNotification = function () {
            $scope.ReturnNotification.ID = -1;
            $scope.Notification.subject = null;
            $scope.Notification.text = null;

            switch ($controllerState) {
                case 'ContactUs':                    
                    break;
                case 'HumanResourcesNotify':
                    //$scope.NotificationUploader.clearQueue();
                    $scope.NotificationUploader.queue = [];
                    $scope.NotificationUploader.progress = 0;
                    $scope.NotificationUploader.successItemsList = [];
                    break;
            }
        }
    }
})();
