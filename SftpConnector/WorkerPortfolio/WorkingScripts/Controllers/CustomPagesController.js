﻿(function () {

    angular.module('WorkerPortfolioApp').controller('CustomPagesController', CustomPagesController);

    CustomPagesController.$inject = ['$scope', '$rootScope', '$Page'];

    function CustomPagesController($scope, $rootScope, $Page) {
        $scope.Page = $Page.data;
        $scope.Page.subject = $scope.Page.subject !== null && $scope.Page.subject.length > 1 ? $scope.Page.subject : $scope.Page.name;
    }
})();
