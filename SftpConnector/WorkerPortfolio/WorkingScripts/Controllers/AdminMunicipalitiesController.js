﻿(function () {
    angular.module('WorkerPortfolioApp').controller('AdminMunicipalitiesController', AdminMunicipalitiesController);

    AdminMunicipalitiesController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$timeout', 'FileSaver', 'toastrService', 'Admin', 'ValidAdminUser', 'municipalities', '$municipality'];

    function AdminMunicipalitiesController($scope, $rootScope, $state, $stateParams, $timeout, FileSaver, toastrService, Admin, ValidAdminUser, municipalities, $municipality) {

        if (ValidAdminUser.data !== true || ValidAdminUser.status !== 200) {
            $state.go('Home');
            toastrService.info('', 'הרשאה לא תקינה!', { onHidden: function () { $state.go('Home'); } });
            return void [0];
        }


        $scope.getWSPostModel = $rootScope.getWSPostModel();
        $scope.MunicipalitiesList = [];
        $scope.UserSearch = {
            idNumber: $scope.getWSPostModel.idNumber,
            confirmationToken: $scope.getWSPostModel.confirmationToken,
            page: 1,
            hidden_value: false,
            $hidden_value: '',
            searchIdNumber: $stateParams.IdNUmber,
            cell: null,
            firstName: null,
            lastName: null,
            password: null,
            newPassword: null,
            reEnterPassword: null,
            municipalities: null,
            userToken: null,
            searchMunicipalityName: null,
            searchMunicipalitiesId: null,
            active: null,
            has101: null,
            hasProcess: null,
            hasEmailPaycheck: null,
            hasMunicipalityMenu: null,
            hasSystemsEmail: null,
            municipalitiesList: [],
        };
        $scope.Municipality = $municipality !== null ? $municipality.data : null;

        $scope.UserSearch.municipalities = $scope.getWSPostModel.municipalities.map(function (elem) {
            return elem.ID;
        }).join(",");

        $scope.DeleteContact = function ($index, contact) {
            Admin.DeleteHRContact({ idNumber: $scope.UserSearch.idNumber, contactId: contact.ID }).then(function successCallback(res) {
                if (res.status === 200 && res.data) {
                    $scope.Municipality.humanResourcesContacts.splice($index, 1);
                    toastrService.success('', 'רשומה ' + contact.ID + ' נמחקה!');

                    $scope.$Pager.Counter = $scope.municipality.data.humanResourcesContacts.length;
                    $scope.$Pager.MaxPages = Math.ceil($scope.municipality.data.humanResourcesContacts.length / $scope.$Pager.PageSize);
                    for (var i = 0; i <= $scope.$Pager.MaxPages; i++)
                        $scope.$Pager.pagerList.push(i + 1);
                } else {
                    toastrService.error('רשומה לא נמחקה.', 'תקלת מערכת!');
                }
            }, function errorCallback(res) {
                toastrService.error('', 'תקלת מערכת!');
            });
        }
        $scope.StartUpdateContact = function ($index, type) {
            if ($index !== null && type !== null && $index < $scope.Municipality.humanResourcesContacts.length) {
                $scope.Municipality.humanResourcesContacts[$index].Editing = type;
            }
            $scope.Municipality.humanResourcesContacts.filter(function (hr, index) {
                if ($index === null || index !== $index) hr.Editing = false;
            });
            switch (type) {
                case true:
                    $scope.UserSearch.contact = angular.copy($scope.Municipality.humanResourcesContacts[$index]);
                    $scope.UserSearch.contact.$index = $index;
                    break;
                case false:
                    $scope.UserSearch.contact = null;
                    break;
                case null:
                    $scope.UserSearch.contact = {
                        Editing: true,
                        $index: $scope.Municipality.humanResourcesContacts.length,
                        email: '',
                        job: '',
                        name: '',
                        phone: '',
                        municipalityID: $stateParams.MunicipalityId,
                    }
                    break;
            }
        }
        $scope.UpdateContact = function ($index) {
            $scope.MunicipalityForm.$setSubmitted();
            if ($scope.MunicipalityForm.$invalid || !$scope.MunicipalityForm.$valid)
                return void [0];

            Admin.UpdateContact($scope.UserSearch).then(function successCallback(res) {
                if (res.status === 200) {
                    if ($index < $scope.Municipality.humanResourcesContacts.length) {
                        $scope.Municipality.humanResourcesContacts[$index] = res.data;

                        toastrService.success('', 'רשומה ' + res.data.ID + ' עודכנה בהצלחה!');
                    } else {
                        $scope.UserSearch.contact = null;
                        $scope.Municipality.humanResourcesContacts.push(res.data);
                        $scope.$Pager.Counter = $scope.Municipality.humanResourcesContacts.length;
                        $scope.$Pager.MaxPages = Math.ceil($scope.Municipality.humanResourcesContacts.length / $scope.$Pager.PageSize);
                        for (var i = 0; i <= $scope.$Pager.MaxPages; i++)
                            $scope.$Pager.pagerList.push(i + 1);

                        toastrService.success('', 'רשומה ' + res.data.ID + ' נוספה בהצלחה!');
                    }
                    $scope.Municipality.humanResourcesContacts[$index].Editing = false;
                    $scope.UserSearch.contact = null;

                } else {
                    $scope.Municipality.humanResourcesContacts[$index].Editing = false;
                    $scope.UserSearch.contact = null;
                    $scope.MunicipalityForm.$setSubmitted();
                    toastrService.error('רשומה לא התעדכנה.', 'תקלה!');
                }
            }, function errorCallback(res) {
                toastrService.error('', 'תקלת מערכת!');
            });
        }
        $scope.Update = function () {
            $scope.UserSearch.municipalitiesList = [];
            $scope.UserSearch.municipalitiesList.push($scope.Municipality);
            Admin.UpdateMunicipalities($scope.UserSearch)
               .then(function successCallback(res) {
                   res.data ? toastrService.success('העדכון נשמר בהצלחה!', '') : toastrService.error('', 'תקלת מערכת!');

               }, function errorCallback(res) {
                   toastrService.error('', 'תקלת מערכת!');
               });
        }

        ///pager
        $scope.$Pager = {
            PageSize: 15,
            PageNumber: 1,
            Counter: 0,
            MaxPages: 0,
            pagerList: []
        }

        if (municipalities.data && municipalities.data.length > 1) {
            $scope.$Pager.Counter = municipalities.data.length;
            $scope.$Pager.MaxPages = Math.ceil(municipalities.data.length / $scope.$Pager.PageSize);
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++)
                $scope.$Pager.pagerList.push(i + 1);

            $scope.MunicipalitiesList = municipalities.data;
        }
        if ($municipality && $municipality.data && $municipality.data.humanResourcesContacts && $municipality.data.humanResourcesContacts.length > 1) {
            $scope.$Pager.Counter = $municipality.data.humanResourcesContacts.length;
            $scope.$Pager.MaxPages = Math.ceil($municipality.data.humanResourcesContacts.length / $scope.$Pager.PageSize);
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++)
                $scope.$Pager.pagerList.push(i + 1);
        }


        $scope.allow101Change = function (municipality) {
            if (municipality.allow101)
                municipality.active = true;
            municipality.$pristine = false;
        }

        $scope.SearchMunicipality = function () {
            $scope.$Pager.PageNumber = 1;
            Admin.GetMunicipalities($scope.UserSearch)
                .then(function successCallback(res) {
                    $scope.MunicipalitiesList = [];
                    $scope.$Pager.Counter = res.data.length;
                    $scope.$Pager.MaxPages = Math.ceil(res.data.length / $scope.$Pager.PageSize);

                    $scope.$Pager.pagerList = [];
                    for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                        $scope.$Pager.pagerList.push(i + 1);
                    }

                    $scope.MunicipalitiesList = res.data;

                }, function errorCallback(res) {
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.updateMunicipalitiesList = function () {
            $scope.UserSearch.municipalitiesList = [];
            $scope.UserSearch.municipalitiesList = $.grep($scope.MunicipalitiesList, function (item) {
                return item.$pristine === false;
            });

            Admin.UpdateMunicipalities($scope.UserSearch)
               .then(function successCallback(res) {
                   res.data ? toastrService.success('העדכון נשמר בהצלחה!', '') : toastrService.error('', 'תקלת מערכת!');

               }, function errorCallback(res) {
                   toastrService.error('', 'תקלת מערכת!');
               });
        }

        $scope.updateMunicipality = function (municipality) {
            $scope.UserSearch.municipality = municipality;

            Admin.UpdateAuthorizationList($scope.UserSearch)
                .then(function successCallback(res) {

                    toastrService.success('העדכון נשמר בהצלחה!', '');

                }, function errorCallback(res) {
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.Export = function () {
            var dataObj = [];
            $.map($scope.MunicipalitiesList, function (item) {
                dataObj.push({
                    'מספר_ארגון': item.rashut,
                    'שם_ארגון': item.name,
                    'פעיל': item.active ? 'כן' : 'לא',
                    '101_דיגיטאלי': item.allow101 ? 'כן' : 'לא',
                    'דפי_ארגון': item.allowMunicipalityMenu ? 'כן' : 'לא',
                    'מיילר': item.allowSystemsEmail ? 'כן' : 'לא',
                    'תלוש_במייל': item.allowemailpaycheck ? 'כן' : 'לא',
                    'תהליכים': item.allowprocess ? 'כן' : 'לא',
                });
            });
            return dataObj;
        }
    }
})();
