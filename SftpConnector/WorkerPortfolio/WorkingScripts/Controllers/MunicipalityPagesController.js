﻿(function () {
    angular.module('WorkerPortfolioApp').controller('MunicipalityPagesController', MunicipalityPagesController);

    MunicipalityPagesController.$inject = ['$scope', '$rootScope', '$state', 'toastrService', 'FileManager', 'ngDialog', 'ValidAdminUser', 'Templates'];

    function MunicipalityPagesController($scope, $rootScope, $state, toastrService, FileManager, ngDialog, ValidAdminUser, Templates) {

        if (ValidAdminUser.data !== true || ValidAdminUser.status !== 200) {
            $state.go('Home');
            toastrService.info('', 'הרשאה לא תקינה!', { onHidden: function () { $state.go('Home'); } });
            return void [0];
        }
        $scope.WSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'MUNICIPALITYMENUROLE');

        $scope.HtmlTemplates = [];
        $scope.HtmlTemplateModel = {
            Editing: false,
            ID: null,
            municipalityId: $scope.WSPostModel.currentMunicipalityId,
            createdDate: new Date(),
            updateDate: new Date(),
            updateUserId: null,
            template: null,
            subject: null,
            name: null,
            fromEmail: null,
            municipality: null,
            user: null,
            type: 2,
            active: false,
        }
        ///pager
        $scope.$Pager = {
            PageSize: 15,
            PageNumber: 1,
            Counter: 0,
            MaxPages: 0,
            pagerList: [],
            sizes: [15, 30, 45]
        }
        $scope.changePagerSize = function () {
            $scope.setUpController($scope.HtmlTemplates);
        }

        $scope.getItem = function (list, num) {
            var items = list.filter(function (s) {
                return s.ID === num;
            });

            return (items instanceof Array) ? items[0] : items;
        }

        $scope.DeleteTemplate = function (tmplt) {

            ngDialog.openConfirm({
                template: '\
                <p><b>האם למחוק את</b> "' + tmplt.subject + '"?<\p>\
                <p><b>שים לב</b>, לא ניתן לשחזרת דף שנמחק!</p>\
                <div class="ngdialog-buttons">\
                    <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">בטל</button>\
                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(' + tmplt.ID + ')">מחק</button>\
                </div>',
                plain: true,

                scope: $scope,
            }).then(function (success) {
                
                FileManager.DeleteTemplate({ wsModel: $scope.WSPostModel, template: tmplt })
                    .then(function successCallback(res) {
                        if (res.data) {
                            $scope.HtmlTemplates.splice($scope.HtmlTemplates.indexOf(tmplt), 1);
                            $scope.setUpController($scope.HtmlTemplates);
                            if (tmplt.ID === $scope.HtmlTemplateModel.ID) {
                                $scope.createNewTemplate(false);
                            }

                            toastrService.success('התבנית נמחקה!', '');
                        } else {
                            toastrService.error('', 'תקלה!');
                        }                        
                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });

            }, function (error) {
                ///do nothing, user canceld
            });

        }
        $scope.SaveTemplate = function () {
            $scope.TemplateFrom.$setSubmitted();
            if ($scope.TemplateFrom.$invalid || !$scope.TemplateFrom.$valid)
                return void [0];

            $scope.HtmlTemplateModel.updateDate = new Date();

            FileManager.UpdateAddTemplate({ wsModel: $scope.WSPostModel, template: $scope.HtmlTemplateModel })
                    .then(function successCallback(res) {
                        var item = $scope.getItem($scope.HtmlTemplates, res.data.ID);
                         if (item === undefined) {
                             $scope.HtmlTemplates.push(res.data);
                         } else {
                             $scope.HtmlTemplates.splice($scope.HtmlTemplates.indexOf(item), 1);
                             $scope.HtmlTemplates.push(res.data);
                         }
                         $scope.setUpController($scope.HtmlTemplates);
                         $scope.EditTemplate(res.data);
                         toastrService.success('התבנית נשמרה!', '');

                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
        }
        $scope.EditTemplate = function (template) {
            $scope.HtmlTemplateModel = angular.copy(template);
            $scope.HtmlTemplateModel.Editing = true;
        }
        $scope.createNewTemplate = function (state) {
            $scope.HtmlTemplateModel = {
                Editing: state,
                ID: null,
                municipalityId: $scope.WSPostModel.currentMunicipalityId,
                createdDate: new Date(),
                updateDate: new Date(),
                updateUserId: null,
                template: null,
                subject: null,
                name: null,
                fromEmail: null,
                type: 2,
                active: false,
            }
        }
        $scope.activetTemplate = function (template) {
            template.active = !template.active;

            FileManager.UpdateAddTemplate({ wsModel: $scope.WSPostModel, template: template })
                    .then(function successCallback(res) {
                        if ($scope.HtmlTemplateModel.ID === template.ID)
                            $scope.EditTemplate(res.data);

                        toastrService.success('התבנית נשמרה!', '');

                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
        }

        $scope.file_browser_callbackFunction = function (field_name, url, type, win) {
            try {
                $('#mce-modal-block').attr('style', '');
                var style = $('.mce-container.mce-panel.mce-floatpanel.mce-window.mce-in').attr('style');
                style = style.split(';');
                style.filter(function (s) {
                    if (s.indexOf('z-index') >= 0) {
                        style[style.indexOf(s)] = 'z-index: 123';
                    }
                });
                $('.mce-container.mce-panel.mce-floatpanel.mce-window.mce-in').attr('style', style.join(';'));
            } catch (e) {
                //console.log(e);
            }

            ngDialog.open({
                template: '../../Scripts/views/Email-Templates/file-manager.html',
                closeByNavigation: true,
                closeByDocument: true,
                closeByEscape: true,
                scope: $scope,
                backdrop: 'static',
                showClose: true,
                appendClassName: 'file-manager-custom',
                name: 'fileManagerPopUp',
                resolve: {
                    fileStructure: ['FileManager', function (FileManager) {
                        return FileManager.GetFileStructure($scope.WSPostModel);
                    }],
                    options: [function () {
                        return { field_name: field_name, url: url, type: type, win: win };
                    }],
                },
                controller: 'FileManagerController',
            });
        }
        $scope.tinymceOptions = {
            selector: "textarea", theme: "modern", height: 400,
            plugins: [
                 "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                 "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                 "table contextmenu directionality emoticons paste textcolor code",
                 "directionality"
            ],
            toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | link unlink anchor | image media | forecolor backcolor  | ltr rtl",

            directionality: "rtl",

            file_browser_callback: $scope.file_browser_callbackFunction,
        };

        /* *** set up controller *** */
        $scope.setUpController = function (templates) {
            $scope.HtmlTemplates = templates;
            $scope.$Pager.PageNumber = 1;

            $scope.$Pager.Counter = templates.length;
            $scope.$Pager.MaxPages = Math.ceil(templates.length / $scope.$Pager.PageSize);
            var num = 1;

            $scope.$Pager.pagerList = [];
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                $scope.$Pager.pagerList.push(1 + i);
            }
        }
        $scope.setUpController(Templates.data);
    }
})();
