﻿(function () {
    angular.module('WorkerPortfolioApp').controller('AdminController', AdminController);

    AdminController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', 'toastrService', 'Admin', 'ValidAdminUser', '$user', '$passwordSettings', '$groups'];
    
    function AdminController($scope, $rootScope, $state, $stateParams, toastrService, Admin, ValidAdminUser, $user, $passwordSettings, $groups) {
        
        if (ValidAdminUser.data !== true || ValidAdminUser.status !== 200) {
            $state.go('Home');
            toastrService.info('', 'הרשאה לא תקינה!', { onHidden: function () { $state.go('Home'); } });
            return void [0];
        }

        var mailType = {
            1: 'איפוס סיסמה',
            2: 'הרשמה לאתר',
            3: 'ברוך הבא למערכת',
            4: 'כללי'
        }
        var pad = function (val) {
            return val > 9 ? String(val) : ('0' + String(val));
        }
        var parseDateToString = function (date) {
            if (date === undefined || date === null)
                return null;
            date = new Date(date);
            return pad(date.getDate()) + '/' + pad(date.getMonth() + 1) + '/' + date.getFullYear() + ' ' + pad(date.getHours()) + ':' + pad(date.getMinutes()) + ':' + pad(date.getSeconds());
        }
        var last7 = function () {
            var now = new Date();
            return new Date(now.getFullYear(), now.getMonth(), now.getDate() - 7);
        }
        var parseSmsMessages = function (data) {
            $.map(data, function (msg) {
                msg.sendDate = parseDateToString(msg.sendDate);
                msg.response = JSON.parse(msg.response);
                msg.anser = returnAnser(msg.response.Msg);
            });
            return data;
        }
        var returnAnser = function (Msg) {
            var anser = '';
            switch(Msg.RcType){
                case 1:
                    switch(Msg.RcNumber){
                        case 0:
                            anser = 'נשלח תקין';
                            break;
                        case 200:
                            anser = 'מספר לא תקין';
                            break;

                        default:
                            anser = 'שגיאת מערכת שליחה';
                            break;
                    }
                    break;
                case 9:
                    switch (Msg.RcNumber) {
                        case -99:
                            anser = 'תשובה לא תקינה';
                            break;
                        case 0:
                            anser = 'מספר לא תקין';
                            break;
                        case 1:
                        case 380000:
                            anser = 'שגיאת מערכת שליחה';
                            break;
                        case 500:
                            anser = 'משלוח נכשל';
                            break;

                        default:
                            anser = 'שגיאת מערכת שליחה';
                            break;
                    }
                    break;
                default:
                    anser = 'שגיאת מערכת שליחה';
                    break;
            }
            
            return anser;
        }
        var parseMailMessages = function (data) {
            $.map(data, function (msg) {
                console.log(msg);
                msg.MailType = mailType[msg.MailType];
                msg.createdDate = parseDateToString(msg.createdDate);
            });

            return data;
        }

        $scope.$user = $user && $user.data ? $user.data : {};
        $scope.$passwordSettings = $passwordSettings.data;
        
        $scope.getWSPostModel = $rootScope.getWSPostModel(null, null, null, null, 'ADMIN');
        $scope.UsersList = [];
        $scope.SmsMessages = new Array();
        $scope.EmailMessages = new Array();
        $scope.UserSearch = {
            idNumber: $scope.getWSPostModel.idNumber,
            confirmationToken: $scope.getWSPostModel.confirmationToken,
            page: 1,
            hidden_value: false,
            $hidden_value:'',
            searchIdNumber: $stateParams.IdNUmber,
            cell: null,
            firstName: null,
            lastName: null,
            password: null,
            newPassword: null,
            reEnterPassword: null,
            municipalities: null,
            userToken: null,
            searchMunicipalityName: null,
            searchMunicipalitiesId: null,
            requiredRole: WSPostModel.requiredRole
        };
        
        if (typeof $scope.$user !== 'undefined' && $scope.$user !== null && $scope.$user.ID > 0) {
            $scope.$user.municipalities = $scope.$user.municipalities.map(function (elem) {
                                                return elem.name;
            }).join(", ");
        }

        ///pager
        $scope.$Pager = {
            PageSize: 15,
            PageNumber: 1,
            Counter:0,
            MaxPages: 0,
            pagerList: []
        }
        ///validate password
        $scope.validatePasswords = function ($form) {
            $scope.UserSearch.newPassword = $scope.UserSearch.newPassword.replace(/[\/]/gi, '');
            $form.newPassword.$setValidity('custom', true);
            $form.reEnterPassword.$setValidity('custom', true);
            
            if ($rootScope.$$credentials.isAdmin < 100) {
                ///return if not valid on other checks
                if ($form.reEnterPassword.$error.required || $form.reEnterPassword.$error.minlength || $form.reEnterPassword.$error.maxlength
                    || $form.newPassword.$error.required || $form.newPassword.$error.minlength || $form.newPassword.$error.maxlength)
                    return void [0];
                ///check minRequiredNonAlphanumericCharacters
                if ($scope.UserSearch.newPassword.replace(/[^\W|_]/gi, '').length < $scope.$passwordSettings.minNonAlphanumeric) {
                    $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] תווים מיוחדים!'.replace('[0]', $scope.$passwordSettings.minNonAlphanumeric);
                    $form.newPassword.$setValidity('custom', false);
                    return void [0];
                }
                ///check minRequiredNumericCharacters
                if ($scope.UserSearch.newPassword.replace(/[\D|_]/gi, '').length < $scope.$passwordSettings.minNumeric) {
                    $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] מספרים!'.replace('[0]', $scope.$passwordSettings.minNumeric);
                    $form.newPassword.$setValidity('custom', false);
                    return void [0];
                }
            }

            ///passwords dont match
            if ($scope.UserSearch.newPassword !== $scope.UserSearch.reEnterPassword) {
                $form.reEnterPassword.$setValidity('custom', false);
                return void [0];
            }
        }
        ///check if search form is empty
        $scope.emptyForm = function () {
            if ($scope.UserSearchForm.searchIdNumber.$pristine && $scope.UserSearchForm.cell.$pristine &&
                    $scope.UserSearchForm.firstName.$pristine && $scope.UserSearchForm.lastName.$pristine) {
                $scope.UserSearch.hidden_value = true;
                $scope.UserSearchForm.hidden_value.$setValidity('required', false);
                return true;
            }

            if ($scope.UserSearchForm.searchIdNumber.$modelValue == null && $scope.UserSearchForm.cell.$modelValue == null &&
                    $scope.UserSearchForm.firstName.$modelValue == null && $scope.UserSearchForm.lastName.$modelValue == null &&
                    $scope.UserSearchForm.searchIdNumber.$modelValue.isempty() && $scope.UserSearchForm.cell.$modelValue.isempty() &&
                    $scope.UserSearchForm.firstName.$modelValue.isempty() && $scope.UserSearchForm.lastName.$modelValue.isempty()) {
                $scope.UserSearch.hidden_value = true;
                $scope.UserSearchForm.hidden_value.$setValidity('required', false);
                return true;
            }
            
            return false;
        }

        $scope.GetUserMessages = function () {
            if ($scope.$user.cell !== null && $scope.$user.cell.length > 5) {
                $scope.CheckSms();
            }
            if ($scope.$user.email !== null && $scope.$user.email.length > 5) {
                $scope.CheckMails();
            }
        }
        $scope.CheckSms = function () {
            $scope.UserSearch.page = $scope.$user.ID;
            $scope.UserSearch.searchDate = last7();

            Admin.CheckUserSmsMessages($scope.UserSearch)
                .then(function successCallback(res) {
                    $scope.SmsMessages = parseSmsMessages(res.data);
                    if ($scope.SmsMessages === null || $scope.SmsMessages.length === 0) {
                        toastrService.info('ב-7 הימים האחרונים.', 'לא נשלחו הודעות SMS,', { timeOut: 5500 });
                    }
                    
                }, function errorCallback(res) {
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.CheckMails = function () {
            $scope.UserSearch.searchIdNumber = $scope.$user.idNumber;
            $scope.UserSearch.searchDate = last7();

            Admin.CheckUserMailMessages($scope.UserSearch)
                .then(function successCallback(res) {
                    $scope.EmailMessages = parseMailMessages(res.data);
                    if ($scope.EmailMessages === null || $scope.EmailMessages.length === 0) {
                        toastrService.info('ב-7 הימים האחרונים,', 'לא נשלחו הודעות דוא``ל.', { timeOut: 5500 });
                    }

                }, function errorCallback(res) {
                    console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.Search = function () {
            $scope.UserSearch.hidden_value = false;
            $scope.UserSearchForm.hidden_value.$setValidity('required', true);
            if ($scope.emptyForm() || $scope.UserSearchForm.$invalid)
                return void [0];

            $scope.$Pager.PageNumber = 1;
            
            Admin.SearchUsers($scope.UserSearch)
                .then(function successCallback(res) {
                    
                    $scope.$Pager.Counter = res.data.length;
                    $scope.$Pager.MaxPages = Math.ceil(res.data.length / $scope.$Pager.PageSize);
                    var num = 1;

                    $scope.$Pager.pagerList = [];
                    for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                        $scope.$Pager.pagerList.push(1 + i);
                    }

                    $scope.UsersList = res.data;

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        
        $scope.SendUserResetToken = function (user, type) {///1-mail, 0-sms
            $scope.UserSearch.page = type;
            $scope.UserSearch.searchIdNumber = user.idNumber;
           
            Admin.SendUserReset($scope.UserSearch)
                .then(function successCallback(res) {
                    if (res.data)
                        type == 0 ?
                            toastrService.success('הקוד נשלח בהודעת טקסט למספר הנייד של המנוי.', 'נשלח קוד אימות!', { allowHtml: true, timeOut: 5000, onHidden: function () { $state.go('Admin.ResetPassword', { IdNUmber: user.idNumber }); } })
                            : toastrService.success('הקוד נשלח בהודעת אימייל לכתובת המנוי.', 'נשלח קוד אימות!', { allowHtml: true, timeOut: 7000 });
                    else
                        toastrService.error('אנא נסה שנית לאחר טעינת הדף מחדש.', 'שגיאת תהליך!', { allowHtml: true, timeOut: 7000, onHidden: function () { $state.reload(); } });
                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!', { onHidden: function () { $state.reload(); } });
                });
        }

        $scope.ResetPayrollDisplay = function (user) {
            $scope.UserSearch.userToken = user.ID;
            Admin.ResetPayrollDisplay($scope.UserSearch)
                .then(function successCallback(res) {

                    if (res.data && res.status === 200) {
                        toastrService.success('תצוגת התלושים אופסה!', '');
                    } else {
                        toastrService.error('אנא נסה שנית.', 'תקלה!');
                    }

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });

        }

        $scope.ResetPassword = function () {
            $scope.validatePasswords($scope.UserSearchForm);
            if ($scope.UserSearchForm.$invalid)
                return void [0];

            $scope.UserSearch.password = $scope.UserSearch.newPassword;
            $scope.UserSearch.page = $stateParams.UserID;
            $scope.UserSearch.searchIdNumber = $stateParams.IdNUmber;

            Admin.ResetPassword($scope.UserSearch)
                .then(function successCallback(res) {
                    if (res.data.success && res.data.exceptionId === 0)
                        toastrService.success('', 'הסיסמה אופסה בהצלחה', { allowHtml: true, timeOut: 5000 });
                    else {
                        switch (res.data.exceptionId) {
                            case 7: //"NullReferenceException", 7
                            case 61:
                                toastrService.error('אנא וודע כי קוד האימות<br/> תואם ל SMS שנשלח ללקוח.', 'שגיאת זיהוי!', { allowHtml: true, timeOut: 5000 });
                                break;
                            case 8: //"verificationTokenExpirationException", 8
                                toastrService.error('פג תוקף אסימון האימות,<br/>אנא הפק אסימון אימות מחדש.', 'שגיאת אימות!', { allowHtml: true, timeOut: 5000, onHidden: function () { $state.go('Admin.SearchUsers'); } });
                                break;
                            case 55: //"UpdatePasswordException", 55
                                toastrService.error('אנא נסה שנית לאחר טעינת הדף מחדש.', 'שגיאת תהליך!', { allowHtml: true, timeOut: 5000, onHidden: function () { $state.reload(); } });
                                break;
                            default:
                                toastrService.error('אנא נסה שנית לאחר טעינת הדף מחדש.', 'שגיאת תהליך!', { allowHtml: true, timeOut: 5000, onHidden: function () { $state.reload(); } });
                                break;
                        }
                        
                    }
                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!', { onHidden: function () { $state.reload(); } });
                });
        }

        $scope.UpdateGroups = function () {
            $scope.UserSearch.searchIdNumber = $scope.$user.idNumber;
            $scope.UserSearch.searchMunicipalityName = $.map($.grep($scope.$groups, function (group) { return group.active }), function (group) { return group.ID }).join(",");
            $scope.UserSearch.searchMunicipalitiesId = $.map($.grep($scope.$groups, function (group) { return !group.active }), function (group) { return group.ID }).join(",");
            
            Admin.UpdateUserGroups($scope.UserSearch)
                .then(function successCallback(res) {
                    if(res.data && res.status === 200)
                        toastrService.success('הרשאות עודכנו בהצלחה', '', { allowHtml: true, timeOut: 5000 });
                    else
                        toastrService.error('','שגיאה!', { allowHtml: true, timeOut: 5000 });
                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!', { onHidden: function () { $state.reload(); } });
                });
        }

        $scope.parseUserGroups = function (groups, user) {
            groups = $.grep(groups, function (group) {
                group.name = group.name.toLowerCase();
                return $scope.getWSPostModel.isAdmin > group.immunity && group.immunity > 1
            });
            var _groups = $.grep(groups, function (group) {
                var _g = $.grep(user.groups, function (g) { return g.ID == group.ID });
                if (_g !== null && _g.length > 0) {
                    group.active = true;
                } else {
                    group.active = false;
                }
                return true;
            });
            return _groups;
        }
        $scope.$groups = $groups && $groups.data ? $scope.parseUserGroups($groups.data, $scope.$user) : {};

    }
})();
