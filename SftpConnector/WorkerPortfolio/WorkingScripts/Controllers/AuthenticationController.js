﻿(function () {
    angular.module('WorkerPortfolioApp').controller('AuthenticationController', AuthenticationController);

    AuthenticationController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$location', 'Credentials', 'Search', 'toastrService', '$controllerState', '$passwordSettings', '$window'];

    function AuthenticationController($scope, $rootScope, $state, $stateParams, $location, Credentials, Search, toastrService, $controllerState, $passwordSettings, $window) {

        ///scope params
        $scope.$passwordSettings = $passwordSettings.data;
        $scope.$controllerState = $controllerState;
        $scope.UserCredentials = {};
        ///scope functions
        $scope.parseLastLogIn = function (dateStr) {
            var dateArray = dateStr.split('T'), model = {};
            model.lastDate = dateArray[0].split('-').splice(1, 2).reverse().join('/');
            model.lastTime = dateArray[1].split('.')[0].split(':').splice(0, 2).join(':');

            return model;
        }
        $scope.parseFullUserData = function (user) {
            return {
                ID: user.ID,
                birthDate: $scope.parseIntoDate(user.birthDate),
                set_birthDate: $scope.parseIntoDate(user.birthDate, true),
                createdDate: user.createdDate,
                cell: user.cell,
                email: user.email,
                firstName: user.firstName,
                idNumber: user.idNumber,
                immigrationDate: $scope.parseIntoDate(user.immigrationDate),
                set_immigrationDate: $scope.parseIntoDate(user.immigrationDate, true),
                lastName: user.lastName,
                phone: user.phone
            }
        }
        $scope.parseIntoDate = function (d, isSet) {
            try {
                var date = d && typeof d === 'string' && d.length > 10 ? new Date(d) : null, returnDate = null;
                if (date !== null) {
                    returnDate = date.getDate() + '/' + (date.getMonth() + 1 < 10 ? ('0' + (date.getMonth() + 1)) : (date.getMonth() + 1)) + '/' + date.getFullYear();
                    if (isSet) returnDate = (date.getMonth() + 1 < 10 ? ('0' + (date.getMonth() + 1)) : (date.getMonth() + 1)) + '/' + date.getDate() + '/' + date.getFullYear();
                }

                return returnDate;
            } catch (e) {
                //console.log('e', e);
                return null;
            }
        }
        $scope.ReSetDate = function (d) {
            if (!d || typeof d !== 'string' || d.length < 8)
                return null;
            var sd = d.split('/'), returnDate = new Date();
            returnDate.setMonth((parseInt(sd[1]) - 1)); returnDate.setFullYear(sd[2]); returnDate.setDate(sd[0]);
            return returnDate;
        }
        $scope.setRequired = function () {
            $scope.UserCredentials.initialPasswordRequired = true;
            $scope.UserCredentials.emailRequired = true;
            $scope.UserCredentials.cellRequired = true;
            if ($scope.UserCredentials.initialPassword != null && !$scope.UserCredentials.initialPassword.isempty() && $scope.RegisterForm.initialPassword.$valid) {
                $scope.UserCredentials.emailRequired = false;
                $scope.UserCredentials.cellRequired = false;
            }
            else if ($scope.UserCredentials.cell != null && !$scope.UserCredentials.cell.isempty() && $scope.RegisterForm.cell.$valid) {
                $scope.UserCredentials.emailRequired = false;
                $scope.UserCredentials.initialPasswordRequired = false;
            }
            else if ($scope.UserCredentials.email != null && !$scope.UserCredentials.email.isempty() && $scope.RegisterForm.email.$valid) {
                $scope.UserCredentials.initialPasswordRequired = false;
                $scope.UserCredentials.cellRequired = false;
            }
        }

        /* ****** AUTOCOMPLETE ****** */
        $scope.selected = { item: null };
        $scope.selectedMunicipality = [];
        $scope.municipalities = [];
        $scope.searchMunicipality = function (m) {
            Search.MunicipalitiesSearch({ query: m, results: 10 })
                .then(function (res) {
                    $scope.municipalities = res.data;
                });
        }
        /* ****** AUTOCOMPLETE ****** */

        switch ($controllerState) {
            case 'PasswordReset':
                $scope.publish('passwordResetLogout');
                $scope.UserCredentials = {
                    newPassword: null,
                    reEnterPassword: null,
                    token: $stateParams.Token,
                    passwordResetType: 1
                };
                break;
            case 'UpdateDetails':
                $scope.UserCredentials = $rootScope.lsGet('$$credentials');

                Credentials.GetFullUserdetails({ Token: $scope.UserCredentials.confirmationToken, newPassword: null })
                    .then(function successCallback(res) {
                        $scope.UserCredentials = $scope.parseFullUserData(res.data);
                    }, function errorCallback(res) {
                        //console.log('full-error', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
                break;
            case 'SMSReset':
                $scope.publish('passwordResetLogout');
                $scope.UserCredentials = {
                    newPassword: null,
                    reEnterPassword: null,
                    token: null,
                    passwordResetType: 2,
                    idNumber: $stateParams.idNumber || null
                };
                break;
            case 'AuthenticatedUserLogin':

                $rootScope.$$credentials_unbind;
                $rootScope.lsRemove('$$credentials');

                Credentials.GetCurrentUser({ authenticationToken: $stateParams.AuthenticationSuccessToken })
                    .then(function successCallback(res) {
                        $scope.publish('RefreshLoginState', res.data);
                    }, function errorCallback(res) {
                        //console.log('error', res);
                        toastrService.error('', 'תקלת מערכת!');
                        $state.go('Error');
                    });

                break;
            case 'UserRegister':
                ///remove the option to register
                $state.go('Home');

                $scope.UserCredentials = {
                    municipalities: null,
                    idNumber: null,
                    isValidToRegister: false,
                    returnObjID: -1,
                    showSmsField: false,
                    token: null,
                    initialPassword: "",
                    initialPasswordRequired: true,
                    email: "",
                    emailRequired: true,
                    cell: "",
                    cellRequired: true,
                };
                break;
            case 'InitialActivation':
                $scope.UserCredentials = {
                    cell: false,
                    email: false,
                    idNumber: $stateParams.idNumber || null,
                    showError: false,
                    sendOption: null,
                    $sendVal: null,
                };
                break;
            case 'LoginToProcess':
                
                $rootScope.$$credentials_unbind;
                $rootScope.lsRemove('$$credentials');
                Credentials.GetCurrentUserForProcesses({ authenticationToken: $stateParams.AuthenticationSuccessToken, rashut: $stateParams.rashut, mncplity: $stateParams.mncplity })
                    .then(function successCallback(res) {
                        $scope.publish('RefreshLoginState', res.data);
                        $state.go('UserMenu.MyProcesses');
                    }, function errorCallback(res) {
                        //console.log('error', res);
                        toastrService.error('', 'תקלת מערכת!');
                        $state.go('Error');
                    });
                break;
            default:
                $state.go('Home');
                break;
        }
        $scope.setSendVal = function (val) {
            if (val == 'undefined' || val == null) {
                $scope.ActivationForm.credentials_sendVal.$setValidity('required', false);
                return void [0];
            }
            $scope.UserCredentials.sendOption = val;
            $scope.ActivationForm.credentials_sendVal.$setValidity('required', true);
        }
        ///InitialActivation
        $scope.activate = function () {
            $scope.setSendVal($scope.UserCredentials.sendOption);
            if ($scope.ActivationForm.$invalid)
                return void [0];
            var data = {
                idNumber: $scope.UserCredentials.idNumber,
                password: $scope.UserCredentials.sendOption,
                key: $scope.UserCredentials.sendOption === 1,
                iv: $scope.UserCredentials.sendOption === 2
            }
            Credentials.InitialActivationCheck(data)
                .then(function successCallback(res) {
                    switch (res.data.exceptionId) {
                        case 200:///IdNumberNullException
                            toastrService.error('יש לוודא מול נציג הארגון שפרטי המייל / טלפון נייד מוגדרים במערכת.', 'לא נמצא משתמש לפי הפרטים שסיפקת!', { allowHtml: true, timeOut: 8000 });
                            $scope.UserCredentials.showError = 1;
                            break;
                        case 201:///InitialCheckException
                            toastrService.error('לא ניתן לבצע אימות ראשוני בשנית!', 'אימות ראשוני כבר בוצע!', { allowHtml: true, timeOut: 7000 });
                            $scope.UserCredentials.showError = 2;
                            break;
                        case 111:///NullEmailException
                            toastrService.error('אנא פנה לכ"א על מנת לעדכן פרטים.<br/>ניתן לבצע אימות נתונים ע"י SMS.', 'למשתמש לא מוגדרת כתובת מייל במערכת!', { allowHtml: true, timeOut: 8000 });
                            break;
                        case 112:
                            toastrService.error('אנא פנה לכ"א על מנת לעדכן פרטים.<br/>ניתן לבצע אימות נתונים ע"י שליחת מייל.', 'למשתמש לא מוגדר מספר טלפון נייד במערכת!', { allowHtml: true, timeOut: 8000 });
                            break;
                        case 0:
                            switch ($scope.UserCredentials.sendOption) {
                                case 1:///email
                                    toastrService.success('מייל עדכון סיסמה נשלח לכתובת המייל הרשומה, אנא עקוב אחר ההוראות במייל לאיפוס סימתך.', 'תודה, ' + res.data.userCredentials.fullName, { allowHtml: true, timeOut: 8000 });
                                    $state.go('Home');
                                    break;
                                case 2:///sms
                                    toastrService.success('מזהה איפוס סיסמה נשלח לטלפון הנייד הרשום במערכת, אנא הכנס את הקוד לשדה המיועד.', 'תודה, ' + res.data.userCredentials.fullName, { allowHtml: true, timeOut: 8000 });
                                    $state.go('Authentication.ResetPasswordFromSms', { idNumber: $scope.UserCredentials.idNumber });
                                    break;
                            }
                            break;
                        default:
                            break;

                    }

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('אנא נסא שנית מאוחר יותר,<br/>במקרה שהתקלה חוזרת ניתן לפנות למנהל האתר בצור קשר באתר.', 'התרחשה שגיאת שרת פנימית!', { allowHtml: true, timeOut: 8000 });
                    $scope.UserCredentials.showError = 3;
                });
        }
        ///user register
        $scope.register = function () {
            if ($scope.UserCredentials.isValidToRegister) $scope.validatePasswords($scope.RegisterForm);

            if ($scope.RegisterForm.$invalid)
                return void [0];

            if (!$scope.UserCredentials.isValidToRegister) {
                Credentials.CheckValidUser($scope.setUserData())
                    .then(function successCallback(res) {
                        var data = res.data;
                        if (!data.success) {
                            toastrService.error('לא נמצא משתמש לפי הפרטים שסיפקת', 'פרטים שגויים');
                            return void [0];
                        }
                        $scope.UserCredentials.returnObjID = data.exceptionId;
                        switch (data.exceptionId) {
                            case 0:///validated by initialPassword
                                $scope.UserCredentials.isValidToRegister = true;
                                break;
                            case 1:
                                $scope.UserCredentials.isValidToRegister = true;
                                break;
                            case 2:
                                $scope.UserCredentials.isValidToRegister = true;
                                break;
                            default:
                                toastrService.error('', 'תקלת מערכת!');
                                return void [0];
                                break;
                        }
                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
            } else {
                switch ($scope.UserCredentials.returnObjID) {
                    case 0:
                        $scope.registerFromInitialPassword();
                        break;
                    case 1:
                        $scope.RegisterFromSms();
                        break;
                    case 2:
                        $scope.RegisterFromEmail();
                        break;
                }
            }

        }
        $scope.verifyToken = function () {
            if ($scope.verifyTokenForm.$invalid)
                return void [0];
            Credentials.VerifySmsToken($scope.setSmsUserData())
                .then(function successCallback(res) {
                    var data = res.data;
                    switch (data.exceptionId) {
                        case 0:
                            $scope.publish('RefreshLoginState', res.data.userCredentials);
                            break;

                        case 7://NullReferenceException
                            toastrService.error('הקוד שסיפקת אינו קיים במערכת!', 'תקלה!');
                            break;
                        case 8://verificationTokenExpirationException
                            toastrService.error('תוקף קוד האימות פקע, אנא צור חדש.', 'תקלה!');
                            break;
                        default:
                            toastrService.error('', 'תקלת מערכת!');
                            return void [0];
                            break;
                    }

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        ///reset password scope functions
        $scope.resetPassword = function () {
            $scope.validatePasswords($scope.PasswordResetForm);
            if ($scope.PasswordResetForm.$invalid)
                return void [0];
            ///reset password
            Credentials.ChangePasswordFromToken({ Token: $scope.UserCredentials.token, newPassword: $scope.UserCredentials.newPassword, passwordResetType: $scope.UserCredentials.passwordResetType, idNumber: $scope.UserCredentials.idNumber })
                .then(function successCallback(res) {

                    if (res.data.success) {
                        ///remove old login data
                        $rootScope.$$credentials_unbind;
                        $rootScope.lsRemove('$$credentials');
                        toastrService.success('סיסמתך עודכנה בהצלחה!', 'תודה.');
                        $scope.publish('passwordResetLonin', res.data.userCredentials);
                        $state.go('Home');
                    } else {
                        switch (res.data.exceptionId) {
                            case 61:///GetUserFromToken
                                toastrService.error('תוקף אסימון אימות הסיסמה פג, אנא נסה ליצור חדש.', 'שגיאת אימות נתונים!');
                                break;

                            default:
                                //console.log('error', res);
                                toastrService.error('', 'תקלת מערכת!');
                                break;
                        }
                    }
                }, function errorCallback(res) {
                    //console.log('error', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.validatePasswords = function ($form) {
            $scope.UserCredentials.newPassword = $scope.UserCredentials.newPassword.replace(/[\/]/gi, '');
            $form.newPassword.$setValidity('custom', true);
            $form.reEnterPassword.$setValidity('custom', true);
            ///return if not valid on other checks
            if ($form.reEnterPassword.$error.required || $form.reEnterPassword.$error.minlength || $form.reEnterPassword.$error.maxlength
                || $form.newPassword.$error.required || $form.newPassword.$error.minlength || $form.newPassword.$error.maxlength)
                return void [0];
            ///check minRequiredNonAlphanumericCharacters
            if ($scope.UserCredentials.newPassword.replace(/[^\W|_]/gi, '').length < $scope.$passwordSettings.minNonAlphanumeric) {
                $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] תווים מיוחדים!'.replace('[0]', $scope.$passwordSettings.minNonAlphanumeric);
                $form.newPassword.$setValidity('custom', false);
                return void [0];
            }
            ///check minRequiredNumericCharacters
            if ($scope.UserCredentials.newPassword.replace(/[\D|_]/gi, '').length < $scope.$passwordSettings.minNumeric) {
                $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] מספרים!'.replace('[0]', $scope.$passwordSettings.minNumeric);
                $form.newPassword.$setValidity('custom', false);
                return void [0];
            }
            ///passwords dont match
            if ($scope.UserCredentials.newPassword !== $scope.UserCredentials.reEnterPassword) {
                $form.reEnterPassword.$setValidity('custom', false);
                return void [0];
            }
        }
        ///update user details scope functions
        $scope.updateUser = function () {
            if ($scope.UpdateDetailsForm.$invalid)
                return void [0];

            Credentials.UpdateUserDetails($scope.setSendData())
                .then(function successCallback(res) {
                    toastrService.success('נתוניך התעדכנו בהצלחה!', 'תודה.');
                    $scope.publish('UpdateShowLoginSatet', res.data);
                }, function errorCallback(res) {
                    //console.log('full-error', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.setSendData = function () {
            var UserModel = angular.copy($scope.UserCredentials);
            UserModel.birthDate = $scope.ReSetDate(UserModel.birthDate);
            UserModel.immigrationDate = $scope.ReSetDate(UserModel.immigrationDate);
            return UserModel;
        }
        $scope.setUserData = function () {
            return {
                idNumber: $scope.UserCredentials.idNumber,
                password: $scope.UserCredentials.newPassword,
                initialPassword: $scope.UserCredentials.initialPassword,
                municipalities: $scope.UserCredentials.municipalities,
                email: $scope.UserCredentials.email,
                cell: $scope.UserCredentials.cell,
            }
        }
        $scope.setSmsUserData = function () {
            return {
                idNumber: $scope.UserCredentials.idNumber,
                initialPassword: $scope.UserCredentials.token,
            }
        }
        $scope.registerFromInitialPassword = function () {
            Credentials.RegisterFromInitialPassword($scope.setUserData())
                    .then(function successCallback(res) {
                        var data = res.data;
                        switch (data.exceptionId) {
                            case 0:
                                $scope.publish('RefreshLoginState', res.data.userCredentials);
                                break;

                            case 2://DuplicateIdNumber
                                toastrService.error('תעודת הזהות כבר קיימת במערכת!', 'תקלה!');
                                break;
                            case 3://DuplicateEmail
                                toastrService.error('האימייל כבר קיים במערכת!', 'תקלה!');
                                break;
                            case 4://InvalidPassword
                                toastrService.error('הסיסמה שסיפקת אינה תקינה!', 'תקלה!');
                                break;
                            case 5://InvalidEmail
                                toastrService.error('האימייל שסיפקת אינו תקין!', 'תקלה!');
                                break;
                            case 6://InvalidIDNumber
                                toastrService.error('תעודת הזהות שסיפקת אינה תקינה!', 'תקלה!');
                                break;

                            case 103:///"UserExistsException"
                                toastrService.error('המשתמש כבר קיים במערכת!', 'תקלה!');
                                break;
                            case 104:///"NullSystemUserException"
                                toastrService.error('לא נמצאו נתונים עבור הפרטים שהועברו!', 'תקלה!');
                                break;
                            case 105:///"InvalidIDNumber"
                                toastrService.error('תעודת הזהות שסיפקת אינה תקינה!', 'תקלה!');
                                break;
                            case 106:///"InvalidReturnDataException"
                                toastrService.error('לא נמצאו נתונים עבור הפרטים שהועברו!', 'תקלה!');
                                break;
                            case 108:///"initialPasswordException"
                                toastrService.error('סיסמת הרישום שסיפקת אינה נכונה!', 'תקלה!');
                                break;
                            default:
                                toastrService.error('', 'תקלת מערכת!');
                                return void [0];
                                break;
                        }

                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
        }
        $scope.RegisterFromSms = function () {
            Credentials.RegisterFromSms($scope.setUserData())
                    .then(function successCallback(res) {
                        var data = res.data;
                        switch (data.exceptionId) {
                            case 0:
                                $scope.UserCredentials.showSmsField = true;
                                break;

                            case 2://DuplicateIdNumber
                                toastrService.error('תעודת הזהות כבר קיימת במערכת!', 'תקלה!');
                                break;
                            case 3://DuplicateEmail
                                toastrService.error('האימייל כבר קיים במערכת!', 'תקלה!');
                                break;
                            case 4://InvalidPassword
                                toastrService.error('הסיסמה שסיפקת אינה תקינה!', 'תקלה!');
                                break;
                            case 5://InvalidEmail
                                toastrService.error('האימייל שסיפקת אינו תקין!', 'תקלה!');
                                break;
                            case 6://InvalidIDNumber
                                toastrService.error('תעודת הזהות שסיפקת אינה תקינה!', 'תקלה!');
                                break;

                            case 103:///"UserExistsException"
                                toastrService.error('המשתמש כבר קיים במערכת!', 'תקלה!');
                                break;
                            case 104:///"NullSystemUserException"
                                toastrService.error('לא נמצאו נתונים עבור הפרטים שהועברו!', 'תקלה!');
                                break;
                            case 105:///"InvalidIDNumber"
                                toastrService.error('תעודת הזהות שסיפקת אינה תקינה!', 'תקלה!');
                                break;
                            case 106:///"InvalidReturnDataException"
                                toastrService.error('לא נמצאו נתונים עבור הפרטים שהועברו!', 'תקלה!');
                                break;
                            case 109:///"EmailException"
                                toastrService.error('מספר הנייד שסיפקת אינו תואם!', 'תקלה!');
                                break;
                            default:
                                toastrService.error('', 'תקלת מערכת!');
                                return void [0];
                                break;
                        }

                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
        }
        $scope.RegisterFromEmail = function () {
            Credentials.RegisterFromEmail($scope.setUserData())
                    .then(function successCallback(res) {
                        var data = res.data;
                        switch (data.exceptionId) {
                            case 0:
                                toastrService.success('להשלמת תהליך ההרשמה אנא פנה למייל שנישלח לכתובת שסיפקת.', 'תודה על ההרשמה לאתר.');
                                break;

                            case 2://DuplicateIdNumber
                                toastrService.error('תעודת הזהות כבר קיימת במערכת!', 'תקלה!');
                                break;
                            case 3://DuplicateEmail
                                toastrService.error('האימייל כבר קיים במערכת!', 'תקלה!');
                                break;
                            case 4://InvalidPassword
                                toastrService.error('הסיסמה שסיפקת אינה תקינה!', 'תקלה!');
                                break;
                            case 5://InvalidEmail
                                toastrService.error('האימייל שסיפקת אינו תקין!', 'תקלה!');
                                break;
                            case 6://InvalidIDNumber
                                toastrService.error('תעודת הזהות שסיפקת אינה תקינה!', 'תקלה!');
                                break;

                            case 103:///"UserExistsException"
                                toastrService.error('המשתמש כבר קיים במערכת!', 'תקלה!');
                                break;
                            case 104:///"NullSystemUserException"
                                toastrService.error('לא נמצאו נתונים עבור הפרטים שהועברו!', 'תקלה!');
                                break;
                            case 105:///"InvalidIDNumber"
                                toastrService.error('תעודת הזהות שסיפקת אינה תקינה!', 'תקלה!');
                                break;
                            case 106:///"InvalidReturnDataException"
                                toastrService.error('לא נמצאו נתונים עבור הפרטים שהועברו!', 'תקלה!');
                                break;
                            case 110:///"EmailException"
                                toastrService.error('המייל שסיפקת אינו קיים במערכת!', 'תקלה!');
                                break;
                            default:
                                toastrService.error('', 'תקלת מערכת!');
                                return void [0];
                                break;
                        }

                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
        }
    }
})();