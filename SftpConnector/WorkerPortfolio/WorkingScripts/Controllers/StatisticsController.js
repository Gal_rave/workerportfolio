﻿(function () {
    angular.module('WorkerPortfolioApp').controller('StatisticsController', StatisticsController);

    StatisticsController.$inject = ['$scope', '$rootScope', '$window', '$timeout', 'toastrService', 'FileSaver', 'Admin', 'ValidAdminUser', 'municipalities', 'statistics', '$controllerState'];

    function StatisticsController($scope, $rootScope, $window, $timeout, toastrService, FileSaver, Admin, ValidAdminUser, municipalities, statistics, $controllerState) {

        if (ValidAdminUser.data !== true || ValidAdminUser.status !== 200) {
            $state.go('Home');
            toastrService.info('', 'הרשאה לא תקינה!', { onHidden: function () { $state.go('Home'); } });
            return void [0];
        }

        $scope.controllerState = $controllerState;

        $scope.WSPostModel = $rootScope.getWSPostModel(true); $rootScope.getWSPostModel(null, null, null, null, 'EMAILPAYCHECKROLE')
        $scope.WSPostModel.isMobileRequest = $scope.isMobile;

        var _requiredRole = 'SITEADMIN';
        switch ($scope.controllerState) {
            case 'EmailPaycheck':
                _requiredRole = 'EMAILPAYCHECKROLE';
                break;
            case '101Form':
            case 'LogInLog':
            case 'DailyUsage':
                _requiredRole = 'SITEADMIN';
                break;
        }

        $scope.fileNameFunction = function () {
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

            return String(monthNames[$scope.selectedMonth.getMonth()]).toUpperCase() + '_' + String($scope.selectedMonth.getFullYear()) + '_ARCHIVE_' + String($scope.WSPostModel.currentRashutId) + '.xlsx';
        }
        var parseDate = function (date, isFull) {
            if (isFull !== true)
                isFull = false;
            if (isFull) {
                ///day              ///month (add 1)            ///year
                return [pad(date.getDate()), pad((date.getMonth() + 1)), date.getFullYear()].join('/') + ' ' + [pad(date.getHours()), pad(date.getMinutes())].join(':');
            }
            return [pad(date.getDate()), pad((date.getMonth() + 1)), date.getFullYear()].join('/');
        }
        var pad = function (num) {
            if (num <= 9) {
                return '0' + num.toString();
            } else {
                return num.toString();
            }
        }
        var dateRangeStart = function () {
            var base = new Date(), s = new Date(base.getFullYear(), base.getMonth(), 1), e = new Date(base.getFullYear(), base.getMonth() + 1, 1);
            return [parseDate(s), parseDate(e), s, e];
        }

        $scope.isMobile = ($window.innerWidth < 801);

        $scope.pcueList = new Array();
        $scope.yearsList = new Array();
        $scope.pcueHolderList = new Array();
        $scope.Search = {
            Name: '',
            Email: '',
            RegistrationDate: '',
            UpdateDate: '',
            IsActive: 0,
            WasSent: 0,
            total: 0,
            IdNumber: '',
            Year: '0',
            MunicipalityIsActive: null,
            ActiveForm: null,
            T_Cause: 0,
            T_Reason: 0,
            T_Date: 0
        };

        $scope.MunicipalitiesList = $.grep(municipalities.data, function (municipality) {
            return municipality.active;
        });

        $scope.selectedMunicipality = undefined;
        $scope.$SM = {
            selectedList: new Array()
        }

        $scope.selectedMunicipalityCallback = function (Selected) {
            $scope.selectedMunicipality = Selected !== undefined ? (Selected.description !== undefined ? Selected.description.ID : Selected.originalObject.ID) : undefined;
            if (Selected !== undefined && Selected.description !== undefined) {
                $scope.pcueList = new Array();
                $scope.pcueHolderList = new Array();
                $scope.$Pager = {
                    PageSize: 25,
                    PageNumber: 1,
                    Counter: 0,
                    MaxPages: 0,
                    pagerList: []
                }
            }
        }

        $scope.selectedMonth = $scope.WSPostModel.selectedDate;
        $scope.selectedMonth.setDate($scope.selectedMonth.getMonth() + 1);

        $scope.InitialValue = $.grep($scope.MunicipalitiesList, function (item) {
            return item.ID == $scope.WSPostModel.currentMunicipalityId;
        })[0];
        $scope.disableInput = $scope.MunicipalitiesList.length < 2;

        $timeout(function () {

            $('#Month').MonthPicker({
                MaxMonth: 0, IsRTL: true, UseInputMask: true, ShowOn: 'both', buttonText: 'בחר חודש',
                OnAfterChooseMonth: function (selectedDate) {
                    selectedDate.setDate(15);
                    $scope.selectedMonth = selectedDate;
                    $scope.UserSearch.searchDate = selectedDate;
                }
            });

        }, 55);

        $scope.UserSearch = {
            idNumber: $scope.WSPostModel.idNumber,
            confirmationToken: $scope.WSPostModel.confirmationToken,
            page: 1,
            hidden_value: false,
            $hidden_value: '',
            searchIdNumber: null,
            cell: null,
            firstName: null,
            lastName: null,
            password: null,
            newPassword: null,
            reEnterPassword: null,
            municipalities: null,
            userToken: null,
            searchMunicipalityName: null,
            searchMunicipalitiesId: $scope.InitialValue.ID,
            searchDate: $scope.selectedMonth,
            endSearchDate: $scope.selectedMonth,
            requiredRole: _requiredRole,
        };

        if ($scope.controllerState === 'DailyUsage') {
            var r = dateRangeStart();
            $scope.UserSearch.searchDate = r[2];
            $scope.UserSearch.endSearchDate = r[3];

            $timeout(function () {

                $('input[name="daterange"]').daterangepicker({
                    "showCustomRangeLabel": false,
                    "timePicker": false,
                    "startDate": r[0],
                    "endDate": r[1],
                    "minDate": "01/01/2010",
                    "maxDate": "01/01/2050",
                    "opens": "center",
                    "autoApply": true,
                    "autoUpdateInput": true,

                    "locale": {
                        "format": "DD/MM/YYYY",
                        "separator": " - ",
                        "applyLabel": "החל",
                        "cancelLabel": "בטל",
                        "fromLabel": "מ-",
                        "toLabel": "עד-",
                        "customRangeLabel": "Custom",
                        "weekLabel": "ש",
                        "daysOfWeek": [
                            "א'",
                            "ב'",
                            "ג'",
                            "ד'",
                            "ה'",
                            "ו'",
                            "שבת"
                        ],
                        "monthNames": [
                            "ינואר",
                            "פברואר",
                            "מרץ",
                            "אפריל",
                            "מאי",
                            "יוני",
                            "יולי",
                            "אוגוסט",
                            "ספטמבר",
                            "אוקטובר",
                            "נובמבר",
                            "דצמבר"
                        ],
                        "firstDay": 0
                    },

                }, function (start, end, label) {
                    $scope.UserSearch.searchDate = start._d;
                    $scope.UserSearch.endSearchDate = end._d;
                });

            }, 55);

        }

        ///pager
        $scope.$Pager = {
            PageSize: 25,
            PageNumber: 1,
            Counter: 0,
            MaxPages: 0,
            pagerList: []
        }

        $scope.GetStatistics = function () {
            $scope.municipality_hidden_value = null;
            $scope.StatisticsForm.hidden_value.$setValidity('required', false);
            if ($scope.selectedMunicipality !== undefined) {
                $scope.municipality_hidden_value = 1;
                $scope.StatisticsForm.hidden_value.$setValidity('required', true);
            }

            if ($scope.StatisticsForm.$invalid)
                return void [0];

            $scope.$Pager.PageNumber = 1;
            $scope.Search = {
                Name: '',
                Email: '',
                RegistrationDate: '',
                UpdateDate: '',
                IsActive: 0,
                WasSent: 0,
                total: 0,
                IdNumber: '',
                Year: '0',
                MunicipalityIsActive: null,
                ActiveForm: null,
                T_Cause: 0,
                T_Reason: 0,
                T_Date: 0
            };

            $scope.UserSearch.searchMunicipalitiesId = $scope.selectedMunicipality;

            Admin.GetUEPStatistics($scope.UserSearch)
                .then(function successCallback(res) {
                    $scope.pcueList = res.data;

                    $scope.$Pager.Counter = res.data.length;
                    $scope.$Pager.MaxPages = Math.ceil(res.data.length / $scope.$Pager.PageSize);
                    var num = 1;

                    $scope.$Pager.pagerList = [];
                    for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                        $scope.$Pager.pagerList.push(1 + i);
                    }

                    $.map($scope.pcueList, function (item) {
                        item._createdDate = item.createdDate != null ? parseDate(new Date(item.createdDate), (item.updateDate != null && item.updateDate !== item.createdDate)) : null;
                        item.createdDate = item.createdDate != null ? new Date(item.createdDate) : null;

                        item._updateDate = item.updateDate != null && item.createdDate != null && item.updateDate !== item.createdDate ? parseDate(new Date(item.updateDate), true) : null;
                        item.updateDate = item.updateDate != null ? new Date(item.updateDate) : null;

                        item._senddate = item.sendDate != null ? parseDate(new Date(item.sendDate), true) : null;
                        item.sendDate = item.sendDate != null ? new Date(item.sendDate) : null;

                        item._terminationDate = item.terminationDate != null ? parseDate(new Date(item.terminationDate), false) : null;
                        item.terminationDate = item.terminationDate != null ? new Date(item.terminationDate) : null;
                    });
                    $scope.pcueHolderList = angular.copy($scope.pcueList);

                }, function errorCallback(res) {
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.FilterStatistics = function () {
            $scope.pcueList = angular.copy($scope.pcueHolderList);

            ///filter by active
            if ($scope.Search.IsActive !== 0)
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    switch ($scope.Search.IsActive) {
                        case 0:///all
                            return true;
                        case 1:///no
                            return !item.activeRecord
                        case 2:///yes
                            return item.activeRecord
                        default:
                            return true;
                    }
                });

            ///filter by sent mail
            if ($scope.Search.WasSent !== 0)
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    switch ($scope.Search.WasSent) {
                        case 0:
                            return true;
                        case 1:
                            return item.sendDate === null;
                        case 2:
                            return item.sendDate !== null;
                        default:
                            return true;
                    }
                });

            ///filter by name
            if ($scope.Search.Name !== null && $scope.Search.Name.length > 0) {
                var regex = RegExp($scope.Search.Name);
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    return regex.test(item.user_Name);
                });
            }

            ///filter by email
            if ($scope.Search.Email !== null && $scope.Search.Email.length > 0) {
                var regex = RegExp($scope.Search.Email);
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    return regex.test(item.email);
                });
            }

            ///filter by id number
            if ($scope.Search.IdNumber !== null && $scope.Search.IdNumber.length > 0) {
                var regex = RegExp($scope.Search.IdNumber);
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    return regex.test(item.idNumber);
                });
            }

            $scope.$Pager.PageNumber = 1;
            $scope.$Pager.Counter = $scope.pcueList.length;
            $scope.$Pager.MaxPages = Math.ceil($scope.pcueList.length / $scope.$Pager.PageSize);
            var num = 1;

            $scope.$Pager.pagerList = [];
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                $scope.$Pager.pagerList.push(1 + i);
            }
        }
        $scope.selectTotal = function () {
            $scope.reSetOtherVariables('total');
            switch ($scope.Search.total) {
                case 0:
                    $scope.Search.total = 1;
                    break;
                case 1:
                    $scope.Search.total = 2;
                    break;
                case 2:
                    $scope.Search.total = 0;
                    break;
                default:
                    $scope.Search.total = 0;
            }
            switch ($scope.controllerState) {
                case '101Form':
                case 'LogInLog':
                    $scope.Filter101Statistics();
                    break;
                case 'EmailPaycheck':
                    $scope.FilterStatistics();
                    break;
                case 'DailyUsage':
                    $scope.FilterDailyUsage();
                    break;
            }
        }
        $scope.selectActive = function () {
            $scope.reSetOtherVariables('IsActive');
            switch ($scope.Search.IsActive) {
                case 0:
                    $scope.Search.IsActive = 1;
                    break;
                case 1:
                    $scope.Search.IsActive = 2;
                    break;
                case 2:
                    $scope.Search.IsActive = 0;
                    break;
                default:
                    $scope.Search.IsActive = 0;
            }
            switch ($scope.controllerState) {
                case '101Form':
                case 'LogInLog':
                    $scope.Filter101Statistics();
                    break;
                case 'EmailPaycheck':
                    $scope.FilterStatistics();
                    break;
                case 'DailyUsage':
                    $scope.FilterDailyUsage();
                    break;
            }
        }
        $scope.selectSent = function () {
            $scope.reSetOtherVariables('WasSent');
            switch ($scope.Search.WasSent) {
                case 0:
                    $scope.Search.WasSent = 1;
                    break;
                case 1:
                    $scope.Search.WasSent = 2;
                    break;
                case 2:
                    $scope.Search.WasSent = 0;
                    break;
                default:
                    $scope.Search.WasSent = 0;
            }
            switch ($scope.controllerState) {
                case '101Form':
                case 'LogInLog':
                    $scope.Filter101Statistics();
                    break;
                case 'EmailPaycheck':
                    $scope.FilterStatistics();
                    break;
                case 'DailyUsage':
                    $scope.FilterDailyUsage();
                    break;
            }
        }
        $scope.Filter101Statistics = function () {
            var tval;
            $scope.pcueList = angular.copy($scope.pcueHolderList);
            ///filter by name
            if ($scope.Search.Name !== null && $scope.Search.Name.length > 0) {
                var regex = RegExp($scope.Search.Name);
                if ($scope.controllerState === '101Form') {
                    $scope.pcueList = $.grep($scope.pcueList, function (item) {
                        return regex.test(item.Name) || regex.test(item.municipalityId);
                    });
                }
                if ($scope.controllerState === 'LogInLog') {
                    $scope.pcueList = $.grep($scope.pcueList, function (item) {
                        return regex.test(item.name) || regex.test(item.id);
                    });
                }
            }

            ///filter 101 forms by year
            if ($scope.Search.Year !== '0') {
                var val = parseInt($scope.Search.Year);
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    return item.Form_Year === val;
                });
            }

            ///filter by active municipality
            if ($scope.Search.MunicipalityIsActive !== null) {
                tval = $scope.Search.MunicipalityIsActive ? 1 : 0;
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    return item.Active === tval;
                });
            }

            ///filter by active 101 form
            if ($scope.Search.ActiveForm !== null) {
                tval = $scope.Search.ActiveForm ? 1 : 0;
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    return item.Allow101 === tval;
                });
            }

            ///order by total forms
            if ($scope.Search.total > 0) {
                switch ($scope.Search.total) {
                    case 1:///desc
                        $scope.pcueList.sort(function (a, b) {
                            return a.prospective_users < b.prospective_users ? 1 : a.prospective_users > b.prospective_users ? -1 : 0
                        });
                        break;
                    case 2:///asc
                        $scope.pcueList.sort(function (a, b) {
                            return a.prospective_users > b.prospective_users ? 1 : a.prospective_users < b.prospective_users ? -1 : 0
                        });
                        break;
                }
            }
            ///order by good forms
            if ($scope.Search.IsActive > 0) {
                switch ($scope.Search.IsActive) {
                    case 1:///desc
                        if ($scope.controllerState === '101Form') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.Oks < b.Oks ? 1 : a.Oks > b.Oks ? -1 : 0
                            });
                        }
                        if ($scope.controllerState === 'LogInLog') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.active_users < b.active_users ? 1 : a.active_users > b.active_users ? -1 : 0
                            });
                        }
                        break;
                    case 2:///asc
                        if ($scope.controllerState === '101Form') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.Oks > b.Oks ? 1 : a.Oks < b.Oks ? -1 : 0
                            });
                        }
                        if ($scope.controllerState === 'LogInLog') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.active_users > b.active_users ? 1 : a.active_users < b.active_users ? -1 : 0
                            });
                        }
                        break;
                }
            }
            ///order by bad forms
            if ($scope.Search.WasSent > 0) {
                switch ($scope.Search.WasSent) {
                    case 1:///desc
                        if ($scope.controllerState === '101Form') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.Not_Ok < b.Not_Ok ? 1 : a.Not_Ok > b.Not_Ok ? -1 : 0
                            });
                        }
                        if ($scope.controllerState === 'LogInLog') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.usegPercentage < b.usegPercentage ? 1 : a.usegPercentage > b.usegPercentage ? -1 : 0
                            });
                        }
                        break;
                    case 2:///asc
                        if ($scope.controllerState === '101Form') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.Not_Ok > b.Not_Ok ? 1 : a.Not_Ok < b.Not_Ok ? -1 : 0
                            });
                        }
                        if ($scope.controllerState === 'LogInLog') {
                            $scope.pcueList.sort(function (a, b) {
                                return a.usegPercentage > b.usegPercentage ? 1 : a.usegPercentage < b.usegPercentage ? -1 : 0
                            });
                        }
                        break;
                }
            }
            ///order by terminationCause
            if ($scope.Search.T_Cause > 0) {
                switch ($scope.Search.T_Cause) {
                    case 1:///desc
                        $scope.pcueList.sort(function (a, b) {
                            return a.terminationCause < b.terminationCause ? 1 : a.terminationCause > b.terminationCause ? -1 : 0
                        });
                        break;
                    case 2:///asc
                        $scope.pcueList.sort(function (a, b) {
                            return a.terminationCause > b.terminationCause ? 1 : a.terminationCause < b.terminationCause ? -1 : 0
                        });
                        break;
                }
            }
            ///order by terminationDate
            if ($scope.Search.T_Date > 0) {
                switch ($scope.Search.T_Date) {
                    case 1:///desc
                        $scope.pcueList.sort(function (a, b) {
                            return a.terminationDate < b.terminationDate ? 1 : a.terminationDate > b.terminationDate ? -1 : 0
                        });
                        break;
                    case 2:///asc
                        $scope.pcueList.sort(function (a, b) {
                            return a.terminationDate > b.terminationDate ? 1 : a.terminationDate < b.terminationDate ? -1 : 0
                        });
                        break;
                }
            }

            $scope.$Pager.PageNumber = 1;
            $scope.$Pager.Counter = $scope.pcueList.length;
            $scope.$Pager.MaxPages = Math.ceil($scope.pcueList.length / $scope.$Pager.PageSize);
            var num = 1;

            $scope.$Pager.pagerList = [];
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                $scope.$Pager.pagerList.push(1 + i);
            }
        }
        $scope.FilterDailyUsage = function () {
            $scope.pcueList = angular.copy($scope.pcueHolderList);
            ///filter by name
            if ($scope.Search.Name !== null && $scope.Search.Name.length > 0) {
                var regex = RegExp($scope.Search.Name);
                $scope.pcueList = $.grep($scope.pcueList, function (item) {
                    return regex.test(item.name) || regex.test(item.id);
                });
            }
            ///order by totalUsersCounter
            switch ($scope.Search.total) {
                case 1:///desc
                    $scope.pcueList.sort(function (a, b) {
                        return a.totalUsersCounter < b.totalUsersCounter ? 1 : a.totalUsersCounter > b.totalUsersCounter ? -1 : 0
                    });
                    break;
                case 2:///asc
                    $scope.pcueList.sort(function (a, b) {
                        return a.totalUsersCounter > b.totalUsersCounter ? 1 : a.totalUsersCounter < b.totalUsersCounter ? -1 : 0
                    });
                    break;
            }
            ///order by dailyUsage
            switch ($scope.Search.WasSent) {
                case 1:///desc
                    $scope.pcueList.sort(function (a, b) {
                        return a.dailyUsage < b.dailyUsage ? 1 : a.dailyUsage > b.dailyUsage ? -1 : 0
                    });
                    break;
                case 2:///asc
                    $scope.pcueList.sort(function (a, b) {
                        return a.dailyUsage > b.dailyUsage ? 1 : a.dailyUsage < b.dailyUsage ? -1 : 0
                    });
                    break;
            }
            ///order by dailyUsage
            switch ($scope.Search.IsActive) {
                case 1:///desc
                    $scope.pcueList.sort(function (a, b) {
                        return a.actionDate < b.actionDate ? 1 : a.actionDate > b.actionDate ? -1 : 0
                    });
                    break;
                case 2:///asc
                    $scope.pcueList.sort(function (a, b) {
                        return a.actionDate > b.actionDate ? 1 : a.actionDate < b.actionDate ? -1 : 0
                    });
                    break;
            }

            $scope.$Pager.PageNumber = 1;
            $scope.$Pager.Counter = $scope.pcueList.length;
            $scope.$Pager.MaxPages = Math.ceil($scope.pcueList.length / $scope.$Pager.PageSize);
            var num = 1;

            $scope.$Pager.pagerList = [];
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                $scope.$Pager.pagerList.push(1 + i);
            }
        }
        $scope.FilterActiveMunicipality = function (val) {
            $scope.Search.MunicipalityIsActive = val;
            $scope.Search.ActiveForm = null;
            $scope.Filter101Statistics();
        }
        $scope.FilterActiveForm = function (val) {
            $scope.Search.ActiveForm = val;
            $scope.Search.MunicipalityIsActive = null;
            $scope.Filter101Statistics();
        }
        $scope.selectCause = function () {
            $scope.reSetOtherVariables('T_Cause');
            switch ($scope.Search.T_Cause) {
                case 0:
                    $scope.Search.T_Cause = 1;
                    break;
                case 1:
                    $scope.Search.T_Cause = 2;
                    break;
                case 2:
                    $scope.Search.T_Cause = 0;
                    break;
                default:
                    $scope.Search.T_Cause = 0;
            }
            $scope.Filter101Statistics();
        }
        $scope.selectReason = function () { }
        $scope.selectDate = function () {
            $scope.reSetOtherVariables('T_Date');
            switch ($scope.Search.T_Date) {
                case 0:
                    $scope.Search.T_Date = 1;
                    break;
                case 1:
                    $scope.Search.T_Date = 2;
                    break;
                case 2:
                    $scope.Search.T_Date = 0;
                    break;
                default:
                    $scope.Search.T_Date = 0;
            }
            $scope.Filter101Statistics();
        }

        $scope.reSetOtherVariables = function (Me) {
            switch (Me) {
                case 'IsActive':
                    if ($scope.controllerState !== "EmailPaycheck")
                        $scope.Search.WasSent = 0;
                    $scope.Search.total = 0;
                    $scope.Search.T_Cause = 0;
                    $scope.Search.T_Reason = 0;
                    $scope.Search.T_Date = 0;
                    break;
                case 'total':
                    $scope.Search.IsActive = 0;
                    $scope.Search.WasSent = 0;
                    $scope.Search.T_Cause = 0;
                    $scope.Search.T_Reason = 0;
                    $scope.Search.T_Date = 0;
                    break;
                case 'WasSent':
                    if ($scope.controllerState !== "EmailPaycheck")
                        $scope.Search.IsActive = 0;
                    $scope.Search.total = 0;
                    $scope.Search.T_Cause = 0;
                    $scope.Search.T_Reason = 0;
                    $scope.Search.T_Date = 0;
                    break;
                case 'T_Cause':
                    $scope.Search.IsActive = 0;
                    $scope.Search.total = 0;
                    $scope.Search.WasSent = 0;
                    $scope.Search.T_Reason = 0;
                    $scope.Search.T_Date = 0;
                    break;
                case 'T_Reason':
                    $scope.Search.IsActive = 0;
                    $scope.Search.total = 0;
                    $scope.Search.WasSent = 0;
                    $scope.Search.T_Cause = 0;
                    $scope.Search.T_Date = 0;
                    break;
                case 'T_Date':
                    $scope.Search.IsActive = 0;
                    $scope.Search.total = 0;
                    $scope.Search.WasSent = 0;
                    $scope.Search.T_Cause = 0;
                    $scope.Search.T_Reason = 0;
                    break;

                default:
                    $scope.Search.IsActive = 0;
                    $scope.Search.total = 0;
                    $scope.Search.WasSent = 0;
                    $scope.Search.T_Cause = 0;
                    $scope.Search.T_Reason = 0;
                    $scope.Search.T_Date = 0;
                    break;
            }
        }

        if ($scope.controllerState === '101Form' || $scope.controllerState === 'LogInLog') {
            $scope.pcueList = statistics.data;
            $scope.$Pager.Counter = $scope.pcueList.length;
            $scope.$Pager.MaxPages = Math.ceil($scope.pcueList.length / $scope.$Pager.PageSize);
            var num = 1;

            $scope.$Pager.pagerList = [];
            for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                $scope.$Pager.pagerList.push(1 + i);
            }
            ///usegPercentage
            if ($scope.controllerState === 'LogInLog') {
                $.map($scope.pcueList, function (item) {
                    item.usegPercentage = parseFloat(((item.active_users / item.prospective_users) * 100).toFixed(2));
                });
            }

            $scope.pcueHolderList = angular.copy($scope.pcueList);

            var list = new Array();
            $.map($scope.pcueList, function (item) {
                if (list.indexOf(item.Form_Year) < 0) {
                    list.push(item.Form_Year);
                    $scope.yearsList.push({ name: item.Form_Year, id: item.Form_Year });
                }
            });
            if (list.length <= 1)
                $scope.yearsList = new Array();
        }

        $scope.selectMulti = function () {
            $scope.StatisticsForm.hidden_value.$setValidity('required', true);
            $scope.StatisticsForm.hidden_value.$setValidity('custom', true);
            if ($scope.$SM.selectedList.length >= 9) {
                $scope.municipality_hidden_value = 9;
                $scope.StatisticsForm.hidden_value.$setDirty();
                $scope.StatisticsForm.hidden_value.$setValidity('custom', false);
            }
        }

        $scope.GetDailyUsageStatistics = function () {
            $scope.StatisticsForm.hidden_value.$setDirty();
            $scope.municipality_hidden_value = null;

            $scope.StatisticsForm.hidden_value.$setValidity('required', false);
            if ($scope.$SM.selectedList != undefined && $scope.$SM.selectedList.length >= 1) {
                $scope.municipality_hidden_value = 1;
                $scope.StatisticsForm.hidden_value.$setValidity('required', true);
            }
            $scope.StatisticsForm.hidden_value.$setValidity('custom', true);
            if ($scope.$SM.selectedList.length > 9) {
                $scope.municipality_hidden_value = 9;
                $scope.StatisticsForm.hidden_value.$setDirty();
                $scope.StatisticsForm.hidden_value.$setValidity('custom', false);
            }

            if ($scope.StatisticsForm.$invalid)
                return void [0];

            $scope.$Pager.PageNumber = 1;
            $scope.Search = {
                Name: '',
                Email: '',
                RegistrationDate: '',
                UpdateDate: '',
                IsActive: 0,
                WasSent: 0,
                total: 0,
                IdNumber: '',
                Year: '0',
                MunicipalityIsActive: null,
                ActiveForm: null,
                T_Cause: 0,
                T_Reason: 0,
                T_Date: 0
            };

            var _l = [];
            $.map($scope.$SM.selectedList, function (item) {
                _l.push(item.ID);
            });
            $scope.UserSearch.searchMunicipalitiesId = _l.join(',');

            Admin.GetDailyUsagetatistics($scope.UserSearch)
                .then(function successCallback(res) {
                    $scope.pcueList = res.data;

                    $scope.$Pager.Counter = res.data.length;
                    $scope.$Pager.MaxPages = Math.ceil(res.data.length / $scope.$Pager.PageSize);
                    var num = 1;

                    $scope.$Pager.pagerList = [];
                    for (var i = 0; i <= $scope.$Pager.MaxPages; i++) {
                        $scope.$Pager.pagerList.push(1 + i);
                    }

                    $.map($scope.pcueList, function (item) {
                        item._actionDate = parseDate(new Date(item.actionDate), false);
                    });
                    $scope.pcueHolderList = angular.copy($scope.pcueList);

                }, function errorCallback(res) {
                    //console.log('errorCallback', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }

        $scope.Export = function () {
            var dataObj = [];
            $.map($scope.pcueList, function (item) {
                dataObj.push({
                    'תעודת זהות': item.idNumber,
                    'שם מלא': item.user_Name,
                    'כתובת מייל': item.email,
                    'תאריך רישום': item._createdDate,
                    'תאריך עדכון': item._updateDate,
                    'אישור תלוש במייל': item.activeRecord ? 1 : 0,
                    'תאריך שליחה': item._senddate,
                    'מקור רישום': item.creationSource !== null ? (item.creationSource ? 'מכלול' : 'תיק עובד') : null,
                    'קוד סיום': item.terminationCause,
                    'סיבת סיום': item.terminationReason,
                    'תאריך סיום': item._terminationDate
                });
            });
            return dataObj;
        }
    }
})();