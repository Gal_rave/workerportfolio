﻿(function () {
    angular.module('WorkerPortfolioApp').controller('UserDetailsController', UserDetailsController);

    UserDetailsController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$location', 'Credentials', 'toastrService', '$controllerState', '$passwordSettings', '$personalDetails'];

    function UserDetailsController($scope, $rootScope, $state, $stateParams, $location, Credentials, toastrService, $controllerState, $passwordSettings, $personalDetails) {

        var maritalStatusArray = ["", "רווק/ה", "נשוי/אה", "גרוש/ה", "אלמן/ה", "פרוד/ה"];
        $scope.$passwordSettings = $passwordSettings.data;
        $scope.$controllerState = $controllerState;
        $scope.parseFullUserData = function (user) {
            return {
                ID: user.ID,
                birthDate: $scope.parseIntoDate(user.birthDate),
                set_birthDate: $scope.parseIntoDate(user.birthDate, true),
                createdDate: user.createdDate,
                cell: user.cell,
                email: user.email,
                firstName: user.firstName,
                idNumber: user.idNumber,
                immigrationDate: $scope.parseIntoDate(user.immigrationDate),
                set_immigrationDate: $scope.parseIntoDate(user.immigrationDate, true),
                lastName: user.lastName,
                phone: user.phone,
                fathersName: user.fathersName,
                previousLastName: user.previousLastName,
                previousFirstName: user.previousFirstName,
                gender: user.gender,
                maritalStatus: maritalStatusArray[user.maritalStatus],
                maritalStatusDate: $scope.parseIntoDate(user.maritalStatusDate),
                set_maritalStatusDate: $scope.parseIntoDate(user.maritalStatusDate, true),
                countryOfBirth: user.countryOfBirth,
                city: user.city,
                zip: user.zip,
                street: user.street,
                poBox: user.poBox,
                houseNumber: user.houseNumber,
                entranceNumber: user.entranceNumber,
                apartmentNumber: user.apartmentNumber,
                hasIncome: user.hasIncome,
                incomeType: user.incomeType,
                hmoCode: user.hmoCode,
                jobTitle: user.jobTitle,
                kibbutzMember: user.kibbutzMember
            }
        }
        $scope.parseIntoDate = function (d, isSet) {
            try {
                var date = d && typeof d === 'string' && d.length > 10 ? new Date(d) : null, returnDate = null;
                if (date !== null) {
                    if (date.getFullYear() < 1900)
                        return '';
                    function pad(val) { return val < 10 ? '0' + val.toString() : val.toString(); }

                    returnDate = pad(date.getDate()) + '/' + pad(date.getMonth() + 1) + '/' + date.getFullYear();
                    if (isSet) returnDate = pad(date.getMonth() + 1) + '/' + pad(date.getDate()) + '/' + date.getFullYear();
                }

                return returnDate;
            } catch (e) {
                //console.log('e', e);
                return null;
            }
        }
        $scope.ReSetDate = function (d) {
            if (!d || typeof d !== 'string' || d.length < 8)
                return null;
            var sd = d.split('/'), returnDate = new Date();
            returnDate.setMonth((parseInt(sd[1]) - 1)); returnDate.setFullYear(sd[2]); returnDate.setDate(sd[0]);
            return returnDate;
        }
        $scope.UserDetails = {};
        $scope.$personalDetails = $personalDetails.data;

        switch ($controllerState) {
            case 'UpdateUser':
                $scope.UserDetails = $scope.parseFullUserData($personalDetails.data);
                break;
            case 'UpdatePartner':
                if (typeof $personalDetails.data !== 'undefined' && $personalDetails.data !== null) $scope.UserDetails = $scope.parseFullUserData($personalDetails.data);
                break;
            case 'UpdateEducation':
                $scope.EducationList = $personalDetails.data;
                if($scope.EducationList.length > 0){
                    $scope.EducationList.filter(function (education) {
                        education.degreeDate = $scope.parseIntoDate(education.degreeDate); 
                    });
                }
                break;
            case 'UpdateChildren':
                $scope.ChildrenList = $personalDetails.data;
                if ($scope.ChildrenList.length > 0) {
                    $scope.ChildrenList.filter(function (child) {
                        child.birthDate = $scope.parseIntoDate(child.birthDate);
                        child.gender = child.gender ? 'זכר' : 'נקבה';
                        child.homeLiving = child.homeLiving ? 'כן' : 'לא';
                    });
                }
                break;
            
            default:
                $state.go('Home');
                break;
        }

        $scope.validatePasswords = function () {
            $scope.PasswordResetForm.newPassword.$setValidity('custom', true);
            $scope.PasswordResetForm.reEnterPassword.$setValidity('custom', true);
            ///return if not valid on other checks
            if ($scope.PasswordResetForm.reEnterPassword.$error.required || $scope.PasswordResetForm.reEnterPassword.$error.minlength || $scope.PasswordResetForm.reEnterPassword.$error.maxlength
                || $scope.PasswordResetForm.newPassword.$error.required || $scope.PasswordResetForm.newPassword.$error.minlength || $scope.PasswordResetForm.newPassword.$error.maxlength)
                return void [0];
            ///check minRequiredNonAlphanumericCharacters
            if ($scope.UserDetails.newPassword.replace(/[^\W|_]/gi, '').length < $scope.$passwordSettings.minNonAlphanumeric) {
                $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] תווים מיוחדים!'.replace('[0]', $scope.$passwordSettings.minNonAlphanumeric);
                $scope.PasswordResetForm.newPassword.$setValidity('custom', false);
                return void [0];
            }
            ///check minRequiredNumericCharacters
            if ($scope.UserDetails.newPassword.replace(/[\D|_]/gi, '').length < $scope.$passwordSettings.minNumeric) {
                $scope.$passwordSettings.validationMessage = 'חובה להכניס לפחות [0] מספרים!'.replace('[0]', $scope.$passwordSettings.minNumeric);
                $scope.PasswordResetForm.newPassword.$setValidity('custom', false);
                return void [0];
            }
            ///passwords dont match
            if ($scope.UserDetails.newPassword !== $scope.UserDetails.reEnterPassword) {
                $scope.PasswordResetForm.reEnterPassword.$setValidity('custom', false);
                return void [0];
            }
        }
        ///update user details scope functions
        $scope.updateUser = function () {
            if ($scope.UpdateDetailsForm.$invalid)
                return void [0];

            Credentials.UpdateUserDetails($scope.setSendData())
                .then(function successCallback(res) {
                    toastrService.success('נתוניך התעדכנו בהצלחה!', 'תודה.');
                    $scope.publish('UpdateShowLoginSatet', res.data);
                }, function errorCallback(res) {
                    //console.log('full-error', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.updatePartner = function () {
            if ($scope.UpdateDetailsForm.$invalid)
                return void [0];
            ///TODO -> FIX TO UPDATE PARTNER!!!
            Credentials.UpdateUserDetails($scope.setSendData())
                .then(function successCallback(res) {
                    toastrService.success('נתוניך התעדכנו בהצלחה!', 'תודה.');
                    $scope.publish('UpdateShowLoginSatet', res.data);
                }, function errorCallback(res) {
                    //console.log('full-error', res);
                    toastrService.error('', 'תקלת מערכת!');
                });
        }
        $scope.updateEducation = function () {
            if ($scope.UpdateEducationForm.$invalid)
                return void [0];
            ///TODO -> FIX TO UPDATE EDUCATION!!!
            
        }
        $scope.updateChildren = function () {
            if ($scope.UpdateChildrenForm.$invalid)
                return void [0];
            ///TODO -> FIX TO UPDATE CHILDREN!!!

        }

        $scope.setSendData = function () {
            var UserModel = angular.copy($scope.UserDetails);
            UserModel.birthDate = $scope.ReSetDate(UserModel.birthDate);
            UserModel.immigrationDate = $scope.ReSetDate(UserModel.immigrationDate);
            UserModel.maritalStatusDate = $scope.ReSetDate(UserModel.maritalStatusDate);
            return UserModel;
        }

    }
})();
