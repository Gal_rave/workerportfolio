﻿(function () {
    SignaturePad.prototype._strokeWidth = function (velocity) {
        return 6;
    };

    angular.module('WorkerPortfolioApp').directive('digitalSignaturePad', DigitalSignaturePad);

    DigitalSignaturePad.$inject = ['$window', '$timeout'];

    function DigitalSignaturePad($window, $timeout) {
        var directive = {
            link: link,
            restrict: 'EA',
            templateUrl: '../../Scripts/views/ng-addons/signature-pad.html',
            replace: true,
            scope: {
                signatureModel: '='
            }
        };
        return directive;

        function link(scope, element, attrs) {
            var mobileSize = 1024;

            var wrapper = document.getElementById("signature-pad"),
            canvas = wrapper.querySelector("canvas.canvas"),
            bkCanvas = wrapper.querySelector("canvas.bk-canvas"),
            parent = $('#signature-pad');

            function setBackground() {
                var ctx = bkCanvas.getContext("2d");
                ctx.strokeStyle = "#CCCCCC";///stroke color
                ctx.lineWidth = 3;
                ctx.beginPath();
                ctx.moveTo(30, canvas.height - 30);
                ctx.lineTo(canvas.width - 30, canvas.height - 30);
                ctx.stroke();

                var x = canvas.width - 57, y = canvas.height - 57;
                ctx.lineWidth = 6;

                if (window.innerWidth >= mobileSize)
                    drawX(ctx, x, y);

            }
            function drawX(ctx, x, y) {
                ctx.beginPath();

                ctx.moveTo(x - 20, y - 20);
                ctx.lineTo(x + 20, y + 20);

                ctx.moveTo(x + 20, y - 20);
                ctx.lineTo(x - 20, y + 20);
                ctx.stroke();
            }

            scope.signatureModel = new SignaturePad(canvas, { dotSize: 30 });
            scope.BK_signatureModel = new SignaturePad(bkCanvas);

            scope.ClearPad = function () {
                scope.signatureModel.clear();
                setBackground();
            }

            // Adjust canvas coordinate space taking into account pixel ratio,
            // to make it look crisp on mobile devices.
            // This also causes canvas to be cleared.
            function resizeCanvas() {
                // When zoomed out to less than 100%, for some very strange reason,
                // some browsers report devicePixelRatio as less than 1
                // and only part of the canvas is cleared then.
                var ratio = 1,//Math.max(window.devicePixelRatio || 1, 1),
                    width = window.innerWidth < mobileSize ? (parent.width()) : (parent.width() - 42),
                    height = window.innerWidth < mobileSize ? (parent.height()) : (parent.height() - 84);
                
                if ((width / 2) > height && window.innerWidth < mobileSize)
                    height = (width / 2);
                
                canvas.width = width;
                canvas.height = height;
                canvas.style.width = width + 'px';
                canvas.style.height = height + 'px';

                canvas.getContext("2d").scale(ratio, ratio);

                bkCanvas.width = width;
                bkCanvas.height = height;
                bkCanvas.style.width = width + 'px';
                bkCanvas.style.height = height + 'px';
                bkCanvas.getContext("2d").scale(ratio, ratio);
                scope.ClearPad();
            }

            window.onresize = resizeCanvas;
            resizeCanvas();

        }
    }

})();