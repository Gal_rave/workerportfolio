﻿(function () {
    angular.module('WorkerPortfolioApp').directive('login', login);

    login.$inject = ['$compile', '$window', '$state', '$rootScope', 'Credentials', 'toastrService', '$templateRequest'];

    function login($compile, $window, $state, $rootScope, Credentials, toastrService, $templateRequest) {
        var helper = {
            _credentials: {
                idNumber: null,
                password: null,
                loggedIn: false,
                resetPassword: false,
                lastLogin: null,
                sendOption: 1,
                $sendVal: null,
                currentTerminationDate: null,
                mobileLogInDisplay: 0,
                currentMunicipalityId: null,
                isAllow101: false,
                isAllowProcess: false,
                isAllowMunicipalityMenu: false,
                isAllowSystemsEmail: false,
                isAllowEPC: false
            },
            parseLastLogIn: function (dateStr) {
                var dateArray = dateStr.split('T'), model = {};
                model.lastDate = dateArray[0].split('-').splice(1, 2).reverse().join('/');
                model.lastTime = dateArray[1].split('.')[0].split(':').splice(0, 2).join(':');

                return model;
            },
            _resetForm: function (loginForm) {
                loginForm.$setPristine();
                loginForm.$setUntouched();
                _.each(loginForm, function (item) {
                    if (typeof item !== 'undefined' && typeof item.$setValidity === 'function') {
                        _.each(item.$validators, function ($validator, validatorName) {
                            item.$setUntouched();
                            item.$setPristine();
                        });
                    }
                });
            },
            mobileTemplate: '../../Scripts/views/ng-addons/mobile-Login-pannel.html',
            pcTemplate: '../../Scripts/views/ng-addons/Login-pannel.html',
            getCurrentTemplate: function () {
                if ($window.innerWidth < 801) {
                    return { template: helper.mobileTemplate, isMobile: true };
                } else {
                    return { template: helper.pcTemplate, isMobile: false };
                }
            },
            doReCompile: function (scope) {
                if (scope._Width == undefined || scope._Width == null) {
                    scope._Width = 0;
                    scope._positionX = null;
                }

                if (($window.innerWidth < (scope._Width - 10) || $window.innerWidth > (scope._Width + 10))) {
                    scope._Width = $window.innerWidth
                    scope._positionX = scope._Width > 800? null : 'bottom';
                    return true;
                } else {
                    return false;
                }
            }
        };
        var directive = {
            link: link,
            restrict: 'E',
            scope: true,
        };
        return directive;

        function link(scope, element, attrs) {
            window.toastrService = toastrService;
            $rootScope.templateHandel = helper.getCurrentTemplate();
            scope.credentials = angular.copy(helper._credentials);

            scope.MobileLogIn = function (val) {
                if (scope.credentials.loggedIn) {
                    scope.credentials.mobileLogInDisplay = 0;
                    return;
                }
                switch (val) {
                    case 1:
                        if (scope.credentials.mobileLogInDisplay === 0)
                            scope.credentials.mobileLogInDisplay = 1;
                        else
                            scope.credentials.mobileLogInDisplay = 0;
                        break;
                    case 2:
                        if (scope.credentials.mobileLogInDisplay === 1)
                            scope.credentials.mobileLogInDisplay = 2;
                        else
                            scope.credentials.mobileLogInDisplay = 1;
                        break;
                    case 3:
                        scope.credentials.mobileLogInDisplay = 0;
                        break;
                }
            }

            ///select the CurrentMunicipality
            scope.linkCurrentMunicipality = function () {
                if (scope.credentials.municipalities && scope.credentials.municipalities.length > 0) {
                    scope.currentMunicipality = $.grep(scope.credentials.municipalities, function (item) {
                        return item.ID == scope.credentials.currentMunicipalityId;
                    })[0];
                }
            }

            if (typeof $rootScope.$$credentials === 'undefined') {
                $rootScope.$$credentials = scope.credentials;
                var ExistingLogIn = $rootScope.lsGet('$$credentials');
                if (ExistingLogIn && typeof ExistingLogIn === 'object') {///user is loged in
                    scope.credentials = ExistingLogIn;
                    $rootScope.lsBind($rootScope, '$$credentials', scope.credentials);
                } else {
                    $rootScope.lsBind($rootScope, '$$credentials', scope.credentials);
                }
            } else {
                scope.credentials = $rootScope.$$credentials;
            }

            scope.linkCurrentMunicipality();

            scope.setSendVal = function (value) {
                if (typeof value !== 'undefined') {
                    scope.credentials.sendOption = value;
                    scope.credentials.$sendVal = scope.credentials.sendOption !== null && scope.credentials.sendOption > 0 ? true : null;
                } else {
                    scope.credentials.$sendVal = scope.credentials.sendOption !== null && scope.credentials.sendOption > 0 ? true : null;
                }
            }

            scope.doLogin = function () {
                if (scope.loginForm.$invalid)
                    return void [0];
                var data = { idNumber: angular.copy(scope.credentials.idNumber), password: angular.copy(scope.credentials.password) };
                scope._logout(false, false);
                Credentials.Login(data)
                    .then(function successCallback(res) {
                        if (res.data.success) {
                            sessionStorage.removeItem("$$popUpWindow");
                            scope._login(res.data.userCredentials, true);
                        } else {
                            scope.credentials.idNumber = data.idNumber;
                            switch (res.data.exceptionId) {
                                case 11://"UserMunicipalityVerifyException"
                                    toastrService.error('הראשות אינה פעילה במערכת.', 'אין כניסה!', { allowHtml: true, timeOut: 7000 });
                                    break;
                                case 10://"UserVerifyException"
                                    toastrService.warning('', 'שם משתמש או סיסמה שגויים!');
                                    break;
                                case 9://UserNotConfirmedException"
                                    toastrService.warning('', 'המשתמש לא אישר את החשבון!');
                                    break;
                                case 7://"NullReferenceException"
                                    toastrService.error('יש לוודא מול נציג הארגון שפרטי המייל / טלפון נייד מוגדרים במערכת.', 'לא נמצא משתמש לפי הפרטים שסיפקת!', { allowHtml: true, timeOut: 8000 });
                                    break;
                                case 101:///user is locked out
                                    toastrService.error('אנא נסה להתחבר מחדש בעוד LOCKTIMELEFT דקות'.replace('LOCKTIMELEFT', res.data.lockTimeLeft), 'סליחה, ננעלת מחוץ למערכת.');
                                    break;
                                default:
                                    //console.log('default Error', res);
                                    toastrService.error('', 'תקלת מערכת!');
                                    break;
                            }
                        }
                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                    });
            }
            scope.doLogout = function (showToast, goHome) {
                Credentials.LogOut()
                    .then(function successCallback(res) {
                        scope._logout(showToast, goHome);
                    }, function errorCallback() {
                        scope._logout(false, false);
                        toastrService.error('', 'תקלת מערכת!');
                    });
            }
            scope.doReset = function () {
                if (scope.resetForm.$invalid)
                    return void [0];

                Credentials.ResetPassword({ idNumber: scope.credentials.idNumber, mail: scope.credentials.sendOption === 1, sms: scope.credentials.sendOption === 2 })
                    .then(function successCallback(res) {
                        if (res.data.success) {
                            scope.credentials.resetPassword = false;
                            switch (scope.credentials.sendOption) {
                                case 1:///email
                                    toastrService.success('מייל עדכון סיסמה נשלח לכתובת המייל הרשומה, אנא עקוב אחר ההוראות במייל לאיפוס סימתך.', 'תודה, ' + res.data.userCredentials.fullName, { positionX: scope._positionX });
                                    break;
                                case 2:///sms
                                    toastrService.success('מזהה איפוס סיסמה נשלח לטלפון הנייד הרשום במערכת, אנא הכנס את הקוד לשדה המיועד.', 'תודה, ' + res.data.userCredentials.fullName, { positionX: scope._positionX });
                                    $state.go('Authentication.ResetPasswordFromSms', { idNumber: scope.credentials.idNumber });
                                    break;
                            }
                        } else {
                            switch (res.data.exceptionId) {
                                case 111:
                                    toastrService.error('אנא פנה לכ"א על מנת לעדכן פרטים.', 'למשתמש לא מוגדרת כתובת מייל במערכת!');
                                    break;
                                case 112:
                                    toastrService.error('אנא פנה לכ"א על מנת לעדכן פרטים.', 'למשתמש לא מוגדר מספר טלפון נייד במערכת!!');
                                    break;
                                case 58:
                                    toastrService.error('אנא פנה למנהל האתר על מנת להסדיר הרשמה או הרשם באתר.', 'המשתמש אינו מוגדר במערכת!');
                                    break;
                                default:
                                    toastrService.error(res.data.exception, 'תקלת מערכת!');
                                    break;
                            }
                        }
                        scope._logout(false, false, true);
                    }, function errorCallback(res) {
                        //console.log('error', res);
                        scope._logout(false, false, true);
                        toastrService.error('', 'תקלת מערכת!');
                    });
            }
            ///do the change municipality function
            scope.doChangeMunicipality = function () {
                if (!scope.currentMunicipality)
                    return void [0];
                scope.credentials.currentMunicipalityId = scope.currentMunicipality.ID;
                scope.credentials.isAllow101 = scope.currentMunicipality.allow101;
                scope.credentials.isAllowProcess = scope.currentMunicipality.isAllowProcess;
                scope.credentials.isAllowMunicipalityMenu = scope.currentMunicipality.isAllowMunicipalityMenu;
                scope.credentials.isAllowSystemsEmail = scope.currentMunicipality.isAllowSystemsEmail;
                scope.credentials.isAllowEPC = scope.currentMunicipality.isAllowEPC;

                $rootScope.$$credentials.currentMunicipalityId = scope.currentMunicipality.ID;
                $rootScope.$$credentials.isAllow101 = scope.currentMunicipality.allow101;
                $rootScope.$$credentials.isAllowProcess = scope.currentMunicipality.isAllowProcess;
                $rootScope.$$credentials.isAllowMunicipalityMenu = scope.currentMunicipality.isAllowMunicipalityMenu;
                $rootScope.$$credentials.isAllowSystemsEmail = scope.currentMunicipality.isAllowSystemsEmail;
                $rootScope.$$credentials.isAllowEPC = scope.currentMunicipality.isAllowEPC;

                Credentials.ChangeMunicipality({ currentMunicipalityId: scope.currentMunicipality.ID, idNumber: scope.credentials.idNumber, confirmationToken: scope.credentials.confirmationToken, calculatedIdNumber: scope.credentials.calculatedIdNumber, updateUserData: true })
                    .then(function successCallback(res) {
                        if (res.status === 200 && res.data.currentMunicipalityId === scope.currentMunicipality.ID) {
                            toastrService.success('', 'החלפת ארגון בהצלחה!', { positionX: scope._positionX });
                            $rootScope.lsRemove('$$tofesModel');
                            $rootScope.lsRemove('$$tofesModelCopy');
                            $rootScope.lsRemove('states');

                            $rootScope.$$credentials = res.data;
                            $rootScope.$$credentials.loggedIn = true;
                            $rootScope.$$credentials.lastLogin = helper.parseLastLogIn($rootScope.$$credentials.loginDate);

                            scope.credentials = $rootScope.$$credentials;
                            scope.publish('VerifayLoginState', scope.credentials);
                            scope.publish('ChangeMunicipality', scope.currentMunicipality.ID);
                            scope.publish('PdfContect.Changed', scope.credentials);

                            sessionStorage.removeItem("$$popUpWindow");

                            if ($state.current.data.type === 'Form101') {
                                $state.current.name == 'Form.employer-employee' ? $state.transitionTo($state.current, {}, { reload: true, inherit: true, notify: true }) : $state.go('Form.employer-employee');
                            } else {
                                $state.transitionTo($state.current, {}, { reload: true, inherit: true, notify: true });
                            }

                        } else {
                            toastrService.error('אנה נסה להיתחבר למערכת מחדש', 'תקלה!');
                            scope.doLogout(false, true);
                        }
                    }, function errorCallback(res) {
                        toastrService.error('', 'תקלת מערכת!');
                        scope.doLogout(false, true);
                    });
            }

            ///inner function to do the login/out 
            scope._login = function ($credentials, showToast) {
                $rootScope.$$credentials = $credentials;
                $rootScope.$$credentials.loggedIn = true;
                $rootScope.$$credentials.lastLogin = helper.parseLastLogIn($rootScope.$$credentials.loginDate);

                scope.credentials = $rootScope.$$credentials;
                scope.linkCurrentMunicipality();

                scope.publish('VerifayLoginState', scope.credentials);
                if (showToast) toastrService.success('ברוך הבא למערכת תיק עובד באינטרנט.', 'שלום, fullName'.replace('fullName', $rootScope.$$credentials.fullName), { positionX: scope._positionX });
                $state.go('Home');

                scope._refreshUserData();
            }
            scope._logout = function (showToast, goHome, doServer) {
                if (doServer) {
                    scope.doLogout(false, false);
                    return void [0];
                }
                if (showToast) toastrService.info('התנתקת מהמערכת בהצלחה.', 'תודה USERNAME'.replace('USERNAME', scope.credentials.fullName));
                $rootScope.$$credentials_unbind;
                $rootScope.lsRemove('$$credentials');
                $rootScope.$$credentials = scope.credentials = angular.copy(helper._credentials);
                scope.publish('VerifayLoginState', scope.credentials);
                if (goHome)
                    $state.go('Home');
            }

            ///inner function to refresh the user data 
            scope._refreshUserData = function () {
                Credentials.RefreshUserData({ currentMunicipalityId: scope.credentials.currentMunicipalityId, idNumber: scope.credentials.idNumber, confirmationToken: scope.credentials.confirmationToken, calculatedIdNumber: scope.credentials.calculatedIdNumber })
                    .then(function successCallback(res) {
                        $rootScope.$$credentials = res.data.userCredentials;
                        $rootScope.$$credentials.loggedIn = true;
                        $rootScope.$$credentials.lastLogin = helper.parseLastLogIn($rootScope.$$credentials.loginDate);
                        scope.credentials = $rootScope.$$credentials;
                        scope.publish('VerifayLoginState', scope.credentials);
                    }, function errorCallback(res) {
                        //console.log('errorCallback', res);
                        toastrService.error('', 'תקלת מערכת!');
                        scope._logout(false, false);
                    });
            }

            ///do login from client authentication - user starts account
            scope.subscribe('RefreshLoginState', function (userCredentials) {
                scope._login(userCredentials, false);
                toastrService.success('חשבונך אותחל בהצלחה!', 'תודה, fullName'.replace('fullName', $rootScope.$$credentials.fullName), { positionX: scope._positionX });
                $state.go('Home');
            });
            ///do login from client authentication - user reset password
            scope.subscribe('passwordResetLonin', function (userCredentials) {
                scope._login(userCredentials, false);
                $state.go('Home');
            });
            ///update the user data on client update
            scope.subscribe('UpdateShowLoginSatet', function (userCredentials) {
                scope._login(userCredentials, false);
            });
            ///subscribe to menu directive - transmit the login state
            scope.subscribe('TransmitLoginState', function () {
                if (scope.credentials.loggedIn) {
                    var login = new Date(scope.credentials.loginDate), now = new Date();
                    var diff = Math.ceil(Math.abs(now - login) / 36e5);
                    if (diff > 12)
                        scope.doLogout(false, true);
                }
                scope.publish('VerifayLoginState', scope.credentials);
            });
            ///subscribe to logout from controllers - when thers a problem in userdata
            scope.subscribe('CatchTransmitLogout', function (showToast, goHome) {
                if (typeof showToast === 'undefined')
                    showToast = false;
                if (typeof goHome === 'undefined')
                    goHome = true;
                scope.doLogout(showToast, goHome);
            });
            ///subscribe to logout from controllers - when thers a problem in userdata
            scope.subscribe('passwordResetLogout', function (val) {
                scope._logout(false, false, true);
            });

            scope.subscribe('showMobileMenu', function (val) {
                scope.credentials.mobileLogInDisplay = 0;
            });

            scope._compile = function () {
                if (!helper.doReCompile(scope))
                    return void [0];
                $rootScope.templateHandel = helper.getCurrentTemplate();

                $templateRequest($rootScope.templateHandel.template).then(function (html) {
                    var template = angular.element(html);
                    element.html('');
                    element.html(template);
                    $compile(template)(scope);
                }).then(function () {
                    scope.setSendVal(1);
                });
            }

            $(window).on("resize.doResize", _.debounce(function () {
                scope._compile();
            }, 150));

            scope._compile();
        }
    }
})();