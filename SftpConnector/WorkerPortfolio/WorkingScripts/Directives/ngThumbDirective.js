﻿(function() {    

    angular.module('WorkerPortfolioApp').directive('ngThumb', ['$window', '$sce', function ($window, $sce) {
        var helper = {
            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
            isFile: function(item) {
                return angular.isObject(item) && item instanceof $window.File;
            },
            isImage: function(file) {
                var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            },
            isPdf: function (file) {
                var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|pdf|'.indexOf(type) !== -1;
            },
            b64toBlob: function (b64Data, contentType) {
                contentType = contentType || '';
                var sliceSize = 512;
                b64Data = b64Data.replace(/^[^,]+,/, '');
                b64Data = b64Data.replace(/\s/g, '');
                var byteCharacters = window.atob(b64Data);
                var byteArrays = [];

                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

                var blob = new Blob(byteArrays, { type: contentType });
                return blob;
            }
        };

        return {
            restrict: 'A',
            template: '<canvas/>',
            link: function (scope, element, attributes) {
                if (!helper.support) return;

                var params = scope.$eval(attributes.ngThumb);
                
                if (!helper.isFile(params.file)) return;
                if (!helper.isImage(params.file)) {
                    element.remove();
                    return;
                }

                var canvas = element.find('canvas');
                var embed = element.find('embed');
                var reader = new FileReader();
                
                reader.onload = onLoadFile;
                reader.readAsDataURL(params.file);

                function onLoadFile(event) {
                    var img = new Image();
                    img.onload = onLoadImage;
                    img.src = event.target.result;
                }
                function onLoadPdf(event) {
                    var file = helper.b64toBlob(PayrollData.data.fileString, 'application/pdf');
                    var fileURL = URL.createObjectURL(file);
                    scope.content = $sce.trustAsResourceUrl(fileURL);
                }
                function onLoadImage() {
                    var width = params.width || this.width / this.height * params.height;
                    var height = params.height || this.height / this.width * params.width;
                    canvas.attr({ width: width, height: height });
                    canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                }
            }
        };
    }]);


})();