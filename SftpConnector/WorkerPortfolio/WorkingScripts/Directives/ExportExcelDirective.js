﻿(function () {
    angular.module('WorkerPortfolioApp').directive('exportExcel', exportExcel);

    exportExcel.$inject = ['FileSaver', '$timeout'];

    function exportExcel(FileSaver, $timeout) {
        var directive = {
            link: link,
            restrict: 'A',
            scope: {
                fileName: "=fileName",
                workSheet: "=workSheet",
                data: "=exportData",
                classes: "@"
            },
            replace: true,
            template: '<button class="btn btn-info btn-ef btn-ef-3 btn-ef-3c mb-10 {{AddindClass}}" ng-click="download()">הורדה לאקסל <i class="fa fa-download"></i></button>'
        };
        return directive;

        ///must function thet return list of data/list of data as JSON && file name/function thet returns file name && work sheet name/function thet returns work sheet name
        function link(scope, element) {
            scope.AddindClass = (typeof scope.classes !== 'undefined' && !scope.classes.isempty()) ? scope.classes : '';

            ///work seat name can b a function or string or null
            scope.getWorkSheetName = function () {
                switch (typeof scope.workSheet) {
                    case 'undefined':
                        return 'WORK_SHEET';
                    case 'function':
                        return scope.workSheet()
                    default:
                        return scope.workSheet;
                }
            }
            ///file name can b a function or string
            scope.getFileName = function () {
                switch (typeof scope.fileName) {
                    case 'function':
                        return scope.fileName()
                    default:
                        return scope.fileName;
                }
            }
            ///data can b a function or an array
            scope.getExportData = function () {
                switch (typeof scope.data) {
                    case 'function':
                        return scope.data()
                    default:
                        return scope.data;
                }
            }

            ///calc columns width
            scope.columnRange = function (workSheet) {
                var sheet2arr = function (sheet) {
                    var result = [];
                    var row;
                    var rowNum;
                    var colNum;
                    var range = XLSX.utils.decode_range(sheet['!ref']);
                    for (rowNum = range.s.r; rowNum <= range.e.r; rowNum++) {
                        row = [];
                        for (colNum = range.s.c; colNum <= range.e.c; colNum++) {
                            if (typeof result[colNum] === 'undefined') result[colNum] = 0;
                            var nextCell = sheet[
                                XLSX.utils.encode_cell({ r: rowNum, c: colNum })
                            ];
                            if (typeof nextCell !== 'undefined') {
                                if (nextCell.t === 's')
                                    result[colNum] = result[colNum] > nextCell.v.length ? result[colNum] : nextCell.v.length;
                                else {
                                    result[colNum] = result[colNum] > String(nextCell.v).length ? result[colNum] : String(nextCell.v).length;
                                }
                            }
                        }
                    }
                    return result;
                };

                var wscols = new Array(),
                range = sheet2arr(workSheet);
                for (var s in range)
                    wscols.push({ wch: (range[s] + 4) });
                return wscols;
            }

            scope.download = function () {
                /* generate a worksheet */
                var ws = XLSX.utils.json_to_sheet(scope.getExportData());
                /* set column width */
                ws['!cols'] = scope.columnRange(ws);
                /* add to workbook */
                var wb = XLSX.utils.book_new();

                XLSX.utils.book_append_sheet(wb, ws, scope.getWorkSheetName());

                /* write workbook and force a download */
                XLSX.writeFile(wb, scope.getFileName());
            };
        }
    }

})();

