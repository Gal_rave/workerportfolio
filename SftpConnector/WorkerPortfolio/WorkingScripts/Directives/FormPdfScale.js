﻿(function() {

    angular.module('WorkerPortfolioApp').directive('formPdfScale', formPdfScale);

    formPdfScale.$inject = ['$window', '$compile', '$rootScope', '$state'];
    
    function formPdfScale($window, $compile, $rootScope, $state) {
        var helper = {
            detectBrowser: function () {
                var userAgent = $window.navigator.userAgent;
                var browsers = { chrome: /chrome/i, safari: /safari/i, firefox: /firefox/i, ie: /internet explorer/i };
                for (var key in browsers) {
                    if (browsers[key].test(userAgent)) {
                        return key;
                    }
                };
                if (parseInt(document.documentMode) > 0)
                    return 'IE';

                return 'undefined';
            },
            //templateMobile: '<div ng-hide="pdfDataStatus == 406"><img ng-src="{{ngUrl}}" style="width:100%;height:100%;min-height:{{minHeight}}px;" /></div>',
            templateMobile: '<div ng-hide="pdfDataStatus == 406" template-mobile><img ng-src="data:image/png;base64,{{fileImageBlob}}" style="width:100%;height:100%;" /></div>',
            templateIE: '<div ng-hide="pdfDataStatus == 406" class="iframe-holder-div"><iframe id="template" ng-src="{{ngUrl}}" width="100%" height="100%" style="width:100%;height:100%;min-height:{{minHeight}}px;"></iframe></div>',
            templateChrom: '<object id="template" ng-bind="contentView" ng-show="pdfShow" data="{{pdfContent}}#{{contentView}}" style="width:100%;height:100%;min-height:{{minHeight}}px;" type="application/pdf" title="654">\
                                    <p>\
                                        It appears you don`t have a PDF plugin for this browser.\
                                        No problem though...\
                                        You can <a href="{{ngUrl}}">click here to download the PDF</a>.\
                                    </p>\
                                </object>',
            currentTemplate: undefined,
            getCurrentTemplate: function () {
                switch (helper.detectBrowser().toLowerCase()) {
                    case 'ie':
                        helper.currentTemplate = helper.templateIE;
                        break;
                    default:
                        helper.currentTemplate = helper.templateChrom;
                        break;
                }
                if ($window.innerWidth < 801)
                    helper.currentTemplate = helper.templateMobile;

                return helper.currentTemplate;
            },
            browserType: function () {
                if ($window.innerWidth < 801)
                    return 1;

                switch (helper.detectBrowser().toLowerCase()) {
                    case 'ie':
                        return 1;
                        break;
                    default:
                        return 0;
                        break;
                }
            }
        };
        var directive = {
            link: link,
            restrict: 'EA',
            replace: true,
            template: function () {
                return helper.currentTemplate || helper.getCurrentTemplate();
            },
            scope: {
                pdfContent: '=pdfContent',
                pdfShow: '=pdfShow',
                pdfDate: '=pdfDate',
                pdfDataStatus: '=pdfDataStatus',
                formPdfImageId: '=formPdfImageId',
                fileImageBlob: '=fileImageBlob',
            }
        };
        return directive;

        function link(scope, element, attrs) {
            window.IFRAME = element;

            scope.Browser = helper.browserType();
            var credentials = typeof $rootScope.$$credentials !== 'undefined' ? $rootScope.$$credentials : $rootScope.lsGet('$$credentials');
            scope.ngUrl = '';

            scope.contentView = '';
            scope.$Width = $window.outerWidth;
            scope.$Height = $window.outerHeight;
            
            scope.$watch('formPdfImageId', function (newValue, oldValue) {
                if (oldValue === newValue || (!oldValue && !newValue) || !newValue) {
                    return void [0];
                }
                
                scope.ngUrl = ($window.innerWidth < 801 ? '/home/Convert?' : '/home/FormIeShow?') + ['idNumber=' + credentials.idNumber, 'imageID=' + scope.formPdfImageId].join('&');

                if (scope.Browser === 1) {
                    element.children().remove();
                    setTimeout(function () {
                        scope.$apply(function () {
                            var content = $compile(helper.currentTemplate)(scope);
                            element.append(content);
                        });
                        pdfView();
                    }, 125);
                }
            }, true);

            var scaleWidthSwitch = function (width) {
                var w = parseInt(width / 50) - 28;
                w = w < 0 ? 0 : (w > 12 ? 12 : w);
                w = w > 0 ? (103 + (5 * (w - 1))) : 0;
                return w;
            }
            var pdfView = function (scale) {
                if (typeof scale === 'undefined')
                    scale = 'view=Fit';

                var w = scaleWidthSwitch(scope.$Width);
                if (w <= 0)
                    scale = 'view=Fit';
                else
                    scale = 'zoom=' + w;

                scope.contentView = scale;

                scope.minHeight = $window.innerWidth < 801 ? parseInt(scope.$Height / 1.15) : parseInt(scope.$Height * 1.15);
                if (scope.Browser === 1) {
                    element.find('iframe').attr('style', 'width:100%;height:100%;min-height:' + scope.minHeight + 'px;');
                }
            }

            $(window).on("resize.doResize", _.debounce(function () {
                scope.$Width = $window.outerWidth;
                scope.$Height = $window.outerHeight;
                if (scope.Browser !== 1) {
                    scope.$apply(function () {
                        var content = $compile(helper.currentTemplate)(scope);
                        element.append(content);
                    });
                }
                pdfView();
            }, 150));

            pdfView();
        }
    }

})();