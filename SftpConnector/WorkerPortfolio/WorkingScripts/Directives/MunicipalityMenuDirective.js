﻿(function () {

    angular.module('WorkerPortfolioApp').directive('municipalityMenu', MunicipalityMenu);

    MunicipalityMenu.$inject = ['$window', '$rootScope', '$timeout', '$state', 'toastrService', '$http'];

    function MunicipalityMenu($window, $rootScope, $timeout, $state, toastrService, $http) {
        var helper = {
           
        };
        var directive = {
            link: link,
            restrict: 'AE',
            templateUrl: '../../Scripts/views/ng-addons/municipality-menu.panel.html',
            replace: true,
            scope: true
        };
        return directive;

        function link(scope, element, attrs) {
            scope.$open = false;
            scope.OpemMenu = function () {
                scope.$open = !scope.$open;
            }

            scope.currentMunicipality = new Object();
            scope.menuTitle = '';
            scope.templates = new Array();

            scope.getCurrentMunicipalityData = function (municipalities, currentMunicipalityId) {
                return municipalities.filter(function (municipality) {
                    return municipality.ID == currentMunicipalityId;
                })[0];
            }
            scope.StartDirective = function (credentials) {
                scope.templates = new Array();
                scope.AllowMunicipalityMenu = credentials.loggedIn && credentials.isAllowMunicipalityMenu;

                if (scope.AllowMunicipalityMenu) {
                    scope.currentMunicipality = scope.getCurrentMunicipalityData(credentials.municipalities, credentials.currentMunicipalityId);
                    scope.menuTitle = scope.currentMunicipality.municipalityMenuTitle === null || scope.currentMunicipality.municipalityMenuTitle.isempty() ? 'מיוחד !!!' : scope.currentMunicipality.municipalityMenuTitle;
                    scope.templates = scope.currentMunicipality.htmlTemplates !== null && scope.currentMunicipality.htmlTemplates.length > 0 ? scope.currentMunicipality.htmlTemplates : new Array();

                    if (scope.templates === null || scope.templates.length < 1) {
                        scope.AllowMunicipalityMenu = false;
                    }
                }
            }
            
            scope.subscribe('VerifayLoginState', function ($credentials) {
                scope.StartDirective($credentials)
            });

            scope.StartDirective(scope.$$credentials);
        }
    }

})();