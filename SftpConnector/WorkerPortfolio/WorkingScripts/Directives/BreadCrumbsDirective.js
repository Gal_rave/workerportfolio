﻿(function() {
    angular.module('WorkerPortfolioApp').directive('breadCrumbs', breadCrumbs);

    breadCrumbs.$inject = ['$window', '$rootScope', '$state', '$stateParams'];
    
    function breadCrumbs($window, $rootScope, $state, $stateParams) {
        var directive = {
            link: link,
            restrict: 'EA',
            templateUrl: '../../Scripts/views/ng-addons/bread-crumbs-pannel.html',
            replace: true,
            scope: true,
        };
        return directive;

        function link(scope, element, attrs) {
            scope.$state = $state;
            scope.$stateParams = $stateParams;
        }
    }

})();

