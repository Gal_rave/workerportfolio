﻿(function() {
    
    angular.module('WorkerPortfolioApp').directive('liveCheckbox', liveCheckbox);

    liveCheckbox.$inject = [];
    
    function liveCheckbox() {
        var directive = {
            link: link,
            restrict: 'AE',
            require: 'ngModel',
            scope:
			{
			    classes: "@",
			    inputType: "@",
			    inputText: "@",
			    //inputDisabled: "=inputDisabled",
			    inputName: "@",
			    inputDisabled: '=inputDisabled',
			},
            templateUrl: '../../Scripts/views/ng-addons/live-checkbox.html',
            replace: true,
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            scope.$watch('inputDisabled', function (newValue, oldValue) {
                if (typeof newValue === 'undefined' || newValue === null || newValue === oldValue)
                    return void [0];
                scope.ngDisabled = parseInt(scope.inputDisabled) === 1;
            });

            scope.inputType = (scope.inputType !== undefined ? scope.inputType : 'button').toLowerCase();

            scope.inputName = typeof scope.inputName !== 'undefined' && !scope.inputName.isempty() ? scope.inputName : 'input_name';
            scope.AddindClass = typeof scope.classes !== 'undefined' && !scope.classes.isempty();
            scope.inputsText = typeof scope.inputText !== 'undefined' && !scope.inputText.isempty() ? scope.inputText.split(',') : ['כן', 'לא'];
            scope.ngDisabled = parseInt(scope.inputDisabled) === 1;
            
            scope.changekModelValue = function (val) {
                if (scope.ngDisabled)
                    return void[0];
                
                scope.ngModel.$viewValue = val;
                scope.ngModel.$commitViewValue();
                scope.ngModel.$setDirty();
            }
            
            scope.ngModel = ngModel;
        }
    }

})();