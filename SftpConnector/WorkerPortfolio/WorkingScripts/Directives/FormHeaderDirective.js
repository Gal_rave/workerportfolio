﻿(function () {
    angular.module('WorkerPortfolioApp').directive('formHeader', formHeader);

    formHeader.$inject = ['$state', '$rootScope', '$window', 'TofesDataService'];

    function formHeader($state, $rootScope, $window, TofesDataService) {
        var directive = {
            link: link,
            restrict: 'AE',
            templateUrl: '../../Scripts/views/ng-addons/progress-bar.component.html',
            replace: true,
            scope: true
        };
        return directive;

        function link(scope, element, attrs) {
            scope.states = scope.lsGet('states') || TofesDataService.GetComputedStates($state.get(), $state.current.name, parseInt(scope.$parent.tofesModel.FormType));

            scope.lsBind(scope, 'states', scope.states);

            scope.states.filter(function (s) {
                s.isCurrent = false;
                if (s.goTo === $state.current.name) {
                    s.isCurrent = true;
                    scope.current = s.position;
                }
            });

            scope.states.filter(function (s) {
                if (s.position < scope.current) {
                    s.clas = 'complete';
                }
                if (s.position === scope.current) {
                    s.clas = 'active';
                }
                if (s.position > scope.current) {
                    s.clas = s.isCompleted ? '' : 'disabled';
                }
            });

            scope.$step = function (goTo, index) {
                ///if current step do nuthing
                if (index === scope.current) {
                    return void [0];
                }
                ///if go-to is next then sent `true` to validate alse send `false`
                scope.publish('goto.step', goTo, index > scope.current);
            }

            scope.subscribe('$$states', function (data) {
                scope.states[scope.current].isCompleted = true;
            });
        }
    }

})();