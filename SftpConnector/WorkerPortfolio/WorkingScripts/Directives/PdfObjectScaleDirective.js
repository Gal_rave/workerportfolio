﻿(function () {

    angular.module('WorkerPortfolioApp').directive('pdfObjectScale', pdfObjectScale);

    pdfObjectScale.$inject = ['$window', '$compile', '$rootScope', '$state'];

    function pdfObjectScale($window, $compile, $rootScope, $state) {
        var helper = {
            detectBrowser: function () {
                var userAgent = $window.navigator.userAgent;
                var browsers = { chrome: /chrome/i, safari: /safari/i, firefox: /firefox/i, ie: /internet explorer/i };
                for (var key in browsers) {
                    if (browsers[key].test(userAgent)) {
                        return key;
                    }
                };
                if (parseInt(document.documentMode) > 0)
                    return 'IE';

                return 'undefined';
            },
            templateIE: '<div ng-hide="pdfDataStatus == 406"><iframe id="template" ng-src="{{ngUrl}}" width="100%" height="100%" style="width:100%;height:100%;min-height:{{minHeight}}px;"></iframe></div>',
            templateMobile: '<div ng-hide="pdfDataStatus == 406" template-mobile><img ng-src="data:image/png;base64,{{fileImageBlob}}" style="width:100%;height:100%;" /></div>',
            templateChrom: '<object id="template" ng-bind="contentView" ng-show="pdfShow" data="{{pdfContent}}#{{contentView}}" style="width:100%;height:100%;min-height:{{minHeight}}px;" type="application/pdf" title="654">\
                                    <p>\
                                        It appears you don`t have a PDF plugin for this browser.\
                                        No problem though...\
                                        You can <a href="{{ngUrl}}">click here to download the PDF</a>.\
                                    </p>\
                                </object>',
            currentTemplate: undefined,
            getCurrentTemplate: function () {
                if ($window.innerWidth < 801) {
                    helper.currentTemplate = helper.templateMobile;
                    return helper.currentTemplate;
                }
                switch (helper.detectBrowser().toLowerCase()) {
                    case 'ie':
                        helper.currentTemplate = helper.templateIE;
                        break;
                    default:
                        helper.currentTemplate = helper.templateChrom;
                        break;
                }
                return helper.currentTemplate;
            },
            browserType: function () {
                if ($window.innerWidth < 801)
                    return 5;

                switch (helper.detectBrowser().toLowerCase()) {
                    case 'ie':
                        return 1;
                    default:
                        return 0;
                }
            },
            setUpUrl: function (controllerState, browserType) {
                if (browserType == 0) {
                    return '';
                }
                var _url = '/home/' + (browserType == 5 ? 'Convert' : '');
                switch (controllerState.toLowerCase()) {
                    case 'paycheck':
                        _url += 'PaycheckResponse?';
                        break;
                    case '106form':
                        _url += 'AnnualFormResponse?';
                        break;
                }

                return _url;
            },
            randomizer: function () {
                return '&V=' + parseInt(Math.random() * 10000);
            }
        };
        var directive = {
            link: link,
            restrict: 'EA',
            replace: true,
            template: function () {
                return helper.currentTemplate || helper.getCurrentTemplate();
            },
            scope: {
                pdfContent: '=pdfContent',
                pdfShow: '=pdfShow',
                pdfDate: '=pdfDate',
                pdfControllerState: '=pdfControllerState',
                pdfDataStatus: '=pdfDataStatus',
                fileImageBlob: '=fileImageBlob',
            }
        };
        return directive;

        function link(scope, element, attrs) {
            scope.ngUrl = '';
            scope.doResize = true;
                                    
            scope.resetDoResize = function () {
                scope.doResize = true;
                var checkDate = scope.pdfControllerState === 'Paycheck' ? new Date(2015, 6, 31) : new Date(2014, 11, 31);
                if (scope.pdfDate < checkDate)
                    scope.doResize = false;
            }

            scope.Browser = helper.browserType();
            var credentials = typeof $rootScope.$$credentials !== 'undefined' ? $rootScope.$$credentials : $rootScope.lsGet('$$credentials');
            
            scope.ngUrl = helper.setUpUrl(scope.pdfControllerState, scope.Browser) + ['idNumber=' + credentials.idNumber, 'year=' + scope.pdfDate.getFullYear(), 'month=' + (scope.pdfDate.getMonth() + 1)].join('&') + helper.randomizer();
            
            scope.contentView = '';
            scope.$Width = $window.innerWidth;
            scope.$Height = $window.innerHeight;

            scope.$watch('pdfDate', function (newValue, oldValue) {
                if (oldValue === newValue || !oldValue || !newValue) {
                    return void [0];
                }
                
                scope.ngUrl = helper.setUpUrl(scope.pdfControllerState, scope.Browser) + ['idNumber=' + credentials.idNumber, 'year=' + scope.pdfDate.getFullYear(), 'month=' + (scope.pdfDate.getMonth() + 1)].join('&') + helper.randomizer();

                if (scope.Browser === 1) {
                    element.children().remove();
                    setTimeout(function () {
                        scope.$apply(function () {
                            var content = $compile(helper.currentTemplate)(scope);
                            element.append(content);
                        });
                        pdfView();
                    }, 125);
                }
            }, true);

            var scaleWidthSwitch = function (width) {
                var w = parseInt(width / 50) - 28;
                w = w < 0 ? 0 : (w > 12 ? 12 : w);
                w = w > 0 ? (103 + (5 * (w - 1))) : 0;
                return w;
            }
            var pdfView = function (scale) {
                if (typeof scale === 'undefined')
                    scale = 'view=Fit';

                var w = scaleWidthSwitch(scope.$Width);
                if (w <= 0)
                    scale = 'view=Fit';
                else if (scope.doResize)
                    scale = 'zoom=' + w;

                scope.contentView = scale;

                scope.minHeight = $window.innerWidth < 801 ? parseInt(scope.$Height / 1.15) : parseInt(scope.$Height * 1.15);
                if (scope.Browser === 1)
                    element.find('iframe').attr('style', 'width:100%;height:100%;min-height:' + scope.minHeight + 'px;');
            }

            $(window).on("resize.doResize", _.debounce(function () {
                scope.$Width = $window.innerWidth;
                scope.$Height = $window.innerHeight;
                if (scope.Browser === 1) {
                    scope.$apply(function () {
                        var content = $compile(helper.currentTemplate)(scope);
                        element.append(content);
                    });
                }
                pdfView();
            }, 150));

            scope.resetDoResize();
            pdfView();

            scope.subscribe('PdfContect.Changed', function (goTo) {
                scope.resetDoResize();
                pdfView();
            });
        }
    }
})();