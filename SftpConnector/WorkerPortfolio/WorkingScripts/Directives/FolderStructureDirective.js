﻿(function () {
    angular.module('WorkerPortfolioApp').directive('folderStructure', folderStructure);

    folderStructure.$inject = [];

    function folderStructure() {
        var directive = {
            link: link,
            restrict: 'EA',
            templateUrl: '../../Scripts/views/Email-Templates/folder-structure-pannel.html',
            replace: true,
            scope: {
                folderStruct: '=folderStruct',
                depthStruct: '=depthStruct',
                inputValidation: '=inputValidation',
                currentDragOver: '=currentDragOver'
            }
        };
        return directive;

        function link(scope, element, attrs) {
            scope.depth_struct = parseInt(scope.depthStruct) + 1;
            
            scope.selectFolder = function (selectedFolder) {
                selectedFolder.selected = true;
                selectedFolder.isSelected = true;
                scope.publish('UpdateSelectedFolder', selectedFolder);
            }
            scope.updateFolder = function (folder) {
                scope.publish('UpdateFolderName', folder);
            }
            scope.addFolder = function (folder) {
                scope.publish('AddNewFolder', folder);
            }

            scope.OnDropFile = function (data, $dragElement, $dropElement, event, dropData) {
                scope.publish('FileDroped', data, dropData);
            };
            scope.onDragOver = function (data, $dragElement, $dropElement, event, dropData) {
                scope.publish('dragOver', $dropElement, dropData);
            };
            scope.onDragLeave = function (data, $dragElement, $dropElement, event, dropData) {
                scope.publish('dragLeave', data, dropData);
            };
        }
    }

})();

