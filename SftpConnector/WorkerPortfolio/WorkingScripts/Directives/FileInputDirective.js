﻿(function () {
    angular.module('WorkerPortfolioApp').directive('uploader', function () {
        return {
            restrict: 'EA',
            link: function (scope, elem, attrs) {
                elem.attr('accept', '.jpg, .png, .gif, .tif, .jpeg, .pdf');
                elem.attr('title', 'גודל מקסימאלי 4MB\r `jpg, png, gif, tif, jpeg, pdf` - סוגי קבצים מותרים');
            }
        };
    });

})();