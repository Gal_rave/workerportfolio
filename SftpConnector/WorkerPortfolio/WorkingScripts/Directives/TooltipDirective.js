﻿(function() {
    

    angular.module('WorkerPortfolioApp').directive('tooltip', tooltip);

    tooltip.$inject = ['$window'];
    
    function tooltip($window) {
        var directive = {
            link: link,
            restrict: 'EA',
            replace: true,
            template: '<div class=popover>\
                            <div class=arrow></div>\
                            <h3 class="popover-title" ng-bind-html="title" ng-show="title"></h3>\
                            <div class="popover-content" ng-bind-html=content></div>\
                        </div>',
            scope: {
                title: "@title",
                content: "@content",
            },
        };
        return directive;

        function link(scope, element, attrs) {
            
        }
    }

})();


