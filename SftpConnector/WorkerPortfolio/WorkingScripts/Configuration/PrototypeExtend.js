﻿(function () {    
    String.prototype.isempty = function () {
        return this === null || this === undefined || typeof this === 'undefined' || this.length < 1;
    };
    Number.prototype.isempty = function () {
        return this === null || this === undefined || typeof this === 'undefined' || this.length < 1;
    };
    Number.prototype.roundTenUp = function () {
        return this % 10 === 0 ? parseInt(this) : ((parseInt(this / 10, 10) + 1) * 10);
    };
})();