﻿(function () {

    angular.module('WorkerPortfolioApp').config(['KeepaliveProvider', 'IdleProvider', function (KeepaliveProvider, IdleProvider) {
        ///for localhost set longer parameters
        var mul = location.hostname === "localhost" ? 1000 : 1;
        ///set the allowd idle time [5m]
        IdleProvider.idle(parseInt(300 * mul)); // in seconds
        ///set the worning timeout [45s]
        IdleProvider.timeout(parseInt(10 * mul)); // in seconds
        ///run check for login state every [1m]
        KeepaliveProvider.interval(parseInt(10 * mul)); // in seconds
    }]);

})();