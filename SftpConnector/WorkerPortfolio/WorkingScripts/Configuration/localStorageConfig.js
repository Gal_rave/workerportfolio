﻿(function () {
    
    angular.module('WorkerPortfolioApp').config(function (localStorageServiceProvider) {
        localStorageServiceProvider
            .setNotify(true, true);
    });

})();