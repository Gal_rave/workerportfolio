﻿(function() {
    angular.module('WorkerPortfolioApp').directive("ngSubmit", function () {
        return {
            require: "?form",
            priority: 10,
            link: {
                pre: function(scope, element, attrs, form) {
                    element.on("submit", function(event) {        
                        element.find('input[IDNumber]').each(function (i, e) {
                            $(e).trigger('blur');
                        });
                    })
                }
            }
        }
    });
})();