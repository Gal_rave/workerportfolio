﻿(function () {
    angular.module('WorkerPortfolioApp').directive('solutionVersionDirective', solutionVersionDirective);

    solutionVersionDirective.$inject = ['$window', '$timeout', '$cookies'];

    function solutionVersionDirective($window, $timeout, $cookies) {
        var directive = {
            link: link,
            restrict: 'A',
            scope: true,
        };
        return directive;

        function link(scope, element, attrs) {
            scope.subscribe('511_responseError', function (action) {
                $rootScope.lsClearAll();
                scope.GetSBVN();
            });

            scope.GetSBVN = function () {
                var sbvn = $rootScope.lsGet('$SolutionBuildVersionNumber');
                if (sbvn === null || sbvn == undefined || sbvn !== _SolutionBuildVersionNumber) {
                    $rootScope.lsClearAll();
                    var obj = $cookies.getAll();
                    $.map(obj, function (value, key) {
                        $cookies.remove(key);
                    });
                    var obj = $cookies.getAll();
                    $.map(obj, function (value, key) {
                        $cookies.remove(key);
                    });

                    $rootScope.lsSet('$SolutionBuildVersionNumber', _SolutionBuildVersionNumber);
                    scope.publish('CatchTransmitLogout', false, false);
                    window.location.reload(true);
                }
            }
            
            $timeout(function () {
                scope.GetSBVN();
            }, 15);

            scope.subscribe('$$SVDcall', function (start) {
                scope.GetSBVN();
            });
        }
    }

})();