﻿(function() {

    angular.module('WorkerPortfolioApp').directive('ngdate', NgDate);

    NgDate.$inject = ['$timeout'];
    
    function NgDate($timeout) {
        var directive = {
            link: link,
            restrict: 'A',
            require: "ngModel",
            scope: {},
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            var validDate = function (val, isRequired) {
                try {
                    if (isRequired !== true && (val == null || String(val).trim() == ''))
                        return true;

                    var validity = new RegExp(/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/);
                    val = String(val);
                    if (!validity.test(val))
                        return false;

                    return true;
                } catch (e) {
                    return false;
                }
            }

            scope.validate = function () {
                ngModel.$setValidity('custom', true);
                
                if (!validDate(ngModel.$modelValue, ngModel.$$attr.required)) {
                    element.addClass('ng-invalid-custom');
                    ngModel.$setValidity('custom', false);
                }
                scope.$apply();
            }

            element.on('focus', function () {
                ngModel.$setValidity('custom', true);
                element.removeClass('ng-invalid-custom');
            });

            element.on('blur', function () {
                $timeout(scope.validate, 200);
            });
        }
    }

})();