﻿(function() {
    angular.module('WorkerPortfolioApp').directive('password', Password);

    Password.$inject = ['$window', 'toastrService'];
    
    function Password($window, toastrService) {
        var keyCode = {
            32 : "רווח",
            81: "/",
            87: "'",
            187 : "+",
            191: "/?",
            111: "/",
            192: "` ~",
            193 : "?, / or °",
            220 : "\\",
            222 : "' \"",
            223 : "`",
        };
        var directive = {
            link: link,
            restrict: 'A',
            scope: {},
        };
        return directive;

        function link(scope, element, attrs) {
            var EN = new RegExp("^[a-zA-Z;<>.,]+$");
            element.on('keydown', function (e) {
                if ([32, 51, 81, 87, 111, 197, 191, 192, 220, 222, 223].indexOf(e.keyCode) > -1) {
                    switch (e.key.toLowerCase()) {
                        case 'q':
                            break;
                        case 'w':
                            break;
                        case '3':
                            break;
                        default:
                            toastrService.error('ערך לא חוקי ` XXX `'.replace('XXX', e.key), null, { timeOut: 5500, extendedTimeOut: 150, progressBar: false });
                            e.preventDefault();
                            return;
                            break;
                    }
                }
                if ((e.keyCode >= 65 && e.keyCode <= 90 && !EN.test(e.key)) || ([186, 188, 190].indexOf(e.keyCode) > -1 && !EN.test(e.key))) {
                    toastrService.error('יש להכניס אותיות לועזיות בלבד.', null, { timeOut: 5500, extendedTimeOut: 150, progressBar: false });
                    e.preventDefault();
                    return;
                }                
            });
        }
    }

})();