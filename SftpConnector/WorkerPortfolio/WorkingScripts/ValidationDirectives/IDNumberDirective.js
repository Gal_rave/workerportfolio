﻿(function() {

    angular.module('WorkerPortfolioApp').directive('idnumber', IDNumber);

    IDNumber.$inject = [];
    
    function IDNumber() {
        var directive = {
            link: link,
            restrict: 'A',
            require: "ngModel",
            scope: {},
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            var sumDigits = function (digits) {
                digits = String(digits).split('');
                var sum = 0;
                _.each(digits, function (elm) {
                    sum += Number(elm);
                });
                return sum;
            }
            ///validate is correct isreali id-number
            var validIDnumber = function (val) {
                try {
                    if (scope.requiredInput && (typeof val === 'undefined' || val === null || val.length == 0) || scope.InpDisabled)
                        return true;
                    var _val = String(val);
                    val = parseInt(val);
                    var validity = new RegExp(/^\d{5,9}$/);
                    if (isNaN(val) || !validity.test(_val) || _val.length < 9 || val <= 0)
                        return false;
                    while (_val.length < 9)
                        _val = '0' + _val;
                    var numbers = _val.split(''), checkDigit = numbers.splice(8, 1).join('');
                    var counter = 0, incNum;
                    for (var i in numbers) {
                        incNum = Number(numbers[i]) * ((i % 2) + 1);    ///multiply digit by 1 or 2
                        counter += sumDigits(incNum);                   ///sum the digits up and add to counter
                    }
                    ///check counter and checkDigit;
                    var res = ((counter.roundTenUp() % 10 == 0) && (counter.roundTenUp() === (Number(counter) + Number(checkDigit))));
                    return res;
                } catch (e) {
                    return false;
                }
            }
            
            element.on('focus', function () {
                scope.requiredInput = typeof ngModel.$validators.required === 'function';
                scope.InpDisabled = attrs.disabled;

                ngModel.$setValidity('custom', true);
                scope.$apply();
            });
                        
            element.on('blur', function () {
                ngModel.$setValidity('custom', true);
                scope.requiredInput = typeof ngModel.$validators.required === 'function';
                scope.InpDisabled = attrs.disabled;

                var value = ngModel.$modelValue;
                if (!validIDnumber(value)) {
                    ngModel.$setValidity('custom', false);
                }
                scope.$apply();
            });
        }
    }

})();