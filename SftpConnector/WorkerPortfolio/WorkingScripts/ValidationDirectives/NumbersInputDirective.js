﻿(function() {
    angular.module('WorkerPortfolioApp').directive('numbersinput', NumbersInput);

    NumbersInput.$inject = [];
    
    function NumbersInput() {
        var directive = {
            link: link,
            restrict: 'A',
            require: "ngModel"
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            var validateNumbersOnly = function (val) {
                if (typeof val === 'number' && !isNaN(val))
                    return false;
                if (typeof val === 'undefined' || val.toString().trim().isempty())
                    return true;
                val = val.toString().trim();
                return val.replace(/[0-9.-]/ig, '').length > 0 || val.replace(/[^-]/ig, '').length > 1 || val.replace(/[^.]/ig, '').length > 1;
            }
                        
            element.on('focus', function () {
                ngModel.$setValidity('custom', true);
            });

            element.on('blur', function () {
                ngModel.$setValidity('custom', true);
                
                if (validateNumbersOnly(ngModel.$modelValue)) {
                    ngModel.$setValidity('custom', false);
                }
                scope.$apply();
            });

        }
    }

})();