﻿(function() {
    angular.module('WorkerPortfolioApp').directive('phone', Phone);

    Phone.$inject = [];
    
    function Phone() {
        var directive = {
            link: link,
            restrict: 'A',
            require: "ngModel",
            scope: {},
        };
        return directive;

        function link(scope, element, attrs, ngModel) {            
            scope.IsRequired = typeof attrs.required !== 'undefined' && attrs.required === true

            var validPhonenumber = function (val) {
                try {
                    val = String(val);
                    if (!scope.IsRequired && val.trim() == '')
                        return true;

                    var validity = new RegExp(/^((\+972|972)|0)( |-)?([1-468-9]( |-)?\d{7}|(5|7)[0-9]( |-)?\d{7})$/);
                    if (!validity.test(val))
                        return false;

                    return true;
                } catch (e) {
                    return false;
                }
            }

            element.on('focus', function () {
                ngModel.$setValidity('custom', true);
            });

            element.on('blur', function () {
                ngModel.$setValidity('custom', true);
                var value = ngModel.$modelValue;
                if (!validPhonenumber(value)) {
                    ngModel.$setValidity('custom', false);
                }
                scope.$apply();
            });
        }
    }

})();