﻿module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-bower-task');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                sourceMap: true,
                mangle: false
            },
            customJS: {
                files: [{
                    expand: true,                           // Enable dynamic expansion.
                    cwd: 'WorkingScripts/Custom-Scripts/',  // Src matches are relative to this path.
                    src: '**/*.js',                         // Actual pattern(s) to match.
                    dest: 'Scripts/js',                      // Destination path prefix.
                    ext: '.min.js'
                }]
            },
            prod: {
                files: [{
                    expand: true,                           // Enable dynamic expansion.
                    cwd: 'WorkingScripts/',  // Src matches are relative to this path.
                    src: ['app.js', 'Configuration/*.js', 'Filters/*.js', ['Directives/HeaderDirective.js', 'Directives/*.js'], 'ValidationDirectives/*.js', 'services/*.js', 'controllers/*.js'],                         // Actual pattern(s) to match.
                    dest: 'Scripts/jsm',                      // Destination path prefix.
                    ext: '.js'
                }]
            }
        },

        copy: {
            font: {
                files: [
                    {
                        expand: true,
                        cwd: 'WorkingContent/Fonts/',
                        src: ["*.*", "**/*.*"],
                        dest: "Content/Fonts/",
                    },
                ]
            },
            img: {
                files: [
                    {
                        expand: true,
                        cwd: 'WorkingContent/Images/',
                        src: ["*.*", "**/*.*"],
                        dest: "Content/Images/",
                    },
                ]
            },
            pdf: {
                files: [
                    {
                        expand: true,
                        cwd: 'WorkingContent/PDFs/',
                        src: ["*.*", "**/*.*"],
                        dest: "Content/PDFs/",
                    },
                ]
            }
        },

        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'WorkingContent/Custom-Css/',
                    src: ['*.css', '!*.min.css', '**/*.css'],
                    dest: 'content/Css',
                    ext: '.css'
                }]
            },
            combine: {
                files: {
                    'Content/Css/output.min.css': ['Content/Css/*.css', '!Content/Css/*.min.css']
                }
            }
        },

        watch: {
            scripts: {
                files: ['WorkingScripts/**/*.js'],
                tasks: ['concat:dist'],
                options: {
                    livereload: 35731,
                }
            },
            html: {
                files: ['WorkingScripts/**/*.html'],
                tasks: ['htmlmin:compile'],
                options: {
                    livereload: 35731,
                }
            },
            css: {
                files: ['WorkingContent/**/*.css'],
                tasks: ['cssmin:target', 'cssmin:combine'],
                options: {
                    livereload: 35731,
                }
            }
        },
        
        concat: {
            options: {
                separator: '\n',
                sourceMap: true
            },
            dist: {
                src: ['WorkingScripts/app.js', 'WorkingScripts/Configuration/*.js', 'WorkingScripts/Filters/*.js', ['WorkingScripts/Directives/HeaderDirective.js', 'WorkingScripts/Directives/*.js'], 'WorkingScripts/ValidationDirectives/*.js', ['WorkingScripts/services/localStorageServiceExtenders.js', 'WorkingScripts/services/*.js'], 'WorkingScripts/controllers/*.js'],
                dest: 'Scripts/app.js',
            },
            prod: {
                src: ['Scripts/jsm/app.js', 'Scripts/jsm/Configuration/*.js', 'Scripts/jsm/Filters/*.js', ['Scripts/jsm/Directives/HeaderDirective.js', 'Scripts/jsm/Directives/*.js'], 'Scripts/jsm/ValidationDirectives/*.js', ['Scripts/jsm/services/localStorageServiceExtenders.js', 'Scripts/jsm/services/*.js'], 'Scripts/jsm/controllers/*.js'],
                dest: 'Scripts/app.js',
            }
        },

        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                expand: true,
                cwd: 'WorkingScripts/Ng-Views/',
                src: ['**/*.html'],
                dest: 'Scripts/Views/'
            },
            compile: {
                files: [
                        {
                            expand: true,     // Enable dynamic expansion.
                            cwd: 'WorkingScripts/Ng-Views/',      // Src matches are relative to this path.
                            src: ['**/*.html'], // Actual pattern(s) to match.
                            dest: 'Scripts/Views/',   // Destination path prefix.
                        }
                ],
                options: {
                    type: 'html',
                    preserveServerScript: true,
                    removeComments: true,
                    collapseWhitespace: true
                }
            }
        },

        clean: {
            Views: {
                src: ['Scripts/jsm/**/*', 'Scripts/Views/**/*', 'Content/**/*', 'Scripts/Js/**/*', 'Scripts/*.js', 'Scripts/*.js.map', 'Scripts/*.min.js']
            },
            prod: {
                src: ['Scripts/jsm/**/*']
            }
        },

    });


    grunt.registerTask('prod', ['clean:Views', 'copy', 'uglify:prod', 'concat:prod', 'cssmin:target', 'cssmin:combine', 'htmlmin:compile', 'uglify:customJS', 'clean:prod']);
    grunt.registerTask('Dev', ['clean:Views', 'copy', 'concat:dist', 'cssmin:target', 'cssmin:combine', 'htmlmin:compile', 'uglify:customJS', 'watch']);
    grunt.registerTask('Clean', ['clean:Views']);
};