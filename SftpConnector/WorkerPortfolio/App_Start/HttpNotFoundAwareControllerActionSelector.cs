﻿using System.Net;
using System.Web.Http;
using System.Web.Http.Controllers;
using WorkerPortfolio.Controllers;
using Logger;

namespace WorkerPortfolio.App_Start
{
    public class HttpNotFoundAwareControllerActionSelector : ApiControllerActionSelector
    {

        public HttpNotFoundAwareControllerActionSelector()
        {
        }
        public override HttpActionDescriptor SelectAction(HttpControllerContext controllerContext)
        {
            HttpActionDescriptor decriptor = null;
            try
            {
                decriptor = base.SelectAction(controllerContext);
            }
            catch (HttpResponseException ex)
            {
                ex.LogError(controllerContext);

                var code = ex.Response.StatusCode;
                if (code != HttpStatusCode.NotFound && code != HttpStatusCode.MethodNotAllowed)
                    throw;
                var routeData = controllerContext.RouteData;
                routeData.Values["action"] = "Handle404";
                IHttpController httpController = new ErrorsController();
                controllerContext.Controller = httpController;
                controllerContext.ControllerDescriptor = new HttpControllerDescriptor(controllerContext.Configuration, "Error", httpController.GetType());
                decriptor = base.SelectAction(controllerContext);
            }
            return decriptor;
        }
    }
}