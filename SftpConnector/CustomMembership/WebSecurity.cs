﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.Routing;
using System.Linq;
using System.Web.WebPages;
using System.Net;
using System.Collections.Generic;

using DataAccess.Models;
using DataAccess.Extensions;
using CustomMembership.Classes;
using DataAccess.Mappers;
using Mail;
using Logger;
using WebServiceProvider;
using DataAccess.BLLs;
using HttpCookieHandler;
using AsyncHelpers;
using System.Threading.Tasks;
using System.Configuration;
using DataAccess.Enums;

namespace CustomMembership
{
    /// <summary>
    /// MembershipException -> 113
    /// 102 -> ChangePasswordFromConfirmationToken
    /// 200 -> CheckInitialUser
    /// </summary>
    public static class WebSecurity
    {
        private static readonly string smsMsg = @"{0} {1} ,שלום רב !
קוד אימות לאתר תיק עובד באינטרנט הוא: {2}
אוטומציה החדשה";
        private static readonly string tokenMsg = @"{0} {1} ,שלום רב !
קוד אימות לעדכון פרטים הינו: {2}
אוטומציה החדשה";

        #region class metods
        internal static HttpContextBase Context
        {
            get { return new HttpContextWrapper(HttpContext.Current); }
        }
        internal static HttpRequestBase Request
        {
            get { return Context.Request; }
        }
        internal static HttpResponseBase Response
        {
            get { return Context.Response; }
        }
        internal static void VerifyProvider()
        {
            if (CustomMembershipProvider.IsInitialized)
                return;

            CustomMembershipProvider.Initialize("WebSecurity", null);
        }
        internal static bool IsDebugeMode
        {
            get { return ConfigurationManager.AppSettings["isDebugeMode"].ToBool(); }
        }
        #endregion

        #region public metods
        public static bool UserExists(string idNumber)
        {
            VerifyProvider();
            return CustomMembershipProvider.UserExists(idNumber);
        }
        public static int GetUserId(string idNumber)
        {
            VerifyProvider();
            return getUser(idNumber).ID;
        }
        public static int GetUserMunicipalityId(string idNumber)
        {
            VerifyProvider();
            return CustomMembershipProvider.GetUserDefaultMunicipality(idNumber);
        }
        public static bool IsCurrentUser(string idNumber)
        {
            VerifyProvider();
            return String.Equals(CurrentUserName, idNumber, StringComparison.OrdinalIgnoreCase);
        }
        public static bool IsConfirmed(string idNumber)
        {
            VerifyProvider();
            return getUser(idNumber).isConfirmed;
        }
        /// <summary>
        /// Make sure the user was authenticated
        /// </summary>
        public static void RequireAuthenticatedUser()
        {
            VerifyProvider();
            var user = Context.User;
            if (user == null || !user.Identity.IsAuthenticated)
            {
                Response.SetStatus(HttpStatusCode.Unauthorized);
            }
        }
        /// <summary>
        /// Make sure the user was authenticated
        /// </summary>
        /// <param name="userId"></param>
        public static void RequireUser(int userId)
        {
            VerifyProvider();
            if (!IsUserLoggedOn(userId))
            {
                Response.SetStatus(HttpStatusCode.Unauthorized);
            }
        }
        public static void RequireUser(string idNumber)
        {
            VerifyProvider();
            if (!String.Equals(CurrentUserName, idNumber, StringComparison.OrdinalIgnoreCase))
            {
                Response.SetStatus(HttpStatusCode.Unauthorized);
            }
        }
        public static void RequireRoles(params string[] roles)
        {
            VerifyProvider();
            foreach (string role in roles)
            {
                if (!Roles.IsUserInRole(CurrentUserName, role))
                {
                    Response.SetStatus(HttpStatusCode.Unauthorized);
                    return;
                }
            }
        }
        public static int CurrentUserId
        {
            get { return _User().Id; }
        }
        public static string CurrentUserName
        {
            get { return _User().idNUmber; }
        }
        public static int CurrentMunicipalityId
        {
            get { return _User().currentMunicipalityId; }
        }
        public static bool HasUserId
        {
            get { return _User().Id > 0; }
        }
        public static bool IsAuthenticated
        {
            get { return Request.IsAuthenticated; }
        }
        public static PasswordSettingsModel PasswordSettings
        {
            get
            {
                VerifyProvider();
                return new PasswordSettingsModel(CustomMembershipProvider.MinRequiredPasswordLength, CustomMembershipProvider.MinRequiredNonAlphanumericCharacters, CustomMembershipProvider.MinRequiredNumericCharacters);
            }
        }
        public static string DefaultSiteEmail
        {
            get
            {
                VerifyProvider();
                return CustomMembershipProvider.DefaultSiteEmail;
            }
        }
        public static int GetFactoryId
        {
            get { return _User().currentFactoryId; }
        }
        #endregion

        #region log in/out
        public static WebSecurityModel LoginFromToken(WebSecurityModel account)
        {
            try
            {
                ///check provider is initialized.
                VerifyProvider();
                //log out all users before start
                Logout(true);
                ///check if user is locked
                if (IsAccountLockedOut(account.userIdNumber))
                {
                    account.success = false;
                    account.isLocked = true;
                    account.lockTimeLeft = calcLockTime(account.userIdNumber);
                    account.exceptionId = 101;
                    return account;
                }
                GetLoginMunicipalityId(account.userIdNumber);
                updateAndSetUser(account, true);
                account.userCredentials = account.user.MapCredentials();
                CustomMembershipProvider.RegisterLoginLog(account.userCredentials);
                account.user = null;
            }
            catch (MembershipException ex)
            {
                account = new WebSecurityModel(ex.LogError());
                RemoveCookieAfterCryptographicError();
            }
            catch (Exception ex)
            {
                account = new WebSecurityModel(ex.LogError());
                RemoveCookieAfterCryptographicError();
            }
            return account;
        }
        public static WebSecurityModel Login(string idNumber, string password, bool persistCookie = false)
        {
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                ///check provider is initialized.
                VerifyProvider();
                //log out all users before start
                Logout(true);
                ///check if user is locked
                if (IsAccountLockedOut(idNumber))
                {
                    model.success = false;
                    model.isLocked = true;
                    model.lockTimeLeft = calcLockTime(idNumber);
                    model.exceptionId = 101;
                    return model;
                }
                model = CustomMembershipProvider.ValidateUser(idNumber, password);
                if (!model.success)
                    return model;
                GetLoginMunicipalityId(idNumber);
                setUser(model, persistCookie);

                model.userCredentials = model.user.MapCredentials();
                CustomMembershipProvider.RegisterLoginLog(model.userCredentials);
                model.user = null;
            }
            catch (MembershipException ex)
            {
                ex.LogError(new { idNumber = idNumber, password = password, persistCookie = persistCookie });
                model = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                ex.LogError(new { idNumber = idNumber, password = password, persistCookie = persistCookie });
                model = new WebSecurityModel(ex.LogError());
            }

            return model;
        }
        public static WebSecurityModel Login(CryptoModel cryptoModel)
        {
            cryptoModel.DecryptCryptoModel();
            return Login(cryptoModel.idNumber, cryptoModel.password);
        }
        public static void Logout(bool passLog = false)
        {
            VerifyProvider();

            FormsAuthentication.SignOut();

            CookieExtender.DeleteCookie(CookieExtender.GetApplication("SecureUserCookieName").ToString());
            CookieExtender.DeleteCookie(CookieExtender.GetApplication("SecureTicketExpirationCookieName").ToString());

            CustomPrincipal contextUser = null;
            try
            {
                contextUser = (CustomPrincipal)Context.User;
            }
            catch
            {
                contextUser = null;
            }
            if (contextUser != null && !passLog)
            {
                CustomMembershipProvider.RegisterLoginLog(contextUser.Id, contextUser.currentMunicipalityId, "logout");
            }
            Context.User = null;
        }
        #endregion

        #region password change
        public static WebSecurityModel ChangePassword(string idNumber, string currentPassword, string newPassword)
        {
            VerifyProvider();
            return CustomMembershipProvider.ChangePassword(idNumber, currentPassword, newPassword);
        }
        public static WebSecurityModel ChangePasswordFromConfirmationToken(string confirmationToken, string currentPassword, string newPassword)
        {
            VerifyProvider();
            WebSecurityModel model = new WebSecurityModel();
            try
            {
                UserModel user = CustomMembershipProvider.GetUserByConfirmationToken(confirmationToken);
                if (user == null)
                    throw new MembershipException("NullUserReferenceException", 102);

                if (IsAccountLockedOut(user.idNumber))
                {
                    model.success = false;
                    model.isLocked = true;
                    model.lockTimeLeft = calcLockTime(user.idNumber);
                    model.exceptionId = 101;
                    return model;
                }
                return CustomMembershipProvider.ChangePassword(user.idNumber, currentPassword, newPassword);
            }
            catch (MembershipException ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                model = new WebSecurityModel(ex.LogError());
            }
            return model;
        }
        #endregion

        #region password reset
        public static WebSecurityModel ResetPassword(string idNumber)
        {
            return ResetPassword(idNumber, 0);
        }
        public static WebSecurityModel ResetPassword(string idNumber, int tokenExpirationInMinutesFromNow)
        {
            VerifyProvider();
            WebSecurityModel model = CustomMembershipProvider.ResetPassword(idNumber);

            string template = string.Empty;
            UserCredentialsModel data = new UserCredentialsModel
            {
                confirmationToken = model.passwordVerificationToken,
                firstName = model.user.firstName,
                lastName = model.user.lastName
            };
            RouteData routedata = Request.RequestContext.RouteData;
            routedata.Values.Add("token", model.passwordVerificationToken);
            template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "PasswordReset", data);

            // save the mail notification
            saveMailNotification(model.user.email, DefaultSiteEmail, idNumber, string.Format("{0} {1}", model.user.firstName, model.user.lastName), "איפוס סיסמה", template, MailTypeEnum.PasswordReset);
            // send the mail
            MailSender.Send(model.user.email, template, false, "tikoved.co.il איפוס סיסמה", null, "", DefaultSiteEmail);

            return model;
        }
        public static WebSecurityModel UserResetPassword(string idNumber, bool mail, bool sms)
        {
            VerifyProvider();
            WebSecurityModel model = new WebSecurityModel();
            ///send reset mail
            if (mail)
            {
                model = CustomMembershipProvider.ResetPassword(idNumber);
                if (!model.success)
                    return model;
                if (string.IsNullOrWhiteSpace(model.user.email))
                {
                    model.exceptionId = 111;
                    model.exception = "NullEmailException";
                    model.success = false;
                    return model;
                }
                string template = string.Empty;
                UserCredentialsModel data = new UserCredentialsModel
                {
                    confirmationToken = model.passwordVerificationToken,
                    firstName = model.user.firstName,
                    lastName = model.user.lastName
                };
                RouteData routedata = Request.RequestContext.RouteData;
                routedata.Values.Add("token", model.passwordVerificationToken);
                template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "PasswordReset", data);

                // save the mail notification
                saveMailNotification(model.user.email, DefaultSiteEmail, idNumber, string.Format("{0} {1}", model.user.firstName, model.user.lastName), "איפוס סיסמה", template, MailTypeEnum.PasswordReset);
                // send the mail
                MailSender.Send(model.user.email, template, false, "tikoved.co.il איפוס סיסמה", null, "", DefaultSiteEmail);
            }
            ///send reset code to sms
            if (sms)
            {
                model = CustomMembershipProvider.GenerateSMSCode(idNumber);
                if (!model.success)
                    return model;
                if (string.IsNullOrWhiteSpace(model.user.cell))
                {
                    model.exceptionId = 112;
                    model.exception = "NullCellException";
                    model.success = false;
                    return model;
                }
                string msg = string.Format(smsMsg, model.user.firstName, model.user.lastName, model.user.membership.smsVerificationToken);
                SmsNotificationModel smsNotification = model.user.MapToSms(msg);
                SmsManagerProvider.SendSMS(smsNotification);
            }
            model.userCredentials = model.user.MapPartialCredentials();
            model.user = null;

            return model;
        }
        public static string GeneratePasswordResetToken(string idNumber)
        {
            VerifyProvider();
            WebSecurityModel model = CustomMembershipProvider.GeneratePasswordResetToken(idNumber);
            if (model.success)
                return model.passwordVerificationToken;
            return string.Empty;
        }
        public static string GeneratePasswordResetToken(string idNumber, int tokenExpirationInMinutesFromNow)
        {
            VerifyProvider();
            WebSecurityModel model = CustomMembershipProvider.GeneratePasswordResetToken(idNumber, tokenExpirationInMinutesFromNow);
            if (model.success)
                return model.passwordVerificationToken;
            return string.Empty;
        }
        public static WebSecurityModel ResetPasswordFromToken(string passwordResetToken, string newPassword, bool autoLogin = false)
        {
            VerifyProvider();
            WebSecurityModel model = CustomMembershipProvider.ResetPasswordWithToken(passwordResetToken, newPassword);

            if (model.success && autoLogin)
            {
                var loginrespons = Login(model.user.idNumber, newPassword, true);
                return loginrespons;
            }
            return model;
        }
        public static WebSecurityModel ResetPasswordFromSmsCode(PostTokenModel tokenModel)
        {
            VerifyProvider();
            WebSecurityModel model = CustomMembershipProvider.ResetPasswordCode(tokenModel.idNumber, tokenModel.Token, tokenModel.newPassword);
            if (model.success)
            {
                return Login(model.user.idNumber, tokenModel.newPassword, true);
            }
            return model;
        }
        public static int GetUserIdFromPasswordResetToken(string token)
        {
            VerifyProvider();
            return getUserFromPasswordResetToken(token).ID;
        }
        public static bool ResetPassword(string passwordResetToken, string newPassword)
        {
            VerifyProvider();
            WebSecurityModel model = CustomMembershipProvider.ResetPasswordWithToken(passwordResetToken, newPassword);
            return model.success;
        }
        #endregion

        #region Account
        public static WebSecurityModel ConfirmAccount(string accountConfirmationToken, bool autoLogin = true)
        {
            VerifyProvider();
            WebSecurityModel model = CustomMembershipProvider.ConfirmAccount(accountConfirmationToken);
            if (model.success && autoLogin)
            {
                return LoginFromToken(model);
            }
            return model;
        }
        public static WebSecurityModel CreateUserAndAccount(UserModel user, bool requireConfirmationToken = true)
        {
            VerifyProvider();
            WebSecurityModel model = CustomMembershipProvider.CreateUser(user, requireConfirmationToken);
            if (!requireConfirmationToken && model.success)
            {
                model = Login(user.idNumber, user.password);
            }
            else if (model.success)
            {
                string template = string.Empty;
                UserCredentialsModel data = new UserCredentialsModel
                {
                    confirmationToken = model.user.membership.confirmationToken,
                    firstName = model.user.firstName,
                    lastName = model.user.lastName
                };
                RouteData routedata = Request.RequestContext.RouteData;
                routedata.Values.Add("token", model.user.membership.confirmationToken);
                template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "NewUserRegister", data);

                // save the mail notification
                saveMailNotification(model.user.email, DefaultSiteEmail, user.idNumber, string.Format("{0} {1}", model.user.firstName, model.user.lastName), "הרשמה לאתר", template, MailTypeEnum.SiteRegisteration);
                // send the mail
                MailSender.Send(model.user.email, template, false, "tikoved.co.il הרשמה לאתר", null, "", DefaultSiteEmail);
            }
            return model;
        }
        public static WebSecurityModel CreateUserAndAccount(string idNumber, string password, object propertyValues = null, bool requireConfirmationToken = false)
        {
            VerifyProvider();
            UserModel user = new UserModel();
            user.idNumber = idNumber;
            user.password = password;
            user.PopulatObject<UserModel>(propertyValues);
            WebSecurityModel model = CustomMembershipProvider.CreateUser(user, requireConfirmationToken);
            if (!requireConfirmationToken && model.success)
            {
                model = Login(idNumber, password);
            }
            else if (model.success)
            {
                string template = string.Empty;
                UserCredentialsModel data = new UserCredentialsModel
                {
                    confirmationToken = model.user.membership.confirmationToken,
                    firstName = model.user.firstName,
                    lastName = model.user.lastName
                };
                RouteData routedata = Request.RequestContext.RouteData;
                routedata.Values.Add("token", model.user.membership.confirmationToken);
                template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "NewUserRegister", data);

                // save the mail notification
                saveMailNotification(model.user.email, DefaultSiteEmail, user.idNumber, string.Format("{0} {1}", model.user.firstName, model.user.lastName), "הרשמה לאתר", template, MailTypeEnum.SiteRegisteration);
                // send the mail
                MailSender.Send(model.user.email, template, false, "tikoved.co.il הרשמה לאתר", null, "", DefaultSiteEmail);
            }
            return model;
        }
        public static WebSecurityModel CreateUserAndAccount(CreateUserModel user, bool requireConfirmationToken = true)
        {
            VerifyProvider();
            WebSecurityModel model = CustomMembershipProvider.CreateUser(user, requireConfirmationToken);
            if (!requireConfirmationToken && model.success)
            {
                model = Login(user.idNumber, user.password);
            }
            else if (model.success)
            {
                string template = string.Empty;
                UserCredentialsModel data = new UserCredentialsModel
                {
                    confirmationToken = model.user.membership.confirmationToken,
                    firstName = model.user.firstName,
                    lastName = model.user.lastName
                };
                RouteData routedata = Request.RequestContext.RouteData;
                routedata.Values.Add("token", model.user.membership.confirmationToken);
                template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "NewUserRegister", data);

                // save the mail notification
                saveMailNotification(model.user.email, DefaultSiteEmail, user.idNumber, string.Format("{0} {1}", model.user.firstName, model.user.lastName), "הרשמה לאתר", template, MailTypeEnum.SiteRegisteration);
                // send the mail
                MailSender.Send(model.user.email, template, false, "tikoved.co.il הרשמה לאתר", null, "", DefaultSiteEmail);
            }
            return model;
        }
        /// <summary>
        /// for bulk user register
        /// NO VALIDATION
        /// </summary>
        /// <param name="registerModel"></param>
        /// <returns></returns>
        public static WebSecurityModel InternalRegisterFromSystem(RegisterUserModel registerModel)
        {
            VerifyProvider();
            WebSecurityModel securityModel = new WebSecurityModel();
            RegisterBll registerBll = new RegisterBll();
            try
            {
                if (!IdNumberExtender.validateIDNumber(registerModel.idNumber))
                    throw new MembershipException("InvalidIDNumber", 105);

                if (!registerBll.CheckRegisterUser(registerModel))
                    throw new MembershipException("CheckRegisterUserException", 112); ;

                UserModel userModel = new UserModel
                {
                    idNumber = registerModel.idNumber,
                    calculatedIdNumber = registerModel.calculatedIdNumber,
                    cell = registerModel.cell,
                    email = registerModel.email,
                };

                EmployeeDada employee = SystemUsersProvider.GetPersonalInformation(registerModel.calculatedIdNumber, registerModel.municipality.rashut.Value);
                if (employee != null)
                {
                    userModel.Merge(employee.MapUser());
                }
                else
                    throw new MembershipException("InvalidReturnDataException", 106);

                userModel.password = randomizedPassword();
                securityModel = CustomMembershipProvider.InternalCreateUserFromSystem(userModel);
            }
            catch (MembershipException ex)
            {
                securityModel = new WebSecurityModel(ex.LogError());
                Log.LogError(registerModel);
            }
            catch (Exception ex)
            {
                ex.LogError(registerModel);
                securityModel = new WebSecurityModel(ex);
            }
            return securityModel;
        }
        public static WebSecurityModel RegisterFromInitialPassword(RegisterUserModel registerModel)
        {
            VerifyProvider();
            WebSecurityModel securityModel = new WebSecurityModel();
            RegisterBll registerBll = new RegisterBll();
            try
            {
                if (!IdNumberExtender.validateIDNumber(registerModel.idNumber))
                    throw new MembershipException("InvalidIDNumber", 105);

                List<SystemUserModel> userAndMunicipalities = registerBll.RegisterInsertedIntoUsers(registerModel);
                if (userAndMunicipalities == null || userAndMunicipalities.Count < 1)
                    throw new MembershipException("NullSystemUserException", 104);

                registerBll.AttachMunicipalities(userAndMunicipalities);
                UserModel userModel = userAndMunicipalities.Map();
                bool Merge = false;

                userModel.municipalities.ForEach(m =>
                {
                    EmployeeDada employee = SystemUsersProvider.GetPersonalInformation(userModel.calculatedIdNumber, m.rashut.ToInt());
                    if (employee != null)
                    {
                        userModel.Merge(employee.MapUser());
                        Merge = true;
                    }
                });

                if (!Merge)
                    throw new MembershipException("InvalidReturnDataException", 106);

                if (string.IsNullOrWhiteSpace(registerModel.initialPassword) || !userAndMunicipalities.Select(u => u.initialPassword).ToList().Contains(registerModel.initialPassword))
                    throw new MembershipException("initialPasswordException", 108);

                userModel.password = registerModel.password;
                securityModel = CustomMembershipProvider.CreateUserFromSystem(userModel, false);
                if (securityModel.success)
                {
                    securityModel = Login(registerModel.idNumber, registerModel.password);
                }
            }
            catch (MembershipException ex)
            {
                securityModel = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                securityModel = new WebSecurityModel(ex.LogError());
            }
            return securityModel;
        }
        public static WebSecurityModel RegisterFromSms(RegisterUserModel registerModel)
        {
            VerifyProvider();
            WebSecurityModel securityModel = new WebSecurityModel();
            RegisterBll registerBll = new RegisterBll();
            try
            {
                if (!IdNumberExtender.validateIDNumber(registerModel.idNumber))
                    throw new MembershipException("InvalidIDNumber", 105);

                List<SystemUserModel> userAndMunicipalities = registerBll.RegisterInsertedIntoUsers(registerModel);
                if (userAndMunicipalities == null || userAndMunicipalities.Count < 1)
                    throw new MembershipException("NullSystemUserException", 104);

                registerBll.AttachMunicipalities(userAndMunicipalities);
                UserModel userModel = userAndMunicipalities.Map();
                bool Merge = false;

                userModel.municipalities.ForEach(m =>
                {
                    EmployeeDada employee = SystemUsersProvider.GetPersonalInformation(userModel.calculatedIdNumber, m.rashut.ToInt());
                    if (employee != null)
                    {
                        userModel.Merge(employee.MapUser());
                        Merge = true;
                    }
                });

                if (!Merge)
                    throw new MembershipException("InvalidReturnDataException", 106);

                if (string.IsNullOrWhiteSpace(registerModel.cell) || !userAndMunicipalities.Select(u => u.cellNumber).ToList().Contains(registerModel.cell))
                    throw new MembershipException("CellNumberException", 109);

                userModel.password = registerModel.password;
                securityModel = CustomMembershipProvider.CreateUserFromSystem(userModel, true);
                if (securityModel.success)
                {
                    ///send sms varification code
                    securityModel = CustomMembershipProvider.GenerateSMSCode(registerModel.idNumber);
                    string msg = string.Format(smsMsg, securityModel.user.firstName, securityModel.user.lastName, securityModel.user.membership.smsVerificationToken);
                    SmsNotificationModel smsNotification = securityModel.user.MapToSms(msg);
                    SmsManagerProvider.SendSMS(smsNotification);
                }
            }
            catch (MembershipException ex)
            {
                securityModel = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                securityModel = new WebSecurityModel(ex.LogError());
            }
            return securityModel;
        }
        public static WebSecurityModel VerifyAccountFromSmsToken(RegisterUserModel model)
        {
            WebSecurityModel securityModel = new WebSecurityModel();
            try
            {
                securityModel = CustomMembershipProvider.ConfirmAccount(model.idNumber, model.initialPassword);
                if (securityModel.success)
                {
                    return LoginFromToken(securityModel);
                }
            }
            catch (MembershipException ex)
            {
                securityModel = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                securityModel = new WebSecurityModel(ex.LogError());
            }
            return securityModel;
        }
        public static WebSecurityModel RegisterFromEmail(RegisterUserModel registerModel)
        {
            VerifyProvider();
            WebSecurityModel securityModel = new WebSecurityModel();
            RegisterBll registerBll = new RegisterBll();
            try
            {
                if (!IdNumberExtender.validateIDNumber(registerModel.idNumber))
                    throw new MembershipException("InvalidIDNumber", 105);

                List<SystemUserModel> userAndMunicipalities = registerBll.RegisterInsertedIntoUsers(registerModel);
                if (userAndMunicipalities == null || userAndMunicipalities.Count < 1)
                    throw new MembershipException("NullSystemUserException", 104);

                registerBll.AttachMunicipalities(userAndMunicipalities);
                UserModel userModel = userAndMunicipalities.Map();
                bool Merge = false;

                userModel.municipalities.ForEach(m =>
                {
                    EmployeeDada employee = SystemUsersProvider.GetPersonalInformation(userModel.calculatedIdNumber, m.rashut.ToInt());
                    if (employee != null)
                    {
                        userModel.Merge(employee.MapUser());
                        Merge = true;
                    }
                });

                if (!Merge)
                    throw new MembershipException("InvalidReturnDataException", 106);

                if (string.IsNullOrWhiteSpace(registerModel.email) || !userAndMunicipalities.Select(u => u.email).ToList().Contains(registerModel.email))
                    throw new MembershipException("EmailException", 110);

                userModel.password = registerModel.password;
                securityModel = CustomMembershipProvider.CreateUserFromSystem(userModel, true);
                if (securityModel.success)
                {
                    ///send mail varification token
                    string template = string.Empty;
                    UserCredentialsModel data = new UserCredentialsModel
                    {
                        confirmationToken = securityModel.user.membership.confirmationToken,
                        firstName = securityModel.user.firstName,
                        lastName = securityModel.user.lastName
                    };
                    RouteData routedata = Request.RequestContext.RouteData;
                    routedata.Values.Add("token", securityModel.user.membership.confirmationToken);
                    template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "NewUserRegister", data);

                    // save the mail notification
                    saveMailNotification(securityModel.user.email, DefaultSiteEmail, registerModel.idNumber, string.Format("{0} {1}", securityModel.user.firstName, securityModel.user.lastName), "הרשמה לאתר", template, MailTypeEnum.SiteRegisteration);
                    // send the mail
                    MailSender.Send(securityModel.user.email, template, false, "tikoved.co.il הרשמה לאתר", null, "", DefaultSiteEmail);
                }
            }
            catch (MembershipException ex)
            {
                securityModel = new WebSecurityModel(ex.LogError());
            }
            catch (Exception ex)
            {
                securityModel = new WebSecurityModel(ex.LogError());
            }
            return securityModel;
        }
        #endregion

        #region user groups

        #endregion

        #region general purpose metods
        public static bool CheckAuthorizetion(WSPostExtensionModel model, bool autoLogIn = false)
        {
            if (model.isEncrypted)
            {
                model.DecryptWSPostModel();
                model.isEncrypted = false;
            }
            return CheckAuthorizetion((WSPostModel)model, autoLogIn);
        }
        public static bool CheckAuthorizetion(WSPostModel model, bool autoLogIn = false)
        {
            if (model.isEncrypted)
            {
                model.DecryptWSPostModel();
            }

            UserModel user = null;
            if (string.IsNullOrWhiteSpace(model.idNumber) && !string.IsNullOrWhiteSpace(model.confirmationToken))
            {
                user = CustomMembershipProvider.GetUserByConfirmationToken(model.confirmationToken);
                if (user == null)
                    throw new FieldAccessException("UserdetailsAccessException");
                model.idNumber = user.idNumber;
            }
            if (IsCurrentUser(model.idNumber))
                return true;
            if (user == null) user = CustomMembershipProvider.GetUserByConfirmationToken(model.confirmationToken);
            if (user == null)
                throw new FieldAccessException("UserdetailsAccessException");
            if (!autoLogIn)
                throw new FieldAccessException("UserdetailsAccessException");
            WebSecurityModel securityModel = new WebSecurityModel();
            securityModel.user = user;
            securityModel.userId = user.ID;
            securityModel.userIdNumber = user.idNumber;
            securityModel = LoginFromToken(securityModel);
            return securityModel.success;
        }
        public static bool CheckAuthorizetion(string idNumber, string confirmationToken, bool autoLogIn = false)
        {
            UserModel user = null;
            if (string.IsNullOrWhiteSpace(idNumber) && !string.IsNullOrWhiteSpace(confirmationToken))
            {
                user = CustomMembershipProvider.GetUserByConfirmationToken(confirmationToken);
                if (user == null)
                    throw new FieldAccessException("UserdetailsAccessException");
                idNumber = user.idNumber;
            }
            if (IsCurrentUser(idNumber))
                return true;
            if (user == null) user = CustomMembershipProvider.GetUserByConfirmationToken(confirmationToken);
            if (user == null)
                throw new FieldAccessException("UserdetailsAccessException");
            if (!autoLogIn)
                throw new FieldAccessException("UserdetailsAccessException");
            WebSecurityModel securityModel = new WebSecurityModel();
            securityModel.user = user;
            securityModel.userId = user.ID;
            securityModel.userIdNumber = user.idNumber;
            securityModel = LoginFromToken(securityModel);
            return securityModel.success;
        }
        public static bool CheckAdminAuthorizetion(UserSearchModel model)
        {
            model.DecryptWSPostModel();

            if (!string.IsNullOrWhiteSpace(model.requiredRole))
                return CheckAdminAuthorizetion(model.idNumber, model.confirmationToken, model.requiredRole);

            return CheckAdminAuthorizetion(model.idNumber, model.confirmationToken);
        }
        public static bool CheckAdminAuthorizetion(WSPostModel model)
        {
            if (model.isEncrypted)
            {
                model.DecryptWSPostModel();
            }
            if (!string.IsNullOrWhiteSpace(model.requiredRole))
                return CheckAdminAuthorizetion(model.idNumber, model.confirmationToken, model.requiredRole);

            return CheckAdminAuthorizetion(model.idNumber, model.confirmationToken);
        }
        public static bool CheckAdminAuthorizetion(string idNumber, string confirmationToken)
        {
            if (!CheckAuthorizetion(idNumber, confirmationToken))
                throw new AccessViolationException("AccessViolationException");
            return CustomRoleProvider.HasPermission(idNumber, "ADMIN");
        }
        public static bool CheckAdminAuthorizetion(string idNumber, string confirmationToken, string groupName)
        {
            if (!CheckAuthorizetion(idNumber, confirmationToken))
                throw new AccessViolationException("AccessViolationException");
            return CustomRoleProvider.HasSpecificPermission(idNumber, groupName);
        }
        public static int GetMaxImmunity(string idNumber)
        {
            return CustomRoleProvider.GetUserMaxImmunity(idNumber);
        }
        public static bool IsAccountLockedOut(string idNumber)
        {
            VerifyProvider();
            return calcLockTime(idNumber) > 0;
        }
        public static int GetPasswordFailuresSinceLastSuccess(string idNumber)
        {
            VerifyProvider();

            return calcLockTime(idNumber, true);
        }
        public static DateTime GetCreateDate(string idNumber)
        {
            VerifyProvider();
            return getUser(idNumber).createdDate;
        }
        public static DateTime GetPasswordChangedDate(string idNumber)
        {
            VerifyProvider();

            DateTime? lastChange = CustomMembershipProvider.GetMembership(idNumber).passwordChangeDdate;
            return lastChange.HasValue ? lastChange.Value : DateTime.Now.AddYears(-7);
        }
        public static DateTime GetLastPasswordFailureDate(string idNumber)
        {
            VerifyProvider();

            DateTime? lastFailure = CustomMembershipProvider.GetMembership(idNumber).lastPasswordFailureDate;
            return lastFailure.HasValue ? lastFailure.Value : DateTime.Now.AddYears(-7);
        }
        #endregion

        #region Utility Methods
        public static WebSecurityModel SendTokenToUser(WSPostExtensionModel model)
        {
            WebSecurityModel securityModel = new WebSecurityModel();
            SendTokenToUser(model, securityModel, true);
            return securityModel;
        }
        public static void SendTokenToUser(WSPostExtensionModel model, WebSecurityModel securityModel, bool ins = false)
        {
            VerifyProvider();
            try
            {
                if (ins)
                {
                    securityModel = CustomMembershipProvider.GenerateSMSCode(model.idNumber);
                }
                else
                {
                    securityModel.user.membership = CustomMembershipProvider.GenerateSMSCode(model.idNumber).user.membership;
                }
                if (IsDebugeMode)
                {
                    securityModel.user.cell = "0545745179";
                    securityModel.user.phone = "0545745179";
                    securityModel.user.email = "galr@ladpc.co.il";
                }

                ///send token mail
                if (model.ToSendType == 2)
                {
                    string template = string.Empty;
                    UserCredentialsModel data = new UserCredentialsModel
                    {
                        confirmationToken = securityModel.user.membership.smsVerificationToken,
                        firstName = securityModel.user.firstName,
                        lastName = securityModel.user.lastName
                    };
                    RouteData routedata = Request.RequestContext.RouteData;
                    routedata.Values.Add("token", securityModel.user.membership.smsVerificationToken);
                    template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "UserTokenMailTemplate", data);

                    // save the mail notification
                    saveMailNotification(securityModel.user.email, DefaultSiteEmail, model.idNumber, string.Format("{0} {1}", securityModel.user.firstName, securityModel.user.lastName), "הרשמה לאתר", template, MailTypeEnum.SiteRegisteration);
                    // send the mail
                    MailSender.Send(securityModel.user.email, template, false, "tikoved.co.il קוד אימות", null, "", DefaultSiteEmail);
                }
                ///send token sms
                if (model.ToSendType == 1)
                {
                    string msg = string.Format(tokenMsg, securityModel.user.firstName, securityModel.user.lastName, securityModel.user.membership.smsVerificationToken);
                    SmsNotificationModel smsNotification = securityModel.user.MapToSms(msg);
                    SmsManagerProvider.SendSMS(smsNotification);
                }
                securityModel.success = true;
                securityModel.exceptionId = 0;
            }
            catch (Exception ex)
            {
                ex.LogError();
                securityModel.success = false;
                securityModel.exceptionId = 20;
            }
        }
        public static void SendWellcomeEmails()
        {
            RegisterBll registerBll = new RegisterBll();
            List<SpouseModel> newUsers = registerBll.GetNewUsers();
            List<decimal> usersToUpdate = new List<decimal>();

            Log.FormLog(new { users = newUsers.Count }, "SendWellcomeEmails");

            if (newUsers.Count == 0)
                return;
            foreach (SpouseModel user in newUsers)
            {
                ///send mail to new user
                try
                {
                    string template = string.Empty;
                    UserCredentialsModel data = new UserCredentialsModel
                    {
                        firstName = user.firstName,
                        lastName = user.lastName,
                        fullName = string.Format("{0} {1}", user.firstName, user.lastName),
                        idNumber = user.idNumber,
                        loginDate = DateTime.Now,
                        calculatedIdNumber = user.calculatedIdNumber,
                        municipalities = user.municipalities
                    };
                    data.confirmationToken = user.incomeType;
                    RouteData routedata = new RouteData();

                    // send the mail
                    if (!IsDebugeMode)
                    {
                        template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "UserRegistretionMail", data);

                        ///save the mail notification
                        saveMailNotification(user.email, DefaultSiteEmail, user.idNumber, string.Format("{0} {1}", user.firstName, user.lastName), "הרשמה למערכת", template, MailTypeEnum.SystemRegisteration);
                        // send the mail
                        MailSender.Send(user.email, template, false, "הרשמה למערכת 'תיק עובד באינטרנט'", null, "הרשמה למערכת 'תיק עובד באינטרנט'", DefaultSiteEmail);
                    }
                    Log.FormLog(user.email, "SendWellcomeEmails");
                    usersToUpdate.Add(user.ID);

                    if (usersToUpdate.Count > 15)
                    {
                        AsyncFunctionCaller.RunAsyncAwait(() =>
                        {
                            registerBll.ChengeNewUserState(usersToUpdate);
                        });
                        usersToUpdate.Clear();
                    }

                }
                catch (Exception ex)
                {
                    ex.LogError();
                }
                finally
                {
                    Task.Delay(125);
                }
            }
            if (usersToUpdate.Count > 0)
                AsyncFunctionCaller.RunAsyncAwait(() =>
                {
                    registerBll.ChengeNewUserState(usersToUpdate);
                    Task.Delay(3456);
                });

            SendWellcomeEmails();
        }
        public static int GetSetCurrentMunicipality(int? municipalityId = null)
        {
            if (municipalityId.HasValue && municipalityId.Value > 0)
            {
                return setCurrentMunicipalityId(municipalityId.Value, CurrentUserName);
            }
            return GetLoginMunicipalityId(CurrentUserName);
        }
        public static int GetLoginMunicipalityId(string idNumber = null)
        {
            int MunicipalityId = getCurrentMunicipalityId(idNumber);
            if (MunicipalityId < 0)
            {
                return setCurrentMunicipalityId(GetUserMunicipalityId(idNumber), idNumber);
            }
            return MunicipalityId;
        }
        public static WebSecurityModel updateAndSetUser(WSPostModel postModel)
        {
            WebSecurityModel model = new WebSecurityModel();
            model.userIdNumber = postModel.idNumber;
            model.user = CustomMembershipProvider.GetUser(postModel.idNumber);

            EmployeeDada employeeDada = SystemUsersProvider.GetPersonalInformation(model.user.calculatedIdNumber, getCurrentRashutId(postModel, model.user.municipalities));
            model.user = CustomMembershipProvider.UpdateUserDataFromWS(employeeDada, model.user);

            model.userCredentials = model.user.MapCredentials();
            model.userId = model.user.ID;
            _User(model, true);
            return model;
        }
        public static void GenerateFailLoginCookie(string idNumber, int num)
        {
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                     1,
                     idNumber,
                     DateTime.Now,
                     DateTime.Now.AddYears(7),
                     false,
                     num.ToString());
            string encTicket = FormsAuthentication.Encrypt(authTicket);
            CookieExtender.SetCookie(idNumber, encTicket, true);
        }

        private static void RemoveCookieAfterCryptographicError()
        {
            try
            {
                Request.Cookies.Clear();
                Response.Cookies.Clear();
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
        }
        private static int calcLockTime(string idNumber, bool returnCounter = false)
        {
            HttpCookie cookie = CookieExtender.GetCookie(idNumber, true);
            if (cookie == null || string.IsNullOrWhiteSpace(cookie.Value))
                return 0;
            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(cookie.Value);
            if (authTicket == null || string.IsNullOrWhiteSpace(authTicket.UserData))
                return 0;
            if (returnCounter)
                return authTicket.UserData.ToInt();
            if (authTicket.UserData.ToInt() >= CustomMembershipProvider.MaxInvalidPasswordAttempts && !authTicket.Expired)
            {
                TimeSpan span = authTicket.IssueDate.AddMinutes(CustomMembershipProvider.UserLockInterval).Subtract(DateTime.Now);
                return span.Minutes > 0 ? span.Minutes : 0;
            }

            return 0;
        }
        private static bool IsUserLoggedOn(int userId)
        {
            VerifyProvider();
            return CurrentUserId == userId && CurrentUserId > 0;
        }
        private static UserModel getUser(string idNumber)
        {
            UserModel user = CustomMembershipProvider.GetUser(idNumber);
            return user == null ? new UserModel() : user;
        }
        private static UserModel getUserFromPasswordResetToken(string passwordResetToken)
        {
            return CustomMembershipProvider.GetUserFromResetToken(passwordResetToken);
        }
        /// <summary>
        /// get the  custom principal user data;
        /// </summary>
        /// <returns></returns>
        private static CustomPrincipal _User()
        {
            try
            {
                var user = Context.User;
                if (user == null || !(user is CustomPrincipal)) user = new CustomPrincipal("");
                return (CustomPrincipal)user;
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        /// <summary>
        /// set the custom principal user data;
        /// </summary>
        /// <param name="user" type="UserModel"></param>
        private static void _User(WebSecurityModel model, bool persistCookie = true)
        {
            int lockTime = calcLockTime(model.userIdNumber);
            CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
            serializeModel.Id = model.userId;
            serializeModel.FirstName = model.user.firstName;
            serializeModel.LastName = model.user.lastName;
            serializeModel.idNUmber = model.userIdNumber;
            serializeModel.email = model.user.email;
            serializeModel.FaildLoginCount = model.failLoginCount;
            serializeModel.IsConfirmed = model.isConfirmed;
            serializeModel.IsLockedOut = lockTime > 0;
            serializeModel.LockedOutTime = lockTime;
            serializeModel.currentMunicipalityId = GetLoginMunicipalityId(model.userIdNumber);
            serializeModel.calculatedIdNumber = model.user.calculatedIdNumber.ToInt();
            serializeModel.currentFactoryId = CustomMembershipProvider.GetUserFactory(model.userId, serializeModel.currentMunicipalityId);

            string userData = serializeModel.ToJSON();
            var expirationTime = DateTime.Now.AddMinutes(CookieExtender.GetApplication("TicketExpirationTime").ToInt());

            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                     5,
                     model.userIdNumber,
                     DateTime.Now,
                     expirationTime,
                     false,
                     userData);

            string encTicket = FormsAuthentication.Encrypt(authTicket);
            CookieExtender.SetCookie(CookieExtender.GetApplication("SecureUserCookieName").ToString(), encTicket);

            Context.User = new CustomPrincipal(model.userIdNumber, serializeModel);

            CookieExtender.SetCookie(
                CookieExtender.GetApplication("SecureTicketExpirationCookieName").ToString(),
                CryptoJSDecryption.EncriptString(expirationTime.ToString("MM-dd-yyyy HH:mm:ss")),
                expirationTime.AddMinutes(1),
                !ConfigurationManager.AppSettings["isDebugeMode"].ToBool()
                );
        }
        private static void setUser(WebSecurityModel model, bool persistCookie = true)
        {
            _User(model, persistCookie);
            FormsAuthentication.SetAuthCookie(model.user.idNumber, persistCookie);
        }
        private static void updateAndSetUser(WebSecurityModel model, bool persistCookie = true)
        {

            EmployeeDada employeeDada = SystemUsersProvider.GetPersonalInformation(model.user.calculatedIdNumber, getCurrentRashutId(GetLoginMunicipalityId(model.user.idNumber)));
            model.user = CustomMembershipProvider.UpdateUserDataFromWS(employeeDada, model.user);
            _User(model, persistCookie);
            FormsAuthentication.SetAuthCookie(model.user.idNumber, persistCookie);
        }
        private static string randomizedPassword()
        {
            return CryptoJSDecryption.GenerateCrypto().key.Substring(0, 8);
        }
        private static int getCurrentMunicipalityId(string idNumber = null)
        {
            FormsAuthenticationTicket authTicket = CookieExtender.GetAuthenticationTicket(CookieExtender.GetApplication("SecureMunicipalityCookieName").ToString());
            if (authTicket == null || string.IsNullOrWhiteSpace(authTicket.UserData) || (string.IsNullOrWhiteSpace(idNumber) ? CurrentUserName : idNumber) != authTicket.Name)
            {
                return -1;
            }
            return authTicket.UserData.ToInt();
        }
        private static int setCurrentMunicipalityId(int municipalityId, string idNumber)
        {
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                     1,
                     idNumber,
                     DateTime.Now,
                     DateTime.Now.AddYears(7),
                     false,
                     municipalityId.ToString());
            string encTicket = FormsAuthentication.Encrypt(authTicket);
            CookieExtender.SetValue(CookieExtender.GetApplication("SecureMunicipalityCookieName").ToString(), encTicket);
            return municipalityId;
        }
        private static int getCurrentRashutId(WSPostModel postModel, List<MunicipalityModel> municipalities)
        {
            if (postModel.currentRashutId > 0)
                return postModel.currentRashutId;
            return municipalities.First(m => m.ID == postModel.currentMunicipalityId).rashut.Value;
        }
        private static int getCurrentRashutId(int municipalityId)
        {
            VerifyProvider();
            return CustomMembershipProvider.GetUserDefaultRashutId(municipalityId);
        }
        private static void saveMailNotification(string recipientEmail, string senderEmail, string userIdNumber, string userName, string subject, string text, MailTypeEnum type, bool includingFiles = false)
        {
            new MailBll().SaveNotification(
                new MAILNOTIFICATION
                {
                    SENDEREMAIL = senderEmail,
                    RECIPIENTEMAIL = recipientEmail,
                    USERIDNUMBER = userIdNumber,
                    USERNAME = userName,
                    SUBJECT = subject,
                    FREETEXT = text,
                    INCLUDINGFILES = includingFiles,
                    MAILTYPE = (int)type,
                    CREATEDDATE = DateTime.Now
                }
            );
        }
        #endregion
    }
}
