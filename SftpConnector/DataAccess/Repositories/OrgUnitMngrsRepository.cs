﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class OrgUnitMngrsRepository : EntityRepositoryBase<ORG_UNIT_MNGRS, int>, IOrgUnitMngrsRepository
    {
        public OrgUnitMngrsRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<ORG_UNIT_MNGRS> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.ORG_UNIT_MNGRS.Include(include).AsQueryable();
            }
        }
        public void SyncUnitMngrsvsUsers()
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                dbContext.UNIT_MNGRS_VS_USERS();
            }
        }
    }
}