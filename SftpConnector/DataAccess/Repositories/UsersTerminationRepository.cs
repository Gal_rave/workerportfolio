﻿using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;
using DataAccess.Extensions;

namespace DataAccess.Repositories
{
    class UsersTerminationRepository : EntityRepositoryBase<USERSTERMINATION, int>, IUsersTerminationRepository
    {
        public UsersTerminationRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<USERSTERMINATION> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.USERSTERMINATIONS.Include(include).AsQueryable();
            }
        }
        public int GetUserDefaultMunicipality(string idNumber)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                ObjectParameter MUNICIPALITYID = new ObjectParameter("MUNICIPALITY_ID", 0);
                var result = dbContext.GETUSERDEFAULTMUNICIPALITY(idNumber, MUNICIPALITYID);
                return MUNICIPALITYID.Value.ToInt();
            }
        }
    }
}
