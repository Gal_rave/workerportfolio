﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class StreetRepository : EntityRepositoryBase<STREET, int>, IStreetRepository
    {
        public StreetRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<STREET> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.STREETS.Include(include).AsQueryable();
            }
        }
    }
}
