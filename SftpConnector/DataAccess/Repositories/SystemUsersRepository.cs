﻿using System.Data.Entity;
using Catel.Data;
using System.Linq;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class SystemUsersRepository : EntityRepositoryBase<SYSTEM_USERS, int>, ISystemUsersRepository
    {
        public SystemUsersRepository(DbContext dbContext)
            : base(dbContext) { }
        public void InsertMunicipalitiesToUser(string idNumber)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                dbContext.MUNICIPALITIESFROM_SYSTEMUSERS(idNumber);
            }
        }
        public IQueryable<SYSTEM_USERS> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.SYSTEM_USERS.Include(include).AsQueryable();
            }
        }
    }
}
