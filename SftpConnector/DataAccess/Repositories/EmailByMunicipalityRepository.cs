﻿using System.Data.Entity;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;
using Catel.Data;
using System.Linq;

namespace DataAccess.Repositories
{
    public class EmailByMunicipalityRepository : EntityRepositoryBase<EMAILBYMUNICIPALITY, int>, IEmailByMunicipalityRepository
    {
        public EmailByMunicipalityRepository(DbContext dbContext)
            : base(dbContext) { }
        public void ConnectEmailByMunicipality()
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                dbContext.CONNECT_EMAILBYMUNICIPALITY();
            }
        }
        public void DeactivateMunicipality()
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                dbContext.DEACTIVATE_MUNICIPALITY_EPC();
            }
        }
        public IQueryable<EMAILBYMUNICIPALITY> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.EMAILBYMUNICIPALITies.Include(include).AsQueryable();
            }
        }
    }
}
