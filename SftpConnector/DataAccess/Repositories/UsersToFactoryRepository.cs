﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class UsersToFactoryRepository : EntityRepositoryBase<USERSTOFACTORy, int>, IUsersToFactoryRepository
    {
        public UsersToFactoryRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<USERSTOFACTORy> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.USERSTOFACTORIES.Include(include).AsQueryable();
            }
        }
    }
}
