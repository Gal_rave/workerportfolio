﻿using System.Data.Entity;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;
using System.Linq;
using Catel.Data;

namespace DataAccess.Repositories
{
    public class PayCheckUsersByMailRepository : EntityRepositoryBase<PAYCHECKUSERSBYMAIL, int>, IPayCheckUsersByMailRepository
    {
        public PayCheckUsersByMailRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<PAYCHECKUSERSBYMAIL> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.PAYCHECKUSERSBYMAILs.Include(include).AsQueryable();
            }
        }
        public void InsertGlobalMunicipalityPCUE(int municipalityid)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                dbContext.INSERT_GLOBAL_PCUE(municipalityid);
            }
        }
    }
}