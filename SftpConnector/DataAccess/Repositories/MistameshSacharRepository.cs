﻿using System.Data.Entity;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;
using Catel.Data;
using System.Linq;

namespace DataAccess.Repositories
{
    public class MistameshSacharRepository : EntityRepositoryBase<SACHAR_USERS, int>, IMistameshSacharRepository
    {
        public MistameshSacharRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<SACHAR_USERS> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.SACHAR_USERS.Include(include).AsQueryable();
            }
        }
        public void InsertSacharUsers()
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                dbContext.INSERT_SACHAR_USERS();
            }
        }
    }
}