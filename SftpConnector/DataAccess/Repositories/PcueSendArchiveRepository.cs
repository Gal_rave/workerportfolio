﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class PcueSendArchiveRepository : EntityRepositoryBase<PCUE_SENDARCHIVE, int>, IPcueSendArchiveRepository
    {
        public PcueSendArchiveRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<PCUE_SENDARCHIVE> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.PCUE_SENDARCHIVE.Include(include).AsQueryable();
            }
        }
    }
}