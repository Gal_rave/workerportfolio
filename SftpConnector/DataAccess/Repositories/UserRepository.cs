﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class UserRepository : EntityRepositoryBase<USER, int>, IUserRepository
    {
        public UserRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<USER> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.USERS.Include(include).AsQueryable();
            }
        }
        public void UpdatUsersFromSystem()
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                dbContext.UPDATUSERSFROMSYSTEM();
            }
        }
        public void RemoveEmptyUsers()
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                dbContext.REMOVEEMPTYUSERS();
            }
        }
    }
}