﻿using System.Data.Entity;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;
using System.Linq;
using Catel.Data;

namespace DataAccess.Repositories
{
    public class TerminationRepository : EntityRepositoryBase<TERMINATION, int>, ITerminationRepository
    {
        public TerminationRepository(DbContext dbContext)
            : base(dbContext) { }
    }
}
