﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class GroupsRepository : EntityRepositoryBase<GROUP, int>, IGroupsRepository
    {
        public GroupsRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<GROUP> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.GROUPS.Include(include).AsQueryable();
            }
        }
    }
}
