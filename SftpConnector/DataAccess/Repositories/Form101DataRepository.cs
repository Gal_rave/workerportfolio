﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    class Form101DataRepository : EntityRepositoryBase<FORM101DATA, int>, IForm101DataRepository
    {
        public Form101DataRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<FORM101DATA> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.FORM101DATA.Include(include).AsQueryable();
            }
        }
    }
}
