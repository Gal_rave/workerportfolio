﻿using System.Data.Entity;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;
using System.Linq;
using Catel.Data;

namespace DataAccess.Repositories
{
    public class HtmlTemplatesRepository : EntityRepositoryBase<HTMLTEMPLATE, int>, IHtmlTemplatesRepository
    {
        public HtmlTemplatesRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<HTMLTEMPLATE> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.HTMLTEMPLATES.Include(include).AsQueryable();
            }
        }
    }
}
