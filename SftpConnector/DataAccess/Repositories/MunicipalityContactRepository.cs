﻿using System.Data.Entity;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;
using System.Linq;
using Catel.Data;

namespace DataAccess.Repositories
{
    public class MunicipalityContactRepository : EntityRepositoryBase<MUNICIPALITYCONTACT, int>, IMunicipalityContactRepository
    {
        public MunicipalityContactRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<MUNICIPALITYCONTACT> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.MUNICIPALITYCONTACTs.Include(include).AsQueryable();
            }
        }
    }
}
