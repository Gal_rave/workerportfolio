﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class MismachimRepository : EntityRepositoryBase<MISMACHIM, int>, IMismachimRepository
    {
        public MismachimRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<MISMACHIM> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.MISMACHIMs.Include(include).AsQueryable();
            }
        }
    }
}
