﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class MailNotificationRepository : EntityRepositoryBase<MAILNOTIFICATION, int>, IMailNotificationRepository
    {
        public MailNotificationRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<MAILNOTIFICATION> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.MAILNOTIFICATIONS.Include(include).AsQueryable();
            }
        }
    }
}
