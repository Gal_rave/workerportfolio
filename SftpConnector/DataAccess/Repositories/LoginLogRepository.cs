﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class LoginLogRepository : EntityRepositoryBase<LOGINLOG, int>, ILoginLogRepository
    {
        public LoginLogRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<LOGINLOG> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.LOGINLOGs.Include(include).AsQueryable();
            }
        }
    }
}
