﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    class System_streetsRepository : EntityRepositoryBase<SYSTEM_STREETS, int>, ISystem_streetsRepository
    {
        public System_streetsRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<SYSTEM_STREETS> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.SYSTEM_STREETS.Include(include).AsQueryable();
            }
        }
        public void delete_systemstreets()
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                dbContext.DELETE_SYSTEMSTREETS();
            }
        }
        public void UpdateCitiesStreets()
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                dbContext.UPDATE_CITIES_STREETS();
            }
        }
    }
}
