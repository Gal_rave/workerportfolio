﻿using System.Data.Entity;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;
using System.Linq;
using Catel.Data;

namespace DataAccess.Repositories
{
    public class AcademicInstRepository : EntityRepositoryBase<ACADEMIC_INST, int>, IAcademicInstRepository
    {
        public AcademicInstRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<ACADEMIC_INST> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.ACADEMIC_INST.Include(include).AsQueryable();
            }
        }
    }
}