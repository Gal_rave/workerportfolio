﻿using System.Data.Entity;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;
using System.Linq;
using Catel.Data;

namespace DataAccess.Repositories
{
    public class ProcessRepository : EntityRepositoryBase<PROCESS, int>, IProcessRepository
    {
        public ProcessRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<PROCESS> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.PROCESSES.Include(include).AsQueryable();
            }
        }
    }
}
