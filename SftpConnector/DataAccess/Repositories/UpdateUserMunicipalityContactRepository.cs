﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class UpdateUserMunicipalityContactRepository : EntityRepositoryBase<UPDATEUSERMUNICIPALITYCONTACT, int>, IUpdateUserMunicipalityContactRepository
    {
        public UpdateUserMunicipalityContactRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<UPDATEUSERMUNICIPALITYCONTACT> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.UPDATEUSERMUNICIPALITYCONTACTs.Include(include).AsQueryable();
            }
        }
    }
}
