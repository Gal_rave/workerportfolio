﻿using DataAccess.Extensions;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public class EmployeeDada
    {
        /*EVENT 1*/
        public string RASHUT_ID { get; set; }
        public string ZEHUT_ID { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string IMMIGRATION_DATE { get; set; }
        public string BIRTH_DATE { get; set; }
        public string GENDER { get; set; }
        public string SUG_EZRACHUT { get; set; }
        public string FATHERS_NAME { get; set; }
        public string COUNTRY_OF_BIRTH_CODE { get; set; }
        public string COUNTRY_OF_BIRTH { get; set; }
        public string PREVIOUS_LAST_NAME { get; set; }
        public string PREVIOUS_FIRST_NAME { get; set; }

        /*EVENT 28*/
        public string HOUSE_NUMBER { get; set; }
        public string HOME_PHONE_PREFIX { get; set; }
        public string HOME_PHONE_NUMBER { get; set; }
        public string ADDITIONAL_PHONE_PREFIX { get; set; }
        public string ADDITIONAL_PHONE { get; set; }
        public string POSTAL_CODE { get; set; }
        public string SETTLEMENT_NAME { get; set; }
        public string STREET_NAME { get; set; }
        public string ORIGIN_SHEM_REHUV { get; set; }
        public string HOUSE_ENTRANCE_NUMBER { get; set; }
        public string APARTMENT_NUMBER { get; set; }
        public string KIBBUTZ_MEMBER { get; set; }
        public string PRIVATE_EMAIL { get; set; }
        public string WORK_EMAIL { get; set; }
        public string MAILBOX { get; set; }
        public string WORK_PHONE { get; set; }
        public string WORK_PHONE_EXTENSION { get; set; }
        public string STREET_NAME_LOCAL { get; set; }
        public string CITYID { get; set; }
        public string STREETID { get; set; }
        public string SHEM_REHUV { get; set; }
        public string SECONDARY_CITYID { get; set; }

        /*EVENT 6*/
        public string MARITAL_STATUS { get; set; }
        public string MARITAL_STATUS_DATE { get; set; }

        /*EVENT 78*/
        public string HMO_CODE { get; set; }

        /*EVENT 13*/
        public string PARTNER_JOB { get; set; }
        public string PARTNER_ID_NUMBER { get; set; }
        public string PARTNER_WORK_BOOL { get; set; }
        public string PARTNER_FIRST_NAME { get; set; }
        public string PARTNER_BIRTH_DATE { get; set; }
        public string PARTNER_PHONE_PREFIX { get; set; }
        public string PARTNER_PHONE_NUMBER { get; set; }
        public string PARTNER_PHONE_EXTENSION { get; set; }
        public string PARTNER_LAST_NAME { get; set; }

        public List<EducationData> educationData { get; set; }
        public List<ChildData> childrenData { get; set; }
    }
}
