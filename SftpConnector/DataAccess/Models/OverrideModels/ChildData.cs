﻿namespace DataAccess.Models
{
    public class ChildData
    {
        public string OVD_ILD_MISPAR_ZEHUT { get; set; }
        public string OVD_ILD_SHEM { get; set; }
        public string OVD_ILD_GAR_BABAIT { get; set; }
        public string OVD_ILD_T_LEDA { get; set; }
        public string OVD_ILD_MIN { get; set; }
    }
}
