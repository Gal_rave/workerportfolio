﻿namespace DataAccess.Models
{
    public class Form101ReportModel
    {
        public int ID { get; set; }
        public int Rashut { get; set; }
        public string Name { get; set; }
        public int Active { get; set; }
        public int Allow101 { get; set; }
        public int Form_Year { get; set; }
        public int Oks { get; set; }
        public int Not_Ok { get; set; }
    }
}
