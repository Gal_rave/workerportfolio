﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class SystemUserModel
    {
        public decimal ID { get; set; }
        public string idNumber { get; set; }
        public string initialPassword { get; set; }
        public string municipalityId { get; set; }
        public string mancol { get; set; }
        public string email { get; set; }
        public string kidomet { get; set; }
        public string cell { get; set; }
        public string cellNumber { get; set; }
        public string fullIdNumber { get; set; }
        public Nullable<int> terminationCause { get; set; }
        public Nullable<System.DateTime> terminationDate { get; set; }
        public Nullable<System.DateTime> commencementDate { get; set; }
        public string terminationDateString { get; set; }
        public MunicipalityModel municipality { get; set; }
        public Nullable<int> mifal { get; set; }
    }
}