//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MISHTAMSHEI_KVUZA
    {
        public decimal REC_ID { get; set; }
        public decimal KVUZA_ID { get; set; }
        public decimal RASHUT { get; set; }
        public decimal MIFAL { get; set; }
        public decimal FROM_DATE { get; set; }
        public decimal TO_DATE { get; set; }
        public decimal MEZAE_TNAY { get; set; }
        public string CREATION_TIME { get; set; }
        public string LAST_UPDATE_TIME { get; set; }
        public decimal LAST_UPDATE_USER { get; set; }
        public decimal STATUS { get; set; }
        public decimal MISHTAMESH_ID { get; set; }
    
        public virtual KVUZOT_MISHTAMSHIM KVUZOT_MISHTAMSHIM { get; set; }
    }
}
