﻿using System;

namespace DataAccess.Models
{
    public class MismachBereshimaModel
    {
        public int recId { get; set; }
        public int rashut { get; set; }
        public int mifal { get; set; }
        public int fromDate { get; set; }
        public int toDate { get; set; }
        public int reshimaId { get; set; }
        public int mismachId { get; set; }
        public int ofiMismachInd { get; set; }
        public int zmanHatraa { get; set; }
        public string creationTime { get; set; }
        public string lastUpdateTime { get; set; }
        public int lastUpdateUser { get; set; }
        public int status { get; set; }
        public MismachModel mismachModel { get; set; }
    }
}
