﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public class GetWorkerDataModel
    {
        public WSPostModel model { get; set; }
        public RepresentedWorkerModel worker { get; set; }
    }
}