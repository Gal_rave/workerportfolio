﻿using System;

namespace DataAccess.Models
{
    public class MishtamsheiKvuzaModel
    {
        public int recId { get; set; }
        public int kvuzaId { get; set; }
        public int mishtameshId { get; set; }
        public int rashut { get; set; }
        public int mifal { get; set; }
        public int fromDate { get; set; }
        public int toDate { get; set; }
        public int mezaheTnay { get; set; }
        public string creationTime { get; set; }
        public string lastUpdateTime { get; set; }
        public int lastUpdateUser { get; set; }
        public int status { get; set; }
    }
}
