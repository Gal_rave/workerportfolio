﻿using System;

namespace DataAccess.Models
{
    public class CourseFormFieldsModel
    {
        public string empFirstName { get; set; }
        public string empLastName { get; set; }
        public int empId { get; set; }
        public string empRole { get; set; }
        public string empDepartment { get; set; }
        public int empYearsOfExperiance { get; set; }
        public string coursePlan { get; set; }
        public string courseInstitution { get; set; }
        public int courseNumberOfDays { get; set; }
        public int courseNumberOfHours { get; set; }
    }
}