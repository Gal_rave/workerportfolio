﻿using System;

namespace DataAccess.Models
{
    public class CourseFormModel
    {
        public SelectedProcessModel selectedProcess { get; set; }
        public CourseFormFieldsModel formFields { get; set; }

        public string ovedNesuBakasha { get; set; }
        public string ovedNesuBakashaName { get; set; }
        public decimal tt_tahalich_id { get; set; }
        public decimal tt_shalav_id { get; set; }
        public decimal curr_mispar_shalav { get; set; }
        public decimal shlavim_counter { get; set; }

        public WSPostModel ovedModel { get; set; }
        public TargetUserModel targetUser { get; set; }
    }
}
