﻿using System;

namespace DataAccess.Models
{
    public class MistameshSacharModel
    {
        public int id { get; set; }
        public int? kodMedaveach { get; set; }
        public int rashut { get; set; }
        public UserModel user { get; set; }
    }
}