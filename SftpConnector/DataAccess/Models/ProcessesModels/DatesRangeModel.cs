﻿using System;

namespace DataAccess.Models
{
    public class DatesRangeModel
    {
        public string from { get; set; }
        public string to { get; set; }
    }
}