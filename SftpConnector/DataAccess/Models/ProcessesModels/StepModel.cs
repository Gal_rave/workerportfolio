﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public class StepModel
    {
        public int recId { get; set; }
        public int rashut { get; set; }
        public int mifal { get; set; }
        public int fromDate { get; set; }
        public int toDate { get; set; }
        public int tahalichId { get; set; }
        public int misparShalav { get; set; }
        public string shemShalav { get; set; }
        public int modulId { get; set; }
        public int rechivId { get; set; }
        public int kvuzatMishtamshim { get; set; }
        public int zmanMaxHours { get; set; }
        public int? reshimatTiyug { get; set; }
        public int hova { get; set; }
        public int tikufNetunimInd { get; set; }
        public int shalavAharonInd { get; set; }
        public int readOnlyInd { get; set; }
        public string creationTime { get; set; }
        public string lastUpdateTime { get; set; }
        public int lastUpdateUser { get; set; }
        public int status { get; set; }
        public int shalavMehuyavInd { get; set; }
        public int timeUnit { get; set; }
        public string notification { get; set; }
        public KvuzatMishtamshimModel kvuzatMishtamshimModel { get; set; }
        public ReshimatTiyugModel reshimatTiyugModel { get; set; }
    }
}
