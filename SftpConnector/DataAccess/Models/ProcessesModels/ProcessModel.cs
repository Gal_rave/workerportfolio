﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public class ProcessModel
    {
        public decimal ID { get; set; }
        public int RASHUT { get; set; }
        public int MIFAL { get; set; }
        public decimal USERID { get; set; }
        public int PROCESS_TYPE { get; set; }
        public string PROCESS_DESCRIPTION { get; set; }
        public string FROM_DATE { get; set; }
        public string TO_DATE { get; set; }
        public decimal TARGET_USERID { get; set; }
        public int STATUS { get; set; }
        public string CONTENT1 { get; set; }
        public string CONTENT2 { get; set; }
        public string CONTENT3 { get; set; }
        public string CONTENT4 { get; set; }
        public string CONTENT5 { get; set; }
        public string CONTENT6 { get; set; }
        public string CONTENT7 { get; set; }
        public string COMMENTS { get; set; }
        public string CREATION_DATE { get; set; }
        public string LAST_UPDATE { get; set; }
        public decimal? OVED_NESU_BAKASHA { get; set; }
        public string OVED_NESU_BAKASHA_NAME { get; set; }
        public decimal TT_TAHALICH_ID { get; set; }
        public decimal TT_SHALAV_ID { get; set; }
        public decimal TM_TAHALICH_ID { get; set; }
        public decimal CURR_MISPAR_SHALAV { get; set; }
        public decimal SHLAVIM_COUNTER { get; set; }
        public UserModel USERMODEL { get; set; }
        public UserModel TARGET_USER { get; set; }
    }
}