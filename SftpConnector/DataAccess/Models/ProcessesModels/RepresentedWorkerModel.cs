﻿using System;

namespace DataAccess.Models
{
    public class RepresentedWorkerModel
    {
        public int idNumber { get; set; }
        public string email { get; set; }
        public string fullName { get; set; }
    }
}