﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public class TahalichModel
    {
        public int recId { get; set; }
        public int rashut { get; set; }
        public int mifal { get; set; }
        public int fromDate { get; set; }
        public int toDate { get; set; }
        public int tahalichType { get; set; }
        public string shemTahalich { get; set; }
        public int zmanMaxHours { get; set; }
        public int owner { get; set; }
        public string creationTime { get; set; }
        public string lastUpdateTime { get; set; }
        public int lastUpdateUser { get; set; }
        public int status { get; set; }
        public int timeUnit { get; set; }
        public List<StepModel> shlavim { get; set; }
    }
}
