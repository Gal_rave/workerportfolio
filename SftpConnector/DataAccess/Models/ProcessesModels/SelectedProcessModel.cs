﻿using System;

namespace DataAccess.Models
{
    public class SelectedProcessModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public int type { get; set; }
    }
}
