﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public class MishtemaeshDinamiModel
    {
        public string shem_prati { get; set; }
        public string shem_mishpaha { get; set; }
        public int rashut { get; set; }
        public int zehut { get; set; }
        public int type1634 { get; set; }
    }
}
