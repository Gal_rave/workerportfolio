﻿using System;
using System.Configuration;

namespace DataAccess.Models
{
    public class MailDataModel
    {
        public decimal userid { get; set; }
        public decimal target_userid { get; set; }
        public string user_name { get; set; }
        public string target_user_name { get; set; }
        public string by_user_name { get; set; }
        public string tahalich_name { get; set; }
        public string from_date { get; set; }
        public string to_date { get; set; }
        public string description { get; set; }
        public string answer { get; set; }
        public string reason { get; set; }
        public string Domain { get { return ConfigurationManager.AppSettings["MainSiteDomain"]; } }
    }
}