﻿using System;

namespace DataAccess.Models
{
    public class SacharUserModel
    {
        public int idNumber { get; set; }
        public string fullName { get; set; }
        public int rashutNumber { get; set; }
        public int? kodMedaveach { get; set; }//for notifications inside michlol
    }
}