﻿namespace DataAccess.Models
{
    public class TerminationCauseModel
    {
        /// <summary>
        /// PNH_EREH_K_NUMERI
        /// </summary>
        public int TERMINATIONCAUSE { get; set; }
        /// <summary>
        /// PNH_TEUR_K
        /// </summary>
        public string TERMINATIONREASON { get; set; }
    }
}
