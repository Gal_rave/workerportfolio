﻿namespace DataAccess.Models
{
    public class GetWorkersResponse
    {
        public int rowIndex { get; set; }
        public bool isDev { get; set; }
        public bool success { get; set; }
    }
}
