﻿using System.Collections.Generic;

namespace DataAccess.Models
{
    public class StreetModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Symbol { get; set; }
        public List<CityModel> Cities { get; set; }
    }
}
