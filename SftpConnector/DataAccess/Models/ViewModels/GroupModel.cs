﻿using System.Collections.Generic;

namespace DataAccess.Models
{
    public class GroupModel
    {
        public decimal ID { get; set; }
        public string name { get; set; }
        public decimal immunity { get; set; }

        public List<UserModel> users { get; set; }
    }
}
