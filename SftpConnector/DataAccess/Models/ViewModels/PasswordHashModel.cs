﻿using DataAccess.Extensions;
using System.Text.RegularExpressions;

namespace DataAccess.Models
{
    public class PasswordHashModel
    {
        public byte[] Password { get; set; }
        public byte[] PasswordSalt { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] ConfirmationToken { get; set; }
        public byte[] VerificationToken { get; set; }
        public string password {
            get {
                return Password.ByteArrayToString();
            }
        }
        public string passwordSalt
        {
            get {
                return PasswordSalt.ByteArrayToString();
            }
        }
        public string passwordHash
        {
            get {
                return PasswordHash.ByteArrayToString();
            }
        }
        public string confirmationToken
        {
            get {
                return Regex.Replace(ConfirmationToken.ByteArrayToString(), "[^a-zA-Z0-9]", "");
            }
        }
        public string verificationToken
        {
            get
            {
                return Regex.Replace(VerificationToken.ByteArrayToString(), "[^a-zA-Z0-9]", "");
            }
        }
    }
}
