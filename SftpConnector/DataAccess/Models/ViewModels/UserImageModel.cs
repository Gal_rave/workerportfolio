﻿using System;

namespace DataAccess.Models
{
    public class UserImageModel
    {
        public int ID { get; set; }
        public int userId { get; set; }
        public string type { get; set; }
        public Nullable<DateTime> fileDate { get; set; }
        public byte[] fileBlob { get; set; }
        public string fileString { get; set; }
        public Nullable<int> municipalityId { get; set; }
        public string fileName { get; set; }
        public byte[] fileimageblob { get; set; }
        public string fileimageString { get; set; }
    }
}