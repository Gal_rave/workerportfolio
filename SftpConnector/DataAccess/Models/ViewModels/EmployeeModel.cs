﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public class EmployeeModel : UserModel
    {
        public int employerID { get; set; }
        public bool ilResident { get; set; }
        public bool kibbutzResident { get; set; }
        public bool hmoMember { get; set; }
        public string hmoName { get; set; }
        public taxCoordinationModel taxCoordination { get; set; }
        public taxExemptionModel taxExemption { get; set; }
        public DateTime startWorkTime { get; set; }
    }
}