﻿using System;

namespace DataAccess.Models
{
    public class LoginLogModel
    {
        public decimal ID { get; set; }
        public decimal userId { get; set; }
        public string action { get; set; }
        public DateTime actionDate { get; set; }
        public decimal municipalityId { get; set; }
        public string actionIP { get; set; }        
    }
}
