﻿using System;

namespace DataAccess.Models
{
    public class IncomeModel
    {
        public DateTime startDate { get; set; }
        public IncomeTypeModel incomeType { get; set; }
        public int incomeTaxCredits { get; set; }
        public bool trainingFund { get; set; }
        public bool workDisability { get; set; }
        public string anotherSourceDetails { get; set; }
        public bool hasIncomes { get; set; }
    }
}