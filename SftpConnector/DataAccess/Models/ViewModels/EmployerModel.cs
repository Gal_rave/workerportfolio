﻿namespace DataAccess.Models
{
    public class EmployerModel
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phoneNumber { get; set; }
        public string deductionFileID { get; set; }
        public string email { get; set; }
        public bool displayEmail { get; set; }
        public bool emailEmployee { get; set; }
        public bool emailEmployer { get; set; }
        public decimal salary { get; set; }
        public decimal taxDeduction { get; set; }
        public IncomeTypeModel incomeType { get; set; }
        public int mifal { get; set; }
        public int rashut { get; set; }
    }
}