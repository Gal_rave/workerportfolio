﻿using System.Collections.Generic;

namespace DataAccess.Models
{
    public class taxCoordinationModel
    {
        public bool requestCoordination { get; set; }
        public int requestReason { get; set; }
        public List<EmployerModel> employers { get; set; }
    }
}
