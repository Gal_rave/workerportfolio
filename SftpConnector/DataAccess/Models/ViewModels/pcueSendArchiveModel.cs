﻿using System;

namespace DataAccess.Models
{
    public class pcueSendArchiveModel
    {
        public decimal id { get; set; }
        public decimal userid { get; set; }
        public DateTime senddate { get; set; }
        public decimal municipalityid { get; set; }
        public int pc_month { get; set; }
        public int pc_year { get; set; }
    }
}
