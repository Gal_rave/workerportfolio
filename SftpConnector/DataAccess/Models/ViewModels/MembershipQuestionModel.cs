﻿namespace DataAccess.Models
{
    public class MembershipQuestionModel
    {
        public decimal ID { get; set; }
        public decimal userId { get; set; }
        public string question { get; set; }
        public string answer { get; set; }

        public UserModel user { get; set; }
    }
}
