﻿using System;

namespace DataAccess.Models
{
    public class MembershipModel
    {
        public decimal userID { get; set; }
        public string passwordSalt { get; set; }
        public string password { get; set; }
        public DateTime createdDate { get; set; }
        public string confirmationToken { get; set; }
        public bool isConfirmed { get; set; }
        public Nullable<DateTime> lastPasswordFailureDate { get; set; }
        public decimal PasswordFailureSCounter { get; set; }
        public Nullable<DateTime> passwordChangeDdate { get; set; }
        public string passwordVerificationToken { get; set; }
        public Nullable<DateTime> verificationTokenExpiration { get; set; }
        public string passwordFormat { get; set; }
        public string smsVerificationToken { get; set; }
        public Nullable<DateTime> smsTokenExpiration { get; set; }

        public UserModel user { get; set; }
    }
}
