﻿using System.Collections.Generic;

namespace DataAccess.Models
{
    public class CityModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public List<StreetModel> Streets { get; set; }
    }
}
