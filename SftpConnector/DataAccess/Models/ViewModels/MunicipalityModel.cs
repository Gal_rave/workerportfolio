﻿using System.Collections.Generic;

namespace DataAccess.Models
{
    public class MunicipalityModel
    {
        public decimal ID { get; set; }
        public string name { get; set; }
        public bool active { get; set; }
        public bool allow101 { get; set; }
        public bool allowprocess { get; set; }
        public bool allowemailpaycheck { get; set; }
        public int? rashut { get; set; }
        public int? factoryEmailPC { get; set; }
        public string synerion_link { get; set; }
        public bool allowMunicipalityMenu { get; set; }
        public bool allowSystemsEmail { get; set; }
        public string municipalityMenuTitle { get; set; }
        public bool performUpdate { get; set; }

        public List<UserModel> users { get; set; }
        public List<HtmlTemplateModel> htmlTemplates { get; set; }
        public List<MunicipalityContactModel> contacts { get; set; }
    }
}