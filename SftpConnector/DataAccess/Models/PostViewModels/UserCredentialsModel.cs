﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace DataAccess.Models
{
    public class UserCredentialsModel
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string fullName { get; set; }
        public string idNumber { get; set; }
        public string confirmationToken { get; set; }
        public DateTime loginDate { get; set; }
        public string calculatedIdNumber { get; set; }
        public int currentMunicipalityId { get; set; }
        public int currentRashutId { get; set; }
        public Nullable<DateTime> currentTerminationDate { get; set; }
        public List<GroupModel> groups { get; set; }
        public int isAdmin { get; set; }
        public string email { get; set; }
        public int? myReferredProcesses { get; set; }
        public int? myRequestsProcesses { get; set; }

        public bool isAllowEPC { get; set; }
        public bool isAllow101 { get; set; }
        public bool isAllowProcess { get; set; }
        public bool isAllowMunicipalityMenu { get; set; }
        public bool isAllowSystemsEmail { get; set; }
        
        public string Domain { get { return ConfigurationManager.AppSettings["MainSiteDomain"]; } }
        public List<MunicipalityModel> municipalities { get; set; }
    }
}
