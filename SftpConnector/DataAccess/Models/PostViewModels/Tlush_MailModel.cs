﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class Tlush_MailModel
    {
        public double REC_ID { get; set; }
        public double RASHUT { get; set; }
        public double MIFAL { get; set; }
        public double ZEHUT { get; set; }
        public Nullable<double> ZEHUT_MALE { get; set; }
        public bool SEND_MAIL { get; set; }          //1-לשלוח במייל
        public double FROM_DATE { get; set; }          //date in `ddmmyyyy` format
        public double TO_DATE { get; set; }            //date in `ddmmyyyy` format
        public DateTime CREATION_DATE { get; set; }
        public double CREATION_USER { get; set; }
        public int CREATION_SOURCE { get; set; }    //1-תפעולי   2-תיק עובד
        public DateTime? UPDATE_DATE { get; set; }
        public double UPDATE_USER { get; set; }
        public int UPDATE_SOURCE { get; set; }      //1-תפעולי   2-תיק עובד
        public double CREATION_KOD_USER { get; set; }
        public double RASHUT_SACHAR { get; set; }
        public int STATUS { get; set; }
    }
}