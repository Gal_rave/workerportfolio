﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class CryptoModel
    {
        public CryptoModel() { }
        public CryptoModel(byte[] key, byte[] iv)
        {
            this.key = Convert.ToBase64String(key);
            this.iv = Convert.ToBase64String(iv);
        }
        public CryptoModel(string key, string iv)
        {
            this.key = key;
            this.iv = iv;
        }
        public CryptoModel(string idNumber, string password, string[] keys)
        {
            this.key = keys[0];
            this.iv = keys[1];
            this.idNumber = idNumber;
            this.password = password;
        }

        public string idNumber { get; set; }
        public string password { get; set; }
        public string key { get; set; }
        public string iv { get; set; }
    }
}
