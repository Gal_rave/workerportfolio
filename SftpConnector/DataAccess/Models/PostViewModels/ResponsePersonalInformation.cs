﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class ResponsePersonalInformation
    {
        public string lastName{ get; set; }
        public string firstName { get; set; }
        public string FathersName { get; set; }
        public string previousLastName { get; set; }
        public string previousFirstName { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public int houseNumber { get; set; }
        public string entranceNumber { get; set; }
        public int apartmentNumber { get; set; }
        public int zip { get; set; }
        public int poBox { get; set; }
        public long phoneNumberPrefix { get; set; }
        public long extraNumberPrefix { get; set; }
        public DateTime birthDate { get; set; }
        public string countryOfBirth { get; set; }
        public DateTime immigrationDate { get; set; }
        public string gender { get; set; }
        public string maritalStatus { get; set; }
        public DateTime statusDate { get; set; }
        public string spouseName { get; set; }
        public int spouseIdNUmber { get; set; }
        public DateTime spousBirthDate { get; set; }
        public string spousJobStatus { get; set; }
        public string spousJobName { get; set; }
        public long spousPhoneNumberPrefix { get; set; }
        public int spousPhoneNumberExtension { get; set; }
        public int count11 { get; set; }
        public int count17 { get; set; }
        public List<ResponseChild> children { get; set; }
        public List<ResponseEducation> courses { get; set; }
    }
}
