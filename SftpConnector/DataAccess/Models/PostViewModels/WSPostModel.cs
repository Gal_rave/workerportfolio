﻿using System;

namespace DataAccess.Models
{
    public class WSPostModel
    {
        public string idNumber { get; set; }
        public string confirmationToken { get; set; }
        public string calculatedIdNumber { get; set; }
        public int currentMunicipalityId { get; set; }
        public int currentRashutId { get; set; }
        public DateTime selectedDate { get; set; }
        public bool updateUserData { get; set; }
        public bool? isMobileRequest { get; set; }
        public bool isEncrypted { get; set; }
        public string requiredRole { get; set; }
    }
}