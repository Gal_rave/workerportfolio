﻿namespace DataAccess.Models
{
    public class MunicipalityContactModel
    {
        public int ID { get; set; }
        public string areaCode { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string houseNumber { get; set; }
        public string city { get; set; }
        public string postalCode { get; set; }
        public string deductionFileID { get; set; }
        public string street { get; set; }
        public int? mifal { get; set; }
        public int? rashut { get; set; }
        public int municipalityId { get; set; }
        public MunicipalityModel municipality { get; set; }
    }
}