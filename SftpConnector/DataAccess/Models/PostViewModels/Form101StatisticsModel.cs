﻿namespace DataAccess.Models
{
    public class Form101StatisticsModel
    {
        public decimal municipalityId { get; set; }
        public decimal counter { get; set; }
        public decimal ok { get; set; }
        public decimal bad { get; set; }
        public string name { get; set; }
    }
}
