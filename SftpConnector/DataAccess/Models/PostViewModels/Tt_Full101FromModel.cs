﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class Tt_Full101FromModel
    {
        public Tt_Tofes101Model t101 { get; set; }
        public List<Tt_Tofes101_YeladimModel> yeladim { get; set; }
        public Tt_Tofes101_OvedModel oved { get; set; }
        public List<Tt_Tofes101_HachnasaModel> hachnasa { get; set; }
    }
}