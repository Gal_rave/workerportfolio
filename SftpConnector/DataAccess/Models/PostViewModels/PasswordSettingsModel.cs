﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class PasswordSettingsModel
    {
        public PasswordSettingsModel() { }
        public PasswordSettingsModel(int length, int nonalphanumeric, int numeric)
        {
            this.minLength = length;
            this.minNonAlphanumeric = nonalphanumeric;
            this.minNumeric = numeric;
        }
        public int minLength { get; set; }
        public int minNonAlphanumeric { get; set; }
        public int minNumeric { get; set; }
    }
}
