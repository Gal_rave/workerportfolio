﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class CreateUserModel
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string idNumber { get; set; }
        public string phone { get; set; }
        public string cell { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string cityName { get; set; }
        public string streetName { get; set; }
        public string municipalityName { get; set; }
        public CityModel city { get; set; }
        public StreetModel street { get; set; }
        public MunicipalityModel municipality { get; set; }
    }
}
