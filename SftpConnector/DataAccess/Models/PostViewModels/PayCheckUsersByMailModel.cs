﻿using System;

namespace DataAccess.Models
{
    public class PayCheckUsersByMailModel
    {
        public int ID { get; set; }
        public int userId { get; set; }
        public int municipalityId { get; set; }
        public DateTime createdDate { get; set; }
        public Nullable<DateTime> updateDate { get; set; }
        public bool activeRecord { get; set; }
        public Nullable<DateTime> lastSendDate { get; set; }
        public bool creationSource { get; set; }
        public Nullable<int> lastSendMonth { get; set; }
        public Nullable<int> lastSendYear { get; set; }
        public bool sendMail { get; set; }

        /// <summary>
        /// custom prop for model only
        /// </summary>
        public string idNumber { get; set; }
        
        public virtual MunicipalityModel municipality { get; set; }
        public virtual UserModel user { get; set; }
    }
}