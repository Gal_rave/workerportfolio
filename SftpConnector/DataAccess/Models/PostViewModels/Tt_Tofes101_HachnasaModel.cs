﻿using System;

namespace DataAccess.Models
{
    public class Tt_Tofes101_HachnasaModel
    {
        public decimal rec_id { get; set; }
        public long rashut { get; set; }
        public long mifal { get; set; }
        public Nullable<long> betokef_me { get; set; }
        public Nullable<long> betokef_ad { get; set; }
        public decimal oved_id { get; set; }
        public string hachnasa_nosefet_shem_maavid { get; set; }
        public string hachnasa_nosefet_ktovet { get; set; }
        public Nullable<long> hachnasa_nosefet_tik_nikuiim { get; set; }
        public Nullable<long> hachnasa_nosefet_sug_hachnasa { get; set; }
        public Nullable<decimal> hachnasa_hodshit { get; set; }
        public Nullable<decimal> hachnasa_nosefet_mas_shnuka { get; set; }
        public Nullable<short> pakid_shuma { get; set; }
        public Nullable<long> Checkhachnasa_nosefet_ktovet { get; set; }
        public Nullable<long> Checkhachnasa_nosefet_tik { get; set; }
        public Nullable<long> Checkhachnasa_nosefet_sug { get; set; }
        public Nullable<long> Checkhachnasa_nosefet_shem { get; set; }
        public Nullable<long> Checkhachnasa_hodshit { get; set; }
        public Nullable<long> Checkhachnasa_nosefet_mas { get; set; }
        public int CheckCounter { get; set; }
        public string sug_hachnasa { get; set; }

        public Tt_Tofes101_OvedModel tt_tofes101_oved { get; set; }
    }
}