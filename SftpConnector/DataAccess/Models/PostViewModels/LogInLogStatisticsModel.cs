﻿namespace DataAccess.Models
{
    public class LogInLogStatisticsModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public int prospective_users { get; set; }
        public int active_users { get; set; }
    }
}