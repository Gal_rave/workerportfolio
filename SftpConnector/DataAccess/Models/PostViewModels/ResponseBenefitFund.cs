﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Extensions;
using System.Globalization;

namespace DataAccess.Models
{
    public class ResponseBenefitFund
    {
        public ResponseBenefitFund() { }
        public ResponseBenefitFund(CultureInfo currentculture, int _month)
        {
            this.month = _month;
            this.monthName = currentculture.DateTimeFormat.GetMonthName(this.month);
            this.order = _month;
            fundBrutoGemel = (0).ToDecimal();
            provisionsAmount = (0).ToDecimal();
            provisionsPercentage = (0).ToDouble();
            deductionAmount = (0).ToDecimal();
            deductionPercentage = (0).ToDouble();
            disabilityInsuranceAmount = (0).ToDouble();
            disabilityInsurancePercentage = (0).ToDouble();
            severanceAmount = (0).ToDouble();
            severancePercentage = (0).ToDouble();
            monthTotal = (0).ToDouble();
        }
        public ResponseBenefitFund(CultureInfo currentculture, int _month, int order)
        {
            this.month = _month;
            this.monthName = currentculture.DateTimeFormat.GetMonthName(this.month);
            this.order = order;
        }
        public ResponseBenefitFund(CultureInfo currentculture, double? fundbrutogemel = null, string fundname = null, int? fundnumber = null, double? provisionsamount = null, double? provisionspercentage = null, 
            int? _month = null, double? deductionamount = null, double? deductionpercentage = null, double? disabilityinsuranceamount = null, double? disabilityinsurancepercentage = null, 
            double? severanceamount = null, double? severancepercentage = null, double? monthtotal = null, int? order = -1) 
        {
            if (fundbrutogemel.HasValue)
                this.fundBrutoGemel = fundbrutogemel.Value;
            if (fundnumber.HasValue)
                this.fundNumber = fundnumber.Value;
            if (provisionsamount.HasValue)
                this.provisionsAmount = provisionsamount.Value;
            if (provisionspercentage.HasValue)
                this.provisionsPercentage = provisionspercentage.Value;
            if (_month.HasValue)
            {
                this.month = _month.Value;
                this.monthName = this.month > 0 && this.month < 13? currentculture.DateTimeFormat.GetMonthName(this.month) : string.Empty;
            }
            if (deductionamount.HasValue)
                this.deductionAmount = deductionamount.Value;
            if (deductionpercentage.HasValue)
                this.deductionPercentage = deductionpercentage.Value;
            if (disabilityinsuranceamount.HasValue)
                this.disabilityInsuranceAmount = disabilityinsuranceamount.Value;
            if (disabilityinsurancepercentage.HasValue)
                this.disabilityInsurancePercentage = disabilityinsurancepercentage.Value;
            if (severanceamount.HasValue)
                this.severanceAmount = severanceamount.Value;
            if (severancepercentage.HasValue)
                this.severancePercentage = severancepercentage.Value;
            if (monthtotal.HasValue)
                this.monthTotal = monthtotal.Value;
            this.fundName = !string.IsNullOrWhiteSpace(fundname) ? fundname : string.Empty;
            this.order = order.HasValue && order.Value > -1 ? order.Value : (_month.HasValue ? _month.Value : 0);
        }

        public object fundBrutoGemel { get; set; }
        public string fundName { get; set; }
        public int fundNumber { get; set; }
        public object provisionsAmount { get; set; }
        public double provisionsPercentage { get; set; }
        public int month { get; set; }
        public object deductionAmount { get; set; }
        public double deductionPercentage { get; set; }
        public object disabilityInsuranceAmount { get; set; }
        public double disabilityInsurancePercentage { get; set; }
        public object severanceAmount { get; set; }
        public double severancePercentage { get; set; }
        public object monthTotal { get; set; }
        public string monthName { get; set; }
        public int order { get; set; }
    }
}
