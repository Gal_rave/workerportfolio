﻿using System;

namespace DataAccess.Models
{
    public class Form101DataModel
    {
        public int ID { get; set; }
        public string jsonData { get; set; }
        public Nullable<int> formImageId { get; set; }
        public Nullable<int> signatureImagId { get; set; }
        public int userId { get; set; }
        public DateTime createdDate { get; set; }
        public string version { get; set; }
        public int deliveryStatus { get; set; }
        public Nullable<DateTime> actualcreationdate { get; set; }
        public Nullable<decimal> municipalityId { get; set; }
    }
}