﻿namespace DataAccess.Models
{
    public class PostDirectoryModel
    {
        public WSPostModel wsModel { get; set; }
        public DirectoryModel directory { get; set; }
        public ImageModel image { get; set; }
        public HtmlTemplateModel template { get; set; }
    }
}