﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class RegisterUserModel
    {
        public string idNumber { get; set; }
        public string calculatedIdNumber { get; set; }
        public string password { get; set; }
        public string initialPassword { get; set; }
        public string email { get; set; }
        public string cell { get; set; }
        public List<MunicipalityModel> municipalities { get; set; }
        public MunicipalityModel municipality { get; set; }
    }
}
