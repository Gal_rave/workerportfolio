﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public class Tt_Tofes101Model
    {
        public decimal rec_id { get; set; }
        public long rashut { get; set; }
        public long mifal { get; set; }
        public Nullable<long> betokef_me { get; set; }
        public Nullable<long> betokef_ad { get; set; }
        public string shem_tofes { get; set; }
        public Nullable<long> shnat_tofes { get; set; }
        public Nullable<long> form_type { get; set; }
        public Nullable<long> status_101 { get; set; }
        public Nullable<long> makav_101 { get; set; }
        public Nullable<long> taarich_milui { get; set; }
        public Nullable<long> zman_milui { get; set; }
        public string version { get; set; }
        public Nullable<System.DateTime> taarich_dchiya { get; set; }
        public string heaara { get; set; }
        public Nullable<short> dchiya { get; set; }
        public Nullable<short> dchiya_mail_status { get; set; }
        public long oved_id { get; set; }

        public List<Tt_Tofes101_OvedModel> tt_tofes101_oved { get; set; }
    }
}