﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class UserSearchModel
    {
        public string idNumber { get; set; }
        public string searchIdNumber { get; set; }
        public string cell { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string confirmationToken { get; set; }
        public int page { get; set; }
        public string password { get; set; }
        public string userToken { get; set; }
        public int maxImmunity { get; set; }
        public int municipalityId { get; set; }
        public List<decimal> municipalities { get; set; }
        public string searchMunicipalityName { get; set; }
        public string searchMunicipalitiesId { get; set; }
        public bool? active { get; set; }
        public bool? has101 { get; set; }
        public bool? hasProcess { get; set; }
        public bool? hasEmailPaycheck { get; set; }
        public bool? hasMunicipalityMenu { get; set; }
        public bool? hasSystemsEmail { get; set; }
        public MunicipalityModel municipality { get; set; }
        public List<MunicipalityModel> municipalitiesList { get; set; }
        public DateTime searchDate { get; set; }
        public DateTime endSearchDate { get; set; }
        public string requiredRole { get; set; }
        public humanResourcesContactModel contact { get; set; }
    }
}
