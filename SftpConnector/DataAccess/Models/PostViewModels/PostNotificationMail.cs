﻿using System;
using DataAccess.Enums;

namespace DataAccess.Models
{
    public class PostNotificationMail
    {
        public int ID { get; set; }
        public string department { get; set; }
        public string files { get; set; }
        public string jobTitle { get; set; }
        public string phone { get; set; }
        public string subject { get; set; }
        public string text { get; set; }
        public int toMailID { get; set; }
        public string userName { get; set; }
        public string userIdNumber { get; set; }
        public UserModel sender { get; set; }
        public humanResourcesContactModel recipient { get; set; }
        public string senderEmail { get; set; }
        public string recipientEmail { get; set; }
        public Nullable<MailTypeEnum> MailType { get; set; }
        public Nullable<DateTime> createdDate { get; set; }
    }
}
