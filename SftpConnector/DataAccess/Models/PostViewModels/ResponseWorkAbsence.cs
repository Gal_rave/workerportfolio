﻿using System.Globalization;

namespace DataAccess.Models
{
    public class ResponseWorkAbsence
    {
        public ResponseWorkAbsence() { }
        public ResponseWorkAbsence(int month, int columnnumber)
        {
            this.month = month;
            this.columnNumber = columnnumber;
            this.value = 0;
        }
        public ResponseWorkAbsence(int month, int columnnumber, CultureInfo currentculture)
        {
            this.month = month;
            this.columnNumber = columnnumber;
            this.value = month > 0 && month < 13 ? currentculture.DateTimeFormat.GetMonthName(month) : string.Empty;
            this.columnTitle = "חודש";
        }

        public int month { get; set; }
        public object value { get; set; }
        public int columnNumber { get; set; }
        public int tabTarget { get; set; }
        public string columnTitle { get; set; }
        public string tabTitle { get; set; }
        public int units { get; set; }
    }
}
