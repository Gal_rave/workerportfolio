﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class TaxFromModel
    {
        public TaxFromModel()
        {
            employee = new EmployeeModel();
            employer = new EmployerModel();
            children = new List<ChildModel>();
            income = new IncomeModel();
            signature = new SignatureModel();
            taxCoordination = new taxCoordinationModel();
            taxExemption = new taxExemptionModel();
            incomeType = new IncomeTypeModel();
            spouse = new SpouseModel();
        }
        public int year { get; set; }
        public DateTime date { get; set; }
        public bool sign { get; set; }
        public DateTime signatureDate { get; set; }
        public int signatureImageID { get; set; }
        public bool emailEmployee { get; set; }
        public EmployeeModel employee { get; set; }
        public EmployerModel employer { get; set; }
        public List<ChildModel> children { get; set; }
        public IncomeModel income { get; set; }
        public SignatureModel signature { get; set; }
        public taxCoordinationModel taxCoordination { get; set; }
        public taxExemptionModel taxExemption { get; set; }
        public IncomeTypeModel incomeType { get; set; }
        public SpouseModel spouse { get; set; }
        public IncomeModel otherIncomes { get; set; }
        public int municipalityId { get; set; }
        public UserImageModel pdf { get; set; }
        public List<UserImageModel> connectedImages { get; set; }
        public string version { get; set; }
        public int FormType { get; set; }
    }
}
