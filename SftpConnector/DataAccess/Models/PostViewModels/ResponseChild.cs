﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class ResponseChild
    {
        public string firstName { get; set; }
        public int idNumber { get; set; }
        public string gender { get; set; }
        public DateTime birthDate { get; set; }
        public string homeLiving { get; set; }
    }
}
