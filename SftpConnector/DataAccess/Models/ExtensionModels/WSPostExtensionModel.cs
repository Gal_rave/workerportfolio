﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class WSPostExtensionModel: WSPostModel
    {
        public string Cell { get; set; }
        public string Email { get; set; }
        public int ToSendType { get; set; }
        public string Token { get; set; }
        public string Password { get; set; }
        public bool success { get; set; }
        public int exceptionId { get; set; }
    }
}
