﻿namespace DataAccess.Models
{
    public class PayCheckUsersByMailExtensionModel : PayCheckUsersByMailModel
    {
        public pcueSendArchiveModel pcue_sendarchive { get; set; }
        public string Email { get; set; }
    }
}