﻿namespace DataAccess.Models
{
    public class Form101DataExtensionModel : Form101DataModel
    {
        public UserModel user { get; set; }
        public MunicipalityModel municipality { get; set; }
        public string FullUserName { get; set; }
        public string MunicipalityName { get; set; }
        public string IdNumber { get; set; }
        public UserImageModel theForm { get; set; }
    }
}
