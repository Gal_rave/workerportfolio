﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class EmployeeExtensionModel: EmployeeModel
    {
        public int CheckbirthDate { get; set; }
        public int Checkcell { get; set; }
        public int Checkcity { get; set; }
        public int Checkemail { get; set; }
        public int CheckfirstName { get; set; }
        public int Checkgender { get; set; }
        public int CheckhmoMember { get; set; }
        public int CheckhmoName { get; set; }
        public int CheckhouseNumber { get; set; }
        public int CheckidNumber { get; set; }
        public int CheckilResident { get; set; }
        public int CheckimmigrationDate { get; set; }
        public int CheckkibbutzResident { get; set; }
        public int ChecklastName { get; set; }
        public int CheckmaritalStatus { get; set; }
        public int Checkphone { get; set; }
        public int Checkstreet { get; set; }
        public int Checkzip { get; set; }
        public new taxCoordinationExtensionModel taxCoordination { get; set; }
        public new taxExemptionExtensionModel taxExemption { get; set; }
    }
}
