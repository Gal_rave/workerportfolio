﻿using DataAccess.Models;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public class MunicipalityExtensionModel : MunicipalityModel
    {
        public int forms { get; set; }
        public int deliveryStatusOk { get; set; }
        public int deliveryStatusBad { get; set; }
        public List<humanResourcesContactModel> humanResourcesContacts { get; set; }
    }
}
