﻿
namespace DataAccess.Models
{
    public class MunicipalitySchemaExtension
    {
        public string schema_name { get; set; }
        public string customer_number { get; set; }
        public string customer_name_heb { get; set; }
        public string mifal { get; set; }
        public string shem_mifal { get; set; }
        public string dist { get; set; }
        public string rashut_mancol { get; set; }
        public string rashut_sachar { get; set; }
        public string factory_entry { get; set; }
        public MunicipalityContactExtension contact { get; set; }
    }
}
