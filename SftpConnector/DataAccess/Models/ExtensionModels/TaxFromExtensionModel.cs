﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class TaxFromExtensionModel : TaxFromModel
    {
        public new EmployeeExtensionModel employee { get; set; }
        public new EmployerExtensionModel employer { get; set; }
        public new List<ChildExtensionModel> children { get; set; }
        public new IncomeExtensionModel income { get; set; }
        public new taxCoordinationExtensionModel taxCoordination { get; set; }
        public new taxExemptionExtensionModel taxExemption { get; set; }
        public new IncomeTypeExtensionModel incomeType { get; set; }
        public new SpouseExtensionModel spouse { get; set; }
        public new IncomeExtensionModel otherIncomes { get; set; }
        public bool? showMobile { get; set; }
    }
}
