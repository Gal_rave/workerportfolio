﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class AddressModel
    {
        public CityModel city { get; set; }
        public StreetModel street { get; set; }
        public string CityName { get; set; }
        public string StreetName { get; set; }
    }
}