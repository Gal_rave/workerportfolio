﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class EmployerExtensionModel: EmployerModel
    {
        public int Checkaddress { get; set; }
        public int CheckdeductionFileID { get; set; }
        public int CheckincomeType { get; set; }
        public int Checkmifal { get; set; }
        public int Checkname { get; set; }
        public int Checkrashut { get; set; }
        public int Checksalary { get; set; }
        public int ChecktaxDeduction { get; set; }
        public new IncomeTypeExtensionModel incomeType { get; set; }
    }
}
