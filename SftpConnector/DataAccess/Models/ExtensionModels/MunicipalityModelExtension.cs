﻿namespace DataAccess.Models
{
    public class MunicipalityModelExtension : MunicipalityModel
    {
        public MunicipalityContactModel contact { get; set; }
    }
}
