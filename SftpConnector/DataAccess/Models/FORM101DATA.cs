//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FORM101DATA
    {
        public decimal ID { get; set; }
        public string JSONDATA { get; set; }
        public Nullable<decimal> FORMIMAGEID { get; set; }
        public Nullable<decimal> SIGNATUREIMAGID { get; set; }
        public decimal USERID { get; set; }
        public System.DateTime CREATEDDATE { get; set; }
        public string VERSION { get; set; }
        public int DELIVERYSTATUS { get; set; }
        public Nullable<System.DateTime> ACTUALCREATIONDATE { get; set; }
        public Nullable<decimal> MUNICIPALITYID { get; set; }
    
        public virtual USERIMAGE USERIMAGE { get; set; }
        public virtual USERIMAGE USERIMAGE1 { get; set; }
        public virtual USER USER { get; set; }
        public virtual MUNICIPALITy MUNICIPALITy { get; set; }
    }
}
