﻿using Catel.Data;
using DataAccess.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Logger;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Extensions
{
    public static class ArrayExtensions
    {
        public static List<int> ToIntLIst(this string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return new List<int>();
            if (str.IndexOf(",") < 0)
                return new List<int>() { str.ToInt() };
            return str.Split(',').Select(int.Parse).ToList();
        }
        public static List<int> ToIntList(this IEnumerable<string> list)
        {
            return list.Select(int.Parse).ToList();
        }
        public static List<int> ToIntList(this IEnumerable<decimal> list)
        {
            return list.Select(d => d.ToInt()).ToList();
        }
        public static List<string> ToStringList<T>(this IEnumerable<T> list)
        {
            return list.Select(d => d.ToString()).ToList();
        }
        public static List<string> ToStringList(this string val, char seperator = ',')
        {
            if (string.IsNullOrWhiteSpace(val))
                return new List<string>();

            return val.Split(seperator).ToList();
        }
        /// <summary>
        /// Splits an array into several smaller arrays.
        /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array to split.</param>
        /// <param name="size">The size of the smaller arrays.</param>
        /// <returns>An array containing smaller arrays.</returns>
        public static IEnumerable<IEnumerable<T>> Split<T>(this T[] array, int size = 100)
        {
            for (var i = 0; i < (float)array.Length / size; i++)
            {
                yield return array.Skip(i * size).Take(size);
            }
        }
        /// <summary>
        /// Splits a list into several smaller lists.
        /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array to split.</param>
        /// <param name="size">The size of the smaller arrays.</param>
        /// <returns>An array containing smaller arrays.</returns>
        public static IEnumerable<IEnumerable<T>> Chunk<T>(this IEnumerable<T> source, int chunksize = 100)
        {
            var pos = 0;
            while (source.Skip(pos).Any())
            {
                yield return source.Skip(pos).Take(chunksize);
                pos += chunksize;
            }
        }
        public static string getItemFromArray(this string[] row, int index)
        {
            try
            {
                return row[index];
            }
            catch
            {
                return "";
            }
        }
        public static IEnumerable<List<T>> SplitList<T>(List<T> locations, int nSize = 30)
        {
            for (int i = 0; i < locations.Count; i += nSize)
            {
                yield return locations.GetRange(i, Math.Min(nSize, locations.Count - i));
            }
        }
        public static string[] ToStringArray(this string str)
        {
            return str.ToStringList().Select(s => s.Trim()).ToArray();
        }
        public static byte[] Combine(List<byte[]> pdfByteContent)
        {

            using (var ms = new MemoryStream())
            {
                using (var doc = new Document())
                {
                    using (var copy = new PdfSmartCopy(doc, ms))
                    {
                        doc.Open();

                        //Loop through each byte array
                        foreach (var p in pdfByteContent)
                        {
                            //Create a PdfReader bound to that byte array
                            using (var reader = new PdfReader(p))
                            {
                                int pageNum = reader.NumberOfPages;
                                for (int i = 1; i <= pageNum; i++)
                                {
                                    reader.SetPageContent(i, reader.GetPageContent(i), 9, true);
                                }
                                reader.RemoveAnnotations();
                                reader.RemoveUnusedObjects();
                                reader.RemoveUsageRights();

                                //Add the entire document instead of page-by-page
                                copy.AddDocument(reader);
                            }
                        }

                        doc.Close();
                    }
                }

                //Return just before disposing
                return ms.ToArray();
            }
        }
        
    }
}
