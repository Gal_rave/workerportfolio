﻿using System;
using iTextSharp.text.pdf;
using System.Collections.Generic;

using DataAccess.Models;
using iTextSharp.text;

namespace DataAccess.Extensions
{
    public static class AcroFieldsExtensions
    {
        /// <summary>
        /// set the field direction inpdf
        /// </summary>
        /// <param name="fields"></param>
        /// <param name="fieldName"></param>
        /// <param name="direction"></param>
        public static void SetDirection(this AcroFields fields, string fieldName, int direction)
        {
            AcroFields.Item item = fields.GetFieldItem(fieldName);
            switch (direction)
            {
                case 1:
                    item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_RIGHT));
                    break;
                case 2:
                    item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_CENTER));
                    break;
                case 3:
                    item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_LEFT));
                    break;
                default:
                    item.GetMerged(0).Put(PdfName.Q, new PdfNumber(PdfFormField.Q_RIGHT));
                    break;
            }
        }
        /// <summary>
        /// set value in checkboxes
        /// </summary>
        /// <param name="fields"></param>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <param name="asTrue"></param>
        public static void SetCheckBox(this AcroFields fields, string fieldName, bool value, bool asTrue)
        {
            if (asTrue) fields.SetField(fieldName, value ? "on" : "Off");
            else fields.SetField(fieldName, value ? "Off" : "on");
        }
        public static int ChildCount_UpToNineteen(this List<ChildModel> children)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = calcDateDiff(child.birthDate);
                if (years >= 18 && years <= 19)
                    counter++;

            }
            return counter;
        }
        public static int ChildCount_SixToEighteen(this List<ChildModel> children)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = calcDateDiff(child.birthDate);
                if (years >= 6 && years <= 18)
                    counter++;
                
            }
            return counter;
        }
        public static int ChildCount_OneToFive(this List<ChildModel> children)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = calcDateDiff(child.birthDate);
                if (years >= 1 && years <= 5)
                    counter++;

            }
            return counter;
        }
        public static int ChildCount_OneOrTow(this List<ChildModel> children)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = calcDateDiff(child.birthDate);
                if (years >= 1 && years <= 2)
                    counter++;

            }
            return counter;
        }
        public static int ChildCount_ThreeOrBorn(this List<ChildModel> children)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = calcDateDiff(child.birthDate);
                if (years == 3 || child.birthDate.Year == DateTime.Now.Year)
                    counter++;

            }
            return counter;
        }
        public static PdfStamper ReplaceField(this AcroFields fields, PdfStamper pdfStamper, string fieldName, object value)
        {
            AcroFields.FieldPosition fieldPosition = fields.GetFieldPositions(fieldName)[0];
            fields.RemoveField(fieldName);

            PushbuttonField imageField = new PushbuttonField(pdfStamper.Writer, fieldPosition.position, "signatureDataUrl_Replaced");
            
            return pdfStamper;
        }
        public static void SetColor(this AcroFields fields, string fieldName, BaseColor color)
        {
            fields.SetFieldProperty(fieldName, "textcolor", color, null);
        }
        public static void SetColor(this AcroFields fields, string fieldName, int checkType)
        {
            switch (checkType)
            {
                case 0:
                    fields.SetFieldProperty(fieldName, "textcolor", BaseColor.BLACK, null);
                    break;
                case 1:
                    //fields.SetFieldProperty(fieldName, "textcolor", BaseColor.GREEN , null);
                    //break;
                case 2:
                    fields.SetFieldProperty(fieldName, "textcolor", BaseColor.RED, null);
                    break;
                default:
                    fields.SetFieldProperty(fieldName, "textcolor", BaseColor.BLACK, null);
                    break;
            }
        }

        #region extension models functions
        public static int ChildCount_OneToFive(this List<ChildExtensionModel> children)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = calcDateDiff(child.birthDate);
                if (years >= 1 && years <= 5)
                    counter++;

            }
            return counter;
        }
        public static int ChildCount_SixToEighteen(this List<ChildExtensionModel> children)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = calcDateDiff(child.birthDate);
                if (years >= 6 && years <= 18)
                    counter++;

            }
            return counter;
        }
        public static int ChildCount_UpToNineteen(this List<ChildExtensionModel> children)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = calcDateDiff(child.birthDate);
                if (years >= 18 && years <= 19)
                    counter++;

            }
            return counter;
        }
        public static int ChildCount_OneOrTow(this List<ChildExtensionModel> children)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = calcDateDiff(child.birthDate);
                if (years >= 1 && years <= 2)
                    counter++;

            }
            return counter;
        }
        public static int ChildCount_ThreeOrBorn(this List<ChildExtensionModel> children)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = calcDateDiff(child.birthDate);
                if (years == 3 || child.birthDate.Year == DateTime.Now.Year)
                    counter++;

            }
            return counter;
        }
        public static int BornOrEighteen(this List<ChildExtensionModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (year == child.birthDate.Year || years == 18)
                    counter++;

            }
            return counter;
        }
        public static int OneToFive(this List<ChildExtensionModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (years <= 5 && years > 0)
                    counter++;

            }
            return counter;
        }
        public static int AnderNineteen(this List<ChildExtensionModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (years > 5 && years < 19 && years != 18)
                    counter++;

            }
            return counter;
        }
        public static int ThreeOrBorn(this List<ChildExtensionModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (years == 3 || years == 0)
                    counter++;

            }
            return counter;
        }
        public static int OneOrTow(this List<ChildExtensionModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (years < 3 && years > 0)
                    counter++;

            }
            return counter;
        }
        public static int ThisYear(this List<ChildExtensionModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (year == child.birthDate.Year)
                    counter++;
            }
            return counter;
        }
        public static int sixToSeventeen(this List<ChildExtensionModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (years >= 6 && years <= 17)
                    counter++;
            }
            return counter;
        }
        public static int Eighteen(this List<ChildExtensionModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (years == 18)
                    counter++;
            }
            return counter;
        }
        
        public static int BornOrEighteen(this List<ChildModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (year == child.birthDate.Year || years == 18)
                    counter++;

            }
            return counter;
        }
        public static int OneToFive(this List<ChildModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (years <= 5 && years > 0)
                    counter++;

            }
            return counter;
        }
        public static int AnderNineteen(this List<ChildModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (years > 5 && years < 19 && years != 18)
                    counter++;

            }
            return counter;
        }
        public static int ThreeOrBorn(this List<ChildModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (years == 3 || years == 0)
                    counter++;

            }
            return counter;
        }
        public static int OneOrTow(this List<ChildModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (years < 3 && years > 0)
                    counter++;

            }
            return counter;
        }
        public static int ThisYear(this List<ChildModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (year == child.birthDate.Year)
                    counter++;
            }
            return counter;
        }
        public static int sixToSeventeen(this List<ChildModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (years >= 6 && years <= 17)
                    counter++;
            }
            return counter;
        }
        public static int Eighteen(this List<ChildModel> children, int year)
        {
            int counter = 0;
            foreach (ChildModel child in children)
            {
                int years = year - child.birthDate.Year;
                if (years == 18)
                    counter++;
            }
            return counter;
        }
        #endregion

        public static bool checkChildAge(this DateTime birthDate, int maxAge)
        {
            return calcDateDiff(birthDate) < maxAge;
        }
        /// <summary>
        /// TODO => return line after checks
        /// DateTime b = new DateTime(DateTime.Now.Year, 12, 31);
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private static int calcDateDiff(DateTime date)
        {
            DateTime zeroTime = new DateTime(1, 1, 1);
            DateTime b = new DateTime(DateTime.Now.Year < date.Year? date.Year : DateTime.Now.Year, 12, 31);
            TimeSpan span = b - date;
            // Because we start at year 1 for the Gregorian
            // calendar, we must subtract a year here.
            return ((zeroTime + span).Year - 1);
        }
    }
}
