﻿using Newtonsoft.Json;

namespace DataAccess.Extensions
{
    public static class JsonExtensions
    {
        public static string ToJSON(this object o)
        {
            var serializerSettings = new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore };
            return JsonConvert.SerializeObject(o, Formatting.None, serializerSettings);
        }
        public static T ToClass<T>(this string json)
        {
            var serializerSettings = new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore };
            return JsonConvert.DeserializeObject<T>(json, serializerSettings);
        }
    }
}
