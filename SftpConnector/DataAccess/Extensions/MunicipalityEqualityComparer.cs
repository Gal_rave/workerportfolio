﻿using DataAccess.Models;

namespace DataAccess.Extensions
{
    public static class MunicipalityEqualityComparer
    {
        public static bool Compare(this MUNICIPALITy m, string val)
        {
            if (m == null || string.IsNullOrWhiteSpace(val))
                return false;

            string mIdVal = m.ID.ToString();
            string mRashutVal = m.RASHUT.ToString(); 
            return mIdVal.StartsWith(val) || mIdVal.Contains(val) || mIdVal.EndsWith(val) || mIdVal == val || mRashutVal.StartsWith(val) || mRashutVal.Contains(val) || mRashutVal.EndsWith(val) || mRashutVal == val;
        }
    }
}
