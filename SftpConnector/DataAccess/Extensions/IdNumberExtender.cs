﻿using System;
using System.Collections.Generic;
using DataAccess.Models;

namespace DataAccess.Extensions
{
    public static class IdNumberExtender
    {
        public static bool validateIDNumber(string idNumber)
        {
            List<int> numbers = fixLength(idNumber, 9).ToIntList(' ');

            int sumOfAllInts = sumAllDigits(numbers) % 10;

            return sumOfAllInts == 0;
        }
        public static string GenerateCalculatedIdNumber(string idNumber)
        {
            int calculated = idNumber.Substring(0, idNumber.Length - 1).ToInt();
            return calculated.ToString();
        }
        public static string GenerateFullIdNumber(string calculatedIdNumber)
        {
            calculatedIdNumber = fixLength(calculatedIdNumber, 8);
            List<int> numbers = calculatedIdNumber.ToIntList(' ');
            int sumOfAllInts = sumAllDigits(numbers);
            int checkSumDigit = 10 - (sumOfAllInts % 10);
            if ((checkSumDigit + sumOfAllInts) % 10 != 0)
                throw new MembershipException("checkSumException", 11);
            if (calculatedIdNumber.Length < 9)
                calculatedIdNumber += checkSumDigit > 9 ? (checkSumDigit - 10).ToString() : checkSumDigit.ToString();
            return calculatedIdNumber;
        }
        public static string CheckAndGenerateFullIdNumber(string calculatedIdNumber)
        {
            if (string.IsNullOrWhiteSpace(calculatedIdNumber))
                throw new NullReferenceException("NullIdNumberException");
            if (calculatedIdNumber.Length > 9)
                throw new Exception("IdNumberToLongException");

            if (calculatedIdNumber.Length == 9)
                return calculatedIdNumber;

            calculatedIdNumber = fixLength(calculatedIdNumber, 8);
            if (checklastDigit(calculatedIdNumber))
            {
                string _calculatedIdNumber = calculatedIdNumber;
                while (_calculatedIdNumber.Length < 9)
                    _calculatedIdNumber = "0" + _calculatedIdNumber;

                if (validateIDNumber(_calculatedIdNumber))
                    return _calculatedIdNumber;
            }

            List<int> numbers = calculatedIdNumber.ToIntList(' ');
            int sumOfAllInts = sumAllDigits(numbers);
            int checkSumDigit = 10 - (sumOfAllInts % 10);
            if ((checkSumDigit + sumOfAllInts) % 10 != 0)
                throw new MembershipException("checkSumException", 11);
            if (calculatedIdNumber.Length < 9)
                calculatedIdNumber += checkSumDigit > 9 ? (checkSumDigit - 10).ToString() : checkSumDigit.ToString();
            return calculatedIdNumber;
        }

        private static int sumAllDigits(List<int> numbers)
        {
            int sumOfAllInts = 0;
            for (int i = 0; i < numbers.Count; i++)
            {
                numbers[i] = (i % 2 == 0 ? 1 : 2) * numbers[i];
                sumOfAllInts += sumDigits(numbers[i]);
            }
            return sumOfAllInts;
        }
        private static int sumDigits(int num)
        {
            int sum = 0;
            while (num != 0)
            {
                sum += num % 10;
                num /= 10;
            }
            return sum;
        }
        private static string fixLength(string str, int length)
        {
            while (str.Length < length)
                str = "0" + str;
            return str;
        }
        private static string returnLastDigit(string calculatedIdNumber)
        {
            List<int> numbers = calculatedIdNumber.ToIntList(' ');
            int sumOfAllInts = sumAllDigits(numbers);
            int checkSumDigit = 10 - (sumOfAllInts % 10);
            if ((checkSumDigit + sumOfAllInts) % 10 != 0)
                throw new MembershipException("checkSumException", 11);
            return checkSumDigit > 9 ? (checkSumDigit - 10).ToString() : checkSumDigit.ToString();
        }
        private static bool checklastDigit(string calculatedIdNumber)
        {
            try
            {
                string _calculatedIdNumber = fixLength(calculatedIdNumber.Substring(0, calculatedIdNumber.Length - 1), 8);
                return calculatedIdNumber.EndsWith(returnLastDigit(_calculatedIdNumber), StringComparison.OrdinalIgnoreCase); ;
            }
            catch
            {
                return false;
            }
        }
    }
}
