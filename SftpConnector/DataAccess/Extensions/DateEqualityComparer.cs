﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Extensions
{
    public class DateEqualityComparer : IEqualityComparer<DateTime>
    {
        public int GetHashCode(DateTime co)
        {
            if (co == null)
            {
                return 0;
            }
            return co.GetHashCode();
        }

        public bool Equals(DateTime x1, DateTime x2)
        {
            if (object.ReferenceEquals(x1, x2))
            {
                return true;
            }
            if (object.ReferenceEquals(x1, null) || object.ReferenceEquals(x2, null))
            {
                return false;
            }
            return x1.Date.CompareTo(x2.Date) == 0;
        }
    }
}
