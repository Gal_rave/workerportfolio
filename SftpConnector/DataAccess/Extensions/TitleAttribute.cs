﻿using System;
using System.Linq;

namespace DataAccess.Extensions
{
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public class TitleAttribute : Attribute
    {
        // This is a positional argument
        public TitleAttribute(string titleString)
        {
            this.Title = titleString;
        }

        // This is a named argument
        public string Title { get; set; }

        public static string GetAttributeCustom<T>(string member, bool returnName = false) where T : class
        {
            try
            {
                var attr = typeof(T).GetMembers().First(m => m.Name == member).GetCustomAttributes(typeof(TitleAttribute), false).FirstOrDefault();
                if (attr != null)
                    return ((TitleAttribute)attr).Title;
                return returnName? typeof(T).GetMembers().First(m => m.Name == member).Name : string.Empty;
            }
            catch (SystemException ex)
            {
                Logger.Log.LogError(ex);
                return null;
            }
        }
    }
}