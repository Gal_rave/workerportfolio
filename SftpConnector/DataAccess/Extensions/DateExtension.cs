﻿using DataAccess.Models;
using System;

namespace DataAccess.Extensions
{
    public static class DateExtension
    {
        public static int ddmmyyyy(this DateTime val)
        {
            return string.Format("{0}{1}{2}", Pad(val.Day), Pad(val.Month), val.Year).ToInt();
        }
        public static string DDMMYYYY(this DateTime val)
        {
            return string.Format("{0}/{1}/{2}", Pad(val.Day), Pad(val.Month), val.Year);
        }
        public static string Pad(int val)
        {
            if (val < 10)
                return "0" + val.ToString();
            return val.ToString();
        }
        public static DateTime IntToDate(this int date)
        {
            ///1010000 => is lowest int to convert into date
            string str = date.ToString();
            if (date < 1010000 || str.Length < 7)
                return new DateTime();

            DateTime now;
            int length = str.Length;
            string year = str.Substring(length - 4, 4);
            string month = str.Substring(length - 6, 2);
            string day = str.Substring(0, length - 6);
            now = new DateTime(year.ToInt(), month.ToInt(), day.ToInt());

            return now;
        }
        public static DateTime? StringToDate(this string date, bool reverse = false)
        {
            if (string.IsNullOrWhiteSpace(date) || date.Length < 7 || date.ToInt() < 1010000)
                return null;
            if (reverse)
                return date.stringToDate();
            DateTime now;
            int length = date.Length;
            string year = date.Substring(0, 4);
            string month = date.Substring(4, 2);
            string day = date.Substring(6, length - 6);
            now = new DateTime(year.ToInt(), month.ToInt(), day.ToInt());

            return now;
        }
        public static DateTime ToDate(this string date)
        {
            DateTime? d = date.StringToDate();
            if (d.HasValue)
                return d.Value;
            return new DateTime();
        }
        public static DateTime maxDateToDate(MunicipalityUpcbmModel municipalityUpcbmModel)
        {
            string cpm = municipalityUpcbmModel.max_date.ToString();
            return new DateTime(cpm.Substring(0, 4).ToInt(), cpm.Substring(4, (cpm.Length - 4)).ToInt(), 1);
        }

        private static DateTime stringToDate(this string date)
        {
            DateTime now = new DateTime();
            int length = date.Length;
            string year = date.Substring(length - 4, 4);
            string month = date.Substring(length - 6, 2);
            string day = date.Substring(0, (length == 8 ? 2 : 1));
            now = new DateTime(year.ToInt(), month.ToInt(), day.ToInt());

            return now;
        }
    }
}
