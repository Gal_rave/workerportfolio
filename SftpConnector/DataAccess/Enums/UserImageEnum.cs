﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Enums
{
    public enum UserImageEnum
    {
        TLUSH = 1,
        T106 = 2,
        FORM101_UPLOAD = 3,
        FORM101 = 4,
        FORM101_SIGNATURE = 5
    }
}