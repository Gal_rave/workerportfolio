﻿namespace DataAccess.Enums
{
    public enum FormTypeEnum
    {
        _2018 = 1,
        _2019 = 2,
        _Retired = 3
    }
}
