﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Catel.Data;
using System;

using DataAccess.Interfaces;
using DataAccess.Mappers;
using DataAccess.Models;
using DataAccess.Extensions;
using Logger;
using HttpCookieHandler;



namespace DataAccess.BLLs
{
    public class MailerBll : BllExtender
    {
        public List<UserModel> GetUsersByMunicipalityId(int municipalityId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    return userRepository.GetQuery(u => u.EMAIL != null && u.MUNICIPALITIES.FirstOrDefault(m => m.ID == municipalityId) != null)
                        .ToList().Select(u => u.Map(false)).ToList();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        
    }
}
