﻿using System;
using System.Collections.Generic;
using System.Linq;
using OfficeOpenXml;
using System.IO;
using Logger;
using DataAccess.Models;

namespace DataAccess.BLLs
{
    public class ExcelBll
    {
        public void CreatePCUE_Excel(List<PayCheckReportModel> report,string basePath, string FileName, string WorksheetName = "Worksheet")
        {
            string[] headers = new string[]
              {
                " תעודת זהות ", " שם מלא ", " אישור תלוש במייל ", " תאריך רישום ", " תאריך עדכון ", " מקור רישום ", " תאריך שליחה ", " כתובת מייל ", " קוד סיום ", " סיבת סיום ", " תאריך סיום"
              };
            
            try
            {
                using (ExcelPackage excel = new ExcelPackage())
                {
                    ///ad work sheet
                    excel.Workbook.Worksheets.Add(WorksheetName);
                    ExcelWorksheet excelWorksheet = excel.Workbook.Worksheets[WorksheetName];

                    ///set the active cells
                    string headerRange = "A1:" + Char.ConvertFromUtf32(headers.Length + 64) + "1";
                    
                    ///style the work sheet 
                    excelWorksheet.Cells[headerRange].LoadFromArrays(new List<string[]>() { headers });
                    excelWorksheet.Cells[headerRange].Style.Font.Bold = true;
                    excelWorksheet.Cells[headerRange].Style.Font.Size = 14;
                    excelWorksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    excelWorksheet.Cells[headerRange].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

                    ///set up the data
                    List<object[]> data = report.Select(r => new object[] {
                        r.idNumber, r.user_Name,
                        (r.activeRecord.HasValue && r.activeRecord.Value == 1? "כן":"לא"),
                        GetExcelDecimalValueForDate(r.createdDate),
                        GetExcelDecimalValueForDate(r.updateDate),
                        (r.creationSource.HasValue? (r.creationSource.Value == 1? "מכלול":"תיק עובד"):""),
                        r.sendDate.HasValue? r.sendDate.Value.ToString("dd/MM/yyyy HH:mm") : null,
                        r.email, r.terminationCause, r.terminationReason,
                        GetExcelDecimalValueForDate(r.terminationDate)
                    }).ToList();

                    int rowCounter = data.Count + 1;

                    ///insert data from row 2, [from row, from cell, number of rows, to cell]
                    excelWorksheet.Cells[2, 1, rowCounter, 11].LoadFromArrays(data);

                    ///format date cells
                    excelWorksheet.Cells[string.Format("D2:D{0}", rowCounter)].Style.Numberformat.Format = "dd/MM/yyyy";
                    excelWorksheet.Cells[string.Format("E2:E{0}", rowCounter)].Style.Numberformat.Format = "dd/MM/yyyy";
                    excelWorksheet.Cells[string.Format("G2:G{0}", rowCounter)].Style.Numberformat.Format = "dd/MM/yyyy HH:mm";
                    excelWorksheet.Cells[string.Format("K2:K{0}", rowCounter)].Style.Numberformat.Format = "dd/MM/yyyy";

                    ///fit content
                    excelWorksheet.Cells[1, 1, rowCounter, 11].AutoFitColumns();
                    
                    ///save the file
                    FileInfo excelFile = new FileInfo(string.Format(@"{1}\{0}.xlsx", FileName, basePath));
                    excel.SaveAs(excelFile);
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }

        private decimal O_GetExcelDecimalValueForDate(DateTime date)
        {
            DateTime start = new DateTime(1900, 1, 1);
            TimeSpan diff = date - start;
            return diff.Days + 2;
        }
        private decimal? GetExcelDecimalValueForDate(DateTime? date)
        {
            if (!date.HasValue)
                return null;
            DateTime start = new DateTime(1900, 1, 1);
            TimeSpan diff = date.Value - start;
            return diff.Days + 2;
        }
    }
}
