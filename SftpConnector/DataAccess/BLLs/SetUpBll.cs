﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data;
using System.Linq;
using System;

using Logger;
using Catel.Data;
using AsyncHelpers;
using DataAccess.Models;
using DataAccess.Mappers;
using DataAccess.Extensions;
using DataAccess.Interfaces;
using DataAccess.Enums;
using System.Configuration;
using System.Web;

namespace DataAccess.BLLs
{

    public class SetUpBll : BllExtender
    {
        public void SaveDatatable2(DataTable dt, int fileCounter)
        {
            Log.ApplicationLog(string.Format("start SaveDatatable2 ({0}) ,fileCounter => {1}", dt.Rows.Count, fileCounter));
            int callIndex = 0;
            int chunk = ConfigurationManager.AppSettings["chunker"].ToInt();
            foreach (IEnumerable<INSERTED_SYSTEMUSERS> part in dt.Rows.Cast<DataRow>().Select(r => r.Map()).ToList().Chunk(chunk))
            {
                AsyncFunctionCaller.RunAsync(() => { doSave(part, callIndex++, fileCounter); });
            }

            Log.ApplicationLog(string.Format("end SaveDatatable2 ({0}), callIndex => {1} ,fileCounter => {2}", dt.Rows.Count, callIndex, fileCounter));
        }
        private void doSave(IEnumerable<INSERTED_SYSTEMUSERS> data, int callIndex, int fileCounter)
        {
            DateTime NOW = DateTime.Now;
            Log.ApplicationLog(string.Format("start doSave ({0}) ,fileCounter => {1}", callIndex, fileCounter));
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IInsertedSystemUsersRepository inserted = session.GetRepository<IInsertedSystemUsersRepository>();
                data.ToList().ForEach(d=> inserted.Add(d));
                session.SaveChanges();
            }
            Log.ApplicationLog(string.Format("end doSave ({0}), Time => {1} ,fileCounter => {2}", callIndex, DateTime.Now.Subtract(NOW).TotalSeconds, fileCounter));
        }



        public void SaveDatatable(DataTable dt)
        {
            try
            {
                DateTime NOW = DateTime.Now;
                Log.ApplicationLog(string.Format("Start SaveDatatable ({0})", dt.Rows.Count));
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IInsertedSystemUsersRepository inserted = session.GetRepository<IInsertedSystemUsersRepository>();

                    foreach (DataRow row in dt.Rows)
                    {
                        inserted.Add(row.Map());
                    }
                    session.SaveChanges();
                }
                Log.ApplicationLog(string.Format("End SaveDatatable ({0}), Time => {1}", dt.Rows.Count, DateTime.Now.Subtract(NOW).TotalSeconds));
            }
            catch (Exception ex)
            {
                Log.ApplicationLog("try re-send to trySaveDatatable");
                AsyncFunctionCaller.RunAsync(() => { trySaveDatatable(dt); });
                throw ex.LogError();
            }
        }
        public void SPImportSystemUsers()
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IInsertedSystemUsersRepository inserted = session.GetRepository<IInsertedSystemUsersRepository>();
                    Log.ApplicationLog("run ImportIntoSystemUsers");
                    inserted.ImportIntoSystemUsers();
                    Log.ApplicationLog("end run ImportIntoSystemUsers");
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public void FixSystemUsersIdNumbers(int? callIndex = 0)
        {
            if (callIndex > 20)
                return;
            try
            {
                using (UnitOfWork<PortfolioEntities> session = new UnitOfWork<PortfolioEntities>())
                {
                    ISystemUsersRepository systemUsers = session.GetRepository<ISystemUsersRepository>();
                    Log.ApplicationLog(new { callIndex = callIndex }, "Run FixSystemUsersIdNumbers");

                    int pageSize = ConfigurationManager.AppSettings["FSUINpageSize"].ToInt(); ;
                    int counter = 0;

                    foreach (IEnumerable<decimal> ids in systemUsers.GetQuery(su => su.FULLIDNUMBER == null || su.FULLIDNUMBER.Length != 9).Select(us => us.ID).Chunk(pageSize))
                    {
                        if (counter > 20)
                            break;
                        AsyncFunctionCaller.RunAsync(HttpContext.Current, () =>
                        {
                            createFullIdNumber(ids, ++counter);
                        });
                    }
                    AsyncFunctionCaller.RunAsyncAll();
                    Log.ApplicationLog(new { callIndex = callIndex }, "End Run FixSystemUsersIdNumbers");

                    int suc = systemUsers.GetQuery(su => su.FULLIDNUMBER == null || su.FULLIDNUMBER.Length != 9).Count();
                    if (suc > 10)
                    {
                        Log.ApplicationLog(string.Format("re call FixSystemUsersIdNumbers counter => {0}", suc));
                        FixSystemUsersIdNumbers(++callIndex);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public void SpliteSystemUsersEmails()
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    Log.ApplicationLog("run SpliteSystemUsersEmails");
                    IInsertedSystemUsersRepository isuRepository = session.GetRepository<IInsertedSystemUsersRepository>();
                    List<decimal> su = isuRepository.GetQuery(s => s.EMAIL.Contains(";")).ToList().Select(s => s.ID).Distinct().ToList();

                    int index = 0;
                    foreach (IEnumerable<decimal> usersChunk in su.Chunk<decimal>(525))
                    {
                        AsyncFunctionCaller.RunAsync(HttpContext.Current, () =>
                        {
                            splitChunk(usersChunk, ++index);
                        });
                    }
                    AsyncFunctionCaller.RunAsyncAll();
                    Log.ApplicationLog("end run SpliteSystemUsersEmails");
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public void UpdatFromSystemAndRefresh()
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    Log.ApplicationLog("run UpdatFromSystemAndRefresh");
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    userRepository.UpdatUsersFromSystem();
                    Log.ApplicationLog("end run UpdatFromSystemAndRefresh");
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<PayCheckUsersByMailModel> SynchronizeUsersPayCheckByMail()
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    DateTime from = DateTime.Now.AddDays(-3);
                    IPayCheckUsersByMailRepository payCheckMailRepository = session.GetRepository<IPayCheckUsersByMailRepository>();
                    return payCheckMailRepository.GetQuery(p => p.ACTIVERECORD && !p.CREATIONSOURCE && (p.CREATEDDATE >= from || (p.UPDATEDATE.HasValue && p.UPDATEDATE.Value >= from)))
                        .Include("USER").OrderBy(p => p.MUNICIPALITYID).ToList().Select(p => p.Map(false)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<PayCheckUsersByMailModel> GetUsersToSendMail(DateTime payMonth, int municipalityId)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IPayCheckUsersByMailRepository payCheckMailRepository = session.GetRepository<IPayCheckUsersByMailRepository>();
                    IPcueSendArchiveRepository pcueArchiveRepository = session.GetRepository<IPcueSendArchiveRepository>();

                    List<decimal> sentUserIds = pcueArchiveRepository
                                                    .GetQuery(s => s.MUNICIPALITYID == municipalityId && s.PC_MONTH == payMonth.Month && s.PC_YEAR == payMonth.Year)
                                                    .Select(s => s.USERID).ToList();

                    return payCheckMailRepository.GetQuery(p => p.ACTIVERECORD && p.SENDMAIL && p.MUNICIPALITYID == municipalityId && !sentUserIds.Contains(p.USERID))
                        .Include("USER").Include("USER.EMAILBYMUNICIPALITies").ToList().Select(p => p.Map(true)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<PayCheckUsersByMailModel> GetUsersByFactoryPC(DateTime payMonth, int municipalityId)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IEmailByMunicipalityRepository emailByMunicipalityRepository = session.GetRepository<IEmailByMunicipalityRepository>();
                    IPcueSendArchiveRepository pcueArchiveRepository = session.GetRepository<IPcueSendArchiveRepository>();

                    List<decimal> sentUserIds = pcueArchiveRepository
                                                    .GetQuery(s => s.MUNICIPALITYID == municipalityId && s.PC_MONTH == payMonth.Month && s.PC_YEAR == payMonth.Year)
                                                    .Select(s => s.USERID).ToList();

                    return emailByMunicipalityRepository.GetQuery(e => e.MUNICIPALITYID == municipalityId && e.USERID.HasValue && !sentUserIds.Contains(e.USERID.Value))
                                                    .Include("USER").ToList().Select(e => e.MapEtoP()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public void UpdateFactoryPCU(PayCheckUsersByMailModel model)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IPcueSendArchiveRepository pcueArchiveRepository = session.GetRepository<IPcueSendArchiveRepository>();
                    PCUE_SENDARCHIVE pcue = new PCUE_SENDARCHIVE
                    {
                        MUNICIPALITYID = model.municipalityId,
                        USERID = model.userId,
                        PC_MONTH = model.lastSendMonth.HasValue ? model.lastSendMonth.Value : (model.lastSendDate.HasValue ? model.lastSendDate.Value.Month : DateTime.Now.Month),
                        PC_YEAR = model.lastSendYear.HasValue ? model.lastSendYear.Value : (model.lastSendDate.HasValue ? model.lastSendDate.Value.Year : DateTime.Now.Year),
                        SENDDATE = DateTime.Now
                    };

                    Log.PcuLog(model, "UpdateFactoryPCU => paycheckusersbymailmodel");
                    Log.PcuLog(pcue, "UpdateFactoryPCU => pcue_sendarchive");
                    pcueArchiveRepository.Add(pcue);
                    session.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public void UpdatePCU(PayCheckUsersByMailModel model)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IPayCheckUsersByMailRepository payCheckMailRepository = session.GetRepository<IPayCheckUsersByMailRepository>();
                    PAYCHECKUSERSBYMAIL pcum = payCheckMailRepository.First(p => p.ID == model.ID);
                    pcum = model.Map();
                    payCheckMailRepository.Update(pcum);

                    IPcueSendArchiveRepository pcueArchiveRepository = session.GetRepository<IPcueSendArchiveRepository>();

                    PCUE_SENDARCHIVE pcue = new PCUE_SENDARCHIVE
                    {
                        MUNICIPALITYID = pcum.MUNICIPALITYID,
                        USERID = pcum.USERID,
                        PC_MONTH = pcum.LASTSENDMONTH.HasValue ? pcum.LASTSENDMONTH.Value : (pcum.LASTSENDDATE.HasValue ? pcum.LASTSENDDATE.Value.Month : DateTime.Now.Month),
                        PC_YEAR = pcum.LASTSENDYEAR.HasValue ? pcum.LASTSENDYEAR.Value : (pcum.LASTSENDDATE.HasValue ? pcum.LASTSENDDATE.Value.Year : DateTime.Now.Year),
                        SENDDATE = DateTime.Now
                    };

                    Log.PcuLog(model, "UpdatePCU=> paycheckusersbymailmodel");
                    Log.PcuLog(pcum, "UpdatePCU=> paycheckusersbymail");
                    Log.PcuLog(pcue, "UpdatePCU=> pcue_sendarchive");
                    pcueArchiveRepository.Add(pcue);
                    session.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        /// <summary>
        /// FACTORYEMAILPC 
        /// 0 => dont allow
        /// 1 => send to all
        /// 2 => allow to register 
        /// </summary>
        /// <param name="factoryPC"></param>
        /// <returns></returns>
        public List<int> GetMunicipalitiesForUPCBM(bool factoryPC = false)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IPayCheckUsersByMailRepository payCheckMailRepository = session.GetRepository<IPayCheckUsersByMailRepository>();
                    IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                    IQueryable<MUNICIPALITy> FPC_municipalities = municipalityRepository.GetQuery(m => m.ACTIVE && m.ALLOWEMAILPAYCHECK && m.FACTORYEMAILPC.HasValue && m.FACTORYEMAILPC.Value > 0);
                    List<decimal> factoryPCmunicipalities;
                    if (factoryPC)
                    {
                        factoryPCmunicipalities = FPC_municipalities.Where(m => m.FACTORYEMAILPC.Value == 1).Select(m => m.ID).ToList();
                        if (factoryPCmunicipalities == null)
                            factoryPCmunicipalities = new List<decimal>();

                        List<int> factorPCm = factoryPCmunicipalities.ToIntList();
                        ///insert recorcs for users in factory municipality
                        factorPCm.ForEach(f => payCheckMailRepository.InsertGlobalMunicipalityPCUE(f));

                        return factorPCm;
                    }
                    factoryPCmunicipalities = FPC_municipalities.Where(m => m.FACTORYEMAILPC.Value == 2).Select(m => m.ID).ToList();
                    return payCheckMailRepository.GetQuery(p => p.ACTIVERECORD && p.SENDMAIL && factoryPCmunicipalities.Contains(p.MUNICIPALITYID)).GroupBy(p => p.MUNICIPALITYID).Select(p => p.Key).ToIntList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public bool UpdateMunicipalities(List<MUNICIPALITy> municipalities)
        {
            bool success = true;
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();

                    MUNICIPALITy municipality = new MUNICIPALITy();
                    municipalities.Distinct().ToList().ForEach(m =>
                    {
                        try
                        {
                            municipality = municipalityRepository.FirstOrDefault(qm => qm.ID == m.ID);
                            if (municipality != null)
                            {
                                municipality.RASHUT = m.RASHUT;
                                municipality.FACTORYEMAILPC = m.FACTORYEMAILPC;
                                municipality.ALLOWEMAILPAYCHECK = m.ALLOWEMAILPAYCHECK;
                                municipalityRepository.Update(municipality);
                            }
                            else
                            {
                                municipality = m;
                                municipalityRepository.Attach(municipality);
                                municipalityRepository.Add(municipality);
                            }
                            session.SaveChanges();
                        }
                        catch (Exception Iex)
                        {
                            Iex.LogError();
                            success = false;
                        }
                    });
                    return success;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public bool UpdateMunicipalities(List<MunicipalitySchemaExtension> schemaMunicipalities)
        {
            bool success = true;
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                    IMunicipalityContactRepository municipalityContactRepository = session.GetRepository<IMunicipalityContactRepository>();

                    MUNICIPALITy municipality = new MUNICIPALITy();
                    List<MunicipalityModelExtension> municipalities = schemaMunicipalities.Select(m => m.MapSchema()).ToList();

                    municipalities.ForEach(m =>
                    {
                        municipality = municipalityRepository.Include("MUNICIPALITYCONTACTs").FirstOrDefault(qm => qm.ID == m.ID);
                        try
                        {
                            if (municipality != null)
                            {
                                municipality.RASHUT = m.rashut;
                                municipality.FACTORYEMAILPC = m.factoryEmailPC;
                                municipality.ALLOWEMAILPAYCHECK = m.allowemailpaycheck;
                                ////update the contacts
                                updateMunicipalityContact(municipalityContactRepository, municipality, m.contact);
                                municipalityRepository.Update(municipality);
                            }
                            else
                            {
                                municipality = m.Map();
                                ////update the contacts
                                updateMunicipalityContact(municipalityContactRepository, municipality, m.contact);
                                municipalityRepository.Attach(municipality);
                                municipalityRepository.Add(municipality);
                            }

                            session.SaveChanges();
                        }
                        catch (Exception Iex)
                        {
                            Iex.LogError();
                            success = false;
                        }
                    });
                    return success;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public void DistinctUserCells(DateTime NOW)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    List<USER> users = userRepository.GetQuery(u => u.CELL != null && u.CELL.Length > 1 && u.CELL.Contains(",")).ToList();
                    Log.ApplicationLog(string.Format("DistinctUserCells GET USERS ,time => {0}, counter => {1}", DateTime.Now.Subtract(NOW).TotalSeconds, users.Count));

                    foreach (IEnumerable<USER> IEusers in users.Chunk(250))
                    {
                        AsyncFunctionCaller.RunAsync(() =>
                        {
                            doDistinctCells(session, IEusers);
                        });
                    }
                    AsyncFunctionCaller.RunAsyncAll();

                    Log.ApplicationLog(string.Format("DistinctUserCells GET USERS ,time => {0}, END ALL", DateTime.Now.Subtract(NOW).TotalSeconds));
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public void DistinctUserEmails(DateTime NOW)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    List<USER> users = userRepository.GetQuery(u => u.EMAIL.Contains(",")).ToList();
                    Log.ApplicationLog(string.Format("DistinctUserEmails GET USERS ,time => {0}, counter => {1}", DateTime.Now.Subtract(NOW).TotalSeconds, users.Count));

                    foreach (IEnumerable<USER> IEusers in users.Chunk(250))
                    {
                        AsyncFunctionCaller.RunAsync(() =>
                        {
                            doDistinctEmails(session, IEusers);
                        });
                    }
                    AsyncFunctionCaller.RunAsyncAll();

                    Log.ApplicationLog(string.Format("DistinctUserEmails GET USERS ,time => {0}, END ALL", DateTime.Now.Subtract(NOW).TotalSeconds));
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<USER> GetAllMunicipalityUsers(DateTime payMonth, int municipalityId)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    Log.PcuLog(string.Format("Start GetAllMunicipalityUsers for municipality =>{2},  Date => {1}/{0}", payMonth.Year, payMonth.Month, municipalityId), "GetAllMunicipalityUsers");

                    IEmailByMunicipalityRepository emailByMunicipalityRepository = session.GetRepository<IEmailByMunicipalityRepository>();
                    IUserImageRepository userImageRepository = session.GetRepository<IUserImageRepository>();

                    List<EMAILBYMUNICIPALITY> emails = emailByMunicipalityRepository.GetQuery(e => e.USERID.HasValue && e.MUNICIPALITYID == municipalityId).Include("USER").Include("USER.USERSTOFACTORIES").ToList();

                    List<decimal> usersIds = emails.Select(e => e.USERID.Value).ToList();
                    List<decimal> images = userImageRepository.GetQuery(i =>
                                                        i.TYPE == UserImageEnum.TLUSH.ToString() &&
                                                        i.FILEDATE.HasValue &&
                                                        i.FILEDATE.Value.Year == payMonth.Year &&
                                                        i.FILEDATE.Value.Month == payMonth.Month &&
                                                        usersIds.Contains(i.USERID)
                                                    ).Select(i => i.USERID).ToList();

                    Log.PcuLog(string.Format("End GetAllMunicipalityUsers for municipality =>{2},  Date => {1}/{0}", payMonth.Year, payMonth.Month, municipalityId), "GetAllMunicipalityUsers");
                    return emails.Where(e => !images.Contains(e.USERID.Value)).Select(e => e.USER).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public void UpdateTerminationCause(List<TERMINATION> list)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    ITerminationRepository terminationRepository = session.GetRepository<ITerminationRepository>();
                    ///remove old records
                    terminationRepository.GetAll().ToList()
                        .ForEach(t =>
                            terminationRepository.Delete(t)
                        );
                    ///insert new records
                    list.Distinct(new TerminationEqualityComparer()).ToList().ForEach(t =>
                        {
                            Log.TestLog(t);
                            terminationRepository.Add(t);
                        }
                    );
                    session.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<int> GetMunicipalitiesForUPCEReport(DateTime reportDate)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IPcueSendArchiveRepository pcueArchiveRepository = session.GetRepository<IPcueSendArchiveRepository>();
                    List<int> municipalities = pcueArchiveRepository.GetQuery(pa =>
                        pa.SENDDATE.Year == reportDate.Year && pa.SENDDATE.Month == reportDate.Month && pa.SENDDATE.Day == reportDate.Day
                    ).ToList().Select(pa => pa.MUNICIPALITYID).ToIntList().Distinct().ToList();

                    return municipalities;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }

        #region private functions
        private void splitChunk(IEnumerable<decimal> usersChunk, int num)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    Log.ApplicationLog(string.Format("Start splitChunk => {0}, size => {1}", num, usersChunk.Count()));
                    IInsertedSystemUsersRepository repo = session.GetRepository<IInsertedSystemUsersRepository>();
                    List<INSERTED_SYSTEMUSERS> su = repo.GetQuery(s => usersChunk.Contains(s.ID)).ToList();
                    INSERTED_SYSTEMUSERS newSu;
                    string[] mails;
                    su.ForEach(s =>
                    {
                        try
                        {
                            mails = s.EMAIL.Split(';');
                            if (mails != null && mails.Length > 0)
                            {
                                mails.ToList().ForEach(m =>
                                {
                                    newSu = s.Clone<INSERTED_SYSTEMUSERS>();
                                    newSu.EMAIL = m;
                                    repo.Add(newSu);
                                });
                                repo.Delete(s);
                            }
                        }
                        catch (Exception Iex)
                        {
                            Iex.LogError(s);
                        }
                    });
                    session.SaveChanges();
                    Log.ApplicationLog(string.Format("End splitChunk => {0}", num));
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
        }
        private void updateMunicipalityContact(IMunicipalityContactRepository repository, MUNICIPALITy municipality, MunicipalityContactModel contact)
        {
            if (contact == null || municipality == null)
                return;

            try
            {
                MUNICIPALITYCONTACT mc = municipality.MUNICIPALITYCONTACTs.FirstOrDefault(c => c.MIFAL == contact.mifal);
                if (mc != null)
                {
                    mc = contact.Map(mc.ID);
                    repository.Update(mc);
                }
                else
                {
                    municipality.MUNICIPALITYCONTACTs.Add(contact.Map());
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        private void doDistinctEmails(UnitOfWork<PortfolioEntities> session, IEnumerable<USER> users)
        {
            try
            {
                List<string> mails;
                foreach (USER usr in users.Where(u => u.EMAIL != null && u.EMAIL.Length > 1).ToList())
                {
                    try
                    {
                        mails = usr.EMAIL.Split(',').ToList().Where(c => c.Length > 5).Select(c => c.Trim().ToLower()).ToList().Distinct().ToList();
                        usr.EMAIL = string.Join(",", mails);
                    }
                    catch (Exception ex)
                    {
                        ex.LogError(usr);
                    }
                }
                session.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        private void doDistinctCells(UnitOfWork<PortfolioEntities> session, IEnumerable<USER> users)
        {
            try
            {
                List<string> cells;
                foreach (USER usr in users.Where(u => u.CELL != null && u.CELL.Length > 1 && u.CELL.Contains(",")).ToList())
                {
                    try
                    {
                        cells = usr.CELL.Split(',').Where(c => c.Length > 8).Select(c => c.Trim()).ToList().Distinct().ToList();
                        usr.CELL = string.Join(",", cells);
                    }
                    catch (Exception ex)
                    {
                        ex.LogError(usr);
                    }
                }
                session.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        private void trySaveDatatable(DataTable dt)
        {
            try
            {
                DateTime NOW = DateTime.Now;
                Log.ApplicationLog(string.Format("Start trySaveDatatable ({0})", dt.Rows.Count));
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IInsertedSystemUsersRepository inserted = session.GetRepository<IInsertedSystemUsersRepository>();

                    foreach (DataRow row in dt.Rows)
                    {
                        try
                        {
                            inserted.Add(row.Map());
                        }
                        catch (Exception IEX)
                        {
                            IEX.LogError(row);
                        }
                    }
                    session.SaveChanges();
                }
                Log.ApplicationLog(string.Format("End trySaveDatatable ({0}), Time => {1}", dt.Rows.Count, DateTime.Now.Subtract(NOW).TotalSeconds));
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        private void createFullIdNumber(IEnumerable<decimal> uIds, int counter)
        {
            try
            {
                Log.ApplicationLog(string.Format("Start createFullIdNumber Index => {0}", counter));
                using (UnitOfWork<PortfolioEntities> session = new UnitOfWork<PortfolioEntities>())
                {
                    ISystemUsersRepository systemUsers = session.GetRepository<ISystemUsersRepository>();
                    List<SYSTEM_USERS> system_users = systemUsers.GetQuery(su => uIds.ToList().Contains(su.ID) && (su.FULLIDNUMBER == null || su.FULLIDNUMBER.Length != 9)).ToList();
                    system_users.ForEach(su =>
                    {
                        try
                        {
                            su.FULLIDNUMBER = IdNumberExtender.GenerateFullIdNumber(su.IDNUMBER);
                        }
                        catch (Exception Iex)
                        {
                            Iex.LogError(su);
                        }
                    });
                    session.SaveChanges();
                    Log.ApplicationLog(string.Format("End createFullIdNumber Index => {0}", counter));
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
        }
        #endregion
    }
}
