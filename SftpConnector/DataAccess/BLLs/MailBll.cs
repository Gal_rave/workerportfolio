﻿using System;
using System.Linq;
using System.Collections.Generic;

using Catel.Data;
using DataAccess.Mappers;
using DataAccess.Models;
using DataAccess.Interfaces;
using Logger;
using DataAccess.Enums;

namespace DataAccess.BLLs
{
    public class MailBll : BllExtender
    {
        public PostNotificationMail SaveNotification(MAILNOTIFICATION model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMailNotificationRepository notificationRepository = session.GetRepository<IMailNotificationRepository>();
                notificationRepository.Add(model);
                doSave<MAILNOTIFICATION>(session, model);
                return model.Map();
            }
        }
        public List<PostNotificationMail> GetUserResetNotifications(string idNumber, MailTypeEnum type, DateTime? dateSpan = null)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMailNotificationRepository notificationRepository = session.GetRepository<IMailNotificationRepository>();
                return notificationRepository.GetQuery(n => 
                        n.USERIDNUMBER != null && n.USERIDNUMBER == idNumber && 
                        n.MAILTYPE == (int)type &&
                        (!dateSpan.HasValue || n.CREATEDDATE >= dateSpan.Value)
                    ).OrderByDescending(n=> n.CREATEDDATE).ToList().Select(n => n.Map()).ToList();
            }
        }
        public PostNotificationMail SaveNotification(PostNotificationMail model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMailNotificationRepository notificationRepository = session.GetRepository<IMailNotificationRepository>();
                MAILNOTIFICATION n = model.Map();
                notificationRepository.Add(n);
                doSave<MAILNOTIFICATION>(session, n);
                return n.Map();
            }
        }
        public List<PostNotificationMail> GetUserNotifications(string idNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMailNotificationRepository notificationRepository = session.GetRepository<IMailNotificationRepository>();
                return notificationRepository.GetQuery(n => n.USERIDNUMBER != null && n.USERIDNUMBER == idNumber)
                    .Select(n => n.Map()).ToList();
            }
        }
        public PostNotificationMail GetNotificationByID(int id)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMailNotificationRepository notificationRepository = session.GetRepository<IMailNotificationRepository>();
                return notificationRepository.GetByKey(id).Map();
            }
        }
        public List<PostNotificationMail> SearchNotifications(string idNumber = null, string subject = null, string text = null, string userName = null, string senderEmail = null, string recipientEmail = null)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMailNotificationRepository notificationRepository = session.GetRepository<IMailNotificationRepository>();
                IQueryable<MAILNOTIFICATION> mn = notificationRepository.GetAll();
                if (!string.IsNullOrWhiteSpace(idNumber))
                    mn = mn.Where(n => n.USERIDNUMBER != null && (
                    n.USERIDNUMBER.StartsWith(idNumber) || 
                    n.USERIDNUMBER.EndsWith(idNumber) || 
                    n.USERIDNUMBER.Contains(idNumber) ||
                    n.USERIDNUMBER == idNumber
                    ));

                if (!string.IsNullOrWhiteSpace(subject))
                    mn = mn.Where(n => n.SUBJECT != null && (
                    n.SUBJECT.StartsWith(subject) ||
                    n.SUBJECT.EndsWith(subject) ||
                    n.SUBJECT.Contains(subject) ||
                    n.SUBJECT == subject
                    ));

                if (!string.IsNullOrWhiteSpace(text))
                    mn = mn.Where(n => n.FREETEXT != null && (
                    n.FREETEXT.StartsWith(text) ||
                    n.FREETEXT.EndsWith(text) ||
                    n.FREETEXT.Contains(text) ||
                    n.FREETEXT == text
                    ));

                if (!string.IsNullOrWhiteSpace(userName))
                    mn = mn.Where(n => n.USERNAME != null && (
                    n.USERNAME.StartsWith(userName) ||
                    n.USERNAME.EndsWith(userName) ||
                    n.USERNAME.Contains(userName) ||
                    n.USERNAME == userName
                    ));

                if (!string.IsNullOrWhiteSpace(senderEmail))
                    mn = mn.Where(n => n.SENDEREMAIL != null && (
                    n.SENDEREMAIL.StartsWith(senderEmail) ||
                    n.SENDEREMAIL.EndsWith(senderEmail) ||
                    n.SENDEREMAIL.Contains(senderEmail) ||
                    n.SENDEREMAIL == senderEmail
                    ));

                if (!string.IsNullOrWhiteSpace(recipientEmail))
                    mn = mn.Where(n => n.RECIPIENTEMAIL != null && (
                    n.RECIPIENTEMAIL.StartsWith(recipientEmail) ||
                    n.RECIPIENTEMAIL.EndsWith(recipientEmail) ||
                    n.RECIPIENTEMAIL.Contains(recipientEmail) ||
                    n.RECIPIENTEMAIL == recipientEmail
                    ));

                return mn.ToList().Select(n => n.Map()).ToList();
            }
        }
        public void SaveSMS(SmsNotificationModel model)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IsmsNotificationRepository smsRepository = session.GetRepository<IsmsNotificationRepository>();
                    SMSNOTIFICATION sms = new SMSNOTIFICATION();
                    if (model.ID > 0)
                    {
                        sms = smsRepository.GetByKey(model.ID);
                        sms.MESSAGE = model.message;
                        sms.PHONENUMBER = model.phoneNumber;
                        sms.RESPONSE = model.response;
                        sms.SENDDATE = model.sendDate;
                        sms.USERID = model.userId;
                        smsRepository.Update(sms);
                    }
                    else
                    {
                        sms = model.Map();
                        smsRepository.Add(sms);
                    }
                    session.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<humanResourcesContactModel> GetContacts(int municipalityId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IHumanResourcesContactRepository contactRepository = session.GetRepository<IHumanResourcesContactRepository>();
                return contactRepository.GetQuery(c => c.MUNICIPALITYID == municipalityId).ToList().Select(c => c.Map(false)).ToList();
            }
        }
        public humanResourcesContactModel GetContactByID(int id)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IHumanResourcesContactRepository contactRepository = session.GetRepository<IHumanResourcesContactRepository>();
                return contactRepository.First(c => c.ID == id).Map();
            }
        }
        public List<SmsNotificationModel> GetUserSmsMessages(int userId, DateTime searchDate)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IsmsNotificationRepository smsRepository = session.GetRepository<IsmsNotificationRepository>();
                return smsRepository.GetQuery(s => s.USERID == userId && s.SENDDATE >= searchDate).OrderByDescending(s => s.SENDDATE).ToList().Select(s => s.Map()).ToList();
            }
        }
    }
}
