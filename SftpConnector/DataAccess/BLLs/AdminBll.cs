﻿using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Data;
using System;

using Catel.Data;
using DataAccess.Interfaces;
using DataAccess.Mappers;
using DataAccess.Models;
using DataAccess.Extensions;
using DataAccess.Enums;
using Logger;

namespace DataAccess.BLLs
{
    public class AdminBll : BllExtender
    {
        public List<UserModel> SearchByFirstName(string query, int counter = 10)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                query = query.ToLower();

                return userRepository
                        .GetQuery(u => u.FIRSTNAME.ToLower().StartsWith(query) || u.FIRSTNAME.ToLower().Contains(query) || u.FIRSTNAME.ToLower().EndsWith(query) || u.FIRSTNAME.ToLower() == query)
                        .OrderBy(c => c.ID)
                        .Take(counter)
                        .ToList().Select(u => u.Map(false)).ToList();
            }
        }
        public List<UserModel> SearchByLastName(string query, int counter = 10)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                query = query.ToLower();

                return userRepository
                        .GetQuery(u => u.LASTNAME.ToLower().StartsWith(query) || u.LASTNAME.ToLower().Contains(query) || u.LASTNAME.ToLower().EndsWith(query) || u.LASTNAME.ToLower() == query)
                        .OrderBy(c => c.ID)
                        .Take(counter)
                        .ToList().Select(u => u.Map(false)).ToList();
            }
        }
        public List<UserModel> SearchByIdNumber(string query, int counter = 10)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                query = query.ToLower();

                return userRepository
                        .GetQuery(u => u.IDNUMBER.ToLower().StartsWith(query) || u.IDNUMBER.ToLower().Contains(query) || u.IDNUMBER.ToLower().EndsWith(query) || u.IDNUMBER.ToLower() == query)
                        .OrderBy(c => c.ID)
                        .Take(counter)
                        .ToList().Select(u => u.Map(false)).ToList();
            }
        }
        public List<UserModel> SearchByCell(string query, int counter = 10)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                query = query.ToLower();

                return userRepository
                        .GetQuery(u => u.CELL.ToLower().StartsWith(query) || u.CELL.ToLower().Contains(query) || u.CELL.ToLower().EndsWith(query) || u.CELL.ToLower() == query)
                        .OrderBy(c => c.ID)
                        .Take(counter)
                        .ToList().Select(u => u.Map(false)).ToList();
            }
        }
        public List<UserModel> Search(UserSearchModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                IQueryable<USER> users = userRepository.GetAll();
                if (model.maxImmunity <= 50)
                {///if user is municipality admin only
                    users = users.Where(u => u.MUNICIPALITIES.FirstOrDefault(m => m.ID == model.municipalityId) != null);
                }

                if (!string.IsNullOrWhiteSpace(model.cell) && model.cell.Length > 0)
                {
                    model.cell = model.cell.ToLower().Trim();
                    users = users.Where(u => u.CELL.StartsWith(model.cell) || u.CELL.Contains(model.cell) || u.CELL.EndsWith(model.cell) || u.CELL == model.cell);
                }
                if (!string.IsNullOrWhiteSpace(model.searchIdNumber) && model.searchIdNumber.Length > 0)
                {
                    model.searchIdNumber = model.searchIdNumber.ToLower().Trim();
                    users = users.Where(u => u.IDNUMBER.StartsWith(model.searchIdNumber) || u.IDNUMBER.Contains(model.searchIdNumber) || u.IDNUMBER.EndsWith(model.searchIdNumber) || u.IDNUMBER == model.searchIdNumber);
                }
                if (!string.IsNullOrWhiteSpace(model.firstName) && model.firstName.Length > 0)
                {
                    model.firstName = model.firstName.ToLower().Trim();
                    users = users.Where(u => u.FIRSTNAME.StartsWith(model.firstName) || u.FIRSTNAME.Contains(model.firstName) || u.FIRSTNAME.EndsWith(model.firstName) || u.FIRSTNAME == model.firstName);
                }
                if (!string.IsNullOrWhiteSpace(model.lastName) && model.lastName.Length > 0)
                {
                    model.lastName = model.lastName.ToLower().Trim();
                    users = users.Where(u => u.LASTNAME.StartsWith(model.lastName) || u.LASTNAME.Contains(model.lastName) || u.LASTNAME.EndsWith(model.lastName) || u.LASTNAME == model.lastName);
                }

                return model.page > 1 ? users.OrderBy(c => c.ID).Skip(model.page * 1000).Take(1000).ToList().Select(u => u.Map(false)).ToList() :
                    users.OrderBy(c => c.ID).Take(1000).ToList().Select(u => u.Map(false)).ToList();
            }
        }
        public UserModel GetUserById(UserSearchModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    if (model.maxImmunity <= 50)
                    {///if user is municipality admin only
                        return userRepository.FirstOrDefault(u => u.ID == model.page && u.MUNICIPALITIES.FirstOrDefault(m => m.ID == model.municipalityId) != null).Map(true);
                    }
                    return userRepository.FirstOrDefault(u => u.ID == model.page).Map(true);
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public MunicipalityExtensionModel GetMunicipality(UserSearchModel model)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                    if (model.maxImmunity <= 50)
                    {
                        return municipalityRepository.First(m => m.ID == model.municipalityId).MapExtension(false);
                    }
                    else
                    {
                        return municipalityRepository.First(m => m.ID == model.page).MapExtension(false);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<MunicipalityModel> GetMunicipalities(UserSearchModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                if (model.maxImmunity <= 50)
                {///if user is municipality admin only, get only his data
                    return municipalityRepository.GetQuery(m => model.municipalities.Contains(m.ID)).ToList().Select(m => m.Map(false)).ToList();
                }

                ///search the list for apper admins
                IQueryable<MUNICIPALITy> Qmunicipality = municipalityRepository.GetAll();
                model.searchMunicipalityName = string.IsNullOrWhiteSpace(model.searchMunicipalityName) ? string.Empty : model.searchMunicipalityName.Trim();
                model.searchMunicipalitiesId = string.IsNullOrWhiteSpace(model.searchMunicipalitiesId) ? string.Empty : model.searchMunicipalitiesId.Trim();

                if (!string.IsNullOrWhiteSpace(model.searchMunicipalityName))
                {
                    Qmunicipality = Qmunicipality.Where(m => m.NAME.Contains(model.searchMunicipalityName) || m.NAME.StartsWith(model.searchMunicipalityName) || m.NAME.EndsWith(model.searchMunicipalityName) || m.NAME == model.searchMunicipalityName);
                }

                if (model.active.HasValue)
                    Qmunicipality = Qmunicipality.Where(o => o.ACTIVE == model.active.Value);
                if (model.has101.HasValue)
                    Qmunicipality = Qmunicipality.Where(o => o.ALLOW101 == model.has101.Value);
                if (model.hasProcess.HasValue)
                    Qmunicipality = Qmunicipality.Where(o => o.ALLOWPROCESS == model.hasProcess.Value);
                if (model.hasEmailPaycheck.HasValue)
                    Qmunicipality = Qmunicipality.Where(o => o.ALLOWEMAILPAYCHECK == model.hasEmailPaycheck.Value);
                if (model.hasMunicipalityMenu.HasValue)
                    Qmunicipality = Qmunicipality.Where(o => o.ALLOWMUNICIPALITYMENU == model.hasMunicipalityMenu.Value);
                if (model.hasSystemsEmail.HasValue)
                    Qmunicipality = Qmunicipality.Where(o => o.ALLOWSYSTEMSEMAIL == model.hasSystemsEmail.Value);

                if (!string.IsNullOrWhiteSpace(model.searchMunicipalitiesId.Trim()))
                {
                    return Qmunicipality.ToList().Where(m => m.Compare(model.searchMunicipalitiesId)).Select(m => m.Map(false)).ToList();
                }

                return Qmunicipality.ToList().Select(m => m.Map(false)).ToList();
            }
        }
        public bool UpdateMunicipalityAuthorizations(UserSearchModel model)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    if (model.maxImmunity <= 70)
                        throw new Exception();

                    IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                    municipalityRepository.Update(model.municipality.Map());
                    session.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return ex.LogError(false);
            }
        }
        public bool UpdateMunicipalities(UserSearchModel model)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    if (model.maxImmunity <= 70)
                        throw new Exception();

                    IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                    IEmailByMunicipalityRepository ebmRepository = session.GetRepository<IEmailByMunicipalityRepository>();

                    model.municipalitiesList.ForEach(m =>
                        municipalityRepository.Update(m.Map())
                    );

                    session.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return ex.LogError(false);
            }
        }
        public List<PayCheckUsersByMailExtensionModel> Parse_UPCBM_Statistics(UserSearchModel model)
        {
            Get_UPCBM_Statistics(model);
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    if (model.maxImmunity <= 40)
                        throw new UnauthorizedAccessException();

                    int municipalityId = model.searchMunicipalitiesId.ToInt();

                    DateTime start = new DateTime(model.searchDate.Year, model.searchDate.Month, 16, 1, 1, 1).AddMonths(-1);
                    DateTime end = new DateTime(model.searchDate.Year, model.searchDate.Month, 16, 1, 1, 1);

                    IPayCheckUsersByMailRepository pcubmRepository = session.GetRepository<IPayCheckUsersByMailRepository>();

                    List<PayCheckUsersByMailExtensionModel> pcubmemList = new List<PayCheckUsersByMailExtensionModel>();
                    PAYCHECKUSERSBYMAIL paycheckusersbymail;
                    PayCheckUsersByMailExtensionModel pcubm_ExtensionModel;

                    List<IGrouping<decimal, PAYCHECKUSERSBYMAIL>> pcubmlist = pcubmRepository.Include("USER").Include("USER.PCUE_SENDARCHIVE")
                                        .Where(p =>
                                            p.MUNICIPALITYID == municipalityId &&
                                            p.SENDMAIL &&
                                            (
                                                (p.ACTIVERECORD && p.CREATEDDATE <= end) ||
                                                p.CREATEDDATE <= end && (!p.UPDATEDATE.HasValue || p.UPDATEDATE.Value >= start)
                                            )
                                        )
                                        .GroupBy(p => p.USERID).ToList();

                    foreach (IGrouping<decimal, PAYCHECKUSERSBYMAIL> pcubm in pcubmlist)
                    {
                        paycheckusersbymail = pcubm.FirstOrDefault(p => p.ACTIVERECORD);
                        if (paycheckusersbymail != null)
                        {
                            pcubmemList.Add(paycheckusersbymail.Map(model.searchDate));
                        }
                        if (paycheckusersbymail == null)
                        {
                            pcubm_ExtensionModel = pcubm.First(p => p.ACTIVERECORD || p.CREATEDDATE == pcubm.Max(pe => pe.CREATEDDATE)).Map(model.searchDate);
                            paycheckusersbymail = pcubm.First(p => p.ACTIVERECORD || p.CREATEDDATE == pcubm.Min(pe => pe.CREATEDDATE));

                            pcubm_ExtensionModel.createdDate = paycheckusersbymail.CREATEDDATE;

                            pcubmemList.Add(pcubm_ExtensionModel);
                        }
                    }

                    return pcubmemList;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<PayCheckReportModel> Get_UPCBM_Statistics(UserSearchModel model)
        {
            try
            {
                if (model.maxImmunity <= 40)
                    throw new UnauthorizedAccessException();

                DateTime start = new DateTime(model.searchDate.Year, model.searchDate.Month, 15, 1, 1, 1);
                DateTime end = new DateTime(model.searchDate.Year, model.searchDate.Month, 15, 1, 1, 1).AddMonths(1);

                using (var db = new PortfolioEntities())
                {
                    return db.Database.SqlQuery<PayCheckReportModel>(@"
SELECT 
     U.ID AS USER_ID, U.IDNUMBER, U.FIRSTNAME || ' ' || U.LASTNAME AS USER_NAME,
    PE.ACTIVERECORD, PE.CREATEDDATE, PE.UPDATEDATE, PE.CREATIONSOURCE,
    AR.SENDDATE, AR.ID as ARCHIVE_ID,
    LOWER(EM.EMAIL) AS EMAIL,
    UT.TERMINATIONCAUSE, T.TERMINATIONREASON, UT.TERMINATIONDATE
FROM USERS U
INNER JOIN USERSTOMUNICIPALITIES UM
    ON UM.USERID = U.ID
LEFT JOIN PAYCHECKUSERSBYMAIL PE
    ON PE.USERID = U.ID AND PE.MUNICIPALITYID = UM.MUNICIPALITYID AND PE.SENDMAIL = 1
    AND (
        (PE.ACTIVERECORD = 1 AND PE.CREATEDDATE <= :enddatedate ) OR
        (PE.CREATEDDATE <= :enddatedate AND (PE.UPDATEDATE IS NULL OR PE.UPDATEDATE >= :startdatedate))
    )
LEFT JOIN PCUE_SENDARCHIVE AR 
    ON AR.USERID = U.ID AND AR.MUNICIPALITYID = UM.MUNICIPALITYID
    AND AR.PC_MONTH = :monthdate AND AR.PC_YEAR = :yeardate
LEFT JOIN (
    SELECT 
        USERID, MUNICIPALITYID, LISTAGG(TRIM(EMAIL),', ') WITHIN GROUP(ORDER BY USERID, MUNICIPALITYID) EMAIL
    FROM EMAILBYMUNICIPALITY
    GROUP BY USERID, MUNICIPALITYID
) EM
    ON EM.USERID = U.ID AND EM.MUNICIPALITYID = UM.MUNICIPALITYID
LEFT JOIN USERSTERMINATIONS UT
    ON UT.USERID = U.ID AND UT.MUNICIPALITYID = UM.MUNICIPALITYID
LEFT JOIN TERMINATIONS T
    ON T.TERMINATIONCAUSE = UT.TERMINATIONCAUSE
WHERE UM.MUNICIPALITYID = :midmid

AND (
    UT.TERMINATIONCAUSE IS NULL
    OR (
        UT.TERMINATIONCAUSE NOT IN (60,50,91)
        AND (
            UT.TERMINATIONCAUSE NOT IN (76,54,52,51,46,41,30,44,43) OR (
                    UT.TERMINATIONCAUSE IN (76,54,52,51,46,41,30,44,43) AND UT.TERMINATIONDATE > ADD_MONTHS( SYSDATE, -12 )
                )
            )
        )
    )

ORDER BY U.ID DESC"
, new OracleParameter("enddatedate", OracleDbType.Date, end, ParameterDirection.Input)
, new OracleParameter("enddatedate", OracleDbType.Date, end, ParameterDirection.Input)
, new OracleParameter("startdatedate", OracleDbType.Date, start, ParameterDirection.Input)
, new OracleParameter("monthdate", OracleDbType.Int32, model.searchDate.Month, ParameterDirection.Input)
, new OracleParameter("yeardate", OracleDbType.Int32, model.searchDate.Year, ParameterDirection.Input)
, new OracleParameter("midmid", OracleDbType.NVarchar2, model.searchMunicipalitiesId, ParameterDirection.Input)
).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<Form101ReportModel> Get_Form101_Statistics(UserSearchModel model)
        {
            try
            {
                if (model.maxImmunity <= 40)
                    throw new UnauthorizedAccessException();

                using (var db = new PortfolioEntities())
                {
                    return db.Database.SqlQuery<Form101ReportModel>(@"
SELECT 
    M.ID, M.RASHUT, M.NAME, M.ACTIVE, M.ALLOW101, TO_NUMBER(OK.FORM_YEAR) AS FORM_YEAR, NVL(TO_NUMBER(OK.OKS), 0) AS OKS, NVL(TO_NUMBER(NOT_K.NOT_OK), 0) AS NOT_OK
FROM MUNICIPALITIES M
LEFT JOIN (
    SELECT OKIN.MUNICIPALITYID, OKIN.FORM_YEAR, COUNT(OKIN.USERID) AS OKS FROM (
        SELECT 
            MUNICIPALITYID, USERID, TO_CHAR(CREATEDDATE, 'YYYY') AS FORM_YEAR
        FROM FORM101DATA
        WHERE MUNICIPALITYID IS NOT NULL AND USERID IS NOT NULL
            AND FORMIMAGEID IS NOT NULL AND SIGNATUREIMAGID IS NOT NULL
            AND DELIVERYSTATUS = 0
        GROUP BY MUNICIPALITYID, USERID, CREATEDDATE
    ) OKIN
    GROUP BY OKIN.MUNICIPALITYID, OKIN.FORM_YEAR
) OK
    ON OK.MUNICIPALITYID = M.ID 
LEFT JOIN (
    SELECT N_OK.MUNICIPALITYID, N_OK.FORM_YEAR, COUNT(N_OK.USERID) AS NOT_OK FROM (
        SELECT 
            EF.MUNICIPALITYID, EF.USERID, TO_CHAR(EF.CREATEDDATE, 'YYYY') AS FORM_YEAR
        FROM FORM101DATA EF
        LEFT JOIN FORM101DATA OK
            ON OK.MUNICIPALITYID = EF.MUNICIPALITYID AND OK.USERID = EF.USERID
            AND TO_CHAR(OK.CREATEDDATE, 'YYYY') = TO_CHAR(EF.CREATEDDATE, 'YYYY')
            AND OK.FORMIMAGEID IS NOT NULL AND OK.SIGNATUREIMAGID IS NOT NULL
            AND OK.DELIVERYSTATUS  = 0
        WHERE EF.MUNICIPALITYID IS NOT NULL AND EF.USERID IS NOT NULL
        AND EF.FORMIMAGEID IS NOT NULL AND EF.SIGNATUREIMAGID IS NOT NULL
        AND EF.DELIVERYSTATUS < 0 AND OK.ID IS NULL
        GROUP BY EF.MUNICIPALITYID, EF.USERID, EF.CREATEDDATE
    ) N_OK
    GROUP BY N_OK.MUNICIPALITYID, N_OK.FORM_YEAR
) NOT_K
    ON NOT_K.MUNICIPALITYID = M.ID AND NOT_K.FORM_YEAR = OK.FORM_YEAR

WHERE OK.MUNICIPALITYID IS NOT NULL

ORDER BY OK.FORM_YEAR, M.ID, M.NAME"
).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<LogInLogStatisticsModel> Parse_LogInLog_Statistics(UserSearchModel model)
        {
            try
            {
                if (model.maxImmunity <= 40)
                    throw new UnauthorizedAccessException();

                using (var db = new PortfolioEntities())
                {
                    return db.Database.SqlQuery<LogInLogStatisticsModel>(@"SELECT P.NAME,P.ID,P.PROSPECTIVE_USERS, A.ACTIVE_USERS FROM (
SELECT M.NAME,M.ID, COUNT(USERID) AS PROSPECTIVE_USERS FROM (
SELECT UM.USERID,M.NAME,M.ID FROM USERSTOMUNICIPALITIES UM
INNER JOIN MUNICIPALITIES M ON M.ID = UM.MUNICIPALITYID
) M
GROUP BY M.NAME ,M.ID
ORDER BY COUNT(USERID) DESC
) P
INNER JOIN (
SELECT MUNICIPALITYID, COUNT(USERID) AS ACTIVE_USERS FROM (
SELECT DISTINCT MUNICIPALITYID,USERID FROM LOGINLOG
WHERE ACTION = 'login'
)
GROUP BY MUNICIPALITYID
ORDER BY COUNT(USERID) DESC
) A ON P.ID = A.MUNICIPALITYID
ORDER BY A.ACTIVE_USERS DESC").ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<LogInLodDailyUsageModel> Parse_DailyUsageLogIn_Statistics(UserSearchModel model)
        {
            try
            {
                if (model.maxImmunity <= 40)
                    throw new UnauthorizedAccessException();

                using (var db = new PortfolioEntities())
                {
                    string sql = string.Format(@"SELECT 
    M.ID,
    M.NAME,
    TT.ACTIONDATE,
    UC.USER_COUNTER AS TOTALUSERSCOUNTER,
    TT.COUNTER AS DAILYUSAGE
FROM (
    SELECT 
        ACTIONDATE,MUNICIPALITYID, COUNT(USERID) AS COUNTER
    FROM (
        SELECT 
            DISTINCT USERID, TO_DATE(TO_CHAR( ACTIONDATE, 'DD-MM-YYYY'),'DD-MM-YYYY') AS ACTIONDATE, MUNICIPALITYID
        FROM LOGINLOG
        WHERE ACTION = 'login'
        ORDER BY ACTIONDATE DESC
    ) T 
    GROUP BY ACTIONDATE,MUNICIPALITYID
) TT
INNER JOIN MUNICIPALITIES M ON M.ID = TT.MUNICIPALITYID
INNER JOIN (
SELECT MUNICIPALITYID, COUNT(USERID) AS USER_COUNTER FROM USERSTOMUNICIPALITIES
GROUP BY MUNICIPALITYID
) UC ON UC.MUNICIPALITYID = TT.MUNICIPALITYID

WHERE M.ID IN ({0})
AND TT.ACTIONDATE BETWEEN TO_DATE('{1}','DD-MM-YYYY') AND TO_DATE('{2}','DD-MM-YYYY')

ORDER BY TT.ACTIONDATE DESC, TT.MUNICIPALITYID DESC", model.searchMunicipalitiesId, model.searchDate.ToString("dd-MM-yyyy"), model.endSearchDate.ToString("dd-MM-yyyy"));

                    return db.Database.SqlQuery<LogInLodDailyUsageModel>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public bool DeleteUserImagesByType(int userId, int imageType)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserImageRepository userImageRepository = session.GetRepository<IUserImageRepository>();
                    userImageRepository.RemoveRange(userImageRepository.GetQuery(i => i.USERID == userId && i.TYPE == ((UserImageEnum)imageType).ToString()));

                    return true;
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public bool DeleteHRContactByID(int contactId)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IHumanResourcesContactRepository IHumanResourcesContactRepository = session.GetRepository<IHumanResourcesContactRepository>();
                    IHumanResourcesContactRepository.Delete(IHumanResourcesContactRepository.First(h => h.ID == contactId));
                    session.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public humanResourcesContactModel UpdateHRContactByID(humanResourcesContactModel contact)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IHumanResourcesContactRepository IHumanResourcesContactRepository = session.GetRepository<IHumanResourcesContactRepository>();
                    HUMANRESOURCESCONTACT con;
                    if (contact.ID > 0)
                    {
                        con = IHumanResourcesContactRepository.First(h => h.ID == contact.ID);
                        con.EMAIL = contact.email;
                        con.JOB = contact.job;
                        con.NAME = contact.name;
                        con.PHONE = contact.phone;
                        IHumanResourcesContactRepository.Update(con);
                    }
                    else
                    {
                        con = contact.Map();
                        IHumanResourcesContactRepository.Add(con);
                    }

                    doSave<HUMANRESOURCESCONTACT>(session, con);
                    return con.Map(false);
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<PayCheckReportModel> GenerateUPCBM_Report(DateTime pcDate, DateTime reportDate, int municipalityId)
        {
            try
            {
                DateTime start = new DateTime(reportDate.Year, reportDate.Month, 25, 1, 1, 1);
                DateTime end = new DateTime(reportDate.Year, reportDate.Month, 15, 1, 1, 1).AddMonths(1);
                Log.PcuLog(new { municipalityId = municipalityId, _start = start, _end = end }, "GenerateUPCBM_Report");

                using (var db = new PortfolioEntities())
                {
                    return db.Database.SqlQuery<PayCheckReportModel>(@"
SELECT * FROM (
SELECT 
     U.ID AS USER_ID, U.IDNUMBER, U.FIRSTNAME || ' ' || U.LASTNAME AS USER_NAME,
    PE.ACTIVERECORD, PE.CREATEDDATE, PE.UPDATEDATE, PE.CREATIONSOURCE,
    AR.SENDDATE, AR.ID as ARCHIVE_ID,
    LOWER(EM.EMAIL) AS EMAIL,
    UT.TERMINATIONCAUSE, T.TERMINATIONREASON, UT.TERMINATIONDATE
FROM USERS U
INNER JOIN USERSTOMUNICIPALITIES UM
    ON UM.USERID = U.ID
LEFT JOIN PAYCHECKUSERSBYMAIL PE
    ON PE.USERID = U.ID AND PE.MUNICIPALITYID = UM.MUNICIPALITYID AND PE.SENDMAIL = 1
    AND (
        (PE.ACTIVERECORD = 1 AND PE.CREATEDDATE <= :enddatedate ) OR
        (PE.CREATEDDATE <= :enddatedate AND (PE.UPDATEDATE IS NULL OR PE.UPDATEDATE >= :startdatedate))
    )
LEFT JOIN PCUE_SENDARCHIVE AR 
    ON AR.USERID = U.ID AND AR.MUNICIPALITYID = UM.MUNICIPALITYID
    AND AR.PC_MONTH = :monthdate AND AR.PC_YEAR = :yeardate
LEFT JOIN (
    SELECT 
        USERID, MUNICIPALITYID, LISTAGG(TRIM(EMAIL),', ') WITHIN GROUP(ORDER BY USERID, MUNICIPALITYID) EMAIL
    FROM EMAILBYMUNICIPALITY
    GROUP BY USERID, MUNICIPALITYID
) EM
    ON EM.USERID = U.ID AND EM.MUNICIPALITYID = UM.MUNICIPALITYID
LEFT JOIN USERSTERMINATIONS UT
    ON UT.USERID = U.ID AND UT.MUNICIPALITYID = UM.MUNICIPALITYID
LEFT JOIN TERMINATIONS T
    ON T.TERMINATIONCAUSE = UT.TERMINATIONCAUSE
WHERE UM.MUNICIPALITYID = :midmid

AND (
    UT.TERMINATIONCAUSE IS NULL
    OR (
        UT.TERMINATIONCAUSE NOT IN (60,50)
        AND (
            UT.TERMINATIONCAUSE NOT IN (76,54,52,51,46,41,30) OR (
                    UT.TERMINATIONCAUSE IN (76,54,52,51,46,41,30) AND UT.TERMINATIONDATE > ADD_MONTHS( SYSDATE, -12 )
                )
            )
        )
    )

) FINAL
WHERE ACTIVERECORD IS NOT NULL
ORDER BY USER_ID DESC"
, new OracleParameter("enddatedate", OracleDbType.Date, end, ParameterDirection.Input)
, new OracleParameter("enddatedate", OracleDbType.Date, end, ParameterDirection.Input)
, new OracleParameter("startdatedate", OracleDbType.Date, start, ParameterDirection.Input)
, new OracleParameter("monthdate", OracleDbType.Int32, pcDate.Month, ParameterDirection.Input)
, new OracleParameter("yeardate", OracleDbType.Int32, pcDate.Year, ParameterDirection.Input)
, new OracleParameter("midmid", OracleDbType.Int32, municipalityId, ParameterDirection.Input)
).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
    }
}
