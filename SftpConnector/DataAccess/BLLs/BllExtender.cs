﻿using System.Collections.Generic;
using Catel.Data;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Linq.Expressions;
using System;

namespace DataAccess.BLLs
{
    public class BllExtender
    {
        public void doSave<T>(UnitOfWork session, IEnumerable<T> collection)
        {
            session.SaveChanges();
            session.Refresh(RefreshMode.StoreWins, collection);
        }
        public void doSave<T>(UnitOfWork session, T item)
        {
            session.SaveChanges();
            session.Refresh(RefreshMode.StoreWins, item);
        }
        public void Refresh<T>(UnitOfWork session, T item)
        {
            session.Refresh(RefreshMode.StoreWins, item);
        }
    }
}
