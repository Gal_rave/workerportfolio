﻿using System.Collections.Generic;
using System.Linq;
using System;

using Catel.Data;
using DataAccess.Interfaces;
using DataAccess.Models;
using Logger;

namespace DataAccess.BLLs
{
    public class StatisticsBll : BllExtender
    {
        public List<FORM_STATISTICS> GetFormStatisticsForMunicipality(UserSearchModel model)
        {
            try
            {
                if (model.maxImmunity <= 40)
                    throw new UnauthorizedAccessException();

                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IFormStatisticsRepository fsRepository = session.GetRepository<IFormStatisticsRepository>();
                    IQueryable<FORM_STATISTICS> Qforms = fsRepository.GetQuery(fs => !fs.CREATEDDATE.HasValue || (fs.CREATEDDATE.HasValue && fs.CREATEDDATE.Value.Year == model.searchDate.Year));
                    return model.maxImmunity > 70 && model.page > 1 ? Qforms.Where(fs => fs.MUNICIPALITYID == model.page).ToList() : Qforms.Where(fs => fs.MUNICIPALITYID == model.municipalityId).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
    }
}
