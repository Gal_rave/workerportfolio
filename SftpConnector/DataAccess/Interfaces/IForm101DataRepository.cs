﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IForm101DataRepository : IEntityRepository<FORM101DATA, int>
    {
        IQueryable<FORM101DATA> Include(string include);
    }
}
