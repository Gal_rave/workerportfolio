﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface ISystem_streetsRepository : IEntityRepository<SYSTEM_STREETS, int>
    {
        IQueryable<SYSTEM_STREETS> Include(string include);
        void delete_systemstreets();
        void UpdateCitiesStreets();
    }
}
