﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IMembershipRepository : IEntityRepository<MEMBERSHIP, int>
    {
        IQueryable<MEMBERSHIP> Include(string include);
    }
}
