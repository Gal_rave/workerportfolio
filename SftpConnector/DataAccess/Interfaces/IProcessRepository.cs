﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IProcessRepository : IEntityRepository<PROCESS, int>
    {
        IQueryable<PROCESS> Include(string include);
    }
}
