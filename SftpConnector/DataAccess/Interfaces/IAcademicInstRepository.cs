﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IAcademicInstRepository : IEntityRepository<ACADEMIC_INST, int>
    {
        IQueryable<ACADEMIC_INST> Include(string include);
    }
}
