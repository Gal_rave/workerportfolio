﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IStreetRepository : IEntityRepository<STREET, int>
    {
        IQueryable<STREET> Include(string include);
    }
}
