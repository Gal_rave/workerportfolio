﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IsmsNotificationRepository : IEntityRepository<SMSNOTIFICATION, int>
    {
        IQueryable<SMSNOTIFICATION> Include(string include);
    }
}
