﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface ITerminationRepository : IEntityRepository<TERMINATION, int>
    {
    }
}
