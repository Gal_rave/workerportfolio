﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IPcueSendArchiveRepository : IEntityRepository<PCUE_SENDARCHIVE, int>
    {
        IQueryable<PCUE_SENDARCHIVE> Include(string include);
    }
}
