﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IHumanResourcesContactRepository : IEntityRepository<HUMANRESOURCESCONTACT, int>
    {
        IQueryable<HUMANRESOURCESCONTACT> Include(string include);
    }
}
