﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IUsersToFactoryRepository : IEntityRepository<USERSTOFACTORy, int>
    {
        IQueryable<USERSTOFACTORy> Include(string include);
    }
}
