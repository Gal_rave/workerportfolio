﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface ISpouseRepository : IEntityRepository<SPOUSE, int>
    {
        IQueryable<SPOUSE> Include(string include);
    }
}
