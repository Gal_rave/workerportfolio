﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface ITahalichimRepository : IEntityRepository<TAHALICHIM, int>
    {
        IQueryable<TAHALICHIM> Include(string include);
    }
}