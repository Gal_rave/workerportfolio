﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface ISystem_citiesRepository : IEntityRepository<SYSTEM_CITIES, int>
    {
        IQueryable<SYSTEM_CITIES> Include(string include);
        void delete_systemcities();
    }
}
