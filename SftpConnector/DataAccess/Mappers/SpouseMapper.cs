﻿using System;
using System.Linq;
using System.Collections.Generic;

using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class SpouseMapper
    {
        public static SPOUSE Map(this SpouseModel model)
        {
            if (model == null)
                return null;

            return new SPOUSE
            {
                ID = model.ID.ToInt(),
                BIRTHDATE = model.birthDate,
                CELL = model.cell,
                CREATEDDATE = model.createdDate,
                EMAIL = model.email,
                FIRSTNAME = model.firstName,
                IDNUMBER = model.idNumber,
                IMMIGRATIONDATE = model.immigrationDate,
                LASTNAME = model.lastName,
                PHONE = model.phone,
                ISDELETED = model.isDeleted,
                CALCULATEDIDNUMBER = model.calculatedIdNumber,
                FATHERSNAME = model.fathersName,
                PREVIOUSLASTNAME = model.previousLastName,
                PREVIOUSFIRSTNAME = model.previousFirstName,
                ZIP = model.zip,
                CITY = model.city,
                STREET = model.street,
                HOUSENUMBER = model.houseNumber,
                ENTRANCENUMBER = model.entranceNumber,
                APARTMENTNUMBER = model.apartmentNumber.ToInt(),
                POBOX = model.poBox,
                COUNTRYOFBIRTH = model.countryOfBirth,
                GENDER = model.gender,
                MARITALSTATUS = model.maritalStatus,
                MARITALSTATUSDATE = model.maritalStatusDate,
                SPOUSEID = model.spouseId,
                JOBTITLE = model.jobTitle,
                ISSPOUSE = true,
                ISALLOW101 = false,
                WELLCOMEMAIL = false,
                INITIALCHECK = false,
            };
        }
        public static SpouseModel Map(this SPOUSE model)
        {
            if (model == null)
                return null;
            return new SpouseModel
            {
                ID = model.ID.ToInt(),
                birthDate = model.BIRTHDATE,
                cell = model.CELL,
                createdDate = model.CREATEDDATE,
                email = model.EMAIL,
                firstName = model.FIRSTNAME,
                idNumber = model.IDNUMBER,
                immigrationDate = model.IMMIGRATIONDATE,
                lastName = model.LASTNAME,
                phone = model.PHONE,
                isDeleted = model.ISDELETED,
                calculatedIdNumber = model.CALCULATEDIDNUMBER,
                fathersName = model.FATHERSNAME,
                previousLastName = model.PREVIOUSLASTNAME,
                previousFirstName = model.PREVIOUSFIRSTNAME,
                zip = model.ZIP,
                city = model.CITY,
                street = model.STREET,
                houseNumber = model.HOUSENUMBER.ToInt(),
                entranceNumber = model.ENTRANCENUMBER,
                apartmentNumber = model.APARTMENTNUMBER.ToInt(),
                poBox = model.POBOX,
                countryOfBirth = model.COUNTRYOFBIRTH,
                gender = model.GENDER,
                maritalStatus = model.MARITALSTATUS.ToInt(),
                maritalStatusDate = model.MARITALSTATUSDATE,
                spouseId = model.SPOUSEID,
                hasIncome = model.HASINCOME.ToBool(),
                incomeType = !string.IsNullOrWhiteSpace(model.INCOMETYPE)? model.INCOMETYPE.Replace(" ","") : string.Empty,
                jobTitle = model.JOBTITLE,
                isSpouse = true,
                isConfirmed = false,
                isAllow101 = false,
                wellcomEmail = false,
            };
        }
        public static SpouseModel MapToSpouseModel(this USER model, bool withInner = true)
        {
            if (model == null)
                return null;
            return new SpouseModel
            {
                ID = model.ID.ToInt(),
                birthDate = model.BIRTHDATE,
                cell = model.CELL,
                createdDate = model.CREATEDDATE,
                email = model.EMAIL,
                firstName = model.FIRSTNAME,
                idNumber = model.IDNUMBER,
                immigrationDate = model.IMMIGRATIONDATE,
                lastName = model.LASTNAME,
                phone = model.PHONE,
                isDeleted = model.ISDELETED,
                isConfirmed = model.MEMBERSHIP != null && model.MEMBERSHIP.ISCONFIRMED,
                calculatedIdNumber = model.CALCULATEDIDNUMBER,
                fathersName = model.FATHERSNAME,
                previousLastName = model.PREVIOUSLASTNAME,
                previousFirstName = model.PREVIOUSFIRSTNAME,
                zip = model.ZIP,
                city = model.CITY,
                street = model.STREET,
                houseNumber = model.HOUSENUMBER.ToInt(),
                entranceNumber = model.ENTRANCENUMBER,
                apartmentNumber = model.APARTMENTNUMBER.ToInt(),
                poBox = model.POBOX,
                countryOfBirth = model.COUNTRYOFBIRTH,
                gender = model.GENDER,
                maritalStatus = model.MARITALSTATUS.ToInt(),
                maritalStatusDate = model.MARITALSTATUSDATE,
                isSpouse = model.ISSPOUSE,
                spouseId = model.SPOUSEID,
                jobTitle = model.JOBTITLE,
                isAllow101 = model.ISALLOW101,
                wellcomEmail = model.WELLCOMEMAIL,
                initialcheck = model.INITIALCHECK,
                hasIncome = model.HASINCOME.ToBool(),
                incomeType = !string.IsNullOrWhiteSpace(model.INCOMETYPE) ? model.INCOMETYPE.Replace(" ", "") : string.Empty,

                spouse = withInner && model.SPOUSEID.HasValue && model.SPOUSEID.Value > 0 ? model.SPOUSE.Map() : null,
                municipalities = withInner ? model.MUNICIPALITIES.ToList().Select(m => m.Map(false)).ToList() : null,
                groups = withInner ? model.GROUPS.ToList().Select(g => g.Map(false)).ToList() : null,
                membership = withInner ? model.MEMBERSHIP.Map(false) : null,
                terminations = withInner ? model.USERSTERMINATIONS.ToList().Select(t => t.Map()).ToList() : null,                
            };
        }
    }
}
