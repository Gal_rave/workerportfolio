﻿using DataAccess.Models;
using System;
using System.Linq;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class ReshimatTiyugMapper
    {
        public static ReshimatTiyugModel Map(this RESHIMOT_TIYUG model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new ReshimatTiyugModel
            {
                recId = model.REC_ID.ToInt(),
                rashut = model.RASHUT.ToInt(),
                mifal = model.MIFAL.ToInt(),
                fromDate = model.FROM_DATE.ToInt(),
                toDate = model.TO_DATE.ToInt(),
                shemReshima = model.SHEM_RESHIMA,
                creationTime = model.CREATION_TIME,
                lastUpdateTime = model.LAST_UPDATE_TIME,
                lastUpdateUser = model.LAST_UPDATE_USER.ToInt(),
                status = model.STATUS.ToInt(),
                mismachimBeReshimaModel = model.MISMACHIM_BERESHIMA.Select(mbr => mbr.Map(withInner)).ToList()
            };
        }
        public static RESHIMOT_TIYUG Map(this ReshimatTiyugModel model)
        {
            if (model == null)
                return null;

            return new RESHIMOT_TIYUG
            {
                REC_ID = model.recId,
                RASHUT = model.rashut,
                MIFAL = model.mifal,
                FROM_DATE = model.fromDate,
                TO_DATE = model.toDate,
                SHEM_RESHIMA = model.shemReshima,
                CREATION_TIME = model.creationTime,
                LAST_UPDATE_TIME = model.lastUpdateTime,
                LAST_UPDATE_USER = model.lastUpdateUser,
                STATUS = model.status
            };
        }
    }
}
