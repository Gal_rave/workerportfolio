﻿using DataAccess.Models;
using System;
using System.Collections.Generic;

namespace DataAccess.Mappers
{
    public static class GetWorkerDataMapper
    {
        public static WSPostModel MapToWSPostModel(this GetWorkerDataModel model)
        {
            if (model == null)
                return null;

            return model.model;
        }
        public static RepresentedWorkerModel MapToRepresentedWorker(this GetWorkerDataModel model)
        {
            if (model == null)
                return null;

            return model.worker;
        }
    }
}