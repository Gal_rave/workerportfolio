﻿using DataAccess.Models;

namespace DataAccess.Mappers
{
    public static class MembershipMapper
    {
        public static MembershipModel Map(this MEMBERSHIP model, bool withInner = true)
        {
            if (model == null)
                return null;
            return new MembershipModel
            {
                confirmationToken = model.CONFIRMATIONTOKEN,
                createdDate = model.CREATEDDATE,
                isConfirmed = model.ISCONFIRMED,
                lastPasswordFailureDate = model.LASTPASSWORDFAILUREDATE,
                password = model.PASSWORD,
                passwordChangeDdate = model.PASSWORDCHANGEDDATE,
                PasswordFailureSCounter = model.PASSWORDFAILURESCOUNTER,
                passwordSalt = model.PASSWORDSALT,
                passwordVerificationToken = model.PASSWORDVERIFICATIONTOKEN,
                userID = model.USERID,
                verificationTokenExpiration = model.VERIFICATIONTOKENEXPIRATION,
                passwordFormat = model.PASSWORDFORMAT,
                user = withInner? model.USER.Map(false) : null,
                smsVerificationToken = model.SMSVERIFICATIONTOKEN,
                smsTokenExpiration = model.SMSTOKENEXPIRATION
            };
        }
        public static MEMBERSHIP Map(this MembershipModel model)
        {
            if (model == null)
                return null;
            return new MEMBERSHIP
            {
                CONFIRMATIONTOKEN = model.confirmationToken,
                CREATEDDATE = model.createdDate,
                ISCONFIRMED = model.isConfirmed,
                LASTPASSWORDFAILUREDATE = model.lastPasswordFailureDate,
                PASSWORD = model.password,
                PASSWORDCHANGEDDATE = model.passwordChangeDdate,
                PASSWORDFAILURESCOUNTER = model.PasswordFailureSCounter,
                PASSWORDSALT = model.passwordSalt,
                PASSWORDVERIFICATIONTOKEN = model.passwordVerificationToken,
                USERID = model.userID,
                VERIFICATIONTOKENEXPIRATION = model.verificationTokenExpiration,
                PASSWORDFORMAT = model.passwordFormat,
                SMSVERIFICATIONTOKEN = model.smsVerificationToken,
                SMSTOKENEXPIRATION = model.smsTokenExpiration
            };
        }
    }
}
