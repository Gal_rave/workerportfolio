﻿using System;
using System.Linq;
using System.Collections.Generic;

using DataAccess.Models;
using DataAccess.Extensions;
using HttpCookieHandler;
using Logger;

namespace DataAccess.Mappers
{
    public static class UserCredentialsMapper
    {
        public static UserCredentialsModel MapCredentials(this USER model)
        {
            if (model == null)
                return null;

            int CurrentMunicipalityId = model.MUNICIPALITIES.getCurrentMunicipalityId(model.IDNUMBER);
            MUNICIPALITy currentMunicipality = model.MUNICIPALITIES.First(m => m.ID == CurrentMunicipalityId);

            return new UserCredentialsModel
            {
                firstName = model.FIRSTNAME,
                lastName = model.LASTNAME,
                fullName = string.Format("{0} {1}", model.FIRSTNAME, model.LASTNAME),
                idNumber = model.IDNUMBER,
                confirmationToken = model.MEMBERSHIP != null ? model.MEMBERSHIP.CONFIRMATIONTOKEN : "",
                loginDate = DateTime.Now,
                calculatedIdNumber = model.CALCULATEDIDNUMBER,
                currentMunicipalityId = CurrentMunicipalityId,
                currentRashutId = currentMunicipality.RASHUT.ToInt(),
                municipalities = model.MUNICIPALITIES.Where(m => m.ACTIVE == true).Select(m => m.Map()).ToList(),
                currentTerminationDate = model.getCurrentTerminationDate(),
                email = String.Join(",", model.EMAILBYMUNICIPALITies.Where(e => e.DOPAYCHECKPOPUP.HasValue && e.DOPAYCHECKPOPUP.Value && e.DOPAYCHECKPOPUP.HasValue && e.DOPAYCHECKPOPUP.Value && e.MUNICIPALITYID == CurrentMunicipalityId).Select(e => e.EMAIL).ToArray()),

                //myReferredProcesses = currentMunicipality.ALLOWPROCESS? model.PROCESSES.Where(p=> p.TARGET_USERID == model.ID && p.RASHUT == currentMunicipality.ID).Count() : -1,
                //myRequestsProcesses = currentMunicipality.ALLOWPROCESS ? model.PROCESSES.Where(p => p.RASHUT == currentMunicipality.ID && (p.OVED_NESU_BAKASHA.HasValue || p.USERID == model.ID)).Count() : -1,

                groups = model.GROUPS.Select(g => g.Map(false)).ToList(),
                isAdmin = isAdmin(model.GROUPS),

                isAllow101 = model.ISALLOW101.HasValue ? model.ISALLOW101.Value : currentMunicipality.ALLOW101,
                isAllowProcess = currentMunicipality.ALLOWPROCESS,
                isAllowEPC = currentMunicipality.ALLOWEMAILPAYCHECK,
                isAllowMunicipalityMenu = currentMunicipality.ALLOWMUNICIPALITYMENU,
                isAllowSystemsEmail = currentMunicipality.ALLOWSYSTEMSEMAIL
            };
        }
        public static UserCredentialsModel MapCredentials(this UserModel model)
        {
            if (model == null)
                return null;
            int CurrentMunicipalityId = model.municipalities.getCurrentMunicipalityId(model.idNumber);
            MunicipalityModel currentMunicipality = model.municipalities.First(m => m.ID == CurrentMunicipalityId);
            
            return new UserCredentialsModel
            {
                firstName = model.firstName,
                lastName = model.lastName,
                fullName = string.Format("{0} {1}", model.firstName, model.lastName),
                idNumber = model.idNumber,
                confirmationToken = model.membership != null ? model.membership.confirmationToken : "",
                loginDate = DateTime.Now,
                calculatedIdNumber = model.calculatedIdNumber,
                currentMunicipalityId = CurrentMunicipalityId,
                currentRashutId = currentMunicipality.rashut.ToInt(),
                municipalities = model.municipalities.Where(m => m.active == true).ToList(),
                currentTerminationDate = model.getCurrentTerminationDate(),
                email = String.Join(",", model.emailByMunicipality.Where(e => e.doPaycheckPopup.HasValue && e.doPaycheckPopup.Value && e.municipalityId == CurrentMunicipalityId).Select(e => e.email).Distinct().ToArray()),
                
                groups = model.groups,
                isAdmin = isAdmin(model.groups),

                isAllow101 = model.isAllow101.HasValue ? model.isAllow101.Value : currentMunicipality.allow101,
                isAllowProcess = currentMunicipality.allowprocess,
                isAllowEPC = currentMunicipality.allowemailpaycheck,
                isAllowMunicipalityMenu = currentMunicipality.allowMunicipalityMenu,
                isAllowSystemsEmail = currentMunicipality.allowSystemsEmail
            };
        }
        public static UserCredentialsModel MapCredentialsWithoutRashut(this UserModel model, int rashut, int mncplity)
        {
            if (model == null || rashut <= 0 || mncplity <= 0)
                return null;
            int CurrentMunicipalityId = mncplity;
            MunicipalityModel currentMunicipality = model.municipalities.First(m => m.ID == CurrentMunicipalityId);
            return new UserCredentialsModel
            {
                firstName = model.firstName,
                lastName = model.lastName,
                fullName = string.Format("{0} {1}", model.firstName, model.lastName),
                idNumber = model.idNumber,
                confirmationToken = model.membership != null ? model.membership.confirmationToken : "",
                loginDate = DateTime.Now,
                calculatedIdNumber = model.calculatedIdNumber,
                currentMunicipalityId = CurrentMunicipalityId,
                currentRashutId = currentMunicipality.rashut.ToInt(),
                municipalities = model.municipalities.Where(m => m.active == true).ToList(),
                currentTerminationDate = model.getCurrentTerminationDate(),
                email = String.Join(",", model.emailByMunicipality.Where(e => e.doPaycheckPopup.HasValue && e.doPaycheckPopup.Value && e.municipalityId == CurrentMunicipalityId).Select(e => e.email).Distinct().ToArray()),

                groups = model.groups,
                isAdmin = isAdmin(model.groups),

                isAllow101 = model.isAllow101.HasValue ? model.isAllow101.Value : currentMunicipality.allow101,
                isAllowProcess = currentMunicipality.allowprocess,
                isAllowEPC = currentMunicipality.allowemailpaycheck,
                isAllowMunicipalityMenu = currentMunicipality.allowMunicipalityMenu,
                isAllowSystemsEmail = currentMunicipality.allowSystemsEmail
            };
        }
        public static UserCredentialsModel MapPartialCredentials(this UserModel model)
        {
            if (model == null)
                return null;
            return new UserCredentialsModel
            {
                firstName = model.firstName,
                lastName = model.lastName,
                fullName = string.Format("{0} {1}", model.firstName, model.lastName)
            };
        }
        
        #region private
        private static DateTime? getCurrentTerminationDate(this USER model)
        {
            USERSTERMINATION termination = model.USERSTERMINATIONS.FirstOrDefault(t => t.MUNICIPALITYID == model.MUNICIPALITIES.getCurrentMunicipalityId(model.IDNUMBER));
            if (termination != null && termination.TERMINATIONDATE.HasValue)
                return termination.TERMINATIONDATE.Value;

            return null;
        }
        private static DateTime? getCurrentTerminationDate(this MEMBERSHIP model, USER user)
        {
            USERSTERMINATION termination = user.USERSTERMINATIONS.FirstOrDefault(t => t.MUNICIPALITYID == user.MUNICIPALITIES.getCurrentMunicipalityId(model.USER.IDNUMBER));
            if (termination != null && termination.TERMINATIONDATE.HasValue)
                return termination.TERMINATIONDATE.Value;

            return null;
        }
        private static DateTime? getCurrentTerminationDate(this UserModel model)
        {
            UsersTerminationModel termination = model.terminations.FirstOrDefault(t => t.municipalityId == model.municipalities.getCurrentMunicipalityId(model.idNumber));
            if (termination != null && termination.terminationDate.HasValue)
                return termination.terminationDate.Value;

            return null;
        }
        private static DateTime? getCurrentTerminationDate(this MembershipModel model)
        {
            UsersTerminationModel termination = model.user.terminations.FirstOrDefault(t => t.municipalityId == model.user.municipalities.getCurrentMunicipalityId(model.user.idNumber));
            if (termination != null && termination.terminationDate.HasValue)
                return termination.terminationDate.Value;

            return null;
        }
        private static int getCurrentMunicipalityId(this ICollection<MUNICIPALITy> collection, string idNumber)
        {
            int municipalityId = getCookieMunicipalityId(idNumber);
            if (municipalityId > 0)
                return municipalityId;

            if (collection == null || collection.Count <= 0)
                return 0;
            if (collection.Count > 1)
                return -1;
            return collection.First(m => m.ACTIVE == true).ID.ToInt();
        }
        private static int getCurrentMunicipalityId(this List<MunicipalityModel> list, string idNumber)
        {
            int municipalityId = getCookieMunicipalityId(idNumber);
            if (municipalityId > 0)
                return municipalityId;

            if (list == null || list.Count <= 0)
                return 0;
            if (list.Count > 1)
                return -1;
            return list.First(m=> m.active == true).ID.ToInt();
        }
        private static MunicipalityModel getCurrentMunicipality(this List<MunicipalityModel> list, string idNumber)
        {
            try
            {
                int municipalityId = getCookieMunicipalityId(idNumber);
                return list.First(m => m.active == true);
            }
            catch
            {
                return null;
            }
        }
        private static MUNICIPALITy getCurrentMunicipality(this ICollection<MUNICIPALITy> collection, string idNumber)
        {
            try
            {
                int municipalityId = getCookieMunicipalityId(idNumber);
                return collection.First(m => m.ACTIVE == true);
            }
            catch
            {
                return null;
            }
        }
        private static int getCookieMunicipalityId(string idNumber)
        {
            return CookieExtender.GetCurrentMunicipalityId(idNumber);
        }
        private static int isAdmin(List<GroupModel> groups)
        {
            try
            {
                return groups.Max(g => g.immunity).ToInt();
            }
            catch
            {
                return 0;
            }
        }
        private static int isAdmin(ICollection<GROUP> groups)
        {
            try
            {
                return groups.Max(g => g.IMMUNITY).ToInt();
            }
            catch
            {
                return 0;
            }
        }
        #endregion
    }
}
