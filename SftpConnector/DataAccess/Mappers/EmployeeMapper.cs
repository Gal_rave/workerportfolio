﻿using System.Linq;
using DataAccess.Models;
using DataAccess.Extensions;
using System.Collections.Generic;

namespace DataAccess.Mappers
{
    public static class EmployeeMapper
    {
        public static EmployeeModel MapEmployee(this UserModel user)
        {
            if (user == null)
                return new EmployeeModel(); ;

            EmployeeModel model = new EmployeeModel()
            {
                apartmentNumber = user.apartmentNumber,
                birthDate = user.birthDate,
                calculatedIdNumber = user.calculatedIdNumber,
                cell = user.cell.IndexOf(",") > -1 ? (user.cell.Split(',').ToList().First()) : user.cell,
                city = user.city,
                countryOfBirth = user.countryOfBirth,
                email = user.email.IndexOf(",") > -1 ? (user.email.Split(',').ToList().First()) : user.email,
                entranceNumber = user.entranceNumber,
                fathersName = user.fathersName,
                firstName = user.firstName,
                gender = user.gender,
                houseNumber = user.houseNumber,
                ID = user.ID,
                idNumber = user.idNumber,
                immigrationDate = user.immigrationDate,
                isSpouse = user.isSpouse,
                jobTitle = user.jobTitle,
                lastName = user.lastName,
                maritalStatus = user.maritalStatus,
                maritalStatusDate = user.maritalStatusDate,
                phone = user.phone.IndexOf(",") > -1 ? (user.phone.Split(',').ToList().First()) : user.phone,
                poBox = user.poBox,
                previousFirstName = user.previousFirstName,
                previousLastName = user.previousLastName,
                street = user.street,
                zip = user.zip,
                hmoMember = true,
                ilResident = true,
                kibbutzResident = user.kibbutzMember,
                hmoCode = user.hmoCode.ToInt(),
                hmoName = user.hmoCode.GetHmoName()
            };

            return model;
        }
        public static EmployeeModel MapEmployee(this USER user)
        {
            if (user == null)
                return new EmployeeModel(); ;

            EmployeeModel model = new EmployeeModel()
            {
                apartmentNumber = user.APARTMENTNUMBER.ToInt(),
                birthDate = user.BIRTHDATE,
                calculatedIdNumber = user.CALCULATEDIDNUMBER,
                cell = !string.IsNullOrWhiteSpace(user.CELL) && user.CELL.IndexOf(",") > -1? (user.CELL.Split(',').ToList().First()) : user.CELL,
                city = user.CITY,
                countryOfBirth = user.COUNTRYOFBIRTH,
                email = !string.IsNullOrWhiteSpace(user.EMAIL) && user.EMAIL.IndexOf(",") > -1 ? (user.EMAIL.Split(',').ToList().First()) : user.EMAIL,
                entranceNumber = user.ENTRANCENUMBER,
                fathersName = user.FATHERSNAME,
                firstName = user.FIRSTNAME,
                gender = user.GENDER,
                houseNumber = user.HOUSENUMBER.ToInt(),
                ID = user.ID.ToInt(),
                idNumber = user.IDNUMBER,
                immigrationDate = user.IMMIGRATIONDATE,
                isSpouse = user.ISSPOUSE,
                jobTitle = user.JOBTITLE,
                lastName = user.LASTNAME,
                maritalStatus = user.MARITALSTATUS.ToInt(),
                maritalStatusDate = user.MARITALSTATUSDATE,
                phone = !string.IsNullOrWhiteSpace(user.PHONE) && user.PHONE.IndexOf(",") > -1 ? (user.PHONE.Split(',').ToList().First()) : user.PHONE,
                poBox = user.POBOX,
                previousFirstName = user.PREVIOUSFIRSTNAME,
                previousLastName = user.PREVIOUSLASTNAME,
                street = user.STREET,
                zip = user.ZIP,
                hmoMember = true,
                ilResident = true,
                kibbutzResident = user.KIBBUTZMEMBER,
                hmoCode = user.HMOCODE.ToInt(),
                hmoName = user.HMOCODE.GetHmoName()
            };

            return model;
        }
    }
}
