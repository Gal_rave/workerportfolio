﻿using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class UpdateUserMunicipalityContactMapper
    {
        public static string Map(this UPDATEUSERMUNICIPALITYCONTACT model)
        {
            if (model == null)
                return null;

            UPDATEUSERMUNICIPALITYCONTACT uumc = new UPDATEUSERMUNICIPALITYCONTACT
            {
                CELL = model.CELL,
                CREATEDDATE = model.CREATEDDATE,
                EMAIL = model.EMAIL,
                MUNICIPALITYID = model.MUNICIPALITy.RASHUT.HasValue? model.MUNICIPALITy.RASHUT.Value : model.MUNICIPALITYID,
                TYPE = model.TYPE,
                UPDATESTATUS = model.UPDATESTATUS,
                USERID = model.USER.CALCULATEDIDNUMBER.ToDecimal()
            };
            return uumc.ToJSON();
        }
    }
}
