﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataAccess.Models;

namespace DataAccess.Mappers
{
    public static class LoginLogMapper
    {
        public static LoginLogModel Map(this LOGINLOG model)
        {
            if (model == null)
                return null;

            return new LoginLogModel
            {
                ID = model.ID,
                userId = model.USERID,
                action = model.ACTION,
                actionDate = model.ACTIONDATE,
                municipalityId = model.MUNICIPALITYID,
                actionIP = model.ACTIONIP
            };
        }
        public static LOGINLOG Map(this LoginLogModel model)
        {
            if (model == null)
                return null;

            return new LOGINLOG
            {
                ID = model.ID,
                USERID = model.userId,
                ACTION = model.action,
                ACTIONDATE = model.actionDate,
                MUNICIPALITYID = model.municipalityId,
                ACTIONIP = model.actionIP
            };
        }
    }
}
