﻿using DataAccess.Models;
using DataAccess.Extensions;
using System;

namespace DataAccess.Mappers
{
    
    public static class VacationFormMapper
    {
        public static ProcessModel MapToProcess(this VacationFormModel model)
        {
            if (model == null)
                return null;

            return new ProcessModel
            {
                CONTENT1 = model.vacationReason,
                CONTENT2 = model.vacationLeft,
                CONTENT3 = null,
                CONTENT4 = null,
                CONTENT5 = null,
                CONTENT6 = null,
                CONTENT7 = null,
                FROM_DATE = model.datesRange.from,
                MIFAL = 999,
                PROCESS_DESCRIPTION = model.selectedProcess.type == 5 ? "בקשת יציאה לחופשה בחול" : "בקשת יציאה לחופשה",
                PROCESS_TYPE = model.selectedProcess.type,
                RASHUT = model.ovedModel.currentMunicipalityId,
                STATUS = 0,
                TARGET_USERID = model.targetUser.idNumber,
                TO_DATE = model.datesRange.to,
                USERID = model.ovedModel.calculatedIdNumber.ToInt(),
                CREATION_DATE = DateTime.Now.ToString("dd/MM/yyyy"),
                LAST_UPDATE = DateTime.Now.ToString("dd/MM/yyyy"),
                OVED_NESU_BAKASHA = model.ovedNesuBakasha.ToDecimal(),
                OVED_NESU_BAKASHA_NAME = model.ovedNesuBakashaName,
                TT_TAHALICH_ID = model.tt_tahalich_id,
                TT_SHALAV_ID = model.tt_shalav_id,
                CURR_MISPAR_SHALAV = model.curr_mispar_shalav,
                SHLAVIM_COUNTER = model.shlavim_counter
            };
        }
    }
}
