﻿using DataAccess.Models;
using System;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class MismachMapper
    {
        public static MismachModel Map(this MISMACHIM model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new MismachModel
            {
                recId = model.REC_ID.ToInt(),
                rashut = model.RASHUT.ToInt(),
                mifal = model.MIFAL.ToInt(),
                fromDate = model.FROM_DATE.ToInt(),
                toDate = model.TO_DATE.ToInt(),
                makorMismach = model.MAKOR_MISMACH.ToInt(),
                shemMismachumn1 = model.SHEM_MISMACH,
                creationTime = model.CREATION_TIME,
                lastUpdateTime = model.LAST_UPDATE_TIME,
                lastUpdateUser = model.LAST_UPDATE_USER.ToInt(),
                status = model.STATUS.ToInt()
            };
        }
        public static MISMACHIM Map(this MismachModel model)
        {
            if (model == null)
                return null;

            return new MISMACHIM
            {
                REC_ID = model.recId,
                RASHUT = model.rashut,
                MIFAL = model.mifal,
                FROM_DATE = model.fromDate,
                TO_DATE = model.toDate,
                MAKOR_MISMACH = model.makorMismach,
                SHEM_MISMACH = model.shemMismachumn1,
                CREATION_TIME = model.creationTime,
                LAST_UPDATE_TIME = model.lastUpdateTime,
                LAST_UPDATE_USER = model.lastUpdateUser,
                STATUS = model.status
            };
        }
    }
}
