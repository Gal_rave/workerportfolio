﻿using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class UserImageMapper
    {
        public static UserImageModel Map(this USERIMAGE model)
        {
            if (model == null)
                return null;

            return new UserImageModel
            {
                ID = model.ID.ToInt(),
                fileBlob = model.FILEBLOB,
                fileDate = model.FILEDATE,
                type = model.TYPE,
                userId = model.USERID.ToInt(),
                fileString = model.FILEBLOB != null && model.FILEBLOB.Length > 100 ? model.FILEBLOB.ByteArrayToString() : string.Empty,
                municipalityId = model.MUNICIPALITYID.NullableInt(),
                fileName = model.FILENAME,
                fileimageblob = model.FILEIMAGEBLOB,
                fileimageString = model.FILEIMAGEBLOB != null && model.FILEIMAGEBLOB.Length > 100 ? model.FILEBLOB.ByteArrayToString() : string.Empty,
            };
        }
        public static USERIMAGE Map(this UserImageModel model)
        {
            if (model == null)
                return null;

            return new USERIMAGE
            {
                ID = model.ID,
                FILEBLOB = model.fileBlob,
                FILEDATE = model.fileDate,
                TYPE = model.type,
                USERID = model.userId,
                MUNICIPALITYID = model.municipalityId,
                FILENAME = model.fileName,
                FILEIMAGEBLOB = model.fileimageblob
            };
        }
    }
}
