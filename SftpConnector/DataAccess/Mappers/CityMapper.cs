﻿using System.Linq;
using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class CityMapper
    {
        public static CityModel Map(this CITy model, bool withInner = true)
        {
            if (model == null)
                return null;
            return new CityModel
            {
                ID = model.ID.ToInt(),
                Name = model.NAME,
                EnglishName = model.ENGLISHNAME,
                Streets = withInner? model.STREETS.Select(s => s.Map(false)).ToList() : null
            };
        }
        public static CITy Map(this CityModel model)
        {
            if (model == null)
                return null;
            return new CITy
            {
                ID = model.ID,
                NAME = model.Name,
                ENGLISHNAME = model.EnglishName
            };
        }
    }
}