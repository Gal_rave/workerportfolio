﻿using System.Collections.Generic;
using System.Linq;

using DataAccess.Models;
using DataAccess.Extensions;
using DataAccess.Enums;

namespace DataAccess.Mappers
{
    public static class MunicipalityMapper
    {
        public static MunicipalityModel Map(this MUNICIPALITy model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new MunicipalityModel
            {
                ID = model.ID,
                name = model.NAME,
                active = model.ACTIVE,
                allow101 = model.ALLOW101,
                allowprocess = model.ALLOWPROCESS,
                allowemailpaycheck = model.ALLOWEMAILPAYCHECK,
                rashut = model.RASHUT,
                factoryEmailPC = model.FACTORYEMAILPC,
                synerion_link = model.SYNERION_LINK,
                allowMunicipalityMenu = model.ALLOWMUNICIPALITYMENU,
                allowSystemsEmail = model.ALLOWSYSTEMSEMAIL,
                municipalityMenuTitle = model.MUNICIPALITYMENU_TITLE,
                performUpdate = model.PERFORMUPDATE,

                users = withInner? model.USERS.ToList().Select(u => u.Map(false)).ToList() : null,
                htmlTemplates = model.ALLOWMUNICIPALITYMENU? model.HTMLTEMPLATES.Where(t=> t.ACTIVE && t.TYPE == (int)TemplateTypeEnum.Page).Select(t=> t.Map(false)).ToList() : null
            };
        }
        public static MUNICIPALITy Map(this MunicipalityModel model)
        {
            if (model == null)
                return null;

            return new MUNICIPALITy
            {
                ID = model.ID,
                NAME = model.name,
                ACTIVE = model.active,
                ALLOW101 = model.allow101,
                ALLOWPROCESS = model.allowprocess,
                ALLOWEMAILPAYCHECK = model.allowemailpaycheck,
                RASHUT = model.rashut.ToInt(),
                FACTORYEMAILPC = model.factoryEmailPC,
                SYNERION_LINK = model.synerion_link,
                ALLOWMUNICIPALITYMENU = model.allowMunicipalityMenu,
                ALLOWSYSTEMSEMAIL = model.allowSystemsEmail,
                MUNICIPALITYMENU_TITLE = model.municipalityMenuTitle,
                PERFORMUPDATE = model.performUpdate,
            };
        }
        public static MunicipalityExtensionModel MapExtension(this MUNICIPALITy model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new MunicipalityExtensionModel
            {
                ID = model.ID,
                name = model.NAME,
                active = model.ACTIVE,
                allow101 = model.ALLOW101,
                allowprocess = model.ALLOWPROCESS,
                allowemailpaycheck = model.ALLOWEMAILPAYCHECK,
                rashut = model.RASHUT,
                factoryEmailPC = model.FACTORYEMAILPC,
                synerion_link = model.SYNERION_LINK,
                allowMunicipalityMenu = model.ALLOWMUNICIPALITYMENU,
                allowSystemsEmail = model.ALLOWSYSTEMSEMAIL,
                municipalityMenuTitle = model.MUNICIPALITYMENU_TITLE,
                performUpdate = model.PERFORMUPDATE,

                forms = withInner? model.FORM101DATA.Count : 0,
                deliveryStatusBad = withInner ? model.FORM101DATA.Where(fd=> fd.DELIVERYSTATUS != 0).Count() : 0,
                deliveryStatusOk = withInner ? model.FORM101DATA.Where(fd => fd.DELIVERYSTATUS == 0).Count() : 0,
                humanResourcesContacts = model.HUMANRESOURCESCONTACTS.ToList().Select(h=> h.Map(false)).ToList()
            };
        }
    }
}
