﻿using DataAccess.Mappers;
using DataAccess.Models;
using System;
using System.Linq;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class TahalichMapper
    {
        public static TahalichModel Map(this TAHALICHIM model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new TahalichModel
            {
                recId = model.RECID.ToInt(),
                rashut = model.RASHUT.ToInt(),
                mifal = model.MIFAL.ToInt(),
                owner = model.OWNER.ToInt(),
                shemTahalich = model.SHEMTAHALICH,
                tahalichType = model.TAHALICHTYPE.ToInt(),
                fromDate = model.FROMDATE.ToInt(),
                toDate = model.TODATE.ToInt(),
                creationTime = model.CREATIONTIME,
                lastUpdateTime = model.LASTUPDATETIME,
                lastUpdateUser = model.LASTUPDATEUSER.ToInt(),
                timeUnit = model.TIMEUNIT.ToInt(),
                zmanMaxHours = model.ZMANMAXHOURS.ToInt(),
                status = model.STATUS.ToInt(),
                shlavim = model.SHLAVIMs.Select(s => s.Map(withInner)).ToList()            
            };
        }
        public static TAHALICHIM Map(this TahalichModel model)
        {
            if (model == null)
                return null;

            return new TAHALICHIM
            {
                RECID = model.recId,
                RASHUT = model.rashut,
                MIFAL = model.mifal,
                OWNER = model.owner,
                SHEMTAHALICH = model.shemTahalich,
                TAHALICHTYPE = model.tahalichType,
                FROMDATE = model.fromDate,
                TODATE = model.toDate,
                CREATIONTIME = model.creationTime,
                LASTUPDATETIME = model.lastUpdateTime,
                LASTUPDATEUSER = model.lastUpdateUser,
                TIMEUNIT = model.timeUnit,
                ZMANMAXHOURS = model.zmanMaxHours,
                STATUS = model.status
            };
        }
    }
}
