﻿using DataAccess.Models;
using System;
using System.Collections.Generic;

namespace DataAccess.Mappers
{
    public static class GetMngrsMapper
    {
        public static WSPostModel MapToWSPostModel(this GetMngrsModel model)
        {
            if (model == null)
                return null;
            
            return model.model;
        }
        public static List<YehidaIrgunitModel> MapToYehidaIrgunit(this GetMngrsModel model)
        {
            if (model == null)
                return null;
            
            return model.yehida;
        }
    }
}
