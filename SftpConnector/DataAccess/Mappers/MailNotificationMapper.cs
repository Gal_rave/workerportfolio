﻿using System;
using DataAccess.Models;
using DataAccess.Extensions;
using DataAccess.Enums;

namespace DataAccess.Mappers
{
    public static class MailNotificationMapper
    {
        public static PostNotificationMail Map(this MAILNOTIFICATION model)
        {
            if (model == null)
                return null;

            return new PostNotificationMail
            {
                ID = model.ID.ToInt(),
                senderEmail = model.SENDEREMAIL,
                recipientEmail = model.RECIPIENTEMAIL,
                userIdNumber = model.USERIDNUMBER,
                userName = model.USERNAME,
                subject = model.SUBJECT,
                phone = model.PHONE,
                text = model.FREETEXT,
                createdDate = model.CREATEDDATE,
                MailType = (MailTypeEnum)model.MAILTYPE
            };
        }
        public static MAILNOTIFICATION Map(this PostNotificationMail model)
        {
            if (model == null)
                return null;

            return new MAILNOTIFICATION
            {
                ID = model.ID,
                SENDEREMAIL = string.IsNullOrWhiteSpace(model.senderEmail) && model.sender != null ? model.sender.email : model.senderEmail,
                RECIPIENTEMAIL = string.IsNullOrWhiteSpace(model.recipientEmail) && model.recipient != null ? model.recipient.email : model.recipientEmail,
                USERIDNUMBER = model.userIdNumber,
                USERNAME = model.userName,
                SUBJECT = model.subject,
                PHONE = model.phone,
                FREETEXT = model.text,
                INCLUDINGFILES = !string.IsNullOrWhiteSpace(model.files),
                CREATEDDATE = model.createdDate.HasValue ? model.createdDate.Value : DateTime.Now,
                MAILTYPE = model.MailType.HasValue? (int)model.MailType.Value : (int)MailTypeEnum.General
            };
        }
    }
}