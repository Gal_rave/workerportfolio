﻿using DataAccess.Models;
using DataAccess.Extensions;
using System;

namespace DataAccess.Mappers
{
    public static class Form101DataMapper
    {
        public static Form101DataModel Map(this FORM101DATA model)
        {
            if (model == null)
                return null;

            return new Form101DataModel
            {
                ID = model.ID.ToInt(),
                jsonData = model.JSONDATA,
                formImageId = model.FORMIMAGEID.NullableInt(),
                signatureImagId = model.SIGNATUREIMAGID.NullableInt(),
                userId = model.USERID.ToInt(),
                createdDate = model.CREATEDDATE,
                version = model.VERSION,
                deliveryStatus = model.DELIVERYSTATUS,
                actualcreationdate = model.ACTUALCREATIONDATE,
                municipalityId = model.MUNICIPALITYID
            };
        }
        public static FORM101DATA Map(this Form101DataModel model)
        {
            if (model == null)
                return null;

            return new FORM101DATA
            {
                ID = model.ID,
                JSONDATA = model.jsonData,
                FORMIMAGEID = model.formImageId,
                SIGNATUREIMAGID = model.signatureImagId,
                USERID = model.userId,
                CREATEDDATE = model.createdDate,
                VERSION = model.version,
                DELIVERYSTATUS = model.deliveryStatus,
                ACTUALCREATIONDATE = model.actualcreationdate.HasValue ? model.actualcreationdate : DateTime.Now,
                MUNICIPALITYID = model.municipalityId
            };
        }
        public static Form101DataExtensionModel Map(this FORM101DATA model, bool withInner = false)
        {
            if (model == null)
                return null;

            return new Form101DataExtensionModel
            {
                ID = model.ID.ToInt(),
                formImageId = model.FORMIMAGEID.NullableInt(),
                signatureImagId = model.SIGNATUREIMAGID.NullableInt(),
                userId = model.USERID.ToInt(),
                createdDate = model.CREATEDDATE,
                version = model.VERSION,
                deliveryStatus = model.DELIVERYSTATUS,
                actualcreationdate = model.ACTUALCREATIONDATE,
                municipalityId = model.MUNICIPALITYID,
                user = model.USER.Map(withInner),
                municipality = withInner? model.MUNICIPALITy.Map(false) : null,
                FullUserName = string.Format("{0} {1}", model.USER.FIRSTNAME, model.USER.LASTNAME),
                IdNumber = model.USER.IDNUMBER,
                MunicipalityName = model.MUNICIPALITy.NAME,
                jsonData = withInner ? model.JSONDATA : null,
                theForm = withInner ? model.USERIMAGE.Map() : null
            };
        }
    }
}
