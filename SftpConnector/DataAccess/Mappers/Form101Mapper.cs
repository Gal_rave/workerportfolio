﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using DataAccess.Models;
using DataAccess.Extensions;
using DataAccess.BLLs;
using Logger;

namespace DataAccess.Mappers
{
    public static class Form101Mapper
    {
        public static Tt_Tofes101Model MapTofes101ToWs(this TaxFromExtensionModel model)
        {
            if (model == null)
                return null;

            return new Tt_Tofes101Model
            {
                betokef_me = DateToInt(DateTime.Now),
                mifal = model.employer.mifal,
                rashut = model.employer.rashut,
                shem_tofes = "TOFES 101",
                shnat_tofes = model.year,
                form_type = model.FormType,
                taarich_milui = DateToInt(model.signatureDate),
                zman_milui = TimeInt(model.signatureDate),
                version = model.version,
            };
        }
        public static List<Tt_Tofes101_YeladimModel> MapYeladimToWs(this TaxFromExtensionModel model)
        {
            if (model == null || model.children == null || model.children.Count < 1)
                return null;
            return model.children.Select(c => c.MapChild(model.employer.mifal, model.employer.rashut)).ToList();
        }
        public static List<Tt_Tofes101_HachnasaModel> MapHachnasaToWs(this TaxFromExtensionModel model)
        {
            if (model == null || model.taxCoordination == null || model.taxCoordination.employers == null || model.taxCoordination.requestReason != 2)
                return null;

            return model.taxCoordination.employers.Select(e => e.MapEmployer(model.employer.mifal, model.employer.rashut)).ToList();
        }
        public static Tt_Tofes101_OvedModel MapOvedToWs(this TaxFromExtensionModel model)
        {
            if (model == null)
                return null;

            int _yeshuvID = 0;
            int _rehovID = 0;

            if (model.employee.Checkcity != 0 || model.employee.Checkstreet != 0)
            {
                CityBll bll = new CityBll();
                CityModel city = bll.GetCity(model.employee.city);
                if (city != null)
                {
                    _yeshuvID = city.ID;
                    StreetModel street = city.Streets.FirstOrDefault(s => s.Name == model.employee.street);
                    if (street != null)
                    {
                        if (!string.IsNullOrWhiteSpace(street.Symbol) && street.Symbol.Length > 2)
                        {
                            string _rehovid = street.Symbol.Split(',').ToList().FirstOrDefault(s => s.IndexOf(_yeshuvID.ToString()) == 0);
                            if (!string.IsNullOrWhiteSpace(_rehovid) && _rehovid.IndexOf("-") >= 0)
                            {
                                _rehovID = _rehovid.Split('-')[1].ToInt();
                            }
                        }
                        else
                        {
                            _rehovID = 0;
                        }
                    }
                }
            }

            return new Tt_Tofes101_OvedModel
            {
                rashut = model.employer.rashut,
                mifal = model.employer.mifal,
                betokef_me = DateToInt(DateTime.Now),
                oved_id = model.employee.idNumber,
                shem_mishpaha = model.employee.lastName,
                shem_prati = model.employee.firstName,
                tarich_alia = model.employee.immigrationDate.HasValue ? model.employee.immigrationDate.Value.ToString("yyyy-MM-dd") : string.Empty,
                tarich_laed = DateToNullbelInt(model.employee.birthDate),
                rehov = model.employee.street,
                rehovID = _rehovID,
                rehov_mispar = model.employee.houseNumber,
                yeshuv = model.employee.city,
                yeshuvID = _yeshuvID,
                mikud = model.employee.zip.NullableInt(),
                phone_number = model.employee.phone.NullableInt(),
                phone_number_nosaf = model.employee.cell.NullableInt(),
                min = model.employee.gender ? "זכר" : "נקבה",
                mazav_mishpati = maritalStatus(model.employee.maritalStatus),
                toshav_israel = model.employee.ilResident ? "כן" : "לא",
                haver_kupat_holim = model.employee.hmoMember.ToShort(),
                kupat_holim = model.employee.hmoName,
                email = model.employee.email,
                haver_kibbutz = model.employee.kibbutzResident.ToShort(),
                /*check employee*/
                Checktarich_laed = model.employee.CheckbirthDate,
                Checkphone_number_nosaf = model.employee.Checkcell,
                Checkyeshuv = model.employee.Checkcity,
                Checkemail = model.employee.Checkemail,
                Checkshem_prati = model.employee.CheckfirstName,
                Checkmin = model.employee.Checkgender,
                Checkhaver_kupat_holim = model.employee.CheckhmoMember,
                Checkkupat_holim = model.employee.CheckhmoName,
                Checkrehov_mispar = model.employee.CheckhouseNumber,
                Checkoved_id = model.employee.CheckidNumber,
                Checktoshav_israel = model.employee.CheckilResident,
                Checktarich_alia = model.employee.CheckimmigrationDate,
                Checkhaver_kibbutz = model.employee.CheckkibbutzResident,
                Checkshem_mishpaha = model.employee.ChecklastName,
                Checkmazav_mishpati = model.employee.CheckmaritalStatus,
                Checkphone_number = model.employee.Checkphone,
                Checkrehov = model.employee.Checkcity > 0 ? 2 : model.employee.Checkstreet,
                Checkmikud = model.employee.Checkzip,
                /*check employee*/
                sug_misra = parseIncomeType(model.income.incomeType),
                /*check incomeType*/
                Checksug_misra = model.income.incomeType.Checksug_misra(),
                /*check incomeType*/
                ben_zug_oved = model.spouse.hasIncome.HasValue && model.spouse.hasIncome.Value ? "כן" : "לא",
                ben_zug_id = model.spouse.idNumber,
                ben_zug_shem_mishpaha = model.spouse.lastName,
                ben_zug_shem_prati = model.spouse.firstName,
                ben_zug_tarich_leda = DateToNullbelInt(model.spouse.birthDate),
                ben_zug_tarich_alia = DateToNullbelInt(model.spouse.immigrationDate),
                SpousePrevIdNumber = model.spouse.PrevIdNumber,
                /*check spouse*/
                Checkben_zug_tarich_leda = model.spouse.CheckbirthDate,
                Checkben_zug_shem_prati = model.spouse.CheckfirstName,
                Checkben_zug_oved = model.spouse.CheckhasIncome,
                Checkben_zug_id = model.spouse.CheckidNumber,
                Checkben_zug_tarich_alia = model.spouse.CheckimmigrationDate,
                Checkben_zug_shem_mishpaha = model.spouse.ChecklastName,
                /*check spouse*/
                hachnasa_sug_misra = parseIncomeType(model.otherIncomes.incomeType),
                hachnasa_nzikui_lelo_nosefet = model.otherIncomes.incomeTaxCredits == 1 ? (Int16)1 : (Int16)0,    ////נקודות זיכוי ומדרגות מס כנגד הכנסתי זו 
                hachnasa_nzikui_nosefet = model.otherIncomes.incomeTaxCredits == 2 ? (Int16)1 : (Int16)0,         ////נקודות זיכוי ומדרגות מס בהכנסה אחרת
                kh_lelo_hafrash = model.otherIncomes.trainingFund.ToShort(),
                lelo_hafrash_lekizba = model.otherIncomes.workDisability.ToShort(),
                hachnasa_nosefet = model.otherIncomes.hasIncomes.ToShort(),
                /*check otherIncomes*/
                Checkhachnasa_nosefet = model.otherIncomes.CheckhasIncomes,
                Checkincomehachnasa_nzikui = model.otherIncomes.CheckincomeTaxCredits,
                Checkkh_lelo_hafrash = model.otherIncomes.ChecktrainingFund,
                Checklelo_hafrash_lekizba = model.otherIncomes.CheckworkDisability,
                /*check otherIncomes*/
                neche_100 = model.taxExemption.blindDisabled.ToShort(),
                ezor_pituah = model.taxExemption.isResident.ToShort(),
                ezor_pituah_taarich = model.taxExemption.isResident && model.taxExemption.startSettlementDate.HasValue ? DateToNullbelInt(model.taxExemption.startSettlementDate) : null,
                ole_hadash = model.taxExemption.isImmigrant && model.taxExemption.immigrationStatus == 1 ? (Int16)1 : (Int16)0,
                ole_hadash_taarich = model.taxExemption.isImmigrant && model.taxExemption.immigrationStatus == 1 && model.taxExemption.immigrationStatusDate.HasValue ? DateToNullbelInt(model.taxExemption.immigrationStatusDate) : null,
                toshav_hozer = model.taxExemption.isImmigrant && model.taxExemption.immigrationStatus == 2 ? (Int16)1 : (Int16)0,
                toshav_hozer_taarich = model.taxExemption.isImmigrant && model.taxExemption.immigrationStatus == 1 && model.taxExemption.immigrationStatusDate.HasValue ? DateToNullbelInt(model.taxExemption.immigrationStatusDate) : null,
                ben_zug_gar_lelo_hachnasot = model.taxExemption.spouseNoIncome.ToShort(),
                hore_had_hori = model.taxExemption.separatedParent.ToShort(),
                yeladim_behezkati = model.taxExemption.childrenInCare.ToShort(),
                incompetentChild = model.taxExemption.incompetentChild.ToShort(),
                yeladim_peotim_lelo_hezka = model.taxExemption.infantchildren.ToShort(),
                hore_yahid = model.taxExemption.separatedParent.ToShort(),
                ben_zug_mezonot = model.taxExemption.exAlimony.ToShort(),
                ben16_katan_m18 = model.taxExemption.partnersUnder18.ToShort(),
                hayal_meshuhrar = model.taxExemption.exServiceman.ToShort(),
                taarich_giyus = model.taxExemption.exServiceman && model.taxExemption.serviceStartDate.HasValue ? DateToNullbelInt(model.taxExemption.serviceStartDate) : null,
                taarich_shihrur = model.taxExemption.exServiceman && model.taxExemption.serviceEndDate.HasValue ? DateToNullbelInt(model.taxExemption.serviceEndDate) : null,
                toar_academi = model.taxExemption.graduation.ToShort(),
                /*check taxExemption*/
                Checkneche_100 = model.taxExemption.CheckblindDisabled,
                Checkyeladim_behezkati = model.taxExemption.CheckchildrenInCare,
                CheckincompetentChild = model.taxExemption.CheckincompetentChild,
                Checkben_zug_mezonot = model.taxExemption.CheckexAlimony,
                Checkhayal_meshuhrar = model.taxExemption.CheckexServiceman,
                Checktoar_academi = model.taxExemption.Checkgraduation,
                Checkole_hadash = model.taxExemption.CheckimmigrantStatus,
                Checkole_hadash_taarich = model.taxExemption.CheckimmigrantStatusDate,
                Checkyeladim_peotim_lelo_hezka = model.taxExemption.Checkinfantchildren,
                Checkezor_pituah = model.taxExemption.CheckisResident,
                Checkben16_katan_m18 = model.taxExemption.CheckpartnersUnder18,
                Checkhore_yahid = model.taxExemption.CheckseparatedParent,
                Checktaarich_shihrur = model.taxExemption.CheckservicemanEndDate,
                Checktaarich_giyus = model.taxExemption.CheckservicemanStartDate,
                Checkben_zug_lelo_hachnasot = model.taxExemption.CheckspouseNoIncome,
                /*check taxExemption*/
                mispar_yeldim_ad_3 = model.children.ThreeOrBorn(model.year),
                mispar_yeldim_ad_1_or_2 = model.children.OneOrTow(model.year),
                yeladim_ad_18 = model.children.BornOrEighteen(model.year),
                yeladim_ad_5 = model.children.OneToFive(model.year),
                yeladim_terem_19 = model.children.AnderNineteen(model.year),
                //no_hacnasa = ??,
                sibat_teum_mas = model.taxCoordination.requestReason,
                checksibat_teum_mas = model.taxCoordination.Checkreason
                //yeled_lo_hezka = ??
            };

        }
        public static Tt_Full101FromModel MapFullTofes101(this TaxFromExtensionModel model)
        {
            if (model == null)
                return null;

            return new Tt_Full101FromModel
            {
                oved = model.MapOvedToWs(),
                hachnasa = model.MapHachnasaToWs(),
                t101 = model.MapTofes101ToWs(),
                yeladim = model.MapYeladimToWs()
            };
        }

        #region private
        private static Tt_Tofes101_HachnasaModel MapEmployer(this EmployerExtensionModel model, int mifal, int rashut)
        {
            if (model == null)
                return null;

            return new Tt_Tofes101_HachnasaModel
            {
                betokef_me = DateToInt(DateTime.Now),
                hachnasa_hodshit = model.salary,
                hachnasa_nosefet_ktovet = model.address,
                hachnasa_nosefet_mas_shnuka = model.taxDeduction,
                hachnasa_nosefet_shem_maavid = model.name,
                hachnasa_nosefet_sug_hachnasa = parseIncomeType(model.incomeType),
                hachnasa_nosefet_tik_nikuiim = model.deductionFileID.ToInt(),
                mifal = mifal,
                rashut = rashut,
                Checkhachnasa_nosefet_ktovet = model.Checkaddress,
                Checkhachnasa_nosefet_tik = model.CheckdeductionFileID,
                Checkhachnasa_nosefet_sug = model.CheckincomeType,
                Checkhachnasa_nosefet_shem = model.Checkname,
                Checkhachnasa_hodshit = model.Checksalary,
                Checkhachnasa_nosefet_mas = model.ChecktaxDeduction
            };
        }
        private static Tt_Tofes101_YeladimModel MapChild(this ChildExtensionModel model, int mifal, int rashut)
        {
            if (model == null)
                return null;

            return new Tt_Tofes101_YeladimModel
            {
                id_yeled = model.idNumber,
                mifal = mifal,
                rashut = rashut,
                shem_yeled = model.firstName,
                tarich_leda_yeled = DateToInt(model.birthDate),
                betokef_me = DateToInt(DateTime.Now),
                Checktarich_leda_yeled = model.CheckbirthDate,
                Checkshem_yeled = model.CheckfirstName,
                Checkid_yeled = model.CheckidNumber,
                PrevIdNumber = model.PrevIdNumber,
                Checkchild_benefit = model.CheckchildBenefit,
                child_benefit = model.childBenefit ? 1 : 0,
                Checkgar_babait = model.CheckhomeLiving,
                gar_babait = model.homeLiving ? 1 : 0,
                gender = model.gender ? 1 : 0,
                Checkgender = model.Checkgender
            };
        }
        private static string maritalStatus(int? val)
        {
            switch (val)
            {
                case 1:
                    return "רווק/ה";
                case 2:
                    return "נשוי/אה";
                case 3:
                    return "גרוש/ה";
                case 4:
                    return "אלמן/נה";
                case 5:
                    return "פרוד/ה";
                default:
                    return "";
            }
        }
        private static int parseIncomeType(IncomeTypeExtensionModel incomeType)
        {
            if (incomeType.monthSalary)///רגילה
                return 1;
            if (incomeType.anotherSalary)///נוספת
                return 2;
            if (incomeType.daySalary)///יומי
                return 3;
            if (incomeType.allowance)///קיצבה
                return 4;
            if (incomeType.stipend)///מילגה
                return 7;
            if (incomeType.partialSalary)///חלקית           TODO-> אם דיווח משכורת נוספת אז 2 אם אין נוספת אז 1
                return 1;
            if (incomeType.anotherSource)///מקור אחר
                return 8;

            return 0;
        }
        private static int DateToInt(DateTime val)
        {
            string date = val.Year.ToString() + PadDate(val.Month) + PadDate(val.Day);
            return date.ToInt();
        }
        private static int? DateToNullbelInt(DateTime? val)
        {
            if (val == null || !val.HasValue)
                return null;

            string date = val.Value.Year.ToString() + PadDate(val.Value.Month) + PadDate(val.Value.Day);
            return date.ToInt();
        }
        private static int TimeInt(DateTime val)
        {
            string date = PadDate(val.Hour) + PadDate(val.Minute) + PadDate(val.Second);
            return date.ToInt();
        }
        private static string PadDate(int date)
        {
            if (date < 10)
                return "0" + date.ToString();
            return date.ToString();
        }
        private static int Checksug_misra(this IncomeTypeExtensionModel model)
        {
            int sum = model.Checkallowance + model.CheckanotherSalary + model.CheckanotherSource + model.CheckdaySalary + model.CheckmonthSalary + model.CheckpartialSalary + model.Checkstipend;
            if (sum == 0)
                return 0;
            if (model.Checkallowance == 1 || model.CheckanotherSalary == 1 || model.CheckanotherSource == 1 || model.CheckdaySalary == 1 || model.CheckmonthSalary == 1 || model.CheckpartialSalary == 1 || model.Checkstipend == 1)
                return 1;
            return 2;
        }
        private static Int16 ToShort(this bool? val)
        {
            if (val == null || !val.HasValue || string.IsNullOrWhiteSpace(val.ToString()))
                return (Int16)0;

            return val.Value ? (Int16)1 : (Int16)0;
        }
        #endregion
    }
}