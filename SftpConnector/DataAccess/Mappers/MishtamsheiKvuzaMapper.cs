﻿using DataAccess.Models;
using System;
using System.Linq;
using DataAccess.Extensions;


namespace DataAccess.Mappers
{
    public static class MishtamsheiKvuzaMapper
    {
        public static MishtamsheiKvuzaModel Map(this MISHTAMSHEI_KVUZA model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new MishtamsheiKvuzaModel
            {
                recId = model.REC_ID.ToInt(),
                rashut = model.RASHUT.ToInt(),
                mifal = model.MIFAL.ToInt(),
                fromDate = model.FROM_DATE.ToInt(),
                toDate = model.TO_DATE.ToInt(),
                kvuzaId = model.KVUZA_ID.ToInt(),
                mezaheTnay = model.MEZAE_TNAY.ToInt(),
                mishtameshId = model.MISHTAMESH_ID.ToInt(),
                creationTime = model.CREATION_TIME,
                lastUpdateTime = model.LAST_UPDATE_TIME,
                lastUpdateUser = model.LAST_UPDATE_USER.ToInt(),
                status = model.STATUS.ToInt()
            };
        }
        public static MISHTAMSHEI_KVUZA Map(this MishtamsheiKvuzaModel model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new MISHTAMSHEI_KVUZA
            {
                REC_ID = model.recId,
                RASHUT = model.rashut,
                MIFAL = model.mifal,
                FROM_DATE = model.fromDate,
                TO_DATE = model.toDate,
                MISHTAMESH_ID = model.mishtameshId,
                KVUZA_ID = model.kvuzaId,
                MEZAE_TNAY = model.mezaheTnay,
                CREATION_TIME = model.creationTime,
                LAST_UPDATE_TIME = model.lastUpdateTime,
                LAST_UPDATE_USER = model.lastUpdateUser,
                STATUS = model.status
            };
        }
    }
}
