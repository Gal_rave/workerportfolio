﻿using DataAccess.Models;
using System.Linq;

namespace DataAccess.Mappers
{
    public static class DirectoryMaper
    {
        public static DirectoryModel Map(this DIRECTORy model)
        {
            if (model == null)
                return null;
            return new DirectoryModel
            {
                ID = model.ID,
                municipalityId = model.MUNICIPALITYID,
                parentId = model.PARENTID,
                name = model.NAME,

                subDirectories = model.DIRECTORIES1.ToList().Select(d=> d.Map()).ToList(),
                images = model.IMAGES.ToList().Select(d => d.Map()).ToList()
            };
        }
        public static DIRECTORy Map(this DirectoryModel model)
        {
            if (model == null)
                return null;
            return new DIRECTORy
            {
                ID = model.ID,
                MUNICIPALITYID = model.municipalityId,
                PARENTID = model.parentId,
                NAME = model.name
            };
        }
    }
}
