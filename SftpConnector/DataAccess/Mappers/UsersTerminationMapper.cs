﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class UsersTerminationMapper
    {
        public static UsersTerminationModel Map(this USERSTERMINATION model)
        {
            if (model == null)
                return null;

            return new UsersTerminationModel
            {
                userId = model.USERID.ToInt(),
                municipalityId = model.MUNICIPALITYID.ToInt(),
                terminationCause = model.TERMINATIONCAUSE.ToInt(),
                terminationDate = model.TERMINATIONDATE,
                commencementDate = model.COMMENCEMENTDATE
            };
        }
        public static USERSTERMINATION Map(this UsersTerminationModel model)
        {
            if (model == null)
                return null;

            return new USERSTERMINATION
            {
                USERID = model.userId,
                MUNICIPALITYID = model.municipalityId,
                TERMINATIONCAUSE = model.terminationCause,
                TERMINATIONDATE = model.terminationDate,
                COMMENCEMENTDATE = model.commencementDate
            };
        }
    }
}
