﻿using DataAccess.Models;
using System;

namespace DataAccess.Mappers
{
    public static class MishtameshSacharMapper
    {
        public static MistameshSacharModel Map(this SACHAR_USERS model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new MistameshSacharModel
            {
                id = Convert.ToInt32(model.USERID),
                kodMedaveach = Convert.ToInt32(model.KOD_MEDAVEACH),
                rashut = Convert.ToInt32(model.RASHUT),
                user = withInner ? model.USER.Map(false) : null
            };
        }
        public static SACHAR_USERS Map(this MistameshSacharModel model)
        {
            if (model == null)
                return null;

            return new SACHAR_USERS
            {
                ID = model.id,
                KOD_MEDAVEACH = model.kodMedaveach,
                RASHUT = model.rashut
            };
        }
    }
}