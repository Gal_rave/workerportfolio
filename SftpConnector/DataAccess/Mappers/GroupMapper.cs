﻿using System.Linq;
using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class GroupMapper
    {
        public static GroupModel Map(this GROUP model, bool withInner = true)
        {
            if (model == null)
                return null;
            return new GroupModel
            {
                ID = model.ID.ToInt(),
                immunity = model.IMMUNITY.ToInt(),
                name = model.NAME,
                users = withInner? model.USERS.ToList().Select(u=> u.Map(false)).ToList() : null
            };
        }
        public static GROUP Map(this GroupModel model)
        {
            if (model == null)
                return null;
            return new GROUP
            {
                ID = model.ID.ToInt(),
                IMMUNITY = model.immunity,
                NAME = model.name,
            };
        }
    }
}
