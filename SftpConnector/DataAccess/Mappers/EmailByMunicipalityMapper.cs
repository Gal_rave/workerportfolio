﻿using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class EmailByMunicipalityMapper
    {
        public static EmailByMunicipalityModel Map(this EMAILBYMUNICIPALITY model)
        {
            if (model == null)
                return null;

            return new EmailByMunicipalityModel
            {
                ID = model.ID.ToInt(),
                idNumber = model.IDNUMBER,
                municipalityId = model.MUNICIPALITYID.ToInt(),
                email = model.EMAIL,
                userId = model.USERID.ToInt(),
                doPaycheckPopup = model.DOPAYCHECKPOPUP
            };
        }
        public static EMAILBYMUNICIPALITY Map(this EmailByMunicipalityModel model)
        {
            if (model == null)
                return null;

            return new EMAILBYMUNICIPALITY
            {
                ID = model.ID,
                IDNUMBER = model.idNumber,
                MUNICIPALITYID = model.municipalityId,
                EMAIL = model.email,
                USERID = model.userId,
                DOPAYCHECKPOPUP = model.doPaycheckPopup
            };
        }
    }
}
