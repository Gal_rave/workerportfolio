﻿using DataAccess.Extensions;
using DataAccess.Models;
using System;

namespace DataAccess.Mappers
{
    public static class HtmlTemplateMapper
    {
        public static HtmlTemplateModel Map(this HTMLTEMPLATE model, bool include = false)
        {
            if (model == null)
                return null;

            return new HtmlTemplateModel
            {
                createdDate = model.CREATEDDATE,
                fromEmail = model.FROMEMAIL,
                ID = model.ID,
                municipalityId = model.MUNICIPALITYID,
                name = model.NAME,
                subject = model.SUBJECT,
                template = model.TEMPLATE,
                updateDate = model.UPDATEDATE,
                updateUserId = model.UPDATEUSERID,
                type = model.TYPE,
                active = model.ACTIVE,

                updateDateString = model.UPDATEDATE.HasValue ? model.UPDATEDATE.Value.ToString("dd/MM/yyyy HH:MM") : model.CREATEDDATE.ToString("dd/MM/yyyy HH:MM"),
                UpdateUser = model.USER != null ? string.Format("{0} {1}", model.USER.FIRSTNAME, model.USER.LASTNAME) : string.Empty,

                municipality = include ? model.MUNICIPALITy.Map(false) : null,
                user = include ? model.USER.Map(false) : null,
            };
        }
        public static HTMLTEMPLATE Map(this HtmlTemplateModel model, int? userID = null)
        {
            if (model == null)
                return null;

            return new HTMLTEMPLATE
            {
                CREATEDDATE = model.ID > 0 ? model.createdDate : DateTime.Now,
                FROMEMAIL = model.fromEmail,
                ID = model.ID,
                MUNICIPALITYID = model.municipalityId,
                NAME = model.name,
                SUBJECT = model.subject,
                TEMPLATE = model.template,
                UPDATEDATE = model.updateDate.HasValue ? model.updateDate.Value : DateTime.Now,
                UPDATEUSERID = userID.HasValue ? userID.Value : model.updateUserId,
                TYPE = model.type,
                ACTIVE = model.active,
            };
        }
        public static void Map(this HTMLTEMPLATE model, HtmlTemplateModel templateModel, int? userID = null)
        {
            if (model == null)
                return;

            model.CREATEDDATE = templateModel.ID > 0 ? templateModel.createdDate : DateTime.Now;
            model.FROMEMAIL = templateModel.fromEmail;
            model.MUNICIPALITYID = templateModel.municipalityId;
            model.NAME = templateModel.name;
            model.SUBJECT = templateModel.subject;
            model.TEMPLATE = templateModel.template;
            model.UPDATEDATE = templateModel.updateDate.HasValue ? templateModel.updateDate.Value : DateTime.Now;
            model.UPDATEUSERID = userID.HasValue ? userID.Value : templateModel.updateUserId;
            model.TYPE = templateModel.type;
            model.ACTIVE = templateModel.active;
        }
    }
}
