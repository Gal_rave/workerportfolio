﻿using DataAccess.Models;

namespace DataAccess.Mappers
{
    public static class MembershipQuestionMapper
    {
        public static MembershipQuestionModel Map(this MEMBERSHIP_QUESTIONS model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new MembershipQuestionModel
            {
                ID = model.ID,
                answer = model.ANSWER,
                question = model.QUESTION,
                userId = model.USERID,
                user = withInner? model.USER.Map() : null
            };
        }

        public static MEMBERSHIP_QUESTIONS Map(this MembershipQuestionModel model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new MEMBERSHIP_QUESTIONS
            {
                ID = model.ID,
                ANSWER = model.answer,
                QUESTION = model.question,
                USERID = model.userId,
            };
        }
    }
}
