﻿using DataAccess.Models;
using System;

namespace DataAccess.Mappers
{
    public static class AcademicInstMapper
    {
        public static AcademicInstModel Map(this ACADEMIC_INST model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new AcademicInstModel
            {
                ID = Convert.ToInt32(model.ID),
                UNIQ_ID = model.UNIQ_ID,
                FCODE_TYPE = model.FCODE_TYPE,
                SETL_CODE = model.SETL_CODE,
                SETL_NAME = model.SETL_NAME,
                NAME = model.NAME,
                LATIN_NAME = model.LATIN_NAME,
                USG_GROUP = model.USG_GROUP,
                USG_CODE = model.USG_CODE,
                E_ORD = model.E_ORD,
                N_ORD = model.N_ORD,
                DATA_YEAR = model.DATA_YEAR,
                PRDCT_VER = model.PRDCT_VER,
                X = model.X,
                Y = model.Y
            };
        }
        public static ACADEMIC_INST Map(this AcademicInstModel model)
        {
            if (model == null)
                return null;

            return new ACADEMIC_INST
            {
                ID = Convert.ToInt32(model.ID),
                UNIQ_ID = model.UNIQ_ID,
                FCODE_TYPE = model.FCODE_TYPE,
                SETL_CODE = model.SETL_CODE,
                SETL_NAME = model.SETL_NAME,
                NAME = model.NAME,
                LATIN_NAME = model.LATIN_NAME,
                USG_GROUP = model.USG_GROUP,
                USG_CODE = model.USG_CODE,
                E_ORD = model.E_ORD,
                N_ORD = model.N_ORD,
                DATA_YEAR = model.DATA_YEAR,
                PRDCT_VER = model.PRDCT_VER,
                X = model.X,
                Y = model.Y
            };
        }
    }
}
