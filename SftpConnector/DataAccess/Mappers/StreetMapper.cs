﻿using System.Linq;
using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class StreetMapper
    {
        public static StreetModel Map(this STREET model, bool withInner = true)
        {
            if (model == null)
                return null;
            return new StreetModel
            {
                ID = model.ID.ToInt(),
                Name = model.NAME,
                Symbol = model.SYMBOL,
                Cities = withInner ? model.CITIES.ToList().Select(c => c.Map(false)).ToList() : null
            };
        }
        public static STREET Map(this StreetModel model)
        {
            if (model == null)
                return null;
            return new STREET
            {
                ID = model.ID,
                NAME = model.Name,
                SYMBOL = model.Symbol,
            };
        }
    }
}
