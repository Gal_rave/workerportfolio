﻿using DataAccess.Models;

namespace DataAccess.Mappers
{
    public static class ProcessMapper
    {
        public static ProcessModel Map(this PROCESS model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new ProcessModel
            {
                CONTENT1 = model.CONTENT1,
                CONTENT2 = model.CONTENT2,
                CONTENT3 = model.CONTENT3,
                CONTENT4 = model.CONTENT4,
                CONTENT5 = model.CONTENT5,
                CONTENT6 = model.CONTENT6,
                CONTENT7 = model.CONTENT7,
                COMMENTS = model.COMMENTS,
                FROM_DATE = model.FROM_DATE,
                ID = model.ID,
                MIFAL = model.MIFAL,
                PROCESS_DESCRIPTION = model.PROCESS_DESCRIPTION,
                PROCESS_TYPE = model.PROCESS_TYPE,
                RASHUT = model.RASHUT,
                STATUS = model.STATUS,
                TARGET_USERID = model.TARGET_USERID,
                TO_DATE = model.TO_DATE,
                USERID = model.USERID,
                CREATION_DATE = model.CREATION_DATE,
                LAST_UPDATE = model.LAST_UPDATE,
                OVED_NESU_BAKASHA = model.OVED_NESU_BAKASHA,
                OVED_NESU_BAKASHA_NAME = model.OVED_NESU_NAME,
                USERMODEL = !withInner ? null : model.USER1.Map(false),
                TARGET_USER = !withInner ? null : model.USER.Map(false),
                TT_TAHALICH_ID = model.TT_TAHALICH_ID,
                TT_SHALAV_ID = model.TT_SHALAV_ID,
                CURR_MISPAR_SHALAV = model.CURR_MISPAR_SHALAV,
                SHLAVIM_COUNTER = model.SHLAVIM_COUNTER,
                TM_TAHALICH_ID = model.TM_TAHALICH_ID
            };
        }
        public static PROCESS Map(this ProcessModel model)
        {
            if (model == null)
                return null;

            return new PROCESS
            {
                CONTENT1 = model.CONTENT1,
                CONTENT2 = model.CONTENT2,
                CONTENT3 = model.CONTENT3,
                CONTENT4 = model.CONTENT4,
                CONTENT5 = model.CONTENT5,
                CONTENT6 = model.CONTENT6,
                CONTENT7 = model.CONTENT7,
                COMMENTS = model.COMMENTS,
                FROM_DATE = model.FROM_DATE,
                ID = model.ID,
                MIFAL = model.MIFAL,
                PROCESS_DESCRIPTION = model.PROCESS_DESCRIPTION,
                PROCESS_TYPE = model.PROCESS_TYPE,
                RASHUT = model.RASHUT,
                STATUS = model.STATUS,
                TARGET_USERID = model.TARGET_USERID,
                TO_DATE = model.TO_DATE,
                USERID = model.USERID,
                CREATION_DATE = model.CREATION_DATE,
                LAST_UPDATE = model.LAST_UPDATE,
                OVED_NESU_BAKASHA = model.OVED_NESU_BAKASHA,
                OVED_NESU_NAME = model.OVED_NESU_BAKASHA_NAME,
                TT_TAHALICH_ID = model.TT_TAHALICH_ID,
                TT_SHALAV_ID = model.TT_SHALAV_ID,
                CURR_MISPAR_SHALAV = model.CURR_MISPAR_SHALAV,
                SHLAVIM_COUNTER = model.SHLAVIM_COUNTER,
                TM_TAHALICH_ID = model.TM_TAHALICH_ID
            };
        }
    }
}
