﻿using DataAccess.Models;
using System;
using System.Linq;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class KvuzotMishtamshimMapper
    {
        public static KvuzatMishtamshimModel Map(this KVUZOT_MISHTAMSHIM model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new KvuzatMishtamshimModel
            {
                recId = model.REC_ID.ToInt(),
                rashut = model.RASHUT.ToInt(),
                mifal = model.MIFAL.ToInt(),
                fromDate = model.FROM_DATE.ToInt(),
                toDate = model.TO_DATE.ToInt(),
                kvuzaType = model.KVUZA_TYPE.ToInt(),
                shemKvuza = model.SEHM_KVUZA,
                creationTime = model.CREATION_TIME,
                lastUpdateTime = model.LAST_UPDATE_TIME,
                lastUpdateUser = model.LAST_UPDATE_USER.ToInt(),
                status = model.STATUS.ToInt(),
                mishtamsheiKvuzaModel = model.MISHTAMSHEI_KVUZA.Select(mk => mk.Map(withInner)).ToList()
            };
        }
        public static KVUZOT_MISHTAMSHIM Map(this KvuzatMishtamshimModel model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new KVUZOT_MISHTAMSHIM
            {
                REC_ID = model.recId,
                RASHUT = model.rashut,
                MIFAL = model.mifal,
                FROM_DATE = model.fromDate,
                TO_DATE = model.toDate,
                KVUZA_TYPE = model.kvuzaType,
                SEHM_KVUZA = model.shemKvuza,
                CREATION_TIME = model.creationTime,
                LAST_UPDATE_TIME = model.lastUpdateTime,
                LAST_UPDATE_USER = model.lastUpdateUser,
                STATUS = model.status
            };
        }
    }
}
