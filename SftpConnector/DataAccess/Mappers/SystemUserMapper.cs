﻿using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class SystemUserMapper
    {
        public static SystemUserModel Map(this SYSTEM_USERS model)
        {
            if (model == null)
                return null;

            return new SystemUserModel()
            {
                ID = model.ID,
                idNumber = model.IDNUMBER,
                initialPassword = model.INITIALPASSWORD,
                municipalityId = model.MUNICIPALITYID,
                mancol = model.MANCOL,
                email = model.EMAIL,
                kidomet = model.KIDOMET,
                cell = model.CELL,
                cellNumber = model.CELLNUMBER,
                fullIdNumber = model.FULLIDNUMBER,
                terminationCause = model.TERMINATIONCAUSE.NullableInt(),
                terminationDate = model.TERMINATIONDATE,
                terminationDateString = model.TERMINATIONDATESTRING,
                commencementDate = model.COMMENCEMENTDATE,
                mifal = model.MIFAL
            };
        }
        public static SystemUserModel Map(this SYSTEM_USERS model, MunicipalityModel municipalityModel)
        {
            if (model == null)
                return null;

            return new SystemUserModel()
            {
                ID = model.ID,
                idNumber = model.IDNUMBER,
                initialPassword = model.INITIALPASSWORD,
                municipalityId = model.MUNICIPALITYID,
                mancol = model.MANCOL,
                email = model.EMAIL,
                kidomet = model.KIDOMET,
                cell = model.CELL,
                cellNumber = model.CELLNUMBER,
                fullIdNumber = model.FULLIDNUMBER,
                terminationCause = model.TERMINATIONCAUSE.NullableInt(),
                terminationDate = model.TERMINATIONDATE,
                terminationDateString = model.TERMINATIONDATESTRING,
                municipality = municipalityModel,
                commencementDate = model.COMMENCEMENTDATE,
                mifal = model.MIFAL
            };
        }
        public static SystemUserModel Map(this SYSTEM_USERS model, MUNICIPALITy municipality)
        {
            if (model == null)
                return null;

            return new SystemUserModel()
            {
                ID = model.ID,
                idNumber = model.IDNUMBER,
                initialPassword = model.INITIALPASSWORD,
                municipalityId = model.MUNICIPALITYID,
                mancol = model.MANCOL,
                email = model.EMAIL,
                kidomet = model.KIDOMET,
                cell = model.CELL,
                cellNumber = model.CELLNUMBER,
                fullIdNumber = model.FULLIDNUMBER,
                terminationCause = model.TERMINATIONCAUSE.NullableInt(),
                terminationDate = model.TERMINATIONDATE,
                terminationDateString = model.TERMINATIONDATESTRING,
                municipality = municipality.Map(false),
                commencementDate = model.COMMENCEMENTDATE,
                mifal = model.MIFAL
            };
        }
        public static SYSTEM_USERS Map(this SystemUserModel model)
        {
            if (model == null)
                return null;

            return new SYSTEM_USERS()
            {
                ID = model.ID,
                IDNUMBER = model.idNumber,
                INITIALPASSWORD = model.initialPassword,
                MUNICIPALITYID = model.municipalityId,
                MANCOL = model.mancol,
                EMAIL = model.email,
                KIDOMET = model.kidomet,
                CELL = model.cell,
                CELLNUMBER = model.cellNumber,
                FULLIDNUMBER = model.fullIdNumber,
                TERMINATIONCAUSE = model.terminationCause,
                TERMINATIONDATE = model.terminationDate,
                TERMINATIONDATESTRING = model.terminationDateString,
                COMMENCEMENTDATE = model.commencementDate,
                MIFAL = model.mifal
            };
        }
    }
}
