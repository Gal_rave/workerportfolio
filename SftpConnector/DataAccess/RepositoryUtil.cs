﻿using Catel.IoC;
using DataAccess.Interfaces;
using DataAccess.Repositories;

namespace DataAccess
{
    public class RepositoryUtil
    {
        public static void SetDbContextType()
        {
            var serviceLocator = ServiceLocator.Default;
            serviceLocator.RegisterType<IChildRepository, ChildRepository>();
            serviceLocator.RegisterType<ICityRepository, CityRepository>();
            serviceLocator.RegisterType<ICoursRepository, CoursRepository>();
            serviceLocator.RegisterType<IEmailByMunicipalityRepository, EmailByMunicipalityRepository>();
            serviceLocator.RegisterType<IDirectoryRepository, DirectoryRepository>();
            serviceLocator.RegisterType<IForm101DataRepository, Form101DataRepository>();
            serviceLocator.RegisterType<IHtmlTemplatesRepository, HtmlTemplatesRepository>();
            serviceLocator.RegisterType<IFormStatisticsRepository, FormStatisticsRepository>();
            serviceLocator.RegisterType<IGroupsRepository, GroupsRepository>();
            serviceLocator.RegisterType<IHumanResourcesContactRepository, HumanResourcesContactRepository>();
            serviceLocator.RegisterType<IInsertedSystemUsersRepository, InsertedSystemUsersRepository>();
            serviceLocator.RegisterType<IImageRepository, ImageRepository>();
            serviceLocator.RegisterType<ILoginLogRepository, LoginLogRepository>();
            serviceLocator.RegisterType<IMailNotificationRepository, MailNotificationRepository>();
            serviceLocator.RegisterType<IMembershipArchiveRepository, MembershipArchiveRepository>();
            serviceLocator.RegisterType<IMembershipQuestionsRepository, MembershipQuestionsRepository>();
            serviceLocator.RegisterType<IMembershipRepository, MembershipRepository>();
            serviceLocator.RegisterType<IMunicipalityContactRepository, MunicipalityContactRepository>();
            serviceLocator.RegisterType<IMunicipalityRepository, MunicipalityRepository>();
            serviceLocator.RegisterType<IPayCheckUsersByMailRepository, PayCheckUsersByMailRepository>();
            serviceLocator.RegisterType<IProcessRepository, ProcessRepository>();
            serviceLocator.RegisterType<IOrgUnitMngrsRepository, OrgUnitMngrsRepository>();
            serviceLocator.RegisterType<IPcueSendArchiveRepository, PcueSendArchiveRepository>();
            serviceLocator.RegisterType<ITahalichimRepository, TahalichimRepository>();
            serviceLocator.RegisterType<IShlavimRepository, ShlavimRepository>();
            serviceLocator.RegisterType<IReshimotTiyugRepository, ReshimotTiyugRepository>(); 
            serviceLocator.RegisterType<IMismachimBeReshimaRepository, MismachimBeReshimaRepository>(); 
            serviceLocator.RegisterType<IMismachimRepository, MismachimRepository>();
            serviceLocator.RegisterType<IMishtamsheiKvuzaRepository, MishtamsheiKvuzaRepository>();
            serviceLocator.RegisterType<IKvuzotMishtamshimRepository, KvuzotMishtamshimRepository>();
            serviceLocator.RegisterType<IAcademicInstRepository, AcademicInstRepository>();
            serviceLocator.RegisterType<ISacharUserRepository, SacharUserRepository>();//gelem table
            serviceLocator.RegisterType<IMistameshSacharRepository, MistameshSacharRepository>();//real data table
            serviceLocator.RegisterType<IsmsNotificationRepository, smsNotificationRepository>();
            serviceLocator.RegisterType<ISpouseRepository, SpouseRepository>();
            serviceLocator.RegisterType<IStreetRepository, StreetRepository>();
            serviceLocator.RegisterType<ISystem_citiesRepository, System_citiesRepository>();
            serviceLocator.RegisterType<ISystem_streetsRepository, System_streetsRepository>();
            serviceLocator.RegisterType<ISystemUsersRepository, SystemUsersRepository>();
            serviceLocator.RegisterType<ITerminationRepository, TerminationRepository>();
            serviceLocator.RegisterType<IUpdateUserMunicipalityContactRepository, UpdateUserMunicipalityContactRepository>();
            serviceLocator.RegisterType<IUserImageRepository, UserImageRepository>();
            serviceLocator.RegisterType<IUserRepository, UserRepository>();
            serviceLocator.RegisterType<IUsersTerminationRepository, UsersTerminationRepository>();
            serviceLocator.RegisterType<IUsersToFactoryRepository, UsersToFactoryRepository>();
        }
    }
}
