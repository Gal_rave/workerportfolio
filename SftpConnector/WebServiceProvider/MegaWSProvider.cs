﻿using System;
using System.Configuration;

using WebServiceProvider.Mga2WS;
using DataAccess.Extensions;
using Logger;

namespace WebServiceProvider
{
    public static class MegaWSProvider
    {
        private static string interfaceId = "1";
        private static string functionId = "506";

        private static Mga2Provider getWS(int idNumber, int customerId)
        {
            Mga2Provider WS = new Mga2Provider();
            WS.RequestHeaderValue = getHeader(idNumber, customerId);
            WS.Url = ConfigurationManager.AppSettings["MegaWSProvider"];
            WS.RequestEncoding = System.Text.Encoding.UTF8;
            
            return WS;
        }
        private static RequestHeader getHeader(int idNumber, int customerId)
        {
            RequestHeader requestheader = new RequestHeader();
            requestheader.Headers = new object[4];
            requestheader.Headers[0] = customerId.ToString();
            requestheader.Headers[1] = functionId;
            requestheader.Headers[2] = idNumber.ToString();
            requestheader.Headers[3] = interfaceId;

            return requestheader;
        }        
        public static int GetMifal(int idNumber, int customerId, DateTime requestTime)
        {
            Mga2Provider WS = getWS(idNumber, customerId);

            Kai005n0Response response;
            
            Kai005n0Request request = new Kai005n0Request();
            request.Oved = idNumber;
            request.Year = requestTime.Year.ToShort();
            request.RashutHanHesbon = customerId;
            request.Month = requestTime.Month.ToShort();

            try
            {
                response = WS.ExecuteKAI005A0(request);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
            return response.Mifal.ToInt();
        }
        public static object GetTrainingCourses(int idNumber, int customerId)
        {
            Mga2Provider WS = getWS(idNumber, customerId);

            Kai002n1Response response;
            Kai002n1Request request = new Kai002n1Request();
            request.Dbid = 0;
            request.Debug = 0;
            request.Oved = idNumber;
            request.RashutHanHesbon = customerId;

            try
            {
                response = WS.ExecuteKAI002A1(request);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }

            return response;
        }
        public static object GetEmployeeAbsence(int idNumber, int customerId, DateTime requestTime)
        {
            Mga2Provider WS = getWS(idNumber, customerId);

            Kai005n0Response response = null;
            Kai005n0Request request = new Kai005n0Request();
            request.Dbid = 0;
            request.Debug = 0;
            request.Oved = idNumber;
            request.RashutHanHesbon = customerId;
            request.Year = requestTime.Year.ToShort();

            try
            {
                response = WS.ExecuteKAI005A0(request);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }

            return response;
        }
    }
}
