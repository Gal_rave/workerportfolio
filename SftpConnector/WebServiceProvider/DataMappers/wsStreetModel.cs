﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServiceProvider.DataMappers
{
    public class wsStreetModel
    {
        public int SETTLEMENTSYMBOL { get; set; }
        public int STREETSYMBOL { get; set; }
        public string STREETNAME { get; set; }
    }
}
