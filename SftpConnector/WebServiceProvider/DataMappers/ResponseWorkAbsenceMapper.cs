﻿using DataAccess.Extensions;
using DataAccess.Models;
using System.Globalization;
using WebServiceProvider.PortalWS;

namespace WebServiceProvider.DataMappers
{
    public static class ResponseWorkAbsenceMapper
    {
        public static ResponseWorkAbsence Map(this Kai005n0ResponseNetuneHeadrut model)
        {
            if (model == null)
                return null;
            
            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;

            return new ResponseWorkAbsence
            {
                month = model.Month,
                value = (model.Value / 10).ToString("N", nfi),
                columnNumber = model.Headrut,
                tabTarget = model.KodNoseAv,
                columnTitle = model.THeadrut.StringTrim(),
                tabTitle = model.TeurNoseAv.StringTrim(),
                units = model.Yehidot
            };
        }
    }
}
