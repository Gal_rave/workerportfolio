﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataAccess.Models;
using DataAccess.Extensions;
using WebServiceProvider.PortalWS;

namespace WebServiceProvider.DataMappers
{
    public static class ResponsePersonalInformationMapper
    {
        public static ResponsePersonalInformation Map(this Kai002n0Response model)
        {
            if (model == null)
                return null;

            return new ResponsePersonalInformation
            {
                lastName = model.OvdShmMishpaha.StringTrim(),
                firstName = model.OvdShmPrati.StringTrim(),
                FathersName = model.OvdShmAv.StringTrim(),
                previousLastName = model.OvdShmMishpahaKodem.StringTrim(),
                previousFirstName = model.OvdShmPratiKodem.StringTrim(),
                city = model.OvdKtvShemYeshuv.StringTrim(),
                street = model.OvdKtvShemRehuv.StringTrim(),
                houseNumber = model.OvdKtvMisparBait,
                entranceNumber = model.OvdKtvKnisa.StringTrim(),
                apartmentNumber = model.OvdKtvDira,
                zip = model.OvdKtvMikud,
                poBox = model.OvdKtvTaDoar,
                phoneNumberPrefix = model.OvdKtvKidometMisparBait,
                extraNumberPrefix = model.OvdKtvKidometMisparNosaf,
                birthDate = model.OvdMozTLeda.ToDate(),
                countryOfBirth = model.OvdMozEretzLeda.StringTrim(),
                immigrationDate = model.OvdMozTAlia.ToDate(),
                gender = model.OvdMozMin.StringTrim(),
                maritalStatus = model.OvdMamMazavMishpaht.StringTrim(),
                statusDate = model.OvdMamTMazav.ToDate(),
                spouseName = model.OvdZugShem.StringTrim(),
                spouseIdNUmber = model.OvdZugMisparZehut,
                spousBirthDate = model.OvdZugTLeda.ToDate(),
                spousJobStatus = model.OvdZugOvedLoOved.StringTrim(),
                spousJobName = model.OvdZugMakomAvoda.StringTrim(),
                spousPhoneNumberPrefix = model.OvdZugTelKidometMispar,
                spousPhoneNumberExtension = model.OvdZugTelShlucha,
                count11 = model.Count11,
                count17 = model.Count17,
                children = model.Kai002n0ResponseOvdIld != null ? model.Kai002n0ResponseOvdIld.ToList().Select(c => c.Map()).ToList() : null,
                courses = model.Kai002n0ResponseOvdHas != null ? model.Kai002n0ResponseOvdHas.ToList().Select(c => c.Map()).ToList() : null,
            };
        }

        public static ResponseChild Map(this Kai002n0ResponseOvdIld model)
        {
            if (model == null)
                return null;

            return new ResponseChild
            {
                birthDate = model.OvdIldTLeda.ToDate(),
                firstName = model.OvdIldShem.StringTrim(),
                gender = model.OvdIldMin.StringTrim(),
                homeLiving = model.OvdIldGarBabait.StringTrim(),
                idNumber = model.OvdIldMisparZehut
            };
        }

        public static ResponseEducation Map(this Kai002n0ResponseOvdHas model)
        {
            if (model == null)
                return null;

            return new ResponseEducation
            {
                certificateName = model.OvdHasRama.StringTrim(),
                institutionName = model.OvdHasShemMosad.StringTrim(),
                studySubject1 = model.OvdHasMegama1.StringTrim(),
                studySubject2 = model.OvdHasMegama2.StringTrim(),
                educationYears = model.OvdHasSnotLimud,
                degree = model.OvdHasToar.StringTrim(),
                degreeDate = model.OvdHasTToar.ToDate()
            };
        }
    }
}