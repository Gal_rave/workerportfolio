﻿using System;
using System.Globalization;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;

using WebServiceProvider.PortalWS;
using WebServiceProvider.DataMappers;
using DataAccess.Extensions;
using DataAccess.Models;
using Logger;

namespace WebServiceProvider
{
    public static class WSProvider
    {
        private static int sender;
        private static int recipient;
        private static string userId;
        private static string password;
        private static string version;
        private static bool enableTrace;
        private static bool isInitialized = false;
        private static string url;
        private static TikOvedWSService WS;

        private static void initializ()
        {
            if (isInitialized)
                return;

            sender = ConfigurationManager.AppSettings["WebServiceProvider_sender"].ToInt();
            recipient = ConfigurationManager.AppSettings["WebServiceProvider_recipient"].ToInt();
            userId = ConfigurationManager.AppSettings["WebServiceProvider_userName"];
            password = ConfigurationManager.AppSettings["WebServiceProvider_password"];
            version = ConfigurationManager.AppSettings["WebServiceProvider_version"];
            enableTrace = ConfigurationManager.AppSettings["WebServiceProvider_enableTrace"].ToBool();
            url = ConfigurationManager.AppSettings["WebServiceProvider"];
            WS = new TikOvedWSService();
            WS.Url = url;

            isInitialized = true;
        }
        private static SystemHeader setHeader(int customerId)
        {
            return new SystemHeader
            {
                Customer = customerId,
                Sender = sender,
                Recipient = recipient,
                UserId = userId,
                UserPass = password,
                Version = version
            };
        }

        /// <summary>
        /// נתוני שכר - גמל והשתלמות
        /// </summary>
        /// <param name="idNumber"></param>
        /// <param name="customerId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static List<ResponseBenefitFund> GetFunds(int idNumber, int customerId, int year)
        {
            initializ();

            Kai006n1Response response = null;
            Kai006n1Request request = new Kai006n1Request();
            request.Dbid = 0;
            request.Debug = 0;
            request.Oved = idNumber;
            request.RashutHanHesbon = customerId;
            request.Year = year.ToShort();

            try
            {
                response = WS.getFunds_Data(setHeader(customerId), request);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
            List<ResponseBenefitFund> benefits = new List<ResponseBenefitFund>();
            for (int i = 0; i < response.Count; i++)
            {
                benefits.Add(response.Kai006n1ResponseNetuneFunds[i].Map());
            }

            return benefits;
        }
        public static List<WSTableModel> GetFundsTable(int idNumber, int customerId, int year)
        {
            initializ();

            Kai006n1Response response = null;
            Kai006n1Request request = new Kai006n1Request();
            request.Dbid = 0;
            request.Debug = 0;
            request.Oved = idNumber;
            request.RashutHanHesbon = customerId;
            request.Year = year.ToShort();

            try
            {
                response = WS.getFunds_Data(setHeader(customerId), request);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }

            List<WSTableModel> _tables = new List<WSTableModel>();
            WSTableModel _table;

            List<ResponseBenefitFund> benefits = new List<ResponseBenefitFund>();
            for (int i = 0; i < response.Count; i++)
            {
                benefits.Add(response.Kai006n1ResponseNetuneFunds[i].Map());
            }

            foreach (ICollection<ResponseBenefitFund> benefitsGroup in benefits.GroupBy(b => b.fundNumber))
            {
                _table = new WSTableModel();
                _table.Titles.Add(new Title(0, benefitsGroup.First().fundName));
                List<ResponseBenefitFund> tsumfundtotal = sumFundTotal(benefitsGroup);

                List<int> months = tsumfundtotal.Select(sf => sf.month).ToList().Distinct().ToList();
                List<int> allMonths = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
                List<int> missingMonths = allMonths.Where(x => !months.Contains(x)).ToList();

                if (missingMonths.Count >= 12)
                    continue;

                /////add missing moths
                if (missingMonths.Count > 0 && DateTime.Now.Year > year) tsumfundtotal.AddRange(addMonthsToFundsTable(missingMonths));

                tsumfundtotal = tsumfundtotal.OrderBy(f => f.order).ToList();
                _table.AddDataRow<ResponseBenefitFund>(tsumfundtotal);
                _tables.Add(_table);
            }

            return _tables;
        }

        /// <summary>
        /// קורסים והשתלמויות
        /// </summary>
        /// <param name="idNumber"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public static Kai002n1Response GetKursimIshtalmuyot(int idNumber, int customerId)
        {
            initializ();

            Kai002n1Response response = null;
            Kai002n1Request request = new Kai002n1Request();
            request.Dbid = 0;
            request.Debug = 0;
            request.Oved = idNumber;
            request.RashutHanHesbon = customerId;

            try
            {
                response = WS.getKursimIshtalmuyot(setHeader(customerId), request);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
            return response;
        }

        /// <summary>
        /// נתוני היעדרויות
        /// </summary>
        /// <param name="idNumber"></param>
        /// <param name="customerId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static object GetNetuneHeadrut(int idNumber, int customerId, int year)
        {
            initializ();

            Kai005n0Response response = null;
            Kai005n0Request request = new Kai005n0Request();
            request.Dbid = 0;
            request.Debug = 0;
            request.Month = 1;
            request.Oved = idNumber;
            request.RashutHanHesbon = customerId;
            request.Year = year.ToShort();

            try
            {
                response = WS.getNetuneHeadrut(setHeader(customerId), request);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }

            List<ResponseWorkAbsence> absence = new List<ResponseWorkAbsence>();
            for (int i = 0; i < response.Count; i++)
            {
                absence.Add(response.Kai005n0ResponseNetuneHeadrut[i].Map());
            }

            return absence;
            //return response;
        }
        public static List<WSTableModel> GetEmployeeAttendanceFiguresData(int idNumber, int customerId, int year)
        {
            initializ();

            List<ResponseWorkAbsence> absence = new List<ResponseWorkAbsence>();
            List<ResponseWorkAbsence>[] _absence = new List<ResponseWorkAbsence>[13];
            List<WSTableModel> _tables = new List<WSTableModel>();
            WSTableModel _table;
            int maxColumn = 0;

            Kai005n0Response response = null;
            Kai005n0Request request = new Kai005n0Request();
            request.Dbid = 0;
            request.Debug = 0;
            request.Month = 1;
            request.Oved = idNumber;
            request.RashutHanHesbon = customerId;
            request.Year = year.ToShort();

            try
            {
                response = WS.getNetuneHeadrut(setHeader(customerId), request);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }

            for (int i = 0; i < response.Count; i++)
            {
                absence.Add(response.Kai005n0ResponseNetuneHeadrut[i].Map());
            }
            ///split list by tabTarget (data type)
            foreach (ICollection<ResponseWorkAbsence> tabCollection in absence.GroupBy(a => a.tabTarget))
            {
                _table = new WSTableModel();
                _table.Titles = tabCollection.ListTitles(true);

                maxColumn = tabCollection.Select(wa => wa.columnNumber).ToList().Distinct<int>().ToList().Count;

                ///insert existind data
                foreach (ICollection<ResponseWorkAbsence> monthCollection in tabCollection.GroupBy(a => a.month))
                {
                    _absence[monthCollection.First().month] = addMonthColumnToExistindData(monthCollection);
                }

                List<int> months = tabCollection.Select(sf => sf.month).ToList().Distinct().ToList();
                List<int> allMonths = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
                List<int> missingMonths = allMonths.Where(x => !months.Contains(x)).ToList();

                if (missingMonths.Count >= 12)
                    continue;

                ///add missing month
                if (missingMonths.Count > 0 && DateTime.Now.Year > year)
                    foreach (int month in missingMonths)
                    {
                        _absence[month] = addMissingMonthColumns(maxColumn, month).OrderBy(a => a.month).ToList();
                    }


                foreach (var item in _absence.Where(a => a != null))
                {
                    _table.AddDataRow<ResponseWorkAbsence>(item);
                }

                _tables.Add(_table);
            }

            return _tables;
        }
        public static WSTableModel GetNetuneHeadrutTable(int idNumber, int customerId, int year)
        {
            initializ();

            Kai005n0Response response = null;
            Kai005n0Request request = new Kai005n0Request();
            request.Dbid = 0;
            request.Debug = 0;
            request.Month = 1;
            request.Oved = idNumber;
            request.RashutHanHesbon = customerId;
            request.Year = year.ToShort();

            try
            {
                response = WS.getNetuneHeadrut(setHeader(customerId), request);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }

            WSTableModel _table = new WSTableModel();
            List<ResponseWorkAbsence> absence = new List<ResponseWorkAbsence>();
            for (int i = 0; i < response.Count; i++)
            {
                absence.Add(response.Kai005n0ResponseNetuneHeadrut[i].Map());
            }

            _table.Titles = absence.Select(sf => new Title(sf.columnNumber, sf.columnTitle)).Distinct<Title>().ToList();

            foreach (ICollection<ResponseWorkAbsence> figers in absence.GroupBy(f => f.month))
            {
                _table.AddDataRow<ResponseWorkAbsence>(figers);
            }

            return _table;
        }

        /// <summary>
        /// נתוני שכר - כללי
        /// </summary>
        /// <param name="idNumber"></param>
        /// <param name="customerId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static List<ResponseSalaryFigures> GetNetuneSachar(int idNumber, int customerId, int year)
        {
            initializ();

            Kai003n0Response response = null;
            Kai003n0Request request = new Kai003n0Request();
            request.Dbid = 0;
            request.Debug = 0;
            request.Oved = idNumber;
            request.RashutHanHesbon = customerId;
            request.Year = year.ToShort(); ;

            try
            {
                response = WS.getNetuneSachar(setHeader(customerId), request);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }

            WSTableModel _table = new WSTableModel();
            List<ResponseSalaryFigures> salaryfigures = new List<ResponseSalaryFigures>();
            for (int i = 0; i < response.Count; i++)
            {
                salaryfigures.Add(response.Kai003n0ResponseNetuneSachar[i].Map(CultureInfo.CurrentCulture));
            }

            return salaryfigures;
        }
        public static WSTableModel GetNetuneSacharTable(int idNumber, int customerId, int year)
        {
            initializ();

            WSTableModel _table = new WSTableModel();

            Kai003n0Response response = null;
            Kai003n0Request request = new Kai003n0Request();
            request.Dbid = 0;
            request.Debug = 0;
            request.Oved = idNumber;
            request.RashutHanHesbon = customerId;
            request.Year = year.ToShort();

            try
            {
                response = WS.getNetuneSachar(setHeader(customerId), request);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }

            if (response.Count <= 0)
                return _table;

            List<ResponseSalaryFigures> salaryfigures = new List<ResponseSalaryFigures>();
            for (int i = 0; i < response.Count; i++)
            {
                salaryfigures.Add(response.Kai003n0ResponseNetuneSachar[i].Map(CultureInfo.CurrentCulture));
            }
            List<ResponseSalaryFigures> s;

            List<int> months = salaryfigures.Select(sf => sf.month).ToList().Distinct().ToList();
            List<int> allMonths = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            List<int> missingMonths = allMonths.Where(x => !months.Contains(x)).ToList();

            if (missingMonths.Count >= 12)
                return _table;

            if (DateTime.Now.Year > year)
                foreach (int i in missingMonths)
                {
                    s = new List<ResponseSalaryFigures>() { new ResponseSalaryFigures(i, 1), new ResponseSalaryFigures(i, 2), new ResponseSalaryFigures(i, 3), new ResponseSalaryFigures(i, 4), new ResponseSalaryFigures(i, 5), new ResponseSalaryFigures(i, 6), new ResponseSalaryFigures(i, 7) };
                    salaryfigures.AddRange(s);
                }

            ///add month colmun to existing rows
            foreach (ICollection<ResponseSalaryFigures> figers in salaryfigures.GroupBy(f => f.month))
            {
                int m = figers.First().month;
                if (m > 0)
                    salaryfigures.Add(new ResponseSalaryFigures(m, 0, CultureInfo.CurrentCulture));
            }
            salaryfigures = salaryfigures.OrderBy(sf => sf.month).ToList();
            ///create existing rows
            foreach (ICollection<ResponseSalaryFigures> figers in salaryfigures.GroupBy(f => f.month))
            {
                if (figers.First().month > 0)
                    _table.AddDataRow<ResponseSalaryFigures>(figers);
            }
            ///add summary / total row
            s = new List<ResponseSalaryFigures>() {
                        new ResponseSalaryFigures(CultureInfo.CurrentCulture, month:13, columnnumber:0, columntitle:"סה\"כ", val:"סה\"כ"),
                        new ResponseSalaryFigures(CultureInfo.CurrentCulture,month:13, columntitle:"", columnnumber:1, val:0),
                        new ResponseSalaryFigures(CultureInfo.CurrentCulture,month:13, columntitle:"", columnnumber:2, val:0),
                        new ResponseSalaryFigures(CultureInfo.CurrentCulture,month:13, columntitle:"", columnnumber:3, val:0),
                        new ResponseSalaryFigures(CultureInfo.CurrentCulture,month:13, columntitle:"", columnnumber:4, val:0),
                        new ResponseSalaryFigures(CultureInfo.CurrentCulture,month:13, columntitle:"", columnnumber:5, val:0),
                        new ResponseSalaryFigures(CultureInfo.CurrentCulture,month:13, columntitle:"", columnnumber:6, val:0),
                        new ResponseSalaryFigures(CultureInfo.CurrentCulture,month:13, columntitle:"", columnnumber:7, val:0)
                    };
            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
            salaryfigures.ForEach(sf =>
            {
                if (sf.value.ToDouble() != 0)
                    s[sf.columnNumber].value = Math.Round((s[sf.columnNumber].value.ToDouble() + sf.value.ToDouble()), 2).ToString("N", nfi);
            });

            _table.AddDataRow<ResponseSalaryFigures>(s);

            _table.Titles = salaryfigures.ListTitles();

            return _table;
        }

        /// <summary>
        /// נתוני שכר - לפי סמלים
        /// </summary>
        /// <param name="idNumber"></param>
        /// <param name="customerId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static List<ResponseSalaryFigures> GetNetuneSacharNivharim(int idNumber, int customerId, int year)
        {
            initializ();

            Kai004n0Response response = null;
            Kai004n0Request request = new Kai004n0Request();
            request.Dbid = 0;
            request.Debug = 0;
            request.Oved = idNumber;
            request.RashutHanHesbon = customerId;
            request.Year = year.ToShort();

            try
            {
                response = WS.getNetuneSacharNivharim(setHeader(customerId), request);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }

            List<ResponseSalaryFigures> salaryfigures = new List<ResponseSalaryFigures>();
            for (int i = 0; i < response.Count; i++)
            {
                salaryfigures.Add(response.Kai004n0ResponseSacharSmalim[i].Map(CultureInfo.CurrentCulture));
            }

            return salaryfigures;
        }
        public static WSTableModel GetNetuneSacharNivharimTable(int idNumber, int customerId, int year)
        {
            initializ();

            WSTableModel _table = new WSTableModel();

            Kai004n0Response response = null;
            Kai004n0Request request = new Kai004n0Request();
            request.Dbid = 0;
            request.Debug = 0;
            request.Oved = idNumber;
            request.RashutHanHesbon = customerId;
            request.Year = year.ToShort();

            try
            {
                response = WS.getNetuneSacharNivharim(setHeader(customerId), request);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }

            if (response.Count <= 0)
                return _table;

            List<ResponseSalaryFigures> salaryfigures = new List<ResponseSalaryFigures>();
            for (int i = 0; i < response.Count; i++)
            {
                salaryfigures.Add(response.Kai004n0ResponseSacharSmalim[i].Map(CultureInfo.CurrentCulture));
            }

            ///fix columnnumbers 
            List<int> colns = salaryfigures.Select(sf => sf.columnNumber).Distinct().OrderBy(sf => sf).ToList();
            foreach (ResponseSalaryFigures sf in salaryfigures)
            {
                sf.columnNumber = colns.FindIndex(p => p == sf.columnNumber) + 1;
            }

            List<ResponseSalaryFigures> s;
            List<int> months = salaryfigures.Select(sf => sf.month).ToList().Distinct().ToList();
            List<int> allMonths = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            List<int> missingMonths = allMonths.Where(x => !months.Contains(x)).ToList();

            if (missingMonths.Count >= 12)
                return _table;
            ///add missing month in past data
            if (DateTime.Now.Year > year)
                foreach (int month in missingMonths)
                {
                    s = addMissingMonths(month, colns.Count, false);
                    salaryfigures.AddRange(s);
                }

            ///add month colmun to existing rows
            foreach (ICollection<ResponseSalaryFigures> figers in salaryfigures.GroupBy(f => f.month))
            {
                int m = figers.First().month;
                salaryfigures.Add(new ResponseSalaryFigures(m, 0, CultureInfo.CurrentCulture));
            }
            ///create existing rows
            foreach (ICollection<ResponseSalaryFigures> figers in salaryfigures.GroupBy(f => f.month))
            {
                _table.AddDataRow<ResponseSalaryFigures>(figers);
            }
            ///add summary / total row
            s = addMissingMonths(13, colns.Count, true);

            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
            salaryfigures.ForEach(sf =>
            {
                if (sf.value.ToDouble() != 0)
                    s[sf.columnNumber].value = Math.Round((s[sf.columnNumber].value.ToDouble() + sf.value.ToDouble()), 2).ToString("N", nfi);
            });

            _table.AddDataRow<ResponseSalaryFigures>(s);

            _table.Titles = salaryfigures.ListTitles();

            return _table;
        }

        /// <summary>
        /// פרטים אישיים
        /// </summary>
        /// <param name="idNumber"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public static ResponsePersonalInformation GetPratimIshim(int idNumber, int customerId)
        {
            initializ();

            Kai002n0Response response = null;
            Kai002n0Request request6 = new Kai002n0Request();
            request6.Dbid = 0;
            request6.Debug = 0;
            request6.Oved = idNumber;
            request6.RashutHanHesbon = customerId;

            try
            {
                response = WS.getPratimIshim(setHeader(customerId), request6);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }

            return response.Map();
        }

        #region private
        private static List<ResponseSalaryFigures> addMissingMonths(int month, int columnnumber, bool isSummary)
        {
            List<ResponseSalaryFigures> list = new List<ResponseSalaryFigures>();
            if (!isSummary)
            {
                for (int i = 1; i <= columnnumber; i++)
                {
                    list.Add(new ResponseSalaryFigures(month, i));
                }
            }
            else
            {
                list.Add(new ResponseSalaryFigures(CultureInfo.CurrentCulture, month: 13, columnnumber: 0, columntitle: "סה\"כ", val: "סה\"כ"));
                for (int i = 1; i <= columnnumber; i++)
                {
                    list.Add(new ResponseSalaryFigures(CultureInfo.CurrentCulture, month: 13, columntitle: "", columnnumber: i, val: 0));
                }
            }

            return list;
        }
        private static List<ResponseBenefitFund> sumFundTotal(ICollection<ResponseBenefitFund> fundGroup)
        {
            List<ResponseBenefitFund> list = fundGroup.ToList();
            ResponseBenefitFund s = new ResponseBenefitFund(CultureInfo.CurrentCulture, 0, "סה\"כ", 0, 0, 0, 0/*month*/, 0, 0, 0, 0, 0, 0, 0, 15);
            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
            foreach (ResponseBenefitFund fund in fundGroup)
            {
                s.deductionAmount = s.deductionAmount.ToDouble() + fund.deductionAmount.ToDouble();
                s.deductionPercentage += fund.deductionPercentage;
                s.disabilityInsuranceAmount = s.disabilityInsuranceAmount.ToDouble() + fund.disabilityInsuranceAmount.ToDouble();
                s.disabilityInsurancePercentage += fund.disabilityInsurancePercentage;
                s.fundBrutoGemel = s.fundBrutoGemel.ToDouble() + fund.fundBrutoGemel.ToDouble();
                s.provisionsAmount = s.provisionsAmount.ToDouble() + fund.provisionsAmount.ToDouble();
                s.provisionsPercentage += fund.provisionsPercentage;
                s.severanceAmount = s.severanceAmount.ToDouble() + fund.severanceAmount.ToDouble();
                s.severancePercentage += fund.severancePercentage;
                s.monthTotal = s.monthTotal.ToDouble() + fund.deductionAmount.ToDouble() + fund.disabilityInsuranceAmount.ToDouble() + fund.provisionsAmount.ToDouble() + fund.severanceAmount.ToDouble();
                s.monthName = "סה\"כ";
            }

            s.deductionAmount = Math.Round(s.deductionAmount.ToDouble(), 2).ToString("N", nfi);
            s.deductionPercentage = Math.Round(s.deductionPercentage, 2);
            s.disabilityInsuranceAmount = Math.Round(s.disabilityInsuranceAmount.ToDouble(), 2).ToString("N", nfi);
            s.disabilityInsurancePercentage = Math.Round(s.disabilityInsurancePercentage, 2);
            s.fundBrutoGemel = Math.Round(s.fundBrutoGemel.ToDouble(), 2).ToString("N", nfi);
            s.provisionsAmount = Math.Round(s.provisionsAmount.ToDouble(), 2).ToString("N", nfi);
            s.provisionsPercentage = Math.Round(s.provisionsPercentage, 2);
            s.severanceAmount = Math.Round(s.severanceAmount.ToDouble(), 2).ToString("N", nfi);
            s.severancePercentage = Math.Round(s.severancePercentage, 2);
            s.monthTotal = Math.Round(s.monthTotal.ToDouble(), 2).ToString("N", nfi);

            list.Add(s);
            return list;
        }
        private static List<ResponseWorkAbsence> addMissingMonthColumns(int maxColumn, int month)
        {
            List<ResponseWorkAbsence> cl = new List<ResponseWorkAbsence>();
            for (int i = 0; i <= maxColumn; i++)
            {
                if (i == 0)
                    cl.Add(new ResponseWorkAbsence(month, i, CultureInfo.CurrentCulture));
                else
                    cl.Add(new ResponseWorkAbsence(month, i));
            }
            return cl.OrderBy(a => a.columnNumber).ToList();
        }
        private static List<ResponseWorkAbsence> addMonthColumnToExistindData(ICollection<ResponseWorkAbsence> monthCollection)
        {
            List<ResponseWorkAbsence> ral = monthCollection.ToList();
            ral.Add(new ResponseWorkAbsence(ral.First().month, 0, CultureInfo.CurrentCulture));
            return ral.OrderBy(a => a.columnNumber).ToList();
        }
        private static List<ResponseBenefitFund> addMonthsToFundsTable(List<int> missingMonths)
        {
            List<ResponseBenefitFund> l = new List<ResponseBenefitFund>();
            ResponseBenefitFund m;
            foreach (int month in missingMonths)
            {
                m = new ResponseBenefitFund(CultureInfo.CurrentCulture, month);
                l.Add(m);
            }

            return l;
        }
        #endregion
    }
}
