﻿using System;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Web;
using System.Collections.Generic;

using DataAccess.Extensions;
using DataAccess.BLLs;
using DataAccess.Models;
using Logger;
using WebServiceProvider.DataMappers;
using DataAccess.Mappers;
using DataAccess.Enums;

namespace WebServiceProvider
{
    public static class FormsProvider
    {
        private static string url;
        private static string WSurl;
        private static string FOurl;
        private static string SynchronizeUrl;
        private static string ReDourl;
        private static bool isInitialized = false;

        private static void initializ()
        {
            if (isInitialized)
                return;
            url = ConfigurationManager.AppSettings["ArchiveServiceURL"];
            WSurl = ConfigurationManager.AppSettings["SystemUsersProvider"];
            FOurl = ConfigurationManager.AppSettings["Form101Provider"];
            SynchronizeUrl = ConfigurationManager.AppSettings["SynchronizeUEPC"];
            ReDourl = HttpContext.Current.Request.Url.Host == "localhost" ? "http://localhost:2422" : "http://10.238.237.19";

            isInitialized = false;
        }
        /// <summary>
        /// create url to pdf resource
        /// </summary>
        /// <param name="idNumber"></param>
        /// <param name="calculatedIdNumber"></param>
        /// <param name="customerId"></param>
        /// <param name="requestTime"></param>
        /// <param name="requestType"></param>
        /// <returns></returns>
        private static string createUrl(string idNumber, int calculatedIdNumber, int customerId, DateTime requestTime, TofesTypeEnum requestType, int? _mifal = null)
        {
            int mifal = _mifal.HasValue && _mifal.Value > 0? _mifal.Value : MegaWSProvider.GetMifal(calculatedIdNumber, customerId, requestTime);
            string clientUrl = string.Format("{0}{1}/", url, mifal.GetThreeDigitsMifal());
            switch (requestType)
            {
                case TofesTypeEnum.T101:
                case TofesTypeEnum.T106:
                    clientUrl = string.Format("{0}{1}/{2}/{3}.pdf", clientUrl, ((TofesTypeEnum)requestType).ToString(), requestTime.Year, idNumber.ToInt());
                    break;
                case TofesTypeEnum.TLUSH:
                    clientUrl = string.Format("{0}{1}/{2}{3}/{4}.pdf", clientUrl, ((TofesTypeEnum)requestType).ToString(), requestTime.Year, (requestTime.Month > 9 ? requestTime.Month.ToString() : ("0" + requestTime.Month.ToString())), idNumber.ToInt());
                    break;
            }
            Log.FormLog(clientUrl, "createUrl");

            return clientUrl;
        }

        public static UserImageModel Get106(string idNumber, int calculatedIdNumber, int customerId, DateTime requestTime)
        {
            initializ();
            UserBll bll = new UserBll();
            UserImageModel image;
            try
            {
                image = bll.GetUserImage(idNumber, (TofesTypeEnum.T106).ToString(), requestTime, customerId);
                if (image != null)
                    return image;
            }
            catch (Exception ex)
            {
                ex.LogError();
            }

            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] pdf = client.DownloadData(createUrl(idNumber, calculatedIdNumber, customerId, requestTime, TofesTypeEnum.T106));
                    image = new UserImageModel
                    {
                        fileDate = requestTime,
                        type = (TofesTypeEnum.T106).ToString(),
                        fileBlob = pdf,
                        fileString = pdf.ByteArrayToString(),
                        municipalityId = customerId,
                    };
                    image = bll.CreateUserImage(image, idNumber);
                    return image;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static UserImageModel GetPaycheck(string idNumber, int customerId, DateTime requestTime, int mifal)
        {
            initializ();
            UserBll bll = new UserBll();
            UserImageModel image;
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient(12000))
                {
                    byte[] pdf = client.DownloadData(createUrl(idNumber, 0, customerId, requestTime, TofesTypeEnum.TLUSH, mifal));
                    image = new UserImageModel
                    {
                        fileDate = requestTime,
                        type = (TofesTypeEnum.TLUSH).ToString(),
                        fileBlob = pdf,
                        fileString = pdf.ByteArrayToString(),
                        municipalityId = customerId,
                    };
                    image = bll.CreateUserImage(image, idNumber);
                    return image;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static UserImageModel GetPaycheck(string idNumber, int calculatedIdNumber, int customerId, DateTime requestTime)
        {
            initializ();

            UserBll bll = new UserBll();
            UserImageModel image = bll.GetUserImage(idNumber, (TofesTypeEnum.TLUSH).ToString(), requestTime, customerId);
            if (image != null)
            {
                return image;
            }

            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient(12000))
                {
                    byte[] pdf = client.DownloadData(createUrl(idNumber, calculatedIdNumber, customerId, requestTime, TofesTypeEnum.TLUSH));
                    image = new UserImageModel
                    {
                        fileDate = requestTime,
                        type = (TofesTypeEnum.TLUSH).ToString(),
                        fileBlob = pdf,
                        fileString = pdf.ByteArrayToString(),
                        municipalityId = customerId,
                    };
                    image = bll.CreateUserImage(image, idNumber);
                    return image;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static UserImageModel Collect101Form(string idNumber, int calculatedIdNumber, int customerId, DateTime requestTime)
        {
            initializ();
            try
            {
                UserImageModel image;

                using (TimeoutWebClient client = new TimeoutWebClient(12000))
                {
                    byte[] pdf = client.DownloadData(createUrl(idNumber, calculatedIdNumber, customerId, requestTime, TofesTypeEnum.T101));
                    image = new UserImageModel
                    {
                        fileDate = requestTime,
                        type = (TofesTypeEnum.T101).ToString(),
                        fileBlob = pdf,
                        fileString = pdf.ByteArrayToString(),
                        municipalityId = customerId,
                    };
                    return image;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static MunicipalityContactModel GetMunicipalityContact(int idNumber, int municipalityId)
        {
            initializ();
            int Mifal = MegaWSProvider.GetMifal(idNumber, municipalityId, DateTime.Now);
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}{2}", WSurl, "GetMunicipalityContact?mifal=", Mifal));
                    var str = Encoding.UTF8.GetString(contact);
                    MunicipalityContactExtension municipalitycontact = str.ToClass<MunicipalityContactExtension>();
                    return municipalitycontact.Map(Mifal);
                }
            }
            catch (Exception ex)
            {
                return ex.LogError(new MunicipalityContactModel());
            }
        }
        public static List<MUNICIPALITy> UpdateMunicipalitySchema()
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}", WSurl, "GetAllSchemas"));
                    var str = Encoding.UTF8.GetString(contact);
                    List<MunicipalitySchemaExtension> municipalitycontact = str.ToClass<List<MunicipalitySchemaExtension>>();

                    return municipalitycontact.Distinct().Select(m => m.Map()).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static List<MunicipalitySchemaExtension> GetMunicipalitySchema()
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}", WSurl, "GetAllSchemas"));
                    var str = Encoding.UTF8.GetString(contact);

                    return str.ToClass<List<MunicipalitySchemaExtension>>();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static bool PostUserDataToOperationalSystem(UPDATEUSERMUNICIPALITYCONTACT uumc)
        {
            initializ();
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}", FOurl, "DataMigration/UpdateUserContactInfo"));
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 180000;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    try
                    {
                        string json = uumc.Map();
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    catch (Exception Iex)
                    {
                        throw Iex.LogError();
                    }
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    return result.ToBool();
                }
            }
            catch (WebException wex)
            {
                var pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();
                pageContent.LogError();
                throw wex.LogError();
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static bool PostTofes101ToWs(TaxFromExtensionModel form101, bool isReDoSEnd = false, bool isLast = false)
        {
            initializ();
            try
            {
                Tt_Full101FromModel fullForm = form101.MapFullTofes101();
                Log.FormLog(fullForm.ToJSON());

                string _url = string.Format("{0}{1}{2}{3}", FOurl, "Form101", (isReDoSEnd ? "/PostReDo" : ""), (isReDoSEnd && isLast ? "?isLast=true" : ""));
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(_url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 180000;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    try
                    {
                        string json = fullForm.ToJSON();
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    catch (Exception Iex)
                    {
                        throw Iex.LogError();
                    }
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Log.FormLog(result, "PostTofes101ToWs END");
                }
            }
            catch (WebException wex)
            {
                var pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();
                pageContent.LogError();
                throw wex.LogError();
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
            return true;
        }
        public static bool LocalPutServis(TaxFromExtensionModel form101, bool isLast = false)
        {
            initializ();

            string url = string.Format("{0}{1}{2}", ReDourl, "/api/FilesUploades/LocalSendFormToWS", (isLast ? "?inError=false&isLast=true" : ""));
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "PUT";
                httpWebRequest.Timeout = 180000;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    try
                    {
                        string json = form101.ToJSON();
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    catch (Exception Iex)
                    {
                        throw Iex.LogError();
                    }
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    return true;
                }
            }
            catch (WebException wex)
            {
                var pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();
                pageContent.LogError();
                throw wex.LogError();
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static string SynchronizeUPCBMFromSite(PayCheckUsersByMailModel model)
        {
            initializ();
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}", SynchronizeUrl, "SynchronizeUEPC"));
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 180000;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    try
                    {
                        string json = model.MapTlush().ToJSON();
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    catch (Exception Iex)
                    {
                        throw Iex.LogError();
                    }
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    return result;
                }
            }
            catch (WebException wex)
            {
                var pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();
                pageContent.LogError();
                throw wex.LogError();
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static List<Tlush_MailModel> GetUsersForEmailPayCheckFromWS(int municipality)
        {
            initializ();
            try
            {
                Log.PcuLog(new { municipality = municipality }, "GetUsersForEmailPayCheckFromWS");
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] content = client.DownloadData(string.Format("{0}{1}{2}", SynchronizeUrl, "UpdateEPCUFromWS?municipality=", municipality));

                    var str = Encoding.UTF8.GetString(content);
                    List<Tlush_MailModel> users = str.ToClass<List<Tlush_MailModel>>();
                    return users;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static MunicipalityUpcbmModel CheckRashutLastPayCheckMonth(int municipalityId)
        {
            initializ();
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] contact = client.DownloadData(string.Format("{0}{1}{2}", WSurl, "GetLastPayCheckMonth?municipalityId=", municipalityId));
                    var str = Encoding.UTF8.GetString(contact);
                    MunicipalityUpcbmModel municipalityUpcbmModel = str.ToClass<MunicipalityUpcbmModel>();
                    return municipalityUpcbmModel;
                }
            }
            catch (Exception ex)
            {
                return ex.LogError(new MunicipalityUpcbmModel());
            }
        }
        public static bool GetRetirementStatus(WSPostModel model)
        {
            initializ();

            try
            {
                string _url = string.Format("{0}{1}", WSurl, "GetRetirementStatus");
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(_url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 180000;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    try
                    {
                        string json = model.ToJSON();
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    catch (Exception Iex)
                    {
                        return Iex.LogError(false);
                    }
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    WsReturnData returnData = result.ToClass<WsReturnData>();
                    Log.FormLog(result, "GetRetirementStatus END");

                    return returnData.rowIndex > 0 && !string.IsNullOrWhiteSpace(returnData.OvedId) && string.Compare(model.calculatedIdNumber, returnData.OvedId, true) == 0;
                }
            }
            catch (WebException wex)
            {
                var pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();
                pageContent.LogError();
                return wex.LogError(false);
            }
            catch (Exception ex)
            {
                return ex.LogError(false);
            }
        }
        public static List<TERMINATION> GetTerminationCauseList()
        {
            initializ();
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}", FOurl, "DataMigration/TerminationCause"));
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 180000;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    try
                    {
                        string json = new { getData = true}.ToJSON();
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    catch (Exception Iex)
                    {
                        throw Iex.LogError();
                    }
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    string result = streamReader.ReadToEnd();
                    List<TERMINATION> terminations = result.ToClass<List<TERMINATION>>();
                    return terminations;
                }
            }
            catch (WebException wex)
            {
                var pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();
                pageContent.LogError();
                throw wex.LogError();
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static List<ManagerModel> GetManagersContacts(int municipalityId)
        {
            initializ();            
            try
            {
                using (TimeoutWebClient client = new TimeoutWebClient { Timeout = 500000 })
                {
                    byte[] content = client.DownloadData(string.Format("{0}{1}{2}", FOurl, "DataMigration/GetManagersContact?municipality=", municipalityId));

                    var str = Encoding.UTF8.GetString(content);
                    List<ManagerModel> managers = str.ToClass<List<ManagerModel>>();
                    return managers;
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
                return null;
            }
        }
    }
}
