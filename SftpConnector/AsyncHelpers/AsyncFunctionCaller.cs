﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using System.Linq;

using Logger;
using System.Web;

namespace AsyncHelpers
{
    public static class AsyncFunctionCaller
    {
        private static int concurrencyLevel;
        private static LimitedConcurrencyLevelTaskScheduler lcts;
        private static List<Task> tasks;
        private static bool isInitialized = false;
        private static int timeout;
        /// <param name="level">maximum number of working tasks simultaneously</param>
        /// <param name="DoubleQuantity">double the number of working tasks simultaneously</param>
        /// <param name="timeout">maximum task working time of 12m befor canceletion</param>
        public static void InIt(int? level = null, bool DoubleQuantity = false)
        {
            if (isInitialized) return;
            concurrencyLevel = level.HasValue ? level.Value : int.Parse(ConfigurationManager.AppSettings["AsyncConcurrencyLevel"]);
            if (DoubleQuantity) concurrencyLevel = concurrencyLevel * 2;
            lcts = new LimitedConcurrencyLevelTaskScheduler(concurrencyLevel);
            tasks = new List<Task>();
            timeout = int.Parse(ConfigurationManager.AppSettings["AsyncTimeoutLevel"]);

            isInitialized = true;
        }
        public static void InIt(int timeout)
        {
            if (isInitialized) return;
            concurrencyLevel = int.Parse(ConfigurationManager.AppSettings["AsyncConcurrencyLevel"]);
            lcts = new LimitedConcurrencyLevelTaskScheduler(concurrencyLevel);
            tasks = new List<Task>();
            timeout = timeout > 0 ? timeout : int.Parse(ConfigurationManager.AppSettings["AsyncTimeoutLevel"]);

            isInitialized = true;
        }
        public static void UnInit()
        {
            isInitialized = false;
        }
        
        public static void RunAsyncAll()
        {
            InIt();
            tasks.Where(t => t.IsCanceled || t.IsCompleted).ToList().ForEach(t=> tasks.Remove(t));
            Task.WaitAll(tasks.ToArray());
        }
        public static void RunAsync(Action method, bool inError = false)
        {
            InIt();

            TaskFactory factory = createFactory();
            CancellationTokenSource cts = createCancellationToken();
            
            Task task = factory.StartNew(() =>
            {
                try
                {
                    using (cts.Token.Register(Thread.CurrentThread.Abort))
                    {
                        method();
                    }
                }
                catch (ThreadAbortException tae)
                {
                    tae.LogError();
                    cts.Cancel();
                }
                catch (OperationCanceledException oce)
                {
                    oce.LogError();
                    cts.Cancel();
                }
                catch (Exception ex)
                {
                    ex.LogError();
                    if (!inError) RunAsync(method, true);
                }
            }, cts.Token)
            .ContinueWith(antecendent => cts.Dispose());

            tasks.Add(task);
        }
        public static void RunAsync(HttpContext Context, Action method, bool inError = false)
        {
            InIt();

            TaskFactory factory = createFactory();
            CancellationTokenSource cts = createCancellationToken();

            Task task = factory.StartNew(() =>
            {
                HttpContext.Current = Context;
                try
                {
                    using (cts.Token.Register(Thread.CurrentThread.Abort))
                    {
                        method();
                    }
                }
                catch (ThreadAbortException tae)
                {
                    tae.LogError();
                    cts.Cancel();
                }
                catch (OperationCanceledException oce)
                {
                    oce.LogError();
                    cts.Cancel();
                }
                catch (Exception ex)
                {
                    ex.LogError();
                    if (!inError) RunAsync(method, true);
                }
            }, cts.Token)
            .ContinueWith(antecendent => cts.Dispose());

            tasks.Add(task);
        }
        public static void RunAsyncAwait(Action method)
        {
            InIt();

            TaskFactory factory = createFactory();
            CancellationTokenSource cts = createCancellationToken();

            Task task = Task.Factory.StartNew(() =>
            {
                try
                {
                    using (cts.Token.Register(Thread.CurrentThread.Abort))
                    {
                        method();
                    }
                }
                catch (ThreadAbortException tae)
                {
                    tae.LogError();
                    cts.Cancel();
                }
                catch (OperationCanceledException oce)
                {
                    oce.LogError();
                    cts.Cancel();
                }
                catch (Exception ex)
                {
                    ex.LogError();
                    cts.Cancel();
                }
            }, cts.Token)
            .ContinueWith(antecendent => cts.Dispose());

            tasks.Add(task);
            task.Wait();
        }
        public static void RunAsyncAwait(HttpContext Context, Action method)
        {
            InIt();

            TaskFactory factory = createFactory();
            CancellationTokenSource cts = createCancellationToken();

            Task task = Task.Factory.StartNew(() =>
            {
                HttpContext.Current = Context;
                try
                {
                    using (cts.Token.Register(Thread.CurrentThread.Abort))
                    {
                        method();
                    }
                }
                catch (ThreadAbortException tae)
                {
                    tae.LogError();
                    cts.Cancel();
                }
                catch (OperationCanceledException oce)
                {
                    oce.LogError();
                    cts.Cancel();
                }
                catch (Exception ex)
                {
                    ex.LogError();
                    cts.Cancel();
                }
            }, cts.Token)
            .ContinueWith(antecendent => cts.Dispose());

            tasks.Add(task);
            task.Wait();
        }

        private static TaskFactory createFactory()
        {
            return new TaskFactory(lcts);
        }
        private static CancellationTokenSource createCancellationToken()
        {
            return new CancellationTokenSource(timeout);
        }
    }
}
