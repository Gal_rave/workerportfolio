﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncHelpers
{
    sealed class AsyncLimiter
    {
        int running;
        readonly int maxrunning;
        readonly ConcurrentQueue<TaskCompletionSource<AsyncLimiterLock>> tasks;

        /// <summary>
        /// Instantiates a new AsyncLimiter
        /// </summary>
        /// <param name="max">The maximum number of locks to give at once.</param>
        public AsyncLimiter(int max)
        {
            maxrunning = max;
            tasks = new ConcurrentQueue<TaskCompletionSource<AsyncLimiterLock>>();
        }

        /// <summary>
        /// Runs a synchronous action through the limiter.
        /// </summary>
        /// <param name="action">The action to run.</param>
        /// <returns>A Task which completes after the action has ran.</returns>
        public Task Run(Action action)
        {
            return Lock().ContinueWith(disp =>
            {
                using (disp.Result)
                {
                    action();
                }
            });
        }

        /// <summary>
        /// Runs a function through the limiter.
        /// </summary>
        /// <typeparam name="T">The return type of the function.</typeparam>
        /// <param name="func">The function to run.</param>
        /// <returns>A Task which completes after the function has ran.</returns>
        public Task<T> Run<T>(Func<T> func)
        {
            return Lock().ContinueWith(disp =>
            {
                using (disp.Result)
                {
                    return func();
                }
            });
        }

        /// <summary>
        /// Runs a function through the limiter.
        /// </summary>
        /// <typeparam name="T">The return type of the function.</typeparam>
        /// <typeparam name="Arg1">The type of the function's first argument.</typeparam>
        /// <param name="func">The function to run.</param>
        /// <param name="arg1">A Task which completes after the function has ran.</param>
        /// <returns>A Task which completes after the function has ran.</returns>
        public Task<T> Run<T, Arg1>(Func<Arg1, T> func, Arg1 arg1)
        {
            return Lock().ContinueWith(disp =>
            {
                using (disp.Result)
                {
                    return func(arg1);
                }
            });
        }

        public Task<AsyncLimiterLock> Lock()
        {
            // see if we can lock immediately.

            if (TryReserve())
            {
                TaskCompletionSource<AsyncLimiterLock> imm = new TaskCompletionSource<AsyncLimiterLock>();
                imm.SetResult(new AsyncLimiterLock(this));
                return imm.Task;

                //In .NET 4.5:
                //return Task.FromResult((IDisposable)new AsyncLimiterLock(this));
            }

            // if no, enqueue a lock.

            TaskCompletionSource<AsyncLimiterLock> tcs = new TaskCompletionSource<AsyncLimiterLock>();
            tasks.Enqueue(tcs);

            // try one more time to start, in case the running count changed since we last tried.
            TryStart();

            return tcs.Task;
        }

        /// <summary>
        /// Attempts to complete a lock request.
        /// </summary>
        void TryStart()
        {
            // A loop here is required in case a lock request
            // is added after TryDequeue() but before Decrement().

            while (!tasks.IsEmpty && TryReserve())
            {
                TaskCompletionSource<AsyncLimiterLock> tcs;

                if (tasks.TryDequeue(out tcs))
                {
                    tcs.SetResult(new AsyncLimiterLock(this));
                    return;
                }
                else
                {
                    Decrement();
                }
            }
        }

        /// <summary>
        /// Attempts to add a new running lock.
        /// </summary>
        /// <returns>True if a lock could be added, otherwise false.</returns>
        bool TryReserve()
        {
            // A lock-free loop is used for better perf.
            // This is a thread-safe version of:
            //     if(running < maxrunning) { ++running; return true; }
            //     else return false;

            int oldrunning, newrunning = Thread.VolatileRead(ref running);
            while (newrunning < maxrunning)
            {
                oldrunning = newrunning;
                newrunning = Interlocked.CompareExchange(ref running, newrunning + 1, newrunning);

                if (newrunning == oldrunning)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Attempts to complete the next lock request when an old one is finished.
        /// </summary>
        /// <remarks>
        /// Called when an AsyncLimiterLock is disposed.
        /// </remarks>
        void Unlock()
        {
            // See if another task can be started immediately.

            TaskCompletionSource<AsyncLimiterLock> tcs;
            if (tasks.TryDequeue(out tcs))
            {
                tcs.SetResult(new AsyncLimiterLock(this));
            }
            else
            {
                Decrement();

                // Must call TryStart() here in case a lock request
                // is added after TryDequeue but before Decrement.
                TryStart();
            }
        }

        /// <summary>
        /// Decrements the number of running locks.
        /// </summary>
        void Decrement()
        {
            int newrunning = Interlocked.Decrement(ref running);
            Debug.Assert(newrunning >= 0);
        }

        public struct AsyncLimiterLock : IDisposable
        {
            AsyncLimiter limiter;

            public AsyncLimiterLock(AsyncLimiter limiter)
            {
                this.limiter = limiter;
            }

            public void Dispose()
            {
                if (limiter != null)
                {
                    limiter.Unlock();
                    limiter = null;
                    GC.SuppressFinalize(this);
                }
            }
        }
    }
}
