﻿using System;
using System.IO;
using System.Web;
using System.Configuration;
using System.IO.Compression;
using System.Collections.Generic;

using Logger;
using DataAccess.Models;
using DataAccess.Extensions;
using Renci.SshNet;

namespace SftpConnector
{
    public static class SftpTransfer
    {
        private static string SFTP_host;
        private static string SFTP_username;
        private static int SFTP_port;
        private static bool isInitialized = false;
        private static readonly string baseContent = "~/Content/PDFs";
        private static string filesBase;
        private static string SFTP_keyFile;
        public static void Initialize()
        {
            if (isInitialized)
                return;

            SFTP_host = ConfigurationManager.AppSettings["SFTP_host"];
            SFTP_username = ConfigurationManager.AppSettings["SFTP_username"];
            SFTP_port = ConfigurationManager.AppSettings["SFTP_port"].ToInt();

            string[] host = SFTP_host.Split('.');

            filesBase = Path.Combine(HttpContext.Current.Server.MapPath(baseContent), "PDFs");
            SFTP_keyFile = string.Format(ConfigurationManager.AppSettings["SFTP_keyFile"], host[host.Length - 1]);
            SFTP_keyFile = Path.Combine(HttpContext.Current.Server.MapPath("~/"), SFTP_keyFile);

            isInitialized = true;
        }
        public static bool UplodTransferZip(List<UserImageModel> caseAttachmentModels, string zipName, string directory = null)
        {
            try
            {
                Initialize();
                byte[] zip = createZip(caseAttachmentModels, zipName);
                postZip(zipName, zip, directory);
                return true;
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        
        #region private
        private static byte[] createZip(List<UserImageModel> caseAttachmentModels, string zipName = null)
        {
            try
            {                
                using (var compressedFileStream = new MemoryStream())
                {
                    //Create an archive and store the stream in memory.
                    using (var zipArchive = new ZipArchive(compressedFileStream, ZipArchiveMode.Update, false))
                    {
                        foreach (var caseAttachmentModel in caseAttachmentModels)
                        {
                            string zipDirectory = "";
                            if (!string.IsNullOrWhiteSpace(zipName))
                                zipDirectory = zipName.Replace("-", "/").Replace(Path.GetFileNameWithoutExtension(caseAttachmentModel.fileName) + ".zip", "");
                            //Create a zip entry for each attachment
                            var zipEntry = zipArchive.CreateEntry(zipDirectory + caseAttachmentModel.fileName);

                            //Get the stream of the attachment
                            using (var originalFileStream = new MemoryStream(caseAttachmentModel.fileBlob))
                            {
                                using (var zipEntryStream = zipEntry.Open())
                                {
                                    //Copy the attachment stream to the zip entry stream
                                    originalFileStream.CopyTo(zipEntryStream);
                                }
                            }
                        }

                    }
                    return compressedFileStream.ToArray();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        
        private static void postZip(string sourcefile, byte[] zip, string directory = null)
        {
             try
             {
                 using (SftpClient client = new SftpClient(createConnection()))
                 {
                     client.Connect();
                     if (client.IsConnected)
                     {
                        if (!string.IsNullOrWhiteSpace(directory))
                         {
                            if (!directory.StartsWith("/"))
                                directory = "/" + directory;
                            if (!directory.EndsWith("/"))
                                directory = directory + "/";
                            client.ChangeDirectory(directory);
                         }
                         
                         using (var ms = new MemoryStream(zip))
                         {
                             client.BufferSize = (uint)ms.Length; // bypass Payload error large files
                             client.UploadFile(ms, Path.GetFileName(sourcefile));
                         }
                     }
                 }
             }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        private static void deleteFile(string file)
        {
            if (string.IsNullOrWhiteSpace(file))
                return;

            try
            {
                file = Path.Combine(filesBase, file);
                if (File.Exists(file))
                    File.Delete(file);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        private static string pad(int num)
        {
            return num < 10 ? ("0" + num.ToString()) : num.ToString();
        }
        private static ConnectionInfo createConnection()
        {
            var keyFile = new PrivateKeyFile(SFTP_keyFile);
            var keyFiles = new[] { keyFile };

            var methods = new List<AuthenticationMethod>();
            methods.Add(new PrivateKeyAuthenticationMethod(SFTP_username, keyFiles));
            
            return new ConnectionInfo(SFTP_host, SFTP_port, SFTP_username, methods.ToArray());
        }
        #endregion
    }
}
