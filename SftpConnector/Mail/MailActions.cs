﻿using System;
using System.Web;
using System.Web.Routing;

using DataAccess.Models;
using DataAccess.BLLs;
using System.IO;
using Logger;

namespace Mail
{
    public static class MailActions
    {
        private static UserBll userBll;
        private static MailBll mailBll;

        public static PostNotificationMail SendNotificationMail(PostNotificationMail model)
        {
            try
            {
                userBll = new UserBll();
                mailBll = new MailBll();

                string template = string.Empty;

                RouteData routedata = HttpContext.Current.Request.RequestContext.RouteData;
                model.sender = userBll.GetUserByID(model.userIdNumber);
                model.recipient = mailBll.GetContactByID(model.toMailID);
                
                template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "HumanResourcesNotification", model);

                // send the mail
                MailSender.Send(model.recipient.email, template, false, model.subject, model.files, "הודעה ממערכת `תיק עובד`", model.sender.email);
                deleteAttachments(model.userIdNumber);
                
                return mailBll.SaveNotification(model);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static PostNotificationMail SendContactUsMail(PostNotificationMail model)
        {
            try
            {
                mailBll = new MailBll();
                string template = string.Empty;
                RouteData routedata = HttpContext.Current.Request.RequestContext.RouteData;
                model.recipientEmail = MailClient.SiteManagerMail;
                template = MailControllerExtension.RenderView(HttpContext.Current, routedata, "MailTemplate", "ContactUs", model);                
                
                MailSender.Send(recipients: model.recipientEmail, htmlBody: template, async: false, subject: model.subject, fielsPath: null, displayName: "צור קשר ממערכת `תיק עובד`", from: "", isreplyto: true, replytoMail: model.senderEmail);

                return mailBll.SaveNotification(model);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }

        private static void deleteAttachments(string userIdNumber)
        {
            if (string.IsNullOrWhiteSpace(userIdNumber))
                return;

            try
            {
                string directory = "~/Content/uploads/notification/" + userIdNumber;

                directory = HttpContext.Current.Server.MapPath(directory);
                if (Directory.Exists(directory))
                    Directory.Delete(directory, true);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
    }
}
