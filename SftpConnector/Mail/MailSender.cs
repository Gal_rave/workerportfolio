﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;

using Logger;

namespace Mail
{
    public static class MailSender
    {
        public static void Send(string recipients, string htmlBody)
        {
            Send("", "", parseMails(recipients), null, null, "", htmlBody, false);
        }
        public static void Send(string recipients, string subject, string htmlBody, bool async = false, bool isreplyto = false, string replytoMail = "")
        {
            Send("", "", parseMails(recipients), null, null, subject, htmlBody, async, null, isreplyto, replytoMail);
        }
        public static void Send(string recipients, string cc, string bcc, string htmlBody, bool async = false, bool isreplyto = false, string replytoMail = "")
        {
            Send("", "", parseMails(recipients), parseMails(cc), parseMails(bcc), "", htmlBody, async, null, isreplyto, replytoMail);
        }
        public static void Send(string recipients, string subject, string cc, string bcc, string htmlBody, bool async = false, bool isreplyto = false, string replytoMail = "")
        {
            Send("", "", parseMails(recipients), parseMails(cc), parseMails(bcc), subject, htmlBody, async, null, isreplyto, replytoMail);
        }
        public static void Send(string recipients, string htmlBody, bool async = false, string subject = "", string fielsPath = null, string displayName = "", string from = "", bool isreplyto = false, string replytoMail = "")
        {
            Send(from, displayName, parseMails(recipients), null, null, subject, htmlBody, async, fielsPath, isreplyto, replytoMail);
        }
        public static void Send(string recipients, string ccRecipients, string bccRecipients, string htmlBody, bool async = false, string subject = "", string fielsPath = null, string displayName = "", string from = "", bool isreplyto = false, string replytoMail = "")
        {
            Send(from, displayName, parseMails(recipients), (!string.IsNullOrWhiteSpace(ccRecipients) ? parseMails(ccRecipients) : null), (!string.IsNullOrWhiteSpace(bccRecipients) ? parseMails(bccRecipients) : null), subject, htmlBody, async, fielsPath, isreplyto, replytoMail);
        }
        /// <summary>
        /// main send message, will have override
        /// </summary>
        /// <param name="from"></param>
        /// <param name="displayName"></param>
        /// <param name="to"></param>
        /// <param name="cc"></param>
        /// <param name="bcc"></param>
        /// <param name="subject"></param>
        /// <param name="htmlBody"></param>
        /// <param name="async"></param>
        /// <param name="fielsPath"></param>
        public static void Send(string from, string displayName, MailAddressCollection to, MailAddressCollection cc, MailAddressCollection bcc, string subject, string htmlBody, bool async = false, string fielsPath = null, bool isreplyto = false, string replytoMail = "")
        {
            if (to.Count == 0)
            {
                return;
            }

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = SetUpFromMail(from, displayName);

            mailMessage.Subject = subject.Replace('\r', ' ').Replace('\n', ' ');
            mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            #region Addressees
            foreach (MailAddress mailAddress in to)
            {
                mailMessage.To.Add(mailAddress);
            }

            if (cc != null)
            {
                foreach (MailAddress mailAddress in cc)
                {
                    mailMessage.CC.Add(mailAddress);
                }
            }

            if (bcc != null)
            {
                foreach (MailAddress mailAddress in bcc)
                {
                    mailMessage.Bcc.Add(mailAddress);
                }
            }
            if (isreplyto && MailClient.IsReplyTo && !string.IsNullOrWhiteSpace(MailClient.ReplyToAddress))
            {
                foreach (MailAddress mailAddress in parseMails(MailClient.ReplyToAddress))
                {
                    mailMessage.ReplyToList.Add(mailAddress);
                }
            }
            if (isreplyto && !string.IsNullOrWhiteSpace(replytoMail))
            {
                mailMessage.From = SetUpFromMail("", displayName);
                mailMessage.ReplyToList.ToList().ForEach(a =>
                {
                    mailMessage.ReplyToList.Remove(a);
                });
                foreach (MailAddress mailAddress in parseMails(replytoMail))
                {
                    mailMessage.ReplyToList.Add(mailAddress);
                }
            }
            #endregion

            #region Alternate Views
            string textBody = htmlBody;
            AlternateView textView = AlternateView.CreateAlternateViewFromString(textBody, Encoding.UTF8, "text/plain");
            mailMessage.AlternateViews.Add(textView);

            StringBuilder htmlContent = new StringBuilder();
            htmlContent.AppendLine(htmlBody);

            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlContent.ToString(), Encoding.UTF8, "text/html");
            mailMessage.AlternateViews.Add(htmlView);
            #endregion

            #region Attachments
            if (fielsPath != null)
            {
                mailMessage.addAttachments(fielsPath);
            }
            #endregion

            Send(mailMessage, async);
        }
        public static void SendMail(string recipients, string htmlBody, bool async = false, string subject = "", Dictionary<string, byte[]> fielsListArray = null, string displayName = "", string from = "", bool isreplyto = false, string replytoMail = "")
        {
            SendMail(from, displayName, parseMails(recipients), null, null, subject, htmlBody, async, fielsListArray, isreplyto, replytoMail);
        }
        public static void SendMail(string recipients, string ccRecipients, string bccRecipients, string htmlBody, bool async = false, string subject = "", Dictionary<string, byte[]> fielsListArray = null, string displayName = "", string from = "", bool isreplyto = false, string replytoMail = "")
        {
            SendMail(from, displayName, parseMails(recipients), (!string.IsNullOrWhiteSpace(ccRecipients) ? parseMails(ccRecipients) : null), (!string.IsNullOrWhiteSpace(bccRecipients) ? parseMails(bccRecipients) : null), subject, htmlBody, async, fielsListArray, isreplyto, replytoMail);
        }
        /// <summary>
        /// secondery send message function, the Attachments are sent as byte array
        /// </summary>
        /// <param name="from"></param>
        /// <param name="displayName"></param>
        /// <param name="to"></param>
        /// <param name="cc"></param>
        /// <param name="bcc"></param>
        /// <param name="subject"></param>
        /// <param name="htmlBody"></param>
        /// <param name="async"></param>
        /// <param name="fielsPath"></param>
        /// <param name="isreplyto"></param>
        /// <param name="replytoMail"></param>
        public static void SendMail(string from, string displayName, MailAddressCollection to, MailAddressCollection cc, MailAddressCollection bcc, string subject, string htmlBody, bool async = false, Dictionary<string, byte[]> fielsListArray = null, bool isreplyto = false, string replytoMail = "")
        {
            if (to.Count == 0)
            {
                return;
            }

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = SetUpFromMail(from, displayName);

            mailMessage.Subject = subject.Replace('\r', ' ').Replace('\n', ' ');
            mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            #region Addressees
            foreach (MailAddress mailAddress in to)
            {
                mailMessage.To.Add(mailAddress);
            }

            if (cc != null)
            {
                foreach (MailAddress mailAddress in cc)
                {
                    mailMessage.CC.Add(mailAddress);
                }
            }

            if (bcc != null)
            {
                foreach (MailAddress mailAddress in bcc)
                {
                    mailMessage.Bcc.Add(mailAddress);
                }
            }
            if (isreplyto && MailClient.IsReplyTo && !string.IsNullOrWhiteSpace(MailClient.ReplyToAddress))
            {
                foreach (MailAddress mailAddress in parseMails(MailClient.ReplyToAddress))
                {
                    mailMessage.ReplyToList.Add(mailAddress);
                }
            }
            if (isreplyto && !string.IsNullOrWhiteSpace(replytoMail))
            {
                mailMessage.From = SetUpFromMail("", displayName);
                mailMessage.ReplyToList.ToList().ForEach(a =>
                {
                    mailMessage.ReplyToList.Remove(a);
                });
                foreach (MailAddress mailAddress in parseMails(replytoMail))
                {
                    mailMessage.ReplyToList.Add(mailAddress);
                }
            }
            #endregion

            #region Alternate Views
            string textBody = htmlBody;
            AlternateView textView = AlternateView.CreateAlternateViewFromString(textBody, Encoding.UTF8, "text/plain");
            mailMessage.AlternateViews.Add(textView);

            StringBuilder htmlContent = new StringBuilder();
            htmlContent.AppendLine(htmlBody);

            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlContent.ToString(), Encoding.UTF8, "text/html");
            mailMessage.AlternateViews.Add(htmlView);
            #endregion

            #region Attachments
            if (fielsListArray != null)
            {
                mailMessage.addAttachments(fielsListArray);
            }
            #endregion

            SendMail(mailMessage, async);
        }
        private static async void SendMail(MailMessage message, bool IsSendMailAsync = false, int exceptionIndex = 0)
        {
            try
            {
                SmtpClient client = MailClient.Client;

                //client.SendCompleted += Client_SendCompleted;

                message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess;
                message.DeliveryNotificationOptions = DeliveryNotificationOptions.Delay;
                message.BodyEncoding = UTF8Encoding.UTF8;
                message.BodyTransferEncoding = TransferEncoding.Base64;

                if (IsSendMailAsync)
                {
                    client.SendCompleted += Client_SendCompleted;
                    await client.SendMailAsync(message);
                    message.Dispose();
                }
                else
                {
                    client.Send(message);
                }
            }
            catch (SmtpFailedRecipientsException ex)
            {
                ex.LogError();

                for (; exceptionIndex < ex.InnerExceptions.Length; exceptionIndex++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[exceptionIndex].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy || status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Log.FormLog("Delivery failed - retrying in 5 seconds.");
                        System.Threading.Thread.Sleep(5000);
                        SendMail(message, IsSendMailAsync, exceptionIndex);
                    }
                    else
                    {
                        Log.LogError(ex.InnerExceptions[exceptionIndex].FailedRecipient, "Failed to deliver message");
                    }
                }
            }
            catch (SmtpException ex)
            {
                ex.LogError();
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
            finally
            {
                message.Dispose();
            }
        }

        private static void Client_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            Log.FormLog("client_SendCompleted");
            Log.FormLog(sender);
            Log.FormLog(e);
        }
        private static async void Send(MailMessage message, bool IsSendMailAsync = false)
        {
            try
            {
                SmtpClient client = MailClient.Client;
                if (IsSendMailAsync)
                {
                    client.SendCompleted += Client_SendCompleted;
                    await client.SendMailAsync(message);
                    message.Dispose();
                }
                else
                {
                    client.Send(message);
                    message.Dispose();
                }
            }
            catch (SmtpException ex)
            {
                throw ex.LogError();
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        private static void addAttachments(this MailMessage mailMessage, Dictionary<string, byte[]> fielsListArray)
        {
            if (!fielsListArray.Any())
                return;

            foreach (KeyValuePair<string, byte[]> file in fielsListArray)
            {
                Attachment data = new Attachment(new MemoryStream(file.Value), file.Key, MediaTypeNames.Application.Octet);
                mailMessage.Attachments.Add(data);
            }
        }
        private static void addAttachments(this MailMessage mailMessage, string fielsPath)
        {
            var files = fielsPath.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (!files.Any())
            {
                files = null;
            }
            else
            {
                foreach (var file in files)
                {
                    string sf = HttpContext.Current.Server.MapPath(file);
                    Attachment data = new Attachment(sf, MediaTypeNames.Application.Octet);
                    ContentDisposition disposition = data.ContentDisposition;
                    disposition.CreationDate = System.IO.File.GetCreationTime(sf);
                    disposition.ModificationDate = System.IO.File.GetLastWriteTime(sf);
                    disposition.ReadDate = System.IO.File.GetLastAccessTime(sf);
                    mailMessage.Attachments.Add(data);
                }
            }
        }
        private static MailAddress SetUpFromMail(string from = null, string displayName = null)
        {
            MailAddress From;
            if (string.IsNullOrWhiteSpace(from))
                from = MailClient.FromName;
            if (string.IsNullOrWhiteSpace(displayName))
                displayName = MailClient.DisplayName;
            try
            {
                From = new MailAddress(from, displayName);
            }
            catch
            {
                try
                {
                    From = new MailAddress("no_reply@" + from, displayName);
                }
                catch
                {
                    From = new MailAddress("no_reply@tikoved.co.il", displayName);
                }
            }
            return From;
        }
        private static MailAddressCollection parseMails(string MailAddress)
        {
            string[] tos = MailAddress.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);

            MailAddressCollection collection = new MailAddressCollection();

            foreach (var to in tos)
            {
                try
                {
                    collection.Add(new MailAddress(to));
                }
                catch { }
            }
            return collection;
        }
    }
}
