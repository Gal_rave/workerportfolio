﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using Logger;

namespace Mail
{
    public static class MailControllerExtension
    {
        public static string RenderView(HttpContext context, RouteData routeData, string controllerName, string viewName, object viewData)
        {
            //Create memory writer
            try
            {
                var writer = new StringWriter();

                if(routeData.Values.ContainsKey("controller"))
                    routeData.Values.Remove("controller");
                routeData.Values.Add("controller", controllerName);

                var MailTemplateControllerContext = new ControllerContext(new HttpContextWrapper(context), routeData, new MailTemplateController());

                var razorViewEngine = new RazorViewEngine();
                var razorViewResult = razorViewEngine.FindView(MailTemplateControllerContext, viewName, "", false);
                
                var viewContext = new ViewContext(MailTemplateControllerContext, razorViewResult.View, new ViewDataDictionary(viewData), new TempDataDictionary(), writer);
                razorViewResult.View.Render(viewContext, writer);

                return writer.ToString();
            }
            catch (Exception ex)
            {
                return ex.LogError(ex.ToString());
            }
        }
    }
}
