﻿using System.Configuration;
using System.Net.Mail;
using DataAccess.Extensions;

namespace Mail
{
    public static class MailClient
    {
        #region Class Variables
        private static bool _InitializingLock;
        private static bool isInitialized = false;
        private static SmtpClient client;
        private static string smtpHost;
        private static int smtpPort;
        private static bool smtpDefaultCredentials;
        private static SmtpDeliveryMethod smtpDeliveryMethod;
        private static bool smtpEnableSsl;
        private static int smtpTimeout;
        #endregion
        #region Properties 
        public static string Host
        {
            get
            {
                return smtpHost;
            }
            set
            {
                smtpHost = value;
            }
        }
        public static string DefaultEmail
        {
            get
            {
                return ConfigurationManager.AppSettings["smtpDefaultEmail"];
            }
        }
        public static int Port
        {
            get
            {
                return smtpPort;
            }
            set
            {
                smtpPort = value;
            }
        }
        public static int Timeout
        {
            get
            {
                return smtpTimeout;
            }
            set
            {
                smtpTimeout = value;
            }
        }
        public static bool EnableSsl
        {
            get
            {
                return smtpEnableSsl;
            }
            set
            {
                smtpEnableSsl = value;
            }
        }
        public static bool DefaultCredentials
        {
            get
            {
                return smtpDefaultCredentials;
            }
            set
            {
                smtpDefaultCredentials = value;
            }
        }
        public static bool IsReplyTo
        {
            get
            {
                return ConfigurationManager.AppSettings["smtpIsReplyTo"].ToBool();
            }
        }
        public static string DisplayName
        {
            get
            {
                return ConfigurationManager.AppSettings["smtpDisplayName"];
            }
        }
        public static string FromName
        {
            get
            {
                return ConfigurationManager.AppSettings["smtpFromName"];
            }
        }
        public static string ReplyToAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["smtpReplyToAddress"];
            }
        }
        public static string SiteManagerMail
        {
            get
            {
                return ConfigurationManager.AppSettings["smtpSiteManagerMail"];
            }
        }
        public static SmtpDeliveryMethod DeliveryMethod
        {
            get
            {
                return smtpDeliveryMethod;
            }
            set
            {
                smtpDeliveryMethod = value;
            }
        }
        public static SmtpClient Client
        {
            get
            {
                if (client == null || !Initializ()) Initializ();
                return client;
            }
            set
            {
                client = value;
            }
        }
        #endregion
        public static bool Initializ()
        {
            if (_InitializingLock) return true;
            if (isInitialized)
                return isInitialized;
            _InitializingLock = true;
            Host = ConfigurationManager.AppSettings["smtpHost"];
            Port = ConfigurationManager.AppSettings["smtpPort"].ToInt();
            Timeout = ConfigurationManager.AppSettings["smtpTimeout"].ToInt();
            DefaultCredentials = ConfigurationManager.AppSettings["smtpDefaultCredentials"].ToBool();
            EnableSsl = ConfigurationManager.AppSettings["smtpEnableSsl"].ToBool();

            switch (ConfigurationManager.AppSettings["smtpDeliveryMethod"].ToInt())
            {
                case 0:
                    DeliveryMethod = SmtpDeliveryMethod.Network;
                    break;
                case 1:
                    DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                    break;
                case 2:
                    DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
                    break;
                default:
                    DeliveryMethod = SmtpDeliveryMethod.Network;
                    break;
            }

            SmtpClient _Client = new SmtpClient(Host, Port);
            _Client.DeliveryMethod = DeliveryMethod;
            _Client.EnableSsl = EnableSsl;
            _Client.Timeout = Timeout;
            _Client.UseDefaultCredentials = DefaultCredentials;
            Client = _Client;
            isInitialized = true;
            _InitializingLock = false;
            return isInitialized;
        }
    }
}
