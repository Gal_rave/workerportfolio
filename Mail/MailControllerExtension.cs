﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using Logger;

namespace Mail
{
    public static class MailControllerExtension
    {
        public static string RenderView(HttpContext context, RouteData routeData, string controllerName, string viewName, object viewData)
        {
            //Create memory writer
            try
            {
                var writer = new StringWriter();

                if (routeData.Values.ContainsKey("controller"))
                    routeData.Values.Remove("controller");
                routeData.Values.Add("controller", controllerName);

                var MailTemplateControllerContext = new ControllerContext(new HttpContextWrapper(context), routeData, new MailTemplateController());

                var razorViewEngine = new RazorViewEngine();
                var razorViewResult = razorViewEngine.FindView(MailTemplateControllerContext, viewName, "", false);

                var viewContext = new ViewContext(MailTemplateControllerContext, razorViewResult.View, new ViewDataDictionary(viewData), new TempDataDictionary(), writer);
                razorViewResult.View.Render(viewContext, writer);

                return writer.ToString();
            }
            catch (Exception ex)
            {
                //return ex.LogError(ex.ToString());
                throw ex.LogError();
            }
        }
    }

    public class MCE
    {
        public string RenderView(HttpContext context, RouteData routeData, string controllerName, string viewName, object viewData, bool inError = false)
        {
            //Create memory writer
            try
            {
                var writer = new StringWriter();

                if (!routeData.Values.ContainsKey("controller"))
                    routeData.Values.Add("controller", controllerName);
                if (!routeData.Values.ContainsKey("View"))
                    routeData.Values.Add("View", viewName);

                ControllerContext MailTemplateControllerContext = new ControllerContext(new HttpContextWrapper(context), routeData, new MailTemplateController());

                RazorViewEngine razorViewEngine = new RazorViewEngine();
                ViewEngineResult razorViewResult = razorViewEngine.FindView(MailTemplateControllerContext, viewName, "", false);

                ViewContext viewContext = new ViewContext(MailTemplateControllerContext, razorViewResult.View, new ViewDataDictionary(viewData), new TempDataDictionary(), writer);

                razorViewResult.View.Render(viewContext, writer);
                razorViewEngine.ReleaseView(MailTemplateControllerContext, razorViewResult.View);
                
                return writer.ToString();
            }
            catch (Exception ex)
            {
                ex.LogError(routeData.Values["token"]);
                throw ex;
            }
        }
    }
}
