﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;

using Logger;
using System.Threading.Tasks;

namespace Mail
{
    public class MailSender
    {
        public void Send(string recipients, string htmlBody, bool async = false, string subject = "", string fielsPath = null, string displayName = "", string from = "", bool isreplyto = false, string replytoMail = "")
        {
            Send(from, displayName, parseMails(recipients), null, null, subject, htmlBody, async, fielsPath, isreplyto, replytoMail);
        }
        public void Send(string recipients, string ccRecipients, string bccRecipients, string htmlBody, bool async = false, string subject = "", string fielsPath = null, string displayName = "", string from = "", bool isreplyto = false, string replytoMail = "")
        {
            Send(from, displayName, parseMails(recipients), (!string.IsNullOrWhiteSpace(ccRecipients) ? parseMails(ccRecipients) : null), (!string.IsNullOrWhiteSpace(bccRecipients) ? parseMails(bccRecipients) : null), subject, htmlBody, async, fielsPath, isreplyto, replytoMail);
        }
        public void Send(string from, string displayName, MailAddressCollection to, MailAddressCollection cc, MailAddressCollection bcc, string subject, string htmlBody, bool async = false, string fielsPath = null, bool isreplyto = false, string replytoMail = "")
        {
            if (to.Count == 0)
            {
                return;
            }

            MailMessage mailMessage = createMailMessage();

            mailMessage.From = SetUpFromMail(from, displayName);

            mailMessage.Subject = subject.Replace('\r', ' ').Replace('\n', ' ');
            mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            #region Addressees
            foreach (MailAddress mailAddress in to)
            {
                mailMessage.To.Add(mailAddress);
            }

            if (cc != null)
            {
                foreach (MailAddress mailAddress in cc)
                {
                    mailMessage.CC.Add(mailAddress);
                }
            }

            if (bcc != null)
            {
                foreach (MailAddress mailAddress in bcc)
                {
                    mailMessage.Bcc.Add(mailAddress);
                }
            }
            if (isreplyto && MailClient.IsReplyTo && !string.IsNullOrWhiteSpace(MailClient.ReplyToAddress))
            {
                foreach (MailAddress mailAddress in parseMails(MailClient.ReplyToAddress))
                {
                    mailMessage.ReplyToList.Add(mailAddress);
                }
            }
            if (isreplyto && !string.IsNullOrWhiteSpace(replytoMail))
            {
                mailMessage.From = SetUpFromMail("", displayName);
                mailMessage.ReplyToList.ToList().ForEach(a =>
                {
                    mailMessage.ReplyToList.Remove(a);
                });
                foreach (MailAddress mailAddress in parseMails(replytoMail))
                {
                    mailMessage.ReplyToList.Add(mailAddress);
                }
            }
            #endregion

            #region Alternate Views
            string textBody = htmlBody;
            AlternateView textView = AlternateView.CreateAlternateViewFromString(textBody, Encoding.UTF8, "text/plain");
            mailMessage.AlternateViews.Add(textView);

            StringBuilder htmlContent = new StringBuilder();
            htmlContent.AppendLine(htmlBody);

            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlContent.ToString(), Encoding.UTF8, "text/html");
            mailMessage.AlternateViews.Add(htmlView);
            #endregion

            #region Attachments
            if (fielsPath != null)
            {
                addAttachments(mailMessage, fielsPath);
            }
            #endregion

            Send(mailMessage, async);
        }

        public void SendMail(string recipients, string htmlBody, bool async = false, string subject = "", Dictionary<string, byte[]> fielsListArray = null, string displayName = "", string from = "", bool isreplyto = false, string replytoMail = "")
        {
            SendMail(from, displayName, parseMails(recipients), null, null, subject, htmlBody, async, fielsListArray, isreplyto, replytoMail);
        }
        public void SendMail(string recipients, string ccRecipients, string bccRecipients, string htmlBody, bool async = false, string subject = "", Dictionary<string, byte[]> fielsListArray = null, string displayName = "", string from = "", bool isreplyto = false, string replytoMail = "")
        {
            SendMail(from, displayName, parseMails(recipients), (!string.IsNullOrWhiteSpace(ccRecipients) ? parseMails(ccRecipients) : null), (!string.IsNullOrWhiteSpace(bccRecipients) ? parseMails(bccRecipients) : null), subject, htmlBody, async, fielsListArray, isreplyto, replytoMail);
        }
        public void SendMail(string from, string displayName, MailAddressCollection to, MailAddressCollection cc, MailAddressCollection bcc, string subject, string htmlBody, bool async = false, Dictionary<string, byte[]> fielsListArray = null, bool isreplyto = false, string replytoMail = "")
        {
            if (to.Count == 0)
            {
                return;
            }

            MailMessage mailMessage = createMailMessage();

            mailMessage.From = SetUpFromMail(from, displayName);

            mailMessage.Subject = subject.Replace('\r', ' ').Replace('\n', ' ');
            mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            #region Addressees
            foreach (MailAddress mailAddress in to)
            {
                mailMessage.To.Add(mailAddress);
            }

            if (cc != null)
            {
                foreach (MailAddress mailAddress in cc)
                {
                    mailMessage.CC.Add(mailAddress);
                }
            }

            if (bcc != null)
            {
                foreach (MailAddress mailAddress in bcc)
                {
                    mailMessage.Bcc.Add(mailAddress);
                }
            }
            if (isreplyto && MailClient.IsReplyTo && !string.IsNullOrWhiteSpace(MailClient.ReplyToAddress))
            {
                foreach (MailAddress mailAddress in parseMails(MailClient.ReplyToAddress))
                {
                    mailMessage.ReplyToList.Add(mailAddress);
                }
            }
            if (isreplyto && !string.IsNullOrWhiteSpace(replytoMail))
            {
                mailMessage.From = SetUpFromMail("", displayName);
                mailMessage.ReplyToList.ToList().ForEach(a =>
                {
                    mailMessage.ReplyToList.Remove(a);
                });
                foreach (MailAddress mailAddress in parseMails(replytoMail))
                {
                    mailMessage.ReplyToList.Add(mailAddress);
                }
            }
            #endregion

            #region Alternate Views
            string textBody = htmlBody;
            AlternateView textView = AlternateView.CreateAlternateViewFromString(textBody, Encoding.UTF8, "text/plain");
            mailMessage.AlternateViews.Add(textView);

            StringBuilder htmlContent = new StringBuilder();
            htmlContent.AppendLine(htmlBody);

            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlContent.ToString(), Encoding.UTF8, "text/html");
            mailMessage.AlternateViews.Add(htmlView);
            #endregion

            #region Attachments
            if (fielsListArray != null)
            {
                addAttachments(mailMessage, fielsListArray);
            }
            #endregion

            SendMail(mailMessage, async);
        }

        private async void SendMail(MailMessage message, bool IsSendMailAsync = false, int exceptionIndex = 0)
        {
            try
            {
                SmtpClient client = createSmtpClient(IsSendMailAsync);
                if (IsSendMailAsync)
                {
                    await client.SendMailAsync(message);
                }
                else
                {
                    client.Send(message);
                }
            }
            catch (SmtpFailedRecipientsException ex)
            {
                ex.LogError();

                for (; exceptionIndex < ex.InnerExceptions.Length; exceptionIndex++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[exceptionIndex].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy || status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Log.MailLog("Delivery failed - retrying in 5 seconds.");
                        System.Threading.Thread.Sleep(5000);
                        SendMail(message, IsSendMailAsync, exceptionIndex);
                    }
                    else
                    {
                        Log.MailLog(ex.InnerExceptions[exceptionIndex].FailedRecipient, "Failed to deliver message");
                    }
                }
            }
            catch (SmtpException ex)
            {
                ex.LogError();
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
        }
        private async void Send(MailMessage message, bool IsSendMailAsync = false, bool inError = false)
        {
            try
            {
                SmtpClient client = createSmtpClient(IsSendMailAsync);
                if (IsSendMailAsync)
                {
                    await client.SendMailAsync(message);
                }
                else
                {
                    client.Send(message);
                }
            }
            catch (SmtpException ex)
            {
                if (!inError)
                {
                    Send(message, IsSendMailAsync, true);
                    return;
                }
                //ex.LogError();
                throw ex.LogError();
            }
            catch (Exception ex)
            {
                if (!inError)
                {
                    Send(message, IsSendMailAsync, true);
                    return;
                }

                //ex.LogError();
                throw ex.LogError();
            }
        }

        private void addAttachments(MailMessage mailMessage, Dictionary<string, byte[]> fielsListArray)
        {
            if (!fielsListArray.Any())
                return;

            foreach (KeyValuePair<string, byte[]> file in fielsListArray)
            {
                Attachment data = new Attachment(new MemoryStream(file.Value), file.Key, MediaTypeNames.Application.Octet);
                mailMessage.Attachments.Add(data);
            }
        }
        private void addAttachments(MailMessage mailMessage, string fielsPath)
        {
            var files = fielsPath.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (!files.Any())
            {
                files = null;
            }
            else
            {
                foreach (var file in files)
                {
                    string sf = HttpContext.Current.Server.MapPath(file);
                    Attachment data = new Attachment(sf, MediaTypeNames.Application.Octet);
                    ContentDisposition disposition = data.ContentDisposition;
                    disposition.CreationDate = System.IO.File.GetCreationTime(sf);
                    disposition.ModificationDate = System.IO.File.GetLastWriteTime(sf);
                    disposition.ReadDate = System.IO.File.GetLastAccessTime(sf);
                    mailMessage.Attachments.Add(data);
                }
            }
        }

        private MailAddress SetUpFromMail(string from = null, string displayName = null)
        {
            MailAddress From;
            if (string.IsNullOrWhiteSpace(from))
                from = MailClient.FromName;
            if (string.IsNullOrWhiteSpace(displayName))
                displayName = MailClient.DisplayName;
            try
            {
                From = new MailAddress(from, displayName);
            }
            catch
            {
                try
                {
                    From = new MailAddress("no_reply@" + from, displayName);
                }
                catch
                {
                    From = new MailAddress("no_reply@tikoved.co.il", displayName);
                }
            }
            return From;
        }
        private MailAddressCollection parseMails(string MailAddress)
        {
            string[] tos = MailAddress.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);

            MailAddressCollection collection = new MailAddressCollection();

            foreach (var to in tos)
            {
                try
                {
                    collection.Add(new MailAddress(to));
                }
                catch { }
            }
            return collection;
        }
        private MailMessage createMailMessage()
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            mailMessage.BodyEncoding = UTF8Encoding.UTF8;
            mailMessage.BodyTransferEncoding = TransferEncoding.Base64;
            mailMessage.SubjectEncoding = UTF8Encoding.UTF8;
            return mailMessage;
        }
        private SmtpClient createSmtpClient(bool IsSendMailAsync)
        {
            SmtpClient _Client = new SmtpClient(MailClient.Host, MailClient.Port);
            _Client.DeliveryMethod = MailClient.DeliveryMethod;
            _Client.EnableSsl = MailClient.EnableSsl;
            _Client.Timeout = MailClient.Timeout;
            _Client.UseDefaultCredentials = MailClient.DefaultCredentials;
            _Client.SendCompleted += IsSendMailAsync ? new SendCompletedEventHandler(Client_SendCompleted) : new SendCompletedEventHandler(SendCompletedCallback);
            return _Client;
        }

        private void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            string token = (string)e.UserState;

            if (e.Cancelled)
            {
                Log.MailLog(string.Format("[{0}] Send canceled.", token));
            }
            if (e.Error != null)
            {
                Log.MailLog(string.Format("[{0}] Send Error {1}", token, e.Error.ToString()));
            }
            //else
            //{
            //    Log.MailLog(string.Format("[{0}] Message sent.", token));
            //}
        }
        private void Client_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                Log.MailLog("Send canceled.");
            }
            if (e.Error != null)
            {
                Log.MailLog(string.Format("Send Error [{0}]", e.Error.ToString()));
            }
            //else
            //{
            //    Log.MailLog(e, "Client_SendCompleted e");
            //}
        }
    }
}
