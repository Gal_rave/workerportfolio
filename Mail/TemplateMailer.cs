﻿using System;
using System.Web;
using System.Web.Routing;

using DataAccess.Models;
using DataAccess.BLLs;
using DataAccess.Enums;
using System.IO;
using Logger;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Mail
{
    public class TemplateMailer
    {
        private readonly string match = @"\[(.+?)]";
        private Regex regex;

        private FilesBll filesBll;
        private MailerBll mailerBll;

        public TemplateMailer()
        {
            filesBll = new FilesBll();
            mailerBll = new MailerBll();
        }

        public void Send(int municipalityId, int TemplateID, List<decimal> uIds)
        {
            HtmlTemplateModel Template = filesBll.GetTemplateById(TemplateID);
            MunicipalityModel Municipality = mailerBll.GetMunicipalityAndContacts(municipalityId);
            List<USER> users = mailerBll.GetUsersByIds(uIds, municipalityId);

            Log.TestLog(Template);
            if (Template == null || Municipality == null || users == null)
                return;
            Match maches = Regex.Match(Template.template, match);
            Log.TestLog(maches, "maches");
            while (maches != null)
            {
                Template.template.Replace(maches.Value, "XXXX");
                maches = Regex.Match(Template.template, match);
                Log.TestLog(maches, "maches");
            }
            Log.TestLog(Template, "Template");

            //users.ForEach(u=> SendToUser(u, Municipality, Template));
        }

        private void SendToUser(USER user, MUNICIPALITy Municipality, HtmlTemplateModel Template)
        {
            var maches = Regex.Match(Template.template, match);
            if (maches != null)
                //foreach (var m in maches)
                //{
                //    Log.TestLog(m, "foreach");
                //}
                Log.TestLog(maches, "maches");
        }
    }
}
