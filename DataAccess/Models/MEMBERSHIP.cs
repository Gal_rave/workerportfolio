//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MEMBERSHIP
    {
        public decimal USERID { get; set; }
        public string PASSWORDSALT { get; set; }
        public string PASSWORD { get; set; }
        public System.DateTime CREATEDDATE { get; set; }
        public string CONFIRMATIONTOKEN { get; set; }
        public bool ISCONFIRMED { get; set; }
        public Nullable<System.DateTime> LASTPASSWORDFAILUREDATE { get; set; }
        public decimal PASSWORDFAILURESCOUNTER { get; set; }
        public Nullable<System.DateTime> PASSWORDCHANGEDDATE { get; set; }
        public string PASSWORDVERIFICATIONTOKEN { get; set; }
        public Nullable<System.DateTime> VERIFICATIONTOKENEXPIRATION { get; set; }
        public string PASSWORDFORMAT { get; set; }
        public string SMSVERIFICATIONTOKEN { get; set; }
        public Nullable<System.DateTime> SMSTOKENEXPIRATION { get; set; }
    
        public virtual USER USER { get; set; }
    }
}
