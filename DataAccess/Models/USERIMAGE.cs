//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class USERIMAGE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public USERIMAGE()
        {
            this.FORM101DATA = new HashSet<FORM101DATA>();
            this.FORM101DATA1 = new HashSet<FORM101DATA>();
            this.UPC_MAILS = new HashSet<UPC_MAILS>();
        }
    
        public decimal ID { get; set; }
        public byte[] FILEBLOB { get; set; }
        public decimal USERID { get; set; }
        public string TYPE { get; set; }
        public Nullable<System.DateTime> FILEDATE { get; set; }
        public Nullable<decimal> MUNICIPALITYID { get; set; }
        public string FILENAME { get; set; }
        public byte[] FILEIMAGEBLOB { get; set; }
    
        public virtual USER USER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FORM101DATA> FORM101DATA { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FORM101DATA> FORM101DATA1 { get; set; }
        public virtual MUNICIPALITy MUNICIPALITy { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UPC_MAILS> UPC_MAILS { get; set; }
    }
}
