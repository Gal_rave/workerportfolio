//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class HUMANRESOURCESCONTACT
    {
        public decimal ID { get; set; }
        public string NAME { get; set; }
        public string JOB { get; set; }
        public string EMAIL { get; set; }
        public string PHONE { get; set; }
        public decimal MUNICIPALITYID { get; set; }
    
        public virtual MUNICIPALITy MUNICIPALITy { get; set; }
    }
}
