﻿using System;

namespace DataAccess.Models
{
    public class PopulationDefinitionModel
    {
        public int population_id { get; set; }
        public Nullable<short> rashut { get; set; }
        public string population_name { get; set; }
        public Nullable<short> population_type { get; set; }
        public Nullable<int> from_date { get; set; }
        public Nullable<int> to_date { get; set; }
        public string population_full_description { get; set; }
        public string population_sql_fixed_statement { get; set; }
        public Nullable<short> status { get; set; }
        public Nullable<DateTime> creation_date { get; set; }
        public Nullable<DateTime> last_update_date { get; set; }
        public Nullable<int> user_id { get; set; }
    }
}
