﻿namespace DataAccess.Models
{
    public class ManagerModel
    {
        public int idnumber { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Cell { get; set; }
    }
}
