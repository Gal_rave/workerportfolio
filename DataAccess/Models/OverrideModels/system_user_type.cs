﻿namespace DataAccess.Models
{
    public class system_user_type
    {
        public string ovd_kav_mispar_zehut { get; set; }
        public string ovd_moz_sisma_internet { get; set; }
        public string rashut_mancol { get; set; }
        public string rashut_sachar { get; set; }
        public string email { get; set; }
        public string ovd_ktv_tel_nosaf_kidomet { get; set; }
        public string ovd_ktv_tel_nosaf { get; set; }
    }
}
