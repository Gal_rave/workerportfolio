﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class UserEventModel
    {
        public string idNumber { get; set; }
        public bool isAllow101 { get; set; }
        public int municipalityId { get; set; }
    }
}
