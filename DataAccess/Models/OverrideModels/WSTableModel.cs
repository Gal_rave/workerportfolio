﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class WSTableModel
    {
        public WSTableModel()
        {
            this.Titles = new List<Title>();
            this.Data = new List<List<object>>();
            this.startYearMonth = 0;
        }
        public WSTableModel(List<Title> titles)
        {
            this.Titles = titles;
            this.Data = new List<List<object>>();
        }
        public void AddDataRow<T>(ICollection<T> list)
        {
            this.Data.Add(list.Select(l=> (object)l).ToList());
        }

        public int startYearMonth { get; set; }
        public List<Title> Titles { get; set; }
        public List<List<object>> Data { get; set; }
    }
    
    public class Title
    {
        public Title() { }
        public Title(int n, string t)
        {
            this.number = n;
            this.title = t;
        }
        public Title(int n, string t, string vt)
        {
            this.number = n;
            this.title = t;
            this.valueType = vt;
        }

        public int number { get; set; }
        public string title { get; set; }
        public string valueType { get; set; }
    }
}
