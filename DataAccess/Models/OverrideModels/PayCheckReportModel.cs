﻿using System;
using DataAccess.Extensions;

namespace DataAccess.Models
{
    public class PayCheckReportModel
    {
        public int user_Id { get; set; }
        //[TitleAttribute("תעודת זהות")]
        public string idNumber { get; set; }
        //[TitleAttribute("שם מלא")]
        public string user_Name { get; set; }
        //[TitleAttribute("אישור תלוש במייל")]
        public Nullable<Int16> activeRecord { get; set; }
        //[TitleAttribute("תאריך רישום")]
        public Nullable<DateTime> createdDate { get; set; }
        //[TitleAttribute("תאריך עדכון")]
        public Nullable<DateTime> updateDate { get; set; }
        //[TitleAttribute("מקור רישום")]
        public Nullable<Int16> creationSource { get; set; }
        //[TitleAttribute("תאריך שליחה")]
        public Nullable<DateTime> sendDate { get; set; }
        public Nullable<int> archive_Id { get; set; }
        //[TitleAttribute("כתובת מייל")]
        public string email { get; set; }
        //[TitleAttribute("קוד סיום")]
        public Nullable<int> terminationCause { get; set; }
        //[TitleAttribute("סיבת סיום")]
        public string terminationReason { get; set; }
        //[TitleAttribute("תאריך סיום")]
        public Nullable<DateTime> terminationDate { get; set; }
    }
}
