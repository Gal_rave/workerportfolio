﻿namespace DataAccess.Models
{
    public class ShortUserModel
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public bool Send { get; set; }
    }
}
