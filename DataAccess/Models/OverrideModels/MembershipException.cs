﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class MembershipException : Exception
    {
        #region custom Variables
        public int ExceptionId { get; set; }
        #endregion
        public MembershipException() : base() { }
        public MembershipException(int exceptionid) : base()
        {
            this.ExceptionId = exceptionid;
        }
        public MembershipException(String message) : base(message) { }
        public MembershipException(String message, int exceptionid) : base(message)
        {
            this.ExceptionId = exceptionid;
        }
        public MembershipException(String message, Exception innerException) : base(message, innerException) { }
        public MembershipException(String message, int exceptionid, Exception innerException) : base(message, innerException)
        {
            this.ExceptionId = exceptionid;
        }
        protected MembershipException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
