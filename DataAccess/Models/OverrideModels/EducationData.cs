﻿namespace DataAccess.Models
{
    public class EducationData
    {
        public string CERTIFICATE_NAM { get; set; }
        public string INSTITUTION_NAME { get; set; }
        public string SUBJECT_1 { get; set; }
        public string SUBJECT_2 { get; set; }
        public string EDUCATION_YEARS { get; set; }
        public string DEGREE { get; set; }
        public string GRADUATION_DATE { get; set; }
    }
}
