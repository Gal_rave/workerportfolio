﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class taxExemptionExtensionModel : taxExemptionModel
    {
        public int CheckalimonyParticipates { get; set; }
        public int CheckincompetentChild { get; set; }
        public int CheckblindDisabled { get; set; }
        public int CheckchildrenInCare { get; set; }
        public int CheckexAlimony { get; set; }
        public int CheckexServiceman { get; set; }
        public int Checkgraduation { get; set; }
        public int CheckimmigrantStatus { get; set; }
        public int CheckimmigrantStatusDate { get; set; }
        public int Checkinfantchildren { get; set; }
        public int CheckisImmigrant { get; set; }
        public int CheckisResident { get; set; }
        public int ChecknoIncomeDate { get; set; }
        public int CheckpartnersUnder18 { get; set; }
        public int CheckseparatedParent { get; set; }
        public int CheckservicemanEndDate { get; set; }
        public int CheckservicemanStartDate { get; set; }
        public int Checksettlement { get; set; }
        public int ChecksingleParent { get; set; }
        public int CheckspouseNoIncome { get; set; }
        public int CheckstartResidentDate { get; set; }
    }
}
