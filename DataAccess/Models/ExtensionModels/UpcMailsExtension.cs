﻿namespace DataAccess.Models
{
    public class UpcMailsExtension : UPC_MAILS
    {
        public UPC_MAILS GetBase
        {
            get
            {
                return new UPC_MAILS
                {
                    ID = this.ID,
                    IDNUMBER = this.IDNUMBER,
                    FULLNAME = this.FULLNAME,
                    CALCULATEDIDNUMBER = this.CALCULATEDIDNUMBER,
                    EMAIL = this.EMAIL,
                    MUNICIPALITYID = this.MUNICIPALITYID,
                    CONFIRMATIONTOKEN = this.CONFIRMATIONTOKEN,
                    USERIMAGEID = this.USERIMAGEID,
                    CURRENTPAYMONTH = this.CURRENTPAYMONTH,
                    USERID = this.USERID,
                    PCUE_SENDARCHIVE_ID = this.PCUE_SENDARCHIVE_ID,
                    ACTIVE = this.ACTIVE,
                    CREATEDDATE = this.CREATEDDATE,
                    SENDDATE = this.SENDDATE,
                    //PCUE_SENDARCHIVE = this.PCUE_SENDARCHIVE
                };
            }
        }

        public UpcMailsExtension() { }
        public UpcMailsExtension(UPC_MAILS upc)
        {
            base.ID = upc.ID;
            base.IDNUMBER = upc.IDNUMBER;
            base.FULLNAME = upc.FULLNAME;
            base.CALCULATEDIDNUMBER = upc.CALCULATEDIDNUMBER;
            base.EMAIL = upc.EMAIL;
            base.MUNICIPALITYID = upc.MUNICIPALITYID;
            base.CONFIRMATIONTOKEN = upc.CONFIRMATIONTOKEN;
            base.USERIMAGEID = upc.USERIMAGEID;
            base.CURRENTPAYMONTH = upc.CURRENTPAYMONTH;
            base.USERID = upc.USERID;
            base.PCUE_SENDARCHIVE_ID = upc.PCUE_SENDARCHIVE_ID;
            base.ACTIVE = upc.ACTIVE;
            base.CREATEDDATE = upc.CREATEDDATE;
            base.SENDDATE = upc.SENDDATE;
        }
        public UpcMailsExtension(UPC_MAILS upc, byte[] fileblob)
        {
            base.ID = upc.ID;
            base.IDNUMBER = upc.IDNUMBER;
            base.FULLNAME = upc.FULLNAME;
            base.CALCULATEDIDNUMBER = upc.CALCULATEDIDNUMBER;
            base.EMAIL = upc.EMAIL;
            base.MUNICIPALITYID = upc.MUNICIPALITYID;
            base.CONFIRMATIONTOKEN = upc.CONFIRMATIONTOKEN;
            base.USERIMAGEID = upc.USERIMAGEID;
            base.CURRENTPAYMONTH = upc.CURRENTPAYMONTH;
            base.USERID = upc.USERID;
            base.PCUE_SENDARCHIVE_ID = upc.PCUE_SENDARCHIVE_ID;
            base.ACTIVE = upc.ACTIVE;
            base.CREATEDDATE = upc.CREATEDDATE;
            base.SENDDATE = upc.SENDDATE;
            this.FILEBLOB = fileblob;
        }
        public UpcMailsExtension(UPC_MAILS upc, byte[] fileblob, bool isFactory)
        {
            base.ID = upc.ID;
            base.IDNUMBER = upc.IDNUMBER;
            base.FULLNAME = upc.FULLNAME;
            base.CALCULATEDIDNUMBER = upc.CALCULATEDIDNUMBER;
            base.EMAIL = upc.EMAIL;
            base.MUNICIPALITYID = upc.MUNICIPALITYID;
            base.CONFIRMATIONTOKEN = upc.CONFIRMATIONTOKEN;
            base.USERIMAGEID = upc.USERIMAGEID;
            base.CURRENTPAYMONTH = upc.CURRENTPAYMONTH;
            base.USERID = upc.USERID;
            base.PCUE_SENDARCHIVE_ID = upc.PCUE_SENDARCHIVE_ID;
            base.ACTIVE = upc.ACTIVE;
            base.CREATEDDATE = upc.CREATEDDATE;
            base.SENDDATE = upc.SENDDATE;
            this.FILEBLOB = fileblob;
            this.isFactoryUser = isFactory;
        }

        public byte[] FILEBLOB { get; set; }
        public bool isFactoryUser { get; set; }
        public int FACTORY_USER { get; set; }
        public int ACTIVE_RECORD { get; set; }
    }
}
