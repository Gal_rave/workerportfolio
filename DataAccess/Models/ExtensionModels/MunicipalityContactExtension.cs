﻿namespace DataAccess.Models
{
    public class MunicipalityContactExtension
    {
        public string KIDOMET_TELEFON { get; set; }
        public string TELEFON { get; set; }
        public string EMAIL { get; set; }
        public string MISPAR_BAIT { get; set; }
        public string ISHUV { get; set; }
        public string MIKUD { get; set; }
        public string H_P { get; set; }
        public string REHOV { get; set; }
        public int MIFAL { get; set; }
        public int RASHUT { get; set; }
    }
}
