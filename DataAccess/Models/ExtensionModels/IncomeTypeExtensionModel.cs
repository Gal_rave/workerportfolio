﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class IncomeTypeExtensionModel : IncomeTypeModel
    {
        public int Checkallowance { get; set; }
        public int CheckanotherSalary { get; set; }
        public int CheckanotherSource { get; set; }
        public int CheckanotherSourceDetails { get; set; }
        public int CheckdaySalary { get; set; }
        public int CheckmonthSalary { get; set; }
        public int CheckpartialSalary { get; set; }
        public int Checkstipend { get; set; }
    }
}
