﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class IncomeExtensionModel : IncomeModel
    {
        public int CheckhasIncomes { get; set; }
        public int CheckincomeTaxCredits { get; set; }
        public int ChecktrainingFund { get; set; }
        public int CheckworkDisability { get; set; }
        public int CheckanotherSourceDetails { get; set; }
        public int CheckstartDate { get; set; }
        public new IncomeTypeExtensionModel incomeType { get; set; }
    }
}
