﻿namespace DataAccess.Models
{
    public class ChildExtensionModel : ChildModel
    {
        public int CheckbirthDate { get; set; }
        public int CheckchildBenefit { get; set; }
        public int CheckfirstName { get; set; }
        public int Checkgender { get; set; }
        public int CheckhomeLiving { get; set; }
        public int CheckidNumber { get; set; }
        public int Checkpossession { get; set; }
        public string PrevIdNumber { get; set; }
    }
}