﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class SpouseExtensionModel : SpouseModel
    {
        public int CheckbirthDate { get; set; }
        public int CheckfirstName { get; set; }
        public int CheckidNumber { get; set; }
        public int CheckimmigrationDate { get; set; }
        public int CheckincomeType { get; set; }
        public int ChecklastName { get; set; }
        public int CheckhasIncome { get; set; }
        public string PrevIdNumber { get; set; }
    }
}
