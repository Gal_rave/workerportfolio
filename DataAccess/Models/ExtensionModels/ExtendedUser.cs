﻿namespace DataAccess.Models
{
    public class ExtendedUser
    {
        public ExtendedUser() { }
        public ExtendedUser(USER _user)
        {
            this.user = _user;
        }
        public ExtendedUser(USER _user, USERIMAGE _userimage)
        {
            this.user = _user;
            this.userimage = _userimage;
        }

        public USER user { get; set; }
        public USERIMAGE userimage { get; set; }
    }
}
