﻿using System;

namespace DataAccess.Models
{
    public class HtmlTemplateModel
    {
        public decimal ID { get; set; }
        public decimal municipalityId { get; set; }
        public DateTime createdDate { get; set; }
        public Nullable<DateTime> updateDate { get; set; }
        public decimal updateUserId { get; set; }
        public string template { get; set; }
        public string subject { get; set; }
        public string name { get; set; }
        public string fromEmail { get; set; }
        public int type { get; set; }
        public bool active { get; set; }

        public string updateDateString { get; set; }
        public string UpdateUser { get; set; }

        public MunicipalityModel municipality { get; set; }
        public UserModel user { get; set; }
    }
}