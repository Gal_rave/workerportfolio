﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class CoursModel
    {
        public int ID { get; set; }
        public string certificateName { get; set; }
        public string institutionName { get; set; }
        public string studySubject1 { get; set; }
        public string studySubject2 { get; set; }
        public Nullable<int> educationYears { get; set; }
        public string degree { get; set; }
        public Nullable<DateTime> degreeDate { get; set; }
        public decimal userId { get; set; }

        public virtual UserModel user { get; set; }
    }
}
