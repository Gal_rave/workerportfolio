﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class EmailByMunicipalityModel
    {
        public int ID { get; set; }
        public string idNumber { get; set; }
        public int municipalityId { get; set; }
        public string email { get; set; }
        public Nullable<int> userId { get; set; }
        public Nullable<bool> doPaycheckPopup { get; set; }
    }
}
