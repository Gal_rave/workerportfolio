﻿namespace DataAccess.Models
{
    public class UsersToFactoryModel
    {
        public int userId { get; set; }
        public int factoryId { get; set; }
        public int municipalityId { get; set; }
    }
}
