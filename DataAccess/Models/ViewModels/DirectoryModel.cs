﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public class DirectoryModel
    {
        public decimal ID { get; set; }
        public string name { get; set; }
        public Nullable<decimal> parentId { get; set; }
        public decimal municipalityId { get; set; }

        public List<DirectoryModel> subDirectories { get; set; }
        public List<ImageModel> images { get; set; }
    }
}
