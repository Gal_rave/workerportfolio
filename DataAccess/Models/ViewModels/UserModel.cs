﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public class UserModel
    {
        public int ID { get; set; }
        public string idNumber { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public Nullable<DateTime> birthDate { get; set; }
        public Nullable<DateTime> immigrationDate { get; set; }        
        public string phone { get; set; }
        public string cell { get; set; }        
        public DateTime createdDate { get; set; }
        public bool isDeleted { get; set; }
        public DateTime lastUpdatedDate { get; set; }
        public string calculatedIdNumber { get; set; }
        public string fathersName { get; set; }
        public string previousLastName { get; set; }
        public string previousFirstName { get; set; }
        public string zip { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public Nullable<int> houseNumber { get; set; }
        public string entranceNumber { get; set; }
        public Nullable<int> apartmentNumber { get; set; }
        public string poBox { get; set; }
        public string countryOfBirth { get; set; }
        public bool gender { get; set; }
        public Nullable<int> maritalStatus { get; set; }
        public Nullable<DateTime> maritalStatusDate { get; set; }
        public bool isSpouse { get; set; }
        public Nullable<decimal> spouseId { get; set; }
        public Nullable<bool> hasIncome { get; set; }
        public string incomeType { get; set; }
        public string jobTitle { get; set; }
        public bool wellcomEmail { get; set; }
        public Nullable<bool> isAllow101 { get; set; }
        public bool initialcheck { get; set; }
        public Nullable<int> citizenshipType { get; set; }
        public bool kibbutzMember { get; set; }
        public Nullable<int> hmoCode { get; set; }
        
        /*custom properties*/
        public bool isConfirmed { get; set; }
        public string password { get; set; }
        public int factoryId { get; set; }

        public SpouseModel spouse { get; set; }
        public MembershipModel membership { get; set; }
        public virtual List<EmailByMunicipalityModel> emailByMunicipality { get; set; }
        public virtual List<GroupModel> groups { get; set; }
        public virtual List<MunicipalityModel> municipalities { get; set; }
        public virtual List<UserImageModel> userImages { get; set; }
        public virtual List<UsersTerminationModel> terminations { get; set; }
        public virtual List<PayCheckUsersByMailModel> payCheckUsersByMails { get; set; }
        public virtual List<pcueSendArchiveModel> sendArchive { get; set; }
        public virtual List<UsersToFactoryModel> usersToFactories { get; set; }
    }
}
