﻿namespace DataAccess.Models
{
    public class IncomeTypeModel
    {
        public bool monthSalary { get; set; }
        public bool anotherSalary { get; set; }
        public bool partialSalary { get; set; }
        public bool daySalary { get; set; }
        public bool allowance { get; set; }
        public bool stipend { get; set; }
        public bool anotherSource { get; set; }
        public string anotherSourceDetails { get; set; }
    }
}