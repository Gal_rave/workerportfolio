﻿using System;

namespace DataAccess.Models
{
    public class humanResourcesContactModel
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string job { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public int municipalityID { get; set; }
        public MunicipalityModel municipality { get; set; }
    }
}
