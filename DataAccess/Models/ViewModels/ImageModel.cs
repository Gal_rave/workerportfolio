﻿using System;

namespace DataAccess.Models
{
    public class ImageModel
    {
        public decimal ID { get; set; }
        public Nullable<decimal> directoryId { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public decimal municipalityId { get; set; }
        public byte[] fileBlob { get; set; }
        public string fileBlobString { get; set; }
        public Nullable<decimal> userId { get; set; }

        public virtual DirectoryModel directory { get; set; }
        public virtual MunicipalityModel municipality { get; set; }
        public virtual UserModel user { get; set; }
    }
}