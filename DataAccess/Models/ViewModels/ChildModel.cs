﻿using System;

namespace DataAccess.Models
{
    public class ChildModel
    {
        public int ID { get; set; }
        public bool possession { get; set; }
        public bool childBenefit { get; set; }
        public bool homeLiving { get; set; }
        public string firstName { get; set; }
        public string idNumber { get; set; }
        public bool gender { get; set; }
        public DateTime birthDate { get; set; }
        public int userID { get; set; }
    }
}