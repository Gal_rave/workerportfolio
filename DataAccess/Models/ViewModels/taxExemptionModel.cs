﻿using System;

namespace DataAccess.Models
{
    public class taxExemptionModel
    {
        public bool blindDisabled { get; set; }
        public bool isResident { get; set; }
        public bool isImmigrant { get; set; }
        public int immigrationStatus { get; set; }
        public Nullable<DateTime> immigrationStatusDate { get; set; }
        public Nullable<DateTime> immigrationNoIncomeDate { get; set; }
        public bool spouseNoIncome { get; set; }
        public bool separatedParent { get; set; }
        public bool childrenInCare { get; set; }
        public bool infantchildren { get; set; }
        public bool singleParent { get; set; }
        public bool exAlimony { get; set; }
        public bool partnersUnder18 { get; set; }
        public bool exServiceman { get; set; }
        public Nullable<DateTime> serviceStartDate { get; set; }
        public Nullable<DateTime> serviceEndDate { get; set; }
        public bool graduation { get; set; }
        public bool alimonyParticipates { get; set; }
        public bool incompetentChild { get; set; }
        public string settlement { get; set; }
        public Nullable<DateTime> startSettlementDate { get; set; }
    }
}
