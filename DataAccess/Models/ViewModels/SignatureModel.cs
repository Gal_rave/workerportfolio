﻿namespace DataAccess.Models
{
    public class SignatureModel
    {
        public string Token { get; set; }
        public string idNumber { get; set; }
        public string year { get; set; }
        public string base64 { get; set; }
        public string fileName { get; set; }
        public bool saved { get; set; }
        public int municipalityId { get; set; }
        public UserImageModel image { get; set; }
    }
}
