﻿using System;

namespace DataAccess.Models
{
    public class UsersTerminationModel
    {
        public int userId { get; set; }
        public int municipalityId { get; set; }
        public Nullable<DateTime> terminationDate { get; set; }
        public Nullable<int> terminationCause { get; set; }
        public Nullable<DateTime> commencementDate { get; set; }
    }
}
