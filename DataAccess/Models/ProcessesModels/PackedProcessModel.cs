﻿namespace DataAccess.Models
{
    public class PackedProcessModel
    {
        public TahalichModel process { get; set; }
        public StepModel step { get; set; }
        public ReshimatTiyugModel reshimatTiyug { get; set; }
        public MismachBereshimaModel mismachBeReshima { get; set; }
        public MismachModel mismach { get; set; }
        public KvuzatMishtamshimModel kvuzatMishtamshim { get; set; }
        public MishtamsheiKvuzaModel mishtamsheyKvuza { get; set; }
    }
}
