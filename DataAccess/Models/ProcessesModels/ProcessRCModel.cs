﻿using System;

namespace DataAccess.Models
{
    public class ProcessRCModel
    {
        public int mailRc { get; set; }
        public string mailMsg { get; set; }
        public int processRc { get; set; }
        public string processMsg { get; set; }
        public int notificationRc { get; set; }
        public string notificationMsg { get; set; }
    }
}
