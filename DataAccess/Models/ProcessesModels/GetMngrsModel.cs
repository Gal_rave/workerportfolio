﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public class GetMngrsModel
    {
        public WSPostModel model { get; set; }
        public List<YehidaIrgunitModel> yehida { get; set; }
    }
}