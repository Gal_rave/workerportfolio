﻿using System;

namespace DataAccess.Models
{
    public class UnitManagerModel
    {
        public int idNumber { get; set; }
        public int rashutNumber { get; set; }
        public string fName { get; set; }
        public string lName { get; set; }
        public int zehutNatzigKoachAdam { get; set; }
        public int tekenKoachAdam { get; set; }
        public int tekenMenaelYahidaIrgunit { get; set; }
        public string unitDescription { get; set; }
        public int treeNumber { get; set; }
        public int unitNumber { get; set; }
        public int supervisorUnitNumber { get; set; }
        public int ramaIrgunit { get; set; }
    }
}