﻿using System;

namespace DataAccess.Models
{
    public class YehidaIrgunitModel
    {
        public int treeNumber { get; set; }
        public int unitNumber { get; set; }
    }
}