﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public class KvuzatMishtamshimModel
    {
        public int recId { get; set; }
        public int rashut { get; set; }
        public int mifal { get; set; }
        public int fromDate { get; set; }
        public int toDate { get; set; }
        public string shemKvuza { get; set; }
        public string creationTime { get; set; }
        public string lastUpdateTime { get; set; }
        public int lastUpdateUser { get; set; }
        public int status { get; set; }
        public int kvuzaType { get; set; }
        public List<MishtamsheiKvuzaModel> mishtamsheiKvuzaModel { get; set; }
    }
}
