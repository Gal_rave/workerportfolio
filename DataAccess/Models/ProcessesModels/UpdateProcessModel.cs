﻿using System;

/**
 * class to recieve data about running processes instance from java, and update tik oved processes table statuses
 * */
namespace DataAccess.Models
{
    public class UpdateProcessModel
    {
        public int customerNumber { get; set; }
        public int rashutNumber { get; set; }
        public int tmProcessId { get; set; }
        public int currStepNumber { get; set; }
        public int processStatus { get; set; }
    }
}
