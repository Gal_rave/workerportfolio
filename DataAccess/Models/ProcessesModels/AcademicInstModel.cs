﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public class AcademicInstModel
    {
        public int ID { get; set; }
        public string UNIQ_ID { get; set; }
        public string FCODE_TYPE { get; set; }
        public string SETL_CODE { get; set; }
        public string SETL_NAME { get; set; }
        public string NAME { get; set; }
        public string LATIN_NAME { get; set; }
        public string USG_GROUP { get; set; }
        public string USG_CODE { get; set; }
        public string E_ORD { get; set; }
        public string N_ORD { get; set; }
        public string DATA_YEAR { get; set; }
        public Nullable<System.DateTime> PRDCT_VER { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
   }

   public class academicResult
    {
        public object fields { get; set; }
        public List<AcademicInstModel> records { get; set; }
        public string resource_id { get; set; }
        public int total { get; set; }
        public object _links { get; set; }
    }

    public class academicWrraper
    {
        public string help { get; set; }
        public academicResult result { get; set; }
        public bool success { get; set; }
    }
}