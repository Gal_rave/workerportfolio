﻿using System;

namespace DataAccess.Models
{
    public class TargetUserModel
    {
        public int idNumber { get; set; }
        public int rashutNumber { get; set; }
        public string fullName { get; set; }
    }
}