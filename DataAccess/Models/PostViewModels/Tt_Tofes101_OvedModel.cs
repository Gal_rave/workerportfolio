﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public class Tt_Tofes101_OvedModel
    {
        public decimal rec_id { get; set; }
        public int rashut { get; set; }
        public int mifal { get; set; }
        public Nullable<int> betokef_me { get; set; }
        public Nullable<int> betokef_ad { get; set; }
        public string oved_id { get; set; }
        public string shem_mishpaha { get; set; }
        public string shem_prati { get; set; }
        public string tarich_alia { get; set; }
        public Nullable<int> tarich_laed { get; set; }
        public string rehov { get; set; }
        public int rehovID { get; set; }
        public Nullable<int> rehov_mispar { get; set; }
        public string yeshuv { get; set; }
        public int yeshuvID { get; set; }
        public Nullable<int> mikud { get; set; }
        public Nullable<int> phone_number { get; set; }
        public string min { get; set; }
        public string mazav_mishpati { get; set; }
        public string toshav_israel { get; set; }
        public Nullable<short> haver_kupat_holim { get; set; }
        public string kupat_holim { get; set; }
        public string ben_zug_oved { get; set; }
        public Nullable<int> sug_misra { get; set; }
        public string ben_zug_id { get; set; }
        public string ben_zug_shem_mishpaha { get; set; }
        public string ben_zug_shem_prati { get; set; }
        public string SpousePrevIdNumber { get; set; }
        public Nullable<int> ben_zug_tarich_leda { get; set; }
        public Nullable<int> ben_zug_tarich_alia { get; set; }
        public Nullable<int> hachnasa_sug_misra { get; set; }
        public Nullable<short> hachnasa_nzikui_lelo_nosefet { get; set; }
        public Nullable<short> hachnasa_nzikui_nosefet { get; set; }
        public Nullable<short> kh_lelo_hafrash { get; set; }
        public Nullable<short> lelo_hafrash_lekizba { get; set; }
        public Nullable<short> neche_100 { get; set; }
        public Nullable<short> ezor_pituah { get; set; }
        public Nullable<int> ezor_pituah_taarich { get; set; }
        public Nullable<short> ole_hadash { get; set; }
        public Nullable<int> ole_hadash_taarich { get; set; }
        public Nullable<short> toshav_hozer { get; set; }
        public Nullable<int> toshav_hozer_taarich { get; set; }
        public Nullable<short> no_hacnasa { get; set; }
        public Nullable<short> ben_zug_gar_lelo_hachnasot { get; set; }
        public Nullable<short> hore_had_hori { get; set; }
        public Nullable<short> yeladim_behezkati { get; set; }
        public Nullable<short> incompetentChild { get; set; }
        public Nullable<int> yeladim_ad_18 { get; set; }
        public Nullable<int> yeladim_ad_5 { get; set; }
        public Nullable<int> yeladim_terem_19 { get; set; }
        public Nullable<short> yeladim_peotim_lelo_hezka { get; set; }
        public Nullable<int> mispar_yeldim_ad_3 { get; set; }
        public Nullable<int> mispar_yeldim_ad_1_or_2 { get; set; }
        public Nullable<short> hore_yahid { get; set; }
        public Nullable<short> yeled_lo_hezka { get; set; }
        public Nullable<short> ben_zug_mezonot { get; set; }
        public Nullable<short> ben16_katan_m18 { get; set; }
        public Nullable<short> hayal_meshuhrar { get; set; }
        public Nullable<int> taarich_giyus { get; set; }
        public Nullable<int> taarich_shihrur { get; set; }
        public Nullable<short> toar_academi { get; set; }
        public Nullable<short> hachnasa_nosefet { get; set; }
        public Nullable<int> phone_number_nosaf { get; set; }
        public decimal tt_tofes101_rec_id { get; set; }
        /// <summary>
        /// added inputs
        /// </summary>
        public string email { get; set; }
        public short haver_kibbutz { get; set; }
        public long sibat_teum_mas { get; set; }

        public Tt_Tofes101Model tt_tofes101 { get; set; }
        public List<Tt_Tofes101_HachnasaModel> tt_tofes101_hachnasa { get; set; }
        public List<Tt_Tofes101_YeladimModel> tt_tofes101_yeladim { get; set; }
        /*check employee*/
        public int Checktarich_laed { get; set; }
        public int Checkphone_number_nosaf { get; set; }
        public int Checkyeshuv { get; set; }
        public int Checkemail { get; set; }
        public int Checkshem_prati { get; set; }
        public int Checkmin { get; set; }
        public int Checkhaver_kupat_holim { get; set; }
        public int Checkkupat_holim { get; set; }
        public int Checkrehov_mispar { get; set; }
        public int Checkoved_id { get; set; }
        public int Checktoshav_israel { get; set; }
        public int Checktarich_alia { get; set; }
        public int Checkhaver_kibbutz { get; set; }
        public int Checkshem_mishpaha { get; set; }
        public int Checkmazav_mishpati { get; set; }
        public int Checkphone_number { get; set; }
        public int Checkrehov { get; set; }
        public int Checkmikud { get; set; }
        /*check employee*/
        /*check incomeType*/
        public int Checksug_misra { get; set; }
        /*check incomeType*/
        /*check spouse*/
        public int Checkben_zug_tarich_leda { get; set; }
        public int Checkben_zug_shem_prati { get; set; }
        public int Checkben_zug_id { get; set; }
        public int Checkben_zug_tarich_alia { get; set; }
        public int Checkben_zug_shem_mishpaha { get; set; }
        public int Checkben_zug_oved { get; set; }
        /*check spouse*/
        /*check otherIncomes*/
        public int Checkhachnasa_nosefet { get; set; }
        public int Checkincomehachnasa_nzikui { get; set; }
        public int Checkkh_lelo_hafrash { get; set; }
        public int Checklelo_hafrash_lekizba { get; set; }
        /*check otherIncomes*/
        /*check taxExemption*/
        public int Checkneche_100 { get; set; }
        public int Checkyeladim_behezkati { get; set; }
        public int CheckincompetentChild { get; set; }
        public int Checkben_zug_mezonot { get; set; }
        public int Checkhayal_meshuhrar { get; set; }
        public int Checktoar_academi { get; set; }
        public int Checkole_hadash { get; set; }
        public int Checkole_hadash_taarich { get; set; }
        public int Checkyeladim_peotim_lelo_hezka { get; set; }
        public int Checkezor_pituah { get; set; }
        public int Checkben16_katan_m18 { get; set; }
        public int Checkhore_yahid { get; set; }
        public int Checktaarich_shihrur { get; set; }
        public int Checktaarich_giyus { get; set; }
        public int Checkben_zug_lelo_hachnasot { get; set; }
        /*check taxExemption*/
        /* taxCoordination */
        public Nullable<long> checksibat_teum_mas { get; set; }
        /* taxCoordination */
    }
}
