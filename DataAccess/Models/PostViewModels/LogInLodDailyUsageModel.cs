﻿using System;

namespace DataAccess.Models
{
    public class LogInLodDailyUsageModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime actionDate { get; set; }
        public int totalUsersCounter { get; set; }
        public int dailyUsage { get; set; }
    }
}
