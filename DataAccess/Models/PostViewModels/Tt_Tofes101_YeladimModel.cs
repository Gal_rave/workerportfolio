﻿using System;

namespace DataAccess.Models
{
    public class Tt_Tofes101_YeladimModel
    {
        public decimal rec_id { get; set; }
        public long rashut { get; set; }
        public long mifal { get; set; }
        public Nullable<long> betokef_me { get; set; }
        public Nullable<long> betokef_ad { get; set; }
        public decimal oved_id { get; set; }
        public string shem_yeled { get; set; }
        public string id_yeled { get; set; }
        public int gar_babait { get; set; }
        public Nullable<long> Checkgar_babait { get; set; }
        public int child_benefit { get; set; }
        public Nullable<long> Checkchild_benefit { get; set; }
        public Nullable<long> tarich_leda_yeled { get; set; }
        public Nullable<long> Checktarich_leda_yeled { get; set; }
        public Nullable<long> Checkshem_yeled { get; set; }
        public Nullable<long> Checkid_yeled { get; set; }
        public int CheckCounter { get; set; }
        public string tarich_leda { get; set; }
        public string PrevIdNumber { get; set; }
        public int gender { get; set; }
        public Nullable<long> Checkgender { get; set; }

        public Tt_Tofes101_OvedModel tt_tofes101_oved { get; set; }
    }
}