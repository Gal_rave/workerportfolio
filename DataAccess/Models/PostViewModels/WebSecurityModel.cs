﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class WebSecurityModel
    {
        public WebSecurityModel()
        {
            this.success = true;
            this.exceptionId = 0;
            this.user = new UserModel();
            this.userCredentials = new UserCredentialsModel();
            this.isConfirmed = false;
        }
        public WebSecurityModel(MembershipException ex)
        {
            this.success = false;
            this.exceptionId = ex.ExceptionId;
            this.exception = ex.Message;
            //this.originalException = ex;
            this.innerException = ex.InnerException;
        }
        public WebSecurityModel(Exception ex)
        {
            this.success = false;
            this.exceptionId = -1;
            this.exception = ex.Message;
            //this.originalException = ex;
            this.innerException = ex.InnerException;
        }

        public UserCredentialsModel userCredentials { get; set; }
        public UserModel user { get; set; }
        public string userIdNumber { get; set; }
        public int userId { get; set; }
        public bool success { get; set; }
        public int exceptionId { get; set; }
        public string exception { get; set; }
        public int failLoginCount { get; set; }
        public double lockTimeLeft { get; set; }
        public bool isLocked { get; set; }
        public string passwordVerificationToken { get; set; }
        public bool isConfirmed { get; set; }
        public Exception innerException { get; set; }
        public Exception originalException { get; set; }
    }
}
