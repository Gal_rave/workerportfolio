﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class ResponseEducation
    {
        public string certificateName { get; set; }
        public string institutionName { get; set; }
        public string studySubject1 { get; set; }
        public string studySubject2 { get; set; }
        public int educationYears { get; set; }
        public string degree { get; set; }
        public DateTime degreeDate { get; set; }
    }
}
