﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class PostTokenModel
    {
        public string Token { get; set; }
        public string newPassword { get; set; }
        public string oldPassword { get; set; }
        public int passwordResetType { get; set; }
        public string idNumber { get; set; }
    }
}
