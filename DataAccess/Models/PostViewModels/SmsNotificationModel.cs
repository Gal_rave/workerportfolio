﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class SmsNotificationModel
    {
        public int ID { get; set; }
        public int userId { get; set; }
        public DateTime sendDate { get; set; }
        public string phoneNumber { get; set; }
        public string message { get; set; }
        public string response { get; set; }
        public UserModel user { get; set; }
    }
}
