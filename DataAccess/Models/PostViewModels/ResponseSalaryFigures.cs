﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class ResponseSalaryFigures
    {
        public ResponseSalaryFigures() { }
        public ResponseSalaryFigures(CultureInfo currentculture, int month = 0, int columnnumber = 0, string columntitle = null, object val = null, string monthname = null)
        {
            this.month = month;
            this.columnNumber = columnnumber;
            this.columnTitle = string.IsNullOrWhiteSpace(columntitle) ? string.Empty : columntitle;
            this.value = val;
            this.monthName = string.IsNullOrWhiteSpace(monthname) ? (month > 0 ? currentculture.DateTimeFormat.GetMonthName(month) : string.Empty) : monthname;
        }
        public ResponseSalaryFigures(int month, int columnnumber)
        {
            this.month = month;
            this.columnNumber = columnnumber;
            this.value = 0;
        }
        public ResponseSalaryFigures(int month, int columnnumber, CultureInfo currentculture)
        {
            this.month = month;
            this.columnNumber = columnnumber;
            this.monthName = month> 0 && month < 13? currentculture.DateTimeFormat.GetMonthName(month) : string.Empty;
            this.value = this.monthName;
            this.columnTitle = "חודש";
        }
        
        public int month { get; set; }
        public int columnNumber { get; set; }
        public string columnTitle { get; set; }
        public object value { get; set; }
        public string monthName { get; set; }
    }
}
