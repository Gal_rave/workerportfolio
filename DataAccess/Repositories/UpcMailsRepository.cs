﻿using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using Catel.Data;
using System;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;
using DataAccess.Extensions;

namespace DataAccess.Repositories
{
    public class UpcMailsRepository : EntityRepositoryBase<UPC_MAILS, int>, IUpcMailsRepository
    {
        public UpcMailsRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<UPC_MAILS> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.UPC_MAILS.Include(include).AsQueryable();
            }
        }
        public int saveUpcMail(DateTime currentPayMonth, string email, decimal userId, decimal userImageId, decimal municipalityId)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                ObjectParameter UPC_ID = new ObjectParameter("UPC_ID", 0);
                var result = dbContext.SAVE_UPC_MAILS(currentPayMonth, email, userId, userImageId, municipalityId, UPC_ID);
                return UPC_ID.Value.ToInt();
            }
        }
    }
}
