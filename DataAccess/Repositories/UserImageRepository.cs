﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    class UserImageRepository : EntityRepositoryBase<USERIMAGE, int>, IUserImageRepository
    {
        public UserImageRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<USERIMAGE> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.USERIMAGES.Include(include).AsQueryable();
            }
        }
        public void RemoveRange(IQueryable<USERIMAGE> source)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                dbContext.USERIMAGES.RemoveRange(source);
                dbContext.SaveChanges();
            }
        }
    }
}
