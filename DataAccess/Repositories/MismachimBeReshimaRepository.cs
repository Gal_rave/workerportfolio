﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class MismachimBeReshimaRepository : EntityRepositoryBase<MISMACHIM_BERESHIMA, int>, IMismachimBeReshimaRepository
    {
        public MismachimBeReshimaRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<MISMACHIM_BERESHIMA> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.MISMACHIM_BERESHIMA.Include(include).AsQueryable();
            }
        }
    }
}