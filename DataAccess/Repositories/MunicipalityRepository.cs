﻿using System.Data.Entity;
using System.Linq;

using Catel.Data.Repositories;
using Catel.Data;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    class MunicipalityRepository : EntityRepositoryBase<MUNICIPALITy, int>, IMunicipalityRepository
    {
        public MunicipalityRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<MUNICIPALITy> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.MUNICIPALITIES.Include(include).AsQueryable();
            }
        }
    }
}