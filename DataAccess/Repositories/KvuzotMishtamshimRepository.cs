﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class KvuzotMishtamshimRepository : EntityRepositoryBase<KVUZOT_MISHTAMSHIM, int>, IKvuzotMishtamshimRepository
    {
        public KvuzotMishtamshimRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<KVUZOT_MISHTAMSHIM> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.KVUZOT_MISHTAMSHIM.Include(include).AsQueryable();
            }
        }
    }
}