﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class ReshimotTiyugRepository : EntityRepositoryBase<RESHIMOT_TIYUG, int>, IReshimotTiyugRepository
    {
        public ReshimotTiyugRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<RESHIMOT_TIYUG> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.RESHIMOT_TIYUG.Include(include).AsQueryable();
            }
        }
    }
}