﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class ImageRepository : EntityRepositoryBase<IMAGE, int>, IImageRepository
    {
        public ImageRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<IMAGE> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.IMAGES.Include(include).AsQueryable();
            }
        }
    }
}
