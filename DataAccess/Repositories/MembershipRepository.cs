﻿using System.Data.Entity;
using System.Linq;

using Catel.Data.Repositories;
using Catel.Data;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class MembershipRepository : EntityRepositoryBase<MEMBERSHIP, int>, IMembershipRepository
    {
        public MembershipRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<MEMBERSHIP> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.MEMBERSHIPs.Include(include).AsQueryable();
            }
        }
    }
}
