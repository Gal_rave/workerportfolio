﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class SacharUserRepository : EntityRepositoryBase<SACHAR_USERS_GELEM, int>, ISacharUserRepository
    {
        public SacharUserRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<SACHAR_USERS_GELEM> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.SACHAR_USERS_GELEM.Include(include).AsQueryable();
            }
        }
    }
}