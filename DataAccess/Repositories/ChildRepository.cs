﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class ChildRepository : EntityRepositoryBase<Child, int>, IChildRepository
    {
        public ChildRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<Child> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.CHILDREN.Include(include).AsQueryable();
            }
        }
    }
}