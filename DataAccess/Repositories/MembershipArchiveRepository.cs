﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class MembershipArchiveRepository : EntityRepositoryBase<MEMBERSHIPARCHIVE, int>, IMembershipArchiveRepository
    {
        public MembershipArchiveRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<MEMBERSHIPARCHIVE> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.MEMBERSHIPARCHIVEs.Include(include).AsQueryable();
            }
        }
    }
}
