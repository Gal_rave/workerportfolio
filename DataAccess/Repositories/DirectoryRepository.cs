﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class DirectoryRepository : EntityRepositoryBase<DIRECTORy, int>, IDirectoryRepository
    {
        public DirectoryRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<DIRECTORy> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.DIRECTORIES.Include(include).AsQueryable();
            }
        }
    }
}