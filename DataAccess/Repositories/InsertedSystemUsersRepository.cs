﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    class InsertedSystemUsersRepository : EntityRepositoryBase<INSERTED_SYSTEMUSERS, int>, IInsertedSystemUsersRepository
    {
        public InsertedSystemUsersRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<INSERTED_SYSTEMUSERS> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.INSERTED_SYSTEMUSERS.Include(include).AsQueryable();
            }
        }
        public void ImportIntoSystemUsers()
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                dbContext.IMPORTINTO_SYSTEMUSERS();
            }
        }
    }
}
