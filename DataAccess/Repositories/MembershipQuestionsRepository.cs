﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class MembershipQuestionsRepository : EntityRepositoryBase<MEMBERSHIP_QUESTIONS, int>, IMembershipQuestionsRepository
    {
        public MembershipQuestionsRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<MEMBERSHIP_QUESTIONS> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.MEMBERSHIP_QUESTIONS.Include(include).AsQueryable();
            }
        }
    }
}
