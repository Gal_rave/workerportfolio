﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class ShlavimRepository : EntityRepositoryBase<SHLAVIM, int>, IShlavimRepository
    {
        public ShlavimRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<SHLAVIM> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.SHLAVIMs.Include(include).AsQueryable();
            }
        }
    }
}
