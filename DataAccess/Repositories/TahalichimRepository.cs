﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class TahalichimRepository : EntityRepositoryBase<TAHALICHIM, int>, ITahalichimRepository
    {
        public TahalichimRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<TAHALICHIM> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.TAHALICHIMs.Include(include).AsQueryable();
            }
        }
    }
}