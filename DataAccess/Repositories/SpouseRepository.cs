﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class SpouseRepository : EntityRepositoryBase<SPOUSE, int>, ISpouseRepository
    {
        public SpouseRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<SPOUSE> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.SPOUSEs.Include(include).AsQueryable();
            }
        }
    }
}
