﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class System_citiesRepository : EntityRepositoryBase<SYSTEM_CITIES, int>, ISystem_citiesRepository
    {
        public System_citiesRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<SYSTEM_CITIES> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.SYSTEM_CITIES.Include(include).AsQueryable();
            }
        }
        public void delete_systemcities()
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                dbContext.DELETE_SYSTEMCITIES();
            }
        }
    }
}
