﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    class smsNotificationRepository : EntityRepositoryBase<SMSNOTIFICATION, int>, IsmsNotificationRepository
    {
        public smsNotificationRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<SMSNOTIFICATION> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.SMSNOTIFICATIONS.Include(include).AsQueryable();
            }
        }
    }
}
