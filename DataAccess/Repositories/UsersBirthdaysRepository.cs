﻿using System.Data.Entity;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;
using System.Linq;
using Catel.Data;

namespace DataAccess.Repositories
{
    public class UsersBirthdaysRepository : EntityRepositoryBase<USERS_BIRTHDAYS, int>, IUsersBirthdaysRepository
    {
        public UsersBirthdaysRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<USERS_BIRTHDAYS> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.USERS_BIRTHDAYS.Include(include).AsQueryable();
            }
        }
    }
}
