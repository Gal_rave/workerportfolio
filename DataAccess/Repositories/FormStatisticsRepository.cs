﻿using System.Data.Entity;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;
using System.Linq;
using Catel.Data;

namespace DataAccess.Repositories
{
    public class FormStatisticsRepository : EntityRepositoryBase<FORM_STATISTICS, int>, IFormStatisticsRepository
    {
        public FormStatisticsRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<FORM_STATISTICS> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.FORM_STATISTICS.Include(include).AsQueryable();
            }
        }
    }
}
