﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class MishtamsheiKvuzaRepository : EntityRepositoryBase<MISHTAMSHEI_KVUZA, int>, IMishtamsheiKvuzaRepository
    {
        public MishtamsheiKvuzaRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<MISHTAMSHEI_KVUZA> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.MISHTAMSHEI_KVUZA.Include(include).AsQueryable();
            }
        }
    }
}