﻿using System.Data.Entity;
using System.Linq;
using Catel.Data;

using Catel.Data.Repositories;
using DataAccess.Models;
using DataAccess.Interfaces;

namespace DataAccess.Repositories
{
    public class CityRepository : EntityRepositoryBase<CITy, int>, ICityRepository
    {
        public CityRepository(DbContext dbContext)
            : base(dbContext) { }
        public IQueryable<CITy> Include(string include)
        {
            using (var dbContextManager = DbContextManager<PortfolioEntities>.GetManager())
            {
                var dbContext = dbContextManager.Context;
                return dbContext.CITIES.Include(include).AsQueryable();
            }
        }
    }
}
