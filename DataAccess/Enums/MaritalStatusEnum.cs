﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Enums
{
    public enum MaritalStatusEnum
    {
        Single = 1,
        Married = 2,
        Divorcee = 3,
        Widow = 4,
        Separated = 5
    }
}
