﻿namespace DataAccess.Enums
{
    public enum MailTypeEnum
    {
        PasswordReset = 1,
        SiteRegisteration = 2,
        SystemRegisteration = 3,
        General = 4
    }
}
