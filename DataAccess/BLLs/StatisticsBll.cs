﻿using System.Collections.Generic;
using System.Linq;
using System;

using Catel.Data;
using DataAccess.Interfaces;
using DataAccess.Models;
using Logger;
using DataAccess.Mappers;
using DataAccess.Extensions;

namespace DataAccess.BLLs
{
    public class StatisticsBll : BllExtender
    {
        public List<FORM_STATISTICS> GetFormStatisticsForMunicipality(UserSearchModel model)
        {
            try
            {
                if (model.maxImmunity <= 40)
                    throw new UnauthorizedAccessException();

                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IFormStatisticsRepository fsRepository = session.GetRepository<IFormStatisticsRepository>();
                    IQueryable<FORM_STATISTICS> Qforms = fsRepository.GetQuery(fs => !fs.CREATEDDATE.HasValue || (fs.CREATEDDATE.HasValue && fs.CREATEDDATE.Value.Year == model.searchDate.Year));
                    return model.maxImmunity > 70 && model.page > 1 ? Qforms.Where(fs => fs.MUNICIPALITYID == model.page).ToList() : Qforms.Where(fs => fs.MUNICIPALITYID == model.municipalityId).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<ShortUserModel> GetPopulationUsers(List<string> ids, int municipalityId)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    return userRepository.GetQuery(u=> u.MUNICIPALITIES.FirstOrDefault(m => m.ID == municipalityId) != null && ids.Contains(u.CALCULATEDIDNUMBER) && u.EMAIL != null && u.EMAIL.Length > 1)
                        .ToList().Select(u=> u.ShortMap()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<ShortUserModel> GroupEmailSearch(UserSearchModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                IQueryable<USER> users = userRepository.GetAll();

                ///filter only users from same municipality
                users = users.Where(u => u.MUNICIPALITIES.FirstOrDefault(m => m.ID == model.municipalityId) != null && u.EMAIL != null && u.EMAIL.Length > 2);
                ///search idnumber
                if (!string.IsNullOrWhiteSpace(model.searchIdNumber) && model.searchIdNumber.Length > 0)
                {
                    model.searchIdNumber = model.searchIdNumber.ToLower().Trim();
                    users = users.Where(u => u.IDNUMBER.StartsWith(model.searchIdNumber) || u.IDNUMBER.Contains(model.searchIdNumber) || u.IDNUMBER.EndsWith(model.searchIdNumber) || u.IDNUMBER == model.searchIdNumber);
                }
                ///seach email
                if (!string.IsNullOrWhiteSpace(model.cell) && model.cell.Length > 0)
                {
                    model.cell = model.cell.ToLower().Trim();
                    users = users.Where(u => u.EMAIL.StartsWith(model.cell) || u.EMAIL.Contains(model.cell) || u.EMAIL.EndsWith(model.cell) || u.EMAIL == model.cell);
                }
                ///search name (first AND last)
                if (!string.IsNullOrWhiteSpace(model.firstName) && model.firstName.Length > 0)
                {
                    model.firstName = model.firstName.ToLower().Trim();
                    users = users.Where(u => u.FIRSTNAME.StartsWith(model.firstName) || u.FIRSTNAME.Contains(model.firstName) || u.FIRSTNAME.EndsWith(model.firstName) || u.FIRSTNAME == model.firstName ||
                        u.LASTNAME.StartsWith(model.firstName) || u.LASTNAME.Contains(model.firstName) || u.LASTNAME.EndsWith(model.firstName) || u.LASTNAME == model.firstName);
                }
                ///search user groups
                if (!string.IsNullOrWhiteSpace(model.searchMunicipalitiesId) && model.searchMunicipalitiesId.ToInt() > 0)
                {
                    int group = model.searchMunicipalitiesId.ToInt();
                    users = users.Where(u => u.GROUPS.FirstOrDefault(g => g.ID == group) != null);
                }

                return users.OrderBy(c => c.ID).Take(500).ToList().Select(u => u.ShortMap()).ToList();
            }
        }
        public List<GroupModel> GetPopulationGroups()
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IGroupsRepository groupsRepository = session.GetRepository<IGroupsRepository>();
                    return groupsRepository.GetQuery(g => g.IMMUNITY == 0).ToList().Select(g => g.Map(false)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
    }
}
