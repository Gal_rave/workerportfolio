﻿using System.Collections.Generic;
using System.Linq;

using Catel.Data;
using DataAccess.Interfaces;
using DataAccess.Mappers;
using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.BLLs
{
    public class CityBll : BllExtender
    {
        public List<CityModel> CitySearch(string query, int counter = 10)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ICityRepository cityRepository = session.GetRepository<ICityRepository>();

                return cityRepository
                        .GetQuery(c => c.NAME.StartsWith(query) || c.NAME.Contains(query) || c.NAME == query || c.ENGLISHNAME.StartsWith(query) || c.ENGLISHNAME.Contains(query) || c.ENGLISHNAME == query)
                        .OrderBy(c => c.NAME)
                        .Take(counter)
                        .ToList().Select(c => c.Map()).ToList();
            }
        }
        public CityModel GetCity(string query)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ICityRepository cityRepository = session.GetRepository<ICityRepository>();

                return cityRepository.FirstOrDefault(c => c.NAME == query).Map(true);
            }
        }
        public List<StreetModel> StreetSearch(int cityID, string query, int counter = 10)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ICityRepository cityRepository = session.GetRepository<ICityRepository>();
                CITy city = cityRepository.FirstOrDefault(c => c.ID == cityID);
                if (city != null)
                    return city.STREETS
                            .Where(s => s.NAME.StartsWith(query) || s.NAME.Contains(query) || s.NAME == query || s.NAME.EndsWith(query))
                            .OrderBy(s => s.NAME)
                            .Take(counter)
                            .ToList().Select(s => s.Map(false)).ToList();

                return null;
            }
        }
        public List<MunicipalityModel> GetMunicipalities(string query, int counter = 10)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                return municipalityRepository
                    .GetQuery(m => m.ACTIVE && (m.NAME.StartsWith(query) || m.NAME.Contains(query) || m.NAME == query || m.NAME.EndsWith(query)))
                        .OrderBy(m => m.NAME)
                        .Take(counter)
                        .ToList().Select(c => c.Map(false)).ToList();
            }
        }
        public void ImportCitiesFromWS(List<SYSTEM_CITIES> cities)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ISystem_citiesRepository systemCitiesRepository = session.GetRepository<ISystem_citiesRepository>();

                systemCitiesRepository.delete_systemcities();
                cities.ForEach(c =>
                    systemCitiesRepository.Add(c)
                );
                session.SaveChanges();
            }
        }
    }
}
