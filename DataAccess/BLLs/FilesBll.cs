﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Data.Entity;

using Catel.Data;
using DataAccess.Interfaces;
using DataAccess.Mappers;
using DataAccess.Models;
using DataAccess.Extensions;
using Logger;
using System.Text;
using System.Text.RegularExpressions;
using Catel.Data.Repositories;
using DataAccess.Enums;

namespace DataAccess.BLLs
{
    public class FilesBll : BllExtender
    {
        #region class vars
        private Regex directoryRgx = new Regex("[^a-zA-Z0-9א-ת]");
        private Regex imageRgx = new Regex("[^a-zA-Z0-9א-ת.]");
        #endregion

        public DirectoryModel DirectoryStructure(int municipalityId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IDirectoryRepository directoryRepository = session.GetRepository<IDirectoryRepository>();

                    DirectoryModel model = getDirectoryStructure(directoryRepository, municipalityId);
                    if (model != null)
                        return model;
                    IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                    return CreateDirectory(new DirectoryModel { name = directoryRgx.Replace(string.Format("ראשי_{0}", municipalityRepository.First(m=> m.ID == municipalityId).NAME), "_"), parentId = null, municipalityId = municipalityId });
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public DirectoryModel UpdateDirectory(DirectoryModel model)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IDirectoryRepository directoryRepository = session.GetRepository<IDirectoryRepository>();
                    DIRECTORy directory = directoryRepository.First(d => d.ID == model.ID);
                    directory.NAME = directoryRgx.Replace(model.name, "_");
                    directory.PARENTID = model.parentId;

                    doSave<DIRECTORy>(session, directory);

                    return getDirectoryStructure(directoryRepository, model.municipalityId.ToInt());
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public DirectoryModel CreateDirectory(DirectoryModel model)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IDirectoryRepository directoryRepository = session.GetRepository<IDirectoryRepository>();
                    DIRECTORy directory = model.Map();
                    directory.NAME = directoryRgx.Replace(directory.NAME, "_");
                    directoryRepository.Add(directory);
                    doSave<DIRECTORy>(session, directory);
                    return getDirectoryStructure(directoryRepository, model.municipalityId.ToInt());
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public DirectoryModel DeleteDirectory(DirectoryModel model)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IDirectoryRepository directoryRepository = session.GetRepository<IDirectoryRepository>();
                    DIRECTORy directory = directoryRepository.First(d=> d.ID == model.ID);
                    directoryRepository.Delete(directory);
                    doSave<DIRECTORy>(session, directoryRepository.GetAll());
                    return getDirectoryStructure(directoryRepository, model.municipalityId.ToInt());
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public ImageModel GetImageById(int imgId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IImageRepository imageRepository = session.GetRepository<IImageRepository>();
                    return imageRepository.First(i => i.ID == imgId).Map();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public ImageModel GetImageByGuidName(string Guid)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IImageRepository imageRepository = session.GetRepository<IImageRepository>();
                    return imageRepository.First(i => !i.DIRECTORYID.HasValue && i.USERID.HasValue && i.NAME == Guid).Map();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public ImageModel AddImage(ImageModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IImageRepository imageRepository = session.GetRepository<IImageRepository>();
                    IMAGE image = model.Map();
                    imageRepository.Add(image);
                    doSave<IMAGE>(session, image);
                    return image.Map();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public ImageModel UpdateImage(ImageModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IImageRepository imageRepository = session.GetRepository<IImageRepository>();
                    IMAGE image = imageRepository.First(i=> i.ID == model.ID);
                    image.NAME = imageRgx.Replace(model.name, "_");
                    image.DIRECTORYID = model.directoryId;

                    doSave<IMAGE>(session, image);
                    return image.Map();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public bool DeleteImage(int imageId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IImageRepository imageRepository = session.GetRepository<IImageRepository>();
                    IMAGE image = imageRepository.First(i=> i.ID == imageId);
                    imageRepository.Delete(image);
                    session.SaveChanges();

                    return true;
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public List<HtmlTemplateModel> GetEmailTemplates(int municipalityId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IHtmlTemplatesRepository htmlTemplatesRepository = session.GetRepository<IHtmlTemplatesRepository>();
                    return htmlTemplatesRepository.GetQuery(e => e.MUNICIPALITYID == municipalityId && e.TYPE == (int)TemplateTypeEnum.Email).OrderByDescending(e=> e.ID).ToList().Select(e => e.Map()).ToList();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public List<HtmlTemplateModel> GetHtmlTemplates(int municipalityId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IHtmlTemplatesRepository htmlTemplatesRepository = session.GetRepository<IHtmlTemplatesRepository>();
                    return htmlTemplatesRepository.GetQuery(e => e.MUNICIPALITYID == municipalityId && e.TYPE == (int)TemplateTypeEnum.Page).OrderByDescending(e => e.ID).ToList().Select(e => e.Map()).ToList();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public HtmlTemplateModel UpdateHtmlTemplate(HtmlTemplateModel model, int userId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IHtmlTemplatesRepository htmlTemplatesRepository = session.GetRepository<IHtmlTemplatesRepository>();
                    HTMLTEMPLATE htmltemplate;
                    if (model.ID > 0)
                    {
                        htmltemplate = htmlTemplatesRepository.First(e=> e.ID == model.ID);
                        htmltemplate.Map(model, userId);
                        htmlTemplatesRepository.Update(htmltemplate);
                    }
                    else
                    {
                        htmltemplate = model.Map(userId);
                        htmlTemplatesRepository.Add(htmltemplate);
                    }
                    doSave<HTMLTEMPLATE>(session, htmltemplate);
                    return htmltemplate.Map();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public bool DeleteHtmlTemplate(HtmlTemplateModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IHtmlTemplatesRepository htmlTemplatesRepository = session.GetRepository<IHtmlTemplatesRepository>();
                    htmlTemplatesRepository.Delete(htmlTemplatesRepository.First(e=> e.ID == model.ID));
                    session.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public HtmlTemplateModel GetTemplateById(HtmlTemplateModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IHtmlTemplatesRepository htmlTemplatesRepository = session.GetRepository<IHtmlTemplatesRepository>();
                    return htmlTemplatesRepository.First(e => e.TYPE == (int)TemplateTypeEnum.Page && e.MUNICIPALITYID == model.municipalityId && e.ID == model.ID).Map(false);
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public HtmlTemplateModel GetTemplateById(int TemplateID)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IHtmlTemplatesRepository htmlTemplatesRepository = session.GetRepository<IHtmlTemplatesRepository>();
                    return htmlTemplatesRepository.First(e => e.ID == TemplateID).Map(false);
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }

        #region private
        private DirectoryModel getDirectoryStructure(IDirectoryRepository directoryRepository, int municipalityId)
        {
            return directoryRepository.Include("DIRECTORIES1").Include("IMAGES").OrderBy(d=> d.ID).FirstOrDefault(d => d.MUNICIPALITYID == municipalityId && !d.PARENTID.HasValue).Map();
        }
        #endregion
    }
}
