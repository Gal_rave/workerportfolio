﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Catel.Data;

using Logger;
using DataAccess.Extensions;
using DataAccess.Interfaces;
using DataAccess.Models;
using DataAccess.Mappers;
using DataAccess.EqualityComparers;

namespace DataAccess.BLLs
{
    public class RegisterBll : BllExtender
    {
        public List<SystemUserModel> CheckUserBeforeRegister(RegisterUserModel model)
        {
            Regex phonenumberRegex = new Regex(@"[\D\/]");

            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ISystemUsersRepository inserted = session.GetRepository<ISystemUsersRepository>();
                IQueryable<SYSTEM_USERS> users = inserted.GetQuery(i => i.IDNUMBER == model.idNumber || i.FULLIDNUMBER == model.idNumber);
                if (!string.IsNullOrWhiteSpace(model.initialPassword))
                {
                    users = users.Where(i => i.INITIALPASSWORD == model.initialPassword);
                }
                else if (!string.IsNullOrWhiteSpace(model.cell))
                {
                    model.cell = phonenumberRegex.Replace(model.cell, "");
                    users = users.Where(i => i.CELLNUMBER == model.cell);
                }
                else if (!string.IsNullOrWhiteSpace(model.email))
                {
                    users = users.Where(i => i.EMAIL == model.email);
                }
                List<string> municipalities = model.municipalities.Select(m => m.ID.ToString()).ToList();
                users = users.Where(i => municipalities.Contains(i.MUNICIPALITYID));

                return users.ToList().Select(i => i.Map()).ToList();
            }
        }
        public List<SystemUserModel> AttachMunicipalities(List<SystemUserModel> users)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                users.ForEach(su =>
                {
                    int municipalityId = su.municipalityId.ToInt();
                    su.municipality = municipalityRepository.FirstOrDefault(m => m.ID == municipalityId).Map(false);
                });
            }
            return users;
        }
        public bool CheckRegisterUser(RegisterUserModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ISystemUsersRepository systemUsersRepository = session.GetRepository<ISystemUsersRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                string municipalityId = model.municipality.ID.ToString();

                if (userRepository.FirstOrDefault(u => u.IDNUMBER == model.idNumber) != null)
                    throw new MembershipException("UserExistsException", 103);
                if (systemUsersRepository.FirstOrDefault(i => (i.IDNUMBER == model.idNumber || i.FULLIDNUMBER == model.idNumber) && i.MUNICIPALITYID == municipalityId) == null)
                    throw new MembershipException("NullSystemUserException", 104);

                return true;
            }
        }
        public List<SystemUserModel> RegisterInsertedIntoUsers(RegisterUserModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ISystemUsersRepository systemUsersRepository = session.GetRepository<ISystemUsersRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                List<string> municipalities = model.municipalities.Select(m => m.ID).ToStringList();

                if (userRepository.FirstOrDefault(u => u.IDNUMBER == model.idNumber) != null)
                    throw new MembershipException("UserExistsException", 103);
                if (systemUsersRepository.FirstOrDefault(i => (i.IDNUMBER == model.idNumber || i.FULLIDNUMBER == model.idNumber) && municipalities.Contains(i.MUNICIPALITYID)) == null)
                    throw new MembershipException("NullSystemUserException", 104);

                return systemUsersRepository.GetQuery(i => i.IDNUMBER == model.idNumber || i.FULLIDNUMBER == model.idNumber).ToList().Select(su => su.Map()).ToList();
            }
        }
        public void InsertMunicipalitiesToUser(string idNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ISystemUsersRepository systemUsersRepository = session.GetRepository<ISystemUsersRepository>();
                systemUsersRepository.InsertMunicipalitiesToUser(idNumber);
            }
        }
        public List<SystemUserModel> GetBulkUsersToInternalRegister(int municipality)
        {
            IQueryable<SYSTEM_USERS> system_users;
            List<string> fullIdNumbers;
            List<string> ExceptList;
            List<string> existing;

            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    ISystemUsersRepository systemUsersRepository = session.GetRepository<ISystemUsersRepository>();
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();

                    existing = userRepository.GetAll().Select(u => u.IDNUMBER).Distinct().ToList();

                    string _municipality = municipality.ToString();

                    fullIdNumbers = systemUsersRepository.GetQuery(su => su.MUNICIPALITYID == _municipality).Select(u => u.FULLIDNUMBER).Distinct().ToList();
                    if (fullIdNumbers.Count == 0)
                        return new List<SystemUserModel>();

                    MunicipalityModel mm = municipalityRepository.First(m => m.ID == municipality).Map(false);
                    ExceptList = fullIdNumbers.Except(existing, new IdNumberIEqualityComparer()).ToList();
                    system_users = systemUsersRepository.GetQuery(su => su.MUNICIPALITYID == _municipality && ExceptList.Contains(su.FULLIDNUMBER));

                    return system_users.ToList().Select(su => su.Map(mm)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<SystemUserModel> GetBulkUsersToRegister(int municipality, List<decimal> TerminationCauseExitList)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    string _municipality = municipality.ToString();
                    IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                    MunicipalityModel mm = municipalityRepository.First(m => m.ID == municipality && m.ACTIVE && m.PERFORMUPDATE).Map(false);
                    DateTime firstOfYear = new DateTime(DateTime.Now.Year, 1, 1);

                    ISystemUsersRepository systemUsersRepository = session.GetRepository<ISystemUsersRepository>();
                    List<string> fullIdNumbers = systemUsersRepository.GetQuery(su => su.MUNICIPALITYID == _municipality && 
                                                        (
                                                            !su.TERMINATIONCAUSE.HasValue || 
                                                            (
                                                                su.TERMINATIONCAUSE.HasValue &&
                                                                !TerminationCauseExitList.Contains(su.TERMINATIONCAUSE.Value) &&
                                                                su.TERMINATIONDATE.HasValue && su.TERMINATIONDATE.Value >= firstOfYear
                                                            )
                                                        )
                                                    )
                                                        .Select(u => u.FULLIDNUMBER).Distinct().ToList();
                    if (fullIdNumbers.Count == 0)
                        return new List<SystemUserModel>();

                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    List<string> existing = userRepository.GetAll().Select(u => u.IDNUMBER).Distinct().ToList();
                    List<string> ExceptList = fullIdNumbers.Except(existing, new IdNumberIEqualityComparer()).ToList();
                    IQueryable<SYSTEM_USERS> system_users = systemUsersRepository.GetQuery(su => su.MUNICIPALITYID == _municipality && ExceptList.Contains(su.FULLIDNUMBER));
                    return system_users.ToList().Select(su => su.Map(mm)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public List<int> GetAllmunicipalities(bool active = false, bool? isProcess = null)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                IQueryable<MUNICIPALITy> municipality;
                if (active)
                    municipality = municipalityRepository.GetQuery(m => m.ACTIVE == active && m.PERFORMUPDATE);
                else
                    municipality = municipalityRepository.GetQuery(m => m.PERFORMUPDATE);
                if (isProcess.HasValue && isProcess.Value)
                    municipality = municipality.Where(m => m.ALLOWPROCESS);

                return municipality.Select(m => m.ID).ToList().ToIntList();
            }
        }
        public List<int> GetAllRashuyot(bool active = false)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                return municipalityRepository.GetQuery(m => m.ACTIVE == active && m.RASHUT.HasValue).Select(m => m.RASHUT.Value).ToList();
            }
        }
        public List<SpouseModel> GetNewUsers()
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                return userRepository.GetQuery(u => u.WELLCOMEMAIL == false && u.INCOMETYPE != null && u.EMAIL != null).ToList().Take(1000).OrderBy(u => u.ID).Select(u => u.MapToSpouseModel(false)).ToList();
            }
        }
        public void ChengeNewUserState(List<decimal> userIds)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                List<USER> users = userRepository.GetQuery(u => userIds.Contains(u.ID)).ToList();
                users.ToList().ForEach(u =>
                    {
                        u.INCOMETYPE = null;
                        u.WELLCOMEMAIL = true;
                        userRepository.Update(u);
                    }
                );
                session.SaveChanges();
            }
        }
        public CreateUserModel CheckInitialUser(CryptoModel cryptoModel)
        {
            CreateUserModel user = new CreateUserModel();
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                USER us = userRepository.FirstOrDefault(u => u.IDNUMBER == cryptoModel.idNumber);
                if (us != null)
                {
                    if (us.INITIALCHECK == true)
                    {
                        user.idNumber = us.IDNUMBER;
                        user.cell = !string.IsNullOrWhiteSpace(us.CELL) ? "1" : "0";
                        user.email = !string.IsNullOrWhiteSpace(us.EMAIL) ? "1" : "0";
                        return user;
                    }
                    throw new MembershipException("InitialCheckException", 201);
                }

                throw new MembershipException("IdNumberNullException", 200);
            }
        }
        public void RemoveEmptyUsers()
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                userRepository.RemoveEmptyUsers();
            }
        }
        public void ConnectEmailsByMunicipality()
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IEmailByMunicipalityRepository emailByMunicipalityRepository = session.GetRepository<IEmailByMunicipalityRepository>();
                emailByMunicipalityRepository.ConnectEmailByMunicipality();
            }
        }
        public bool SaveRashutNumber(int mun, int rashutNumber)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IMunicipalityRepository munRepository = session.GetRepository<IMunicipalityRepository>();
                    MUNICIPALITy mncplity = munRepository.FirstOrDefault(m => m.ID == mun);
                    if (mncplity != null && rashutNumber > 0)
                    {
                        mncplity.RASHUT = rashutNumber;
                        session.SaveChanges();
                    }
                    else
                    {
                        Log.LogError(new { msg = "saveRashutNumber", _mun = mun, _rashutNumber = rashutNumber }, "saveRashutNumber");
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return ex.LogError(false);
            }
        }
        public string GetSynerionLinkByMunicipalityId(int currMunicipalityId)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                    string synerionLink = municipalityRepository.FirstOrDefault(m => m.ID == currMunicipalityId).SYNERION_LINK;
                    return synerionLink != null ? synerionLink : null;
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
                return null;
            }
        }
        public bool UpdateUserMunicipalityContact(WSPostExtensionModel model)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    IUpdateUserMunicipalityContactRepository uumcRepository = session.GetRepository<IUpdateUserMunicipalityContactRepository>();
                    USER user = userRepository.First(u => u.IDNUMBER == model.idNumber);
                    List<string> joindData = new List<string>();
                    ///updateting the municipality email
                    if (model.ToSendType == 1)
                    {
                        //model.currentMunicipalityId
                        EMAILBYMUNICIPALITY ebm = new EMAILBYMUNICIPALITY
                        {
                            EMAIL = model.Email,
                            MUNICIPALITYID = model.currentMunicipalityId,
                            USERID = user.ID,
                            IDNUMBER = user.CALCULATEDIDNUMBER
                        };
                        user.EMAILBYMUNICIPALITies.Add(ebm);
                    }

                    ///remove old data to update => allow only 1 update data per user per municipality per type to await execution plan
                    user.UPDATEUSERMUNICIPALITYCONTACTs.Where(u => u.MUNICIPALITYID == model.currentMunicipalityId && u.TYPE == (model.ToSendType == 1) && !u.UPDATESTATUS)
                        .ToList().ForEach(u => uumcRepository.Delete(u));

                    UPDATEUSERMUNICIPALITYCONTACT uumc = new UPDATEUSERMUNICIPALITYCONTACT
                    {
                        CELL = model.Cell,
                        CREATEDDATE = DateTime.Now,
                        EMAIL = model.Email,
                        MUNICIPALITYID = model.currentMunicipalityId,
                        UPDATESTATUS = false,
                        USERID = user.ID,
                        TYPE = (model.ToSendType == 1)///false => cell, true => email
                    };
                    user.UPDATEUSERMUNICIPALITYCONTACTs.Add(uumc);

                    session.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return ex.LogError(false);
            }
        }
        public List<UPDATEUSERMUNICIPALITYCONTACT> GetOutStandingUUMC()
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUpdateUserMunicipalityContactRepository uumcRepository = session.GetRepository<IUpdateUserMunicipalityContactRepository>();
                return uumcRepository.Include("MUNICIPALITy").Include("USER").Where(u => !u.UPDATESTATUS).ToList();
            }
        }
        public void UpdateUUMCStatus(UPDATEUSERMUNICIPALITYCONTACT model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUpdateUserMunicipalityContactRepository uumcRepository = session.GetRepository<IUpdateUserMunicipalityContactRepository>();
                uumcRepository.Update(model);
                session.SaveChanges();
            }
        }
        public List<UserModel> GetUsersBirthdays(int? day = null)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUsersBirthdaysRepository usersBirthdaysRepository = session.GetRepository<IUsersBirthdaysRepository>();

                    DateTime tody = DateTime.Now;
                    if (day.HasValue && day.Value > 0)
                        tody = new DateTime(tody.Year, tody.Month, day.Value);

                    return usersBirthdaysRepository
                        .GetQuery(u =>
                            !u.ISDELETED && !u.ISSPOUSE && u.EMAIL != null && u.BIRTHDATE.HasValue && u.BIRTHDATE.Value.Month == tody.Month && u.BIRTHDATE.Value.Day == tody.Day
                        )
                        .ToList().Select(u => u.Map()).ToList();

                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
    }
}
