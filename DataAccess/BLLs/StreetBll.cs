﻿using System.Collections.Generic;
using System.Linq;

using Catel.Data;
using DataAccess.Interfaces;
using DataAccess.Mappers;
using DataAccess.Models;
using DataAccess.Extensions;
using AsyncHelpers;
using System.Threading;

namespace DataAccess.BLLs
{
    public class StreetBll : BllExtender
    {
        public List<StreetModel> StreetSearch(string cityQuery, string streetQuery, int counter = 10)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IStreetRepository streetRepository = session.GetRepository<IStreetRepository>();
                return streetRepository.GetQuery(s => s.NAME.StartsWith(streetQuery) || s.NAME.Contains(streetQuery) || s.NAME == streetQuery)
                        .Where(s => s.CITIES.FirstOrDefault(c => c.NAME.StartsWith(cityQuery) || c.NAME.Contains(cityQuery) || c.NAME == cityQuery ||
                                     c.ENGLISHNAME.StartsWith(cityQuery) || c.ENGLISHNAME.Contains(cityQuery) || c.ENGLISHNAME == cityQuery) != null ||
                                     s.CITIES.Count == 0 || s.CITIES.FirstOrDefault(c => c.ID == 0) != null
                        )
                        .OrderBy(s => s.NAME).Take(counter)
                        .ToList().Select(s => s.Map(false)).ToList();
            }
        }
        public bool AddMunicipalityToUser(MunicipalityModel Mun, UserModel Ur)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                USER user = userRepository.First(u => u.IDNUMBER == Ur.idNumber && !u.ISDELETED);
                MUNICIPALITy municipality = municipalityRepository.First(m => m.ID == Mun.ID);
                if (user.MUNICIPALITIES.FirstOrDefault(m => m.ID == Mun.ID) == null)
                {
                    user.MUNICIPALITIES.Add(municipality);
                    doSave<USER>(session, userRepository.GetAll());
                }
                return true;
            }
        }
        public bool AddMunicipalityToUser(string municipalityName, UserModel User)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                MUNICIPALITy municipality = municipalityRepository.FirstOrDefault(m => m.NAME == municipalityName);

                if (municipality != null && municipality.USERS.FirstOrDefault(u => u.ID == User.ID) == null)
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    municipality.USERS.Add(userRepository.First(u => u.IDNUMBER == User.idNumber && !u.ISDELETED));
                    doSave<MUNICIPALITy>(session, municipalityRepository.GetAll());
                    return true;
                }
            }
            return false;
        }
        public void ImportStreetsFromWS(List<SYSTEM_STREETS> streets)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ISystem_streetsRepository systemStreetsRepository = session.GetRepository<ISystem_streetsRepository>();

                systemStreetsRepository.delete_systemstreets();
                foreach (IEnumerable<SYSTEM_STREETS> systemStreets in streets.Chunk(1500))
                {
                    AsyncFunctionCaller.RunAsync(() =>
                    {
                        saveSystemStreets(systemStreets);
                    });
                }
                Thread.Sleep(100000);
                systemStreetsRepository.UpdateCitiesStreets();
            }
        }
        public SYSTEM_STREETS GetSystemStreet(int cityID, string streetName)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ISystem_streetsRepository systemStreetsRepository = session.GetRepository<ISystem_streetsRepository>();

                return systemStreetsRepository.FirstOrDefault(st => st.SETTLEMENTSYMBOL == cityID &&
                                                (st.STREETNAME == streetName || st.STREETNAME.StartsWith(streetName) || st.STREETNAME.EndsWith(streetName) || st.STREETNAME.Contains(streetName)));
            }
        }

        private void saveSystemStreets(IEnumerable<SYSTEM_STREETS> streets)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ISystem_streetsRepository systemStreetsRepository = session.GetRepository<ISystem_streetsRepository>();

                streets.ToList().ForEach(s =>
                    systemStreetsRepository.Add(s)
                );
                session.SaveChanges();
            }
        }
    }
}