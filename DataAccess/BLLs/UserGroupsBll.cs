﻿using System.Linq;
using System.Collections.Generic;

using Catel.Data;
using DataAccess.Interfaces;
using DataAccess.Models;
using DataAccess.Mappers;
using DataAccess.Extensions;

namespace DataAccess.BLLs
{
    public class UserGroupsBll : BllExtender
    {
        public void AddUserToGroup(string idNumber, int groupId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IGroupsRepository groupsRepository = session.GetRepository<IGroupsRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                USER user = userRepository.First(u => u.IDNUMBER == idNumber);
                if (user.GROUPS.FirstOrDefault(g => g.ID == groupId) == null)
                {
                    user.GROUPS.Add(groupsRepository.First(g => g.ID == groupId));
                    session.SaveChanges();
                }
            }
        }
        public void AddUserToGroup(int userId, int groupId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IGroupsRepository groupsRepository = session.GetRepository<IGroupsRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                USER user = userRepository.First(u => u.ID == userId);
                if (user.GROUPS.FirstOrDefault(g => g.ID == groupId) == null)
                {
                    user.GROUPS.Add(groupsRepository.First(g => g.ID == groupId));
                    doSave<USER>(session, user);
                }
            }
        }
        public bool AddUserToGroup(string idNumber, string groupName)
        {
            groupName = groupName.ToUpper();
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IGroupsRepository groupsRepository = session.GetRepository<IGroupsRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                GROUP group = groupsRepository.FirstOrDefault(g=> g.NAME == groupName);
                USER user = userRepository.FirstOrDefault(u => u.IDNUMBER == idNumber);
                if (group == null || user == null)
                    return false;
                if (user.GROUPS.FirstOrDefault(g => g.NAME == groupName) != null)
                    return true;
                ///add new group to user
                user.GROUPS.Add(group);
                doSave<USER>(session, userRepository.GetAll());
                return true;
            }
        }
        public bool RemoveUserFromGroup(string idNumber, string groupName)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IGroupsRepository groupsRepository = session.GetRepository<IGroupsRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                GROUP group = groupsRepository.FirstOrDefault(g => g.NAME == groupName);
                USER user = userRepository.FirstOrDefault(u => u.IDNUMBER == idNumber);
                if (group == null || user == null)
                    return false;
                if (user.GROUPS.FirstOrDefault(g => g.NAME == groupName) != null)
                {
                    ///add new group to user
                    user.GROUPS.Remove(group);
                    doSave<USER>(session, userRepository.GetAll());
                    return true;
                }
                return false;
            }
        }
        public bool RemoveUserFromGroup(string idNumber, int groupId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                USER user = userRepository.FirstOrDefault(u => u.IDNUMBER == idNumber);
                GROUP group = user.GROUPS.FirstOrDefault(g => g.ID == groupId);

                if (user == null)
                    return false;
                if (group == null)
                    return true;

                ///add new group to user
                user.GROUPS.Remove(group);
                session.SaveChanges();
                return true;
            }
        }
        public void AddUserToGroup(USER user, int groupId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IGroupsRepository groupsRepository = session.GetRepository<IGroupsRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                
                if (user.GROUPS.FirstOrDefault(g => g.ID == groupId) == null)
                {
                    user.GROUPS.Add(groupsRepository.First(g => g.ID == groupId));

                    doSave<USER>(session, userRepository.GetAll());
                }
            }
        }
        public void DeleteUserGroup(int userId, int groupId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IGroupsRepository groupsRepository = session.GetRepository<IGroupsRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                USER user = userRepository.First(u => u.ID == userId);
                if (user.GROUPS.FirstOrDefault(g => g.ID == groupId) != null)
                {
                    user.GROUPS.Remove(groupsRepository.First(g => g.ID == groupId));

                    doSave<USER>(session, userRepository.GetAll());
                }
            }
        }
        public void DeleteUserGroup(USER user, int groupId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IGroupsRepository groupsRepository = session.GetRepository<IGroupsRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                
                if (user.GROUPS.FirstOrDefault(g => g.ID == groupId) != null)
                {
                    user.GROUPS.Remove(groupsRepository.First(g => g.ID == groupId));

                    doSave<USER>(session, userRepository.GetAll());
                }
            }
        }
        public List<GroupModel> GetGroupIdsFromNames(string [] names)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IGroupsRepository groupsRepository = session.GetRepository<IGroupsRepository>();
                return groupsRepository.GetQuery(g => names.Contains(g.NAME)).Select(g => g.Map(false)).ToList();
            }
        }
        public List<GroupModel> GetAllGroups()
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IGroupsRepository groupsRepository = session.GetRepository<IGroupsRepository>();
                return groupsRepository.GetAll().ToList().Select(g => g.Map(false)).ToList();
            }
        }
        public GROUP GetGroup(string name)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IGroupsRepository groupsRepository = session.GetRepository<IGroupsRepository>();
                return groupsRepository.FirstOrDefault(g=> g.NAME == name);
            }
        }
        public bool CreateGroup(string name, int immunity)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IGroupsRepository groupsRepository = session.GetRepository<IGroupsRepository>();
                GROUP group = new GROUP
                {
                    NAME = name,
                    IMMUNITY = immunity
                };
                if(GetGroup(name) == null)
                {
                    groupsRepository.Add(group);
                    doSave<GROUP>(session, groupsRepository.GetAll());
                    return true;
                }
                return false;
            }
        }
        public bool DeleteGroup(string name)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IGroupsRepository groupsRepository = session.GetRepository<IGroupsRepository>();
                GROUP group = GetGroup(name);
                if (group  != null)
                {
                    groupsRepository.Delete(group);
                    doSave<GROUP>(session, groupsRepository.GetAll());
                    return true;
                }
                return false;
            }
        }
        public List<UserModel> FindUsersInGroup(string groupName)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IGroupsRepository groupsRepository = session.GetRepository<IGroupsRepository>();
                return groupsRepository.First(g => g.NAME == groupName).USERS.Select(u => u.Map(true)).ToList();
            }
        }
        public IQueryable<USER> GetUsersInGroup(string groupName)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IGroupsRepository groupsRepository = session.GetRepository<IGroupsRepository>();
                return groupsRepository.First(g => g.NAME == groupName).USERS.AsQueryable<USER>();
            }
        }
        public List<GroupModel> GetGroupsForUser(string idNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository IUserRepository = session.GetRepository<IUserRepository>();
                return IUserRepository.First(u => u.IDNUMBER == idNumber).GROUPS.Select(g => g.Map(false)).ToList();
            }
        }
        public List<GroupModel> GetGroupsForUser(int id)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository IUserRepository = session.GetRepository<IUserRepository>();
                return IUserRepository.First(u => u.ID == id).GROUPS.Select(g => g.Map(false)).ToList();
            }
        }
        public int GetMaxGroupImmunityForUser(string idNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository IUserRepository = session.GetRepository<IUserRepository>();
                return IUserRepository.First(u => u.IDNUMBER == idNumber).GROUPS.Max(g => g.IMMUNITY).ToInt();
            }
        }
        public bool UserHasGroup(string idNumber, string groupName)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository IUserRepository = session.GetRepository<IUserRepository>();
                return IUserRepository.First(u => u.IDNUMBER == idNumber).GROUPS.FirstOrDefault(g => g.NAME.ToLower() == groupName.ToLower()) == null;
            }
        }
    }
}
