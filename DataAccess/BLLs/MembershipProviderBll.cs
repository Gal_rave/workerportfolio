﻿using System;
using System.Collections.Generic;
using System.Linq;
using Catel.Data;

using Logger;
using DataAccess.Interfaces;
using DataAccess.Mappers;
using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.BLLs
{
    /// <summary>
    /// MembershipException - 62;
    /// </summary>
    public class MembershipProviderBll : BllExtender
    {
        public UserModel GetUserByEmail(string email)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                return userRepository.FirstOrDefault(u => u.EMAIL.ToLower() == email.ToLower() && !u.ISDELETED && u.EMAIL.TrimStart() != null).Map();
            }
        }
        public UserModel GetUser(string idNumber, string email)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                return userRepository.FirstOrDefault(u => (u.EMAIL.ToLower() == email.ToLower() || u.IDNUMBER == idNumber) && !u.ISDELETED).Map();
            }
        }
        public bool IsDuplicateIdNumber(string idNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                return userRepository.FirstOrDefault(u => !u.ISDELETED && u.IDNUMBER == idNumber) == null;
            }
        }
        public UserModel GetUserByIdNumber(string idNumber)
        {
            idNumber = idNumber.Trim();
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                return userRepository.FirstOrDefault(u => u.IDNUMBER == idNumber && !u.ISDELETED).Map();
            }
        }
        public int GetUserDefaultMunicipality(string idNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUsersTerminationRepository terminationRepository = session.GetRepository<IUsersTerminationRepository>();

                return terminationRepository.GetUserDefaultMunicipality(idNumber);
            }
        }
        public UserModel GetUserById(decimal id)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                return userRepository.First(u => u.ID == id).Map(true);
            }
        }
        public IQueryable<UserModel> GetUsers(string idNumber, string email)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                return userRepository.GetQuery(u => (u.EMAIL.ToLower() == email.ToLower() || u.IDNUMBER == idNumber) && !u.ISDELETED).Select(u => u.Map(false));
            }
        }
        public UserModel CreateUser(UserModel user)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                try
                {
                    USER u = user.Map();
                    u.MEMBERSHIP = user.membership.Map();
                    u.INCOMETYPE = user.password;
                    userRepository.Add(u);
                    doSave<USER>(session, u);

                    return u.Map();
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("InvalidUserData", 50, ex));
                }
            }
        }
        public bool DeleteUser(string iDnumber, bool deleteRelatedData)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    USER user = userRepository.FirstOrDefault(u => u.IDNUMBER == iDnumber);
                    if (user == null)
                        throw new MembershipException("NullReferenceException", 51);
                    user.ISDELETED = true;
                    if (deleteRelatedData)
                    {
                        userRepository.Delete(user);
                        ///TODO -> delete related records;
                    }
                    else
                    {
                        userRepository.Update(user);
                    }
                    doSave<USER>(session, user);
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("DeleteUserException", 52, ex));
                }
            }
        }
        public List<UserModel> GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    totalRecords = userRepository.GetQuery(u => !u.ISDELETED).Count();
                    return userRepository.GetAll().Where(u => !u.ISDELETED).Skip(pageIndex * pageSize).Take(pageSize).Select(u => u.Map(false)).ToList();
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("GetAllUsersException", 53, ex));
                }
            }
        }
        public void UpdateUser(UserModel user)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    USER u = user.Map();
                    userRepository.Update(u);
                    doSave<USER>(session, u);
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("UpdateUserException", 54, ex));
                }
            }
        }
        public void UpdateUser(USER user)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    userRepository.Update(user);
                    doSave<USER>(session, user);
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("UpdateUserException", 54, ex));
                }
            }
        }
        public bool UpdatePassword(MembershipModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IMembershipRepository membershipRepository = session.GetRepository<IMembershipRepository>();
                    membershipRepository.Update(model.Map());
                    session.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("UpdatePasswordException", 55, ex));
                }
            }
        }
        public bool ResetPassword(UserSearchModel model, PasswordHashModel passHash, string PasswordFormat)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IMembershipRepository membershipRepository = session.GetRepository<IMembershipRepository>();
                    MEMBERSHIP membership = membershipRepository.First(m => m.USERID == model.page);
                    membership.CONFIRMATIONTOKEN = passHash.confirmationToken;
                    membership.PASSWORD = passHash.password;
                    membership.PASSWORDCHANGEDDATE = DateTime.Now;
                    membership.PASSWORDFAILURESCOUNTER = 0;
                    membership.PASSWORDSALT = passHash.passwordSalt;
                    membership.PASSWORDVERIFICATIONTOKEN = passHash.verificationToken;
                    membership.VERIFICATIONTOKENEXPIRATION = DateTime.Now.AddDays(1);
                    membership.PASSWORDFORMAT = PasswordFormat;
                    membershipRepository.Update(membership);
                    session.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("UpdatePasswordException", 55, ex));
                }
            }
        }
        public bool UpdatePassword(MembershipModel model, PasswordHashModel passHash, string PasswordFormat)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IMembershipRepository membershipRepository = session.GetRepository<IMembershipRepository>();
                    model.confirmationToken = passHash.confirmationToken;
                    model.password = passHash.password;
                    model.passwordChangeDdate = DateTime.Now;
                    model.PasswordFailureSCounter = 0;
                    model.passwordSalt = passHash.passwordSalt;
                    model.passwordVerificationToken = passHash.verificationToken;
                    model.verificationTokenExpiration = DateTime.Now.AddDays(1);
                    model.passwordFormat = PasswordFormat;
                    membershipRepository.Update(model.Map());
                    session.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("UpdatePasswordException", 55, ex));
                }
            }
        }
        public bool UpdateQuestionAndAnswer(string question, string answer, int userId, int? questionId = null)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IMembershipQuestionsRepository questionsRepository = session.GetRepository<IMembershipQuestionsRepository>();
                    MEMBERSHIP_QUESTIONS q;
                    if (questionId.HasValue && questionId.Value > -1)
                    {
                        q = questionsRepository.GetByKey(questionId.Value);
                    }
                    else
                    {
                        q = questionsRepository.First(a => a.USERID == userId);
                    }
                    q.QUESTION = question;
                    q.ANSWER = answer;
                    questionsRepository.Update(q);
                    doSave<MEMBERSHIP_QUESTIONS>(session, questionsRepository.GetAll());
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("UpdateQuestionAndAnswerException", 56, ex));
                }
            }
        }
        public bool CreateQuestionAndAnswer(MembershipQuestionModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IMembershipQuestionsRepository questionsRepository = session.GetRepository<IMembershipQuestionsRepository>();
                    if (model.ID > -1)
                    {
                        questionsRepository.Update(model.Map());

                    }
                    else
                    {
                        questionsRepository.Add(model.Map());
                    }
                    doSave<MEMBERSHIP_QUESTIONS>(session, questionsRepository.GetAll());
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("CreateQuestionAndAnswerException", 57, ex));
                }
            }
        }
        public bool CreateQuestionAndAnswer(MEMBERSHIP_QUESTIONS model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IMembershipQuestionsRepository questionsRepository = session.GetRepository<IMembershipQuestionsRepository>();
                    if (model.ID > -1)
                    {
                        questionsRepository.Update(model);

                    }
                    else
                    {
                        questionsRepository.Add(model);
                    }
                    doSave<MEMBERSHIP_QUESTIONS>(session, questionsRepository.GetAll());
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("CreateQuestionAndAnswer", 57, ex));
                }
            }
        }
        public MembershipModel GetMembershipByIdNumber(string idNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    USER user = userRepository.FirstOrDefault(u => u.IDNUMBER == idNumber && !u.ISDELETED);
                    if (user == null || user.MEMBERSHIP == null)
                        throw new Exception("NullReferenceException");
                    return user.MEMBERSHIP.Map();
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("ResetPasswordException", 58, ex));
                }
            }
        }
        public UserModel GetMembershipAndUserByIdNumber(string idNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                USER user = userRepository.FirstOrDefault(u => u.IDNUMBER == idNumber && !u.ISDELETED);
                if (user == null || user.MEMBERSHIP == null)
                    throw new MembershipException("ResetPasswordException", 58);
                return user.Map(true);
            }
        }
        public MembershipModel GetMembershipById(int userId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    USER user = userRepository.FirstOrDefault(u => u.ID == userId && !u.ISDELETED);
                    if (user == null || user.MEMBERSHIP == null)
                        throw new Exception("NullReferenceException");
                    return user.MEMBERSHIP.Map();
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("ResetPasswordException", 58, ex));
                }
            }
        }
        public List<UserModel> FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                totalRecords = userRepository.GetQuery(u => !u.ISDELETED).Count();
                return userRepository.GetQuery(u => !u.ISDELETED && (u.EMAIL.StartsWith(emailToMatch) || u.EMAIL.EndsWith(emailToMatch) || u.EMAIL.Contains(emailToMatch)))
                    .Skip(pageIndex * pageSize).Take(pageSize).Select(u => u.Map(false)).ToList();

            }
        }
        public List<UserModel> FindUsersByIdNumber(string idNumberToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                totalRecords = userRepository.GetQuery(u => !u.ISDELETED).Count();
                return userRepository.GetQuery(u => !u.ISDELETED && (u.IDNUMBER.StartsWith(idNumberToMatch) || u.IDNUMBER.EndsWith(idNumberToMatch) || u.IDNUMBER.Contains(idNumberToMatch)))
                    .Skip(pageIndex * pageSize).Take(pageSize).Select(u => u.Map(false)).ToList();

            }
        }
        public void UpdatePasswordFailure(MembershipModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IMembershipRepository membershipRepository = session.GetRepository<IMembershipRepository>();
                    model.lastPasswordFailureDate = DateTime.Now;
                    model.PasswordFailureSCounter = ++model.PasswordFailureSCounter;
                    membershipRepository.Update(model.Map());
                    session.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("UpdatePasswordFailureException", 59, ex));
                }
            }
        }
        public DateTime? GetLastPasswordFailureDate(string idNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    return userRepository.First(u => u.IDNUMBER == idNumber).MEMBERSHIP.LASTPASSWORDFAILUREDATE;
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("LastPasswordFailureDateException", 60, ex));
                }
            }
        }
        public MembershipModel ConfirmUserFromToken(string confirmationToken)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IMembershipRepository membershipRepository = session.GetRepository<IMembershipRepository>();
                    MEMBERSHIP member = membershipRepository.First(m => m.CONFIRMATIONTOKEN == confirmationToken);
                    member.ISCONFIRMED = true;
                    membershipRepository.Update(member);
                    doSave<MEMBERSHIP>(session, membershipRepository.GetAll());
                    return member.Map(true);
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("ConfirmUserFromToken", 61, ex));
                }
            }
        }
        public MembershipModel ConfirmUserFromSmsToken(string smsToken)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IMembershipRepository membershipRepository = session.GetRepository<IMembershipRepository>();
                    MEMBERSHIP member = membershipRepository.First(m => m.SMSVERIFICATIONTOKEN == smsToken);
                    member.ISCONFIRMED = true;
                    membershipRepository.Update(member);
                    doSave<MEMBERSHIP>(session, membershipRepository.GetAll());
                    return member.Map(true);
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("ConfirmUserFromToken", 61, ex));
                }
            }
        }
        public UserModel TokenUserConfirmation(string confirmationToken)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IMembershipRepository membershipRepository = session.GetRepository<IMembershipRepository>();
                    MEMBERSHIP member = membershipRepository.First(m => m.CONFIRMATIONTOKEN == confirmationToken);
                    member.ISCONFIRMED = true;
                    membershipRepository.Update(member);
                    doSave<MEMBERSHIP>(session, membershipRepository.GetAll());

                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    USER user = userRepository.First(u => !u.ISDELETED && u.ID == member.USER.ID);
                    return user.Map(true);
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("ConfirmUserFromToken", 61, ex));
                }
            }
        }
        public UserModel GetUserFromPasswordVerificationToken(string passwordResetToken)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IMembershipRepository IMembershipRepository = session.GetRepository<IMembershipRepository>();
                    return IMembershipRepository.First(m => m.PASSWORDVERIFICATIONTOKEN == passwordResetToken && !m.USER.ISDELETED).USER.Map();
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("GetUserFromToken", 61, ex));
                }
            }
        }
        public UserModel GetUserFromPasswordVerificationCode(string idNumber, string smsverificationtoken)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IMembershipRepository IMembershipRepository = session.GetRepository<IMembershipRepository>();
                    return IMembershipRepository.First(m => m.SMSVERIFICATIONTOKEN == smsverificationtoken && m.USER.IDNUMBER == idNumber && !m.USER.ISDELETED).USER.Map();
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("GetUserFromSmsToken", 61, ex));
                }
            }
        }
        public UserModel GetUserByConfirmationToken(string confirmationToken)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    return userRepository.First(u => !u.ISDELETED && u.MEMBERSHIP != null && u.MEMBERSHIP.CONFIRMATIONTOKEN == confirmationToken).Map();
                }
                catch (Exception ex)
                {
                    throw ex.LogError(new MembershipException("GetUserByConfirmationToken", 61, ex));
                }
            }
        }
        public void UpdateUserTokens(UserModel userModel, PasswordHashModel hash)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                USER user = userRepository.First(u => u.ID == userModel.ID);
                user.MEMBERSHIP.PASSWORDVERIFICATIONTOKEN = hash.verificationToken;
                user.MEMBERSHIP.CONFIRMATIONTOKEN = hash.confirmationToken;
                user.MEMBERSHIP.VERIFICATIONTOKENEXPIRATION = DateTime.Now.AddDays(4);

                session.SaveChanges();
            }
        }
        public void UpdateVerificationTokenExpiration(UserModel userModel)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMembershipRepository membershipRepository = session.GetRepository<IMembershipRepository>();
                membershipRepository.Update(userModel.membership.Map());
                session.SaveChanges();
            }
        }
        public int GetUserFactoryId(int userId, int municipalityId)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IUsersToFactoryRepository usersToFactoryRepository = session.GetRepository<IUsersToFactoryRepository>();
                    USERSTOFACTORy userstofactory = usersToFactoryRepository.FirstOrDefault(f => f.USERID == userId && f.MUNICIPALITYID == municipalityId);
                    if (userstofactory != null)
                        return userstofactory.FACTORYID.ToInt();
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public int GetUserDefaultRashutId(int municipalityId)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IMunicipalityRepository IMunicipalityRepository = session.GetRepository<IMunicipalityRepository>();
                    return IMunicipalityRepository.First(m => m.ID == municipalityId).RASHUT.Value;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
    }
}
