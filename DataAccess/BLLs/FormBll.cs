﻿using System;
using Catel.Data;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;

using DataAccess.Interfaces;
using DataAccess.Mappers;
using DataAccess.Models;
using DataAccess.Extensions;
using Logger;

namespace DataAccess.BLLs
{
    public class FormBll : BllExtender
    {
        public TaxFromModel GetBaseForm101Data(int userID)
        {
            TaxFromModel model = new TaxFromModel();

            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                USER user = userRepository.First(u => !u.ISDELETED && u.ID == userID);
                model.employee = user.MapEmployee();
                model.children = user.CHILDREN.Where(c => c.BIRTHDATE.checkChildAge(19)).Select(c => c.Map()).ToList().OrderBy(c => c.birthDate).ToList();
                model.year = DateTime.Now.Year;
                model.emailEmployee = !string.IsNullOrWhiteSpace(user.EMAIL);
                model.spouse = userRepository.First(u => !u.ISDELETED && u.ID == userID).SPOUSE.Map();
            }
            ///added pre select on child for merid men and all woman
            model.children.ForEach(c =>
            {
                if (!model.employee.gender || (model.employee.gender && model.employee.maritalStatus == 2))
                {
                    c.homeLiving = true;
                }
                if (!model.employee.gender)
                    c.childBenefit = true;
            });
            return model;
        }
        public TaxFromModel GetBaseForm101Data(int userID, int Year)
        {
            TaxFromModel model = new TaxFromModel();

            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                USER user = userRepository.First(u => !u.ISDELETED && u.ID == userID);
                model.employee = user.MapEmployee();
                model.children = user.CHILDREN.Where(c => c.BIRTHDATE.checkChildAge(19)).Select(c => c.Map()).ToList().OrderBy(c => c.birthDate).ToList();
                model.year = Year;
                model.emailEmployee = !string.IsNullOrWhiteSpace(user.EMAIL);
                model.spouse = userRepository.First(u => !u.ISDELETED && u.ID == userID).SPOUSE.Map();
            }
            ///added pre select on child for merid men and all woman
            model.children.ForEach(c =>
            {
                if (!model.employee.gender || (model.employee.gender && model.employee.maritalStatus == 2))
                {
                    c.homeLiving = true;
                }
                if (!model.employee.gender)
                    c.childBenefit = true;
            });
            return model;
        }
        public TaxFromModel GetBaseForm101Data(WSPostModel wsModel)
        {
            TaxFromModel model = new TaxFromModel();

            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                USER user = userRepository.First(u => !u.ISDELETED && u.IDNUMBER == wsModel.idNumber);
                model.employee = user.MapEmployee();
                model.children = user.CHILDREN.Where(c => c.BIRTHDATE.checkChildAge(19)).Select(c => c.Map()).ToList().OrderBy(c => c.birthDate).ToList();
                model.year = wsModel.selectedDate.Year;
                model.emailEmployee = !string.IsNullOrWhiteSpace(user.EMAIL);
                model.spouse = userRepository.First(u => !u.ISDELETED && u.ID == model.employee.ID).SPOUSE.Map();
            }
            ///added pre select on child for merid men and all woman
            model.children.ForEach(c =>
            {
                if (!model.employee.gender || (model.employee.gender && model.employee.maritalStatus == 2))
                {
                    c.homeLiving = true;
                }
                if (!model.employee.gender)
                    c.childBenefit = true;
            });
            return model;
        }
        public DateTime GetUserStartWorkDate(int userID, int municipalityId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUsersTerminationRepository terminationRepository = session.GetRepository<IUsersTerminationRepository>();
                USERSTERMINATION termination = terminationRepository.GetQuery(t => t.USERID == userID && t.MUNICIPALITYID == municipalityId && t.COMMENCEMENTDATE.HasValue).OrderByDescending(t => t.COMMENCEMENTDATE).FirstOrDefault();
                if (termination != null)
                    return termination.COMMENCEMENTDATE.Value;
            }
            DateTime firstDayOfYear = new DateTime(DateTime.Now.Year, 1, 1);
            return firstDayOfYear;
        }
        public MunicipalityModel GetMunicipality(WSPostModel wsModel)
        {
            TaxFromModel model = new TaxFromModel();

            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                decimal municipalityId = wsModel.currentMunicipalityId.ToDecimal();
                return municipalityRepository.First(m => m.ID == municipalityId).Map(false);
            }
        }
        public void SaveFormData(Form101DataModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IForm101DataRepository form101DataRepository = session.GetRepository<IForm101DataRepository>();
                    FORM101DATA data;

                    if (model.ID > 0)
                    {
                        data = form101DataRepository.First(f => f.ID == model.ID);
                        data = model.Map();
                        form101DataRepository.Update(data);
                    }
                    else
                    {
                        data = model.Map();
                        form101DataRepository.Add(data);
                    }
                    session.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public Form101DataModel GetFormDataByVersion(string version)
        {
            if (string.IsNullOrWhiteSpace(version))
                return null;
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IForm101DataRepository form101DataRepository = session.GetRepository<IForm101DataRepository>();
                    return form101DataRepository.FirstOrDefault(f => f.VERSION == version).Map();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public List<UserImageModel> GetFormImages(List<decimal> imagesIds)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserImageRepository imageRepository = session.GetRepository<IUserImageRepository>();
                return imageRepository.GetQuery(i => imagesIds.Contains(i.ID)).ToList().Select(i => i.Map()).ToList();
            }
        }
        public List<Form101DataExtensionModel> GetErroneousForm101(string year)
        {
            List<FORM101DATA> forms = new List<FORM101DATA>();
            string searchVal = string.Format("V:{0}", year);

            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IForm101DataRepository form101DataRepository = session.GetRepository<IForm101DataRepository>();
                IQueryable<FORM101DATA> fs = form101DataRepository
                    .Include("USER").Include("USER.SPOUSEs").Include("USER.MUNICIPALITIES").Include("USER.GROUPS").Include("USER.MEMBERSHIP")
                    .Include("USER.USERSTERMINATIONS").Include("USER.EMAILBYMUNICIPALITies")
                    .Include("USERIMAGE").Include("USERIMAGE1")
                    .Where(f => 
                            f.DELIVERYSTATUS < 0 && 
                            f.SIGNATUREIMAGID.HasValue && f.SIGNATUREIMAGID.Value > 0 && 
                            f.VERSION.IndexOf(searchVal) >= 0 && 
                            f.MUNICIPALITYID.HasValue
                        );

                List<decimal> OKs = form101DataRepository.GetQuery(f => 
                            f.DELIVERYSTATUS == 0 && f.SIGNATUREIMAGID.HasValue && f.SIGNATUREIMAGID.Value > 0 && f.MUNICIPALITYID.HasValue && f.VERSION.IndexOf(searchVal) >= 0)
                        .Select(f => f.USERID).ToList().Distinct<decimal>().ToList();

                fs = fs.Where(f => !OKs.Contains(f.USERID));

                OKs = new List<decimal>();

                foreach (FORM101DATA form in yealdForms(fs.OrderByDescending(f => f.ID)))
                {
                    if (!OKs.Contains(form.USERID))
                    {
                        OKs.Add(form.USERID);
                        forms.Add(form);

                        Log.LogLoad(new { USERID = form.USERID, formid = form.ID, municipalityid = form.MUNICIPALITYID }, "GetErroneousForm101");
                        if (forms.Count >= 75)
                            break;
                    }
                }

                return forms.Select(f => f.Map(true)).ToList();
            }
        }
        public void UpdateForm101(Form101DataExtensionModel model)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IForm101DataRepository form101DataRepository = session.GetRepository<IForm101DataRepository>();
                    FORM101DATA form = form101DataRepository.First(f=> f.ID == model.ID);
                    
                    form = model.Map();
                    
                    form101DataRepository.Update(form);
                    session.SaveChanges();
                }
            }
            catch (Exception e)
            {
                e.LogError();
            }
        }
        public MunicipalityContactModel GetMunicipalityContactByFactoryId(int municipalityId, int factoryId, bool withInner = false)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IMunicipalityContactRepository municipalityContactRepository = session.GetRepository<IMunicipalityContactRepository>();
                    return municipalityContactRepository.First(m => m.MUNICIPALITYID == municipalityId && m.MIFAL.HasValue && m.MIFAL.Value == factoryId).Map(withInner);
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        
        #region private
        private IEnumerable<FORM101DATA> yealdForms(IQueryable<FORM101DATA> Qforms)
        {
            foreach (FORM101DATA form in Qforms)
            {
                yield return form;
            }
        }
        #endregion
    }
}
