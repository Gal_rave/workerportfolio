﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Catel.Data;
using System;

using DataAccess.Interfaces;
using DataAccess.Mappers;
using DataAccess.Models;
using DataAccess.Extensions;
using Logger;
using HttpCookieHandler;
using System.Configuration;

namespace DataAccess.BLLs
{
    public class MailerBll : BllExtender
    {
        public List<UserModel> GetUsersByMunicipalityId(int municipalityId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    return userRepository.GetQuery(u => u.EMAIL != null && u.MUNICIPALITIES.FirstOrDefault(m => m.ID == municipalityId) != null)
                        .ToList().Select(u => u.Map(false)).ToList();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public List<MunicipalityContactModel> GetMunicipalityContacts(int municipalityId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IMunicipalityContactRepository mcRepository = session.GetRepository<IMunicipalityContactRepository>();
                    return mcRepository.GetQuery(c => c.MUNICIPALITYID == municipalityId).ToList().Select(c => c.Map(false)).ToList();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public MunicipalityModel GetMunicipalityAndContacts(int municipalityId, bool withInner = true)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                    return municipalityRepository.Include("MUNICIPALITYCONTACTs").First(m => m.ID == municipalityId).Map(withInner);
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public List<USER> GetUsersByIds(List<decimal> uIds, int municipalityId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserRepository IUserRepository = session.GetRepository<IUserRepository>();
                    return IUserRepository.Include("MUNICIPALITIES").Include("USERSTOFACTORIES")
                                .Where(u => uIds.Contains(u.ID)).ToList()
                                .Where(u => u.MUNICIPALITIES.FirstOrDefault(m => m.ID == municipalityId) != null).ToList();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public List<USER> UsersByMunicipalityEmail(int municipalityId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IProposalUEPCRepository puepcRepository = session.GetRepository<IProposalUEPCRepository>();
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();

                    List<decimal> userIds = puepcRepository.GetQuery(u => u.MUNICIPALITYID == municipalityId).ToList().Select(u => u.ID).ToList();
                    return userRepository.Include("EMAILBYMUNICIPALITies").Include("MEMBERSHIP").Where(u => userIds.Contains(u.ID)).ToList();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public List<USER> GetFormReminderUsers(int municipalityId, int year)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    List<decimal> terminationCause = ConfigurationManager.AppSettings["TerminationCauseExit"].ToDecimalLIst();
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    IFormStatisticsRepository fsRepository = session.GetRepository<IFormStatisticsRepository>();
                    IForm101DataRepository formRepository = session.GetRepository<IForm101DataRepository>();
                    DateTime firstOfYear = new DateTime(DateTime.Now.Year, 1, 1);
                    DateTime firstOfLastYear = new DateTime((DateTime.Now.Year - 1), 1, 1);

                    /* **** users ho sent a form in year **** */
                    List<decimal> fd_ids = formRepository.GetQuery(f => f.MUNICIPALITYID == municipalityId && f.DELIVERYSTATUS == 0 && f.CREATEDDATE.Year == year).Select(u => u.USERID).ToList();

                    List<decimal> fs_ids = fsRepository.GetQuery(fs=>                         
                        fs.MUNICIPALITYID == municipalityId &&
                        (
                            !fs.CREATEDDATE.HasValue || !fd_ids.Contains(fs.USERID)
                         ) &&
                        (
                            !fs.TERMINATIONCAUSE.HasValue ||
                            (
                                fs.TERMINATIONCAUSE.HasValue && terminationCause.Contains(fs.TERMINATIONCAUSE.Value) &&
                                fs.TERMINATIONDATE.HasValue && fs.TERMINATIONDATE.Value >= firstOfYear
                            ) ||
                            (
                                fs.TERMINATIONCAUSE.HasValue && !terminationCause.Contains(fs.TERMINATIONCAUSE.Value) &&
                                fs.TERMINATIONDATE.HasValue && fs.TERMINATIONDATE.Value >= firstOfLastYear
                            )
                         )
                    ).Select(fs=> fs.USERID).ToList();

                    return userRepository.Include("EMAILBYMUNICIPALITies").Include("MEMBERSHIP").Where(u => 
                        fs_ids.Contains(u.ID) &&
                        u.EMAILBYMUNICIPALITies.FirstOrDefault(e=> e.MUNICIPALITYID == municipalityId) != null
                        ).ToList();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
    }
}
