﻿using System.Collections.Generic;
using System.Linq;
using System;

using Catel.Data;
using DataAccess.Interfaces;
using DataAccess.Mappers;
using DataAccess.Models;
using DataAccess.Extensions;
using Logger;
using System.Globalization;
using System.Data.Entity;

namespace DataAccess.BLLs
{
    public class ProcessBll : BllExtender
    {
        public bool SaveTahalichimData(List<TahalichModel> tahalichim, List<StepModel> shlavim, List<ReshimatTiyugModel> reshimotTiyug, List<MismachBereshimaModel> mismachimBeReshima, List<MismachModel> mismachim, List<KvuzatMishtamshimModel> kvuzotMishtamshim, List<MishtamsheiKvuzaModel> mishtamsheiKvuza, int rashut)
        {
            if(tahalichim.Count() == 0 ||shlavim.Count() == 0 || kvuzotMishtamshim.Count() == 0 || mishtamsheiKvuza.Count() == 0 || !(rashut > 0))
            {
                return false;
            }
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                Log.FormLog( "Import process -> saveTahalichim -> before deletion for rashut " + rashut );

                try
                {
                    ITahalichimRepository tahalichimRepository = session.GetRepository<ITahalichimRepository>();
                    //delete old tahalichim
                    tahalichimRepository.GetAll().ToList().Where(t => t.RASHUT == rashut).ToList().ForEach(tahalich =>
                    {
                        tahalichimRepository.Delete(tahalich);
                        Log.FormLog(new { importer = "tahalichim", deletNow = tahalich });

                    });
                    //insert new tahalichim
                    tahalichim.ForEach(tahalich =>
                    {
                        TAHALICHIM t = tahalich.Map();
                        tahalichimRepository.Add(t);
                    });
                    session.SaveChanges();

                    IKvuzotMishtamshimRepository kvuzotMishtamshimRepository = session.GetRepository<IKvuzotMishtamshimRepository>();
                    //delete old kvuzotMishtam
                    kvuzotMishtamshimRepository.GetAll().ToList().Where(km => km.RASHUT == rashut).ToList().ForEach(kvuzatMishtamshim =>
                    {
                        kvuzotMishtamshimRepository.Delete(kvuzatMishtamshim);
                        Log.FormLog(new { importer = "kvuzatMishtamshim", deletNow = kvuzatMishtamshim });
                    });
                    //insert new kvuzotMishtam
                    kvuzotMishtamshim.ForEach(kvuzatMishtamshim =>
                    {
                        KVUZOT_MISHTAMSHIM km = kvuzatMishtamshim.Map();
                        kvuzotMishtamshimRepository.Add(km);
                    });
                    session.SaveChanges();

                    IMishtamsheiKvuzaRepository mishtamsheiKvuzaRepository = session.GetRepository<IMishtamsheiKvuzaRepository>();
                    //delete old mishtamsheiKvuza
                    mishtamsheiKvuzaRepository.GetAll().ToList().Where(mk => mk.RASHUT == rashut).ToList().ForEach(mishtameshKvuza =>
                    {
                        mishtamsheiKvuzaRepository.Delete(mishtameshKvuza);
                        Log.FormLog(new { importer = "mishtameshKvuza", deletNow = mishtameshKvuza });
                    });
                    //insert new mishtamsheiKvuza
                    mishtamsheiKvuza.ForEach(mishtameshKvuza =>
                    {
                        MISHTAMSHEI_KVUZA mk = mishtameshKvuza.Map();
                        mishtamsheiKvuzaRepository.Add(mk);
                    });
                    session.SaveChanges();

                    if (mismachim.Count > 0)
                    {

                        IMismachimRepository mismachimRepository = session.GetRepository<IMismachimRepository>();
                        //delete old mismachim
                        mismachimRepository.GetAll().ToList().Where(m => m.RASHUT == rashut).ToList().ForEach(misamch =>
                        {
                            mismachimRepository.Delete(misamch);
                            Log.FormLog(new { importer = "misamch", deletNow = misamch });
                        });
                        //insert new mismachim
                        mismachim.ForEach(misamch =>
                        {
                            MISMACHIM m = misamch.Map();
                            mismachimRepository.Add(m);
                        });
                        session.SaveChanges();
                    }

                    if (reshimotTiyug.Count > 0)
                    {
                        IReshimotTiyugRepository reshimotTiyugRepository = session.GetRepository<IReshimotTiyugRepository>();
                        //delete old reshimotTiyug
                        reshimotTiyugRepository.GetAll().ToList().Where(r => r.RASHUT == rashut).ToList().ForEach(reshimatTiyug =>
                        {
                            reshimotTiyugRepository.Delete(reshimatTiyug);
                            Log.FormLog(new { importer = "reshimotTiyug", deletNow = reshimatTiyug });
                        });
                        //insert new reshimotTiyug
                        reshimotTiyug.ForEach(reshimatTiyug =>
                        {
                            RESHIMOT_TIYUG rt = reshimatTiyug.Map();
                            reshimotTiyugRepository.Add(rt);
                        });
                        session.SaveChanges();
                    }

                    if (mismachimBeReshima.Count > 0)
                    {
                        IMismachimBeReshimaRepository mismachimBeReshimaRepository = session.GetRepository<IMismachimBeReshimaRepository>();
                        //delete old mismachimBeReshima
                        mismachimBeReshimaRepository.GetAll().ToList().Where(m => m.RASHUT == rashut).ToList().ForEach(misamchBereshima =>
                        {
                            mismachimBeReshimaRepository.Delete(misamchBereshima);
                            Log.FormLog(new { importer = "misamchBereshima", deletNow = misamchBereshima });
                        });
                        //insert new mismachimBeReshima
                        mismachimBeReshima.ForEach(misamchBereshima =>
                        {
                            MISMACHIM_BERESHIMA mbr = misamchBereshima.Map();
                            mismachimBeReshimaRepository.Add(mbr);
                        });
                        session.SaveChanges();
                    }
                    
                    IShlavimRepository shlavimRepository = session.GetRepository<IShlavimRepository>();
                    //delete old shlavim
                    shlavimRepository.GetAll().ToList().Where(s => s.RASHUT == rashut).ToList().ForEach(shalav =>
                    {
                        shlavimRepository.Delete(shalav);
                        Log.FormLog(new { importer = "shlavim", deletNow = shalav });
                    });
                    //insert new shlavim
                    shlavim.ForEach(shalav =>
                    {
                        SHLAVIM s = shalav.Map();
                        shlavimRepository.Add(s);
                    });
                    session.SaveChanges();
                }
                catch (Exception ex)
                {
                    return ex.LogError(false);
                }

                return true;
            }
        }
        public List<MistameshSacharModel> getMishtamsheiKvuza(int rashutId, int municipalityId, List<MishtamsheiKvuzaModel> mishtamshim)
        {
            List<MistameshSacharModel> users = new List<MistameshSacharModel>();
            MistameshSacharModel user = new MistameshSacharModel();
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IMistameshSacharRepository sacharUsersRepository = session.GetRepository<IMistameshSacharRepository>();
                    IUserRepository usersRepository = session.GetRepository<IUserRepository>();

                    foreach (MishtamsheiKvuzaModel mishtamesh in mishtamshim)
                    {
                        string mishtameshId = (mishtamesh.mishtameshId/10).ToString();

                        USER usr = usersRepository.FirstOrDefault(us => us.CALCULATEDIDNUMBER == mishtameshId);

                        int userID = usr.ID.ToInt();

                        user = sacharUsersRepository.FirstOrDefault(u => u.USERID == userID).Map();

                        if (user != null)
                            users.Add(user);
                    }

                    return users;
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public bool saveAcademicInsts(academicWrraper academicInstsWraped)
        {
            if(academicInstsWraped == null && academicInstsWraped.result.records == null)
            {
                return false;
            }
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IAcademicInstRepository academicInstRepository = session.GetRepository<IAcademicInstRepository>();

                //delete old
                academicInstRepository.GetAll().ToList().ForEach(inst => academicInstRepository.Delete(inst));

                //insert new
                academicInstsWraped.result.records.ForEach(inst => {
                    academicInstRepository.Add(inst.Map());
                });
                session.SaveChanges();
                return true;
            }
        }
        public void SaveUnitMngrs(List<UnitManagerModel> unitManagers, int rashut)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IOrgUnitMngrsRepository orgUnitMngrsRepository = session.GetRepository<IOrgUnitMngrsRepository>();
                if (rashut > 0)
                {
                    //delete old
                    orgUnitMngrsRepository.GetAll().ToList().Where(um => um.RASHUT == rashut).ToList().ForEach(mngr => orgUnitMngrsRepository.Delete(mngr));

                    //insert new
                    unitManagers.ForEach(mngr => orgUnitMngrsRepository.Add(mngr.Map()));
                    session.SaveChanges();
                }
                return;
            }
        }
        public List<TahalichModel> getRashutProcesses(int mncplity)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ITahalichimRepository tahalichimRepository = session.GetRepository<ITahalichimRepository>();;
                List<TahalichModel> tahalichim = tahalichimRepository.GetQuery(tahalich => tahalich.RASHUT == mncplity).ToList().Select(u => u.Map()).ToList();
                return tahalichim;
            }
        }
        public List<YehidaIrgunitModel> findSecretaryUnits(WSPostModel wsModel, int mncplity)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                int tempId = wsModel.calculatedIdNumber.ToInt();
                IOrgUnitMngrsRepository unitRepository = session.GetRepository<IOrgUnitMngrsRepository>();
                List<UnitManagerModel> secretaryRecords = unitRepository.Find(unit => unit.RASHUT == mncplity && unit.ZEHUT_NATZIG_K_ADAM == tempId).ToList().Select(unit => unit.Map()).ToList();
                List<YehidaIrgunitModel> secretaryUnits = new List<YehidaIrgunitModel>();
                if(secretaryRecords.Count() > 0)
                {
                    foreach(UnitManagerModel rec in secretaryRecords)
                    {
                        YehidaIrgunitModel yehida = new YehidaIrgunitModel();
                        yehida.treeNumber = rec.treeNumber;
                        yehida.unitNumber = rec.unitNumber;
                        secretaryUnits.Add(yehida);
                    }
                }
                return secretaryUnits;
            }
        }
        public List<RepresentedWorkerModel> getWorkers(List<int> workersIds)
        {

            List<RepresentedWorkerModel> workers = new List<RepresentedWorkerModel>();
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                foreach (int id in workersIds)
                {
                    string tempId = id.ToString();
                    RepresentedWorkerModel worker = new RepresentedWorkerModel();
                    USER user = userRepository.FirstOrDefault(u => u.CALCULATEDIDNUMBER == tempId);
                    if(user != null)
                    {
                        worker.idNumber = id;
                        worker.fullName = user.FIRSTNAME + " " + user.LASTNAME;
                        worker.email = user.EMAIL;
                        workers.Add(worker);
                    }
                }
            }
            return workers;
        }
        public List<UnitManagerModel> getUserUnitMngrs(List<YehidaIrgunitModel> yehidot, int mncplity, string IdNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                List<UnitManagerModel> mngrs = new List<UnitManagerModel>();
                IOrgUnitMngrsRepository unitMngrRepository = session.GetRepository<IOrgUnitMngrsRepository>();
                foreach(YehidaIrgunitModel yehida in yehidot)
                {
                    UnitManagerModel manager = unitMngrRepository.FirstOrDefault(mngr => mngr.RASHUT == mncplity && mngr.UNIT_NUMBER == yehida.unitNumber && mngr.TREE_NUMBER == yehida.treeNumber).Map();
                    if (manager != null)
                    {
                        if (!(manager.idNumber == IdNumber.ToInt()))
                        {
                            mngrs.Add(manager);
                        }
                        else
                        {
                            if (manager.supervisorUnitNumber != 0)
                            {
                                manager = unitMngrRepository.FirstOrDefault(mngr => mngr.RASHUT == mncplity && mngr.UNIT_NUMBER == manager.supervisorUnitNumber && mngr.TREE_NUMBER == manager.treeNumber).Map();
                            }
                            else
                            {
                                mngrs.Add(manager);
                            }
                            if (manager != null)
                            {
                                mngrs.Add(manager);
                            }
                        }

                        //09.07.2018 need 1 manager. for 3 managers uncomment below code lines:

                        //if (manager.supervisorUnitNumber != 0)
                        //{
                        //    manager = unitMngrRepository.FirstOrDefault(mngr => mngr.RASHUT == mncplity && mngr.UNIT_NUMBER == manager.supervisorUnitNumber && mngr.TREE_NUMBER == manager.treeNumber).Map();
                        //}
                        //if (manager != null)
                        //{
                        //    mngrs.Add(manager);

                        //    if (manager.supervisorUnitNumber != 0)
                        //    {
                        //        manager = unitMngrRepository.FirstOrDefault(mngr => mngr.RASHUT == mncplity && mngr.UNIT_NUMBER == manager.supervisorUnitNumber && mngr.TREE_NUMBER == manager.treeNumber).Map();
                        //        if(manager != null)
                        //        {
                        //            mngrs.Add(manager);
                        //        }
                        //    }
                        //}
                    }
                }

                return mngrs;
            }
        }
        public List<PROCESS> getProcessesByTmIdAndRashut(int tmProcessId, int customerNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IProcessRepository processRepository = session.GetRepository<IProcessRepository>();
                List<PROCESS> prcss = processRepository.Find(p => p.TM_TAHALICH_ID == tmProcessId && p.RASHUT == customerNumber).ToList();
                return prcss;
            }
        }
        public void updateProcessRecordStatus(PROCESS pr, UpdateProcessModel process)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IProcessRepository processRepository = session.GetRepository<IProcessRepository>();
                pr.STATUS = process.processStatus == 1 ? 2 : process.processStatus == 2 ? 3 : pr.STATUS;//1 in java is canceled so 2 in tikOved. 2 in java is completed so 3 in tik oved.
                if(pr.STATUS == 3)
                {
                    pr.CURR_MISPAR_SHALAV = pr.SHLAVIM_COUNTER;
                }
                else pr.CURR_MISPAR_SHALAV = process.currStepNumber;
                processRepository.Update(pr);
                session.SaveChanges();
            }
        }
        public ProcessModel getProcessRecord(decimal id)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IProcessRepository processRepository = session.GetRepository<IProcessRepository>();
                ProcessModel pr = processRepository.FirstOrDefault(p => p.ID == id).Map(true);
                return pr;
            }
        }
        public List<StepModel> getRashutSteps(int mncplity)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IShlavimRepository shlavimRepository = session.GetRepository<IShlavimRepository>();
                List<StepModel> steps = shlavimRepository.GetQuery(step => step.RASHUT == mncplity).ToList().Select(u => u.Map()).ToList();
                return steps;
            }
        }
        public List<UnitManagerModel> getUnitMngrs(int mncplity)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IOrgUnitMngrsRepository orgUnitMngrsRepository = session.GetRepository<IOrgUnitMngrsRepository>();
                List<UnitManagerModel> unitManagers = orgUnitMngrsRepository.GetQuery(u => u.RASHUT == mncplity).ToList().Select(u => u.Map()).ToList();
                return unitManagers;
            }
        }
        public bool synchronizeUsersAndSave(List<SACHAR_USERS_GELEM> users)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMistameshSacharRepository mishtameshRepository = session.GetRepository<IMistameshSacharRepository>();
                ISacharUserRepository sacahrGelemRepository = session.GetRepository<ISacharUserRepository>();
                foreach (SACHAR_USERS_GELEM user in users)
                {
                    sacahrGelemRepository.Add(user);
                }
                session.SaveChanges();

                mishtameshRepository.InsertSacharUsers();
            }
            return true;
        }
        public void SyncUnitMngrsAndUsers()
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IOrgUnitMngrsRepository orgUnitMngrsRepository = session.GetRepository<IOrgUnitMngrsRepository>();
                orgUnitMngrsRepository.SyncUnitMngrsvsUsers();
            }
        }
        public void CreateProcess(ProcessModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IProcessRepository processRepository = session.GetRepository<IProcessRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                string tempId = model.USERID.ToString();
                int tempRecId = userRepository.FirstOrDefault(user => user.CALCULATEDIDNUMBER == tempId).ID.ToInt();
                model.USERID = tempRecId;

                int intId = model.TARGET_USERID.ToInt();
                string mishtameshId1 = (intId / 10).ToString();
                string mishtameshId2 = intId.ToString();
                model.TARGET_USERID = userRepository.FirstOrDefault(user => user.CALCULATEDIDNUMBER == mishtameshId1 || user.CALCULATEDIDNUMBER == mishtameshId2).ID.ToInt();

                PROCESS processRec = model.Map();
                processRepository.Add(processRec);
                session.SaveChanges();
                return;
            }
        }
        public void deleteProcessRecordsFromUsers(ProcessModel model, string calculatedIdNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IProcessRepository processRepository = session.GetRepository<IProcessRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                int intId = model.TARGET_USERID.ToInt();
                string targetUserId = (intId / 10).ToString();
                model.TARGET_USERID = userRepository.FirstOrDefault(u => u.CALCULATEDIDNUMBER == targetUserId).ID.ToDecimal();

                if(model.TARGET_USERID > 0)
                {
                    //delete rest of the processes and keep only the new target user process record
                    processRepository.GetAll().ToList()
                        .Where(pr => pr.RASHUT == model.RASHUT && pr.TM_TAHALICH_ID == model.TM_TAHALICH_ID && pr.TARGET_USERID != model.TARGET_USERID).ToList()
                        .ForEach(pr => processRepository.Delete(pr));
                    
                    session.SaveChanges();
                }

                return;
            }
        }
        public void updateKvuzatMishtamshim(ProcessModel model, List<MishtamsheiKvuzaModel> mishtamshim)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IProcessRepository processRepository = session.GetRepository<IProcessRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                
                if(mishtamshim.Count > 0)
                {
                    processRepository.Find(pr => pr.TM_TAHALICH_ID == model.TM_TAHALICH_ID).ToList()
                        .ForEach(pr => processRepository.Delete(pr));

                    foreach(MishtamsheiKvuzaModel mishtamesh in mishtamshim)
                    {
                        //set 2 fields below to be ready to use by createProcess method
                        model.TARGET_USERID = mishtamesh.mishtameshId;
                        model.USERID = model.USERMODEL.calculatedIdNumber.ToDecimal();

                        //create process record for the current mishtamesh
                        CreateProcess(model);
                    }
                }
                session.SaveChanges();
            }
        }
        public List<StepModel> getTahalichSteps(decimal ttTahalichId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                List<StepModel> steps = new List<StepModel>();

                IShlavimRepository shlavimRepository = session.GetRepository<IShlavimRepository>();
                steps = shlavimRepository.Find(s => s.TAHALICHID == ttTahalichId).ToList().Select(s => s.Map(true)).ToList();
                return steps;
            }
        }
        public void setStatusToProcess(ProcessModel model, string calculatedIdNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IProcessRepository processRepository = session.GetRepository<IProcessRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                
                List<ProcessModel> processesToUpdate = processRepository.Find(p => p.TM_TAHALICH_ID == model.TM_TAHALICH_ID).ToList().Select(p => p.Map(false)).ToList();
                foreach(ProcessModel process in processesToUpdate)
                {
                    process.STATUS = model.STATUS;//actually 1 or 2
                    PROCESS proc = process.Map();
                    processRepository.Update(proc);
                }
                session.SaveChanges();
                    
                return;
            }
        }
        public void updateComments(ProcessModel model, string calculatedIdNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IProcessRepository processRepository = session.GetRepository<IProcessRepository>();

                    List<ProcessModel> processesToUpdate = processRepository.Find(p => p.TM_TAHALICH_ID == model.TM_TAHALICH_ID).ToList().Select(p => p.Map(false)).ToList();
                    foreach (ProcessModel process in processesToUpdate)
                    {
                        bool commentsEq = !string.IsNullOrEmpty(process.COMMENTS) ? process.COMMENTS.Equals(model.COMMENTS) : false;
                        process.COMMENTS = !commentsEq && !string.IsNullOrEmpty(process.COMMENTS) ? process.COMMENTS + " " + getTargetUsername(calculatedIdNumber.ToDecimal()) + ": " + model.COMMENTS + ". " : !commentsEq && string.IsNullOrEmpty(process.COMMENTS) ? getTargetUsername(calculatedIdNumber.ToDecimal()) + ": " + model.COMMENTS + ". " : process.COMMENTS + ". ";
                        PROCESS proc = process.Map();
                        processRepository.Update(proc);
                    }
                    session.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public void incrementToNextStep(ProcessModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IProcessRepository processRepository = session.GetRepository<IProcessRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                List<ProcessModel> processesToUpdate = processRepository.Find(p => p.TM_TAHALICH_ID == model.TM_TAHALICH_ID).ToList().Select(p => p.Map(false)).ToList();
                foreach (ProcessModel process in processesToUpdate)
                {
                    process.CURR_MISPAR_SHALAV = model.CURR_MISPAR_SHALAV;//increment to next step
                    PROCESS proc = process.Map();
                    processRepository.Update(proc);
                }
                session.SaveChanges();

                return;
            }
        }
        public List<ProcessModel> getMyProcesses(WSPostModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IProcessRepository processRepository = session.GetRepository<IProcessRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                decimal id = userRepository.First(user => user.CALCULATEDIDNUMBER == model.calculatedIdNumber).ID.ToInt();
                List<ProcessModel> myProcesses = processRepository.Include("USER").Include("USER1").Where(prcs => prcs.TARGET_USERID == id)
                    .ToList().OrderByDescending(prcs => prcs.ID).Select(p => p.Map(true)).ToList();
                return myProcesses;
            }
        }
        public List<ProcessModel> getMyProcesses(string calcIdNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IProcessRepository processRepository = session.GetRepository<IProcessRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                decimal id = userRepository.First(user => user.CALCULATEDIDNUMBER == calcIdNumber).ID.ToInt();
                List<ProcessModel> myProcesses = processRepository.Include("USER").Include("USER1").Where(prcs => prcs.TARGET_USERID == id)
                    .ToList().OrderByDescending(prcs => prcs.ID).Select(pr => pr.Map(true)).ToList();
                return myProcesses;
            }
        }
        public UserModel getUserRecord(WSPostModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                UserModel user = userRepository.First(u => u.IDNUMBER == model.idNumber).Map();
                return user;
            }
        }
        public UserModel getNewOwnerUserModel(decimal targetUserId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                UserModel newOwner = userRepository.FirstOrDefault(u => u.ID == targetUserId).Map(true);
                return newOwner;
            }
        }
        public List<ProcessModel> getMyRequestProcesses(WSPostModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IProcessRepository processRepository = session.GetRepository<IProcessRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                USER user = userRepository.First(u => u.CALCULATEDIDNUMBER == model.calculatedIdNumber);
                decimal recid = user.ID;

                List<ProcessModel> myReqProcesses = processRepository.GetQuery(prcs => prcs.USERID == recid).ToList().OrderByDescending(prcs => prcs.ID).Select(prcs => prcs.Map()).ToList();
                return myReqProcesses;
            }
        }
        public List<MistameshSacharModel> getUsers(WSPostModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMistameshSacharRepository mishtameshRepository = session.GetRepository<IMistameshSacharRepository>();
                IQueryable<SACHAR_USERS> users = mishtameshRepository.GetQuery().Include("USER").Where(u => u.RASHUT == model.currentRashutId);
                
                return users.ToList().Select(u => u.Map(true)).ToList();
            }
        }
        public List<AcademicInstModel> getAcademicInsts()
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IAcademicInstRepository academicInstRepository = session.GetRepository<IAcademicInstRepository>();
                List<AcademicInstModel> acInsts = academicInstRepository.GetAll().ToList().Select(inst => inst.Map()).ToList();
                return acInsts;
            }
        }
        public List<ProcessModel> saveProcess(ProcessModel model, string calcIdNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IProcessRepository processRepository = session.GetRepository<IProcessRepository>();
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();

                    PROCESS p = processRepository.FirstOrDefault(pr => pr.ID == model.ID);
                    p.COMMENTS = model.COMMENTS;
                    p.STATUS = model.STATUS;
                    string tempId = model.TARGET_USERID.ToString();
                    decimal idTemp = tempId.ToDecimal();
                    decimal newRecID = userRepository.FirstOrDefault(u => u.CALCULATEDIDNUMBER == tempId || u.ID == idTemp).ID;
                    if (newRecID > 0)
                    {
                        p.TARGET_USERID = newRecID;
                    }
                    doSave<PROCESS>(session, p);
                    
                    decimal id = userRepository.First(user => user.CALCULATEDIDNUMBER == calcIdNumber).ID.ToInt();
                    List<ProcessModel> myProcesses = processRepository.Include("USER").Include("USER1").Where(prcs => prcs.TARGET_USERID == id)
                        .ToList().OrderByDescending(prcs => prcs.ID).Select(pr => pr.Map(true)).ToList();
                    return myProcesses;
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public string getEmployeeEmail(decimal? ovedId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository usersRepository = session.GetRepository<IUserRepository>();
                string id = ovedId.ToString();
                USER user = usersRepository.FirstOrDefault(u => u.CALCULATEDIDNUMBER == id);
                return user.EMAIL;
            }
        }
        public string getTargetUsername(decimal userId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository usersRepository = session.GetRepository<IUserRepository>();
                string id = userId.ToString();
                USER user = usersRepository.FirstOrDefault(u => u.CALCULATEDIDNUMBER == id);
                return user.FIRSTNAME + " " + user.LASTNAME;
            }
        }
        public decimal getZehut(decimal TasrgetUserID)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository usersRepository = session.GetRepository<IUserRepository>();
                USER user = usersRepository.FirstOrDefault(u => u.ID == TasrgetUserID);
                return user.IDNUMBER.ToDecimal();
            }
        }
        public int GetRashut(int municipalityId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMunicipalityRepository municipalityRepository = session.GetRepository<IMunicipalityRepository>();
                return municipalityRepository.First(m => m.ID == municipalityId).RASHUT.ToInt();
            }
        }

        #region private
        private DateTime getDate(string date)
        {
            DateTime dt = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            return dt;
        }
        #endregion
    }
}
