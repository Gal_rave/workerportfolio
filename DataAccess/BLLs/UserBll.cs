﻿using System.Collections.Generic;
using System.Linq;
using Catel.Data;
using System;

using DataAccess.Interfaces;
using DataAccess.Mappers;
using DataAccess.Models;
using DataAccess.Extensions;
using Logger;
using HttpCookieHandler;

namespace DataAccess.BLLs
{
    public class UserBll : BllExtender
    {
        public UserModel GetUserByVerificationToken(string confirmationToken, bool withInner = false)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMembershipRepository membershipRepository = session.GetRepository<IMembershipRepository>();
                return membershipRepository.First(m => m.PASSWORDVERIFICATIONTOKEN == confirmationToken
                    && !m.USER.ISDELETED
                    && m.VERIFICATIONTOKENEXPIRATION.HasValue
                    && m.VERIFICATIONTOKENEXPIRATION.Value > DateTime.Now.AddDays(-30)
                    ).USER.Map(withInner);
            }
        }
        public UserModel GetUserByConfirmationToken(string confirmationToken, bool withInner = false)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IMembershipRepository membershipRepository = session.GetRepository<IMembershipRepository>();
                return membershipRepository.First(m => m.CONFIRMATIONTOKEN == confirmationToken && !m.USER.ISDELETED).USER.Map(withInner);
            }
        }
        public UserCredentialsModel UpdateUserDetails(UserModel user)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                USER u = user.Map();
                userRepository.Update(u);
                doSave<USER>(session, u);
                if (u.MEMBERSHIP == null)
                    u = userRepository.First(f => f.ID == u.ID);

                return u.MapCredentials();
            }
        }
        public List<UserImageModel> GetUserImages(int id, string type = null, DateTime? imageDate = null, int? municipalityId = null, string fileName = null)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserImageRepository imageRepository = session.GetRepository<IUserImageRepository>();

                IQueryable<USERIMAGE> images = imageRepository.GetQuery(i => i.USERID == id);
                if (!string.IsNullOrWhiteSpace(type))
                    images = images.Where(i => i.TYPE == type);
                if (imageDate != null && imageDate.HasValue)
                {
                    if (type == "T106")
                        images = images.Where(i => i.FILEDATE.HasValue && i.FILEDATE.Value.Year == imageDate.Value.Year);
                    else
                        images = images.Where(i => i.FILEDATE.HasValue && i.FILEDATE.Value.Year == imageDate.Value.Year && i.FILEDATE.Value.Month == imageDate.Value.Month);
                }
                if (municipalityId.HasValue)
                    images = images.Where(i => i.MUNICIPALITYID.HasValue && i.MUNICIPALITYID.Value == municipalityId.Value);
                if (!string.IsNullOrWhiteSpace(fileName))
                    images = images.Where(i => i.FILENAME == fileName || i.FILENAME.Contains(fileName) || fileName.Contains(i.FILENAME));

                return images.Select(i => i.Map()).ToList();
            }
        }
        public List<UserImageModel> GetUserImages(string idNumber, string type = null, DateTime? imageDate = null, int? municipalityId = null, string fileName = null)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                IQueryable<USERIMAGE> images = userRepository.First(u => u.IDNUMBER == idNumber).USERIMAGES.AsQueryable<USERIMAGE>();
                if (!string.IsNullOrWhiteSpace(type))
                    images = images.Where(i => i.TYPE == type);
                if (imageDate != null && imageDate.HasValue)
                {
                    if (type == "T106")
                        images = images.Where(i => i.FILEDATE.HasValue && i.FILEDATE.Value.Year == imageDate.Value.Year);
                    else
                        images = images.Where(i => i.FILEDATE.HasValue && i.FILEDATE.Value.Year == imageDate.Value.Year && i.FILEDATE.Value.Month == imageDate.Value.Month);
                }
                if (municipalityId.HasValue)
                    images = images.Where(i => i.MUNICIPALITYID.HasValue && i.MUNICIPALITYID.Value == municipalityId.Value);
                if (!string.IsNullOrWhiteSpace(fileName))
                    images = images.Where(i => i.FILENAME == fileName || i.FILENAME.Contains(fileName) || fileName.Contains(i.FILENAME));

                return images.Select(i => i.Map()).ToList();
            }
        }
        public UserImageModel GetUserImage(string idNumber, string type = null, DateTime? imageDate = null, int? municipalityId = null, string fileName = null)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                IQueryable<USERIMAGE> images = userRepository.First(u => u.IDNUMBER == idNumber).USERIMAGES.AsQueryable<USERIMAGE>();
                if (!string.IsNullOrWhiteSpace(type))
                    images = images.Where(i => i.TYPE == type);
                if (imageDate != null && imageDate.HasValue)
                {
                    if (type == "T106")
                        images = images.Where(i => i.FILEDATE.HasValue && i.FILEDATE.Value.Year == imageDate.Value.Year);
                    else
                        images = images.Where(i => i.FILEDATE.HasValue && i.FILEDATE.Value.Year == imageDate.Value.Year && i.FILEDATE.Value.Month == imageDate.Value.Month);
                }
                if (municipalityId.HasValue)
                    images = images.Where(i => i.MUNICIPALITYID.HasValue && i.MUNICIPALITYID.Value == municipalityId.Value);
                if (!string.IsNullOrWhiteSpace(fileName))
                    images = images.Where(i => i.FILENAME == fileName || i.FILENAME.Contains(fileName) || fileName.Contains(i.FILENAME));

                return images.FirstOrDefault().Map();
            }
        }
        public USERIMAGE GetUSERIMAGE(string idNumber, string type = null, DateTime? imageDate = null, int? municipalityId = null, string fileName = null)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();

                IQueryable<USERIMAGE> images = userRepository.First(u => u.IDNUMBER == idNumber).USERIMAGES.AsQueryable<USERIMAGE>();
                if (!string.IsNullOrWhiteSpace(type))
                    images = images.Where(i => i.TYPE == type);
                if (imageDate != null && imageDate.HasValue)
                {
                    if (type == "T106")
                        images = images.Where(i => i.FILEDATE.HasValue && i.FILEDATE.Value.Year == imageDate.Value.Year);
                    else
                        images = images.Where(i => i.FILEDATE.HasValue && i.FILEDATE.Value.Year == imageDate.Value.Year && i.FILEDATE.Value.Month == imageDate.Value.Month);
                }
                if (municipalityId.HasValue)
                    images = images.Where(i => i.MUNICIPALITYID.HasValue && i.MUNICIPALITYID.Value == municipalityId.Value);
                if (!string.IsNullOrWhiteSpace(fileName))
                    images = images.Where(i => i.FILENAME == fileName || i.FILENAME.Contains(fileName) || fileName.Contains(i.FILENAME));

                return images.FirstOrDefault();
            }
        }
        public List<UserImageModel> CreateUserImages(List<UserImageModel> models, string idNumber = null)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserImageRepository imageRepository = session.GetRepository<IUserImageRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                int userId = 0;
                if (!string.IsNullOrWhiteSpace(idNumber))
                {
                    userId = userRepository.First(u => u.IDNUMBER == idNumber).ID.ToInt();
                }
                models.ForEach(i =>
                {
                    if (i.ID > 0)
                    {
                        if (userId > 0) i.userId = userId;
                        imageRepository.Update(i.Map());
                    }
                    else
                    {
                        if (userId > 0) i.userId = userId;
                        imageRepository.Add(i.Map());
                    }
                });

                doSave<USERIMAGE>(session, imageRepository.GetAll());
                return models;
            }
        }
        public UserImageModel CreateUserImage(UserImageModel model, string idNumber = null)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserImageRepository imageRepository = session.GetRepository<IUserImageRepository>();
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    USERIMAGE image;

                    if (model.ID > 0)
                    {
                        image = imageRepository.First(i => i.ID == model.ID);
                        image.FILEBLOB = model.fileBlob;
                        image.FILEDATE = model.fileDate;
                        image.FILEIMAGEBLOB = model.fileimageblob;
                        image.FILENAME = model.fileName;
                        image.MUNICIPALITYID = model.municipalityId;
                        image.TYPE = model.type;
                        if (!string.IsNullOrWhiteSpace(idNumber))
                        {
                            image.USERID = userRepository.First(u => u.IDNUMBER == idNumber).ID.ToInt();
                        }
                        else
                        {
                            image.USERID = model.userId;
                        }

                        imageRepository.Update(image);
                        session.SaveChanges();
                    }
                    else
                    {
                        image = model.Map();
                        if (!string.IsNullOrWhiteSpace(idNumber))
                        {
                            image.USERID = userRepository.First(u => u.IDNUMBER == idNumber).ID.ToInt();
                        }
                        else
                        {
                            image.USERID = model.userId;
                        }

                        imageRepository.Add(image);
                        doSave<USERIMAGE>(session, image);
                    }

                    return image.Map();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public USERIMAGE CreateUserImage(USERIMAGE userImage)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserImageRepository imageRepository = session.GetRepository<IUserImageRepository>();
                    
                    imageRepository.Add(userImage);
                    doSave<USERIMAGE>(session, userImage);

                    return userImage;
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public UserImageModel CreateUpdateUserImage(UserImageModel model, string idNumber = null)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                try
                {
                    IUserImageRepository imageRepository = session.GetRepository<IUserImageRepository>();
                    USERIMAGE image = model.ID > 0 ? imageRepository.FirstOrDefault(i => i.ID == model.ID) :
                        imageRepository.FirstOrDefault(i => i.TYPE == model.type && i.USERID == model.userId && i.FILENAME == model.fileName &&
                            (!i.MUNICIPALITYID.HasValue || !model.municipalityId.HasValue || (i.MUNICIPALITYID.HasValue && model.municipalityId.HasValue && model.municipalityId.Value == i.MUNICIPALITYID.Value)) &&
                            (!i.FILEDATE.HasValue || !model.fileDate.HasValue || (i.FILEDATE.HasValue && model.fileDate.HasValue && i.FILEDATE.Value.Year == model.fileDate.Value.Year)));

                    if (image == null)
                    {
                        image = model.Map();

                        if (!string.IsNullOrWhiteSpace(idNumber))
                        {
                            IUserRepository userRepository = session.GetRepository<IUserRepository>();
                            image.USERID = userRepository.First(u => u.IDNUMBER == idNumber).ID.ToInt();
                        }

                        imageRepository.Add(image);
                        doSave<USERIMAGE>(session, image);
                    }
                    else
                    {
                        image.FILEBLOB = model.fileBlob;
                        image.FILEDATE = model.fileDate;
                        image.FILENAME = model.fileName;
                        if (!image.MUNICIPALITYID.HasValue && model.municipalityId.HasValue)
                            image.MUNICIPALITYID = model.municipalityId;
                        model.ID = image.ID.ToInt();

                        imageRepository.Update(image);
                        session.SaveChanges();
                    }

                    return image.Map();
                }
                catch (Exception ex)
                {
                    throw ex.LogError();
                }
            }
        }
        public UserImageModel SaveForm101Image(UserImageModel model, string idNumber = null)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                USERIMAGE oldImage = new USERIMAGE();
                IUserImageRepository imageRepository = session.GetRepository<IUserImageRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                if (string.IsNullOrWhiteSpace(idNumber) && model.userId <= 0)
                    throw new NullReferenceException("NullUserException");

                if (model.ID > 0)
                {
                    oldImage = imageRepository.First(i => i.ID == model.ID);
                    oldImage.FILEBLOB = model.fileBlob;
                    oldImage.FILEDATE = model.fileDate;
                    oldImage.FILENAME = model.fileName;
                    oldImage.MUNICIPALITYID = model.municipalityId;
                    oldImage.USERID = model.userId;
                    imageRepository.Update(oldImage);
                }
                else
                {
                    ///check if user have an image by this type in this year
                    if (!string.IsNullOrWhiteSpace(idNumber))
                    {
                        model.userId = userRepository.First(u => u.IDNUMBER == idNumber).ID.ToInt();
                    }
                    oldImage = imageRepository.FirstOrDefault(i => i.USERID == model.userId && i.MUNICIPALITYID == model.municipalityId && i.TYPE == model.type && i.FILEDATE.HasValue && model.fileDate.HasValue && i.FILEDATE.Value.Year == model.fileDate.Value.Year);
                    if (oldImage != null)
                    {
                        imageRepository.Delete(oldImage);
                    }
                    oldImage = model.Map();
                    imageRepository.Add(oldImage);
                }

                doSave<USERIMAGE>(session, oldImage);

                return oldImage.Map();
            }
        }
        public UserModel UpdateUserDataFromWS(EmployeeDada employee, int userId)
        {
            try
            {
                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    ICoursRepository coursRepository = session.GetRepository<ICoursRepository>();
                    IChildRepository childRepository = session.GetRepository<IChildRepository>();
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    ISpouseRepository spouseRepository = session.GetRepository<ISpouseRepository>();
                    List<string> cells = new List<string>();

                    USER user = userRepository.First(u => u.ID == userId);
                    if (user == null)
                        return null;

                    AddressModel Address = getAddress(employee.CITYID.ToInt(), employee.SETTLEMENT_NAME, employee.STREETID.ToInt(),
                        employee.STREET_NAME.NullString() ?? employee.ORIGIN_SHEM_REHUV.NullString() ?? employee.SHEM_REHUV.NullString() ?? "");
                    UserModel responseUser = employee.MapUser(Address);
                    SpouseModel responseSpouse = employee.MapSpouse();
                    List<ChildModel> children = employee.MapChildren(userId);
                    List<CoursModel> courses = employee.MapCourses(userId);

                    #region USER
                    user.FIRSTNAME = responseUser.firstName ?? user.FIRSTNAME;
                    user.LASTNAME = responseUser.lastName ?? user.LASTNAME;
                    user.IMMIGRATIONDATE = responseUser.immigrationDate ?? user.IMMIGRATIONDATE;
                    user.BIRTHDATE = responseUser.birthDate ?? user.BIRTHDATE;
                    user.GENDER = responseUser.gender;
                    user.FATHERSNAME = responseUser.fathersName ?? user.FATHERSNAME;
                    user.COUNTRYOFBIRTH = responseUser.countryOfBirth ?? user.COUNTRYOFBIRTH;
                    user.PREVIOUSFIRSTNAME = responseUser.previousFirstName ?? user.PREVIOUSFIRSTNAME;
                    user.PREVIOUSLASTNAME = responseUser.previousLastName ?? user.PREVIOUSLASTNAME;
                    user.HOUSENUMBER = responseUser.houseNumber ?? user.HOUSENUMBER;
                    user.ZIP = responseUser.zip ?? user.ZIP;
                    user.CITY = responseUser.city ?? user.CITY;
                    user.STREET = responseUser.street ?? user.STREET;
                    user.ENTRANCENUMBER = responseUser.entranceNumber ?? user.ENTRANCENUMBER;
                    user.APARTMENTNUMBER = responseUser.apartmentNumber ?? user.APARTMENTNUMBER;
                    user.POBOX = responseUser.poBox ?? user.POBOX;
                    user.MARITALSTATUS = responseUser.maritalStatus ?? user.MARITALSTATUS;
                    user.MARITALSTATUSDATE = responseUser.maritalStatusDate ?? user.MARITALSTATUSDATE;
                    user.CITIZENSHIPTYPE = responseUser.citizenshipType ?? user.CITIZENSHIPTYPE;
                    user.KIBBUTZMEMBER = responseUser.kibbutzMember;
                    user.HMOCODE = responseUser.hmoCode ?? user.HMOCODE;

                    if (!string.IsNullOrWhiteSpace(responseUser.cell))
                        user.CELL = user.CELL.SplitDistinct(responseUser.cell);

                    if (!string.IsNullOrWhiteSpace(responseUser.phone))
                        user.PHONE = user.PHONE.SplitDistinct(responseUser.phone);

                    if (!string.IsNullOrWhiteSpace(responseUser.email))
                        user.EMAIL = user.EMAIL.SplitDistinct(responseUser.email);
                    #endregion USER

                    #region SPOUSE
                    if (responseSpouse != null && !string.IsNullOrWhiteSpace(responseSpouse.firstName))
                    {
                        if (user.SPOUSE != null)
                        {
                            user.SPOUSE.JOBTITLE = responseSpouse.jobTitle ?? user.SPOUSE.JOBTITLE;
                            user.SPOUSE.INCOMETYPE = responseSpouse.incomeType ?? user.SPOUSE.INCOMETYPE;
                            user.SPOUSE.IDNUMBER = responseSpouse.idNumber ?? user.SPOUSE.IDNUMBER;
                            user.SPOUSE.CALCULATEDIDNUMBER = responseSpouse.calculatedIdNumber ?? user.SPOUSE.CALCULATEDIDNUMBER;
                            user.SPOUSE.CELL = responseSpouse.cell ?? user.SPOUSE.CELL;
                            user.SPOUSE.HASINCOME = responseSpouse.hasIncome ?? user.SPOUSE.HASINCOME;
                            user.SPOUSE.FIRSTNAME = responseSpouse.firstName ?? user.SPOUSE.FIRSTNAME;
                            user.SPOUSE.LASTNAME = responseSpouse.lastName ?? user.SPOUSE.LASTNAME;
                            user.SPOUSE.BIRTHDATE = responseSpouse.birthDate ?? user.SPOUSE.BIRTHDATE;
                            user.SPOUSE.GENDER = responseSpouse.gender;
                            user.SPOUSE.MARITALSTATUS = responseSpouse.maritalStatus ?? user.SPOUSE.MARITALSTATUS;
                            user.SPOUSE.MARITALSTATUSDATE = responseSpouse.maritalStatusDate ?? user.SPOUSE.MARITALSTATUSDATE;

                            if (!string.IsNullOrWhiteSpace(employee.PARTNER_PHONE_NUMBER) && !string.IsNullOrWhiteSpace(employee.PARTNER_PHONE_PREFIX))
                            {
                                user.SPOUSE.PHONE = user.SPOUSE.PHONE.SplitDistinct(string.Join("{0}{1}", employee.PARTNER_PHONE_PREFIX.prefixPhoneNumber(), employee.PARTNER_PHONE_NUMBER));
                            }
                            user.SPOUSE.ISSPOUSE = true;
                        }
                        if (user.SPOUSE == null && !string.IsNullOrWhiteSpace(employee.PARTNER_FIRST_NAME))
                        {

                            if (!string.IsNullOrWhiteSpace(employee.PARTNER_PHONE_NUMBER) && !string.IsNullOrWhiteSpace(employee.PARTNER_PHONE_PREFIX))
                            {
                                responseSpouse.phone = responseSpouse.phone.SplitDistinct(string.Join("{0}{1}", employee.PARTNER_PHONE_PREFIX.prefixPhoneNumber(), employee.PARTNER_PHONE_NUMBER));
                            }

                            if (user.ID > 0) responseSpouse.spouseId = user.ID;
                            user.SPOUSE = responseSpouse.Map();
                            spouseRepository.Add(responseSpouse.Map());
                        }
                    }
                    #endregion SPOUSE

                    #region CHILDREN
                    List<Child> userschildren = childRepository.GetQuery(c => c.USERID == user.ID).ToList();
                    ///delete childrens and re-create new;
                    userschildren.ForEach(c =>
                    {
                        string idNumber = IdNumberExtender.CheckAndGenerateFullIdNumber(c.IDNUMBER);
                        ChildModel child = children != null ? children.FirstOrDefault(cd => cd.idNumber == c.IDNUMBER) : null;
                        if (child == null)
                        {
                            childRepository.Delete(c);
                        }
                        else
                        {
                            c.FIRSTNAME = child.firstName;
                            c.HOMELIVING = child.homeLiving;
                            c.BIRTHDATE = child.birthDate;
                            c.GENDER = child.gender;

                            children.Remove(child);
                            childRepository.Update(c);
                        }
                    });
                    if (children != null && children.Count > 0)
                        children.ForEach(c => childRepository.Add(c.Map()));
                    #endregion CHILDREN

                    #region COURS
                    ///delete courses and re-create new;
                    coursRepository.GetQuery(co => co.USERID == user.ID).ToList().ForEach(c =>
                    {
                        coursRepository.Delete(c);
                    });
                    if (courses != null && courses.Count > 0)
                        courses.ForEach(c => coursRepository.Add(c.Map()));
                    #endregion COURS

                    session.SaveChanges();
                    return user.Map();
                }
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public UserModel UpdateUserDataFromWS(ResponsePersonalInformation response, UserModel usermodel)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ICoursRepository coursRepository = session.GetRepository<ICoursRepository>();
                IChildRepository childRepository = session.GetRepository<IChildRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                ISpouseRepository spouseRepository = session.GetRepository<ISpouseRepository>();

                USER user = userRepository.FirstOrDefault(u => !u.ISDELETED && u.IDNUMBER == usermodel.idNumber);
                UserModel responseUser = response.MapUser();
                SpouseModel spouse = response.MapSpouse();
                List<ChildModel> children = response.MapChildren();
                List<CoursModel> courses = response.MapCourses();
                if (user == null)
                    throw new MembershipException("UpdateUserNullException", 62);

                #region USER
                ///update user
                if (!string.IsNullOrWhiteSpace(responseUser.lastName)) user.LASTNAME = responseUser.lastName;
                if (!string.IsNullOrWhiteSpace(responseUser.firstName)) user.FIRSTNAME = responseUser.firstName;
                if (!string.IsNullOrWhiteSpace(responseUser.fathersName)) user.FATHERSNAME = responseUser.fathersName;
                if (!string.IsNullOrWhiteSpace(responseUser.previousLastName)) user.PREVIOUSLASTNAME = responseUser.previousLastName;
                if (!string.IsNullOrWhiteSpace(responseUser.previousFirstName)) user.PREVIOUSFIRSTNAME = responseUser.previousFirstName;
                if (!string.IsNullOrWhiteSpace(responseUser.city)) user.CITY = responseUser.city;
                if (!string.IsNullOrWhiteSpace(responseUser.street)) user.STREET = responseUser.street;
                if (!string.IsNullOrWhiteSpace(responseUser.entranceNumber)) user.ENTRANCENUMBER = responseUser.entranceNumber;
                if (!string.IsNullOrWhiteSpace(responseUser.zip)) user.ZIP = responseUser.zip;
                if (!string.IsNullOrWhiteSpace(responseUser.poBox)) user.POBOX = responseUser.poBox;
                if (!string.IsNullOrWhiteSpace(responseUser.phone)) user.PHONE = responseUser.phone;
                if (!string.IsNullOrWhiteSpace(responseUser.cell)) user.CELL = responseUser.cell;
                if (!string.IsNullOrWhiteSpace(responseUser.countryOfBirth)) user.COUNTRYOFBIRTH = responseUser.countryOfBirth;

                if (responseUser.birthDate.HasValue) user.BIRTHDATE = responseUser.birthDate;
                if (responseUser.immigrationDate.HasValue) user.IMMIGRATIONDATE = responseUser.immigrationDate;
                if (responseUser.maritalStatusDate.HasValue) user.MARITALSTATUSDATE = responseUser.maritalStatusDate;

                user.GENDER = responseUser.gender;

                if (responseUser.apartmentNumber.HasValue) user.APARTMENTNUMBER = responseUser.apartmentNumber;
                if (responseUser.houseNumber.HasValue) user.HOUSENUMBER = responseUser.houseNumber;
                if (responseUser.maritalStatus.HasValue) user.MARITALSTATUS = responseUser.maritalStatus;
                #endregion USER

                #region SPOUSE
                if (user.SPOUSE != null && user.SPOUSE.ISDELETED)
                {
                    spouseRepository.Delete(user.SPOUSE);
                    user.SPOUSE = null;
                }
                if (user.SPOUSE != null)
                {
                    ///update spouse
                    if (!string.IsNullOrWhiteSpace(spouse.firstName)) user.SPOUSE.FIRSTNAME = spouse.firstName;
                    if (!string.IsNullOrWhiteSpace(spouse.idNumber)) user.SPOUSE.IDNUMBER = spouse.idNumber;
                    if (!string.IsNullOrWhiteSpace(spouse.phone)) user.SPOUSE.PHONE = spouse.phone;
                    if (!string.IsNullOrWhiteSpace(spouse.cell)) user.SPOUSE.CELL = spouse.cell;
                    if (!string.IsNullOrWhiteSpace(spouse.incomeType)) user.SPOUSE.INCOMETYPE = spouse.incomeType;

                    user.SPOUSE.HASINCOME = spouse.hasIncome;
                    user.SPOUSE.GENDER = spouse.gender;

                    if (spouse.birthDate.HasValue) user.SPOUSE.BIRTHDATE = spouse.birthDate;
                    if (spouse.maritalStatusDate.HasValue) user.SPOUSE.MARITALSTATUSDATE = spouse.maritalStatusDate;

                    if (spouse.maritalStatus.HasValue) user.SPOUSE.MARITALSTATUS = spouse.maritalStatus;
                }
                else if (user.SPOUSE == null)
                {
                    ///create spouse
                    SPOUSE _SPOUSE = spouse.Map();
                    _SPOUSE.HASINCOME = spouse.hasIncome;
                    _SPOUSE.INCOMETYPE = spouse.incomeType;
                    _SPOUSE.ISSPOUSE = true;
                    user.ISSPOUSE = false;

                    if (_SPOUSE.ID > 0) user.SPOUSEID = _SPOUSE.ID;
                    if (user.ID > 0) _SPOUSE.SPOUSEID = user.ID;
                    user.SPOUSE = _SPOUSE;
                    _SPOUSE.USER = user;

                    _SPOUSE.MARITALSTATUS = spouse.maritalStatus;
                    _SPOUSE.MARITALSTATUSDATE = spouse.maritalStatusDate;
                    _SPOUSE.GENDER = spouse.gender;

                    spouseRepository.Add(_SPOUSE);
                }
                #endregion SPOUSE

                #region CHILDREN
                List<Child> userschildren = childRepository.GetQuery(c => c.USERID == user.ID).ToList();
                ///delete childrens and re-create new;
                userschildren.ForEach(c =>
                {
                    childRepository.Delete(c);
                });
                children.ForEach(c =>
                {
                    childRepository.Add(c.Map(user.ID.ToInt()));
                });
                #endregion CHILDREN

                #region COURS
                List<COURS> usercourses = coursRepository.GetQuery(co => co.USERID == user.ID).ToList();
                ///delete courses and re-create new;
                usercourses.ForEach(c =>
                {
                    coursRepository.Delete(c);
                });
                courses.ForEach(c =>
                {
                    coursRepository.Add(c.Map(user.ID.ToInt()));
                });
                #endregion COURS

                session.SaveChanges();
                return user.Map();
            }
        }
        public UserModel GetFullUserdetails(string idNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                return userRepository.First(u => !u.ISDELETED && u.IDNUMBER == idNumber).Map(true);
            }
        }
        public SpouseModel GetPartnerDetails(string idNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                return userRepository.First(u => !u.ISDELETED && u.IDNUMBER == idNumber).SPOUSE.Map();
            }
        }
        public List<CoursModel> GetUserEducation(string idNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                MembershipProviderBll bll = new MembershipProviderBll();
                UserModel user = bll.GetUserByIdNumber(idNumber);
                if (user == null)
                    throw new NullReferenceException("NullUserReference");

                ICoursRepository coursRepository = session.GetRepository<ICoursRepository>();
                return coursRepository.GetQuery(c => c.USERID == user.ID).ToList().Select(c => c.Map(false)).ToList();
            }
        }
        public List<ChildModel> GetUserChildren(string idNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                MembershipProviderBll bll = new MembershipProviderBll();
                UserModel user = bll.GetUserByIdNumber(idNumber);
                if (user == null)
                    throw new NullReferenceException("NullUserReference");

                IChildRepository childRepository = session.GetRepository<IChildRepository>();
                return childRepository.GetQuery(c => c.USERID == user.ID).ToList().Select(c => c.Map()).ToList().OrderBy(c => c.birthDate).ToList();
            }
        }
        public void UpdateLoginLog(int userId, int currentMunicipality, string action = "login", string ip = "")
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ILoginLogRepository loginLogRepository = session.GetRepository<ILoginLogRepository>();
                LOGINLOG log = new LOGINLOG
                {
                    ACTION = action,
                    ACTIONIP = ip,
                    ACTIONDATE = DateTime.Now,
                    USERID = userId,
                    MUNICIPALITYID = currentMunicipality
                };
                loginLogRepository.Add(log);
                session.SaveChanges();
            }
        }
        public void UpdateLoginLog(string idNumber, int currentMunicipality, string action = "login", string ip = "")
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ILoginLogRepository loginLogRepository = session.GetRepository<ILoginLogRepository>();
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                USER user = userRepository.First(u => u.IDNUMBER == idNumber);
                int userid = user.ID.ToInt();
                LOGINLOG log = new LOGINLOG
                {
                    ACTION = action,
                    ACTIONIP = ip,
                    ACTIONDATE = DateTime.Now,
                    USERID = userid,
                    MUNICIPALITYID = currentMunicipality
                };
                loginLogRepository.Add(log);
                if (user.INITIALCHECK)
                {
                    user.INITIALCHECK = false;
                    userRepository.Update(user);
                }
                session.SaveChanges();
            }
        }
        public UserModel GetUserByID(int id, bool withInner = true)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                return userRepository.First(u => !u.ISDELETED && u.ID == id).Map(withInner);
            }
        }
        public UserModel GetUserByID(string idNumber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                return userRepository.First(u => !u.ISDELETED && u.IDNUMBER == idNumber).Map();
            }
        }
        public UserImageModel GetImageByID(int imageId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserImageRepository userImageRepository = session.GetRepository<IUserImageRepository>();
                return userImageRepository.First(i => i.ID == imageId).Map();
            }
        }
        public string UpdateUserSeventyNineEvent(UserEventModel model)
        {
            try
            {
                string _idnumber = IdNumberExtender.CheckAndGenerateFullIdNumber(model.idNumber);

                using (var session = new UnitOfWork<PortfolioEntities>())
                {
                    IUserRepository userRepository = session.GetRepository<IUserRepository>();
                    USER user = userRepository.FirstOrDefault(u => u.IDNUMBER == _idnumber && u.CALCULATEDIDNUMBER == model.idNumber);
                    if (user == null)
                        return "NullUserException";
                    user.ISALLOW101 = null;
                    if (model.isAllow101)
                        user.ISALLOW101 = false;
                    session.SaveChanges();
                }
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.LogError(ex.ToString());
            }
        }
        public PayCheckUsersByMailModel UpdateUserPayCheckToEmailStatus(WSPostModel model)
        {
            PAYCHECKUSERSBYMAIL new_pc = null;
            PAYCHECKUSERSBYMAIL pc = null;
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                IEmailByMunicipalityRepository emailByMunicipalityRepository = session.GetRepository<IEmailByMunicipalityRepository>();
                if (!model.isMobileRequest.HasValue)
                    return null;

                USER user = userRepository.FirstOrDefault(u => u.IDNUMBER == model.idNumber);
                if (user == null)
                    return null;

                pc = user.PAYCHECKUSERSBYMAILs.FirstOrDefault(p => p.ACTIVERECORD && p.MUNICIPALITYID == model.currentMunicipalityId);
                if (pc != null)
                {
                    ///no chenge in status
                    if (pc.SENDMAIL == model.isMobileRequest.Value)
                    {
                        pc.UPDATEDATE = DateTime.Now;
                        pc.CREATIONSOURCE = false;
                    }
                    else
                    {
                        pc.ACTIVERECORD = false;
                        pc.UPDATEDATE = DateTime.Now;

                        new_pc = new PAYCHECKUSERSBYMAIL
                        {
                            ACTIVERECORD = true,
                            SENDMAIL = model.isMobileRequest.Value,
                            CREATEDDATE = DateTime.Now,
                            CREATIONSOURCE = false,
                            MUNICIPALITYID = model.currentMunicipalityId,
                            UPDATEDATE = DateTime.Now,
                            USERID = user.ID
                        };
                        user.PAYCHECKUSERSBYMAILs.Add(new_pc);
                    }
                }
                else
                {
                    new_pc = new PAYCHECKUSERSBYMAIL
                    {
                        ACTIVERECORD = true,
                        SENDMAIL = model.isMobileRequest.Value,
                        CREATEDDATE = DateTime.Now,
                        CREATIONSOURCE = false,
                        MUNICIPALITYID = model.currentMunicipalityId,
                        UPDATEDATE = DateTime.Now,
                        USERID = user.ID
                    };
                    user.PAYCHECKUSERSBYMAILs.Add(new_pc);
                }

                doSave<USER>(session, user);

                emailByMunicipalityRepository.ConnectEmailByMunicipality();

                return new_pc != null ? new_pc.Map() : (pc != null ? pc.Map() : null);
            }
        }
        public bool UpdateUserPayCheckToEmailStatus(Tlush_MailModel model)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                IEmailByMunicipalityRepository emailByMunicipalityRepository = session.GetRepository<IEmailByMunicipalityRepository>();

                string idNumber = IdNumberExtender.CheckAndGenerateFullIdNumber(model.ZEHUT.ToString());
                USER user = userRepository.FirstOrDefault(u => u.IDNUMBER == idNumber);

                if (user == null)
                {
                    Log.PcuLog(new { _idNumber = idNumber, _ZEHUT = model.ZEHUT.ToString(), rashut = model.RASHUT, rashut_sachar = model.RASHUT_SACHAR }, "Test 1 UpdateUserPayCheckToEmailStatus user is null");
                    idNumber = IdNumberExtender.GenerateFullIdNumber(model.ZEHUT.ToString());
                    user = userRepository.FirstOrDefault(u => u.IDNUMBER == idNumber);
                    if (user == null)
                    {
                        Log.PcuLog(new { _idNumber = idNumber, _ZEHUT = model.ZEHUT.ToString(), rashut = model.RASHUT, rashut_sachar = model.RASHUT_SACHAR }, "Test 2 UpdateUserPayCheckToEmailStatus user is null");
                        return false;
                    }
                }

                decimal municipalityid = model.RASHUT_SACHAR.ToDecimal();
                PAYCHECKUSERSBYMAIL new_pc;
                PAYCHECKUSERSBYMAIL pc = user.PAYCHECKUSERSBYMAILs.FirstOrDefault(p => p.ACTIVERECORD && p.MUNICIPALITYID == municipalityid);
                if (pc != null)
                {
                    ///local record is newer => do nothing
                    if (pc.UPDATEDATE >= model.UPDATE_DATE)
                        return true;

                    ///no chenge in status
                    if (pc.SENDMAIL == model.SEND_MAIL)
                    {
                        pc.UPDATEDATE = model.UPDATE_DATE;
                        pc.CREATIONSOURCE = true;
                    }
                    else
                    {
                        pc.ACTIVERECORD = false;
                        pc.UPDATEDATE = DateTime.Now;

                        new_pc = new PAYCHECKUSERSBYMAIL
                        {
                            ACTIVERECORD = true,
                            SENDMAIL = model.SEND_MAIL,
                            CREATEDDATE = DateTime.Now,
                            CREATIONSOURCE = true,
                            MUNICIPALITYID = municipalityid,
                            UPDATEDATE = model.UPDATE_DATE,
                            USERID = user.ID
                        };
                        user.PAYCHECKUSERSBYMAILs.Add(new_pc);
                    }
                }
                else
                {
                    new_pc = new PAYCHECKUSERSBYMAIL
                    {
                        ACTIVERECORD = true,
                        SENDMAIL = model.SEND_MAIL,
                        CREATEDDATE = DateTime.Now,
                        CREATIONSOURCE = true,
                        MUNICIPALITYID = municipalityid,
                        UPDATEDATE = model.UPDATE_DATE,
                        USERID = user.ID
                    };
                    user.PAYCHECKUSERSBYMAILs.Add(new_pc);
                }

                session.SaveChanges();
                emailByMunicipalityRepository.ConnectEmailByMunicipality();

                return true;
            }
        }
        public List<UserImageModel> GetUserImagesForCreateMobileImages(int counter, string type = null)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserImageRepository userImageRepository = session.GetRepository<IUserImageRepository>();
                IQueryable<USERIMAGE> images = userImageRepository.GetQuery(i => i.FILEIMAGEBLOB == null).OrderByDescending(i => i.ID);
                if (!string.IsNullOrWhiteSpace(type))
                {
                    type = type.ToUpper();
                    images = images.Where(i => i.TYPE == type);
                }
                counter = counter > 1 ? counter : 100;
                return images.Take(counter).ToList().Select(i => i.Map()).ToList();
            }
        }
        public UPCEdataHolder GetUserUPCEData(string idNUmber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                int municipalityId = CookieExtender.GetCurrentMunicipalityId(idNUmber);

                IEmailByMunicipalityRepository emailByMunicipalityRepository = session.GetRepository<IEmailByMunicipalityRepository>();
                IPayCheckUsersByMailRepository payCheckUsersByMailRepository = session.GetRepository<IPayCheckUsersByMailRepository>();

                List<EMAILBYMUNICIPALITY> mails = emailByMunicipalityRepository.GetQuery(e => e.MUNICIPALITYID == municipalityId && e.USER != null && e.USER.IDNUMBER == idNUmber).OrderByDescending(e => e.ID).ToList();
                EmailByMunicipalityModel email = mails.FirstOrDefault().Map();
                if (email != null)
                {
                    email.email = string.Join(", ", mails.Select(e => e.EMAIL).ToList());
                }

                PayCheckUsersByMailModel pc = payCheckUsersByMailRepository.FirstOrDefault(p => p.MUNICIPALITYID == municipalityId && p.USER != null && p.USER.IDNUMBER == idNUmber && p.ACTIVERECORD).Map(true);

                return new UPCEdataHolder
                {
                    Email = email,
                    PayCheck = pc
                };
            }
        }
        public int FindUserUpdateTypes(string idNUmber)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserRepository userRepository = session.GetRepository<IUserRepository>();
                USER user = userRepository.FirstOrDefault(u => u.IDNUMBER == idNUmber);
                if (user == null)
                    return -1;
                if (!string.IsNullOrWhiteSpace(user.CELL) && !string.IsNullOrWhiteSpace(user.EMAIL))
                    return 4;
                if (!string.IsNullOrWhiteSpace(user.CELL))
                    return 1;
                if (!string.IsNullOrWhiteSpace(user.EMAIL))
                    return 2;
            }
            return -1;
        }
        public void RemoveImageById(int imageId)
        {
            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                IUserImageRepository imageRepository = session.GetRepository<IUserImageRepository>();
                USERIMAGE img = imageRepository.FirstOrDefault(ui => ui.ID == imageId);
                if (img != null)
                {
                    imageRepository.Delete(img);
                    session.SaveChanges();
                }
            }
        }

        private AddressModel getAddress(int cityId, string cityQuery, int streetId, string streetQuery)
        {
            AddressModel address = new AddressModel();
            if (string.IsNullOrWhiteSpace(cityQuery) || string.IsNullOrWhiteSpace(streetQuery))
                return findCity(cityId, cityQuery, streetId, streetQuery);

            address.CityName = cityQuery;
            address.StreetName = streetQuery;

            return address;
        }
        private AddressModel findCity(int cityId, string cityQuery, int streetId, string streetQuery)
        {
            AddressModel address = new AddressModel();

            using (var session = new UnitOfWork<PortfolioEntities>())
            {
                ICityRepository cityRepository = session.GetRepository<ICityRepository>();
                IStreetRepository streetRepository = session.GetRepository<IStreetRepository>();
                ///find city, first try by id, if not by name
                CITy city = cityRepository.Include("STREETS").FirstOrDefault(c => c.ID == cityId);
                if (city == null)
                {
                    city = cityRepository.Include("STREETS").FirstOrDefault(c => c.NAME == cityQuery) ??
                                cityRepository.Include("STREETS").FirstOrDefault(c => c.NAME == cityQuery || cityQuery.Contains(c.NAME) || c.NAME.Contains(cityQuery));
                    if (city == null)
                    {
                        city = new CITy
                        {
                            ID = cityId,
                            NAME = cityQuery
                        };
                    }
                }
                ///now get the street, by id or name
                STREET street = (streetId > 0 && streetId != 9999) ?
                                    city.STREETS.FirstOrDefault(s => s.ID == streetId) :
                                    city.STREETS.FirstOrDefault(s => s.NAME == streetQuery) ??
                                    city.STREETS.FirstOrDefault(s => s.NAME == streetQuery || streetQuery.Contains(s.NAME) || s.NAME.Contains(streetQuery));
                if (street == null)
                {
                    street = city.STREETS.FirstOrDefault(s => s.NAME == streetQuery) ??
                             city.STREETS.FirstOrDefault(s => s.NAME == streetQuery || streetQuery.Contains(s.NAME) || s.NAME.Contains(streetQuery));
                }

                if (street == null)
                {
                    street = (streetId > 0 && streetId != 9999) ?
                        streetRepository.FirstOrDefault(s => s.ID == streetId) :
                        streetRepository.FirstOrDefault(s => s.NAME == streetQuery) ??
                        streetRepository.FirstOrDefault(s => s.NAME == streetQuery || streetQuery.Contains(s.NAME) || s.NAME.Contains(streetQuery))
                        ;
                }
                if (street == null)
                {
                    street = new STREET
                    {
                        ID = streetId,
                        NAME = streetQuery
                    };
                }
                address.city = city.Map(false);
                address.street = street.Map(false);

                return address;
            }
        }
    }
}