﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace DataAccess.Extensions
{
    public static class ConversionExtensions
    {
        private static Regex numbersRegex = new Regex("[^0-9]");
        public static int ToInt(this object model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.ToString()))
                return 0;

            int num;
            if (Int32.TryParse(model.ToString(), out num))
                return num;
            else
                return 0;
        }
        public static int? NullableInt(this object model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.ToString()))
                return null;

            int num;
            if (Int32.TryParse(model.ToString(), out num))
                return num;
            else
                return null;
        }
        public static bool ToBool(this object model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.ToString()))
                return false;

            bool num;
            if (bool.TryParse(model.ToString(), out num))
                return num;
            else
                return false;
        }
        public static double ToDouble(this object model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.ToString()))
                return 0;

            double num;
            if (double.TryParse(model.ToString(), out num))
                return num;
            else
                return 0;
        }
        public static decimal ToDecimal(this object model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.ToString()))
                return 0;

            decimal num;
            if (decimal.TryParse(model.ToString(), out num))
                return num;
            else
                return 0;
        }
        public static byte[] ToByteArray(this string model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model))
                return new byte[0];
            return Convert.FromBase64String(model);
        }
        public static string ByteArrayToString(this byte[] model)
        {
            if (model == null || model.Length < 1)
                return string.Empty;
            return Convert.ToBase64String(model);
        }
        public static List<int> ToIntList(this string str, char split = ',')
        {
            if (string.IsNullOrWhiteSpace(str))
                return new List<int>();
            return char.IsWhiteSpace(split) ? str.ToCharArray().Select(n => n.ToInt()).ToList() : str.Split(split).Select(n => n.ToInt()).ToList();
        }
        public static Int16 ToShort(this object val)
        {
            if (val == null || string.IsNullOrWhiteSpace(val.ToString()))
                return 0;

            switch (Type.GetTypeCode(val.GetType()))
            {
                case TypeCode.Byte:
                case TypeCode.Single:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.SByte:
                case TypeCode.Int64:
                case TypeCode.Int16:
                case TypeCode.Double:
                case TypeCode.Decimal:
                case TypeCode.Int32:
                    return Int16.Parse(val.ToString());

                case TypeCode.Boolean:
                    return val.ToBool() ? (Int16)1 : (Int16)0;

                case TypeCode.Char:
                case TypeCode.String:
                    return string.IsNullOrWhiteSpace(val.ToString()) ? (Int16)0 : (Int16)1;

                case TypeCode.DateTime:
                    return 0;
                case TypeCode.Empty:
                    return 0;

                default:
                    return 0;
            }
        }
        public static string StringTrim(this object val)
        {
            string res = string.Empty;
            if (val == null)
                return res;
            res = val.ToString();
            if (!string.IsNullOrWhiteSpace(res))
                return res.Trim();
            return res.Trim();
        }
        public static string NullString(this object val)
        {
            if (val == null)
                return null;
            string res = val.ToString();
            if (!string.IsNullOrWhiteSpace(res))
                return res.Trim();
            return null;
        }
        public static string prefixPhoneNumber(this object number)
        {
            string retVal = string.Empty;
            if (number == null || numbersRegex.Replace(number.ToString(), "").ToDecimal() == 0)
                return retVal;
            retVal = numbersRegex.Replace(number.ToString(), "");
            if (retVal.StartsWith("0"))
                return retVal;
            return string.Format("{0}{1}", "0", retVal);
        }
        public static string GetPhonePrefix(this object number, bool isCell = false)
        {
            string phone = number.prefixPhoneNumber();
            return isCell ? phone.Substring(0, 3) : phone.Substring(0, 2);
        }
        public static string GetPhoneNumber(this object number, bool isCell = false)
        {
            if (number == null)
                return string.Empty;

            if(number != null && number.ToString().IndexOf(',') > -1)
            {
                number = number.ToString().Split(',').First();
            }
            
            string phone = number.prefixPhoneNumber();
            if ((isCell && phone.Length < 3) || (!isCell && phone.Length < 2))
                return string.Empty;

            return isCell ? phone.Substring(3, phone.Length - 3) : phone.Substring(2, phone.Length - 2);
        }
        public static DateTime? ParseExactDate(this object date)
        {
            if (date == null || string.IsNullOrWhiteSpace(date.ToString()))
                return null;

            try
            {
                DateTime dt = DateTime.ParseExact(date.ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                return dt;
            }
            catch
            {
                try
                {
                    DateTime dt = DateTime.ParseExact(date.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    return dt;
                }
                catch { }
            }
            return null;
        }
        public static string Reverse(this string input)
        {
            return new string(input.ToCharArray().Reverse().ToArray());
        }
        public static string Translate(this string input)
        {
            input = input.Replace("A", "ב").Replace("B", "ג").Replace("C", "ד").Replace("D", "ה").Replace("E", "ו").Replace("F", "ז").Replace("G", "ח").Replace("H", "ט").Replace("I", "י")
                .Replace("J", "ך").Replace("K", "כ").Replace("L", "ל").Replace("M", "ם").Replace("N", "מ").Replace("O", "ן").Replace("P", "נ").Replace("Q", "ס").Replace("R", "ע").Replace("S", "ף")
                .Replace("T", "פ").Replace("U", "ץ").Replace("V", "צ").Replace("W", "ק").Replace("X", "ר").Replace("Y", "ש").Replace("Z", "ת");
            return input;
        }
        public static IEnumerable<int> AllIndexesOf(this string str, string value)
        {
            if (String.IsNullOrEmpty(value))
                throw new ArgumentException("the string to find may not be empty", "value");
            for (int index = 0; ; index += value.Length)
            {
                index = str.IndexOf(value, index);
                if (index == -1)
                    break;
                yield return index;
            }
        }
        public static IEnumerable<int> AllIndexesOf(this string str, char value)
        {
            for (int index = 0; ; index++)
            {
                index = str.IndexOf(value, index);
                if (index == -1)
                    break;
                yield return index;
            }
        }
        public static string SpecialCharacters(this string input)
        {
            Regex rgx = new Regex("[A-zא-ת0-9 _.-]");
            char[] characters = rgx.Replace(input, "").ToCharArray();
            int index = -1;
            int length = input.Length - 1;
            foreach (char chr in characters)
            {
                foreach (int inx in input.AllIndexesOf(chr))
                {
                    index = length - inx;
                    if (index < 0) { }
                }                
            }
            return input;
        }
        public static string GetThreeDigitsMifal(this int mifal)
        {
            string _mifal = mifal.ToString();
            while (_mifal.Length < 3)
                _mifal = "0" + _mifal;
            return _mifal;
        }
        public static List<decimal> IntsToDecimals(this List<int> list)
        {
            return list.Select(n => n.ToDecimal()).ToList();
        }
        public static string PaycheckDate(this DateTime date)
        {
            return string.Format("{0}{1}", date.Year, date.Month);
        }
        public static string Pad(this object val, int length = 0, string adder = "0")
        {
            if (val == null || string.IsNullOrWhiteSpace(val.ToString()))
                return string.Empty;
            string _val = val.ToString();
            while (_val.Length < length)
                _val = adder + _val;

            return _val;
        }
        public static string NullableString(this object str)
        {
            if (str == null || string.IsNullOrWhiteSpace(str.ToString()))
                return string.Empty;
            return str.StringTrim();
        }
        public static string DeductionFileID(this string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return string.Empty;
            int nine = str.Substring(0, 1).ToInt();
            if (nine == 9)
                return str.Substring(1, (str.Length - 1));
            return str;
        }
    }
}
