﻿using System.Linq;
using System.Data.Entity;

using Catel.Data;
using System;
using System.Collections.Generic;

namespace DataAccess.Extensions
{
    public static class DataExtensions
    {
        private static readonly string[] maritalStatusStrings = new string[] { "ErrorStatus", "רווק/ה", "נשוי/ה", "גרוש/ה", "אלמן/ה", "פרוד/ה" };
        private static readonly int[] maritalStatusNumbers = new int[] { 0, 1, 2, 3, 4, 5 };

        private static readonly Dictionary<int, string> hmos = new Dictionary<int, string>() { { 2, "כללית" }, { 3, "לאומית" }, { 4, "מכבי" }, { 23, "מאוחדת" } };

        public static IQueryable<T> MultipleInclude<T>(this IQueryable<T> query, params string[] navProperties) where T : class
        {
            foreach (var navProperty in navProperties)
                query = query.Include(navProperty);

            return query;
        }
        public static int RandomNumber(int? min = null, int? max = null)
        {
            Random rnd = new Random();
            int card = rnd.Next((min.HasValue? min.Value : 7), (max.HasValue ? max.Value : 77));
            return card;
        }
        public static string SplitDistinct(this string originField, string addedField, char split = ',')
        {
            if (string.IsNullOrWhiteSpace(originField))
                originField = string.Empty;
            if (string.IsNullOrWhiteSpace(addedField))
                addedField = string.Empty;

            List<string> data = originField.Split(split).ToList();
            data.AddRange(addedField.Split(split).ToList().Distinct().ToList());
            data = data.Select(c => c.Trim()).Where(c => !string.IsNullOrWhiteSpace(c)).ToList().Distinct().ToList();
            originField = string.Join(split.ToString(), data);
            return originField;
        }
        public static int GetMaritalStatus(this string maritalStatus)
        {
            return Array.IndexOf(maritalStatusStrings, maritalStatus);
        }
        public static string GetHmoName(this int? hmoCode)
        {
            try
            {
                string hmo = "";
                hmos.TryGetValue(hmoCode.ToInt(), out hmo);
                return hmo;
            }
            catch
            {
                return "";
            }
        }
    }
}
