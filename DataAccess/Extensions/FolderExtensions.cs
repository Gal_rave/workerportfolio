﻿using System;
using System.IO;

using Logger;

namespace DataAccess.Extensions
{
    public static class FolderExtensions
    {
        public static void DeleteFile(string file)
        {
            if (string.IsNullOrWhiteSpace(file))
                return;

            try
            {
                if (File.Exists(file))
                    File.Delete(file);
            }
            catch (Exception ex)
            {
                throw ex.LogError();
            }
        }
        public static bool CreateFolder(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return false;

            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }
        public static void ReNameFile(string newName, string oldName)
        {
            DeleteFile(newName);
            File.Move(oldName, newName);
            DeleteFile(oldName);
        }
        public static byte[] ReadFile(string file)
        {
            return File.ReadAllBytes(file);
        }
        public static bool DeletFolder(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return false;

            if (Directory.Exists(path))
            {
                try
                {
                    Directory.Delete(path, true);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }
        public static void CleanDirectory(string parentDirectory, int? time = null)
        {
            if (!Directory.Exists(parentDirectory))
            {
                CreateFolder(parentDirectory);
            }
            string[] subdirectoryEntries = Directory.GetDirectories(parentDirectory);
            foreach (string subdirectory in subdirectoryEntries)
            {
                if (Directory.Exists(subdirectory) && time.HasValue ? (Directory.GetCreationTime(subdirectory) < DateTime.Now.AddMinutes(time.Value)) : true)
                {
                    Directory.Delete(subdirectory, true);
                }
            }
            subdirectoryEntries = Directory.GetFiles(parentDirectory);
            foreach (string file in subdirectoryEntries)
            {
                File.Delete(file);
            }
        }
        public static void CreateEmptyFolder(string path)
        {
            DeletFolder(path);
            CreateFolder(path);
        }
    }
}
