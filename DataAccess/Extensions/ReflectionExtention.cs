﻿using DataAccess.Models;
using Logger;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Extensions
{
    public static class ReflectionExtention
    {
        public static object PopulatObject<T>(this object obj, object propertyValues = null)
        {
            List<string> props = obj.GetProps();
            List<string> vals = propertyValues != null ? propertyValues.GetProps() : new List<string>();
            List<string> intersected = ListIntersect(props, vals);
            foreach (string key in intersected)
            {
                object val = propertyValues.GetType().GetProperty(key).GetValue(propertyValues);
                obj.GetType().GetProperty(key).SetValue(obj, val);
            }

            return obj;
        }
        public static List<T> ListIntersect<T>(List<T> List1, List<T> List2)
        {
            return List1.Intersect(List2.Select(s => s)).ToList();
        }
        public static List<string> GetProps(this object model)
        {
            return model.GetType().GetProperties().ToList().Select(p => p.Name).ToList();
        }
        public static Dictionary<string, object> GetPropertiesAndValues(this object model)
        {
            return model.GetType().GetProperties().ToDictionary(p => p.Name, p => p.GetValue(model));
        }
        public static string GetPropsAsString(this object model)
        {
            return string.Join("&", model.GetPropertiesAndValues().ToList().Select(s => s.Key + "=" + (s.Value != null ? s.Value.ToString() : "")));
        }
        public static void Clear<T>(this DbSet<T> dbSet) where T : class
        {
            dbSet.RemoveRange(dbSet);
        }
        public static T Clone<T>(this T Source)
        {
            Type typeSource = Source.GetType();
            object Target = Activator.CreateInstance(typeSource);

            PropertyInfo[] propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (PropertyInfo property in propertyInfo)
            {
                if (property.CanWrite)
                {
                    property.SetValue(Target, property.GetValue(Source, null), null);
                }
            }

            return (T)Target;
        }
        
    }
}
