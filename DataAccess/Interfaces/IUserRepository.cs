﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IUserRepository : IEntityRepository<USER, int>
    {
        IQueryable<USER> Include(string include);
        void UpdatUsersFromSystem();
        void RemoveEmptyUsers();
    }
}
