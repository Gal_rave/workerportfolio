﻿using Catel.Data.Repositories;
using DataAccess.Models;

using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IPayCheckUsersByMailRepository : IEntityRepository<PAYCHECKUSERSBYMAIL, int>
    {
        IQueryable<PAYCHECKUSERSBYMAIL> Include(string include);
        void InsertGlobalMunicipalityPCUE(int municipalityid);
    }
}