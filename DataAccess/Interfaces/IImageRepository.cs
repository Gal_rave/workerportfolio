﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IImageRepository : IEntityRepository<IMAGE, int>
    {
        IQueryable<IMAGE> Include(string include);
    }
}
