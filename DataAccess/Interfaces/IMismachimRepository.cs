﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IMismachimRepository : IEntityRepository<MISMACHIM, int>
    {
        IQueryable<MISMACHIM> Include(string include);
    }
}