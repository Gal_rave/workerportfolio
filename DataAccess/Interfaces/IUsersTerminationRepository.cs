﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IUsersTerminationRepository : IEntityRepository<USERSTERMINATION, int>
    {
        IQueryable<USERSTERMINATION> Include(string include);
        int GetUserDefaultMunicipality(string idNumber);
    }
}
