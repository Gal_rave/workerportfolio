﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface ICityRepository : IEntityRepository<CITy, int>
    {
        IQueryable<CITy> Include(string include);
    }
}
