﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IUserImageRepository : IEntityRepository<USERIMAGE, int>
    {
        IQueryable<USERIMAGE> Include(string include);
        void RemoveRange(IQueryable<USERIMAGE> source);
    }
}
