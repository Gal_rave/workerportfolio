﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IHtmlTemplatesRepository : IEntityRepository<HTMLTEMPLATE, int>
    {
        IQueryable<HTMLTEMPLATE> Include(string include);
    }
}