﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IEmailByMunicipalityRepository : IEntityRepository<EMAILBYMUNICIPALITY, int>
    {
        void ConnectEmailByMunicipality();
        void DeactivateMunicipality();
        IQueryable<EMAILBYMUNICIPALITY> Include(string include);
    }
}
