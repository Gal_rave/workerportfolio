﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IProposalUEPCRepository : IEntityRepository<PROPOSAL_UEPC, decimal>
    {
        IQueryable<PROPOSAL_UEPC> Include(string include);
    }
}
