﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface ILoginLogRepository : IEntityRepository<LOGINLOG, int>
    {
        IQueryable<LOGINLOG> Include(string include);
    }
}
