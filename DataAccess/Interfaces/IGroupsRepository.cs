﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IGroupsRepository : IEntityRepository<GROUP, int>
    {
        IQueryable<GROUP> Include(string include);
    }
}
