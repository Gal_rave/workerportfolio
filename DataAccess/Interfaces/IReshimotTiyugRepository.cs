﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IReshimotTiyugRepository : IEntityRepository<RESHIMOT_TIYUG, int>
    {
        IQueryable<RESHIMOT_TIYUG> Include(string include);
    }
}