﻿using System.Linq;

using Catel.Data.Repositories;
using DataAccess.Models;

namespace DataAccess.Interfaces
{
    public interface IMunicipalityRepository : IEntityRepository<MUNICIPALITy, int>
    {
        IQueryable<MUNICIPALITy> Include(string include);
    }
}
