﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IMistameshSacharRepository : IEntityRepository<SACHAR_USERS, int>
    {
        IQueryable<SACHAR_USERS> Include(string include);
        void InsertSacharUsers();
    }
}
