﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IChildRepository : IEntityRepository<Child, int>
    {
        IQueryable<Child> Include(string include);
    }
}
