﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IMunicipalityContactRepository : IEntityRepository<MUNICIPALITYCONTACT, int>
    {
        IQueryable<MUNICIPALITYCONTACT> Include(string include);
    }
}
