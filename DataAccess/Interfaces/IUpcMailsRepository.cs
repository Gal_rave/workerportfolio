﻿using Catel.Data.Repositories;
using DataAccess.Models;

using System;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IUpcMailsRepository : IEntityRepository<UPC_MAILS, int>
    {
        IQueryable<UPC_MAILS> Include(string include);
        int saveUpcMail(DateTime currentPayMonth, string email, decimal userId, decimal userImageId, decimal municipalityId);
    }
}
