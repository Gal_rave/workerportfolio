﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IKvuzotMishtamshimRepository : IEntityRepository<KVUZOT_MISHTAMSHIM, int>
    {
        IQueryable<KVUZOT_MISHTAMSHIM> Include(string include);
    }
}