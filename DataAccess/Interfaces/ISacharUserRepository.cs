﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface ISacharUserRepository : IEntityRepository<SACHAR_USERS_GELEM, int>
    {
        IQueryable<SACHAR_USERS_GELEM> Include(string include);
    }
}