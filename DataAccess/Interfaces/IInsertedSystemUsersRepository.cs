﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IInsertedSystemUsersRepository : IEntityRepository<INSERTED_SYSTEMUSERS, int>
    {
        IQueryable<INSERTED_SYSTEMUSERS> Include(string include);
        void ImportIntoSystemUsers();
    }
}
