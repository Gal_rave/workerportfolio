﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IMembershipQuestionsRepository : IEntityRepository<MEMBERSHIP_QUESTIONS, int>
    {
        IQueryable<MEMBERSHIP_QUESTIONS> Include(string include);
    }
}
