﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IOrgUnitMngrsRepository : IEntityRepository<ORG_UNIT_MNGRS, int>
    {
        IQueryable<ORG_UNIT_MNGRS> Include(string include);
        void SyncUnitMngrsvsUsers();
    }
}
