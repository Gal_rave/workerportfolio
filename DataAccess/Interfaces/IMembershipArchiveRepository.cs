﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IMembershipArchiveRepository : IEntityRepository<MEMBERSHIPARCHIVE, int>
    {
        IQueryable<MEMBERSHIPARCHIVE> Include(string include);
    }
}
