﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface ICoursRepository : IEntityRepository<COURS, int>
    {
        IQueryable<COURS> Include(string include);
    }
}
