﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IMismachimBeReshimaRepository : IEntityRepository<MISMACHIM_BERESHIMA, int>
    {
        IQueryable<MISMACHIM_BERESHIMA> Include(string include);
    }
}