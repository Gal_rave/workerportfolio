﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IMishtamsheiKvuzaRepository : IEntityRepository<MISHTAMSHEI_KVUZA, int>
    {
        IQueryable<MISHTAMSHEI_KVUZA> Include(string include);
    }
}