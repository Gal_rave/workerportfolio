﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IDirectoryRepository : IEntityRepository<DIRECTORy, int>
    {
        IQueryable<DIRECTORy> Include(string include);
    }
}
