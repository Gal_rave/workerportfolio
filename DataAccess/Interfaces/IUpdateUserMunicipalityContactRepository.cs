﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IUpdateUserMunicipalityContactRepository : IEntityRepository<UPDATEUSERMUNICIPALITYCONTACT, int>
    {
        IQueryable<UPDATEUSERMUNICIPALITYCONTACT> Include(string include);
    }
}
