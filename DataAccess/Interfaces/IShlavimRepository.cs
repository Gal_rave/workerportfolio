﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IShlavimRepository : IEntityRepository<SHLAVIM, int>
    {
        IQueryable<SHLAVIM> Include(string include);
    }
}