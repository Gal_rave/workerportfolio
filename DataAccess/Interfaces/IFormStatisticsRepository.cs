﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IFormStatisticsRepository : IEntityRepository<FORM_STATISTICS, int>
    {
        IQueryable<FORM_STATISTICS> Include(string include);
    }
}
