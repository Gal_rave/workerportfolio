﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IMailNotificationRepository : IEntityRepository<MAILNOTIFICATION, int>
    {
        IQueryable<MAILNOTIFICATION> Include(string include);
    }
}
