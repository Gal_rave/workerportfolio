﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface IUsersBirthdaysRepository : IEntityRepository<USERS_BIRTHDAYS, int>
    {
        IQueryable<USERS_BIRTHDAYS> Include(string include);
    }
}
