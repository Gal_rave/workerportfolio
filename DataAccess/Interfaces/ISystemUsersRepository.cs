﻿using Catel.Data.Repositories;
using DataAccess.Models;
using System.Linq;

namespace DataAccess.Interfaces
{
    public interface ISystemUsersRepository : IEntityRepository<SYSTEM_USERS, int>
    {
        IQueryable<SYSTEM_USERS> Include(string include);
        void InsertMunicipalitiesToUser(string idNumber);
    }
}
