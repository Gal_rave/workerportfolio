﻿using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class ChildMapper
    {
        public static ChildModel Map(this Child model)
        {
            if (model == null || model.USER == null)
                return null;

            return new ChildModel
            {
                possession = model.POSSESSION.ToBool(),
                childBenefit = model.CHILDBENEFIT.ToBool(),
                homeLiving = model.HOMELIVING.ToBool(),
                birthDate = model.BIRTHDATE,
                firstName = model.FIRSTNAME,
                gender = model.GENDER.ToBool(),
                ID = model.ID.ToInt(),
                idNumber = model.IDNUMBER,
                userID = model.USERID.ToInt()
            };
        }
        public static Child Map(this ChildModel model, int? userid = null)
        {
            if (model == null)
                return null;

            return new Child
            {
                POSSESSION = model.possession,
                CHILDBENEFIT = model.childBenefit,
                HOMELIVING = model.homeLiving,
                USERID = userid.HasValue? userid.Value : model.userID,
                BIRTHDATE = model.birthDate,
                FIRSTNAME = model.firstName,
                GENDER = model.gender,
                ID = model.ID,
                IDNUMBER = model.idNumber,
            };
        }
    }
}
