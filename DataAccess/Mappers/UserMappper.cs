﻿using System;
using System.Linq;
using System.Collections.Generic;

using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class UserMappper
    {
        public static UserModel Map(this USER model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new UserModel
            {
                ID = model.ID.ToInt(),
                birthDate = model.BIRTHDATE,
                cell = model.CELL,
                createdDate = model.CREATEDDATE,
                email = model.EMAIL,
                firstName = model.FIRSTNAME,
                idNumber = model.IDNUMBER,
                immigrationDate = model.IMMIGRATIONDATE,
                lastName = model.LASTNAME,
                phone = model.PHONE,
                isDeleted = model.ISDELETED,
                isConfirmed = model.MEMBERSHIP != null && model.MEMBERSHIP.ISCONFIRMED,
                calculatedIdNumber = model.CALCULATEDIDNUMBER,
                fathersName = model.FATHERSNAME,
                previousLastName = model.PREVIOUSLASTNAME,
                previousFirstName = model.PREVIOUSFIRSTNAME,
                zip = model.ZIP,
                city = model.CITY,
                street = model.STREET,
                houseNumber = model.HOUSENUMBER.ToInt(),
                entranceNumber = model.ENTRANCENUMBER,
                apartmentNumber = model.APARTMENTNUMBER.ToInt(),
                poBox = model.POBOX,
                countryOfBirth = model.COUNTRYOFBIRTH,
                gender = model.GENDER,
                maritalStatus = model.MARITALSTATUS.ToInt(),
                maritalStatusDate = model.MARITALSTATUSDATE,
                isSpouse = model.ISSPOUSE,
                spouseId = model.SPOUSEID,
                jobTitle = model.JOBTITLE,
                isAllow101 = model.ISALLOW101,
                wellcomEmail = model.WELLCOMEMAIL,
                initialcheck = model.INITIALCHECK,
                citizenshipType = model.CITIZENSHIPTYPE,
                hasIncome = model.HASINCOME,
                hmoCode = model.HMOCODE,
                incomeType = model.INCOMETYPE,
                kibbutzMember = model.KIBBUTZMEMBER,
                lastUpdatedDate = model.LASTUPDATEDDATE,

                spouse = withInner && model.SPOUSEID.HasValue && model.SPOUSEID.Value > 0 ? model.SPOUSE.Map() : null,
                municipalities = withInner ? model.MUNICIPALITIES.ToList().Select(m => m.Map(false)).ToList() : null,
                groups = withInner ? model.GROUPS.ToList().Select(g => g.Map(false)).ToList() : null,
                membership = withInner ? model.MEMBERSHIP.Map(false) : null,
                terminations = withInner ? model.USERSTERMINATIONS.ToList().Select(t => t.Map()).ToList() : null,
                emailByMunicipality = withInner ? model.EMAILBYMUNICIPALITies.ToList().Select(e => e.Map()).ToList() : null,
            };
        }
        public static USER Map(this UserModel model)
        {
            if (model == null)
                return null;
            return new USER
            {
                ID = model.ID.ToInt(),
                BIRTHDATE = model.birthDate,
                CELL = model.cell,
                CREATEDDATE = model.createdDate,
                EMAIL = model.email,
                FIRSTNAME = model.firstName,
                IDNUMBER = model.idNumber,
                IMMIGRATIONDATE = model.immigrationDate,
                LASTNAME = model.lastName,
                PHONE = model.phone,
                ISDELETED = model.isDeleted,
                LASTUPDATEDDATE = DateTime.Now,
                CALCULATEDIDNUMBER = model.calculatedIdNumber,
                FATHERSNAME = model.fathersName,
                PREVIOUSLASTNAME = model.previousLastName,
                PREVIOUSFIRSTNAME = model.previousFirstName,
                ZIP = model.zip,
                CITY = model.city,
                STREET = model.street,
                HOUSENUMBER = model.houseNumber,
                ENTRANCENUMBER = model.entranceNumber,
                APARTMENTNUMBER = model.apartmentNumber,
                POBOX = model.poBox,
                COUNTRYOFBIRTH = model.countryOfBirth,
                GENDER = model.gender,
                MARITALSTATUS = model.maritalStatus,
                MARITALSTATUSDATE = model.maritalStatusDate,
                ISSPOUSE = model.isSpouse,
                SPOUSEID = model.spouseId,
                JOBTITLE = model.jobTitle,
                ISALLOW101 = model.isAllow101,
                WELLCOMEMAIL = model.wellcomEmail,
                INITIALCHECK = model.initialcheck,
                CITIZENSHIPTYPE = model.citizenshipType,
                HASINCOME = model.hasIncome,
                HMOCODE = model.hmoCode,
                INCOMETYPE = model.incomeType,
                KIBBUTZMEMBER = model.kibbutzMember
            };
        }
        public static UserModel Map(this CreateUserModel model)
        {
            if (model == null)
                return null;
            return new UserModel
            {
                cell = model.cell,
                email = model.email,
                firstName = model.firstName,
                idNumber = model.idNumber,
                lastName = model.lastName,
                phone = model.phone,
                city = model.cityName,
                street = model.streetName,
                password = model.password,
            };
        }
        public static UserModel Map(this RegisterUserModel model)
        {
            if (model == null)
                return null;

            return new UserModel()
            {
                idNumber = model.idNumber,
                municipalities = model.municipalities,
                calculatedIdNumber = IdNumberExtender.GenerateCalculatedIdNumber(model.idNumber)
            };
        }
        public static UserModel Map(this List<SystemUserModel> models)
        {
            if (models == null || models.Count < 1)
                return null;
            var user = models.First();
            return new UserModel()
            {
                idNumber = user.fullIdNumber,
                municipalities = models.Select(su => su.municipality).ToList(),
                calculatedIdNumber = IdNumberExtender.GenerateCalculatedIdNumber(user.fullIdNumber),
                cell = user.cellNumber,
                email = user.email,
            };
        }
        public static ShortUserModel ShortMap(this USER model)
        {
            if (model == null)
                return null;
            return new ShortUserModel
            {
                ID = model.ID.ToInt(),
                email = model.EMAIL.ToLower(),
                name = string.Format("{0} {1}", model.FIRSTNAME, model.LASTNAME).ToLower()
            };
        }
        public static UserModel Map(this USERS_BIRTHDAYS model)
        {
            if (model == null)
                return null;
            return new UserModel
            {
                ID = model.ID.ToInt(),
                birthDate = model.BIRTHDATE,
                cell = model.CELL,
                createdDate = model.CREATEDDATE,
                email = model.EMAIL,
                firstName = model.FIRSTNAME,
                idNumber = model.IDNUMBER,
                immigrationDate = model.IMMIGRATIONDATE,
                lastName = model.LASTNAME,
                phone = model.PHONE,
                isDeleted = model.ISDELETED,
                calculatedIdNumber = model.CALCULATEDIDNUMBER,
                fathersName = model.FATHERSNAME,
                previousLastName = model.PREVIOUSLASTNAME,
                previousFirstName = model.PREVIOUSFIRSTNAME,
                zip = model.ZIP,
                city = model.CITY,
                street = model.STREET,
                houseNumber = model.HOUSENUMBER.ToInt(),
                entranceNumber = model.ENTRANCENUMBER,
                apartmentNumber = model.APARTMENTNUMBER.ToInt(),
                poBox = model.POBOX,
                countryOfBirth = model.COUNTRYOFBIRTH,
                gender = model.GENDER,
                maritalStatus = model.MARITALSTATUS.ToInt(),
                maritalStatusDate = model.MARITALSTATUSDATE,
                isSpouse = model.ISSPOUSE,
                spouseId = model.SPOUSEID,
                jobTitle = model.JOBTITLE,
                isAllow101 = model.ISALLOW101,
                wellcomEmail = model.WELLCOMEMAIL,
                initialcheck = model.INITIALCHECK,
                citizenshipType = model.CITIZENSHIPTYPE,
                hasIncome = model.HASINCOME,
                hmoCode = model.HMOCODE,
                incomeType = model.INCOMETYPE,
                kibbutzMember = model.KIBBUTZMEMBER,
                lastUpdatedDate = model.LASTUPDATEDDATE,
            };
        }
    }
}
