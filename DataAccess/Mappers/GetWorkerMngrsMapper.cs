﻿using DataAccess.Models;
using System;
using System.Collections.Generic;

namespace DataAccess.Mappers
{
    public static class GetWorkerMngrsMapper
    {
        public static WSPostModel MapToWSPostModel(this GetWorkerMngrsModel model)
        {
            if (model == null)
                return null;

            return model.model;
        }
        public static RepresentedWorkerModel MapToRepresentedWorker(this GetWorkerMngrsModel model)
        {
            if (model == null)
                return null;

            return model.worker;
        }
        public static List<YehidaIrgunitModel> MapToUnits(this GetWorkerMngrsModel model)
        {
            if (model == null)
                return null;

            return model.units;
        }
    }
}