﻿using System;
using System.Linq;

using DataAccess.Models;
using DataAccess.Extensions;
using System.Collections.Generic;

namespace DataAccess.Mappers
{
    public static class PersonalInformationMapper
    {
        private static readonly string[] maritalStatus = new string[] { "ErrorStatus", "רווק/ה", "נשוי/ה", "גרוש/ה", "אלמן/ה", "פרוד/ה" };

        public static UserModel MapUser(this ResponsePersonalInformation model)
        {
            if (model == null)
                return null;

            return new UserModel
            {
                lastName = model.lastName,
                firstName = model.firstName,
                fathersName = model.FathersName,
                previousLastName = model.previousLastName,
                previousFirstName = model.previousFirstName,
                city = model.city,
                street = model.street,
                houseNumber = model.houseNumber,
                entranceNumber = model.entranceNumber,
                apartmentNumber = model.apartmentNumber,
                zip = model.zip.ToString(),
                poBox = model.poBox.ToString(),
                phone = model.phoneNumberPrefix.prefixPhoneNumber(),
                cell = model.extraNumberPrefix.prefixPhoneNumber(),
                birthDate = model.birthDate,
                countryOfBirth = model.countryOfBirth,
                immigrationDate = model.immigrationDate,
                gender = string.Compare(model.gender, "זכר", true) == 0,
                maritalStatus = model.maritalStatus.GetMaritalStatus(),
                maritalStatusDate = model.statusDate,
                isSpouse = false,
            };
        }
        public static SpouseModel MapSpouse(this ResponsePersonalInformation model)
        {
            if (model == null)
                return null;

            return new SpouseModel
            {
                maritalStatus = model.maritalStatus.GetMaritalStatus(),
                maritalStatusDate = model.statusDate,
                firstName = model.spouseName,
                calculatedIdNumber = model.spouseIdNUmber.ToString(),
                idNumber = IdNumberExtender.CheckAndGenerateFullIdNumber(model.spouseIdNUmber.ToString()),
                birthDate = model.spousBirthDate,
                hasIncome = string.Compare(model.spousJobStatus, "כ", true) == 0,
                incomeType = model.spousJobName,
                phone = model.spousPhoneNumberPrefix.prefixPhoneNumber(),
                cell = model.spousPhoneNumberExtension.prefixPhoneNumber(),
                gender = string.Compare(model.gender, "זכר", true) != 0,
                isSpouse = true,
            };
        }
        public static ChildModel MapChild(this ResponseChild model)
        {
            if (model == null)
                return null;

            return new ChildModel
            {
                birthDate = model.birthDate,
                firstName = model.firstName,
                gender = string.Compare(model.gender, "זכר", true) == 0,
                homeLiving = string.Compare(model.homeLiving, "כ", true) == 0,
                idNumber = IdNumberExtender.CheckAndGenerateFullIdNumber(model.idNumber.ToString()),
            };
        }
        public static List<ChildModel> MapChildren(this ResponsePersonalInformation model)
        {
            if (model == null)
                return null;

            return model.children.Where(c => c.idNumber > 0).Select(c => c.MapChild()).ToList();
        }
        public static CoursModel MapCourse(this ResponseEducation model)
        {
            if (model == null)
                return null;

            return new CoursModel
            {
                certificateName = model.certificateName.StringTrim(),
                institutionName = model.institutionName.StringTrim(),
                studySubject1 = model.studySubject1.StringTrim(),
                studySubject2 = model.studySubject2.StringTrim(),
                educationYears = model.educationYears,
                degree = model.degree.StringTrim(),
                degreeDate = model.degreeDate,
            };
        }
        public static List<CoursModel> MapCourses(this ResponsePersonalInformation model)
        {
            if (model == null)
                return null;
         
            return model.courses.Select(c => c.MapCourse()).ToList().Where(c=> c.certificateName.Length > 1).ToList();
        }
        public static void Merge(this UserModel model, UserModel merge)
        {
            if (model == null || merge == null)
                return;

            model.cell = model.cell.SplitDistinct(merge.cell);
            model.email = model.email.SplitDistinct(merge.email);
            model.phone = model.phone.SplitDistinct(merge.phone);
            model.firstName = merge.firstName ?? model.firstName;
            model.lastName = merge.lastName ?? model.lastName;
        }
    }
}