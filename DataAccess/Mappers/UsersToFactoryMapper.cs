﻿using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class UsersToFactoryMapper
    {
        public static UsersToFactoryModel Map(this USERSTOFACTORy model)
        {
            if (model == null)
                return null;

            return new UsersToFactoryModel
            {
                factoryId = model.FACTORYID.ToInt(),
                municipalityId = model.MUNICIPALITYID.ToInt(),
                userId = model.USERID.ToInt()
            };
        }
        public static USERSTOFACTORy Map(this UsersToFactoryModel model)
        {
            if (model == null)
                return null;

            return new USERSTOFACTORy
            {
                FACTORYID = model.factoryId,
                MUNICIPALITYID = model.municipalityId,
                USERID = model.userId
            };
        }
    }
}
