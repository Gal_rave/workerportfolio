﻿using System.Linq;
using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class humanResourcesContactMapper
    {
        public static humanResourcesContactModel Map(this HUMANRESOURCESCONTACT model, bool withInner = true)
        {
            if (model == null)
                return null;
            return new humanResourcesContactModel
            {
                ID = model.ID.ToInt(),
                name = model.NAME,
                email = model.EMAIL,
                job = model.JOB,
                phone = model.PHONE,
                municipalityID = model.MUNICIPALITYID.ToInt(),
                municipality = withInner? model.MUNICIPALITy.Map(false) : null
            };
        }
        public static HUMANRESOURCESCONTACT Map(this humanResourcesContactModel model)
        {
            if (model == null)
                return null;
            return new HUMANRESOURCESCONTACT
            {
                ID = model.ID,
                NAME = model.name,
                EMAIL = model.email,
                JOB = model.job,
                PHONE = model.phone,
                MUNICIPALITYID = model.municipalityID
            };
        }
    }
}
