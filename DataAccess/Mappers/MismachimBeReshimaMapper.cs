﻿using DataAccess.Models;
using System;
using System.Linq;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class MismachimBeReshimaMapper
    {
        public static MismachBereshimaModel Map(this MISMACHIM_BERESHIMA model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new MismachBereshimaModel
            {
                recId = model.REC_ID.ToInt(),
                rashut = model.RASHUT.ToInt(),
                mifal = model.MIFAL.ToInt(),
                fromDate = model.FROM_DATE.ToInt(),
                toDate = model.TO_DATE.ToInt(),
                mismachId = model.MISMACH_ID.ToInt(),
                ofiMismachInd = model.OFI_MISMACH_IND.ToInt(),
                reshimaId = model.RESHIMA_ID.ToInt(),
                zmanHatraa = model.ZMAN_HATRAA.ToInt(),
                creationTime = model.CREATION_TIME,
                lastUpdateTime = model.LAST_UPDATE_TIME,
                lastUpdateUser = model.LAST_UPDATE_USER.ToInt(),
                status = model.STATUS.ToInt(),
                mismachModel = model.MISMACHIM.Map(withInner)
            };
        }
        public static MISMACHIM_BERESHIMA Map(this MismachBereshimaModel model)
        {
            if (model == null)
                return null;

            return new MISMACHIM_BERESHIMA
            {
                REC_ID = model.recId,
                RASHUT = model.rashut,
                MIFAL = model.mifal,
                FROM_DATE = model.fromDate,
                TO_DATE = model.toDate,
                MISMACH_ID = model.mismachId,
                OFI_MISMACH_IND = model.ofiMismachInd,
                RESHIMA_ID = model.reshimaId,
                ZMAN_HATRAA = model.zmanHatraa,
                CREATION_TIME = model.creationTime,
                LAST_UPDATE_TIME = model.lastUpdateTime,
                LAST_UPDATE_USER = model.lastUpdateUser,
                STATUS = model.status
            };
        }
    }
}
