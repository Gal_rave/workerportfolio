﻿using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class MunicipalityContactMapper
    {
        public static MunicipalityContactModel Map(this MUNICIPALITYCONTACT model, bool withInner = false)
        {
            if (model == null)
                return null;

            return new MunicipalityContactModel
            {
                ID = model.ID.ToInt(),
                areaCode = model.AREACODE,
                phone = model.PHONE,
                email = model.EMAIL,
                houseNumber = model.HOUSENUMBER,
                city = model.CITY,
                postalCode = model.POSTALCODE,
                deductionFileID = model.DEDUCTIONFILEID,
                street = model.STREET,
                mifal = model.MIFAL,
                rashut = model.RASHUT,
                municipalityId = model.MUNICIPALITYID.ToInt(),
                municipality = withInner? model.MUNICIPALITy.Map(false) : null
            };
        }
        public static MUNICIPALITYCONTACT Map(this MunicipalityContactModel model, decimal? Id = null)
        {
            if (model == null)
                return null;

            return new MUNICIPALITYCONTACT
            {
                ID = Id.HasValue && Id.Value > 0? Id.Value : model.ID,
                AREACODE = model.areaCode,
                PHONE = model.phone,
                EMAIL = model.email,
                HOUSENUMBER = model.houseNumber,
                CITY = model.city,
                POSTALCODE = model.postalCode,
                DEDUCTIONFILEID = model.deductionFileID,
                STREET = model.street,
                MIFAL = model.mifal,
                RASHUT = model.rashut,
                MUNICIPALITYID = model.municipalityId
            };
        }
        public static MunicipalityContactModel Map(this MunicipalityContactExtension model, int mifal)
        {
            if (model == null)
                return null;

            return new MunicipalityContactModel
            {
                areaCode = model.KIDOMET_TELEFON.StringTrim(),
                city = model.ISHUV.StringTrim(),
                deductionFileID = model.H_P.StringTrim(),
                email = model.EMAIL.StringTrim(),
                houseNumber = model.MISPAR_BAIT.StringTrim(),
                phone = (model.KIDOMET_TELEFON.StringTrim() + model.TELEFON.StringTrim()).prefixPhoneNumber(),
                postalCode = model.MIKUD.StringTrim(),
                street = model.REHOV.Translate().Reverse().StringTrim(),
                mifal = mifal,
                rashut = model.RASHUT
            };
        }
        public static MUNICIPALITy Map(this MunicipalitySchemaExtension model)
        {
            if (model == null)
                return null;
            return new MUNICIPALITy
            {
                RASHUT = model.rashut_mancol.ToInt(),
                ID = model.rashut_sachar.ToDecimal(),
                FACTORYEMAILPC = model.factory_entry.ToInt(), ///0 => dont allow, 1 => send to all, 2 => allow to register 
                NAME = model.shem_mifal,
                ALLOWEMAILPAYCHECK = (model.factory_entry.ToInt() > 0),

                ACTIVE = false,
                SYNERION_LINK = "https://interface00.synerioncloud.com/SynerionWeb/api/automation?token={0}",
                ALLOW101 = false,
                ALLOWPROCESS = false,
                ALLOWMUNICIPALITYMENU = false,
                ALLOWSYSTEMSEMAIL = false,
                MUNICIPALITYMENU_TITLE = string.Empty
            };
        }
        /// <summary>
        /// map `Municipality` and `MunicipalityContact` to model
        /// Unused variables =>> schema_name, customer_number, customer_name_heb, mifal, dist
        /// </summary>
        /// <param name="model">MunicipalitySchemaExtension</param>
        /// <returns>`MunicipalityModel` includs `MunicipalityContactModel`</returns>
        public static MunicipalityModelExtension MapSchema(this MunicipalitySchemaExtension model)
        {
            if (model == null)
                return null;
            return new MunicipalityModelExtension
            {
                rashut = model.rashut_mancol.ToInt(),
                ID = model.rashut_sachar.ToDecimal(),
                factoryEmailPC = model.factory_entry.ToInt(), ///0 => dont allow, 1 => send to all, 2 => allow to register 
                name = model.shem_mifal,
                allowemailpaycheck = (model.factory_entry.ToInt() > 0),

                ///map the `MunicipalityContact`
                contact = model.MapContact(),

                ///set up variables
                active = false,
                synerion_link = "https://interface00.synerioncloud.com/SynerionWeb/api/automation?token={0}",
                allow101 = false,
                allowprocess = false,
                allowMunicipalityMenu = false,
                allowSystemsEmail = false,
                municipalityMenuTitle = string.Empty
            };
        }
        public static MunicipalityContactModel MapContact(this MunicipalitySchemaExtension model)
        {
            if (model == null || model.contact == null)
                return null;

            return new MunicipalityContactModel
            {
                areaCode = model.contact.KIDOMET_TELEFON.StringTrim(),
                city = model.contact.ISHUV.StringTrim(),
                deductionFileID = model.contact.H_P.StringTrim(),
                email = model.contact.EMAIL.StringTrim(),
                houseNumber = model.contact.MISPAR_BAIT.StringTrim(),
                phone = (model.contact.KIDOMET_TELEFON.StringTrim() + model.contact.TELEFON.StringTrim()).prefixPhoneNumber(),
                postalCode = model.contact.MIKUD.StringTrim(),
                street = model.contact.REHOV.Translate().Reverse().StringTrim(),
                mifal = model.mifal.ToInt(),
                rashut = model.rashut_mancol.ToInt(),
                municipalityId = model.rashut_sachar.ToInt()
            };
        }
    }
}
