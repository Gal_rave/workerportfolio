﻿using DataAccess.Models;
using DataAccess.Extensions;
using System;
using System.Linq;

namespace DataAccess.Mappers
{
    public static class PayCheckUsersByMailMapper
    {
        public static PayCheckUsersByMailModel Map(this PAYCHECKUSERSBYMAIL model, bool withInner = false)
        {
            if (model == null)
                return null;

            return new PayCheckUsersByMailModel()
            {
                activeRecord = model.ACTIVERECORD,
                createdDate = model.CREATEDDATE,
                ID = model.ID.ToInt(),
                idNumber = model.USER.CALCULATEDIDNUMBER,
                lastSendDate = model.LASTSENDDATE,
                municipalityId = model.MUNICIPALITYID.ToInt(),
                updateDate = model.UPDATEDATE,
                creationSource = model.CREATIONSOURCE, /// false tikOved, true java
                lastSendMonth = model.LASTSENDMONTH,
                lastSendYear = model.LASTSENDYEAR,
                sendMail = model.SENDMAIL,

                userId = model.USERID.ToInt(),

                municipality = withInner ? model.MUNICIPALITy.Map(false) : null,
                user = withInner ? model.USER.Map(true) : model.USER.Map(false),
            };
        }
        public static PayCheckUsersByMailExtensionModel Map(this PAYCHECKUSERSBYMAIL model, DateTime searchDate)
        {
            if (model == null)
                return null;

            return new PayCheckUsersByMailExtensionModel()
            {
                activeRecord = model.ACTIVERECORD,
                createdDate = model.CREATEDDATE,
                ID = model.ID.ToInt(),
                idNumber = model.USER.CALCULATEDIDNUMBER,
                lastSendDate = model.LASTSENDDATE,
                municipalityId = model.MUNICIPALITYID.ToInt(),
                updateDate = model.UPDATEDATE.HasValue? model.UPDATEDATE : null,
                creationSource = model.CREATIONSOURCE, /// false tikOved, true java
                lastSendMonth = model.LASTSENDMONTH,
                lastSendYear = model.LASTSENDYEAR,
                sendMail = model.SENDMAIL,

                userId = model.USERID.ToInt(),
                pcue_sendarchive = model.USER.PCUE_SENDARCHIVE.FirstOrDefault(p => p.MUNICIPALITYID == model.MUNICIPALITYID && p.PC_MONTH == searchDate.Month && p.PC_YEAR == searchDate.Year).Map(),
                user = model.USER.Map(false),
                Email = string.Join(",", model.USER.EMAILBYMUNICIPALITies.Where(e=> e.MUNICIPALITYID == model.MUNICIPALITYID).Select(e=> e.EMAIL).ToList())
            };
        }
        public static PAYCHECKUSERSBYMAIL Map(this PayCheckUsersByMailModel model)
        {
            if (model == null)
                return null;

            return new PAYCHECKUSERSBYMAIL()
            {
                ACTIVERECORD = model.activeRecord,
                CREATEDDATE = model.createdDate,
                ID = model.ID,
                LASTSENDDATE = model.lastSendDate,
                MUNICIPALITYID = model.municipalityId,
                UPDATEDATE = model.updateDate,
                USERID = model.userId,
                CREATIONSOURCE = model.creationSource,
                LASTSENDMONTH = model.lastSendMonth,
                LASTSENDYEAR = model.lastSendYear,
                SENDMAIL = model.sendMail
            };
        }
        public static Tlush_MailModel MapTlush(this PayCheckUsersByMailModel model)
        {
            if (model == null)
                return null;

            return new Tlush_MailModel()
            {
                CREATION_DATE = model.createdDate.ToUniversalTime(),
                CREATION_SOURCE = model.creationSource ? 2 : 1,
                CREATION_USER = model.idNumber.ToDouble(),
                FROM_DATE = model.createdDate.ddmmyyyy(),
                RASHUT_SACHAR = model.municipalityId,
                SEND_MAIL = model.sendMail,
                UPDATE_DATE = model.updateDate.HasValue ? model.updateDate.Value.ToUniversalTime() : new DateTime().ToUniversalTime(),
                UPDATE_SOURCE = model.creationSource ? 2 : 1,
                UPDATE_USER = model.idNumber.ToDouble(),
                ZEHUT = model.idNumber.ToDouble(),
                ZEHUT_MALE = IdNumberExtender.CheckAndGenerateFullIdNumber(model.idNumber).ToDouble(),
            };
        }
        public static PayCheckUsersByMailModel MapEtoP(this EMAILBYMUNICIPALITY model)
        {
            if (model == null)
                return null;

            return new PayCheckUsersByMailModel()
            {
                activeRecord = true,
                ID = model.ID.ToInt(),
                idNumber = model.USER.CALCULATEDIDNUMBER,
                municipalityId = model.MUNICIPALITYID.ToInt(),
                creationSource = true, /// false tikOved, true java
                sendMail = true,

                userId = model.USERID.ToInt(),
                
                user = model.USER.Map(true),
            };
        }
    }
}
