﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Mappers
{
    public static class pcueSendArchiveMapper
    {
        public static pcueSendArchiveModel Map(this PCUE_SENDARCHIVE model)
        {
            if (model == null)
                return null;

            return new pcueSendArchiveModel
            {
                id = model.ID,
                municipalityid = model.MUNICIPALITYID,
                pc_month = model.PC_MONTH,
                pc_year = model.PC_YEAR,
                senddate = model.SENDDATE,
                userid = model.USERID
            };
        }
    }
}
