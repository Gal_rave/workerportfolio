﻿using DataAccess.Models;
using System;

namespace DataAccess.Mappers
{
    public static class OrgUnitMngrMapper
    {
        public static ORG_UNIT_MNGRS Map(this UnitManagerModel model)
        {
            if(model == null)
            {
                return null;
            }
            return new ORG_UNIT_MNGRS
            {
                ID_NUMBER = model.idNumber,
                FIRST_NAME = model.fName,
                LAST_NAME = model.lName,
                RASHUT = model.rashutNumber,
                TEKEN_K_ADAM = model.tekenKoachAdam,
                TEKEN_MENAEL_YEHIDA_IRGUNIT = model.tekenMenaelYahidaIrgunit,
                ZEHUT_NATZIG_K_ADAM = model.zehutNatzigKoachAdam,
                TREE_NUMBER = model.treeNumber,
                UNIT_NUMBER = model.unitNumber,
                UNIT_DESCRIPTION = model.unitDescription,
                SUPERVISOR_UNIT = model.supervisorUnitNumber,
                RAMA_IRGUNIT = model.ramaIrgunit
            };
        }
        public static UnitManagerModel Map(this ORG_UNIT_MNGRS model)
        {
            if (model == null)
            {
                return null;
            }
            return new UnitManagerModel
            {
                idNumber = Convert.ToInt32(model.ID_NUMBER),
                fName = model.FIRST_NAME,
                lName = model.LAST_NAME,
                rashutNumber = Convert.ToInt32(model.RASHUT),
                tekenKoachAdam = Convert.ToInt32(model.TEKEN_K_ADAM),
                tekenMenaelYahidaIrgunit = Convert.ToInt32(model.TEKEN_MENAEL_YEHIDA_IRGUNIT),
                zehutNatzigKoachAdam = Convert.ToInt32(model.ZEHUT_NATZIG_K_ADAM),
                treeNumber = Convert.ToInt32(model.TREE_NUMBER),
                unitNumber = Convert.ToInt32(model.UNIT_NUMBER),
                unitDescription = model.UNIT_DESCRIPTION,
                supervisorUnitNumber = Convert.ToInt32(model.SUPERVISOR_UNIT),
                ramaIrgunit = Convert.ToInt32(model.RAMA_IRGUNIT)
            };
        }
    }
}
