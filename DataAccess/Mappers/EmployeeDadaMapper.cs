﻿using DataAccess.Extensions;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Mappers
{
    public static class EmployeeDadaMapper
    {
        public static UserModel MapUser(this EmployeeDada employee)
        {
            if (employee == null)
                return null;
            string _email = string.Empty;
            if (!string.IsNullOrWhiteSpace(employee.PRIVATE_EMAIL))
            {
                _email = employee.PRIVATE_EMAIL.Reverse();
            }
            if (!string.IsNullOrWhiteSpace(employee.WORK_EMAIL))
            {
                _email = _email.SplitDistinct(employee.WORK_EMAIL.Reverse());
            }
            string _phone = string.Empty;
            if (!string.IsNullOrWhiteSpace(employee.HOME_PHONE_NUMBER) && !string.IsNullOrWhiteSpace(employee.HOME_PHONE_PREFIX) && employee.HOME_PHONE_NUMBER.Length >= 7)
            {
                _phone = string.Format("{0}{1}", employee.HOME_PHONE_PREFIX.prefixPhoneNumber(), employee.HOME_PHONE_NUMBER);
            }
            if (!string.IsNullOrWhiteSpace(employee.WORK_PHONE) && !string.IsNullOrWhiteSpace(employee.WORK_PHONE_EXTENSION) && employee.WORK_PHONE.Length >= 7)
            {
                _phone = _phone.SplitDistinct(string.Format("{0}{1}", employee.WORK_PHONE_EXTENSION.prefixPhoneNumber(), employee.WORK_PHONE));
            }

            return new UserModel
            {
                firstName = employee.FIRST_NAME.NullString(),
                lastName = employee.LAST_NAME.NullString(),
                immigrationDate = employee.IMMIGRATION_DATE.StringToDate(),
                birthDate = employee.BIRTH_DATE.StringToDate(),
                gender = employee.GENDER.ToInt() == 1,
                fathersName = employee.FATHERS_NAME.NullString(),
                countryOfBirth = employee.COUNTRY_OF_BIRTH.NullString(),
                previousFirstName = employee.PREVIOUS_FIRST_NAME.NullString(),
                previousLastName = employee.PREVIOUS_LAST_NAME.NullString(),
                houseNumber = employee.HOUSE_NUMBER.NullableInt(),
                zip = employee.POSTAL_CODE.NullString(),
                ////
                city = employee.SETTLEMENT_NAME.NullString(),
                street = employee.STREET_NAME.NullString(),
                ////
                entranceNumber = employee.HOUSE_ENTRANCE_NUMBER.NullString(),
                apartmentNumber = employee.APARTMENT_NUMBER.NullableInt(),
                poBox = employee.MAILBOX.NullString(),
                maritalStatus = employee.MARITAL_STATUS.NullableInt(),
                maritalStatusDate = employee.MARITAL_STATUS_DATE.StringToDate(true),
                citizenshipType = employee.SUG_EZRACHUT.NullableInt(),
                kibbutzMember = employee.KIBBUTZ_MEMBER.ToBool(),
                hmoCode = employee.HMO_CODE.NullableInt(),
                cell = !string.IsNullOrWhiteSpace(employee.ADDITIONAL_PHONE_PREFIX) && !string.IsNullOrWhiteSpace(employee.ADDITIONAL_PHONE) && employee.ADDITIONAL_PHONE.Length >= 7 ?
                        string.Format("{0}{1}", employee.ADDITIONAL_PHONE_PREFIX.prefixPhoneNumber(), employee.ADDITIONAL_PHONE) : string.Empty,
                phone = _phone,
                email = _email
            };
        }
        public static UserModel MapUser(this EmployeeDada employee, AddressModel Address)
        {
            if (employee == null)
                return null;
            string _email = string.Empty;
            if (!string.IsNullOrWhiteSpace(employee.PRIVATE_EMAIL))
            {
                _email = employee.PRIVATE_EMAIL.Reverse();
            }
            if (!string.IsNullOrWhiteSpace(employee.WORK_EMAIL))
            {
                _email = _email.SplitDistinct(employee.WORK_EMAIL.Reverse());
            }
            string _phone = string.Empty;
            if (!string.IsNullOrWhiteSpace(employee.HOME_PHONE_NUMBER) && !string.IsNullOrWhiteSpace(employee.HOME_PHONE_PREFIX) && employee.HOME_PHONE_NUMBER.Length == 7)
            {
                _phone = string.Format("{0}{1}", employee.HOME_PHONE_PREFIX.prefixPhoneNumber(), employee.HOME_PHONE_NUMBER);
            }
            if (!string.IsNullOrWhiteSpace(employee.WORK_PHONE) && !string.IsNullOrWhiteSpace(employee.WORK_PHONE_EXTENSION) && employee.WORK_PHONE.Length == 7)
            {
                _phone = _phone.SplitDistinct(string.Format("{0}{1}", employee.WORK_PHONE_EXTENSION.prefixPhoneNumber(), employee.WORK_PHONE));
            }

            return new UserModel
            {
                firstName = employee.FIRST_NAME.NullString(),
                lastName = employee.LAST_NAME.NullString(),
                immigrationDate = employee.IMMIGRATION_DATE.StringToDate(),
                birthDate = employee.BIRTH_DATE.StringToDate(),
                gender = employee.GENDER.ToInt() == 1,
                fathersName = employee.FATHERS_NAME.NullString(),
                countryOfBirth = employee.COUNTRY_OF_BIRTH.NullString(),
                previousFirstName = employee.PREVIOUS_FIRST_NAME.NullString(),
                previousLastName = employee.PREVIOUS_LAST_NAME.NullString(),
                houseNumber = employee.HOUSE_NUMBER.NullableInt(),
                zip = employee.POSTAL_CODE.NullString(),
                //////
                //city = Address != null && Address.city != null && !string.IsNullOrWhiteSpace(Address.city.Name) ? Address.city.Name : employee.SETTLEMENT_NAME.NullString(),
                //street = Address != null && Address.street != null && !string.IsNullOrWhiteSpace(Address.street.Name) ? Address.street.Name : employee.STREET_NAME.NullString(),
                //////

                ////
                city = Address != null && !string.IsNullOrWhiteSpace(Address.CityName) ? Address.CityName : (Address != null && Address.city != null && !string.IsNullOrWhiteSpace(Address.city.Name) ? Address.city.Name : employee.SETTLEMENT_NAME.NullString()),

                street = Address != null && !string.IsNullOrWhiteSpace(Address.StreetName) ? Address.StreetName : (Address != null && Address.street != null && !string.IsNullOrWhiteSpace(Address.street.Name) ? Address.street.Name : employee.STREET_NAME.NullString()),
                ////

                entranceNumber = employee.HOUSE_ENTRANCE_NUMBER.NullString(),
                apartmentNumber = employee.APARTMENT_NUMBER.NullableInt(),
                poBox = employee.MAILBOX.NullString(),
                maritalStatus = employee.MARITAL_STATUS.NullableInt(),
                maritalStatusDate = employee.MARITAL_STATUS_DATE.StringToDate(true),
                citizenshipType = employee.SUG_EZRACHUT.NullableInt(),
                kibbutzMember = employee.KIBBUTZ_MEMBER.ToBool(),
                hmoCode = employee.HMO_CODE.NullableInt(),
                cell = !string.IsNullOrWhiteSpace(employee.ADDITIONAL_PHONE_PREFIX) && !string.IsNullOrWhiteSpace(employee.ADDITIONAL_PHONE) && employee.ADDITIONAL_PHONE.Length >= 7 ?
                        string.Format("{0}{1}", employee.ADDITIONAL_PHONE_PREFIX.prefixPhoneNumber(), employee.ADDITIONAL_PHONE) : string.Empty,
                phone = _phone,
                email = _email
            };
        }
        public static SpouseModel MapSpouse(this EmployeeDada employee)
        {
            if (employee == null || string.IsNullOrWhiteSpace(employee.PARTNER_ID_NUMBER))
                return null;
            return new SpouseModel
            {
                jobTitle = employee.PARTNER_JOB.NullString(),
                incomeType = employee.PARTNER_JOB.NullString(),
                idNumber = !string.IsNullOrWhiteSpace(employee.PARTNER_ID_NUMBER.NullString()) ? IdNumberExtender.CheckAndGenerateFullIdNumber(employee.PARTNER_ID_NUMBER) : null,
                calculatedIdNumber = employee.PARTNER_ID_NUMBER.NullString(),
                cell = employee.PARTNER_PHONE_EXTENSION.NullString().prefixPhoneNumber(),
                hasIncome = string.Compare(employee.PARTNER_WORK_BOOL, "כ", true) == 0,
                firstName = employee.PARTNER_FIRST_NAME.NullString(),
                lastName = employee.PARTNER_LAST_NAME.NullString(),
                birthDate = employee.PARTNER_BIRTH_DATE.StringToDate(),
                gender = employee.GENDER.ToInt() == 1,
                maritalStatus = employee.MARITAL_STATUS.NullableInt(),
                maritalStatusDate = employee.MARITAL_STATUS_DATE.StringToDate(true)
            };
        }
        public static List<ChildModel> MapChildren(this EmployeeDada employee, int userId)
        {
            if (employee == null || employee.childrenData == null || employee.childrenData.Count <= 0)
                return null;
            return employee.childrenData.Select(c => c.mapChild(userId)).ToList();
        }
        public static List<CoursModel> MapCourses(this EmployeeDada employee, int userId)
        {
            if (employee == null || employee.educationData == null || employee.educationData.Count <= 0)
                return null;
            return employee.educationData.Select(e => e.mapCours(userId)).ToList();
        }

        private static ChildModel mapChild(this ChildData child, int userId)
        {
            if (child == null || string.IsNullOrWhiteSpace(child.OVD_ILD_MISPAR_ZEHUT))
                return null;
            return new ChildModel
            {
                homeLiving = string.Compare(child.OVD_ILD_GAR_BABAIT, "כ", true) == 0,
                userID = userId,
                birthDate = child.OVD_ILD_T_LEDA.ToDate(),
                firstName = child.OVD_ILD_SHEM,
                gender = child.OVD_ILD_MIN.ToInt() == 1,
                idNumber = IdNumberExtender.CheckAndGenerateFullIdNumber(child.OVD_ILD_MISPAR_ZEHUT),
            };
        }
        private static CoursModel mapCours(this EducationData education, int userId)
        {
            if (education == null)
                return null;

            return new CoursModel
            {
                certificateName = education.CERTIFICATE_NAM.NullString(),
                degree = education.DEGREE.NullString(),
                degreeDate = education.GRADUATION_DATE.StringToDate(),
                educationYears = education.EDUCATION_YEARS.ToInt(),
                institutionName = education.INSTITUTION_NAME.NullString(),
                studySubject1 = education.SUBJECT_1.NullString(),
                studySubject2 = education.SUBJECT_2.NullString(),
                userId = userId
            };
        }
    }
}
