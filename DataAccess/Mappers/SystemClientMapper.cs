﻿using DataAccess.Extensions;
using DataAccess.Models;
using System;
using System.Data;
using System.Text.RegularExpressions;

namespace DataAccess.Mappers
{
    public static class SystemClientMapper
    {
        private static Regex verificationTokenRegex = new Regex(@"[^;a-z0-9.@-\\_-]");
        
        public static INSERTED_SYSTEMUSERS Map(this DataRow row)
        {
            if (row == null)
                return null;

            return new INSERTED_SYSTEMUSERS()
            {
                EMAIL = verificationTokenRegex.Replace(row["EMAIL"].StringTrim(), ""),
                OVD_KAV_MISPAR_ZEHUT = row["OVD_KAV_MISPAR_ZEHUT"].StringTrim(),
                OVD_KTV_TEL_NOSAF = row["OVD_KTV_TEL_NOSAF"].StringTrim(),
                OVD_KTV_TEL_NOSAF_KIDOMET = row["OVD_KTV_TEL_NOSAF_KIDOMET"].StringTrim(),
                OVD_MOZ_SISMA_INTERNET = row["OVD_MOZ_SISMA_INTERNET"].StringTrim(),
                RASHUT_MANCOL = row["RASHUT_MANCOL"].StringTrim(),
                RASHUT_SACHAR = row["RASHUT_SACHAR"].StringTrim(),
                FULL_CELL_NUMBER = (!string.IsNullOrWhiteSpace(row["OVD_KTV_TEL_NOSAF_KIDOMET"].StringTrim()) && !string.IsNullOrWhiteSpace(row["OVD_KTV_TEL_NOSAF"].StringTrim())) ? (string.Format("{0}{1}", row["OVD_KTV_TEL_NOSAF_KIDOMET"].StringTrim().prefixPhoneNumber(), row["OVD_KTV_TEL_NOSAF"].StringTrim())) : null,
                OVD_KAV_BETOKEF_ME = row["OVD_KAV_BETOKEF_ME"].StringTrim(),
                OVD_KAV_SIBAT_SIUM = row["OVD_KAV_SIBAT_SIUM"].StringTrim(),
                OVD_KAV_BETOKEF_AD = row["OVD_KAV_BETOKEF_AD"].StringTrim(),
                TOKEF_AD = row["TOKEF_AD"].StringTrim(),
                BETOKEF_ME = row["BETOKEF_ME"].StringTrim(),
                
                WORK_CELL = row["OVD_MOZ_NETINUT_NOSEFET"].StringTrim(),///קידומת לטלפון ניד מהעבודה
                WORK_CELL_KIDOMET = row["OVD_MOZ_T_ALIA"].StringTrim(),///טלפון נייד מהעבודה
                FULL_WORK_CELL = (!string.IsNullOrWhiteSpace(row["OVD_MOZ_NETINUT_NOSEFET"].StringTrim()) && !string.IsNullOrWhiteSpace(row["OVD_MOZ_T_ALIA"].StringTrim())) ? (string.Format("{0}{1}", row["OVD_MOZ_NETINUT_NOSEFET"].StringTrim().prefixPhoneNumber(), row["OVD_MOZ_T_ALIA"].StringTrim())) : null,
                MIFAL = row["MIFAL"].StringTrim()
            };
        }
    }
}
