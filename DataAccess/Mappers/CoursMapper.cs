﻿using System.Linq;
using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class CoursMapper
    {
        public static CoursModel Map(this COURS model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new CoursModel
            {
                certificateName = model.CERTIFICATENAME,
                degree = model.DEGREE,
                degreeDate = model.DEGREEDATE,
                educationYears = model.EDUCATIONYEARS.ToInt(),
                ID = model.ID.ToInt(),
                institutionName = model.INSTITUTIONNAME,
                studySubject1 = model.STUDYSUBJECT1,
                studySubject2 = model.STUDYSUBJECT2,
                userId = model.USERID,
                user = withInner? model.USER.Map(false) : null
            };
        }
        public static COURS Map(this CoursModel model, int? userId = null)
        {
            if (model == null)
                return null;

            return new COURS
            {
                CERTIFICATENAME = model.certificateName,
                DEGREE = model.degree,
                DEGREEDATE = model.degreeDate,
                EDUCATIONYEARS = model.educationYears,
                ID = model.ID,
                INSTITUTIONNAME = model.institutionName,
                STUDYSUBJECT1 = model.studySubject1,
                STUDYSUBJECT2 = model.studySubject2,
                USERID = userId.HasValue? userId.Value : model.userId
            };
        }
    }
}
