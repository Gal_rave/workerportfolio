﻿using DataAccess.Models;
using System;
using System.Linq;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class StepMapper
    {
        public static StepModel Map(this SHLAVIM model, bool withInner = true)
        {
            if (model == null)
                return null;

            return new StepModel
            {
                recId = model.RECID.ToInt(),
                rashut = model.RASHUT.ToInt(),
                mifal = model.MIFAL.ToInt(),
                tahalichId = model.TAHALICHID.ToInt(),
                modulId = model.MODULEID.ToInt(),
                rechivId = model.RECHIVID.ToInt(),
                shemShalav = model.SHEMSHALAV,
                misparShalav = model.MISPARSHALAV.ToInt(),
                notification = model.NOTIFICATION,
                hova = model.HOVA.ToInt(),
                readOnlyInd = model.READONLYIND.ToInt(),
                shalavAharonInd = model.SHALAVAHARONIND.ToInt(),
                shalavMehuyavInd = model.SHALAVMEHUYAVIND.ToInt(),
                tikufNetunimInd = model.TIKUFNETUNIMIND.ToInt(),
                kvuzatMishtamshim = model.KVUZATMISHTAMSHIM.ToInt(),
                reshimatTiyug = getReshimatTiyug(model.RESHIMATTIYUG.ToInt()),
                fromDate = model.FROMDATE.ToInt(),
                toDate = model.TODATE.ToInt(),
                creationTime = model.CREATIONTIME,
                lastUpdateTime = model.LASTUPDATETIME,
                lastUpdateUser = model.LASTUPDATEUSER.ToInt(),
                timeUnit = model.TIMEUNIT.ToInt(),
                zmanMaxHours = model.ZMANMAXHOURS.ToInt(),
                status = model.STATUS.ToInt(),
                kvuzatMishtamshimModel = model.KVUZOT_MISHTAMSHIM.Map(withInner),
                reshimatTiyugModel = model.RESHIMOT_TIYUG.Map(withInner)
            };
        }
        public static SHLAVIM Map(this StepModel model)
        {
            if (model == null)
                return null;

            return new SHLAVIM
            {
                RECID = model.recId,
                RASHUT = model.rashut,
                MIFAL = model.mifal,
                TAHALICHID = model.tahalichId,
                MODULEID = model.modulId,
                RECHIVID = model.rechivId,
                SHEMSHALAV = model.shemShalav,
                MISPARSHALAV = model.misparShalav,
                NOTIFICATION = model.notification,
                HOVA = model.hova,
                READONLYIND = model.readOnlyInd,
                SHALAVAHARONIND = model.shalavAharonInd,
                SHALAVMEHUYAVIND = model.shalavMehuyavInd,
                TIKUFNETUNIMIND = model.tikufNetunimInd,
                KVUZATMISHTAMSHIM = model.kvuzatMishtamshim,
                RESHIMATTIYUG = getReshimatTiyug(model.reshimatTiyug),
                FROMDATE = model.fromDate,
                TODATE = model.toDate,
                CREATIONTIME = model.creationTime,
                LASTUPDATETIME = model.lastUpdateTime,
                LASTUPDATEUSER = model.lastUpdateUser,
                TIMEUNIT = model.timeUnit,
                ZMANMAXHOURS = model.zmanMaxHours,
                STATUS = model.status
            };
        }

        private static int? getReshimatTiyug(decimal? reshimatTiyug)
        {
            if (reshimatTiyug == null)
                return null; 
            if (reshimatTiyug == -1)
                return null;
            else return Convert.ToInt32(reshimatTiyug);
        }
    }
}