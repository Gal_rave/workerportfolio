﻿using DataAccess.Models;
using DataAccess.Extensions;
using System;

namespace DataAccess.Mappers
{
    public static class CourseFormMapper
    {
        public static ProcessModel MapToProcess(this CourseFormModel model)
        {
            if (model == null)
                return null;

            return new ProcessModel
            {
                CONTENT1 = model.formFields.empRole,
                CONTENT2 = model.formFields.empDepartment,
                CONTENT3 = model.formFields.empYearsOfExperiance.ToString(),
                CONTENT4 = model.formFields.coursePlan,
                CONTENT5 = model.formFields.courseInstitution,
                CONTENT6 = model.formFields.courseNumberOfDays.ToString(),
                CONTENT7 = model.formFields.courseNumberOfHours.ToString(),
                FROM_DATE = null,
                MIFAL = 999,
                PROCESS_DESCRIPTION = "בקשת יציאה להשתלמות\\קורס",
                PROCESS_TYPE = model.selectedProcess.type,
                RASHUT = model.ovedModel.currentMunicipalityId,
                STATUS = 0,
                TARGET_USERID = model.targetUser.idNumber,
                TO_DATE = null,
                USERID = model.ovedModel.calculatedIdNumber.ToInt(),
                CREATION_DATE = DateTime.Now.ToString("dd/MM/yyyy"),
                LAST_UPDATE = DateTime.Now.ToString("dd/MM/yyyy"),
                OVED_NESU_BAKASHA = model.ovedNesuBakasha.ToInt(),
                OVED_NESU_BAKASHA_NAME = model.ovedNesuBakashaName,
                TT_TAHALICH_ID = model.tt_tahalich_id,
                TT_SHALAV_ID = model.tt_shalav_id,
                CURR_MISPAR_SHALAV = model.curr_mispar_shalav,
                SHLAVIM_COUNTER = model.shlavim_counter
            };
        }
    }
}
