﻿using DataAccess.Models;
using DataAccess.Extensions;

namespace DataAccess.Mappers
{
    public static class ImageMapper
    {
        public static ImageModel Map(this IMAGE model, bool withInner = false)
        {
            if (model == null)
                return null;
            return new ImageModel
            {
                ID = model.ID,
                directoryId = model.DIRECTORYID,
                municipalityId = model.MUNICIPALITYID,
                name = model.NAME,
                type = model.TYPE,
                fileBlob = model.FILEBLOB,
                fileBlobString = model.FILEBLOB.ByteArrayToString(),
                userId = model.USERID,

                directory = withInner && model.DIRECTORYID.HasValue ? model.DIRECTORy.Map() : null,
                municipality = withInner? model.MUNICIPALITy.Map(false) : null,
                user = withInner && model.USERID.HasValue ? model.USER.Map(false) : null
            };
        }
        public static IMAGE Map(this ImageModel model)
        {
            if (model == null)
                return null;
            return new IMAGE
            {
                ID = model.ID,
                DIRECTORYID = model.directoryId,
                MUNICIPALITYID = model.municipalityId,
                NAME = model.name,
                TYPE = model.type,
                FILEBLOB = model.fileBlob,
                USERID = model.userId
            };
        }
    }
}
