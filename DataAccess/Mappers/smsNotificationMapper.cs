﻿using DataAccess.Models;
using DataAccess.Extensions;
using System.Linq;
using System;

namespace DataAccess.Mappers
{
    public static class smsNotificationMapper
    {
        public static SmsNotificationModel Map(this SMSNOTIFICATION model)
        {
            if (model == null)
                return null;

            return new SmsNotificationModel
            {
                ID = model.ID.ToInt(),
                message = model.MESSAGE,
                phoneNumber = model.PHONENUMBER,
                response = model.RESPONSE,
                userId = model.USERID.ToInt(),
                sendDate = model.SENDDATE
            };
        }
        public static SMSNOTIFICATION Map(this SmsNotificationModel model)
        {
            if (model == null)
                return null;

            return new SMSNOTIFICATION
            {
                ID = model.ID,
                MESSAGE = model.message,
                PHONENUMBER = model.phoneNumber,
                RESPONSE = model.response,
                USERID = model.userId,
                SENDDATE = model.sendDate,
            };
        }
        public static SmsNotificationModel MapToSms(this USER model, string message = null)
        {
            if (model == null)
                return null;

            return new SmsNotificationModel
            {
                message = message,
                phoneNumber = string.IsNullOrWhiteSpace(model.CELL)? model.PHONE : model.CELL,
                userId = model.ID.ToInt(),
            };
        }
        public static SmsNotificationModel MapToSms(this UserModel model, string message = null)
        {
            if (model == null)
                return null;

            return new SmsNotificationModel
            {
                message = message,
                phoneNumber = string.IsNullOrWhiteSpace(model.cell) ? model.phone : model.cell,
                userId = model.ID,
                sendDate = DateTime.Now
            };
        }
        public static SmsNotificationModel MapToSms(this UserModel model, string[] addedPhones, string message = null)
        {
            if (model == null)
                return null;

            var tempLIst = addedPhones.ToList();
            tempLIst.Add((string.IsNullOrWhiteSpace(model.cell) ? model.phone : model.cell));
            return new SmsNotificationModel
            {
                message = message,
                phoneNumber = string.Join(",", tempLIst.Where(x => !string.IsNullOrEmpty(x)).ToArray()),
                userId = model.ID
            };
        }
    }
}
