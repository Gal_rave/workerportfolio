﻿using System.Collections.Generic;
using DataAccess.Models;

namespace DataAccess.EqualityComparers
{
    public class TerminationEqualityComparer : IEqualityComparer<TERMINATION>
    {
        public int GetHashCode(TERMINATION co)
        {
            if (co == null)
            {
                return 0;
            }
            return co.TERMINATIONCAUSE.GetHashCode();
        }

        public bool Equals(TERMINATION x1, TERMINATION x2)
        {
            if (object.ReferenceEquals(x1, x2))
            {
                return true;
            }
            if (object.ReferenceEquals(x1, null) || object.ReferenceEquals(x2, null))
            {
                return false;
            }
            return x1.TERMINATIONCAUSE == x2.TERMINATIONCAUSE;
        }
    }
}