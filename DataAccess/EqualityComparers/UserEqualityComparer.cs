﻿using System.Collections.Generic;
using DataAccess.Models;

namespace DataAccess.EqualityComparers
{
    public class UserEqualityComparer : IEqualityComparer<USER>
    {
        public int GetHashCode(USER usr)
        {
            if (usr == null)
            {
                return 0;
            }
            return usr.ID.GetHashCode();
        }

        public bool Equals(USER usr1, USER usr2)
        {
            if (object.ReferenceEquals(usr1, usr2))
            {
                return true;
            }
            if (object.ReferenceEquals(usr1, null) || object.ReferenceEquals(usr2, null))
            {
                return false;
            }
            return usr1.ID == usr2.ID;
        }
    }
}
