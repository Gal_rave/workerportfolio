﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.EqualityComparers
{
    public static class PcueArchiveEqualityComparer
    {
        public static bool Compare(this ICollection<PCUE_SENDARCHIVE> PCUE_SENDARCHIVE, DateTime payMonth, int municipalityId)
        {
            if (PCUE_SENDARCHIVE == null || PCUE_SENDARCHIVE.FirstOrDefault(pc=> pc.MUNICIPALITYID == municipalityId) == null)
                return false;

            bool res = PCUE_SENDARCHIVE.ToList().FirstOrDefault(pc => pc.MUNICIPALITYID == municipalityId && pc.PC_YEAR == payMonth.Year && pc.PC_MONTH == payMonth.Month) == null;

            return !res;
        }
    }
}
